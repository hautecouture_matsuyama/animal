﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IpadAspectRation : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {

#if UNITY_EDITOR

        if(Screen.width == 2048)
        {
            GetComponent<RectTransform>().localScale = new Vector2(0.8f, 0.8f);
        }

#else
        if(SystemInfo.deviceModel.Contains("iPad"))
        {
             GetComponent<RectTransform>().localScale = new Vector2(0.8f, 0.8f);

        }
#endif
    }

}
