﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour {

    [SerializeField]
    private Sprite sound_off;
    [SerializeField]
    private Sprite sound_on;

    private Image soundImage;

    private void Start()
    {
        soundImage = GetComponent<Image>();
    }

    public void ButtonClick()
    {
        MenuScreen.isSoundOn = !MenuScreen.isSoundOn;
        soundImage.sprite = MenuScreen.isSoundOn ? sound_on : sound_off;
    }

}
