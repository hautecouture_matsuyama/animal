using UnityEngine;
using System.Collections; 

public class SwipeDetector : MonoBehaviour {
    // Values to set:
    public float comfortZone = 50.0f;//touch comfort zone
    public float minSwipeDist = 15.0f;//minimum sipe distance
    public float maxSwipeTime = 0.5f; //maximum swipe duration

    private float startTime;//starting time
    private Vector2 startPos;//starting position
    private bool couldBeSwipe;//this swipe can be vertical swipe
	private bool couldBeHorSwipe;//this swipe can be horizontal swipe
	
	public bool swipeRightCD = false;//right swipe cool down
	private float swipeRightTime = 1f;//pause time between swipes
	private float swipeRightTimeReset = 1f;//pause time reseter
	
	public bool swipeLeftCD = false;//left swipe cool down
	private float swipeLeftTime = 1f;//pause time between swipes
	private float swipeLeftTimeReset = 1f;//pause time reseter

    public enum SwipeDirection {//swipe directions
        None,
        Up,
        Down,
		Right,
		Left
    }
    public SwipeDirection lastSwipe = SwipeDetector.SwipeDirection.None;//set swipe direction 
    public float lastSwipeTime;    
	
    void  Update() 
    {
        if (Input.touchCount > 0) //if we detect touch input
        {
            Touch touch = Input.touches[0];
            switch (touch.phase) 
            {
                case TouchPhase.Began://start touch phase
                    lastSwipe = SwipeDetector.SwipeDirection.None;
                    lastSwipeTime = 0;//zeroing last Swipe Time
                    couldBeSwipe = true;//this swipe can be vertical swipe
				    couldBeHorSwipe = true;//this swipe can be horizontal swipe
                    startPos = touch.position;//starting position is touch position
                    startTime = Time.time;
                    break;
                case TouchPhase.Moved:
					//if we moved finger horizontaly and length of the swipe is greater than comfortZone value
                    if (Mathf.Abs(touch.position.x - startPos.x) > comfortZone){
                        couldBeSwipe = false;//this is not vertical swipe
                    	}
					if (Mathf.Abs(touch.position.y - startPos.y) > comfortZone){
                        couldBeHorSwipe = false;//this is not horizontal swipe
                    	}
                    break;
                case TouchPhase.Ended:
                    if (couldBeSwipe){//if this can be vertical swipe
                        float swipeTime = Time.time - startTime;//calculate swipe duration
					    float swipeDist = (touch.position - startPos).magnitude;//calculate swipe length
						//if swipe duration is lesser  than maxSwipeTime and it's length is greater than minSwipeDist
                        if ((swipeTime < maxSwipeTime) && (swipeDist > minSwipeDist)){   
						//It's a swipe!
						//Return value is 1 when f is positive or zero, -1 when f is negative.
                            float swipeValue = Mathf.Sign(touch.position.y - startPos.y);
                            if (swipeValue > 0){//if positive value
                                lastSwipe = SwipeDetector.SwipeDirection.Up;//it's swipe up 
							}
                            else if (swipeValue < 0){//if negative value
                                lastSwipe = SwipeDetector.SwipeDirection.Down;//it's swipe down                                       
							}
						lastSwipeTime = Time.time;//resetting last swipe time
                        }
                    }
					if (couldBeHorSwipe){//if it can be horizontal swipe
						float swipeTime = Time.time - startTime;//calculate swipe duration
                        float swipeDist = (touch.position - startPos).magnitude;//calculate swipe length
                    	//if swipe duration is lesser  than maxSwipeTime and it's length is greater than minSwipeDist    
						if ((swipeTime < maxSwipeTime) && (swipeDist > minSwipeDist)) 
                        {   
						// It's a swipe
						//Return value is 1 when f is positive or zero, -1 when f is negative.
                            float swipeValue = Mathf.Sign(touch.position.x - startPos.x);
                            //if positive value and swipe right cooldown is false
							if ((swipeValue > 0)&&(!swipeRightCD)){
                                lastSwipe = SwipeDetector.SwipeDirection.Right;//this is right swipe
								swipeRightCD = true;//swipe cool down - true
							}
							//if positive value and swipe right cooldown is false
                            else if ((swipeValue < 0)&&(!swipeLeftCD)){
                                lastSwipe = SwipeDetector.SwipeDirection.Left;//this is left swipe
								swipeLeftCD = true;//swipe cool down - true
							}
						lastSwipeTime = Time.time;//resetting last swipe time
                        }
				}
				
                    break;
            }
        }
		if(swipeRightCD){//if swipe right cool down is true
			if (swipeRightTime > 0)//while swipe Right Time is greater than zero 
				swipeRightTime -= Time.deltaTime;//descrease swipe Right Time by Time.deltaTime (http://docs.unity3d.com/Documentation/ScriptReference/Time-deltaTime.html)
				if (swipeRightTime < 0) 
				swipeRightTime = 0; //don't make swipe Right Time timer lesser than 0
				if (swipeRightTime == 0){//when swipe Right Time timer became zero
				swipeRightCD = false;
				swipeRightTime = swipeRightTimeReset;//lets reset swipe Right Time timer
			}
		}
				if(swipeLeftCD){
			if (swipeLeftTime > 0)//while attack swipe Right Time is greater than zero 
				swipeLeftTime -= Time.deltaTime;//descrease swipe Right Time timer by Time.deltaTime (http://docs.unity3d.com/Documentation/ScriptReference/Time-deltaTime.html)
				if (swipeLeftTime < 0) 
				swipeLeftTime = 0; //don't make swipe Right Time timer lesser than 0
				if (swipeLeftTime == 0){//when swipe Right Time timer became zero
				swipeLeftCD = false;
				swipeLeftTime = swipeLeftTimeReset;//lets reset swipe Right Time timer
			}
		}
    }
}