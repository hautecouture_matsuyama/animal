﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BestScore : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// Set the Best Score GUI Text value to Stored best score
        gameObject.GetComponent<Text>().text = PlayerPrefs.GetInt("Score", 0).ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}