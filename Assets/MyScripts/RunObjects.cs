﻿using UnityEngine;
using System.Collections;

// When applied in any GameObject, it will move according to speed given in PlayerControl class
public class RunObjects : MonoBehaviour {

    [SerializeField]
    private Sprite[] objctSprite; 

	// Use this for initialization
	void Start () {

        GetComponent<SpriteRenderer>().sprite = Random.value >= 0.5f ? objctSprite[0] : objctSprite[1];
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 moveDir = new Vector3(1f, 0, 0);
		transform.Translate(moveDir * PlayerControl.speed * Time.deltaTime);
	}
}
