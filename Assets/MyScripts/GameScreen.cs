﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameScreen : MonoBehaviour {

    private string platformType = null;
	// Ground prefab variable
	public GameObject groundPrefab;
	
	// Obstacle Prefabs for Low and High obstacles.
	public GameObject obsPrefab;
	public GameObject obs2Prefab;

	// Obstacles array variable.
	private GameObject[] obs;

	// Player variable
	private GameObject player;

	// Screen border variables
	private float bottomY;
	private float leftX;
	private float topY;
	private float rightX;

	// Height variale of obstacle and ground
	private float obsTileH;
	private float obs2TileH;
	private float groundTileH;

	// Boolean variable to determine is game is paused
	public static bool paused;

	// This variable will determine position of obstacles either low or high
	private float obsHeightScale;

	// Gap between two obstacles. This decreases over time to increase difficulty
	private float gap;

	private float height1, height2;
	private BoxCollider2D obsCollider, obs2Collider;

	// AdMob variable
//	public static AdMobPlugin admob;

	// Game over sound
	public AudioClip overClip;
	private AudioSource overSource;
	public AudioClip deadClip;
	public static AudioSource deadSource;

	public static bool canControl = true;

    private int objectMax = 6;


    private Renderer pauseBg;
    private Renderer btnResume;
    private Renderer btnHome;
    private Renderer btnReplay;
    private GUIText pauseText;
    private Renderer overBg;
    private Renderer btnHomeOver;
    private Renderer btnReplayOver;
    private Renderer btnPause;

    private GameObject SherePlugins;


    [SerializeField]
    private GameObject gameOverScreen;

	// Use this for initialization
	void Start () {

		// Starting game speed
		PlayerControl.speed = -2.8f;

		// Game is not paused at start
		paused = false;


		// Get screen borders
		float dist = (transform.position - Camera.main.transform.position).z;
		leftX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		rightX = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		topY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, -dist)).y;
		bottomY = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).y;

		// Get the ground line gameObject
		player = GameObject.Find("Player");

		// Get height/width of obstacle & ground
		obsTileH = obsPrefab.GetComponent<Renderer>().bounds.size.y;
		obs2TileH = obs2Prefab.GetComponent<Renderer>().bounds.size.y;
		groundTileH = groundPrefab.GetComponent<Renderer>().bounds.size.y;

		obsCollider = obsPrefab.GetComponent<Collider2D>() as BoxCollider2D;
		obs2Collider = obs2Prefab.GetComponent<Collider2D>() as BoxCollider2D;

		// Get and position pause button
		//GameObject btnPause = GameObject.Find("btn-pause");
//		btnPause.transform.position = new Vector3(leftX + 0.2f, bottomY + btnPause.renderer.bounds.size.y/2 + 0, 0);

		// Gap between obstacles
		gap = 10;

		// Here we create 6 obstacles objects, so that they will loop in game.
		// We don't have to create new obstacles.

		obs = new GameObject[objectMax];
		GameObject obsRandom = getRandomObs();
        
        obs[0] = Instantiate(obsRandom, new Vector3(rightX + 5, bottomY + groundTileH / 2 + obsHeightScale, 0), Quaternion.identity) as GameObject;
        
        for(short idx = 1; idx < objectMax; idx++)
        {
            obsRandom = getRandomObs();
            obs[idx] = Instantiate(obsRandom, new Vector3(obs[idx - 1].transform.position.x + RandomGap(), bottomY + groundTileH / 2 + obsHeightScale, 0), Quaternion.identity) as GameObject;
        }

		// Start Timed and Delayed functions to spawn obstacles/coins,
		// Check for game over, hide info after few seconds, etc
		StartCoroutine(changeSpeed());
		StartCoroutine(changeScore());
		StartCoroutine(showIfGameOver());
		StartCoroutine(hideInfo());

		// Set Game Over Music
		overSource = gameObject.AddComponent<AudioSource>();
		overSource.playOnAwake = false;
		overSource.rolloffMode = AudioRolloffMode.Linear;
		overSource.clip = overClip;

		// Set Game Dead Sound
		deadSource = gameObject.AddComponent<AudioSource>();
		deadSource.playOnAwake = false;
		deadSource.rolloffMode = AudioRolloffMode.Linear;
		deadSource.clip = deadClip;

		// If game is paused/over we have to set Timescale to 1 when Game starts
		if(Time.timeScale !=1){
			Time.timeScale = 1;
		}
	}


	GameObject getRandomObs(){
		GameObject obstacle;
		int rand = Random.Range(1, 6);
		if(rand <= 3){
			obstacle = obsPrefab;
			obsHeightScale = obsTileH/5;
			height1 = obsHeightScale;
		}else{
			obstacle = obs2Prefab;
            obsHeightScale = player.transform.GetComponent<Renderer>().bounds.size.y +player.transform.GetComponent<Renderer>().bounds.size.y / 30;
			height2 = obsHeightScale;
		}
		return obstacle;
	}

	// Method to change speed of game every 5 seconds
	IEnumerator changeSpeed() {
		while (true){
			yield return new WaitForSeconds(5);
			if(PlayerControl.speed > -4 && !PlayerControl.isDead){
				PlayerControl.speed -= 0.21f;
				gap -= 0.99f;
			}
		}
	}

	// Method to change score of game every 0.7 seconds
	IEnumerator changeScore() {
		while (true){
			yield return new WaitForSeconds(0.7f);
			if(!PlayerControl.isDead){
				Score.scoreNum += 1;
			}
		}
	}

	// Method to hide info texts after 3 Seconds
	IEnumerator hideInfo() {
		yield return new WaitForSeconds(3);
        GameObject.Find("InfoText").SetActive(false);
	}

	// Method to check for game over every few seconds
	IEnumerator showIfGameOver() {
		while (true){
			//値によってはゲーム終了時にアニメーションが出ない時がある.
			yield return new WaitForSeconds(1.5f);
			if(PlayerControl.isDead){
				gameOverScene();
			}
		}
	}
	

	// Update is called once per frame
	void Update () {

        if (obs[0].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[0].GetComponent<ObsDownCollisionCheck>();
                //tc は 下の敵のとこを指している。　tc == null は GetComponentで取得したいコンポーネントを取得できなかった場合nullが入る
                if (tc == null)
                {
                    //nullなので下の敵を登録する
                    obs[0].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[0].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[0].GetComponent<ObsUpCollisionCheck>());
                    obs[0].AddComponent<ObsDownCollisionCheck>();
                }
                //ランダム生成の結果、前回と同じ(下の敵)オブジェクトだった為、下の敵用の高さに設定
                obsHeightScale = height1;
            }
            else
            {
                //oc は 上の敵のとこを指している。　
                ObsUpCollisionCheck oc = obs[0].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    //null の場合なので現在下の敵が入っている、今回上の敵を登録したいので今の下の敵を削除して上の敵を登録する
                    obs[0].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[0].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[0].GetComponent<ObsDownCollisionCheck>());
                    obs[0].AddComponent<ObsUpCollisionCheck>();
                }
                //上の適用の高さに設定する
                obsHeightScale = height2;
                if(obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            //0番目用の位置情報を設定する。
            obs[0].transform.position = new Vector3(obs[5].transform.position.x + RandomGap(), bottomY + groundTileH / 2 + obsHeightScale, 0);
            obs[0].GetComponent<Collider2D>().enabled = true;
            obs[0].GetComponent<Renderer>().enabled = true;
        }
        if (obs[1].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[1].GetComponent<ObsDownCollisionCheck>();
                if (tc == null)
                {
                    obs[1].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[1].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[1].GetComponent<ObsUpCollisionCheck>());
                    obs[1].AddComponent<ObsDownCollisionCheck>();
                }
                obsHeightScale = height1;
            }
            else
            {
                ObsUpCollisionCheck oc = obs[1].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    obs[1].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[1].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[1].GetComponent<ObsDownCollisionCheck>());
                    obs[1].AddComponent<ObsUpCollisionCheck>();
                }
                obsHeightScale = height2;
                if (obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            obs[1].transform.position = new Vector3(obs[0].transform.position.x + RandomGap(), bottomY + groundTileH / 2 + obsHeightScale, 0);
            obs[1].GetComponent<Collider2D>().enabled = true;
            obs[1].GetComponent<Renderer>().enabled = true;
        }
        if (obs[2].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[2].GetComponent<ObsDownCollisionCheck>();
                if (tc == null)
                {
                    obs[2].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[2].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[2].GetComponent<ObsUpCollisionCheck>());
                    obs[2].AddComponent<ObsDownCollisionCheck>();
                }
                obsHeightScale = height1;
            }
            else
            {
                ObsUpCollisionCheck oc = obs[2].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    obs[2].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[2].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[2].GetComponent<ObsDownCollisionCheck>());
                    obs[2].AddComponent<ObsUpCollisionCheck>();
                }
                obsHeightScale = height2;
                if (obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            obs[2].transform.position = new Vector3(obs[1].transform.position.x + RandomGap(), bottomY + groundTileH / 2 + obsHeightScale, 0);
            obs[2].GetComponent<Collider2D>().enabled = true;
            obs[2].GetComponent<Renderer>().enabled = true;
        }
        if (obs[3].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[3].GetComponent<ObsDownCollisionCheck>();
                if (tc == null)
                {
                    obs[3].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[3].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[3].GetComponent<ObsUpCollisionCheck>());
                    obs[3].AddComponent<ObsDownCollisionCheck>();
                }
                obsHeightScale = height1;
            }
            else
            {
                ObsUpCollisionCheck oc = obs[3].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    obs[3].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[3].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[3].GetComponent<ObsDownCollisionCheck>());
                    obs[3].AddComponent<ObsUpCollisionCheck>();
                }
                obsHeightScale = height2;
                if (obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            obs[3].transform.position = new Vector3(obs[2].transform.position.x + RandomGap(), bottomY + groundTileH / 2 + obsHeightScale, 0);
            obs[3].GetComponent<Collider2D>().enabled = true;
            obs[3].GetComponent<Renderer>().enabled = true;
        }
        if (obs[4].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[4].GetComponent<ObsDownCollisionCheck>();
                if (tc == null)
                {
                    obs[4].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[4].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[4].GetComponent<ObsUpCollisionCheck>());
                    obs[4].AddComponent<ObsDownCollisionCheck>();
                }
                obsHeightScale = height1;
            }
            else
            {
                ObsUpCollisionCheck oc = obs[4].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    obs[4].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[4].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[4].GetComponent<ObsDownCollisionCheck>());
                    obs[4].AddComponent<ObsUpCollisionCheck>();
                }
                obsHeightScale = height2;
                if (obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            obs[4].transform.position = new Vector3(obs[3].transform.position.x + RandomGap(), bottomY + groundTileH / 2 + obsHeightScale, 0);
            obs[4].GetComponent<Collider2D>().enabled = true;
            obs[4].GetComponent<Renderer>().enabled = true;
        }
        if (obs[5].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[5].GetComponent<ObsDownCollisionCheck>();
                if (tc == null)
                {
                    obs[5].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[5].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[5].GetComponent<ObsUpCollisionCheck>());
                    obs[5].AddComponent<ObsDownCollisionCheck>();
                }
                obsHeightScale = height1;
            }
            else
            {
                ObsUpCollisionCheck oc = obs[5].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    obs[5].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[5].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[5].GetComponent<ObsDownCollisionCheck>());
                    obs[5].AddComponent<ObsUpCollisionCheck>();
                }
                obsHeightScale = height2;
                if (obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            obs[5].transform.position = new Vector3(obs[4].transform.position.x + RandomGap(), bottomY + groundTileH / 2 + obsHeightScale, 0);
            obs[5].GetComponent<Collider2D>().enabled = true;
            obs[5].GetComponent<Renderer>().enabled = true;
        }

		// When back button is pressed, if its not paused, we pause the game,
		// If game is paused, we resume the game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused && !PlayerControl.isDead)
            {
                Time.timeScale = 0; // pause
                paused = true;
                // Make paused screen elements visible when paused
                pauseBg.enabled = true;
                btnResume.enabled = true;
                btnHome.enabled = true;
                btnReplay.enabled = true;
                pauseText.enabled = true;
                PlayerControl.musicSource.Stop(); // stop music

            }
            else if (paused && !PlayerControl.isDead)
            {
                Time.timeScale = 1; // resume
                paused = false;
                // Hide paused screen elements when resumed
                pauseBg.enabled = false;
                btnResume.enabled = false;
                btnHome.enabled = false;
                btnReplay.enabled = false;
                pauseText.enabled = false;
                if (MenuScreen.isSoundOn)
                {
                    PlayerControl.musicSource.Play(); // resume music
                }
            }
        }
	}

	// Method to show Game Over Screen
	public void gameOverScene(){
        //NendAdInterstitial.Instance.Show();
		Time.timeScale = 0; // Stop game

		PlayerControl.musicSource.Stop(); // stop music
		if(MenuScreen.isSoundOn){
			overSource.Play(); // play game over sound
		}

		// If Current score if better than previously saved score, store current score
		if(Score.scoreNum > Score.bestNum){
			PlayerPrefs.SetInt("Score", Score.scoreNum);
		}

        gameOverScreen.SetActive(true);
        GameObject.Find("score_text").GetComponent<Text>().text = Score.scoreNum.ToString();

        NendUnityPlugin.AD.NendAdInterstitial.Instance.Show();

        //banner.Show();
    }


    private int RandomGap()
    {
        int gap = 10;
        if(Score.scoreNum > 40 && Score.scoreNum < 70)
        {
            gap = Random.Range(5, 9);
        }
        else if(Score.scoreNum > 70)
        {
            gap = Random.Range(3, 7);
        }
        return gap;
    }
}



