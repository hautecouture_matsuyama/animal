﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	// Variables for current and best score
	public static int scoreNum;
	public static int bestNum;

	// Use this for initialization
	void Start () {
		// Starting score is 0
        if (gameObject.name != "GameOverScoreUI")
        {
            scoreNum = 0;
        }
		
		// Get previously saved best score and set to the bestNum variable
		bestNum = PlayerPrefs.GetInt("Score", 0);

	}
	
	// Update is called once per frame
	void Update () {
		// Set/ Update the score text.
        
        gameObject.GetComponent<Text>().text = scoreNum.ToString();
        
         
	}
}
