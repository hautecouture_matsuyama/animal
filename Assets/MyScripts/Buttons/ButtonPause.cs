﻿using UnityEngine;
using System.Collections;

public class ButtonPause : MonoBehaviour {

    [SerializeField]
    private GameObject pause_screen;

 	public void OnMouseUpAsButton() {   

		Time.timeScale = 0; // pause
		GameScreen.paused = true;
		PlayerControl.musicSource.Stop();

        pause_screen.SetActive(true);
    }

}
