﻿using UnityEngine;
using System.Collections;

public class ButtonSound : MonoBehaviour {

	// Use this for initialization
    public Color colorPushed;   
	private Color originalColor;

	private GameObject buttonOff;

	// Use this for initialization
	void Start () {
		//originalColor = gameObject.renderer.material.color;    

		// Get Screen border variables
		float dist = (transform.position - Camera.main.transform.position).z;
		float leftX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		float rightX = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		float topY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, -dist)).y;
		float bottomY = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).y;

		transform.position = new Vector3(rightX - transform.GetComponent<Renderer>().bounds.size.x, topY - transform.GetComponent<Renderer>().bounds.size.y, 0);

		buttonOff = GameObject.Find("btn-sOff");
		buttonOff.transform.position = transform.position;

		if(MenuScreen.isSoundOn){
			gameObject.GetComponent<Renderer>().enabled = true;
			buttonOff.gameObject.GetComponent<Renderer>().enabled = false;
		}else{
			buttonOff.gameObject.GetComponent<Renderer>().enabled = true;
			gameObject.GetComponent<Renderer>().enabled = false;
		}
	}
	
	void OnMouseUpAsButton() {
		if(MenuScreen.isSoundOn){
			MenuScreen.isSoundOn = false;
			gameObject.GetComponent<Renderer>().enabled = false;
			buttonOff.gameObject.GetComponent<Renderer>().enabled = true;
		}else{
			MenuScreen.isSoundOn = true;
			buttonOff.gameObject.GetComponent<Renderer>().enabled = false;
			gameObject.GetComponent<Renderer>().enabled = true;
		}
	}

}
