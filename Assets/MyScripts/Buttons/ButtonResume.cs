﻿using UnityEngine;
using System.Collections;

public class ButtonResume : MonoBehaviour {

	public void OnMouseUpAsButton() {       

			if(MenuScreen.isSoundOn){
				PlayerControl.musicSource.Play();
			}
			Time.timeScale = 1;
			GameScreen.paused = false;

        GameObject.Find("pause_screen").SetActive(false);

	}
}
