﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonHome : MonoBehaviour {

	public void OnMouseUpAsButton() {       
		if(Time.timeScale != 1){
			Time.timeScale = 1;
            SceneManager.LoadScene("MenuScene");
		}
	}
}
