﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SherePlugins : MonoBehaviour {

    public Texture2D shareTexture;

    public void ShareTwitter()
    {
		//Debug.Log ("shareTwitter");
        int score = Score.scoreNum;
        string url = "https://play.google.com/store/apps/details?id=com.hautecouture.stickmanrunner&hl=ja";
        //UM_ShareUtility.TwitterShare("Endless_Soccer " + score + " を獲得！君も華麗なドリブルを決めよう！" + url + " #Endless_Soccer", shareTexture);
    }

    public void ShareLINE()
    {
        int score = Score.scoreNum;
        string msg = "Endless_Soccer " + score + " を獲得！君も華麗なドリブルを決めよう！" + "https://play.google.com/store/apps/details?id=com.hautecouture.stickmanrunner&hl=ja";
        string url = "http://line.me/R/msg/text/?" + Uri.EscapeUriString(msg);
        Application.OpenURL(url);
    }

    public void ShareFacebook()
    {
      
    }

    public void OnClick()
    {
        if (gameObject.name == "share_facebook")
        {
            ShareFacebook();
        }

        if (gameObject.name == "share_line")
        {
            ShareLINE();
        }

        if (gameObject.name == "share_twitter")
        {
			//Debug.Log ("shareTwitter");
			ShareTwitter();
        }
    }
}