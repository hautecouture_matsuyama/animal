﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

	public bool jump = false;				// Condition for whether the player should jump.
	public static bool slide = false;				// Condition for whether the player should slide.
	//public static bool sliding = false;		// Variable to check if player is sliding.
	private bool grounded = false;			// Whether or not the player is grounded.
	private bool leftTap;					// If user taps on left of screen.
	private bool rightTap;					// If user taps on right of screen.
	public static bool isDead;				// If Player is dead.

	public float jumpForce = 450f;			// Amount of force added when the player jumps.
	public static float speed = 0;			// Starting game speed, moves everything with this speed

	private Transform groundCheck;			// A position marking where to check if the player is grounded.
	

	public static Animator anim;			// Reference to the player's animator component.
	
	private SwipeDetector swipeDetector;	//touch input script
	

	// Sound variables for Jump, Slide and Background Music
	public AudioClip jumpClip;
	public AudioClip slideClip;
	public AudioClip musicClip;
	private AudioSource jumpSource;
	private AudioSource slideSource;
    public static AudioSource musicSource;


	void Awake() 
	{
		// Setting up references.
		groundCheck = transform.Find("groundCheck");
		anim = GetComponent<Animator>();
		
	}
	
	void Start() 
	{
        jump = false;
        slide = false;
        grounded = false;
		// Initializing and setting up sound and other variables
        swipeDetector = (SwipeDetector)transform.GetComponent(typeof(SwipeDetector));
		leftTap = false;
		rightTap = false;

		jumpSource = gameObject.AddComponent<AudioSource>();
		jumpSource.playOnAwake = false;
		jumpSource.rolloffMode = AudioRolloffMode.Linear;
		jumpSource.clip = jumpClip;

		slideSource = gameObject.AddComponent<AudioSource>();
		slideSource.playOnAwake = false;
		slideSource.rolloffMode = AudioRolloffMode.Linear;
		slideSource.clip = slideClip;

		musicSource = gameObject.AddComponent<AudioSource>();
		musicSource.playOnAwake = false;
		musicSource.volume = 0.75f;
		musicSource.loop = true;
		musicSource.rolloffMode = AudioRolloffMode.Linear;
		musicSource.clip = musicClip;
		if(MenuScreen.isSoundOn){
			musicSource.Play();
		}
		isDead = false;
	}
	
	void Update() {

        if (!isDead)
        {
            ChangePlayerSpeed();

            if (!GameScreen.paused)
            {
                // The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
				grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground") );

                Debug.Log("groundには" + grounded);

				if (!slide)
                {
                    //		if(game)
                    // Get taps from user and check if tap is done in right or left side of screen
                    //現在タッチされている数を返す
                    if (Input.touchCount > 0)
                    {
                        //0番目のタッチ情報を取得
                        Touch touch = Input.GetTouch(0);

                        if (touch.phase == TouchPhase.Began && touch.position.x < Screen.width / 2)
                        {
                            if (grounded)
                            {
                                leftTap = true;
                                rightTap = false;
                            }
                        }
                        else if (touch.phase == TouchPhase.Began && touch.position.x > Screen.width / 2)
                        {
                            if (grounded)
                            {
                                leftTap = false;
                                rightTap = true;
                            }
                        }
                    }
                }


                // UnComment below code to enable Swipe Feature in mobile
                //		if(swipeDetector.lastSwipe == SwipeDetector.SwipeDirection.Up && grounded) {
                if ((Input.GetKey(KeyCode.UpArrow) || rightTap ) && grounded  && !slide )
				//if ((Input.GetButtonDown("Jump") || rightTap) && !slide)
                {
                    //			Debug.Log("Swipped Up!!");
                    jump = true;
                    rightTap = false;
                }

                // UnComment below code to enable Swipe Feature in mobile
                //		if(swipeDetector.lastSwipe == SwipeDetector.SwipeDirection.Down && grounded) {
                if ((Input.GetKey(KeyCode.DownArrow) || leftTap) && grounded && !jump && !slide )
				//if ((Input.GetButtonDown("Slide") || leftTap)  && !jump && !slide)
                {
                    //			Debug.Log("Swipped Down!!");
                    slide = true;
                    leftTap = false;
                    anim.SetTrigger("Slide");

                    if (MenuScreen.isSoundOn)
                    {
                        slideSource.Play(); // Play Slide Sound
                    }
                    StartCoroutine("SlideTime");
                }
            }
        }
	}

	void FixedUpdate () {

        if (!GameScreen.paused)
        {
            if (jump && !slide)
            {
                // Set the Jump animator trigger parameter.
                anim.SetTrigger("Jump");

                if (MenuScreen.isSoundOn)
                {
                    jumpSource.Play(); // Play Jump sound
                }

                // Add a vertical force to the player.
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));

                // Make sure the player can't jump again until the jump conditions from Update are satisfied.
                jump = false;
               // swipeDetector.lastSwipe = SwipeDetector.SwipeDirection.None;
            }
        }
	}

    private IEnumerator SlideTime()
    {
        if(slide)
        {
            yield return new WaitForSeconds(0.5f);
            slide = false;
        }
    }
    private int prevScore = 0;
    private void ChangePlayerSpeed()
    {
        if (Score.scoreNum > 99 && (Score.scoreNum % 100 == 0) && Score.scoreNum != prevScore)
        {
            speed += speed / 100 * 3;
        }
        prevScore = Score.scoreNum;
    }

}







