﻿using UnityEngine;
using System.Collections;

public class MovingBall : MonoBehaviour {


    private float sign;
    private int nowTime;
    private const short moveRimit = 15;
    public short rotateSpeed;

    [SerializeField]
    private float moveSpeed;

    private GameObject soccerBall;

    // Use this for initialization
    void Start () {

        soccerBall = GameObject.Find("SoccerBall");
        sign = 1;
        rotateSpeed = -1000;
        nowTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
        soccerBall.transform.Rotate(new Vector3(0, 0, rotateSpeed * Time.deltaTime));

        if (!GameScreen.paused && !PlayerControl.isDead)
        {
            MoveBall();

            nowTime++;

            Debug.Log("nowは" + nowTime);

            if (nowTime > moveRimit)
            {
                sign *= -1;
                nowTime = 0;
            }
        }
	}

    void MoveBall()
    {
        transform.localPosition = new Vector2(Mathf.Clamp(transform.localPosition.x, 0.3f, 1.3f), -1);
        transform.Translate(moveSpeed * sign * Time.deltaTime, 0, 0);
    }

}
