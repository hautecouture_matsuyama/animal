﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// NendUnityPlugin.Common.NendUtils/Account
struct Account_t2230227361;
// NendUnityPlugin.AD.Native.NendAdNativeView[]
struct NendAdNativeViewU5BU5D_t3641134489;
// NendUnityPlugin.AD.Native.Events.NativeAdViewEvent
struct NativeAdViewEvent_t610464567;
// NendUnityPlugin.AD.Native.Events.NativeAdViewFailedToLoadEvent
struct NativeAdViewFailedToLoadEvent_t3307153462;
// NendUnityPlugin.AD.Native.INativeAdClient
struct INativeAdClient_t1973857504;
// NendUnityPlugin.AD.Native.Utils.SimpleTimer
struct SimpleTimer_t4075384413;
// System.Func`2<NendUnityPlugin.AD.Native.NendAdNativeView,System.Boolean>
struct Func_2_t261507339;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.NendAdNative
struct  NendAdNative_t3381739006  : public MonoBehaviour_t3962482529
{
public:
	// NendUnityPlugin.Common.NendUtils/Account NendUnityPlugin.AD.Native.NendAdNative::account
	Account_t2230227361 * ___account_2;
	// System.Boolean NendUnityPlugin.AD.Native.NendAdNative::loadWhenActivated
	bool ___loadWhenActivated_3;
	// System.Boolean NendUnityPlugin.AD.Native.NendAdNative::enableAutoReload
	bool ___enableAutoReload_4;
	// System.Double NendUnityPlugin.AD.Native.NendAdNative::reloadInterval
	double ___reloadInterval_5;
	// NendUnityPlugin.AD.Native.NendAdNativeView[] NendUnityPlugin.AD.Native.NendAdNative::views
	NendAdNativeViewU5BU5D_t3641134489* ___views_6;
	// NendUnityPlugin.AD.Native.Events.NativeAdViewEvent NendUnityPlugin.AD.Native.NendAdNative::AdLoaded
	NativeAdViewEvent_t610464567 * ___AdLoaded_7;
	// NendUnityPlugin.AD.Native.Events.NativeAdViewFailedToLoadEvent NendUnityPlugin.AD.Native.NendAdNative::AdFailedToReceive
	NativeAdViewFailedToLoadEvent_t3307153462 * ___AdFailedToReceive_8;
	// NendUnityPlugin.AD.Native.INativeAdClient NendUnityPlugin.AD.Native.NendAdNative::m_Client
	Il2CppObject * ___m_Client_9;
	// NendUnityPlugin.AD.Native.Utils.SimpleTimer NendUnityPlugin.AD.Native.NendAdNative::m_Timer
	SimpleTimer_t4075384413 * ___m_Timer_10;

public:
	inline static int32_t get_offset_of_account_2() { return static_cast<int32_t>(offsetof(NendAdNative_t3381739006, ___account_2)); }
	inline Account_t2230227361 * get_account_2() const { return ___account_2; }
	inline Account_t2230227361 ** get_address_of_account_2() { return &___account_2; }
	inline void set_account_2(Account_t2230227361 * value)
	{
		___account_2 = value;
		Il2CppCodeGenWriteBarrier(&___account_2, value);
	}

	inline static int32_t get_offset_of_loadWhenActivated_3() { return static_cast<int32_t>(offsetof(NendAdNative_t3381739006, ___loadWhenActivated_3)); }
	inline bool get_loadWhenActivated_3() const { return ___loadWhenActivated_3; }
	inline bool* get_address_of_loadWhenActivated_3() { return &___loadWhenActivated_3; }
	inline void set_loadWhenActivated_3(bool value)
	{
		___loadWhenActivated_3 = value;
	}

	inline static int32_t get_offset_of_enableAutoReload_4() { return static_cast<int32_t>(offsetof(NendAdNative_t3381739006, ___enableAutoReload_4)); }
	inline bool get_enableAutoReload_4() const { return ___enableAutoReload_4; }
	inline bool* get_address_of_enableAutoReload_4() { return &___enableAutoReload_4; }
	inline void set_enableAutoReload_4(bool value)
	{
		___enableAutoReload_4 = value;
	}

	inline static int32_t get_offset_of_reloadInterval_5() { return static_cast<int32_t>(offsetof(NendAdNative_t3381739006, ___reloadInterval_5)); }
	inline double get_reloadInterval_5() const { return ___reloadInterval_5; }
	inline double* get_address_of_reloadInterval_5() { return &___reloadInterval_5; }
	inline void set_reloadInterval_5(double value)
	{
		___reloadInterval_5 = value;
	}

	inline static int32_t get_offset_of_views_6() { return static_cast<int32_t>(offsetof(NendAdNative_t3381739006, ___views_6)); }
	inline NendAdNativeViewU5BU5D_t3641134489* get_views_6() const { return ___views_6; }
	inline NendAdNativeViewU5BU5D_t3641134489** get_address_of_views_6() { return &___views_6; }
	inline void set_views_6(NendAdNativeViewU5BU5D_t3641134489* value)
	{
		___views_6 = value;
		Il2CppCodeGenWriteBarrier(&___views_6, value);
	}

	inline static int32_t get_offset_of_AdLoaded_7() { return static_cast<int32_t>(offsetof(NendAdNative_t3381739006, ___AdLoaded_7)); }
	inline NativeAdViewEvent_t610464567 * get_AdLoaded_7() const { return ___AdLoaded_7; }
	inline NativeAdViewEvent_t610464567 ** get_address_of_AdLoaded_7() { return &___AdLoaded_7; }
	inline void set_AdLoaded_7(NativeAdViewEvent_t610464567 * value)
	{
		___AdLoaded_7 = value;
		Il2CppCodeGenWriteBarrier(&___AdLoaded_7, value);
	}

	inline static int32_t get_offset_of_AdFailedToReceive_8() { return static_cast<int32_t>(offsetof(NendAdNative_t3381739006, ___AdFailedToReceive_8)); }
	inline NativeAdViewFailedToLoadEvent_t3307153462 * get_AdFailedToReceive_8() const { return ___AdFailedToReceive_8; }
	inline NativeAdViewFailedToLoadEvent_t3307153462 ** get_address_of_AdFailedToReceive_8() { return &___AdFailedToReceive_8; }
	inline void set_AdFailedToReceive_8(NativeAdViewFailedToLoadEvent_t3307153462 * value)
	{
		___AdFailedToReceive_8 = value;
		Il2CppCodeGenWriteBarrier(&___AdFailedToReceive_8, value);
	}

	inline static int32_t get_offset_of_m_Client_9() { return static_cast<int32_t>(offsetof(NendAdNative_t3381739006, ___m_Client_9)); }
	inline Il2CppObject * get_m_Client_9() const { return ___m_Client_9; }
	inline Il2CppObject ** get_address_of_m_Client_9() { return &___m_Client_9; }
	inline void set_m_Client_9(Il2CppObject * value)
	{
		___m_Client_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_Client_9, value);
	}

	inline static int32_t get_offset_of_m_Timer_10() { return static_cast<int32_t>(offsetof(NendAdNative_t3381739006, ___m_Timer_10)); }
	inline SimpleTimer_t4075384413 * get_m_Timer_10() const { return ___m_Timer_10; }
	inline SimpleTimer_t4075384413 ** get_address_of_m_Timer_10() { return &___m_Timer_10; }
	inline void set_m_Timer_10(SimpleTimer_t4075384413 * value)
	{
		___m_Timer_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_Timer_10, value);
	}
};

struct NendAdNative_t3381739006_StaticFields
{
public:
	// System.Func`2<NendUnityPlugin.AD.Native.NendAdNativeView,System.Boolean> NendUnityPlugin.AD.Native.NendAdNative::<>f__am$cache0
	Func_2_t261507339 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(NendAdNative_t3381739006_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Func_2_t261507339 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Func_2_t261507339 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Func_2_t261507339 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
