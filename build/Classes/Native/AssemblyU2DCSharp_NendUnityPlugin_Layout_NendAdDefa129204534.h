﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Layout.NendAdDefaultLayoutParams
struct  NendAdDefaultLayoutParams_t129204534  : public Il2CppObject
{
public:
	// System.Int32 NendUnityPlugin.Layout.NendAdDefaultLayoutParams::gravity
	int32_t ___gravity_0;
	// System.Single NendUnityPlugin.Layout.NendAdDefaultLayoutParams::left
	float ___left_1;
	// System.Single NendUnityPlugin.Layout.NendAdDefaultLayoutParams::top
	float ___top_2;
	// System.Single NendUnityPlugin.Layout.NendAdDefaultLayoutParams::right
	float ___right_3;
	// System.Single NendUnityPlugin.Layout.NendAdDefaultLayoutParams::bottom
	float ___bottom_4;

public:
	inline static int32_t get_offset_of_gravity_0() { return static_cast<int32_t>(offsetof(NendAdDefaultLayoutParams_t129204534, ___gravity_0)); }
	inline int32_t get_gravity_0() const { return ___gravity_0; }
	inline int32_t* get_address_of_gravity_0() { return &___gravity_0; }
	inline void set_gravity_0(int32_t value)
	{
		___gravity_0 = value;
	}

	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(NendAdDefaultLayoutParams_t129204534, ___left_1)); }
	inline float get_left_1() const { return ___left_1; }
	inline float* get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(float value)
	{
		___left_1 = value;
	}

	inline static int32_t get_offset_of_top_2() { return static_cast<int32_t>(offsetof(NendAdDefaultLayoutParams_t129204534, ___top_2)); }
	inline float get_top_2() const { return ___top_2; }
	inline float* get_address_of_top_2() { return &___top_2; }
	inline void set_top_2(float value)
	{
		___top_2 = value;
	}

	inline static int32_t get_offset_of_right_3() { return static_cast<int32_t>(offsetof(NendAdDefaultLayoutParams_t129204534, ___right_3)); }
	inline float get_right_3() const { return ___right_3; }
	inline float* get_address_of_right_3() { return &___right_3; }
	inline void set_right_3(float value)
	{
		___right_3 = value;
	}

	inline static int32_t get_offset_of_bottom_4() { return static_cast<int32_t>(offsetof(NendAdDefaultLayoutParams_t129204534, ___bottom_4)); }
	inline float get_bottom_4() const { return ___bottom_4; }
	inline float* get_address_of_bottom_4() { return &___bottom_4; }
	inline void set_bottom_4(float value)
	{
		___bottom_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
