﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Texture2D
struct Texture2D_t3840446185;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SherePlugins
struct  SherePlugins_t4073479905  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D SherePlugins::shareTexture
	Texture2D_t3840446185 * ___shareTexture_2;

public:
	inline static int32_t get_offset_of_shareTexture_2() { return static_cast<int32_t>(offsetof(SherePlugins_t4073479905, ___shareTexture_2)); }
	inline Texture2D_t3840446185 * get_shareTexture_2() const { return ___shareTexture_2; }
	inline Texture2D_t3840446185 ** get_address_of_shareTexture_2() { return &___shareTexture_2; }
	inline void set_shareTexture_2(Texture2D_t3840446185 * value)
	{
		___shareTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___shareTexture_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
