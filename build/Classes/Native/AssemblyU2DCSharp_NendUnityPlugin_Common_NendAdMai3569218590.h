﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t1110636971;
// NendUnityPlugin.Common.NendAdMainThreadWorker
struct NendAdMainThreadWorker_t3569218590;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Common.NendAdMainThreadWorker
struct  NendAdMainThreadWorker_t3569218590  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct NendAdMainThreadWorker_t3569218590_StaticFields
{
public:
	// System.Collections.Generic.Queue`1<System.Action> NendUnityPlugin.Common.NendAdMainThreadWorker::s_ActionQueue
	Queue_1_t1110636971 * ___s_ActionQueue_2;
	// NendUnityPlugin.Common.NendAdMainThreadWorker NendUnityPlugin.Common.NendAdMainThreadWorker::s_Instance
	NendAdMainThreadWorker_t3569218590 * ___s_Instance_3;

public:
	inline static int32_t get_offset_of_s_ActionQueue_2() { return static_cast<int32_t>(offsetof(NendAdMainThreadWorker_t3569218590_StaticFields, ___s_ActionQueue_2)); }
	inline Queue_1_t1110636971 * get_s_ActionQueue_2() const { return ___s_ActionQueue_2; }
	inline Queue_1_t1110636971 ** get_address_of_s_ActionQueue_2() { return &___s_ActionQueue_2; }
	inline void set_s_ActionQueue_2(Queue_1_t1110636971 * value)
	{
		___s_ActionQueue_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_ActionQueue_2, value);
	}

	inline static int32_t get_offset_of_s_Instance_3() { return static_cast<int32_t>(offsetof(NendAdMainThreadWorker_t3569218590_StaticFields, ___s_Instance_3)); }
	inline NendAdMainThreadWorker_t3569218590 * get_s_Instance_3() const { return ___s_Instance_3; }
	inline NendAdMainThreadWorker_t3569218590 ** get_address_of_s_Instance_3() { return &___s_Instance_3; }
	inline void set_s_Instance_3(NendAdMainThreadWorker_t3569218590 * value)
	{
		___s_Instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___s_Instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
