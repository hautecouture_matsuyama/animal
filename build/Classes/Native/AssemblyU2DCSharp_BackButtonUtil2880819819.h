﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharp_BackKeyAction3328090672.h"

// System.String
struct String_t;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// BackButtonUtil
struct BackButtonUtil_t2880819819;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackButtonUtil
struct  BackButtonUtil_t2880819819  : public MonoBehaviour_t3962482529
{
public:
	// BackKeyAction BackButtonUtil::backKeyAction
	int32_t ___backKeyAction_2;
	// System.String BackButtonUtil::nextSceneName
	String_t* ___nextSceneName_3;
	// UnityEngine.Events.UnityEvent BackButtonUtil::onClickBack
	UnityEvent_t2581268647 * ___onClickBack_4;

public:
	inline static int32_t get_offset_of_backKeyAction_2() { return static_cast<int32_t>(offsetof(BackButtonUtil_t2880819819, ___backKeyAction_2)); }
	inline int32_t get_backKeyAction_2() const { return ___backKeyAction_2; }
	inline int32_t* get_address_of_backKeyAction_2() { return &___backKeyAction_2; }
	inline void set_backKeyAction_2(int32_t value)
	{
		___backKeyAction_2 = value;
	}

	inline static int32_t get_offset_of_nextSceneName_3() { return static_cast<int32_t>(offsetof(BackButtonUtil_t2880819819, ___nextSceneName_3)); }
	inline String_t* get_nextSceneName_3() const { return ___nextSceneName_3; }
	inline String_t** get_address_of_nextSceneName_3() { return &___nextSceneName_3; }
	inline void set_nextSceneName_3(String_t* value)
	{
		___nextSceneName_3 = value;
		Il2CppCodeGenWriteBarrier(&___nextSceneName_3, value);
	}

	inline static int32_t get_offset_of_onClickBack_4() { return static_cast<int32_t>(offsetof(BackButtonUtil_t2880819819, ___onClickBack_4)); }
	inline UnityEvent_t2581268647 * get_onClickBack_4() const { return ___onClickBack_4; }
	inline UnityEvent_t2581268647 ** get_address_of_onClickBack_4() { return &___onClickBack_4; }
	inline void set_onClickBack_4(UnityEvent_t2581268647 * value)
	{
		___onClickBack_4 = value;
		Il2CppCodeGenWriteBarrier(&___onClickBack_4, value);
	}
};

struct BackButtonUtil_t2880819819_StaticFields
{
public:
	// BackButtonUtil BackButtonUtil::instance
	BackButtonUtil_t2880819819 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(BackButtonUtil_t2880819819_StaticFields, ___instance_5)); }
	inline BackButtonUtil_t2880819819 * get_instance_5() const { return ___instance_5; }
	inline BackButtonUtil_t2880819819 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(BackButtonUtil_t2880819819 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
