﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.Rect[]
struct RectU5BU5D_t2936723554;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// HCAnimatedBanner
struct HCAnimatedBanner_t2921968851;
// System.Object
struct Il2CppObject;
// HCAnimatedBanner/<AnimateBanner>c__Iterator2/<AnimateBanner>c__AnonStorey3
struct U3CAnimateBannerU3Ec__AnonStorey3_t102123587;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HCAnimatedBanner/<AnimateBanner>c__Iterator2
struct  U3CAnimateBannerU3Ec__Iterator2_t688012631  : public Il2CppObject
{
public:
	// System.Int32 HCAnimatedBanner/<AnimateBanner>c__Iterator2::index
	int32_t ___index_0;
	// System.Int32 HCAnimatedBanner/<AnimateBanner>c__Iterator2::<spriteWidth>__0
	int32_t ___U3CspriteWidthU3E__0_1;
	// System.Int32 HCAnimatedBanner/<AnimateBanner>c__Iterator2::<spriteHeight>__0
	int32_t ___U3CspriteHeightU3E__0_2;
	// System.Int32 HCAnimatedBanner/<AnimateBanner>c__Iterator2::<horCount>__0
	int32_t ___U3ChorCountU3E__0_3;
	// System.Int32 HCAnimatedBanner/<AnimateBanner>c__Iterator2::<verCount>__0
	int32_t ___U3CverCountU3E__0_4;
	// System.Int32 HCAnimatedBanner/<AnimateBanner>c__Iterator2::<spriteCount>__0
	int32_t ___U3CspriteCountU3E__0_5;
	// UnityEngine.Rect[] HCAnimatedBanner/<AnimateBanner>c__Iterator2::<spriteRects>__0
	RectU5BU5D_t2936723554* ___U3CspriteRectsU3E__0_6;
	// UnityEngine.Rect[] HCAnimatedBanner/<AnimateBanner>c__Iterator2::<uvRects>__0
	RectU5BU5D_t2936723554* ___U3CuvRectsU3E__0_7;
	// UnityEngine.UI.RawImage HCAnimatedBanner/<AnimateBanner>c__Iterator2::<rImage>__0
	RawImage_t3182918964 * ___U3CrImageU3E__0_8;
	// System.Int32 HCAnimatedBanner/<AnimateBanner>c__Iterator2::<rIndex>__0
	int32_t ___U3CrIndexU3E__0_9;
	// System.Single HCAnimatedBanner/<AnimateBanner>c__Iterator2::<wait>__0
	float ___U3CwaitU3E__0_10;
	// HCAnimatedBanner HCAnimatedBanner/<AnimateBanner>c__Iterator2::$this
	HCAnimatedBanner_t2921968851 * ___U24this_11;
	// System.Object HCAnimatedBanner/<AnimateBanner>c__Iterator2::$current
	Il2CppObject * ___U24current_12;
	// System.Boolean HCAnimatedBanner/<AnimateBanner>c__Iterator2::$disposing
	bool ___U24disposing_13;
	// System.Int32 HCAnimatedBanner/<AnimateBanner>c__Iterator2::$PC
	int32_t ___U24PC_14;
	// HCAnimatedBanner/<AnimateBanner>c__Iterator2/<AnimateBanner>c__AnonStorey3 HCAnimatedBanner/<AnimateBanner>c__Iterator2::$locvar0
	U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * ___U24locvar0_15;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U3CspriteWidthU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U3CspriteWidthU3E__0_1)); }
	inline int32_t get_U3CspriteWidthU3E__0_1() const { return ___U3CspriteWidthU3E__0_1; }
	inline int32_t* get_address_of_U3CspriteWidthU3E__0_1() { return &___U3CspriteWidthU3E__0_1; }
	inline void set_U3CspriteWidthU3E__0_1(int32_t value)
	{
		___U3CspriteWidthU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CspriteHeightU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U3CspriteHeightU3E__0_2)); }
	inline int32_t get_U3CspriteHeightU3E__0_2() const { return ___U3CspriteHeightU3E__0_2; }
	inline int32_t* get_address_of_U3CspriteHeightU3E__0_2() { return &___U3CspriteHeightU3E__0_2; }
	inline void set_U3CspriteHeightU3E__0_2(int32_t value)
	{
		___U3CspriteHeightU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3ChorCountU3E__0_3() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U3ChorCountU3E__0_3)); }
	inline int32_t get_U3ChorCountU3E__0_3() const { return ___U3ChorCountU3E__0_3; }
	inline int32_t* get_address_of_U3ChorCountU3E__0_3() { return &___U3ChorCountU3E__0_3; }
	inline void set_U3ChorCountU3E__0_3(int32_t value)
	{
		___U3ChorCountU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CverCountU3E__0_4() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U3CverCountU3E__0_4)); }
	inline int32_t get_U3CverCountU3E__0_4() const { return ___U3CverCountU3E__0_4; }
	inline int32_t* get_address_of_U3CverCountU3E__0_4() { return &___U3CverCountU3E__0_4; }
	inline void set_U3CverCountU3E__0_4(int32_t value)
	{
		___U3CverCountU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CspriteCountU3E__0_5() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U3CspriteCountU3E__0_5)); }
	inline int32_t get_U3CspriteCountU3E__0_5() const { return ___U3CspriteCountU3E__0_5; }
	inline int32_t* get_address_of_U3CspriteCountU3E__0_5() { return &___U3CspriteCountU3E__0_5; }
	inline void set_U3CspriteCountU3E__0_5(int32_t value)
	{
		___U3CspriteCountU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CspriteRectsU3E__0_6() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U3CspriteRectsU3E__0_6)); }
	inline RectU5BU5D_t2936723554* get_U3CspriteRectsU3E__0_6() const { return ___U3CspriteRectsU3E__0_6; }
	inline RectU5BU5D_t2936723554** get_address_of_U3CspriteRectsU3E__0_6() { return &___U3CspriteRectsU3E__0_6; }
	inline void set_U3CspriteRectsU3E__0_6(RectU5BU5D_t2936723554* value)
	{
		___U3CspriteRectsU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CspriteRectsU3E__0_6, value);
	}

	inline static int32_t get_offset_of_U3CuvRectsU3E__0_7() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U3CuvRectsU3E__0_7)); }
	inline RectU5BU5D_t2936723554* get_U3CuvRectsU3E__0_7() const { return ___U3CuvRectsU3E__0_7; }
	inline RectU5BU5D_t2936723554** get_address_of_U3CuvRectsU3E__0_7() { return &___U3CuvRectsU3E__0_7; }
	inline void set_U3CuvRectsU3E__0_7(RectU5BU5D_t2936723554* value)
	{
		___U3CuvRectsU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuvRectsU3E__0_7, value);
	}

	inline static int32_t get_offset_of_U3CrImageU3E__0_8() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U3CrImageU3E__0_8)); }
	inline RawImage_t3182918964 * get_U3CrImageU3E__0_8() const { return ___U3CrImageU3E__0_8; }
	inline RawImage_t3182918964 ** get_address_of_U3CrImageU3E__0_8() { return &___U3CrImageU3E__0_8; }
	inline void set_U3CrImageU3E__0_8(RawImage_t3182918964 * value)
	{
		___U3CrImageU3E__0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrImageU3E__0_8, value);
	}

	inline static int32_t get_offset_of_U3CrIndexU3E__0_9() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U3CrIndexU3E__0_9)); }
	inline int32_t get_U3CrIndexU3E__0_9() const { return ___U3CrIndexU3E__0_9; }
	inline int32_t* get_address_of_U3CrIndexU3E__0_9() { return &___U3CrIndexU3E__0_9; }
	inline void set_U3CrIndexU3E__0_9(int32_t value)
	{
		___U3CrIndexU3E__0_9 = value;
	}

	inline static int32_t get_offset_of_U3CwaitU3E__0_10() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U3CwaitU3E__0_10)); }
	inline float get_U3CwaitU3E__0_10() const { return ___U3CwaitU3E__0_10; }
	inline float* get_address_of_U3CwaitU3E__0_10() { return &___U3CwaitU3E__0_10; }
	inline void set_U3CwaitU3E__0_10(float value)
	{
		___U3CwaitU3E__0_10 = value;
	}

	inline static int32_t get_offset_of_U24this_11() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U24this_11)); }
	inline HCAnimatedBanner_t2921968851 * get_U24this_11() const { return ___U24this_11; }
	inline HCAnimatedBanner_t2921968851 ** get_address_of_U24this_11() { return &___U24this_11; }
	inline void set_U24this_11(HCAnimatedBanner_t2921968851 * value)
	{
		___U24this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_11, value);
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U24current_12)); }
	inline Il2CppObject * get_U24current_12() const { return ___U24current_12; }
	inline Il2CppObject ** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(Il2CppObject * value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_12, value);
	}

	inline static int32_t get_offset_of_U24disposing_13() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U24disposing_13)); }
	inline bool get_U24disposing_13() const { return ___U24disposing_13; }
	inline bool* get_address_of_U24disposing_13() { return &___U24disposing_13; }
	inline void set_U24disposing_13(bool value)
	{
		___U24disposing_13 = value;
	}

	inline static int32_t get_offset_of_U24PC_14() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U24PC_14)); }
	inline int32_t get_U24PC_14() const { return ___U24PC_14; }
	inline int32_t* get_address_of_U24PC_14() { return &___U24PC_14; }
	inline void set_U24PC_14(int32_t value)
	{
		___U24PC_14 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_15() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__Iterator2_t688012631, ___U24locvar0_15)); }
	inline U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * get_U24locvar0_15() const { return ___U24locvar0_15; }
	inline U3CAnimateBannerU3Ec__AnonStorey3_t102123587 ** get_address_of_U24locvar0_15() { return &___U24locvar0_15; }
	inline void set_U24locvar0_15(U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * value)
	{
		___U24locvar0_15 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
