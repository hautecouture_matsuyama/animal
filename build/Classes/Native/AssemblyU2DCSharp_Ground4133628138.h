﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ground
struct  Ground_t4133628138  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Ground::tilePrefab
	GameObject_t1113636619 * ___tilePrefab_2;
	// System.Single Ground::bottomY
	float ___bottomY_3;
	// System.Single Ground::leftX
	float ___leftX_4;
	// System.Single Ground::topY
	float ___topY_5;
	// System.Single Ground::rightX
	float ___rightX_6;
	// System.Single Ground::groundTileW
	float ___groundTileW_7;
	// System.Single Ground::groundTileH
	float ___groundTileH_8;
	// UnityEngine.GameObject Ground::block1
	GameObject_t1113636619 * ___block1_9;
	// UnityEngine.GameObject Ground::block2
	GameObject_t1113636619 * ___block2_10;

public:
	inline static int32_t get_offset_of_tilePrefab_2() { return static_cast<int32_t>(offsetof(Ground_t4133628138, ___tilePrefab_2)); }
	inline GameObject_t1113636619 * get_tilePrefab_2() const { return ___tilePrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_tilePrefab_2() { return &___tilePrefab_2; }
	inline void set_tilePrefab_2(GameObject_t1113636619 * value)
	{
		___tilePrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___tilePrefab_2, value);
	}

	inline static int32_t get_offset_of_bottomY_3() { return static_cast<int32_t>(offsetof(Ground_t4133628138, ___bottomY_3)); }
	inline float get_bottomY_3() const { return ___bottomY_3; }
	inline float* get_address_of_bottomY_3() { return &___bottomY_3; }
	inline void set_bottomY_3(float value)
	{
		___bottomY_3 = value;
	}

	inline static int32_t get_offset_of_leftX_4() { return static_cast<int32_t>(offsetof(Ground_t4133628138, ___leftX_4)); }
	inline float get_leftX_4() const { return ___leftX_4; }
	inline float* get_address_of_leftX_4() { return &___leftX_4; }
	inline void set_leftX_4(float value)
	{
		___leftX_4 = value;
	}

	inline static int32_t get_offset_of_topY_5() { return static_cast<int32_t>(offsetof(Ground_t4133628138, ___topY_5)); }
	inline float get_topY_5() const { return ___topY_5; }
	inline float* get_address_of_topY_5() { return &___topY_5; }
	inline void set_topY_5(float value)
	{
		___topY_5 = value;
	}

	inline static int32_t get_offset_of_rightX_6() { return static_cast<int32_t>(offsetof(Ground_t4133628138, ___rightX_6)); }
	inline float get_rightX_6() const { return ___rightX_6; }
	inline float* get_address_of_rightX_6() { return &___rightX_6; }
	inline void set_rightX_6(float value)
	{
		___rightX_6 = value;
	}

	inline static int32_t get_offset_of_groundTileW_7() { return static_cast<int32_t>(offsetof(Ground_t4133628138, ___groundTileW_7)); }
	inline float get_groundTileW_7() const { return ___groundTileW_7; }
	inline float* get_address_of_groundTileW_7() { return &___groundTileW_7; }
	inline void set_groundTileW_7(float value)
	{
		___groundTileW_7 = value;
	}

	inline static int32_t get_offset_of_groundTileH_8() { return static_cast<int32_t>(offsetof(Ground_t4133628138, ___groundTileH_8)); }
	inline float get_groundTileH_8() const { return ___groundTileH_8; }
	inline float* get_address_of_groundTileH_8() { return &___groundTileH_8; }
	inline void set_groundTileH_8(float value)
	{
		___groundTileH_8 = value;
	}

	inline static int32_t get_offset_of_block1_9() { return static_cast<int32_t>(offsetof(Ground_t4133628138, ___block1_9)); }
	inline GameObject_t1113636619 * get_block1_9() const { return ___block1_9; }
	inline GameObject_t1113636619 ** get_address_of_block1_9() { return &___block1_9; }
	inline void set_block1_9(GameObject_t1113636619 * value)
	{
		___block1_9 = value;
		Il2CppCodeGenWriteBarrier(&___block1_9, value);
	}

	inline static int32_t get_offset_of_block2_10() { return static_cast<int32_t>(offsetof(Ground_t4133628138, ___block2_10)); }
	inline GameObject_t1113636619 * get_block2_10() const { return ___block2_10; }
	inline GameObject_t1113636619 ** get_address_of_block2_10() { return &___block2_10; }
	inline void set_block2_10(GameObject_t1113636619 * value)
	{
		___block2_10 = value;
		Il2CppCodeGenWriteBarrier(&___block2_10, value);
	}
};

struct Ground_t4133628138_StaticFields
{
public:
	// System.Single Ground::posY
	float ___posY_11;
	// System.Single Ground::groundH
	float ___groundH_12;

public:
	inline static int32_t get_offset_of_posY_11() { return static_cast<int32_t>(offsetof(Ground_t4133628138_StaticFields, ___posY_11)); }
	inline float get_posY_11() const { return ___posY_11; }
	inline float* get_address_of_posY_11() { return &___posY_11; }
	inline void set_posY_11(float value)
	{
		___posY_11 = value;
	}

	inline static int32_t get_offset_of_groundH_12() { return static_cast<int32_t>(offsetof(Ground_t4133628138_StaticFields, ___groundH_12)); }
	inline float get_groundH_12() const { return ___groundH_12; }
	inline float* get_address_of_groundH_12() { return &___groundH_12; }
	inline void set_groundH_12(float value)
	{
		___groundH_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
