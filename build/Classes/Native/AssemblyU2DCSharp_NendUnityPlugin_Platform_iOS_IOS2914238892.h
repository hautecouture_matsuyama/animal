﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdR3161671531.h"
#include "mscorlib_System_IntPtr840150181.h"

// NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd/NendUnityVideoAdNormalCallback
struct NendUnityVideoAdNormalCallback_t3134600846;
// NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd/NendUnityVideoAdErrorCallback
struct NendUnityVideoAdErrorCallback_t418341462;
// NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd/NendUnityRewardedVideoAdCallback
struct NendUnityRewardedVideoAdCallback_t1320073170;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd
struct  IOSRewardedVideoAd_t2914238892  : public NendAdRewardedVideo_t3161671531
{
public:
	// System.IntPtr NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd::m_iOSRewardedVideoAdPtr
	IntPtr_t ___m_iOSRewardedVideoAdPtr_11;
	// System.IntPtr NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd::m_selfPtr
	IntPtr_t ___m_selfPtr_12;

public:
	inline static int32_t get_offset_of_m_iOSRewardedVideoAdPtr_11() { return static_cast<int32_t>(offsetof(IOSRewardedVideoAd_t2914238892, ___m_iOSRewardedVideoAdPtr_11)); }
	inline IntPtr_t get_m_iOSRewardedVideoAdPtr_11() const { return ___m_iOSRewardedVideoAdPtr_11; }
	inline IntPtr_t* get_address_of_m_iOSRewardedVideoAdPtr_11() { return &___m_iOSRewardedVideoAdPtr_11; }
	inline void set_m_iOSRewardedVideoAdPtr_11(IntPtr_t value)
	{
		___m_iOSRewardedVideoAdPtr_11 = value;
	}

	inline static int32_t get_offset_of_m_selfPtr_12() { return static_cast<int32_t>(offsetof(IOSRewardedVideoAd_t2914238892, ___m_selfPtr_12)); }
	inline IntPtr_t get_m_selfPtr_12() const { return ___m_selfPtr_12; }
	inline IntPtr_t* get_address_of_m_selfPtr_12() { return &___m_selfPtr_12; }
	inline void set_m_selfPtr_12(IntPtr_t value)
	{
		___m_selfPtr_12 = value;
	}
};

struct IOSRewardedVideoAd_t2914238892_StaticFields
{
public:
	// NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd/NendUnityVideoAdNormalCallback NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd::<>f__mg$cache0
	NendUnityVideoAdNormalCallback_t3134600846 * ___U3CU3Ef__mgU24cache0_13;
	// NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd/NendUnityVideoAdErrorCallback NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd::<>f__mg$cache1
	NendUnityVideoAdErrorCallback_t418341462 * ___U3CU3Ef__mgU24cache1_14;
	// NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd/NendUnityRewardedVideoAdCallback NendUnityPlugin.Platform.iOS.IOSRewardedVideoAd::<>f__mg$cache2
	NendUnityRewardedVideoAdCallback_t1320073170 * ___U3CU3Ef__mgU24cache2_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(IOSRewardedVideoAd_t2914238892_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline NendUnityVideoAdNormalCallback_t3134600846 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline NendUnityVideoAdNormalCallback_t3134600846 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(NendUnityVideoAdNormalCallback_t3134600846 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_14() { return static_cast<int32_t>(offsetof(IOSRewardedVideoAd_t2914238892_StaticFields, ___U3CU3Ef__mgU24cache1_14)); }
	inline NendUnityVideoAdErrorCallback_t418341462 * get_U3CU3Ef__mgU24cache1_14() const { return ___U3CU3Ef__mgU24cache1_14; }
	inline NendUnityVideoAdErrorCallback_t418341462 ** get_address_of_U3CU3Ef__mgU24cache1_14() { return &___U3CU3Ef__mgU24cache1_14; }
	inline void set_U3CU3Ef__mgU24cache1_14(NendUnityVideoAdErrorCallback_t418341462 * value)
	{
		___U3CU3Ef__mgU24cache1_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_15() { return static_cast<int32_t>(offsetof(IOSRewardedVideoAd_t2914238892_StaticFields, ___U3CU3Ef__mgU24cache2_15)); }
	inline NendUnityRewardedVideoAdCallback_t1320073170 * get_U3CU3Ef__mgU24cache2_15() const { return ___U3CU3Ef__mgU24cache2_15; }
	inline NendUnityRewardedVideoAdCallback_t1320073170 ** get_address_of_U3CU3Ef__mgU24cache2_15() { return &___U3CU3Ef__mgU24cache2_15; }
	inline void set_U3CU3Ef__mgU24cache2_15(NendUnityRewardedVideoAdCallback_t1320073170 * value)
	{
		___U3CU3Ef__mgU24cache2_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache2_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
