﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// NendUnityPlugin.AD.Native.INativeAd
struct INativeAd_t3252869353;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey1
struct  U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185  : public Il2CppObject
{
public:
	// NendUnityPlugin.AD.Native.INativeAd NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey1::ad
	Il2CppObject * ___ad_0;
	// System.Int32 NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey1::code
	int32_t ___code_1;
	// System.String NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey1::message
	String_t* ___message_2;

public:
	inline static int32_t get_offset_of_ad_0() { return static_cast<int32_t>(offsetof(U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185, ___ad_0)); }
	inline Il2CppObject * get_ad_0() const { return ___ad_0; }
	inline Il2CppObject ** get_address_of_ad_0() { return &___ad_0; }
	inline void set_ad_0(Il2CppObject * value)
	{
		___ad_0 = value;
		Il2CppCodeGenWriteBarrier(&___ad_0, value);
	}

	inline static int32_t get_offset_of_code_1() { return static_cast<int32_t>(offsetof(U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185, ___code_1)); }
	inline int32_t get_code_1() const { return ___code_1; }
	inline int32_t* get_address_of_code_1() { return &___code_1; }
	inline void set_code_1(int32_t value)
	{
		___code_1 = value;
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier(&___message_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
