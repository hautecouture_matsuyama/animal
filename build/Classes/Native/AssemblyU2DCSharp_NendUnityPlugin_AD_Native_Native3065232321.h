﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>
struct Action_3_t567914338;
// NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey1
struct U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey0
struct  U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321  : public Il2CppObject
{
public:
	// System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String> NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey0::callback
	Action_3_t567914338 * ___callback_0;
	// NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey1 NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey0::<>f__ref$1
	U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321, ___callback_0)); }
	inline Action_3_t567914338 * get_callback_0() const { return ___callback_0; }
	inline Action_3_t567914338 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_3_t567914338 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321, ___U3CU3Ef__refU241_1)); }
	inline U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
