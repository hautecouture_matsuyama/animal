﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t4012913780;
// UnityEngine.WWW
struct WWW_t3688466362;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0
struct  U3CLoadTextureU3Ec__Iterator0_t2469200630  : public Il2CppObject
{
public:
	// System.String NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::url
	String_t* ___url_0;
	// UnityEngine.Texture2D NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::<texture>__0
	Texture2D_t3840446185 * ___U3CtextureU3E__0_1;
	// System.Action`1<UnityEngine.Texture2D> NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::callback
	Action_1_t4012913780 * ___callback_2;
	// UnityEngine.WWW NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::<www>__1
	WWW_t3688466362 * ___U3CwwwU3E__1_3;
	// System.Object NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator0_t2469200630, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_U3CtextureU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator0_t2469200630, ___U3CtextureU3E__0_1)); }
	inline Texture2D_t3840446185 * get_U3CtextureU3E__0_1() const { return ___U3CtextureU3E__0_1; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtextureU3E__0_1() { return &___U3CtextureU3E__0_1; }
	inline void set_U3CtextureU3E__0_1(Texture2D_t3840446185 * value)
	{
		___U3CtextureU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtextureU3E__0_1, value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator0_t2469200630, ___callback_2)); }
	inline Action_1_t4012913780 * get_callback_2() const { return ___callback_2; }
	inline Action_1_t4012913780 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_1_t4012913780 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_3() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator0_t2469200630, ___U3CwwwU3E__1_3)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__1_3() const { return ___U3CwwwU3E__1_3; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__1_3() { return &___U3CwwwU3E__1_3; }
	inline void set_U3CwwwU3E__1_3(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator0_t2469200630, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator0_t2469200630, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator0_t2469200630, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
