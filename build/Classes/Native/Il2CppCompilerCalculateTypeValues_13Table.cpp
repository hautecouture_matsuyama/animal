﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2108887433.h"
#include "UnityEngine_UnityEngine_Time2420636075.h"
#include "UnityEngine_UnityEngine_HideFlags4250555765.h"
#include "UnityEngine_UnityEngine_Object631007953.h"
#include "UnityEngine_UnityEngine_WWW3688466362.h"
#include "UnityEngine_UnityEngine_YieldInstruction403091072.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3531467704.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Playa882318307.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3858037524.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Scri1800649443.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier3366210299.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit1546283851.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification1697500732.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification818046696.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene2348375561.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3251202195.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2787271929.h"
#include "UnityEngine_UnityEngine_Experimental_Rendering_Scri274343796.h"
#include "UnityEngine_UnityEngine_Transform3600365921.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator3442430665.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties3813433528.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker2562230146.h"
#include "UnityEngine_UnityEngine_RectTransform3704657025.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive1258266594.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1530570602.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1856666072.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit240592346.h"
#include "UnityEngine_UnityEngine_Collision4262080450.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction962663221.h"
#include "UnityEngine_UnityEngine_Physics2310948930.h"
#include "UnityEngine_UnityEngine_ContactPoint3758755253.h"
#include "UnityEngine_UnityEngine_Rigidbody3916780224.h"
#include "UnityEngine_UnityEngine_Collider1773347010.h"
#include "UnityEngine_UnityEngine_RaycastHit1056001966.h"
#include "UnityEngine_UnityEngine_CharacterController1138636865.h"
#include "UnityEngine_UnityEngine_RaycastHit2D2279581989.h"
#include "UnityEngine_UnityEngine_Physics2D1528932956.h"
#include "UnityEngine_UnityEngine_ForceMode2D255358695.h"
#include "UnityEngine_UnityEngine_ContactFilter2D3805203441.h"
#include "UnityEngine_UnityEngine_Rigidbody2D939494601.h"
#include "UnityEngine_UnityEngine_Collider2D2806799626.h"
#include "UnityEngine_UnityEngine_BoxCollider2D3581341831.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3390240644.h"
#include "UnityEngine_UnityEngine_Collision2D2842956331.h"
#include "UnityEngine_UnityEngine_AudioSettings3587374600.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu2089929874.h"
#include "UnityEngine_UnityEngine_AudioClip3680889665.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac1677636661.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCa1059417452.h"
#include "UnityEngine_UnityEngine_AudioRolloffMode832723327.h"
#include "UnityEngine_UnityEngine_AudioSource3935305588.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr2857104338.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour957311111.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3045280095.h"
#include "UnityEngine_UnityEngine_AnimationEvent1536042487.h"
#include "UnityEngine_UnityEngine_AnimationClip2318505987.h"
#include "UnityEngine_UnityEngine_AnimationState1108360407.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete3317225440.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3156717155.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo509032636.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2534804151.h"
#include "UnityEngine_UnityEngine_Animator434523843.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete1758260042.h"
#include "UnityEngine_UnityEngine_SkeletonBone4134054672.h"
#include "UnityEngine_UnityEngine_HumanLimit2901552972.h"
#include "UnityEngine_UnityEngine_HumanBone2465339518.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController2933699135.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1397024487.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2837525843.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1193230317.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2591841208.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2436992687.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim3136944237.h"
#include "UnityEngine_UnityEngine_Motion1110556653.h"
#include "UnityEngine_UnityEngine_FontStyle82229486.h"
#include "UnityEngine_UnityEngine_TextGenerationError3604799999.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings1351628751.h"
#include "UnityEngine_UnityEngine_TextGenerator3211863866.h"
#include "UnityEngine_UnityEngine_TextAnchor2035777396.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2172737147.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode2936607737.h"
#include "UnityEngine_UnityEngine_GUIText402233326.h"
#include "UnityEngine_UnityEngine_Font1956802104.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCal2467502454.h"
#include "UnityEngine_UnityEngine_UICharInfo75501106.h"
#include "UnityEngine_UnityEngine_UILineInfo4195266810.h"
#include "UnityEngine_UnityEngine_UIVertex4057497605.h"
#include "UnityEngine_UnityEngine_RectTransformUtility1743242446.h"
#include "UnityEngine_UnityEngine_RenderMode4077056833.h"
#include "UnityEngine_UnityEngine_Canvas3310196443.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases3309123499.h"
#include "UnityEngine_UnityEngine_CanvasGroup4083511760.h"
#include "UnityEngine_UnityEngine_CanvasRenderer2598313366.h"
#include "UnityEngine_UnityEngine_Event2956885303.h"
#include "UnityEngine_UnityEngine_EventType3528516131.h"
#include "UnityEngine_UnityEngine_EventModifiers2016417398.h"
#include "UnityEngine_UnityEngine_GUI1624858472.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3146511083.h"
#include "UnityEngine_UnityEngine_GUIContent3050628031.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { sizeof (RenderTexture_t2108887433), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (Time_t2420636075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (HideFlags_t4250555765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1302[10] = 
{
	HideFlags_t4250555765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (Object_t631007953), sizeof(Object_t631007953_marshaled_pinvoke), sizeof(Object_t631007953_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1303[2] = 
{
	Object_t631007953::get_offset_of_m_CachedPtr_0(),
	Object_t631007953_StaticFields::get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (WWW_t3688466362), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (YieldInstruction_t403091072), sizeof(YieldInstruction_t403091072_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (PlayState_t3531467704)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1306[3] = 
{
	PlayState_t3531467704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (PlayableHandle_t882318307)+ sizeof (Il2CppObject), sizeof(PlayableHandle_t882318307 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1307[2] = 
{
	PlayableHandle_t882318307::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayableHandle_t882318307::get_offset_of_m_Version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (Playable_t3858037524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1308[1] = 
{
	Playable_t3858037524::get_offset_of_handle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (ScriptPlayable_t1800649443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (CalendarIdentifier_t3366210299)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1310[12] = 
{
	CalendarIdentifier_t3366210299::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (CalendarUnit_t1546283851)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1311[12] = 
{
	CalendarUnit_t1546283851::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (LocalNotification_t1697500732), -1, sizeof(LocalNotification_t1697500732_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1312[2] = 
{
	LocalNotification_t1697500732::get_offset_of_notificationWrapper_0(),
	LocalNotification_t1697500732_StaticFields::get_offset_of_m_NSReferenceDateTicks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (RemoteNotification_t818046696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1313[1] = 
{
	RemoteNotification_t818046696::get_offset_of_notificationWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (Scene_t2348375561)+ sizeof (Il2CppObject), sizeof(Scene_t2348375561 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1314[1] = 
{
	Scene_t2348375561::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (LoadSceneMode_t3251202195)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1315[3] = 
{
	LoadSceneMode_t3251202195::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { sizeof (SceneManager_t2787271929), -1, sizeof(SceneManager_t2787271929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1316[3] = 
{
	SceneManager_t2787271929_StaticFields::get_offset_of_sceneLoaded_0(),
	SceneManager_t2787271929_StaticFields::get_offset_of_sceneUnloaded_1(),
	SceneManager_t2787271929_StaticFields::get_offset_of_activeSceneChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { sizeof (ScriptableRenderContext_t274343796)+ sizeof (Il2CppObject), sizeof(ScriptableRenderContext_t274343796 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1317[1] = 
{
	ScriptableRenderContext_t274343796::get_offset_of_m_Ptr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (Transform_t3600365921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (Enumerator_t3442430665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1319[2] = 
{
	Enumerator_t3442430665::get_offset_of_outer_0(),
	Enumerator_t3442430665::get_offset_of_currentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (DrivenTransformProperties_t3813433528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1320[26] = 
{
	DrivenTransformProperties_t3813433528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (DrivenRectTransformTracker_t2562230146)+ sizeof (Il2CppObject), sizeof(DrivenRectTransformTracker_t2562230146 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (RectTransform_t3704657025), -1, sizeof(RectTransform_t3704657025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1322[1] = 
{
	RectTransform_t3704657025_StaticFields::get_offset_of_reapplyDrivenProperties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (ReapplyDrivenProperties_t1258266594), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (Edge_t1530570602)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1324[5] = 
{
	Edge_t1530570602::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (Axis_t1856666072)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1325[3] = 
{
	Axis_t1856666072::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (ControllerColliderHit_t240592346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1326[7] = 
{
	ControllerColliderHit_t240592346::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t240592346::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t240592346::get_offset_of_m_Point_2(),
	ControllerColliderHit_t240592346::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t240592346::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t240592346::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t240592346::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (Collision_t4262080450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1327[5] = 
{
	Collision_t4262080450::get_offset_of_m_Impulse_0(),
	Collision_t4262080450::get_offset_of_m_RelativeVelocity_1(),
	Collision_t4262080450::get_offset_of_m_Rigidbody_2(),
	Collision_t4262080450::get_offset_of_m_Collider_3(),
	Collision_t4262080450::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (QueryTriggerInteraction_t962663221)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1328[4] = 
{
	QueryTriggerInteraction_t962663221::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (Physics_t2310948930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (ContactPoint_t3758755253)+ sizeof (Il2CppObject), sizeof(ContactPoint_t3758755253 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1330[5] = 
{
	ContactPoint_t3758755253::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_ThisColliderInstanceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_OtherColliderInstanceID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_Separation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (Rigidbody_t3916780224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (Collider_t1773347010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (RaycastHit_t1056001966)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1333[6] = 
{
	RaycastHit_t1056001966::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { sizeof (CharacterController_t1138636865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { sizeof (RaycastHit2D_t2279581989)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1335[6] = 
{
	RaycastHit2D_t2279581989::get_offset_of_m_Centroid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Point_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Normal_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Fraction_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { sizeof (Physics2D_t1528932956), -1, sizeof(Physics2D_t1528932956_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1336[1] = 
{
	Physics2D_t1528932956_StaticFields::get_offset_of_m_LastDisabledRigidbody2D_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (ForceMode2D_t255358695)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1337[3] = 
{
	ForceMode2D_t255358695::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (ContactFilter2D_t3805203441)+ sizeof (Il2CppObject), sizeof(ContactFilter2D_t3805203441_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1338[11] = 
{
	ContactFilter2D_t3805203441::get_offset_of_useTriggers_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useLayerMask_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useDepth_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useOutsideDepth_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useNormalAngle_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useOutsideNormalAngle_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_layerMask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_minDepth_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_maxDepth_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_minNormalAngle_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_maxNormalAngle_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (Rigidbody2D_t939494601), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (Collider2D_t2806799626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (BoxCollider2D_t3581341831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (ContactPoint2D_t3390240644)+ sizeof (Il2CppObject), sizeof(ContactPoint2D_t3390240644 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1342[11] = 
{
	ContactPoint2D_t3390240644::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_RelativeVelocity_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Separation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_NormalImpulse_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_TangentImpulse_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Collider_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_OtherCollider_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Rigidbody_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_OtherRigidbody_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Enabled_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (Collision2D_t2842956331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1343[7] = 
{
	Collision2D_t2842956331::get_offset_of_m_Collider_0(),
	Collision2D_t2842956331::get_offset_of_m_OtherCollider_1(),
	Collision2D_t2842956331::get_offset_of_m_Rigidbody_2(),
	Collision2D_t2842956331::get_offset_of_m_OtherRigidbody_3(),
	Collision2D_t2842956331::get_offset_of_m_Contacts_4(),
	Collision2D_t2842956331::get_offset_of_m_RelativeVelocity_5(),
	Collision2D_t2842956331::get_offset_of_m_Enabled_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (AudioSettings_t3587374600), -1, sizeof(AudioSettings_t3587374600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1344[1] = 
{
	AudioSettings_t3587374600_StaticFields::get_offset_of_OnAudioConfigurationChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (AudioConfigurationChangeHandler_t2089929874), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (AudioClip_t3680889665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1346[2] = 
{
	AudioClip_t3680889665::get_offset_of_m_PCMReaderCallback_2(),
	AudioClip_t3680889665::get_offset_of_m_PCMSetPositionCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (PCMReaderCallback_t1677636661), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { sizeof (PCMSetPositionCallback_t1059417452), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (AudioRolloffMode_t832723327)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1349[4] = 
{
	AudioRolloffMode_t832723327::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (AudioSource_t3935305588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (SharedBetweenAnimatorsAttribute_t2857104338), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (StateMachineBehaviour_t957311111), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (AnimationEventSource_t3045280095)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1353[4] = 
{
	AnimationEventSource_t3045280095::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (AnimationEvent_t1536042487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1354[11] = 
{
	AnimationEvent_t1536042487::get_offset_of_m_Time_0(),
	AnimationEvent_t1536042487::get_offset_of_m_FunctionName_1(),
	AnimationEvent_t1536042487::get_offset_of_m_StringParameter_2(),
	AnimationEvent_t1536042487::get_offset_of_m_ObjectReferenceParameter_3(),
	AnimationEvent_t1536042487::get_offset_of_m_FloatParameter_4(),
	AnimationEvent_t1536042487::get_offset_of_m_IntParameter_5(),
	AnimationEvent_t1536042487::get_offset_of_m_MessageOptions_6(),
	AnimationEvent_t1536042487::get_offset_of_m_Source_7(),
	AnimationEvent_t1536042487::get_offset_of_m_StateSender_8(),
	AnimationEvent_t1536042487::get_offset_of_m_AnimatorStateInfo_9(),
	AnimationEvent_t1536042487::get_offset_of_m_AnimatorClipInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (AnimationClip_t2318505987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (AnimationState_t1108360407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (AnimatorControllerParameterType_t3317225440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1357[5] = 
{
	AnimatorControllerParameterType_t3317225440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (AnimatorClipInfo_t3156717155)+ sizeof (Il2CppObject), sizeof(AnimatorClipInfo_t3156717155 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1358[2] = 
{
	AnimatorClipInfo_t3156717155::get_offset_of_m_ClipInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorClipInfo_t3156717155::get_offset_of_m_Weight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (AnimatorStateInfo_t509032636)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t509032636 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1359[9] = 
{
	AnimatorStateInfo_t509032636::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Path_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_FullPath_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Length_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Speed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_SpeedMultiplier_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Tag_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Loop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (AnimatorTransitionInfo_t2534804151)+ sizeof (Il2CppObject), sizeof(AnimatorTransitionInfo_t2534804151_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1360[6] = 
{
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_FullPath_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_UserName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_AnyState_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_TransitionType_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (Animator_t434523843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (AnimatorControllerParameter_t1758260042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1362[5] = 
{
	AnimatorControllerParameter_t1758260042::get_offset_of_m_Name_0(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_Type_1(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_DefaultFloat_2(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_DefaultInt_3(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_DefaultBool_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (SkeletonBone_t4134054672)+ sizeof (Il2CppObject), sizeof(SkeletonBone_t4134054672_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1363[5] = 
{
	SkeletonBone_t4134054672::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_parentName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_position_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_rotation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_scale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (HumanLimit_t2901552972)+ sizeof (Il2CppObject), sizeof(HumanLimit_t2901552972 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1364[5] = 
{
	HumanLimit_t2901552972::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_Center_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_AxisLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_UseDefaultValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (HumanBone_t2465339518)+ sizeof (Il2CppObject), sizeof(HumanBone_t2465339518_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1365[3] = 
{
	HumanBone_t2465339518::get_offset_of_m_BoneName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t2465339518::get_offset_of_m_HumanName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t2465339518::get_offset_of_limit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (RuntimeAnimatorController_t2933699135), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (AnimatorControllerPlayable_t1397024487), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (AnimationMixerPlayable_t2837525843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (AnimationLayerMixerPlayable_t1193230317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (AnimationClipPlayable_t2591841208), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (AnimationPlayable_t2436992687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (AnimationOffsetPlayable_t3136944237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (Motion_t1110556653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (FontStyle_t82229486)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1374[5] = 
{
	FontStyle_t82229486::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (TextGenerationError_t3604799999)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1375[5] = 
{
	TextGenerationError_t3604799999::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { sizeof (TextGenerationSettings_t1351628751)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1376[18] = 
{
	TextGenerationSettings_t1351628751::get_offset_of_font_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_lineSpacing_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_richText_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_scaleFactor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_fontStyle_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_textAnchor_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_alignByGeometry_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_resizeTextForBestFit_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_resizeTextMinSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_resizeTextMaxSize_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_updateBounds_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_verticalOverflow_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_horizontalOverflow_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_generationExtents_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_pivot_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_generateOutOfBounds_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { sizeof (TextGenerator_t3211863866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1377[11] = 
{
	TextGenerator_t3211863866::get_offset_of_m_Ptr_0(),
	TextGenerator_t3211863866::get_offset_of_m_LastString_1(),
	TextGenerator_t3211863866::get_offset_of_m_LastSettings_2(),
	TextGenerator_t3211863866::get_offset_of_m_HasGenerated_3(),
	TextGenerator_t3211863866::get_offset_of_m_LastValid_4(),
	TextGenerator_t3211863866::get_offset_of_m_Verts_5(),
	TextGenerator_t3211863866::get_offset_of_m_Characters_6(),
	TextGenerator_t3211863866::get_offset_of_m_Lines_7(),
	TextGenerator_t3211863866::get_offset_of_m_CachedVerts_8(),
	TextGenerator_t3211863866::get_offset_of_m_CachedCharacters_9(),
	TextGenerator_t3211863866::get_offset_of_m_CachedLines_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (TextAnchor_t2035777396)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1378[10] = 
{
	TextAnchor_t2035777396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { sizeof (HorizontalWrapMode_t2172737147)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1379[3] = 
{
	HorizontalWrapMode_t2172737147::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (VerticalWrapMode_t2936607737)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1380[3] = 
{
	VerticalWrapMode_t2936607737::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (GUIText_t402233326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (Font_t1956802104), -1, sizeof(Font_t1956802104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1382[2] = 
{
	Font_t1956802104_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t1956802104::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { sizeof (FontTextureRebuildCallback_t2467502454), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { sizeof (UICharInfo_t75501106)+ sizeof (Il2CppObject), sizeof(UICharInfo_t75501106 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1384[2] = 
{
	UICharInfo_t75501106::get_offset_of_cursorPos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UICharInfo_t75501106::get_offset_of_charWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { sizeof (UILineInfo_t4195266810)+ sizeof (Il2CppObject), sizeof(UILineInfo_t4195266810 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1385[4] = 
{
	UILineInfo_t4195266810::get_offset_of_startCharIdx_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4195266810::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4195266810::get_offset_of_topY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4195266810::get_offset_of_leading_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { sizeof (UIVertex_t4057497605)+ sizeof (Il2CppObject), sizeof(UIVertex_t4057497605 ), sizeof(UIVertex_t4057497605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1386[11] = 
{
	UIVertex_t4057497605::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_color_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv2_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv3_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_tangent_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605_StaticFields::get_offset_of_s_DefaultColor_8(),
	UIVertex_t4057497605_StaticFields::get_offset_of_s_DefaultTangent_9(),
	UIVertex_t4057497605_StaticFields::get_offset_of_simpleVert_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { sizeof (RectTransformUtility_t1743242446), -1, sizeof(RectTransformUtility_t1743242446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1387[1] = 
{
	RectTransformUtility_t1743242446_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (RenderMode_t4077056833)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1388[4] = 
{
	RenderMode_t4077056833::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (Canvas_t3310196443), -1, sizeof(Canvas_t3310196443_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1389[1] = 
{
	Canvas_t3310196443_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (WillRenderCanvases_t3309123499), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (CanvasGroup_t4083511760), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (CanvasRenderer_t2598313366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (Event_t2956885303), sizeof(Event_t2956885303_marshaled_pinvoke), sizeof(Event_t2956885303_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1394[4] = 
{
	Event_t2956885303::get_offset_of_m_Ptr_0(),
	Event_t2956885303_StaticFields::get_offset_of_s_Current_1(),
	Event_t2956885303_StaticFields::get_offset_of_s_MasterEvent_2(),
	Event_t2956885303_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (EventType_t3528516131)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1395[33] = 
{
	EventType_t3528516131::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (EventModifiers_t2016417398)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1396[9] = 
{
	EventModifiers_t2016417398::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (GUI_t1624858472), -1, sizeof(GUI_t1624858472_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1397[12] = 
{
	GUI_t1624858472_StaticFields::get_offset_of_s_ScrollStepSize_0(),
	GUI_t1624858472_StaticFields::get_offset_of_s_HotTextField_1(),
	GUI_t1624858472_StaticFields::get_offset_of_s_BoxHash_2(),
	GUI_t1624858472_StaticFields::get_offset_of_s_RepeatButtonHash_3(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ToggleHash_4(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ButtonGridHash_5(),
	GUI_t1624858472_StaticFields::get_offset_of_s_SliderHash_6(),
	GUI_t1624858472_StaticFields::get_offset_of_s_BeginGroupHash_7(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ScrollviewHash_8(),
	GUI_t1624858472_StaticFields::get_offset_of_U3CnextScrollStepTimeU3Ek__BackingField_9(),
	GUI_t1624858472_StaticFields::get_offset_of_s_Skin_10(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ScrollViewStates_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (WindowFunction_t3146511083), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (GUIContent_t3050628031), -1, sizeof(GUIContent_t3050628031_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1399[7] = 
{
	GUIContent_t3050628031::get_offset_of_m_Text_0(),
	GUIContent_t3050628031::get_offset_of_m_Image_1(),
	GUIContent_t3050628031::get_offset_of_m_Tooltip_2(),
	GUIContent_t3050628031_StaticFields::get_offset_of_s_Text_3(),
	GUIContent_t3050628031_StaticFields::get_offset_of_s_Image_4(),
	GUIContent_t3050628031_StaticFields::get_offset_of_s_TextImage_5(),
	GUIContent_t3050628031_StaticFields::get_offset_of_none_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
