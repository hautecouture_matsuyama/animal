﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonReplayOver
struct  ButtonReplayOver_t967915415  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color ButtonReplayOver::colorPushed
	Color_t2555686324  ___colorPushed_2;
	// UnityEngine.Color ButtonReplayOver::originalColor
	Color_t2555686324  ___originalColor_3;

public:
	inline static int32_t get_offset_of_colorPushed_2() { return static_cast<int32_t>(offsetof(ButtonReplayOver_t967915415, ___colorPushed_2)); }
	inline Color_t2555686324  get_colorPushed_2() const { return ___colorPushed_2; }
	inline Color_t2555686324 * get_address_of_colorPushed_2() { return &___colorPushed_2; }
	inline void set_colorPushed_2(Color_t2555686324  value)
	{
		___colorPushed_2 = value;
	}

	inline static int32_t get_offset_of_originalColor_3() { return static_cast<int32_t>(offsetof(ButtonReplayOver_t967915415, ___originalColor_3)); }
	inline Color_t2555686324  get_originalColor_3() const { return ___originalColor_3; }
	inline Color_t2555686324 * get_address_of_originalColor_3() { return &___originalColor_3; }
	inline void set_originalColor_3(Color_t2555686324  value)
	{
		___originalColor_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
