﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonSound
struct  ButtonSound_t35905277  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color ButtonSound::colorPushed
	Color_t2555686324  ___colorPushed_2;
	// UnityEngine.Color ButtonSound::originalColor
	Color_t2555686324  ___originalColor_3;
	// UnityEngine.GameObject ButtonSound::buttonOff
	GameObject_t1113636619 * ___buttonOff_4;

public:
	inline static int32_t get_offset_of_colorPushed_2() { return static_cast<int32_t>(offsetof(ButtonSound_t35905277, ___colorPushed_2)); }
	inline Color_t2555686324  get_colorPushed_2() const { return ___colorPushed_2; }
	inline Color_t2555686324 * get_address_of_colorPushed_2() { return &___colorPushed_2; }
	inline void set_colorPushed_2(Color_t2555686324  value)
	{
		___colorPushed_2 = value;
	}

	inline static int32_t get_offset_of_originalColor_3() { return static_cast<int32_t>(offsetof(ButtonSound_t35905277, ___originalColor_3)); }
	inline Color_t2555686324  get_originalColor_3() const { return ___originalColor_3; }
	inline Color_t2555686324 * get_address_of_originalColor_3() { return &___originalColor_3; }
	inline void set_originalColor_3(Color_t2555686324  value)
	{
		___originalColor_3 = value;
	}

	inline static int32_t get_offset_of_buttonOff_4() { return static_cast<int32_t>(offsetof(ButtonSound_t35905277, ___buttonOff_4)); }
	inline GameObject_t1113636619 * get_buttonOff_4() const { return ___buttonOff_4; }
	inline GameObject_t1113636619 ** get_address_of_buttonOff_4() { return &___buttonOff_4; }
	inline void set_buttonOff_4(GameObject_t1113636619 * value)
	{
		___buttonOff_4 = value;
		Il2CppCodeGenWriteBarrier(&___buttonOff_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
