﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect2440176439.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.NicerOutline
struct  NicerOutline_t1293666038  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.NicerOutline::m_EffectColor
	Color_t2555686324  ___m_EffectColor_3;
	// System.Single UnityEngine.UI.NicerOutline::m_EffectDistance
	float ___m_EffectDistance_4;
	// System.Int32 UnityEngine.UI.NicerOutline::m_nEffectNumber
	int32_t ___m_nEffectNumber_5;
	// System.Boolean UnityEngine.UI.NicerOutline::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_6;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(NicerOutline_t1293666038, ___m_EffectColor_3)); }
	inline Color_t2555686324  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2555686324  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(NicerOutline_t1293666038, ___m_EffectDistance_4)); }
	inline float get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline float* get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(float value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_nEffectNumber_5() { return static_cast<int32_t>(offsetof(NicerOutline_t1293666038, ___m_nEffectNumber_5)); }
	inline int32_t get_m_nEffectNumber_5() const { return ___m_nEffectNumber_5; }
	inline int32_t* get_address_of_m_nEffectNumber_5() { return &___m_nEffectNumber_5; }
	inline void set_m_nEffectNumber_5(int32_t value)
	{
		___m_nEffectNumber_5 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_6() { return static_cast<int32_t>(offsetof(NicerOutline_t1293666038, ___m_UseGraphicAlpha_6)); }
	inline bool get_m_UseGraphicAlpha_6() const { return ___m_UseGraphicAlpha_6; }
	inline bool* get_address_of_m_UseGraphicAlpha_6() { return &___m_UseGraphicAlpha_6; }
	inline void set_m_UseGraphicAlpha_6(bool value)
	{
		___m_UseGraphicAlpha_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
