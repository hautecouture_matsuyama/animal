﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdU3769286665.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.String,System.Double>
struct Dictionary_2_t379921662;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t4177511560;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Video.NendAdUserFeature
struct  NendAdUserFeature_t4152365987  : public Il2CppObject
{
public:
	// NendUnityPlugin.AD.Video.NendAdUserFeature/Gender NendUnityPlugin.AD.Video.NendAdUserFeature::gender
	int32_t ___gender_0;
	// System.Int32 NendUnityPlugin.AD.Video.NendAdUserFeature::dayOfBirth
	int32_t ___dayOfBirth_1;
	// System.Int32 NendUnityPlugin.AD.Video.NendAdUserFeature::monthOfBirth
	int32_t ___monthOfBirth_2;
	// System.Int32 NendUnityPlugin.AD.Video.NendAdUserFeature::yearOfBirth
	int32_t ___yearOfBirth_3;
	// System.Int32 NendUnityPlugin.AD.Video.NendAdUserFeature::age
	int32_t ___age_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> NendUnityPlugin.AD.Video.NendAdUserFeature::customFeaturesIntDic
	Dictionary_2_t2736202052 * ___customFeaturesIntDic_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Double> NendUnityPlugin.AD.Video.NendAdUserFeature::customFeaturesDoubleDic
	Dictionary_2_t379921662 * ___customFeaturesDoubleDic_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> NendUnityPlugin.AD.Video.NendAdUserFeature::customFeaturesStringDic
	Dictionary_2_t1632706988 * ___customFeaturesStringDic_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> NendUnityPlugin.AD.Video.NendAdUserFeature::customFeaturesBoolDic
	Dictionary_2_t4177511560 * ___customFeaturesBoolDic_8;

public:
	inline static int32_t get_offset_of_gender_0() { return static_cast<int32_t>(offsetof(NendAdUserFeature_t4152365987, ___gender_0)); }
	inline int32_t get_gender_0() const { return ___gender_0; }
	inline int32_t* get_address_of_gender_0() { return &___gender_0; }
	inline void set_gender_0(int32_t value)
	{
		___gender_0 = value;
	}

	inline static int32_t get_offset_of_dayOfBirth_1() { return static_cast<int32_t>(offsetof(NendAdUserFeature_t4152365987, ___dayOfBirth_1)); }
	inline int32_t get_dayOfBirth_1() const { return ___dayOfBirth_1; }
	inline int32_t* get_address_of_dayOfBirth_1() { return &___dayOfBirth_1; }
	inline void set_dayOfBirth_1(int32_t value)
	{
		___dayOfBirth_1 = value;
	}

	inline static int32_t get_offset_of_monthOfBirth_2() { return static_cast<int32_t>(offsetof(NendAdUserFeature_t4152365987, ___monthOfBirth_2)); }
	inline int32_t get_monthOfBirth_2() const { return ___monthOfBirth_2; }
	inline int32_t* get_address_of_monthOfBirth_2() { return &___monthOfBirth_2; }
	inline void set_monthOfBirth_2(int32_t value)
	{
		___monthOfBirth_2 = value;
	}

	inline static int32_t get_offset_of_yearOfBirth_3() { return static_cast<int32_t>(offsetof(NendAdUserFeature_t4152365987, ___yearOfBirth_3)); }
	inline int32_t get_yearOfBirth_3() const { return ___yearOfBirth_3; }
	inline int32_t* get_address_of_yearOfBirth_3() { return &___yearOfBirth_3; }
	inline void set_yearOfBirth_3(int32_t value)
	{
		___yearOfBirth_3 = value;
	}

	inline static int32_t get_offset_of_age_4() { return static_cast<int32_t>(offsetof(NendAdUserFeature_t4152365987, ___age_4)); }
	inline int32_t get_age_4() const { return ___age_4; }
	inline int32_t* get_address_of_age_4() { return &___age_4; }
	inline void set_age_4(int32_t value)
	{
		___age_4 = value;
	}

	inline static int32_t get_offset_of_customFeaturesIntDic_5() { return static_cast<int32_t>(offsetof(NendAdUserFeature_t4152365987, ___customFeaturesIntDic_5)); }
	inline Dictionary_2_t2736202052 * get_customFeaturesIntDic_5() const { return ___customFeaturesIntDic_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_customFeaturesIntDic_5() { return &___customFeaturesIntDic_5; }
	inline void set_customFeaturesIntDic_5(Dictionary_2_t2736202052 * value)
	{
		___customFeaturesIntDic_5 = value;
		Il2CppCodeGenWriteBarrier(&___customFeaturesIntDic_5, value);
	}

	inline static int32_t get_offset_of_customFeaturesDoubleDic_6() { return static_cast<int32_t>(offsetof(NendAdUserFeature_t4152365987, ___customFeaturesDoubleDic_6)); }
	inline Dictionary_2_t379921662 * get_customFeaturesDoubleDic_6() const { return ___customFeaturesDoubleDic_6; }
	inline Dictionary_2_t379921662 ** get_address_of_customFeaturesDoubleDic_6() { return &___customFeaturesDoubleDic_6; }
	inline void set_customFeaturesDoubleDic_6(Dictionary_2_t379921662 * value)
	{
		___customFeaturesDoubleDic_6 = value;
		Il2CppCodeGenWriteBarrier(&___customFeaturesDoubleDic_6, value);
	}

	inline static int32_t get_offset_of_customFeaturesStringDic_7() { return static_cast<int32_t>(offsetof(NendAdUserFeature_t4152365987, ___customFeaturesStringDic_7)); }
	inline Dictionary_2_t1632706988 * get_customFeaturesStringDic_7() const { return ___customFeaturesStringDic_7; }
	inline Dictionary_2_t1632706988 ** get_address_of_customFeaturesStringDic_7() { return &___customFeaturesStringDic_7; }
	inline void set_customFeaturesStringDic_7(Dictionary_2_t1632706988 * value)
	{
		___customFeaturesStringDic_7 = value;
		Il2CppCodeGenWriteBarrier(&___customFeaturesStringDic_7, value);
	}

	inline static int32_t get_offset_of_customFeaturesBoolDic_8() { return static_cast<int32_t>(offsetof(NendAdUserFeature_t4152365987, ___customFeaturesBoolDic_8)); }
	inline Dictionary_2_t4177511560 * get_customFeaturesBoolDic_8() const { return ___customFeaturesBoolDic_8; }
	inline Dictionary_2_t4177511560 ** get_address_of_customFeaturesBoolDic_8() { return &___customFeaturesBoolDic_8; }
	inline void set_customFeaturesBoolDic_8(Dictionary_2_t4177511560 * value)
	{
		___customFeaturesBoolDic_8 = value;
		Il2CppCodeGenWriteBarrier(&___customFeaturesBoolDic_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
