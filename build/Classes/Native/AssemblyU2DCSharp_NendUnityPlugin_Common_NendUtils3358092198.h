﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Common.NendUtils/NendID
struct  NendID_t3358092198  : public Il2CppObject
{
public:
	// System.String NendUnityPlugin.Common.NendUtils/NendID::apiKey
	String_t* ___apiKey_0;
	// System.Int32 NendUnityPlugin.Common.NendUtils/NendID::spotID
	int32_t ___spotID_1;

public:
	inline static int32_t get_offset_of_apiKey_0() { return static_cast<int32_t>(offsetof(NendID_t3358092198, ___apiKey_0)); }
	inline String_t* get_apiKey_0() const { return ___apiKey_0; }
	inline String_t** get_address_of_apiKey_0() { return &___apiKey_0; }
	inline void set_apiKey_0(String_t* value)
	{
		___apiKey_0 = value;
		Il2CppCodeGenWriteBarrier(&___apiKey_0, value);
	}

	inline static int32_t get_offset_of_spotID_1() { return static_cast<int32_t>(offsetof(NendID_t3358092198, ___spotID_1)); }
	inline int32_t get_spotID_1() const { return ___spotID_1; }
	inline int32_t* get_address_of_spotID_1() { return &___spotID_1; }
	inline void set_spotID_1(int32_t value)
	{
		___spotID_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
