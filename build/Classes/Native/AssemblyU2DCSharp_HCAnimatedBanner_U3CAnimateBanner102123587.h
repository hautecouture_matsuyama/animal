﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// HCAnimatedBanner/<AnimateBanner>c__Iterator2
struct U3CAnimateBannerU3Ec__Iterator2_t688012631;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HCAnimatedBanner/<AnimateBanner>c__Iterator2/<AnimateBanner>c__AnonStorey3
struct  U3CAnimateBannerU3Ec__AnonStorey3_t102123587  : public Il2CppObject
{
public:
	// System.Int32 HCAnimatedBanner/<AnimateBanner>c__Iterator2/<AnimateBanner>c__AnonStorey3::textureWidth
	int32_t ___textureWidth_0;
	// System.Int32 HCAnimatedBanner/<AnimateBanner>c__Iterator2/<AnimateBanner>c__AnonStorey3::textureHeight
	int32_t ___textureHeight_1;
	// HCAnimatedBanner/<AnimateBanner>c__Iterator2 HCAnimatedBanner/<AnimateBanner>c__Iterator2/<AnimateBanner>c__AnonStorey3::<>f__ref$2
	U3CAnimateBannerU3Ec__Iterator2_t688012631 * ___U3CU3Ef__refU242_2;

public:
	inline static int32_t get_offset_of_textureWidth_0() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__AnonStorey3_t102123587, ___textureWidth_0)); }
	inline int32_t get_textureWidth_0() const { return ___textureWidth_0; }
	inline int32_t* get_address_of_textureWidth_0() { return &___textureWidth_0; }
	inline void set_textureWidth_0(int32_t value)
	{
		___textureWidth_0 = value;
	}

	inline static int32_t get_offset_of_textureHeight_1() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__AnonStorey3_t102123587, ___textureHeight_1)); }
	inline int32_t get_textureHeight_1() const { return ___textureHeight_1; }
	inline int32_t* get_address_of_textureHeight_1() { return &___textureHeight_1; }
	inline void set_textureHeight_1(int32_t value)
	{
		___textureHeight_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_2() { return static_cast<int32_t>(offsetof(U3CAnimateBannerU3Ec__AnonStorey3_t102123587, ___U3CU3Ef__refU242_2)); }
	inline U3CAnimateBannerU3Ec__Iterator2_t688012631 * get_U3CU3Ef__refU242_2() const { return ___U3CU3Ef__refU242_2; }
	inline U3CAnimateBannerU3Ec__Iterator2_t688012631 ** get_address_of_U3CU3Ef__refU242_2() { return &___U3CU3Ef__refU242_2; }
	inline void set_U3CU3Ef__refU242_2(U3CAnimateBannerU3Ec__Iterator2_t688012631 * value)
	{
		___U3CU3Ef__refU242_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU242_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
