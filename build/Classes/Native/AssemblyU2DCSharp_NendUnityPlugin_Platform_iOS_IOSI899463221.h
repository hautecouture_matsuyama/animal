﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdI1383172729.h"
#include "mscorlib_System_IntPtr840150181.h"

// NendUnityPlugin.Platform.iOS.IOSInterstitialVideoAd/NendUnityVideoAdNormalCallback
struct NendUnityVideoAdNormalCallback_t3142830758;
// NendUnityPlugin.Platform.iOS.IOSInterstitialVideoAd/NendUnityVideoAdErrorCallback
struct NendUnityVideoAdErrorCallback_t4190580340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Platform.iOS.IOSInterstitialVideoAd
struct  IOSInterstitialVideoAd_t899463221  : public NendAdInterstitialVideo_t1383172729
{
public:
	// System.IntPtr NendUnityPlugin.Platform.iOS.IOSInterstitialVideoAd::m_iOSInterstitialVideoAdPtr
	IntPtr_t ___m_iOSInterstitialVideoAdPtr_10;
	// System.IntPtr NendUnityPlugin.Platform.iOS.IOSInterstitialVideoAd::m_selfPtr
	IntPtr_t ___m_selfPtr_11;

public:
	inline static int32_t get_offset_of_m_iOSInterstitialVideoAdPtr_10() { return static_cast<int32_t>(offsetof(IOSInterstitialVideoAd_t899463221, ___m_iOSInterstitialVideoAdPtr_10)); }
	inline IntPtr_t get_m_iOSInterstitialVideoAdPtr_10() const { return ___m_iOSInterstitialVideoAdPtr_10; }
	inline IntPtr_t* get_address_of_m_iOSInterstitialVideoAdPtr_10() { return &___m_iOSInterstitialVideoAdPtr_10; }
	inline void set_m_iOSInterstitialVideoAdPtr_10(IntPtr_t value)
	{
		___m_iOSInterstitialVideoAdPtr_10 = value;
	}

	inline static int32_t get_offset_of_m_selfPtr_11() { return static_cast<int32_t>(offsetof(IOSInterstitialVideoAd_t899463221, ___m_selfPtr_11)); }
	inline IntPtr_t get_m_selfPtr_11() const { return ___m_selfPtr_11; }
	inline IntPtr_t* get_address_of_m_selfPtr_11() { return &___m_selfPtr_11; }
	inline void set_m_selfPtr_11(IntPtr_t value)
	{
		___m_selfPtr_11 = value;
	}
};

struct IOSInterstitialVideoAd_t899463221_StaticFields
{
public:
	// NendUnityPlugin.Platform.iOS.IOSInterstitialVideoAd/NendUnityVideoAdNormalCallback NendUnityPlugin.Platform.iOS.IOSInterstitialVideoAd::<>f__mg$cache0
	NendUnityVideoAdNormalCallback_t3142830758 * ___U3CU3Ef__mgU24cache0_12;
	// NendUnityPlugin.Platform.iOS.IOSInterstitialVideoAd/NendUnityVideoAdErrorCallback NendUnityPlugin.Platform.iOS.IOSInterstitialVideoAd::<>f__mg$cache1
	NendUnityVideoAdErrorCallback_t4190580340 * ___U3CU3Ef__mgU24cache1_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_12() { return static_cast<int32_t>(offsetof(IOSInterstitialVideoAd_t899463221_StaticFields, ___U3CU3Ef__mgU24cache0_12)); }
	inline NendUnityVideoAdNormalCallback_t3142830758 * get_U3CU3Ef__mgU24cache0_12() const { return ___U3CU3Ef__mgU24cache0_12; }
	inline NendUnityVideoAdNormalCallback_t3142830758 ** get_address_of_U3CU3Ef__mgU24cache0_12() { return &___U3CU3Ef__mgU24cache0_12; }
	inline void set_U3CU3Ef__mgU24cache0_12(NendUnityVideoAdNormalCallback_t3142830758 * value)
	{
		___U3CU3Ef__mgU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_13() { return static_cast<int32_t>(offsetof(IOSInterstitialVideoAd_t899463221_StaticFields, ___U3CU3Ef__mgU24cache1_13)); }
	inline NendUnityVideoAdErrorCallback_t4190580340 * get_U3CU3Ef__mgU24cache1_13() const { return ___U3CU3Ef__mgU24cache1_13; }
	inline NendUnityVideoAdErrorCallback_t4190580340 ** get_address_of_U3CU3Ef__mgU24cache1_13() { return &___U3CU3Ef__mgU24cache1_13; }
	inline void set_U3CU3Ef__mgU24cache1_13(NendUnityVideoAdErrorCallback_t4190580340 * value)
	{
		___U3CU3Ef__mgU24cache1_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
