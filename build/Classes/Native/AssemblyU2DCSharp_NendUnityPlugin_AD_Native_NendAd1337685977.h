﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey1
struct  U3CLoadAdU3Ec__AnonStorey1_t1337685977  : public Il2CppObject
{
public:
	// System.Int32 NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey1::tag
	int32_t ___tag_0;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(U3CLoadAdU3Ec__AnonStorey1_t1337685977, ___tag_0)); }
	inline int32_t get_tag_0() const { return ___tag_0; }
	inline int32_t* get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(int32_t value)
	{
		___tag_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
