﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "AssemblyU2DCSharp_SwipeDetector_SwipeDirection1609046864.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwipeDetector
struct  SwipeDetector_t548578696  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SwipeDetector::comfortZone
	float ___comfortZone_2;
	// System.Single SwipeDetector::minSwipeDist
	float ___minSwipeDist_3;
	// System.Single SwipeDetector::maxSwipeTime
	float ___maxSwipeTime_4;
	// System.Single SwipeDetector::startTime
	float ___startTime_5;
	// UnityEngine.Vector2 SwipeDetector::startPos
	Vector2_t2156229523  ___startPos_6;
	// System.Boolean SwipeDetector::couldBeSwipe
	bool ___couldBeSwipe_7;
	// System.Boolean SwipeDetector::couldBeHorSwipe
	bool ___couldBeHorSwipe_8;
	// System.Boolean SwipeDetector::swipeRightCD
	bool ___swipeRightCD_9;
	// System.Single SwipeDetector::swipeRightTime
	float ___swipeRightTime_10;
	// System.Single SwipeDetector::swipeRightTimeReset
	float ___swipeRightTimeReset_11;
	// System.Boolean SwipeDetector::swipeLeftCD
	bool ___swipeLeftCD_12;
	// System.Single SwipeDetector::swipeLeftTime
	float ___swipeLeftTime_13;
	// System.Single SwipeDetector::swipeLeftTimeReset
	float ___swipeLeftTimeReset_14;
	// SwipeDetector/SwipeDirection SwipeDetector::lastSwipe
	int32_t ___lastSwipe_15;
	// System.Single SwipeDetector::lastSwipeTime
	float ___lastSwipeTime_16;

public:
	inline static int32_t get_offset_of_comfortZone_2() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___comfortZone_2)); }
	inline float get_comfortZone_2() const { return ___comfortZone_2; }
	inline float* get_address_of_comfortZone_2() { return &___comfortZone_2; }
	inline void set_comfortZone_2(float value)
	{
		___comfortZone_2 = value;
	}

	inline static int32_t get_offset_of_minSwipeDist_3() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___minSwipeDist_3)); }
	inline float get_minSwipeDist_3() const { return ___minSwipeDist_3; }
	inline float* get_address_of_minSwipeDist_3() { return &___minSwipeDist_3; }
	inline void set_minSwipeDist_3(float value)
	{
		___minSwipeDist_3 = value;
	}

	inline static int32_t get_offset_of_maxSwipeTime_4() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___maxSwipeTime_4)); }
	inline float get_maxSwipeTime_4() const { return ___maxSwipeTime_4; }
	inline float* get_address_of_maxSwipeTime_4() { return &___maxSwipeTime_4; }
	inline void set_maxSwipeTime_4(float value)
	{
		___maxSwipeTime_4 = value;
	}

	inline static int32_t get_offset_of_startTime_5() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___startTime_5)); }
	inline float get_startTime_5() const { return ___startTime_5; }
	inline float* get_address_of_startTime_5() { return &___startTime_5; }
	inline void set_startTime_5(float value)
	{
		___startTime_5 = value;
	}

	inline static int32_t get_offset_of_startPos_6() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___startPos_6)); }
	inline Vector2_t2156229523  get_startPos_6() const { return ___startPos_6; }
	inline Vector2_t2156229523 * get_address_of_startPos_6() { return &___startPos_6; }
	inline void set_startPos_6(Vector2_t2156229523  value)
	{
		___startPos_6 = value;
	}

	inline static int32_t get_offset_of_couldBeSwipe_7() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___couldBeSwipe_7)); }
	inline bool get_couldBeSwipe_7() const { return ___couldBeSwipe_7; }
	inline bool* get_address_of_couldBeSwipe_7() { return &___couldBeSwipe_7; }
	inline void set_couldBeSwipe_7(bool value)
	{
		___couldBeSwipe_7 = value;
	}

	inline static int32_t get_offset_of_couldBeHorSwipe_8() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___couldBeHorSwipe_8)); }
	inline bool get_couldBeHorSwipe_8() const { return ___couldBeHorSwipe_8; }
	inline bool* get_address_of_couldBeHorSwipe_8() { return &___couldBeHorSwipe_8; }
	inline void set_couldBeHorSwipe_8(bool value)
	{
		___couldBeHorSwipe_8 = value;
	}

	inline static int32_t get_offset_of_swipeRightCD_9() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___swipeRightCD_9)); }
	inline bool get_swipeRightCD_9() const { return ___swipeRightCD_9; }
	inline bool* get_address_of_swipeRightCD_9() { return &___swipeRightCD_9; }
	inline void set_swipeRightCD_9(bool value)
	{
		___swipeRightCD_9 = value;
	}

	inline static int32_t get_offset_of_swipeRightTime_10() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___swipeRightTime_10)); }
	inline float get_swipeRightTime_10() const { return ___swipeRightTime_10; }
	inline float* get_address_of_swipeRightTime_10() { return &___swipeRightTime_10; }
	inline void set_swipeRightTime_10(float value)
	{
		___swipeRightTime_10 = value;
	}

	inline static int32_t get_offset_of_swipeRightTimeReset_11() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___swipeRightTimeReset_11)); }
	inline float get_swipeRightTimeReset_11() const { return ___swipeRightTimeReset_11; }
	inline float* get_address_of_swipeRightTimeReset_11() { return &___swipeRightTimeReset_11; }
	inline void set_swipeRightTimeReset_11(float value)
	{
		___swipeRightTimeReset_11 = value;
	}

	inline static int32_t get_offset_of_swipeLeftCD_12() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___swipeLeftCD_12)); }
	inline bool get_swipeLeftCD_12() const { return ___swipeLeftCD_12; }
	inline bool* get_address_of_swipeLeftCD_12() { return &___swipeLeftCD_12; }
	inline void set_swipeLeftCD_12(bool value)
	{
		___swipeLeftCD_12 = value;
	}

	inline static int32_t get_offset_of_swipeLeftTime_13() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___swipeLeftTime_13)); }
	inline float get_swipeLeftTime_13() const { return ___swipeLeftTime_13; }
	inline float* get_address_of_swipeLeftTime_13() { return &___swipeLeftTime_13; }
	inline void set_swipeLeftTime_13(float value)
	{
		___swipeLeftTime_13 = value;
	}

	inline static int32_t get_offset_of_swipeLeftTimeReset_14() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___swipeLeftTimeReset_14)); }
	inline float get_swipeLeftTimeReset_14() const { return ___swipeLeftTimeReset_14; }
	inline float* get_address_of_swipeLeftTimeReset_14() { return &___swipeLeftTimeReset_14; }
	inline void set_swipeLeftTimeReset_14(float value)
	{
		___swipeLeftTimeReset_14 = value;
	}

	inline static int32_t get_offset_of_lastSwipe_15() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___lastSwipe_15)); }
	inline int32_t get_lastSwipe_15() const { return ___lastSwipe_15; }
	inline int32_t* get_address_of_lastSwipe_15() { return &___lastSwipe_15; }
	inline void set_lastSwipe_15(int32_t value)
	{
		___lastSwipe_15 = value;
	}

	inline static int32_t get_offset_of_lastSwipeTime_16() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___lastSwipeTime_16)); }
	inline float get_lastSwipeTime_16() const { return ___lastSwipeTime_16; }
	inline float* get_address_of_lastSwipeTime_16() { return &___lastSwipeTime_16; }
	inline void set_lastSwipeTime_16(float value)
	{
		___lastSwipeTime_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
