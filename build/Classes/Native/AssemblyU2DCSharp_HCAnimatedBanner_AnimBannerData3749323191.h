﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HCAnimatedBanner/AnimBannerData
struct  AnimBannerData_t3749323191  : public Il2CppObject
{
public:
	// System.String HCAnimatedBanner/AnimBannerData::textureIdName
	String_t* ___textureIdName_0;
	// System.String HCAnimatedBanner/AnimBannerData::textureUrl
	String_t* ___textureUrl_1;
	// System.Int32 HCAnimatedBanner/AnimBannerData::textureWidth
	int32_t ___textureWidth_2;
	// System.Int32 HCAnimatedBanner/AnimBannerData::textureHeight
	int32_t ___textureHeight_3;
	// System.Int32 HCAnimatedBanner/AnimBannerData::spriteWidth
	int32_t ___spriteWidth_4;
	// System.Int32 HCAnimatedBanner/AnimBannerData::spriteHeight
	int32_t ___spriteHeight_5;
	// System.Int32 HCAnimatedBanner/AnimBannerData::spriteCount
	int32_t ___spriteCount_6;
	// System.Double HCAnimatedBanner/AnimBannerData::animWaitTime
	double ___animWaitTime_7;
	// System.String HCAnimatedBanner/AnimBannerData::transitionUrlAndroid
	String_t* ___transitionUrlAndroid_8;
	// System.String HCAnimatedBanner/AnimBannerData::transitionUrlIOS
	String_t* ___transitionUrlIOS_9;
	// System.Double HCAnimatedBanner/AnimBannerData::waitForNextAnimSec
	double ___waitForNextAnimSec_10;

public:
	inline static int32_t get_offset_of_textureIdName_0() { return static_cast<int32_t>(offsetof(AnimBannerData_t3749323191, ___textureIdName_0)); }
	inline String_t* get_textureIdName_0() const { return ___textureIdName_0; }
	inline String_t** get_address_of_textureIdName_0() { return &___textureIdName_0; }
	inline void set_textureIdName_0(String_t* value)
	{
		___textureIdName_0 = value;
		Il2CppCodeGenWriteBarrier(&___textureIdName_0, value);
	}

	inline static int32_t get_offset_of_textureUrl_1() { return static_cast<int32_t>(offsetof(AnimBannerData_t3749323191, ___textureUrl_1)); }
	inline String_t* get_textureUrl_1() const { return ___textureUrl_1; }
	inline String_t** get_address_of_textureUrl_1() { return &___textureUrl_1; }
	inline void set_textureUrl_1(String_t* value)
	{
		___textureUrl_1 = value;
		Il2CppCodeGenWriteBarrier(&___textureUrl_1, value);
	}

	inline static int32_t get_offset_of_textureWidth_2() { return static_cast<int32_t>(offsetof(AnimBannerData_t3749323191, ___textureWidth_2)); }
	inline int32_t get_textureWidth_2() const { return ___textureWidth_2; }
	inline int32_t* get_address_of_textureWidth_2() { return &___textureWidth_2; }
	inline void set_textureWidth_2(int32_t value)
	{
		___textureWidth_2 = value;
	}

	inline static int32_t get_offset_of_textureHeight_3() { return static_cast<int32_t>(offsetof(AnimBannerData_t3749323191, ___textureHeight_3)); }
	inline int32_t get_textureHeight_3() const { return ___textureHeight_3; }
	inline int32_t* get_address_of_textureHeight_3() { return &___textureHeight_3; }
	inline void set_textureHeight_3(int32_t value)
	{
		___textureHeight_3 = value;
	}

	inline static int32_t get_offset_of_spriteWidth_4() { return static_cast<int32_t>(offsetof(AnimBannerData_t3749323191, ___spriteWidth_4)); }
	inline int32_t get_spriteWidth_4() const { return ___spriteWidth_4; }
	inline int32_t* get_address_of_spriteWidth_4() { return &___spriteWidth_4; }
	inline void set_spriteWidth_4(int32_t value)
	{
		___spriteWidth_4 = value;
	}

	inline static int32_t get_offset_of_spriteHeight_5() { return static_cast<int32_t>(offsetof(AnimBannerData_t3749323191, ___spriteHeight_5)); }
	inline int32_t get_spriteHeight_5() const { return ___spriteHeight_5; }
	inline int32_t* get_address_of_spriteHeight_5() { return &___spriteHeight_5; }
	inline void set_spriteHeight_5(int32_t value)
	{
		___spriteHeight_5 = value;
	}

	inline static int32_t get_offset_of_spriteCount_6() { return static_cast<int32_t>(offsetof(AnimBannerData_t3749323191, ___spriteCount_6)); }
	inline int32_t get_spriteCount_6() const { return ___spriteCount_6; }
	inline int32_t* get_address_of_spriteCount_6() { return &___spriteCount_6; }
	inline void set_spriteCount_6(int32_t value)
	{
		___spriteCount_6 = value;
	}

	inline static int32_t get_offset_of_animWaitTime_7() { return static_cast<int32_t>(offsetof(AnimBannerData_t3749323191, ___animWaitTime_7)); }
	inline double get_animWaitTime_7() const { return ___animWaitTime_7; }
	inline double* get_address_of_animWaitTime_7() { return &___animWaitTime_7; }
	inline void set_animWaitTime_7(double value)
	{
		___animWaitTime_7 = value;
	}

	inline static int32_t get_offset_of_transitionUrlAndroid_8() { return static_cast<int32_t>(offsetof(AnimBannerData_t3749323191, ___transitionUrlAndroid_8)); }
	inline String_t* get_transitionUrlAndroid_8() const { return ___transitionUrlAndroid_8; }
	inline String_t** get_address_of_transitionUrlAndroid_8() { return &___transitionUrlAndroid_8; }
	inline void set_transitionUrlAndroid_8(String_t* value)
	{
		___transitionUrlAndroid_8 = value;
		Il2CppCodeGenWriteBarrier(&___transitionUrlAndroid_8, value);
	}

	inline static int32_t get_offset_of_transitionUrlIOS_9() { return static_cast<int32_t>(offsetof(AnimBannerData_t3749323191, ___transitionUrlIOS_9)); }
	inline String_t* get_transitionUrlIOS_9() const { return ___transitionUrlIOS_9; }
	inline String_t** get_address_of_transitionUrlIOS_9() { return &___transitionUrlIOS_9; }
	inline void set_transitionUrlIOS_9(String_t* value)
	{
		___transitionUrlIOS_9 = value;
		Il2CppCodeGenWriteBarrier(&___transitionUrlIOS_9, value);
	}

	inline static int32_t get_offset_of_waitForNextAnimSec_10() { return static_cast<int32_t>(offsetof(AnimBannerData_t3749323191, ___waitForNextAnimSec_10)); }
	inline double get_waitForNextAnimSec_10() const { return ___waitForNextAnimSec_10; }
	inline double* get_address_of_waitForNextAnimSec_10() { return &___waitForNextAnimSec_10; }
	inline void set_waitForNextAnimSec_10(double value)
	{
		___waitForNextAnimSec_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
