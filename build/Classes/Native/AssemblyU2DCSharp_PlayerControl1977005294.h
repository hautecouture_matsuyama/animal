﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Animator
struct Animator_t434523843;
// SwipeDetector
struct SwipeDetector_t548578696;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerControl
struct  PlayerControl_t1977005294  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PlayerControl::jump
	bool ___jump_2;
	// System.Boolean PlayerControl::grounded
	bool ___grounded_4;
	// System.Boolean PlayerControl::leftTap
	bool ___leftTap_5;
	// System.Boolean PlayerControl::rightTap
	bool ___rightTap_6;
	// System.Single PlayerControl::jumpForce
	float ___jumpForce_8;
	// UnityEngine.Transform PlayerControl::groundCheck
	Transform_t3600365921 * ___groundCheck_10;
	// SwipeDetector PlayerControl::swipeDetector
	SwipeDetector_t548578696 * ___swipeDetector_12;
	// UnityEngine.AudioClip PlayerControl::jumpClip
	AudioClip_t3680889665 * ___jumpClip_13;
	// UnityEngine.AudioClip PlayerControl::slideClip
	AudioClip_t3680889665 * ___slideClip_14;
	// UnityEngine.AudioClip PlayerControl::musicClip
	AudioClip_t3680889665 * ___musicClip_15;
	// UnityEngine.AudioSource PlayerControl::jumpSource
	AudioSource_t3935305588 * ___jumpSource_16;
	// UnityEngine.AudioSource PlayerControl::slideSource
	AudioSource_t3935305588 * ___slideSource_17;
	// System.Int32 PlayerControl::prevScore
	int32_t ___prevScore_19;

public:
	inline static int32_t get_offset_of_jump_2() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___jump_2)); }
	inline bool get_jump_2() const { return ___jump_2; }
	inline bool* get_address_of_jump_2() { return &___jump_2; }
	inline void set_jump_2(bool value)
	{
		___jump_2 = value;
	}

	inline static int32_t get_offset_of_grounded_4() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___grounded_4)); }
	inline bool get_grounded_4() const { return ___grounded_4; }
	inline bool* get_address_of_grounded_4() { return &___grounded_4; }
	inline void set_grounded_4(bool value)
	{
		___grounded_4 = value;
	}

	inline static int32_t get_offset_of_leftTap_5() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___leftTap_5)); }
	inline bool get_leftTap_5() const { return ___leftTap_5; }
	inline bool* get_address_of_leftTap_5() { return &___leftTap_5; }
	inline void set_leftTap_5(bool value)
	{
		___leftTap_5 = value;
	}

	inline static int32_t get_offset_of_rightTap_6() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___rightTap_6)); }
	inline bool get_rightTap_6() const { return ___rightTap_6; }
	inline bool* get_address_of_rightTap_6() { return &___rightTap_6; }
	inline void set_rightTap_6(bool value)
	{
		___rightTap_6 = value;
	}

	inline static int32_t get_offset_of_jumpForce_8() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___jumpForce_8)); }
	inline float get_jumpForce_8() const { return ___jumpForce_8; }
	inline float* get_address_of_jumpForce_8() { return &___jumpForce_8; }
	inline void set_jumpForce_8(float value)
	{
		___jumpForce_8 = value;
	}

	inline static int32_t get_offset_of_groundCheck_10() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___groundCheck_10)); }
	inline Transform_t3600365921 * get_groundCheck_10() const { return ___groundCheck_10; }
	inline Transform_t3600365921 ** get_address_of_groundCheck_10() { return &___groundCheck_10; }
	inline void set_groundCheck_10(Transform_t3600365921 * value)
	{
		___groundCheck_10 = value;
		Il2CppCodeGenWriteBarrier(&___groundCheck_10, value);
	}

	inline static int32_t get_offset_of_swipeDetector_12() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___swipeDetector_12)); }
	inline SwipeDetector_t548578696 * get_swipeDetector_12() const { return ___swipeDetector_12; }
	inline SwipeDetector_t548578696 ** get_address_of_swipeDetector_12() { return &___swipeDetector_12; }
	inline void set_swipeDetector_12(SwipeDetector_t548578696 * value)
	{
		___swipeDetector_12 = value;
		Il2CppCodeGenWriteBarrier(&___swipeDetector_12, value);
	}

	inline static int32_t get_offset_of_jumpClip_13() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___jumpClip_13)); }
	inline AudioClip_t3680889665 * get_jumpClip_13() const { return ___jumpClip_13; }
	inline AudioClip_t3680889665 ** get_address_of_jumpClip_13() { return &___jumpClip_13; }
	inline void set_jumpClip_13(AudioClip_t3680889665 * value)
	{
		___jumpClip_13 = value;
		Il2CppCodeGenWriteBarrier(&___jumpClip_13, value);
	}

	inline static int32_t get_offset_of_slideClip_14() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___slideClip_14)); }
	inline AudioClip_t3680889665 * get_slideClip_14() const { return ___slideClip_14; }
	inline AudioClip_t3680889665 ** get_address_of_slideClip_14() { return &___slideClip_14; }
	inline void set_slideClip_14(AudioClip_t3680889665 * value)
	{
		___slideClip_14 = value;
		Il2CppCodeGenWriteBarrier(&___slideClip_14, value);
	}

	inline static int32_t get_offset_of_musicClip_15() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___musicClip_15)); }
	inline AudioClip_t3680889665 * get_musicClip_15() const { return ___musicClip_15; }
	inline AudioClip_t3680889665 ** get_address_of_musicClip_15() { return &___musicClip_15; }
	inline void set_musicClip_15(AudioClip_t3680889665 * value)
	{
		___musicClip_15 = value;
		Il2CppCodeGenWriteBarrier(&___musicClip_15, value);
	}

	inline static int32_t get_offset_of_jumpSource_16() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___jumpSource_16)); }
	inline AudioSource_t3935305588 * get_jumpSource_16() const { return ___jumpSource_16; }
	inline AudioSource_t3935305588 ** get_address_of_jumpSource_16() { return &___jumpSource_16; }
	inline void set_jumpSource_16(AudioSource_t3935305588 * value)
	{
		___jumpSource_16 = value;
		Il2CppCodeGenWriteBarrier(&___jumpSource_16, value);
	}

	inline static int32_t get_offset_of_slideSource_17() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___slideSource_17)); }
	inline AudioSource_t3935305588 * get_slideSource_17() const { return ___slideSource_17; }
	inline AudioSource_t3935305588 ** get_address_of_slideSource_17() { return &___slideSource_17; }
	inline void set_slideSource_17(AudioSource_t3935305588 * value)
	{
		___slideSource_17 = value;
		Il2CppCodeGenWriteBarrier(&___slideSource_17, value);
	}

	inline static int32_t get_offset_of_prevScore_19() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294, ___prevScore_19)); }
	inline int32_t get_prevScore_19() const { return ___prevScore_19; }
	inline int32_t* get_address_of_prevScore_19() { return &___prevScore_19; }
	inline void set_prevScore_19(int32_t value)
	{
		___prevScore_19 = value;
	}
};

struct PlayerControl_t1977005294_StaticFields
{
public:
	// System.Boolean PlayerControl::slide
	bool ___slide_3;
	// System.Boolean PlayerControl::isDead
	bool ___isDead_7;
	// System.Single PlayerControl::speed
	float ___speed_9;
	// UnityEngine.Animator PlayerControl::anim
	Animator_t434523843 * ___anim_11;
	// UnityEngine.AudioSource PlayerControl::musicSource
	AudioSource_t3935305588 * ___musicSource_18;

public:
	inline static int32_t get_offset_of_slide_3() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294_StaticFields, ___slide_3)); }
	inline bool get_slide_3() const { return ___slide_3; }
	inline bool* get_address_of_slide_3() { return &___slide_3; }
	inline void set_slide_3(bool value)
	{
		___slide_3 = value;
	}

	inline static int32_t get_offset_of_isDead_7() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294_StaticFields, ___isDead_7)); }
	inline bool get_isDead_7() const { return ___isDead_7; }
	inline bool* get_address_of_isDead_7() { return &___isDead_7; }
	inline void set_isDead_7(bool value)
	{
		___isDead_7 = value;
	}

	inline static int32_t get_offset_of_speed_9() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294_StaticFields, ___speed_9)); }
	inline float get_speed_9() const { return ___speed_9; }
	inline float* get_address_of_speed_9() { return &___speed_9; }
	inline void set_speed_9(float value)
	{
		___speed_9 = value;
	}

	inline static int32_t get_offset_of_anim_11() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294_StaticFields, ___anim_11)); }
	inline Animator_t434523843 * get_anim_11() const { return ___anim_11; }
	inline Animator_t434523843 ** get_address_of_anim_11() { return &___anim_11; }
	inline void set_anim_11(Animator_t434523843 * value)
	{
		___anim_11 = value;
		Il2CppCodeGenWriteBarrier(&___anim_11, value);
	}

	inline static int32_t get_offset_of_musicSource_18() { return static_cast<int32_t>(offsetof(PlayerControl_t1977005294_StaticFields, ___musicSource_18)); }
	inline AudioSource_t3935305588 * get_musicSource_18() const { return ___musicSource_18; }
	inline AudioSource_t3935305588 ** get_address_of_musicSource_18() { return &___musicSource_18; }
	inline void set_musicSource_18(AudioSource_t3935305588 * value)
	{
		___musicSource_18 = value;
		Il2CppCodeGenWriteBarrier(&___musicSource_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
