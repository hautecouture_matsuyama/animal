﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Action
struct Action_t1264377477;
// System.Timers.Timer
struct Timer_t1767341190;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.Utils.SimpleTimer
struct  SimpleTimer_t4075384413  : public Il2CppObject
{
public:
	// System.Action NendUnityPlugin.AD.Native.Utils.SimpleTimer::OnFireEvent
	Action_t1264377477 * ___OnFireEvent_0;
	// System.Timers.Timer NendUnityPlugin.AD.Native.Utils.SimpleTimer::m_Timer
	Timer_t1767341190 * ___m_Timer_1;

public:
	inline static int32_t get_offset_of_OnFireEvent_0() { return static_cast<int32_t>(offsetof(SimpleTimer_t4075384413, ___OnFireEvent_0)); }
	inline Action_t1264377477 * get_OnFireEvent_0() const { return ___OnFireEvent_0; }
	inline Action_t1264377477 ** get_address_of_OnFireEvent_0() { return &___OnFireEvent_0; }
	inline void set_OnFireEvent_0(Action_t1264377477 * value)
	{
		___OnFireEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnFireEvent_0, value);
	}

	inline static int32_t get_offset_of_m_Timer_1() { return static_cast<int32_t>(offsetof(SimpleTimer_t4075384413, ___m_Timer_1)); }
	inline Timer_t1767341190 * get_m_Timer_1() const { return ___m_Timer_1; }
	inline Timer_t1767341190 ** get_address_of_m_Timer_1() { return &___m_Timer_1; }
	inline void set_m_Timer_1(Timer_t1767341190 * value)
	{
		___m_Timer_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Timer_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
