﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"

// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardLoaded
struct NendAdFullBoardLoaded_t3830347218;
// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardFailedToLoad
struct NendAdFullBoardFailedToLoad_t4155484538;
// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardShown
struct NendAdFullBoardShown_t3059274368;
// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardClick
struct NendAdFullBoardClick_t3385596145;
// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardDismiss
struct NendAdFullBoardDismiss_t136425400;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.FullBoard.NendAdFullBoard
struct  NendAdFullBoard_t270881991  : public Il2CppObject
{
public:
	// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardLoaded NendUnityPlugin.AD.FullBoard.NendAdFullBoard::AdLoaded
	NendAdFullBoardLoaded_t3830347218 * ___AdLoaded_0;
	// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardFailedToLoad NendUnityPlugin.AD.FullBoard.NendAdFullBoard::AdFailedToLoad
	NendAdFullBoardFailedToLoad_t4155484538 * ___AdFailedToLoad_1;
	// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardShown NendUnityPlugin.AD.FullBoard.NendAdFullBoard::AdShown
	NendAdFullBoardShown_t3059274368 * ___AdShown_2;
	// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardClick NendUnityPlugin.AD.FullBoard.NendAdFullBoard::AdClicked
	NendAdFullBoardClick_t3385596145 * ___AdClicked_3;
	// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardDismiss NendUnityPlugin.AD.FullBoard.NendAdFullBoard::AdDismissed
	NendAdFullBoardDismiss_t136425400 * ___AdDismissed_4;
	// System.Boolean NendUnityPlugin.AD.FullBoard.NendAdFullBoard::m_isLoading
	bool ___m_isLoading_5;
	// System.Boolean NendUnityPlugin.AD.FullBoard.NendAdFullBoard::m_isShowing
	bool ___m_isShowing_6;
	// System.Boolean NendUnityPlugin.AD.FullBoard.NendAdFullBoard::m_isLoadSuccess
	bool ___m_isLoadSuccess_7;
	// UnityEngine.Color NendUnityPlugin.AD.FullBoard.NendAdFullBoard::backgroundColor
	Color_t2555686324  ___backgroundColor_8;

public:
	inline static int32_t get_offset_of_AdLoaded_0() { return static_cast<int32_t>(offsetof(NendAdFullBoard_t270881991, ___AdLoaded_0)); }
	inline NendAdFullBoardLoaded_t3830347218 * get_AdLoaded_0() const { return ___AdLoaded_0; }
	inline NendAdFullBoardLoaded_t3830347218 ** get_address_of_AdLoaded_0() { return &___AdLoaded_0; }
	inline void set_AdLoaded_0(NendAdFullBoardLoaded_t3830347218 * value)
	{
		___AdLoaded_0 = value;
		Il2CppCodeGenWriteBarrier(&___AdLoaded_0, value);
	}

	inline static int32_t get_offset_of_AdFailedToLoad_1() { return static_cast<int32_t>(offsetof(NendAdFullBoard_t270881991, ___AdFailedToLoad_1)); }
	inline NendAdFullBoardFailedToLoad_t4155484538 * get_AdFailedToLoad_1() const { return ___AdFailedToLoad_1; }
	inline NendAdFullBoardFailedToLoad_t4155484538 ** get_address_of_AdFailedToLoad_1() { return &___AdFailedToLoad_1; }
	inline void set_AdFailedToLoad_1(NendAdFullBoardFailedToLoad_t4155484538 * value)
	{
		___AdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier(&___AdFailedToLoad_1, value);
	}

	inline static int32_t get_offset_of_AdShown_2() { return static_cast<int32_t>(offsetof(NendAdFullBoard_t270881991, ___AdShown_2)); }
	inline NendAdFullBoardShown_t3059274368 * get_AdShown_2() const { return ___AdShown_2; }
	inline NendAdFullBoardShown_t3059274368 ** get_address_of_AdShown_2() { return &___AdShown_2; }
	inline void set_AdShown_2(NendAdFullBoardShown_t3059274368 * value)
	{
		___AdShown_2 = value;
		Il2CppCodeGenWriteBarrier(&___AdShown_2, value);
	}

	inline static int32_t get_offset_of_AdClicked_3() { return static_cast<int32_t>(offsetof(NendAdFullBoard_t270881991, ___AdClicked_3)); }
	inline NendAdFullBoardClick_t3385596145 * get_AdClicked_3() const { return ___AdClicked_3; }
	inline NendAdFullBoardClick_t3385596145 ** get_address_of_AdClicked_3() { return &___AdClicked_3; }
	inline void set_AdClicked_3(NendAdFullBoardClick_t3385596145 * value)
	{
		___AdClicked_3 = value;
		Il2CppCodeGenWriteBarrier(&___AdClicked_3, value);
	}

	inline static int32_t get_offset_of_AdDismissed_4() { return static_cast<int32_t>(offsetof(NendAdFullBoard_t270881991, ___AdDismissed_4)); }
	inline NendAdFullBoardDismiss_t136425400 * get_AdDismissed_4() const { return ___AdDismissed_4; }
	inline NendAdFullBoardDismiss_t136425400 ** get_address_of_AdDismissed_4() { return &___AdDismissed_4; }
	inline void set_AdDismissed_4(NendAdFullBoardDismiss_t136425400 * value)
	{
		___AdDismissed_4 = value;
		Il2CppCodeGenWriteBarrier(&___AdDismissed_4, value);
	}

	inline static int32_t get_offset_of_m_isLoading_5() { return static_cast<int32_t>(offsetof(NendAdFullBoard_t270881991, ___m_isLoading_5)); }
	inline bool get_m_isLoading_5() const { return ___m_isLoading_5; }
	inline bool* get_address_of_m_isLoading_5() { return &___m_isLoading_5; }
	inline void set_m_isLoading_5(bool value)
	{
		___m_isLoading_5 = value;
	}

	inline static int32_t get_offset_of_m_isShowing_6() { return static_cast<int32_t>(offsetof(NendAdFullBoard_t270881991, ___m_isShowing_6)); }
	inline bool get_m_isShowing_6() const { return ___m_isShowing_6; }
	inline bool* get_address_of_m_isShowing_6() { return &___m_isShowing_6; }
	inline void set_m_isShowing_6(bool value)
	{
		___m_isShowing_6 = value;
	}

	inline static int32_t get_offset_of_m_isLoadSuccess_7() { return static_cast<int32_t>(offsetof(NendAdFullBoard_t270881991, ___m_isLoadSuccess_7)); }
	inline bool get_m_isLoadSuccess_7() const { return ___m_isLoadSuccess_7; }
	inline bool* get_address_of_m_isLoadSuccess_7() { return &___m_isLoadSuccess_7; }
	inline void set_m_isLoadSuccess_7(bool value)
	{
		___m_isLoadSuccess_7 = value;
	}

	inline static int32_t get_offset_of_backgroundColor_8() { return static_cast<int32_t>(offsetof(NendAdFullBoard_t270881991, ___backgroundColor_8)); }
	inline Color_t2555686324  get_backgroundColor_8() const { return ___backgroundColor_8; }
	inline Color_t2555686324 * get_address_of_backgroundColor_8() { return &___backgroundColor_8; }
	inline void set_backgroundColor_8(Color_t2555686324  value)
	{
		___backgroundColor_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
