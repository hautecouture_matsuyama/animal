﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nend270881991.h"
#include "mscorlib_System_IntPtr840150181.h"

// NendUnityPlugin.Platform.iOS.IOSFullBoardAd/NendUnityFullBoardAdCallback
struct NendUnityFullBoardAdCallback_t900093931;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Platform.iOS.IOSFullBoardAd
struct  IOSFullBoardAd_t1375839176  : public NendAdFullBoard_t270881991
{
public:
	// System.IntPtr NendUnityPlugin.Platform.iOS.IOSFullBoardAd::m_iOSFullBoardAdPtr
	IntPtr_t ___m_iOSFullBoardAdPtr_9;

public:
	inline static int32_t get_offset_of_m_iOSFullBoardAdPtr_9() { return static_cast<int32_t>(offsetof(IOSFullBoardAd_t1375839176, ___m_iOSFullBoardAdPtr_9)); }
	inline IntPtr_t get_m_iOSFullBoardAdPtr_9() const { return ___m_iOSFullBoardAdPtr_9; }
	inline IntPtr_t* get_address_of_m_iOSFullBoardAdPtr_9() { return &___m_iOSFullBoardAdPtr_9; }
	inline void set_m_iOSFullBoardAdPtr_9(IntPtr_t value)
	{
		___m_iOSFullBoardAdPtr_9 = value;
	}
};

struct IOSFullBoardAd_t1375839176_StaticFields
{
public:
	// NendUnityPlugin.Platform.iOS.IOSFullBoardAd/NendUnityFullBoardAdCallback NendUnityPlugin.Platform.iOS.IOSFullBoardAd::<>f__mg$cache0
	NendUnityFullBoardAdCallback_t900093931 * ___U3CU3Ef__mgU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(IOSFullBoardAd_t1375839176_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline NendUnityFullBoardAdCallback_t900093931 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline NendUnityFullBoardAdCallback_t900093931 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(NendUnityFullBoardAdCallback_t900093931 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
