﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Layout_ComplexUn1764145246.h"

// NendUnityPlugin.Layout.NendAdDefaultLayoutParams
struct NendAdDefaultLayoutParams_t129204534;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Layout.NendAdDefaultLayoutBuilder
struct  NendAdDefaultLayoutBuilder_t3894915050  : public Il2CppObject
{
public:
	// NendUnityPlugin.Layout.NendAdDefaultLayoutParams NendUnityPlugin.Layout.NendAdDefaultLayoutBuilder::layoutParams
	NendAdDefaultLayoutParams_t129204534 * ___layoutParams_0;
	// NendUnityPlugin.Layout.ComplexUnit NendUnityPlugin.Layout.NendAdDefaultLayoutBuilder::unit
	int32_t ___unit_1;

public:
	inline static int32_t get_offset_of_layoutParams_0() { return static_cast<int32_t>(offsetof(NendAdDefaultLayoutBuilder_t3894915050, ___layoutParams_0)); }
	inline NendAdDefaultLayoutParams_t129204534 * get_layoutParams_0() const { return ___layoutParams_0; }
	inline NendAdDefaultLayoutParams_t129204534 ** get_address_of_layoutParams_0() { return &___layoutParams_0; }
	inline void set_layoutParams_0(NendAdDefaultLayoutParams_t129204534 * value)
	{
		___layoutParams_0 = value;
		Il2CppCodeGenWriteBarrier(&___layoutParams_0, value);
	}

	inline static int32_t get_offset_of_unit_1() { return static_cast<int32_t>(offsetof(NendAdDefaultLayoutBuilder_t3894915050, ___unit_1)); }
	inline int32_t get_unit_1() const { return ___unit_1; }
	inline int32_t* get_address_of_unit_1() { return &___unit_1; }
	inline void set_unit_1(int32_t value)
	{
		___unit_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
