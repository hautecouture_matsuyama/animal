﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_NendAdNat91133031.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_NendAdN4247736308.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS1750365051.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS1375839176.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOSF900093931.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS1789666179.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOSI899463221.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS3142830758.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS4190580340.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOSL172280037.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS2862447157.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS1155913684.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS2120700280.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS2914238892.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS3134600846.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOSR418341462.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS1320073170.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS2237018260.h"
#include "AssemblyU2DCSharp_SwipeDetector548578696.h"
#include "AssemblyU2DCSharp_SwipeDetector_SwipeDirection1609046864.h"
#include "AssemblyU2DCSharp_BackKeyAction3328090672.h"
#include "AssemblyU2DCSharp_BackButtonUtil2880819819.h"
#include "AssemblyU2DCSharp_IpadAspectRation3315943056.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_NicerOutline1293666038.h"
#include "AssemblyU2DCSharp_SoundButton3154830285.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (StubInterstitialVideo_t91133031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (StubRewardedVideo_t4247736308), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (IOSBannerInterface_t1750365051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (IOSFullBoardAd_t1375839176), -1, sizeof(IOSFullBoardAd_t1375839176_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1903[2] = 
{
	IOSFullBoardAd_t1375839176::get_offset_of_m_iOSFullBoardAdPtr_9(),
	IOSFullBoardAd_t1375839176_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (NendUnityFullBoardAdCallback_t900093931), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (IOSInterstitialInterface_t1789666179), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (IOSInterstitialVideoAd_t899463221), -1, sizeof(IOSInterstitialVideoAd_t899463221_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1906[4] = 
{
	IOSInterstitialVideoAd_t899463221::get_offset_of_m_iOSInterstitialVideoAdPtr_10(),
	IOSInterstitialVideoAd_t899463221::get_offset_of_m_selfPtr_11(),
	IOSInterstitialVideoAd_t899463221_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_12(),
	IOSInterstitialVideoAd_t899463221_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (NendUnityVideoAdNormalCallback_t3142830758), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (NendUnityVideoAdErrorCallback_t4190580340), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (IOSLoggerInterface_t172280037), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (IOSNativeAd_t2862447157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1910[1] = 
{
	IOSNativeAd_t2862447157::get_offset_of_m_NativeAdPtr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (IOSNativeAdClient_t1155913684), -1, sizeof(IOSNativeAdClient_t1155913684_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1911[2] = 
{
	IOSNativeAdClient_t1155913684::get_offset_of_m_ClientPtr_6(),
	IOSNativeAdClient_t1155913684_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (NendUnityNativeAdCallback_t2120700280), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (IOSRewardedVideoAd_t2914238892), -1, sizeof(IOSRewardedVideoAd_t2914238892_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1913[5] = 
{
	IOSRewardedVideoAd_t2914238892::get_offset_of_m_iOSRewardedVideoAdPtr_11(),
	IOSRewardedVideoAd_t2914238892::get_offset_of_m_selfPtr_12(),
	IOSRewardedVideoAd_t2914238892_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
	IOSRewardedVideoAd_t2914238892_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_14(),
	IOSRewardedVideoAd_t2914238892_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (NendUnityVideoAdNormalCallback_t3134600846), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (NendUnityVideoAdErrorCallback_t418341462), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (NendUnityRewardedVideoAdCallback_t1320073170), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (IOSUserFeature_t2237018260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[1] = 
{
	IOSUserFeature_t2237018260::get_offset_of_m_iOSUserFeaturePtr_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (SwipeDetector_t548578696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[15] = 
{
	SwipeDetector_t548578696::get_offset_of_comfortZone_2(),
	SwipeDetector_t548578696::get_offset_of_minSwipeDist_3(),
	SwipeDetector_t548578696::get_offset_of_maxSwipeTime_4(),
	SwipeDetector_t548578696::get_offset_of_startTime_5(),
	SwipeDetector_t548578696::get_offset_of_startPos_6(),
	SwipeDetector_t548578696::get_offset_of_couldBeSwipe_7(),
	SwipeDetector_t548578696::get_offset_of_couldBeHorSwipe_8(),
	SwipeDetector_t548578696::get_offset_of_swipeRightCD_9(),
	SwipeDetector_t548578696::get_offset_of_swipeRightTime_10(),
	SwipeDetector_t548578696::get_offset_of_swipeRightTimeReset_11(),
	SwipeDetector_t548578696::get_offset_of_swipeLeftCD_12(),
	SwipeDetector_t548578696::get_offset_of_swipeLeftTime_13(),
	SwipeDetector_t548578696::get_offset_of_swipeLeftTimeReset_14(),
	SwipeDetector_t548578696::get_offset_of_lastSwipe_15(),
	SwipeDetector_t548578696::get_offset_of_lastSwipeTime_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (SwipeDirection_t1609046864)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1919[6] = 
{
	SwipeDirection_t1609046864::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (BackKeyAction_t3328090672)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1920[4] = 
{
	BackKeyAction_t3328090672::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (BackButtonUtil_t2880819819), -1, sizeof(BackButtonUtil_t2880819819_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1921[4] = 
{
	BackButtonUtil_t2880819819::get_offset_of_backKeyAction_2(),
	BackButtonUtil_t2880819819::get_offset_of_nextSceneName_3(),
	BackButtonUtil_t2880819819::get_offset_of_onClickBack_4(),
	BackButtonUtil_t2880819819_StaticFields::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (IpadAspectRation_t3315943056), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (NicerOutline_t1293666038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[4] = 
{
	NicerOutline_t1293666038::get_offset_of_m_EffectColor_3(),
	NicerOutline_t1293666038::get_offset_of_m_EffectDistance_4(),
	NicerOutline_t1293666038::get_offset_of_m_nEffectNumber_5(),
	NicerOutline_t1293666038::get_offset_of_m_UseGraphicAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (SoundButton_t3154830285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[3] = 
{
	SoundButton_t3154830285::get_offset_of_sound_off_2(),
	SoundButton_t3154830285::get_offset_of_sound_on_3(),
	SoundButton_t3154830285::get_offset_of_soundImage_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
