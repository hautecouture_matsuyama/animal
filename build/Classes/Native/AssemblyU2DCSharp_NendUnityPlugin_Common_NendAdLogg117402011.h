﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdLogg918333666.h"

// NendUnityPlugin.Platform.NendAdLoggerInterface
struct NendAdLoggerInterface_t2568798110;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Common.NendAdLogger
struct  NendAdLogger_t117402011  : public Il2CppObject
{
public:

public:
};

struct NendAdLogger_t117402011_StaticFields
{
public:
	// NendUnityPlugin.Common.NendAdLogger/NendAdLogLevel NendUnityPlugin.Common.NendAdLogger::s_LogLevel
	int32_t ___s_LogLevel_0;
	// NendUnityPlugin.Platform.NendAdLoggerInterface NendUnityPlugin.Common.NendAdLogger::_interface
	Il2CppObject * ____interface_1;

public:
	inline static int32_t get_offset_of_s_LogLevel_0() { return static_cast<int32_t>(offsetof(NendAdLogger_t117402011_StaticFields, ___s_LogLevel_0)); }
	inline int32_t get_s_LogLevel_0() const { return ___s_LogLevel_0; }
	inline int32_t* get_address_of_s_LogLevel_0() { return &___s_LogLevel_0; }
	inline void set_s_LogLevel_0(int32_t value)
	{
		___s_LogLevel_0 = value;
	}

	inline static int32_t get_offset_of__interface_1() { return static_cast<int32_t>(offsetof(NendAdLogger_t117402011_StaticFields, ____interface_1)); }
	inline Il2CppObject * get__interface_1() const { return ____interface_1; }
	inline Il2CppObject ** get_address_of__interface_1() { return &____interface_1; }
	inline void set__interface_1(Il2CppObject * value)
	{
		____interface_1 = value;
		Il2CppCodeGenWriteBarrier(&____interface_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
