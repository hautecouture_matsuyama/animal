﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RunObjects
struct  RunObjects_t2121017103  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] RunObjects::objctSprite
	SpriteU5BU5D_t2581906349* ___objctSprite_2;

public:
	inline static int32_t get_offset_of_objctSprite_2() { return static_cast<int32_t>(offsetof(RunObjects_t2121017103, ___objctSprite_2)); }
	inline SpriteU5BU5D_t2581906349* get_objctSprite_2() const { return ___objctSprite_2; }
	inline SpriteU5BU5D_t2581906349** get_address_of_objctSprite_2() { return &___objctSprite_2; }
	inline void set_objctSprite_2(SpriteU5BU5D_t2581906349* value)
	{
		___objctSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___objctSprite_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
