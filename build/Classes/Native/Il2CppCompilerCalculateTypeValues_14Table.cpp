﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GUILayout3503650450.h"
#include "UnityEngine_UnityEngine_GUILayoutOption811797299.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_Type3858932131.h"
#include "UnityEngine_UnityEngine_GUILayoutGroup2157789695.h"
#include "UnityEngine_UnityEngine_GUIScrollGroup1523329021.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry3214611570.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility66395690.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCache78309876.h"
#include "UnityEngine_UnityEngine_GUISettings1774757634.h"
#include "UnityEngine_UnityEngine_GUISkin1244372282.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegat1143955295.h"
#include "UnityEngine_UnityEngine_GUIStyleState1397964415.h"
#include "UnityEngine_UnityEngine_ImagePosition641749504.h"
#include "UnityEngine_UnityEngine_GUIStyle3956901511.h"
#include "UnityEngine_UnityEngine_TextClipping865312958.h"
#include "UnityEngine_UnityEngine_GUITargetAttribute25796337.h"
#include "UnityEngine_UnityEngine_ExitGUIException133215258.h"
#include "UnityEngine_UnityEngine_GUIUtility1868551600.h"
#include "UnityEngine_UnityEngine_ScrollViewState3797911395.h"
#include "UnityEngine_UnityEngine_SliderState2207048770.h"
#include "UnityEngine_UnityEngine_TextEditor2759855366.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin2629979741.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments407707935.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelect978153917.h"
#include "UnityEngine_UnityEngineInternal_WebRequestUtils3541624225.h"
#include "UnityEngine_UnityEngine_RemoteSettings1718627291.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven1027848393.h"
#include "UnityEngine_AOT_MonoPInvokeCallbackAttribute378106515.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine2735742303.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent1422053217.h"
#include "UnityEngine_UnityEngine_RequireComponent3490506609.h"
#include "UnityEngine_UnityEngine_AddComponentMenu415040132.h"
#include "UnityEngine_UnityEngine_ContextMenu1295656858.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3727731349.h"
#include "UnityEngine_UnityEngine_DefaultExecutionOrder3059642329.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttri1946008997.h"
#include "UnityEngine_UnityEngine_NativeClassAttribute2601352714.h"
#include "UnityEngine_UnityEngine_Scripting_GeneratedByOldBin433318409.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3580193095.h"
#include "UnityEngine_UnityEngine_Space654135784.h"
#include "UnityEngine_UnityEngine_RuntimePlatform4159857903.h"
#include "UnityEngine_UnityEngine_OperatingSystemFamily1868066375.h"
#include "UnityEngine_UnityEngine_LogType73765434.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_Color322600501292.h"
#include "UnityEngine_UnityEngine_SetupCoroutine2062820429.h"
#include "UnityEngine_UnityEngine_WritableAttribute812406054.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly3442416807.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2719720026.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_643925653.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_675222246.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2125309831.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2362496923.h"
#include "UnityEngine_UnityEngine_TextureWrapMode584250749.h"
#include "UnityEngine_UnityEngine_TextureFormat2701165832.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction2171731108.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask4282245599.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp3446174106.h"
#include "UnityEngine_UnityEngine_KeyCode2599294277.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalU365094499.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP3137328177.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev565359984.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie3217594527.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score1968645328.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1065076763.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal624072491.h"
#include "UnityEngine_UnityEngineInternal_ScriptingUtils2624832893.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3273302915.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3229609740.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState4177058321.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope604006431.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope539351503.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range173988048.h"
#include "UnityEngine_UnityEngine_Plane1000493321.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3677895545.h"
#include "UnityEngine_UnityEngine_TooltipAttribute3957072629.h"
#include "UnityEngine_UnityEngine_SpaceAttribute3956583069.h"
#include "UnityEngine_UnityEngine_HeaderAttribute618189647.h"
#include "UnityEngine_UnityEngine_RangeAttribute3337244227.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute3326046611.h"
#include "UnityEngine_UnityEngine_RangeInt2094684618.h"
#include "UnityEngine_UnityEngine_Ray3785851493.h"
#include "UnityEngine_UnityEngine_Rect2360479859.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute3493465804.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables3872960625.h"
#include "UnityEngine_UnityEngine_SerializeField3286833614.h"
#include "UnityEngine_UnityEngine_PreferBinarySerialization2906007930.h"
#include "UnityEngine_UnityEngine_StackTraceUtility3465565809.h"
#include "UnityEngine_UnityEngine_UnityException3598173660.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType1530597702.h"
#include "UnityEngine_UnityEngine_TrackedReference1199777556.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1400 = { sizeof (GUILayout_t3503650450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1401 = { sizeof (GUILayoutOption_t811797299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1401[2] = 
{
	GUILayoutOption_t811797299::get_offset_of_type_0(),
	GUILayoutOption_t811797299::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1402 = { sizeof (Type_t3858932131)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1402[15] = 
{
	Type_t3858932131::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1403 = { sizeof (GUILayoutGroup_t2157789695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1403[17] = 
{
	GUILayoutGroup_t2157789695::get_offset_of_entries_10(),
	GUILayoutGroup_t2157789695::get_offset_of_isVertical_11(),
	GUILayoutGroup_t2157789695::get_offset_of_resetCoords_12(),
	GUILayoutGroup_t2157789695::get_offset_of_spacing_13(),
	GUILayoutGroup_t2157789695::get_offset_of_sameSize_14(),
	GUILayoutGroup_t2157789695::get_offset_of_isWindow_15(),
	GUILayoutGroup_t2157789695::get_offset_of_windowID_16(),
	GUILayoutGroup_t2157789695::get_offset_of_m_Cursor_17(),
	GUILayoutGroup_t2157789695::get_offset_of_m_StretchableCountX_18(),
	GUILayoutGroup_t2157789695::get_offset_of_m_StretchableCountY_19(),
	GUILayoutGroup_t2157789695::get_offset_of_m_UserSpecifiedWidth_20(),
	GUILayoutGroup_t2157789695::get_offset_of_m_UserSpecifiedHeight_21(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMinWidth_22(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMaxWidth_23(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMinHeight_24(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMaxHeight_25(),
	GUILayoutGroup_t2157789695::get_offset_of_m_Margin_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1404 = { sizeof (GUIScrollGroup_t1523329021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1404[12] = 
{
	GUIScrollGroup_t1523329021::get_offset_of_calcMinWidth_27(),
	GUIScrollGroup_t1523329021::get_offset_of_calcMaxWidth_28(),
	GUIScrollGroup_t1523329021::get_offset_of_calcMinHeight_29(),
	GUIScrollGroup_t1523329021::get_offset_of_calcMaxHeight_30(),
	GUIScrollGroup_t1523329021::get_offset_of_clientWidth_31(),
	GUIScrollGroup_t1523329021::get_offset_of_clientHeight_32(),
	GUIScrollGroup_t1523329021::get_offset_of_allowHorizontalScroll_33(),
	GUIScrollGroup_t1523329021::get_offset_of_allowVerticalScroll_34(),
	GUIScrollGroup_t1523329021::get_offset_of_needsHorizontalScrollbar_35(),
	GUIScrollGroup_t1523329021::get_offset_of_needsVerticalScrollbar_36(),
	GUIScrollGroup_t1523329021::get_offset_of_horizontalScrollbar_37(),
	GUIScrollGroup_t1523329021::get_offset_of_verticalScrollbar_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1405 = { sizeof (GUILayoutEntry_t3214611570), -1, sizeof(GUILayoutEntry_t3214611570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1405[10] = 
{
	GUILayoutEntry_t3214611570::get_offset_of_minWidth_0(),
	GUILayoutEntry_t3214611570::get_offset_of_maxWidth_1(),
	GUILayoutEntry_t3214611570::get_offset_of_minHeight_2(),
	GUILayoutEntry_t3214611570::get_offset_of_maxHeight_3(),
	GUILayoutEntry_t3214611570::get_offset_of_rect_4(),
	GUILayoutEntry_t3214611570::get_offset_of_stretchWidth_5(),
	GUILayoutEntry_t3214611570::get_offset_of_stretchHeight_6(),
	GUILayoutEntry_t3214611570::get_offset_of_m_Style_7(),
	GUILayoutEntry_t3214611570_StaticFields::get_offset_of_kDummyRect_8(),
	GUILayoutEntry_t3214611570_StaticFields::get_offset_of_indent_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1406 = { sizeof (GUILayoutUtility_t66395690), -1, sizeof(GUILayoutUtility_t66395690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1406[5] = 
{
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_s_StoredLayouts_0(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_s_StoredWindows_1(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_current_2(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_kDummyRect_3(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_s_SpaceStyle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1407 = { sizeof (LayoutCache_t78309876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1407[3] = 
{
	LayoutCache_t78309876::get_offset_of_topLevel_0(),
	LayoutCache_t78309876::get_offset_of_layoutGroups_1(),
	LayoutCache_t78309876::get_offset_of_windows_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1408 = { sizeof (GUISettings_t1774757634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1408[5] = 
{
	GUISettings_t1774757634::get_offset_of_m_DoubleClickSelectsWord_0(),
	GUISettings_t1774757634::get_offset_of_m_TripleClickSelectsLine_1(),
	GUISettings_t1774757634::get_offset_of_m_CursorColor_2(),
	GUISettings_t1774757634::get_offset_of_m_CursorFlashSpeed_3(),
	GUISettings_t1774757634::get_offset_of_m_SelectionColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1409 = { sizeof (GUISkin_t1244372282), -1, sizeof(GUISkin_t1244372282_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1409[27] = 
{
	GUISkin_t1244372282::get_offset_of_m_Font_2(),
	GUISkin_t1244372282::get_offset_of_m_box_3(),
	GUISkin_t1244372282::get_offset_of_m_button_4(),
	GUISkin_t1244372282::get_offset_of_m_toggle_5(),
	GUISkin_t1244372282::get_offset_of_m_label_6(),
	GUISkin_t1244372282::get_offset_of_m_textField_7(),
	GUISkin_t1244372282::get_offset_of_m_textArea_8(),
	GUISkin_t1244372282::get_offset_of_m_window_9(),
	GUISkin_t1244372282::get_offset_of_m_horizontalSlider_10(),
	GUISkin_t1244372282::get_offset_of_m_horizontalSliderThumb_11(),
	GUISkin_t1244372282::get_offset_of_m_verticalSlider_12(),
	GUISkin_t1244372282::get_offset_of_m_verticalSliderThumb_13(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbar_14(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbarThumb_15(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbarLeftButton_16(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbarRightButton_17(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbar_18(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbarThumb_19(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbarUpButton_20(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbarDownButton_21(),
	GUISkin_t1244372282::get_offset_of_m_ScrollView_22(),
	GUISkin_t1244372282::get_offset_of_m_CustomStyles_23(),
	GUISkin_t1244372282::get_offset_of_m_Settings_24(),
	GUISkin_t1244372282_StaticFields::get_offset_of_ms_Error_25(),
	GUISkin_t1244372282::get_offset_of_m_Styles_26(),
	GUISkin_t1244372282_StaticFields::get_offset_of_m_SkinChanged_27(),
	GUISkin_t1244372282_StaticFields::get_offset_of_current_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1410 = { sizeof (SkinChangedDelegate_t1143955295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1411 = { sizeof (GUIStyleState_t1397964415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1411[3] = 
{
	GUIStyleState_t1397964415::get_offset_of_m_Ptr_0(),
	GUIStyleState_t1397964415::get_offset_of_m_SourceStyle_1(),
	GUIStyleState_t1397964415::get_offset_of_m_Background_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1412 = { sizeof (ImagePosition_t641749504)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1412[5] = 
{
	ImagePosition_t641749504::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1413 = { sizeof (GUIStyle_t3956901511), -1, sizeof(GUIStyle_t3956901511_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1413[16] = 
{
	GUIStyle_t3956901511::get_offset_of_m_Ptr_0(),
	GUIStyle_t3956901511::get_offset_of_m_Normal_1(),
	GUIStyle_t3956901511::get_offset_of_m_Hover_2(),
	GUIStyle_t3956901511::get_offset_of_m_Active_3(),
	GUIStyle_t3956901511::get_offset_of_m_Focused_4(),
	GUIStyle_t3956901511::get_offset_of_m_OnNormal_5(),
	GUIStyle_t3956901511::get_offset_of_m_OnHover_6(),
	GUIStyle_t3956901511::get_offset_of_m_OnActive_7(),
	GUIStyle_t3956901511::get_offset_of_m_OnFocused_8(),
	GUIStyle_t3956901511::get_offset_of_m_Border_9(),
	GUIStyle_t3956901511::get_offset_of_m_Padding_10(),
	GUIStyle_t3956901511::get_offset_of_m_Margin_11(),
	GUIStyle_t3956901511::get_offset_of_m_Overflow_12(),
	GUIStyle_t3956901511::get_offset_of_m_FontInternal_13(),
	GUIStyle_t3956901511_StaticFields::get_offset_of_showKeyboardFocus_14(),
	GUIStyle_t3956901511_StaticFields::get_offset_of_s_None_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1414 = { sizeof (TextClipping_t865312958)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1414[3] = 
{
	TextClipping_t865312958::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1415 = { sizeof (GUITargetAttribute_t25796337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1415[1] = 
{
	GUITargetAttribute_t25796337::get_offset_of_displayMask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1416 = { sizeof (ExitGUIException_t133215258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1417 = { sizeof (GUIUtility_t1868551600), -1, sizeof(GUIUtility_t1868551600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1417[4] = 
{
	GUIUtility_t1868551600_StaticFields::get_offset_of_s_SkinMode_0(),
	GUIUtility_t1868551600_StaticFields::get_offset_of_s_OriginalID_1(),
	GUIUtility_t1868551600_StaticFields::get_offset_of_U3CguiIsExitingU3Ek__BackingField_2(),
	GUIUtility_t1868551600_StaticFields::get_offset_of_s_EditorScreenPointOffset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1418 = { sizeof (ScrollViewState_t3797911395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1419 = { sizeof (SliderState_t2207048770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1419[3] = 
{
	SliderState_t2207048770::get_offset_of_dragStartPos_0(),
	SliderState_t2207048770::get_offset_of_dragStartValue_1(),
	SliderState_t2207048770::get_offset_of_isDragging_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1420 = { sizeof (TextEditor_t2759855366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1420[16] = 
{
	TextEditor_t2759855366::get_offset_of_keyboardOnScreen_0(),
	TextEditor_t2759855366::get_offset_of_controlID_1(),
	TextEditor_t2759855366::get_offset_of_style_2(),
	TextEditor_t2759855366::get_offset_of_multiline_3(),
	TextEditor_t2759855366::get_offset_of_hasHorizontalCursorPos_4(),
	TextEditor_t2759855366::get_offset_of_isPasswordField_5(),
	TextEditor_t2759855366::get_offset_of_scrollOffset_6(),
	TextEditor_t2759855366::get_offset_of_m_Content_7(),
	TextEditor_t2759855366::get_offset_of_m_CursorIndex_8(),
	TextEditor_t2759855366::get_offset_of_m_SelectIndex_9(),
	TextEditor_t2759855366::get_offset_of_m_RevealCursor_10(),
	TextEditor_t2759855366::get_offset_of_m_MouseDragSelectsWholeWords_11(),
	TextEditor_t2759855366::get_offset_of_m_DblClickInitPos_12(),
	TextEditor_t2759855366::get_offset_of_m_DblClickSnap_13(),
	TextEditor_t2759855366::get_offset_of_m_bJustSelected_14(),
	TextEditor_t2759855366::get_offset_of_m_iAltCursorPos_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1421 = { sizeof (DblClickSnapping_t2629979741)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1421[3] = 
{
	DblClickSnapping_t2629979741::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1422 = { sizeof (Internal_DrawArguments_t407707935)+ sizeof (Il2CppObject), sizeof(Internal_DrawArguments_t407707935 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1422[6] = 
{
	Internal_DrawArguments_t407707935::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_isHover_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_isActive_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_on_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_hasKeyboardFocus_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1423 = { sizeof (Internal_DrawWithTextSelectionArguments_t978153917)+ sizeof (Il2CppObject), sizeof(Internal_DrawWithTextSelectionArguments_t978153917 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1423[11] = 
{
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_firstPos_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_lastPos_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_cursorColor_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_selectionColor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_isHover_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_isActive_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_on_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_hasKeyboardFocus_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_drawSelectionAsComposition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1424 = { sizeof (WebRequestUtils_t3541624225), -1, sizeof(WebRequestUtils_t3541624225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1424[1] = 
{
	WebRequestUtils_t3541624225_StaticFields::get_offset_of_domainRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1425 = { sizeof (RemoteSettings_t1718627291), -1, sizeof(RemoteSettings_t1718627291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1425[1] = 
{
	RemoteSettings_t1718627291_StaticFields::get_offset_of_Updated_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1426 = { sizeof (UpdatedEventHandler_t1027848393), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1427 = { sizeof (MonoPInvokeCallbackAttribute_t378106515), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1428 = { sizeof (AttributeHelperEngine_t2735742303), -1, sizeof(AttributeHelperEngine_t2735742303_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1428[3] = 
{
	AttributeHelperEngine_t2735742303_StaticFields::get_offset_of__disallowMultipleComponentArray_0(),
	AttributeHelperEngine_t2735742303_StaticFields::get_offset_of__executeInEditModeArray_1(),
	AttributeHelperEngine_t2735742303_StaticFields::get_offset_of__requireComponentArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1429 = { sizeof (DisallowMultipleComponent_t1422053217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1430 = { sizeof (RequireComponent_t3490506609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1430[3] = 
{
	RequireComponent_t3490506609::get_offset_of_m_Type0_0(),
	RequireComponent_t3490506609::get_offset_of_m_Type1_1(),
	RequireComponent_t3490506609::get_offset_of_m_Type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1431 = { sizeof (AddComponentMenu_t415040132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1431[2] = 
{
	AddComponentMenu_t415040132::get_offset_of_m_AddComponentMenu_0(),
	AddComponentMenu_t415040132::get_offset_of_m_Ordering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1432 = { sizeof (ContextMenu_t1295656858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1432[3] = 
{
	ContextMenu_t1295656858::get_offset_of_menuItem_0(),
	ContextMenu_t1295656858::get_offset_of_validate_1(),
	ContextMenu_t1295656858::get_offset_of_priority_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1433 = { sizeof (ExecuteInEditMode_t3727731349), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1434 = { sizeof (DefaultExecutionOrder_t3059642329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1434[1] = 
{
	DefaultExecutionOrder_t3059642329::get_offset_of_U3CorderU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1435 = { sizeof (IL2CPPStructAlignmentAttribute_t1946008997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1435[1] = 
{
	IL2CPPStructAlignmentAttribute_t1946008997::get_offset_of_Align_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1436 = { sizeof (NativeClassAttribute_t2601352714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1436[1] = 
{
	NativeClassAttribute_t2601352714::get_offset_of_U3CQualifiedNativeNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1437 = { sizeof (GeneratedByOldBindingsGeneratorAttribute_t433318409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1438 = { sizeof (SendMessageOptions_t3580193095)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1438[3] = 
{
	SendMessageOptions_t3580193095::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1439 = { sizeof (Space_t654135784)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1439[3] = 
{
	Space_t654135784::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1440 = { sizeof (RuntimePlatform_t4159857903)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1440[34] = 
{
	RuntimePlatform_t4159857903::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1441 = { sizeof (OperatingSystemFamily_t1868066375)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1441[5] = 
{
	OperatingSystemFamily_t1868066375::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1442 = { sizeof (LogType_t73765434)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1442[6] = 
{
	LogType_t73765434::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1443 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1443[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1444 = { sizeof (Color_t2555686324)+ sizeof (Il2CppObject), sizeof(Color_t2555686324 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1444[4] = 
{
	Color_t2555686324::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2555686324::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2555686324::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2555686324::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1445 = { sizeof (Color32_t2600501292)+ sizeof (Il2CppObject), sizeof(Color32_t2600501292 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1445[4] = 
{
	Color32_t2600501292::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t2600501292::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t2600501292::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t2600501292::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1446 = { sizeof (SetupCoroutine_t2062820429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1447 = { sizeof (WritableAttribute_t812406054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1448 = { sizeof (AssemblyIsEditorAssembly_t3442416807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1449 = { sizeof (GcUserProfileData_t2719720026)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1449[4] = 
{
	GcUserProfileData_t2719720026::get_offset_of_userName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t2719720026::get_offset_of_userID_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t2719720026::get_offset_of_isFriend_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t2719720026::get_offset_of_image_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1450 = { sizeof (GcAchievementDescriptionData_t643925653)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1450[7] = 
{
	GcAchievementDescriptionData_t643925653::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_Title_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_Image_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_AchievedDescription_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_UnachievedDescription_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_Hidden_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_Points_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1451 = { sizeof (GcAchievementData_t675222246)+ sizeof (Il2CppObject), sizeof(GcAchievementData_t675222246_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1451[5] = 
{
	GcAchievementData_t675222246::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t675222246::get_offset_of_m_PercentCompleted_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t675222246::get_offset_of_m_Completed_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t675222246::get_offset_of_m_Hidden_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t675222246::get_offset_of_m_LastReportedDate_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1452 = { sizeof (GcScoreData_t2125309831)+ sizeof (Il2CppObject), sizeof(GcScoreData_t2125309831_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1452[7] = 
{
	GcScoreData_t2125309831::get_offset_of_m_Category_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_ValueLow_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_ValueHigh_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_Date_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_FormattedValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_PlayerID_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_Rank_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1453 = { sizeof (CameraClearFlags_t2362496923)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1453[6] = 
{
	CameraClearFlags_t2362496923::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1454 = { sizeof (TextureWrapMode_t584250749)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1454[3] = 
{
	TextureWrapMode_t584250749::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1455 = { sizeof (TextureFormat_t2701165832)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1455[54] = 
{
	TextureFormat_t2701165832::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1456 = { sizeof (CompareFunction_t2171731108)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1456[10] = 
{
	CompareFunction_t2171731108::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1457 = { sizeof (ColorWriteMask_t4282245599)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1457[6] = 
{
	ColorWriteMask_t4282245599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1458 = { sizeof (StencilOp_t3446174106)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1458[9] = 
{
	StencilOp_t3446174106::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1459 = { sizeof (KeyCode_t2599294277)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1459[322] = 
{
	KeyCode_t2599294277::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1460 = { sizeof (LocalUser_t365094499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1460[3] = 
{
	LocalUser_t365094499::get_offset_of_m_Friends_5(),
	LocalUser_t365094499::get_offset_of_m_Authenticated_6(),
	LocalUser_t365094499::get_offset_of_m_Underage_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1461 = { sizeof (UserProfile_t3137328177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1461[5] = 
{
	UserProfile_t3137328177::get_offset_of_m_UserName_0(),
	UserProfile_t3137328177::get_offset_of_m_ID_1(),
	UserProfile_t3137328177::get_offset_of_m_IsFriend_2(),
	UserProfile_t3137328177::get_offset_of_m_State_3(),
	UserProfile_t3137328177::get_offset_of_m_Image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1462 = { sizeof (Achievement_t565359984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1462[5] = 
{
	Achievement_t565359984::get_offset_of_m_Completed_0(),
	Achievement_t565359984::get_offset_of_m_Hidden_1(),
	Achievement_t565359984::get_offset_of_m_LastReportedDate_2(),
	Achievement_t565359984::get_offset_of_U3CidU3Ek__BackingField_3(),
	Achievement_t565359984::get_offset_of_U3CpercentCompletedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1463 = { sizeof (AchievementDescription_t3217594527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1463[7] = 
{
	AchievementDescription_t3217594527::get_offset_of_m_Title_0(),
	AchievementDescription_t3217594527::get_offset_of_m_Image_1(),
	AchievementDescription_t3217594527::get_offset_of_m_AchievedDescription_2(),
	AchievementDescription_t3217594527::get_offset_of_m_UnachievedDescription_3(),
	AchievementDescription_t3217594527::get_offset_of_m_Hidden_4(),
	AchievementDescription_t3217594527::get_offset_of_m_Points_5(),
	AchievementDescription_t3217594527::get_offset_of_U3CidU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1464 = { sizeof (Score_t1968645328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1464[6] = 
{
	Score_t1968645328::get_offset_of_m_Date_0(),
	Score_t1968645328::get_offset_of_m_FormattedValue_1(),
	Score_t1968645328::get_offset_of_m_UserID_2(),
	Score_t1968645328::get_offset_of_m_Rank_3(),
	Score_t1968645328::get_offset_of_U3CleaderboardIDU3Ek__BackingField_4(),
	Score_t1968645328::get_offset_of_U3CvalueU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1465 = { sizeof (Leaderboard_t1065076763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1465[10] = 
{
	Leaderboard_t1065076763::get_offset_of_m_Loading_0(),
	Leaderboard_t1065076763::get_offset_of_m_LocalUserScore_1(),
	Leaderboard_t1065076763::get_offset_of_m_MaxRange_2(),
	Leaderboard_t1065076763::get_offset_of_m_Scores_3(),
	Leaderboard_t1065076763::get_offset_of_m_Title_4(),
	Leaderboard_t1065076763::get_offset_of_m_UserIDs_5(),
	Leaderboard_t1065076763::get_offset_of_U3CidU3Ek__BackingField_6(),
	Leaderboard_t1065076763::get_offset_of_U3CuserScopeU3Ek__BackingField_7(),
	Leaderboard_t1065076763::get_offset_of_U3CrangeU3Ek__BackingField_8(),
	Leaderboard_t1065076763::get_offset_of_U3CtimeScopeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1466 = { sizeof (MathfInternal_t624072491)+ sizeof (Il2CppObject), sizeof(MathfInternal_t624072491 ), sizeof(MathfInternal_t624072491_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1466[3] = 
{
	MathfInternal_t624072491_StaticFields::get_offset_of_FloatMinNormal_0(),
	MathfInternal_t624072491_StaticFields::get_offset_of_FloatMinDenormal_1(),
	MathfInternal_t624072491_StaticFields::get_offset_of_IsFlushToZeroEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1467 = { sizeof (ScriptingUtils_t2624832893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1468 = { sizeof (SendMouseEvents_t3273302915), -1, sizeof(SendMouseEvents_t3273302915_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1468[5] = 
{
	SendMouseEvents_t3273302915_StaticFields::get_offset_of_s_MouseUsed_0(),
	SendMouseEvents_t3273302915_StaticFields::get_offset_of_m_LastHit_1(),
	SendMouseEvents_t3273302915_StaticFields::get_offset_of_m_MouseDownHit_2(),
	SendMouseEvents_t3273302915_StaticFields::get_offset_of_m_CurrentHit_3(),
	SendMouseEvents_t3273302915_StaticFields::get_offset_of_m_Cameras_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1469 = { sizeof (HitInfo_t3229609740)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1469[2] = 
{
	HitInfo_t3229609740::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HitInfo_t3229609740::get_offset_of_camera_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1470 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1471 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1472 = { sizeof (UserState_t4177058321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1472[6] = 
{
	UserState_t4177058321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1473 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1474 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1475 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1476 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1477 = { sizeof (UserScope_t604006431)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1477[3] = 
{
	UserScope_t604006431::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1478 = { sizeof (TimeScope_t539351503)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1478[4] = 
{
	TimeScope_t539351503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1479 = { sizeof (Range_t173988048)+ sizeof (Il2CppObject), sizeof(Range_t173988048 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1479[2] = 
{
	Range_t173988048::get_offset_of_from_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Range_t173988048::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1480 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1481 = { sizeof (Plane_t1000493321)+ sizeof (Il2CppObject), sizeof(Plane_t1000493321 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1481[2] = 
{
	Plane_t1000493321::get_offset_of_m_Normal_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Plane_t1000493321::get_offset_of_m_Distance_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1482 = { sizeof (PropertyAttribute_t3677895545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1483 = { sizeof (TooltipAttribute_t3957072629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1483[1] = 
{
	TooltipAttribute_t3957072629::get_offset_of_tooltip_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1484 = { sizeof (SpaceAttribute_t3956583069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1484[1] = 
{
	SpaceAttribute_t3956583069::get_offset_of_height_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1485 = { sizeof (HeaderAttribute_t618189647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1485[1] = 
{
	HeaderAttribute_t618189647::get_offset_of_header_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1486 = { sizeof (RangeAttribute_t3337244227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1486[2] = 
{
	RangeAttribute_t3337244227::get_offset_of_min_0(),
	RangeAttribute_t3337244227::get_offset_of_max_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1487 = { sizeof (TextAreaAttribute_t3326046611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1487[2] = 
{
	TextAreaAttribute_t3326046611::get_offset_of_minLines_0(),
	TextAreaAttribute_t3326046611::get_offset_of_maxLines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1488 = { sizeof (RangeInt_t2094684618)+ sizeof (Il2CppObject), sizeof(RangeInt_t2094684618 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1488[2] = 
{
	RangeInt_t2094684618::get_offset_of_start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RangeInt_t2094684618::get_offset_of_length_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1489 = { sizeof (Ray_t3785851493)+ sizeof (Il2CppObject), sizeof(Ray_t3785851493 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1489[2] = 
{
	Ray_t3785851493::get_offset_of_m_Origin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Ray_t3785851493::get_offset_of_m_Direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1490 = { sizeof (Rect_t2360479859)+ sizeof (Il2CppObject), sizeof(Rect_t2360479859 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1490[4] = 
{
	Rect_t2360479859::get_offset_of_m_XMin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t2360479859::get_offset_of_m_YMin_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t2360479859::get_offset_of_m_Width_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t2360479859::get_offset_of_m_Height_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1491 = { sizeof (SelectionBaseAttribute_t3493465804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1492 = { sizeof (SerializePrivateVariables_t3872960625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1493 = { sizeof (SerializeField_t3286833614), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1494 = { sizeof (PreferBinarySerialization_t2906007930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1495 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1496 = { sizeof (StackTraceUtility_t3465565809), -1, sizeof(StackTraceUtility_t3465565809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1496[1] = 
{
	StackTraceUtility_t3465565809_StaticFields::get_offset_of_projectFolder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1497 = { sizeof (UnityException_t3598173660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1497[2] = 
{
	0,
	UnityException_t3598173660::get_offset_of_unityStackTrace_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1498 = { sizeof (TouchScreenKeyboardType_t1530597702)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1498[10] = 
{
	TouchScreenKeyboardType_t1530597702::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1499 = { sizeof (TrackedReference_t1199777556), sizeof(TrackedReference_t1199777556_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1499[1] = 
{
	TrackedReference_t1199777556::get_offset_of_m_Ptr_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
