﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Score
struct  Score_t2516855617  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Score_t2516855617_StaticFields
{
public:
	// System.Int32 Score::scoreNum
	int32_t ___scoreNum_2;
	// System.Int32 Score::bestNum
	int32_t ___bestNum_3;

public:
	inline static int32_t get_offset_of_scoreNum_2() { return static_cast<int32_t>(offsetof(Score_t2516855617_StaticFields, ___scoreNum_2)); }
	inline int32_t get_scoreNum_2() const { return ___scoreNum_2; }
	inline int32_t* get_address_of_scoreNum_2() { return &___scoreNum_2; }
	inline void set_scoreNum_2(int32_t value)
	{
		___scoreNum_2 = value;
	}

	inline static int32_t get_offset_of_bestNum_3() { return static_cast<int32_t>(offsetof(Score_t2516855617_StaticFields, ___bestNum_3)); }
	inline int32_t get_bestNum_3() const { return ___bestNum_3; }
	inline int32_t* get_address_of_bestNum_3() { return &___bestNum_3; }
	inline void set_bestNum_3(int32_t value)
	{
		___bestNum_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
