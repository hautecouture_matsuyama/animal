﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.WWW
struct WWW_t3688466362;
// HCAnimatedBanner
struct HCAnimatedBanner_t2921968851;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HCAnimatedBanner/<LoadTexture>c__Iterator1
struct  U3CLoadTextureU3Ec__Iterator1_t1051594053  : public Il2CppObject
{
public:
	// System.Int32 HCAnimatedBanner/<LoadTexture>c__Iterator1::<index>__0
	int32_t ___U3CindexU3E__0_0;
	// System.Int32 HCAnimatedBanner/<LoadTexture>c__Iterator1::<textureWidth>__0
	int32_t ___U3CtextureWidthU3E__0_1;
	// System.Int32 HCAnimatedBanner/<LoadTexture>c__Iterator1::<textureHeight>__0
	int32_t ___U3CtextureHeightU3E__0_2;
	// UnityEngine.Texture2D HCAnimatedBanner/<LoadTexture>c__Iterator1::<texture>__0
	Texture2D_t3840446185 * ___U3CtextureU3E__0_3;
	// UnityEngine.WWW HCAnimatedBanner/<LoadTexture>c__Iterator1::<www>__1
	WWW_t3688466362 * ___U3CwwwU3E__1_4;
	// HCAnimatedBanner HCAnimatedBanner/<LoadTexture>c__Iterator1::$this
	HCAnimatedBanner_t2921968851 * ___U24this_5;
	// System.Object HCAnimatedBanner/<LoadTexture>c__Iterator1::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean HCAnimatedBanner/<LoadTexture>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 HCAnimatedBanner/<LoadTexture>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CindexU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator1_t1051594053, ___U3CindexU3E__0_0)); }
	inline int32_t get_U3CindexU3E__0_0() const { return ___U3CindexU3E__0_0; }
	inline int32_t* get_address_of_U3CindexU3E__0_0() { return &___U3CindexU3E__0_0; }
	inline void set_U3CindexU3E__0_0(int32_t value)
	{
		___U3CindexU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtextureWidthU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator1_t1051594053, ___U3CtextureWidthU3E__0_1)); }
	inline int32_t get_U3CtextureWidthU3E__0_1() const { return ___U3CtextureWidthU3E__0_1; }
	inline int32_t* get_address_of_U3CtextureWidthU3E__0_1() { return &___U3CtextureWidthU3E__0_1; }
	inline void set_U3CtextureWidthU3E__0_1(int32_t value)
	{
		___U3CtextureWidthU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtextureHeightU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator1_t1051594053, ___U3CtextureHeightU3E__0_2)); }
	inline int32_t get_U3CtextureHeightU3E__0_2() const { return ___U3CtextureHeightU3E__0_2; }
	inline int32_t* get_address_of_U3CtextureHeightU3E__0_2() { return &___U3CtextureHeightU3E__0_2; }
	inline void set_U3CtextureHeightU3E__0_2(int32_t value)
	{
		___U3CtextureHeightU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CtextureU3E__0_3() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator1_t1051594053, ___U3CtextureU3E__0_3)); }
	inline Texture2D_t3840446185 * get_U3CtextureU3E__0_3() const { return ___U3CtextureU3E__0_3; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtextureU3E__0_3() { return &___U3CtextureU3E__0_3; }
	inline void set_U3CtextureU3E__0_3(Texture2D_t3840446185 * value)
	{
		___U3CtextureU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtextureU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_4() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator1_t1051594053, ___U3CwwwU3E__1_4)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__1_4() const { return ___U3CwwwU3E__1_4; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__1_4() { return &___U3CwwwU3E__1_4; }
	inline void set_U3CwwwU3E__1_4(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__1_4, value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator1_t1051594053, ___U24this_5)); }
	inline HCAnimatedBanner_t2921968851 * get_U24this_5() const { return ___U24this_5; }
	inline HCAnimatedBanner_t2921968851 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(HCAnimatedBanner_t2921968851 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator1_t1051594053, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator1_t1051594053, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CLoadTextureU3Ec__Iterator1_t1051594053, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
