﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.Action
struct Action_t1264377477;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.Handlers.ImpressionHandler
struct  ImpressionHandler_t4278309981  : public MonoBehaviour_t3962482529
{
public:
	// System.Single NendUnityPlugin.AD.Native.Handlers.ImpressionHandler::m_TimeElapsed
	float ___m_TimeElapsed_4;
	// System.Action NendUnityPlugin.AD.Native.Handlers.ImpressionHandler::OnImpression
	Action_t1264377477 * ___OnImpression_5;

public:
	inline static int32_t get_offset_of_m_TimeElapsed_4() { return static_cast<int32_t>(offsetof(ImpressionHandler_t4278309981, ___m_TimeElapsed_4)); }
	inline float get_m_TimeElapsed_4() const { return ___m_TimeElapsed_4; }
	inline float* get_address_of_m_TimeElapsed_4() { return &___m_TimeElapsed_4; }
	inline void set_m_TimeElapsed_4(float value)
	{
		___m_TimeElapsed_4 = value;
	}

	inline static int32_t get_offset_of_OnImpression_5() { return static_cast<int32_t>(offsetof(ImpressionHandler_t4278309981, ___OnImpression_5)); }
	inline Action_t1264377477 * get_OnImpression_5() const { return ___OnImpression_5; }
	inline Action_t1264377477 ** get_address_of_OnImpression_5() { return &___OnImpression_5; }
	inline void set_OnImpression_5(Action_t1264377477 * value)
	{
		___OnImpression_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnImpression_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
