﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.EventHandler
struct EventHandler_t1348719766;
// System.Collections.Generic.HashSet`1<UnityEngine.MonoBehaviour>
struct HashSet_1_t2527432003;
// System.Func`2<UnityEngine.MonoBehaviour,System.Boolean>
struct Func_2_t2922669422;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.NativeAd
struct  NativeAd_t775405424  : public Il2CppObject
{
public:
	// System.EventHandler NendUnityPlugin.AD.Native.NativeAd::AdClicked
	EventHandler_t1348719766 * ___AdClicked_0;
	// System.Boolean NendUnityPlugin.AD.Native.NativeAd::m_IsImpression
	bool ___m_IsImpression_1;
	// System.Collections.Generic.HashSet`1<UnityEngine.MonoBehaviour> NendUnityPlugin.AD.Native.NativeAd::m_Handlers
	HashSet_1_t2527432003 * ___m_Handlers_2;

public:
	inline static int32_t get_offset_of_AdClicked_0() { return static_cast<int32_t>(offsetof(NativeAd_t775405424, ___AdClicked_0)); }
	inline EventHandler_t1348719766 * get_AdClicked_0() const { return ___AdClicked_0; }
	inline EventHandler_t1348719766 ** get_address_of_AdClicked_0() { return &___AdClicked_0; }
	inline void set_AdClicked_0(EventHandler_t1348719766 * value)
	{
		___AdClicked_0 = value;
		Il2CppCodeGenWriteBarrier(&___AdClicked_0, value);
	}

	inline static int32_t get_offset_of_m_IsImpression_1() { return static_cast<int32_t>(offsetof(NativeAd_t775405424, ___m_IsImpression_1)); }
	inline bool get_m_IsImpression_1() const { return ___m_IsImpression_1; }
	inline bool* get_address_of_m_IsImpression_1() { return &___m_IsImpression_1; }
	inline void set_m_IsImpression_1(bool value)
	{
		___m_IsImpression_1 = value;
	}

	inline static int32_t get_offset_of_m_Handlers_2() { return static_cast<int32_t>(offsetof(NativeAd_t775405424, ___m_Handlers_2)); }
	inline HashSet_1_t2527432003 * get_m_Handlers_2() const { return ___m_Handlers_2; }
	inline HashSet_1_t2527432003 ** get_address_of_m_Handlers_2() { return &___m_Handlers_2; }
	inline void set_m_Handlers_2(HashSet_1_t2527432003 * value)
	{
		___m_Handlers_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Handlers_2, value);
	}
};

struct NativeAd_t775405424_StaticFields
{
public:
	// System.Func`2<UnityEngine.MonoBehaviour,System.Boolean> NendUnityPlugin.AD.Native.NativeAd::<>f__am$cache0
	Func_2_t2922669422 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(NativeAd_t775405424_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t2922669422 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t2922669422 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t2922669422 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
