﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Advert2175459971.h"

// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// NendUnityPlugin.AD.Native.Events.NativeAdViewEvent
struct NativeAdViewEvent_t610464567;
// NendUnityPlugin.AD.Native.INativeAd
struct INativeAd_t3252869353;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.NendAdNativeView
struct  NendAdNativeView_t2014609480  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 NendUnityPlugin.AD.Native.NendAdNativeView::viewTag
	int32_t ___viewTag_2;
	// System.Boolean NendUnityPlugin.AD.Native.NendAdNativeView::renderWhenLoaded
	bool ___renderWhenLoaded_3;
	// System.Boolean NendUnityPlugin.AD.Native.NendAdNativeView::renderWhenActivated
	bool ___renderWhenActivated_4;
	// NendUnityPlugin.AD.Native.AdvertisingExplicitly NendUnityPlugin.AD.Native.NendAdNativeView::advertisingExplicitly
	int32_t ___advertisingExplicitly_5;
	// UnityEngine.UI.Text NendUnityPlugin.AD.Native.NendAdNativeView::prText
	Text_t1901882714 * ___prText_6;
	// UnityEngine.UI.Text NendUnityPlugin.AD.Native.NendAdNativeView::shortText
	Text_t1901882714 * ___shortText_7;
	// UnityEngine.UI.Text NendUnityPlugin.AD.Native.NendAdNativeView::longText
	Text_t1901882714 * ___longText_8;
	// UnityEngine.UI.Text NendUnityPlugin.AD.Native.NendAdNativeView::promotionNameText
	Text_t1901882714 * ___promotionNameText_9;
	// UnityEngine.UI.Text NendUnityPlugin.AD.Native.NendAdNativeView::promotionUrlText
	Text_t1901882714 * ___promotionUrlText_10;
	// UnityEngine.UI.Text NendUnityPlugin.AD.Native.NendAdNativeView::actionButtonText
	Text_t1901882714 * ___actionButtonText_11;
	// UnityEngine.UI.RawImage NendUnityPlugin.AD.Native.NendAdNativeView::adImage
	RawImage_t3182918964 * ___adImage_12;
	// UnityEngine.UI.RawImage NendUnityPlugin.AD.Native.NendAdNativeView::logoImage
	RawImage_t3182918964 * ___logoImage_13;
	// NendUnityPlugin.AD.Native.Events.NativeAdViewEvent NendUnityPlugin.AD.Native.NendAdNativeView::AdShown
	NativeAdViewEvent_t610464567 * ___AdShown_14;
	// NendUnityPlugin.AD.Native.Events.NativeAdViewEvent NendUnityPlugin.AD.Native.NendAdNativeView::AdFailedToShow
	NativeAdViewEvent_t610464567 * ___AdFailedToShow_15;
	// NendUnityPlugin.AD.Native.Events.NativeAdViewEvent NendUnityPlugin.AD.Native.NendAdNativeView::AdClicked
	NativeAdViewEvent_t610464567 * ___AdClicked_16;
	// NendUnityPlugin.AD.Native.INativeAd NendUnityPlugin.AD.Native.NendAdNativeView::m_LoadedNativeAd
	Il2CppObject * ___m_LoadedNativeAd_17;
	// NendUnityPlugin.AD.Native.INativeAd NendUnityPlugin.AD.Native.NendAdNativeView::m_ShowingNativeAd
	Il2CppObject * ___m_ShowingNativeAd_18;

public:
	inline static int32_t get_offset_of_viewTag_2() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___viewTag_2)); }
	inline int32_t get_viewTag_2() const { return ___viewTag_2; }
	inline int32_t* get_address_of_viewTag_2() { return &___viewTag_2; }
	inline void set_viewTag_2(int32_t value)
	{
		___viewTag_2 = value;
	}

	inline static int32_t get_offset_of_renderWhenLoaded_3() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___renderWhenLoaded_3)); }
	inline bool get_renderWhenLoaded_3() const { return ___renderWhenLoaded_3; }
	inline bool* get_address_of_renderWhenLoaded_3() { return &___renderWhenLoaded_3; }
	inline void set_renderWhenLoaded_3(bool value)
	{
		___renderWhenLoaded_3 = value;
	}

	inline static int32_t get_offset_of_renderWhenActivated_4() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___renderWhenActivated_4)); }
	inline bool get_renderWhenActivated_4() const { return ___renderWhenActivated_4; }
	inline bool* get_address_of_renderWhenActivated_4() { return &___renderWhenActivated_4; }
	inline void set_renderWhenActivated_4(bool value)
	{
		___renderWhenActivated_4 = value;
	}

	inline static int32_t get_offset_of_advertisingExplicitly_5() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___advertisingExplicitly_5)); }
	inline int32_t get_advertisingExplicitly_5() const { return ___advertisingExplicitly_5; }
	inline int32_t* get_address_of_advertisingExplicitly_5() { return &___advertisingExplicitly_5; }
	inline void set_advertisingExplicitly_5(int32_t value)
	{
		___advertisingExplicitly_5 = value;
	}

	inline static int32_t get_offset_of_prText_6() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___prText_6)); }
	inline Text_t1901882714 * get_prText_6() const { return ___prText_6; }
	inline Text_t1901882714 ** get_address_of_prText_6() { return &___prText_6; }
	inline void set_prText_6(Text_t1901882714 * value)
	{
		___prText_6 = value;
		Il2CppCodeGenWriteBarrier(&___prText_6, value);
	}

	inline static int32_t get_offset_of_shortText_7() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___shortText_7)); }
	inline Text_t1901882714 * get_shortText_7() const { return ___shortText_7; }
	inline Text_t1901882714 ** get_address_of_shortText_7() { return &___shortText_7; }
	inline void set_shortText_7(Text_t1901882714 * value)
	{
		___shortText_7 = value;
		Il2CppCodeGenWriteBarrier(&___shortText_7, value);
	}

	inline static int32_t get_offset_of_longText_8() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___longText_8)); }
	inline Text_t1901882714 * get_longText_8() const { return ___longText_8; }
	inline Text_t1901882714 ** get_address_of_longText_8() { return &___longText_8; }
	inline void set_longText_8(Text_t1901882714 * value)
	{
		___longText_8 = value;
		Il2CppCodeGenWriteBarrier(&___longText_8, value);
	}

	inline static int32_t get_offset_of_promotionNameText_9() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___promotionNameText_9)); }
	inline Text_t1901882714 * get_promotionNameText_9() const { return ___promotionNameText_9; }
	inline Text_t1901882714 ** get_address_of_promotionNameText_9() { return &___promotionNameText_9; }
	inline void set_promotionNameText_9(Text_t1901882714 * value)
	{
		___promotionNameText_9 = value;
		Il2CppCodeGenWriteBarrier(&___promotionNameText_9, value);
	}

	inline static int32_t get_offset_of_promotionUrlText_10() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___promotionUrlText_10)); }
	inline Text_t1901882714 * get_promotionUrlText_10() const { return ___promotionUrlText_10; }
	inline Text_t1901882714 ** get_address_of_promotionUrlText_10() { return &___promotionUrlText_10; }
	inline void set_promotionUrlText_10(Text_t1901882714 * value)
	{
		___promotionUrlText_10 = value;
		Il2CppCodeGenWriteBarrier(&___promotionUrlText_10, value);
	}

	inline static int32_t get_offset_of_actionButtonText_11() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___actionButtonText_11)); }
	inline Text_t1901882714 * get_actionButtonText_11() const { return ___actionButtonText_11; }
	inline Text_t1901882714 ** get_address_of_actionButtonText_11() { return &___actionButtonText_11; }
	inline void set_actionButtonText_11(Text_t1901882714 * value)
	{
		___actionButtonText_11 = value;
		Il2CppCodeGenWriteBarrier(&___actionButtonText_11, value);
	}

	inline static int32_t get_offset_of_adImage_12() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___adImage_12)); }
	inline RawImage_t3182918964 * get_adImage_12() const { return ___adImage_12; }
	inline RawImage_t3182918964 ** get_address_of_adImage_12() { return &___adImage_12; }
	inline void set_adImage_12(RawImage_t3182918964 * value)
	{
		___adImage_12 = value;
		Il2CppCodeGenWriteBarrier(&___adImage_12, value);
	}

	inline static int32_t get_offset_of_logoImage_13() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___logoImage_13)); }
	inline RawImage_t3182918964 * get_logoImage_13() const { return ___logoImage_13; }
	inline RawImage_t3182918964 ** get_address_of_logoImage_13() { return &___logoImage_13; }
	inline void set_logoImage_13(RawImage_t3182918964 * value)
	{
		___logoImage_13 = value;
		Il2CppCodeGenWriteBarrier(&___logoImage_13, value);
	}

	inline static int32_t get_offset_of_AdShown_14() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___AdShown_14)); }
	inline NativeAdViewEvent_t610464567 * get_AdShown_14() const { return ___AdShown_14; }
	inline NativeAdViewEvent_t610464567 ** get_address_of_AdShown_14() { return &___AdShown_14; }
	inline void set_AdShown_14(NativeAdViewEvent_t610464567 * value)
	{
		___AdShown_14 = value;
		Il2CppCodeGenWriteBarrier(&___AdShown_14, value);
	}

	inline static int32_t get_offset_of_AdFailedToShow_15() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___AdFailedToShow_15)); }
	inline NativeAdViewEvent_t610464567 * get_AdFailedToShow_15() const { return ___AdFailedToShow_15; }
	inline NativeAdViewEvent_t610464567 ** get_address_of_AdFailedToShow_15() { return &___AdFailedToShow_15; }
	inline void set_AdFailedToShow_15(NativeAdViewEvent_t610464567 * value)
	{
		___AdFailedToShow_15 = value;
		Il2CppCodeGenWriteBarrier(&___AdFailedToShow_15, value);
	}

	inline static int32_t get_offset_of_AdClicked_16() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___AdClicked_16)); }
	inline NativeAdViewEvent_t610464567 * get_AdClicked_16() const { return ___AdClicked_16; }
	inline NativeAdViewEvent_t610464567 ** get_address_of_AdClicked_16() { return &___AdClicked_16; }
	inline void set_AdClicked_16(NativeAdViewEvent_t610464567 * value)
	{
		___AdClicked_16 = value;
		Il2CppCodeGenWriteBarrier(&___AdClicked_16, value);
	}

	inline static int32_t get_offset_of_m_LoadedNativeAd_17() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___m_LoadedNativeAd_17)); }
	inline Il2CppObject * get_m_LoadedNativeAd_17() const { return ___m_LoadedNativeAd_17; }
	inline Il2CppObject ** get_address_of_m_LoadedNativeAd_17() { return &___m_LoadedNativeAd_17; }
	inline void set_m_LoadedNativeAd_17(Il2CppObject * value)
	{
		___m_LoadedNativeAd_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_LoadedNativeAd_17, value);
	}

	inline static int32_t get_offset_of_m_ShowingNativeAd_18() { return static_cast<int32_t>(offsetof(NendAdNativeView_t2014609480, ___m_ShowingNativeAd_18)); }
	inline Il2CppObject * get_m_ShowingNativeAd_18() const { return ___m_ShowingNativeAd_18; }
	inline Il2CppObject ** get_address_of_m_ShowingNativeAd_18() { return &___m_ShowingNativeAd_18; }
	inline void set_m_ShowingNativeAd_18(Il2CppObject * value)
	{
		___m_ShowingNativeAd_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_ShowingNativeAd_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
