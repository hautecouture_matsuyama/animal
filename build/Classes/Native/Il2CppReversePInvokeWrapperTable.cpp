﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen2901615101.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV2883267092.h"





extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IOSFullBoardAd_FullBoardAdCallback_m1988850410(intptr_t ___selfPtr0, int32_t ___type1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IOSInterstitialVideoAd_NormalInterstitialCallback_m512243254(intptr_t ___selfPtr0, int32_t ___type1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IOSInterstitialVideoAd_ErrorInterstitialCallback_m423066620(intptr_t ___selfPtr0, int32_t ___type1, int32_t ___errorCode2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IOSNativeAdClient_NativeAdCallback_m2493280005(intptr_t ___client0, intptr_t ___nativeAd1, int32_t ___code2, char* ___message3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IOSRewardedVideoAd_NormalRewardedCallback_m1864445161(intptr_t ___selfPtr0, int32_t ___type1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IOSRewardedVideoAd_ErrorRewardedCallback_m826224518(intptr_t ___selfPtr0, int32_t ___type1, int32_t ___errorCode2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IOSRewardedVideoAd_RewardedVideoAdCallback_m69340767(intptr_t ___selfPtr0, int32_t ___type1, char* ___currencyName2, int32_t ___currencyAmount3);
extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[7] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IOSFullBoardAd_FullBoardAdCallback_m1988850410),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IOSInterstitialVideoAd_NormalInterstitialCallback_m512243254),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IOSInterstitialVideoAd_ErrorInterstitialCallback_m423066620),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IOSNativeAdClient_NativeAdCallback_m2493280005),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IOSRewardedVideoAd_NormalRewardedCallback_m1864445161),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IOSRewardedVideoAd_ErrorRewardedCallback_m826224518),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IOSRewardedVideoAd_RewardedVideoAdCallback_m69340767),
};
