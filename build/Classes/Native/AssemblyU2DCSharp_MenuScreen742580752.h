﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuScreen
struct  MenuScreen_t742580752  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MenuScreen::bottomY
	float ___bottomY_2;
	// System.Single MenuScreen::leftX
	float ___leftX_3;
	// System.Single MenuScreen::topY
	float ___topY_4;
	// System.Single MenuScreen::rightX
	float ___rightX_5;
	// UnityEngine.GameObject MenuScreen::groundPrefab
	GameObject_t1113636619 * ___groundPrefab_6;
	// System.Single MenuScreen::groundTileH
	float ___groundTileH_7;

public:
	inline static int32_t get_offset_of_bottomY_2() { return static_cast<int32_t>(offsetof(MenuScreen_t742580752, ___bottomY_2)); }
	inline float get_bottomY_2() const { return ___bottomY_2; }
	inline float* get_address_of_bottomY_2() { return &___bottomY_2; }
	inline void set_bottomY_2(float value)
	{
		___bottomY_2 = value;
	}

	inline static int32_t get_offset_of_leftX_3() { return static_cast<int32_t>(offsetof(MenuScreen_t742580752, ___leftX_3)); }
	inline float get_leftX_3() const { return ___leftX_3; }
	inline float* get_address_of_leftX_3() { return &___leftX_3; }
	inline void set_leftX_3(float value)
	{
		___leftX_3 = value;
	}

	inline static int32_t get_offset_of_topY_4() { return static_cast<int32_t>(offsetof(MenuScreen_t742580752, ___topY_4)); }
	inline float get_topY_4() const { return ___topY_4; }
	inline float* get_address_of_topY_4() { return &___topY_4; }
	inline void set_topY_4(float value)
	{
		___topY_4 = value;
	}

	inline static int32_t get_offset_of_rightX_5() { return static_cast<int32_t>(offsetof(MenuScreen_t742580752, ___rightX_5)); }
	inline float get_rightX_5() const { return ___rightX_5; }
	inline float* get_address_of_rightX_5() { return &___rightX_5; }
	inline void set_rightX_5(float value)
	{
		___rightX_5 = value;
	}

	inline static int32_t get_offset_of_groundPrefab_6() { return static_cast<int32_t>(offsetof(MenuScreen_t742580752, ___groundPrefab_6)); }
	inline GameObject_t1113636619 * get_groundPrefab_6() const { return ___groundPrefab_6; }
	inline GameObject_t1113636619 ** get_address_of_groundPrefab_6() { return &___groundPrefab_6; }
	inline void set_groundPrefab_6(GameObject_t1113636619 * value)
	{
		___groundPrefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___groundPrefab_6, value);
	}

	inline static int32_t get_offset_of_groundTileH_7() { return static_cast<int32_t>(offsetof(MenuScreen_t742580752, ___groundTileH_7)); }
	inline float get_groundTileH_7() const { return ___groundTileH_7; }
	inline float* get_address_of_groundTileH_7() { return &___groundTileH_7; }
	inline void set_groundTileH_7(float value)
	{
		___groundTileH_7 = value;
	}
};

struct MenuScreen_t742580752_StaticFields
{
public:
	// System.Boolean MenuScreen::isSoundOn
	bool ___isSoundOn_8;

public:
	inline static int32_t get_offset_of_isSoundOn_8() { return static_cast<int32_t>(offsetof(MenuScreen_t742580752_StaticFields, ___isSoundOn_8)); }
	inline bool get_isSoundOn_8() const { return ___isSoundOn_8; }
	inline bool* get_address_of_isSoundOn_8() { return &___isSoundOn_8; }
	inline void set_isSoundOn_8(bool value)
	{
		___isSoundOn_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
