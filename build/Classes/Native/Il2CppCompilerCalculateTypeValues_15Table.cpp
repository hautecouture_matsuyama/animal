﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo232255230.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache2187958399.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2703961024.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall832123510.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3448586328.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3407714124.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup3050769227.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2498835369.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase3960448221.h"
#include "UnityEngine_UnityEngine_Events_UnityAction3245792599.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent2581268647.h"
#include "UnityEngine_UnityEngine_UnityString1423233093.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_Vector43319028937.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime189548121.h"
#include "UnityEngine_UnityEngine_ThreadAndSerializationSafeA363116225.h"
#include "UnityEngine_UnityEngine_Collections_ReadOnlyAttrib2029203740.h"
#include "UnityEngine_UnityEngine_Collections_ReadWriteAttrib306517538.h"
#include "UnityEngine_UnityEngine_Collections_WriteOnlyAttrib595109273.h"
#include "UnityEngine_UnityEngine_Collections_DeallocateOnJo3131681843.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine2600515814.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine3790689680.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine1586929818.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram3985821396.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram4130423782.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri2337225216.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAt3592494112.h"
#include "UnityEngine_UnityEngine_Logger274032455.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1170575784.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3081694049.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3411787513.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1491597365.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3251856151.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1684935770.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnection907692441.h"
#include "UnityEngine_UnityEngine_Experimental_Rendering_Ren4036911426.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode1703770351.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative4130846357.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2859083114.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules96689094.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAt254868554.h"
#include "UnityEngine_UnityEngineInternal_GenericStack1310059385.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4089902045.h"
#include "LitJson_U3CModuleU3E692745525.h"
#include "LitJson_LitJson_JsonType2731125707.h"
#include "LitJson_LitJson_JsonMockWrapper82875095.h"
#include "LitJson_LitJson_JsonData1524858407.h"
#include "LitJson_LitJson_OrderedDictionaryEnumerator386339177.h"
#include "LitJson_LitJson_JsonException3682484112.h"
#include "LitJson_LitJson_PropertyMetadata3727440473.h"
#include "LitJson_LitJson_ArrayMetadata894288939.h"
#include "LitJson_LitJson_ObjectMetadata3566284522.h"
#include "LitJson_LitJson_ExporterFunc1851311465.h"
#include "LitJson_LitJson_ImporterFunc3630937194.h"
#include "LitJson_LitJson_WrapperFactory2158548929.h"
#include "LitJson_LitJson_JsonMapper3815285241.h"
#include "LitJson_LitJson_JsonToken3605538862.h"
#include "LitJson_LitJson_JsonReader836887441.h"
#include "LitJson_LitJson_Condition3240125930.h"
#include "LitJson_LitJson_WriterContext1011093999.h"
#include "LitJson_LitJson_JsonWriter3570089748.h"
#include "LitJson_LitJson_FsmContext2331368794.h"
#include "LitJson_LitJson_Lexer1514038666.h"
#include "LitJson_LitJson_Lexer_StateHandler105866779.h"
#include "LitJson_LitJson_ParserToken2380208742.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db53336159630.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db52672650908.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db53636912669.h"
#include "UnityEngine_UI_U3CModuleU3E692745525.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle600343995.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (PersistentListenerMode_t232255230)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1500[8] = 
{
	PersistentListenerMode_t232255230::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (ArgumentCache_t2187958399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1501[6] = 
{
	ArgumentCache_t2187958399::get_offset_of_m_ObjectArgument_0(),
	ArgumentCache_t2187958399::get_offset_of_m_ObjectArgumentAssemblyTypeName_1(),
	ArgumentCache_t2187958399::get_offset_of_m_IntArgument_2(),
	ArgumentCache_t2187958399::get_offset_of_m_FloatArgument_3(),
	ArgumentCache_t2187958399::get_offset_of_m_StringArgument_4(),
	ArgumentCache_t2187958399::get_offset_of_m_BoolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (BaseInvokableCall_t2703961024), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (InvokableCall_t832123510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1503[1] = 
{
	InvokableCall_t832123510::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1504[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1505[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1506[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1507[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1508[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (UnityEventCallState_t3448586328)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1509[4] = 
{
	UnityEventCallState_t3448586328::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (PersistentCall_t3407714124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1510[5] = 
{
	PersistentCall_t3407714124::get_offset_of_m_Target_0(),
	PersistentCall_t3407714124::get_offset_of_m_MethodName_1(),
	PersistentCall_t3407714124::get_offset_of_m_Mode_2(),
	PersistentCall_t3407714124::get_offset_of_m_Arguments_3(),
	PersistentCall_t3407714124::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (PersistentCallGroup_t3050769227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1511[1] = 
{
	PersistentCallGroup_t3050769227::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (InvokableCallList_t2498835369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1512[4] = 
{
	InvokableCallList_t2498835369::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t2498835369::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t2498835369::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t2498835369::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (UnityEventBase_t3960448221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1513[4] = 
{
	UnityEventBase_t3960448221::get_offset_of_m_Calls_0(),
	UnityEventBase_t3960448221::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t3960448221::get_offset_of_m_TypeName_2(),
	UnityEventBase_t3960448221::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (UnityAction_t3245792599), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (UnityEvent_t2581268647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1515[1] = 
{
	UnityEvent_t2581268647::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1517[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1519[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1521[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1523[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (UnityString_t1423233093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (Vector2_t2156229523)+ sizeof (Il2CppObject), sizeof(Vector2_t2156229523 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1525[3] = 
{
	Vector2_t2156229523::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t2156229523::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (Vector4_t3319028937)+ sizeof (Il2CppObject), sizeof(Vector4_t3319028937 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1526[5] = 
{
	0,
	Vector4_t3319028937::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t3319028937::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t3319028937::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t3319028937::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (WaitForSecondsRealtime_t189548121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1527[1] = 
{
	WaitForSecondsRealtime_t189548121::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (ThreadAndSerializationSafeAttribute_t363116225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (ReadOnlyAttribute_t2029203740), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (ReadWriteAttribute_t306517538), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (WriteOnlyAttribute_t595109273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (DeallocateOnJobCompletionAttribute_t3131681843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (NativeContainerAttribute_t2600515814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (NativeContainerSupportsAtomicWriteAttribute_t3790689680), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (NativeContainerSupportsMinMaxWriteRestrictionAttribute_t1586929818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (FrameData_t3985821396)+ sizeof (Il2CppObject), sizeof(FrameData_t3985821396 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1536[6] = 
{
	FrameData_t3985821396::get_offset_of_m_FrameID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_DeltaTime_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_Weight_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_EffectiveWeight_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_EffectiveSpeed_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_Flags_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (Flags_t4130423782)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1537[3] = 
{
	Flags_t4130423782::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (DefaultValueAttribute_t2337225216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1538[1] = 
{
	DefaultValueAttribute_t2337225216::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (ExcludeFromDocsAttribute_t3592494112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (Logger_t274032455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1542[3] = 
{
	Logger_t274032455::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t274032455::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t274032455::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (MessageEventArgs_t1170575784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1543[2] = 
{
	MessageEventArgs_t1170575784::get_offset_of_playerId_0(),
	MessageEventArgs_t1170575784::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (PlayerConnection_t3081694049), -1, sizeof(PlayerConnection_t3081694049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1545[3] = 
{
	PlayerConnection_t3081694049::get_offset_of_m_PlayerEditorConnectionEvents_2(),
	PlayerConnection_t3081694049::get_offset_of_m_connectedPlayers_3(),
	PlayerConnection_t3081694049_StaticFields::get_offset_of_s_Instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (PlayerEditorConnectionEvents_t3411787513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1546[3] = 
{
	PlayerEditorConnectionEvents_t3411787513::get_offset_of_messageTypeSubscribers_0(),
	PlayerEditorConnectionEvents_t3411787513::get_offset_of_connectionEvent_1(),
	PlayerEditorConnectionEvents_t3411787513::get_offset_of_disconnectionEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (MessageEvent_t1491597365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (ConnectionChangeEvent_t3251856151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (MessageTypeSubscribers_t1684935770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1549[3] = 
{
	MessageTypeSubscribers_t1684935770::get_offset_of_m_messageTypeId_0(),
	MessageTypeSubscribers_t1684935770::get_offset_of_subscriberCount_1(),
	MessageTypeSubscribers_t1684935770::get_offset_of_messageCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t907692441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1550[1] = 
{
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t907692441::get_offset_of_messageId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (RenderPipelineManager_t4036911426), -1, sizeof(RenderPipelineManager_t4036911426_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1553[2] = 
{
	RenderPipelineManager_t4036911426_StaticFields::get_offset_of_s_CurrentPipelineAsset_0(),
	RenderPipelineManager_t4036911426_StaticFields::get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (UsedByNativeCodeAttribute_t1703770351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (RequiredByNativeCodeAttribute_t4130846357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (FormerlySerializedAsAttribute_t2859083114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1556[1] = 
{
	FormerlySerializedAsAttribute_t2859083114::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (TypeInferenceRules_t96689094)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1557[5] = 
{
	TypeInferenceRules_t96689094::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (TypeInferenceRuleAttribute_t254868554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1558[1] = 
{
	TypeInferenceRuleAttribute_t254868554::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (GenericStack_t1310059385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (NetFxCoreExtensions_t4089902045), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (U3CModuleU3E_t692745530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (JsonType_t2731125707)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1562[9] = 
{
	JsonType_t2731125707::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (JsonMockWrapper_t82875095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (JsonData_t1524858407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1565[10] = 
{
	JsonData_t1524858407::get_offset_of_inst_array_0(),
	JsonData_t1524858407::get_offset_of_inst_boolean_1(),
	JsonData_t1524858407::get_offset_of_inst_double_2(),
	JsonData_t1524858407::get_offset_of_inst_int_3(),
	JsonData_t1524858407::get_offset_of_inst_long_4(),
	JsonData_t1524858407::get_offset_of_inst_object_5(),
	JsonData_t1524858407::get_offset_of_inst_string_6(),
	JsonData_t1524858407::get_offset_of_json_7(),
	JsonData_t1524858407::get_offset_of_type_8(),
	JsonData_t1524858407::get_offset_of_object_list_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (OrderedDictionaryEnumerator_t386339177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1566[1] = 
{
	OrderedDictionaryEnumerator_t386339177::get_offset_of_list_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { sizeof (JsonException_t3682484112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (PropertyMetadata_t3727440473)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1568[3] = 
{
	PropertyMetadata_t3727440473::get_offset_of_Info_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3727440473::get_offset_of_IsField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3727440473::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (ArrayMetadata_t894288939)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1569[3] = 
{
	ArrayMetadata_t894288939::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t894288939::get_offset_of_is_array_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t894288939::get_offset_of_is_list_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (ObjectMetadata_t3566284522)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1570[3] = 
{
	ObjectMetadata_t3566284522::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t3566284522::get_offset_of_is_dictionary_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t3566284522::get_offset_of_properties_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (ExporterFunc_t1851311465), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (ImporterFunc_t3630937194), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (WrapperFactory_t2158548929), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (JsonMapper_t3815285241), -1, sizeof(JsonMapper_t3815285241_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1574[38] = 
{
	JsonMapper_t3815285241_StaticFields::get_offset_of_max_nesting_depth_0(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_datetime_format_1(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_base_exporters_table_2(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_custom_exporters_table_3(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_base_importers_table_4(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_custom_importers_table_5(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_array_metadata_6(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_array_metadata_lock_7(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_conv_ops_8(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_conv_ops_lock_9(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_object_metadata_10(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_object_metadata_lock_11(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_type_properties_12(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_type_properties_lock_13(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_static_writer_14(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_static_writer_lock_15(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_16(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_17(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_18(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_19(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_20(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_21(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_22(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_23(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_24(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_25(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_26(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_27(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_28(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_29(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_30(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_31(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_32(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_33(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_34(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_35(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_36(),
	JsonMapper_t3815285241_StaticFields::get_offset_of_U3CU3Ef__amU24cache15_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (JsonToken_t3605538862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1575[13] = 
{
	JsonToken_t3605538862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (JsonReader_t836887441), -1, sizeof(JsonReader_t836887441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1576[15] = 
{
	JsonReader_t836887441_StaticFields::get_offset_of_parse_table_0(),
	JsonReader_t836887441::get_offset_of_automaton_stack_1(),
	JsonReader_t836887441::get_offset_of_current_input_2(),
	JsonReader_t836887441::get_offset_of_current_symbol_3(),
	JsonReader_t836887441::get_offset_of_end_of_json_4(),
	JsonReader_t836887441::get_offset_of_end_of_input_5(),
	JsonReader_t836887441::get_offset_of_lexer_6(),
	JsonReader_t836887441::get_offset_of_parser_in_string_7(),
	JsonReader_t836887441::get_offset_of_parser_return_8(),
	JsonReader_t836887441::get_offset_of_read_started_9(),
	JsonReader_t836887441::get_offset_of_reader_10(),
	JsonReader_t836887441::get_offset_of_reader_is_owned_11(),
	JsonReader_t836887441::get_offset_of_skip_non_members_12(),
	JsonReader_t836887441::get_offset_of_token_value_13(),
	JsonReader_t836887441::get_offset_of_token_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (Condition_t3240125930)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1577[6] = 
{
	Condition_t3240125930::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (WriterContext_t1011093999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1578[4] = 
{
	WriterContext_t1011093999::get_offset_of_Count_0(),
	WriterContext_t1011093999::get_offset_of_InArray_1(),
	WriterContext_t1011093999::get_offset_of_InObject_2(),
	WriterContext_t1011093999::get_offset_of_ExpectingValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (JsonWriter_t3570089748), -1, sizeof(JsonWriter_t3570089748_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1579[11] = 
{
	JsonWriter_t3570089748_StaticFields::get_offset_of_number_format_0(),
	JsonWriter_t3570089748::get_offset_of_context_1(),
	JsonWriter_t3570089748::get_offset_of_ctx_stack_2(),
	JsonWriter_t3570089748::get_offset_of_has_reached_end_3(),
	JsonWriter_t3570089748::get_offset_of_hex_seq_4(),
	JsonWriter_t3570089748::get_offset_of_indentation_5(),
	JsonWriter_t3570089748::get_offset_of_indent_value_6(),
	JsonWriter_t3570089748::get_offset_of_inst_string_builder_7(),
	JsonWriter_t3570089748::get_offset_of_pretty_print_8(),
	JsonWriter_t3570089748::get_offset_of_validate_9(),
	JsonWriter_t3570089748::get_offset_of_writer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (FsmContext_t2331368794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1580[4] = 
{
	FsmContext_t2331368794::get_offset_of_Return_0(),
	FsmContext_t2331368794::get_offset_of_NextState_1(),
	FsmContext_t2331368794::get_offset_of_L_2(),
	FsmContext_t2331368794::get_offset_of_StateStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (Lexer_t1514038666), -1, sizeof(Lexer_t1514038666_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1581[42] = 
{
	Lexer_t1514038666_StaticFields::get_offset_of_fsm_return_table_0(),
	Lexer_t1514038666_StaticFields::get_offset_of_fsm_handler_table_1(),
	Lexer_t1514038666::get_offset_of_allow_comments_2(),
	Lexer_t1514038666::get_offset_of_allow_single_quoted_strings_3(),
	Lexer_t1514038666::get_offset_of_end_of_input_4(),
	Lexer_t1514038666::get_offset_of_fsm_context_5(),
	Lexer_t1514038666::get_offset_of_input_buffer_6(),
	Lexer_t1514038666::get_offset_of_input_char_7(),
	Lexer_t1514038666::get_offset_of_reader_8(),
	Lexer_t1514038666::get_offset_of_state_9(),
	Lexer_t1514038666::get_offset_of_string_buffer_10(),
	Lexer_t1514038666::get_offset_of_string_value_11(),
	Lexer_t1514038666::get_offset_of_token_12(),
	Lexer_t1514038666::get_offset_of_unichar_13(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_14(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_15(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_16(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_17(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_18(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_19(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_20(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_21(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_22(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_23(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_24(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_25(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_26(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_27(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_28(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_29(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_30(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_31(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache12_32(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache13_33(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache14_34(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache15_35(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache16_36(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache17_37(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache18_38(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache19_39(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1A_40(),
	Lexer_t1514038666_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1B_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (StateHandler_t105866779), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (ParserToken_t2380208742)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1583[20] = 
{
	ParserToken_t2380208742::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630), -1, sizeof(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1584[13] = 
{
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2DA_10(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2DB_11(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630_StaticFields::get_offset_of_U24fieldU2DC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (U24ArrayTypeU3D12_t2672650908)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t2672650908 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (U24ArrayTypeU3D112_t3636912669)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D112_t3636912669 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (U3CModuleU3E_t692745531), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (EventHandle_t600343995)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1588[3] = 
{
	EventHandle_t600343995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
