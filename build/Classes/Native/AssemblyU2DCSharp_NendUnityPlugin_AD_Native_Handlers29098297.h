﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.Action
struct Action_t1264377477;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.Handlers.ClickHandler
struct  ClickHandler_t29098297  : public MonoBehaviour_t3962482529
{
public:
	// System.Action NendUnityPlugin.AD.Native.Handlers.ClickHandler::OnClick
	Action_t1264377477 * ___OnClick_2;

public:
	inline static int32_t get_offset_of_OnClick_2() { return static_cast<int32_t>(offsetof(ClickHandler_t29098297, ___OnClick_2)); }
	inline Action_t1264377477 * get_OnClick_2() const { return ___OnClick_2; }
	inline Action_t1264377477 ** get_address_of_OnClick_2() { return &___OnClick_2; }
	inline void set_OnClick_2(Action_t1264377477 * value)
	{
		___OnClick_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnClick_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
