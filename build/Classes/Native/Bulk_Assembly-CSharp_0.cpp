﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "AssemblyU2DCSharp_U3CModuleU3E692745525.h"
#include "AssemblyU2DCSharp_BackButtonUtil2880819819.h"
#include "mscorlib_System_Void1185182177.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Object631007953.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "mscorlib_System_Type2483944760.h"
#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Boolean97287965.h"
#include "mscorlib_System_String1847450689.h"
#include "UnityEngine_UnityEngine_KeyCode2599294277.h"
#include "AssemblyU2DCSharp_BackKeyAction3328090672.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent2581268647.h"
#include "UnityEngine_UnityEngine_Events_UnityAction3245792599.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase3960448221.h"
#include "AssemblyU2DCSharp_BestScore1653437491.h"
#include "mscorlib_System_Int322950945753.h"
#include "UnityEngine_UnityEngine_GameObject1113636619.h"
#include "UnityEngine_UnityEngine_Component1923634451.h"
#include "UnityEngine_UI_UnityEngine_UI_Text1901882714.h"
#include "AssemblyU2DCSharp_ButtonHome643243358.h"
#include "mscorlib_System_Single1397266774.h"
#include "AssemblyU2DCSharp_ButtonHomeOver1627401701.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_Renderer2627027031.h"
#include "UnityEngine_UnityEngine_Material340375123.h"
#include "AssemblyU2DCSharp_PlayerControl1977005294.h"
#include "UnityEngine_UnityEngine_Collider2D2806799626.h"
#include "UnityEngine_UnityEngine_Behaviour1437897464.h"
#include "AssemblyU2DCSharp_ButtonPause3620264363.h"
#include "AssemblyU2DCSharp_GameScreen1800529819.h"
#include "UnityEngine_UnityEngine_AudioSource3935305588.h"
#include "AssemblyU2DCSharp_ButtonPlay1912942282.h"
#include "AssemblyU2DCSharp_ButtonReplay987972804.h"
#include "AssemblyU2DCSharp_ButtonReplayOver967915415.h"
#include "AssemblyU2DCSharp_ButtonResume2840336175.h"
#include "AssemblyU2DCSharp_MenuScreen742580752.h"
#include "AssemblyU2DCSharp_ButtonSound35905277.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_Bounds2266837910.h"
#include "UnityEngine_UnityEngine_Transform3600365921.h"
#include "UnityEngine_UnityEngine_Camera4157153871.h"
#include "AssemblyU2DCSharp_Coin2227745140.h"
#include "UnityEngine_UnityEngine_AudioRolloffMode832723327.h"
#include "UnityEngine_UnityEngine_AudioClip3680889665.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_RaycastHit2D2279581989.h"
#include "AssemblyU2DCSharp_Score2516855617.h"
#include "AssemblyU2DCSharp_DontDestroy1446738193.h"
#include "UnityEngine_UnityEngine_Quaternion2301928331.h"
#include "mscorlib_System_Int162552820387.h"
#include "UnityEngine_UnityEngine_BoxCollider2D3581341831.h"
#include "UnityEngine_UnityEngine_Coroutine3829159415.h"
#include "AssemblyU2DCSharp_GameScreen_U3CchangeSpeedU3Ec__Ite85360554.h"
#include "AssemblyU2DCSharp_GameScreen_U3CchangeScoreU3Ec__It982768640.h"
#include "AssemblyU2DCSharp_GameScreen_U3ChideInfoU3Ec__Iter3795124997.h"
#include "AssemblyU2DCSharp_GameScreen_U3CshowIfGameOverU3Ec_966372778.h"
#include "UnityEngine_UnityEngine_Sprite280657092.h"
#include "AssemblyU2DCSharp_ObsDownCollisionCheck2541817184.h"
#include "AssemblyU2DCSharp_ObsUpCollisionCheck2909508583.h"
#include "UnityEngine_UnityEngine_SpriteRenderer3235626157.h"
#include "UnityEngine_UnityEngine_GUIText402233326.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdInterst1417649638.h"
#include "mscorlib_System_UInt322560061978.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1699091251.h"
#include "mscorlib_System_NotSupportedException1314879016.h"
#include "AssemblyU2DCSharp_Ground4133628138.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner2921968851.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage3182918964.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic1660335611.h"
#include "mscorlib_System_Collections_Generic_List_1_gen926430637.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner_U3CLoadJsonData1810388169.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner_U3CLoadTextureU1051594053.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner_U3CAnimateBanner688012631.h"
#include "UnityEngine_UnityEngine_Texture2D3840446185.h"
#include "UnityEngine_UnityEngine_Texture3661962703.h"
#include "UnityEngine_UnityEngine_NetworkReachability3450623372.h"
#include "UnityEngine_UnityEngine_AsyncOperation1445031843.h"
#include "UnityEngine_UnityEngine_Rect2360479859.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner_U3CAnimateBanner102123587.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner_AnimBannerData3749323191.h"
#include "System_Core_System_Func_2_gen1820288298.h"
#include "mscorlib_System_Double594665363.h"
#include "UnityEngine_UnityEngine_WWW3688466362.h"
#include "mscorlib_System_Byte1134296376.h"
#include "AssemblyU2DCSharp_IpadAspectRation3315943056.h"
#include "UnityEngine_UnityEngine_RectTransform3704657025.h"
#include "AssemblyU2DCSharp_MovingBall2489706458.h"
#include "AssemblyU2DCSharp_MovingBg2796125617.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nend270881991.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen3830347218.h"
#include "mscorlib_System_Delegate1188392813.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen4155484538.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen3059274368.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen3385596145.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nend136425400.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen2901615101.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen1510350271.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS1375839176.h"
#include "mscorlib_System_AsyncCallback3962456242.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Advert2175459971.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Events_610464567.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2896268984.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Events3307153462.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen1300214534.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handlers29098297.h"
#include "System_Core_System_Action1264377477.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handle2622036411.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handle2934446534.h"
#include "UnityEngine_UnityEngine_Ray3785851493.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handler283384251.h"
#include "UnityEngine_UnityEngine_RaycastHit1056001966.h"
#include "UnityEngine_UnityEngine_Collider1773347010.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste1003666588.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3807901092.h"
#include "mscorlib_System_Collections_Generic_List_1_gen537414295.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handler258087360.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handle4278309981.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handle2201492025.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handler188448812.h"
#include "mscorlib_System_InvalidOperationException56020091.h"
#include "mscorlib_System_Exception1436737249.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handler188317754.h"
#include "UnityEngine_UnityEngine_Canvas3310196443.h"
#include "UnityEngine_UnityEngine_RenderMode4077056833.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handle3542390976.h"
#include "UnityEngine_UnityEngine_CanvasGroup4083511760.h"
#include "System_Core_System_Func_2_gen398065169.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NativeA775405424.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g2527432003.h"
#include "mscorlib_System_EventHandler1348719766.h"
#include "mscorlib_System_NotImplementedException3489357830.h"
#include "mscorlib_System_Action_1_gen4012913780.h"
#include "mscorlib_System_ArgumentNullException1615371798.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAd2014609480.h"
#include "System_Core_System_Func_2_gen2922669422.h"
#include "mscorlib_System_EventArgs3591816995.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NativeA772434093.h"
#include "System_System_Collections_Generic_Queue_1_gen414173832.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_4075384413.h"
#include "System_Core_System_Action_3_gen567914338.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Native1108917185.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Native3065232321.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdMai3569218590.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Native4254168860.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS1155913684.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAd3381739006.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAdN799839499.h"
#include "System_Core_System_Func_2_gen261507339.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAd1337685977.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAd1337685978.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendUtils2230227361.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendUtils3358092198.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAd2295075562.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAdNa87332621.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_1147818846.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_1611451218.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdLogg918333666.h"
#include "System_System_Timers_ElapsedEventHandler976055006.h"
#include "System_System_Timers_Timer1767341190.h"
#include "System_System_Timers_ElapsedEventArgs3048571484.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_3414853624.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_2469200630.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_3323732960.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3625702484.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_U575704595.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_4270539830.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAd284737821.h"
#include "UnityEngine_UnityEngine_HideFlags4250555765.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_Gravity3768410666.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAd_Margin772722833.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdBanner489953245.h"
#include "mscorlib_System_EventHandler_1_gen1314708246.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Layout_NendAdDef3894915050.h"
#include "mscorlib_System_Text_StringBuilder1712802186.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdBanner_2479415987.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendError2215997335.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdErr3390548813.h"
#include "mscorlib_System_Char3634460470.h"
#include "mscorlib_System_EventHandler_1_gen3479984044.h"
#include "mscorlib_System_EventHandler_1_gen3193289641.h"
#include "mscorlib_System_EventHandler_1_gen1007381714.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdInt2700663047.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdInterst1260857315.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdInt1787058500.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdIntersti974162912.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdInt2404809203.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdInterst3083222281.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_ErrorVi3139399691.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV2883267092.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_VideoAd2567701888.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdI1383172729.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV1781712043.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdR3964314288.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdR3161671531.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdRe687064503.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_Rewarde3338436455.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdU4152365987.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2736202052.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge379921662.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1632706988.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4177511560.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_iOS_IOS2237018260.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdU3769286665.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV1357133087.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV1837550073.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV4265396701.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV3987677121.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdVi895736144.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdVi464199100.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV3646835482.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdVi172780505.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV2144365953.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV3890769867.h"

// BackButtonUtil
struct BackButtonUtil_t2880819819;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t3960448221;
// BestScore
struct BestScore_t1653437491;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Text
struct Text_t1901882714;
// ButtonHome
struct ButtonHome_t643243358;
// ButtonHomeOver
struct ButtonHomeOver_t1627401701;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// ButtonPause
struct ButtonPause_t3620264363;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// ButtonPlay
struct ButtonPlay_t1912942282;
// ButtonReplay
struct ButtonReplay_t987972804;
// ButtonReplayOver
struct ButtonReplayOver_t967915415;
// ButtonResume
struct ButtonResume_t2840336175;
// ButtonSound
struct ButtonSound_t35905277;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Camera
struct Camera_t4157153871;
// Coin
struct Coin_t2227745140;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// DontDestroy
struct DontDestroy_t1446738193;
// GameScreen
struct GameScreen_t1800529819;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// GameScreen/<changeSpeed>c__Iterator0
struct U3CchangeSpeedU3Ec__Iterator0_t85360554;
// GameScreen/<changeScore>c__Iterator1
struct U3CchangeScoreU3Ec__Iterator1_t982768640;
// GameScreen/<hideInfo>c__Iterator2
struct U3ChideInfoU3Ec__Iterator2_t3795124997;
// GameScreen/<showIfGameOver>c__Iterator3
struct U3CshowIfGameOverU3Ec__Iterator3_t966372778;
// ObsDownCollisionCheck
struct ObsDownCollisionCheck_t2541817184;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t3581341831;
// ObsUpCollisionCheck
struct ObsUpCollisionCheck_t2909508583;
// NendUnityPlugin.AD.NendAdInterstitial
struct NendAdInterstitial_t1417649638;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// Ground
struct Ground_t4133628138;
// HCAnimatedBanner
struct HCAnimatedBanner_t2921968851;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// HCAnimatedBanner/<LoadJsonData>c__Iterator0
struct U3CLoadJsonDataU3Ec__Iterator0_t1810388169;
// HCAnimatedBanner/<LoadTexture>c__Iterator1
struct U3CLoadTextureU3Ec__Iterator1_t1051594053;
// HCAnimatedBanner/<AnimateBanner>c__Iterator2
struct U3CAnimateBannerU3Ec__Iterator2_t688012631;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// HCAnimatedBanner/<AnimateBanner>c__Iterator2/<AnimateBanner>c__AnonStorey3
struct U3CAnimateBannerU3Ec__AnonStorey3_t102123587;
// System.Collections.Generic.List`1<HCAnimatedBanner/AnimBannerData>
struct List_1_t926430637;
// HCAnimatedBanner/AnimBannerData
struct AnimBannerData_t3749323191;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Func`2<UnityEngine.Rect,UnityEngine.Rect>
struct Func_2_t1820288298;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Rect>
struct IEnumerable_1_t1340332748;
// UnityEngine.Rect[]
struct RectU5BU5D_t2936723554;
// UnityEngine.WWW
struct WWW_t3688466362;
// HCAnimatedBanner/AnimBannerData[]
struct AnimBannerDataU5BU5D_t1736877454;
// System.Collections.Generic.IEnumerable`1<HCAnimatedBanner/AnimBannerData>
struct IEnumerable_1_t2729176080;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// IpadAspectRation
struct IpadAspectRation_t3315943056;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// MenuScreen
struct MenuScreen_t742580752;
// MovingBall
struct MovingBall_t2489706458;
// MovingBg
struct MovingBg_t2796125617;
// NendUnityPlugin.AD.FullBoard.NendAdFullBoard
struct NendAdFullBoard_t270881991;
// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardLoaded
struct NendAdFullBoardLoaded_t3830347218;
// System.Delegate
struct Delegate_t1188392813;
// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardFailedToLoad
struct NendAdFullBoardFailedToLoad_t4155484538;
// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardShown
struct NendAdFullBoardShown_t3059274368;
// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardClick
struct NendAdFullBoardClick_t3385596145;
// NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardDismiss
struct NendAdFullBoardDismiss_t136425400;
// NendUnityPlugin.Platform.iOS.IOSFullBoardAd
struct IOSFullBoardAd_t1375839176;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// NendUnityPlugin.AD.Native.Events.NativeAdViewEvent
struct NativeAdViewEvent_t610464567;
// UnityEngine.Events.UnityEvent`1<NendUnityPlugin.AD.Native.NendAdNativeView>
struct UnityEvent_1_t2896268984;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t3961765668;
// NendUnityPlugin.AD.Native.Events.NativeAdViewFailedToLoadEvent
struct NativeAdViewFailedToLoadEvent_t3307153462;
// UnityEngine.Events.UnityEvent`3<NendUnityPlugin.AD.Native.NendAdNativeView,System.Int32,System.String>
struct UnityEvent_3_t1300214534;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Int32,System.Object>
struct UnityEvent_3_t4241019141;
// NendUnityPlugin.AD.Native.Handlers.ClickHandler
struct ClickHandler_t29098297;
// System.Action
struct Action_t1264377477;
// NendUnityPlugin.AD.Native.Handlers.ClickHandler2D
struct ClickHandler2D_t2622036411;
// NendUnityPlugin.AD.Native.Handlers.ClickHandlerGO
struct ClickHandlerGO_t2934446534;
// NendUnityPlugin.AD.Native.Handlers.ClickHandler3D
struct ClickHandler3D_t283384251;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// NendUnityPlugin.AD.Native.Handlers.ClickHandlerUI
struct ClickHandlerUI_t258087360;
// NendUnityPlugin.AD.Native.Handlers.ImpressionHandler
struct ImpressionHandler_t4278309981;
// NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerGO
struct ImpressionHandlerGO_t188448812;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI
struct ImpressionHandlerUI_t188317754;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI/<IsViewable>c__AnonStorey0
struct U3CIsViewableU3Ec__AnonStorey0_t3542390976;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector2>
struct Func_2_t398065169;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t1136082412;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t2702166353;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// NendUnityPlugin.AD.Native.NativeAd
struct NativeAd_t775405424;
// System.Collections.Generic.HashSet`1<UnityEngine.MonoBehaviour>
struct HashSet_1_t2527432003;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t1645055638;
// System.EventHandler
struct EventHandler_t1348719766;
// System.NotImplementedException
struct NotImplementedException_t3489357830;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t4012913780;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// NendUnityPlugin.AD.Native.NendAdNativeView
struct NendAdNativeView_t2014609480;
// NendUnityPlugin.AD.Native.INativeAd
struct INativeAd_t3252869353;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// System.Func`2<UnityEngine.MonoBehaviour,System.Boolean>
struct Func_2_t2922669422;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3759279471;
// System.Collections.Generic.IEnumerable`1<UnityEngine.MonoBehaviour>
struct IEnumerable_1_t2942335418;
// System.EventArgs
struct EventArgs_t3591816995;
// NendUnityPlugin.AD.Native.NativeAdClient
struct NativeAdClient_t772434093;
// System.Collections.Generic.Queue`1<System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>>
struct Queue_1_t414173832;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2926365658;
// NendUnityPlugin.AD.Native.Utils.SimpleTimer
struct SimpleTimer_t4075384413;
// System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>
struct Action_3_t567914338;
// NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey1
struct U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185;
// NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey0
struct U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321;
// NendUnityPlugin.Common.NendAdMainThreadWorker
struct NendAdMainThreadWorker_t3569218590;
// System.Action`3<System.Object,System.Int32,System.Object>
struct Action_3_t1173861992;
// NendUnityPlugin.AD.Native.NativeAdClientFactory
struct NativeAdClientFactory_t4254168860;
// NendUnityPlugin.AD.Native.INativeAdClient
struct INativeAdClient_t1973857504;
// NendUnityPlugin.Platform.iOS.IOSNativeAdClient
struct IOSNativeAdClient_t1155913684;
// NendUnityPlugin.AD.Native.NendAdNative
struct NendAdNative_t3381739006;
// NendUnityPlugin.AD.Native.NendAdNativeView[]
struct NendAdNativeViewU5BU5D_t3641134489;
// NendUnityPlugin.AD.Native.NendAdNative/<RegisterAdView>c__AnonStorey0
struct U3CRegisterAdViewU3Ec__AnonStorey0_t799839499;
// System.Func`2<NendUnityPlugin.AD.Native.NendAdNativeView,System.Boolean>
struct Func_2_t261507339;
// System.Collections.Generic.IEnumerable`1<NendUnityPlugin.AD.Native.NendAdNativeView>
struct IEnumerable_1_t994462369;
// System.Array
struct Il2CppArray;
// NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey1
struct U3CLoadAdU3Ec__AnonStorey1_t1337685977;
// NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey2
struct U3CLoadAdU3Ec__AnonStorey2_t1337685978;
// NendUnityPlugin.AD.Native.NendAdNativeImage
struct NendAdNativeImage_t2295075562;
// NendUnityPlugin.AD.Native.NendAdNativeText
struct NendAdNativeText_t87332621;
// NendUnityPlugin.AD.Native.Utils.NendAdLogger
struct NendAdLogger_t1147818846;
// System.Timers.Timer
struct Timer_t1767341190;
// System.Timers.ElapsedEventHandler
struct ElapsedEventHandler_t976055006;
// System.Timers.ElapsedEventArgs
struct ElapsedEventArgs_t3048571484;
// NendUnityPlugin.AD.Native.Utils.TextureLoader
struct TextureLoader_t3414853624;
// NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0
struct U3CLoadTextureU3Ec__Iterator0_t2469200630;
// NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache
struct TextureCache_t3323732960;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>
struct Dictionary_2_t3625702484;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// NendUnityPlugin.AD.Native.Utils.UIRenderer
struct UIRenderer_t575704595;
// NendUnityPlugin.AD.Native.Utils.UIRenderer/<TryRenderImage>c__AnonStorey0
struct U3CTryRenderImageU3Ec__AnonStorey0_t4270539830;
// NendUnityPlugin.AD.NendAd
struct NendAd_t284737821;
// NendUnityPlugin.Common.Gravity[]
struct GravityU5BU5D_t868584943;
// NendUnityPlugin.AD.NendAd/Margin
struct Margin_t772722833;
// NendUnityPlugin.AD.NendAdBanner
struct NendAdBanner_t489953245;
// NendUnityPlugin.Platform.NendAdBannerInterface
struct NendAdBannerInterface_t524056774;
// System.EventHandler`1<NendUnityPlugin.Common.NendAdErrorEventArgs>
struct EventHandler_1_t1314708246;
// NendUnityPlugin.Layout.NendAdLayoutBuilder
struct NendAdLayoutBuilder_t1695755985;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Char[]
struct CharU5BU5D_t3528271667;
// NendUnityPlugin.Common.NendAdErrorEventArgs
struct NendAdErrorEventArgs_t3390548813;
// System.EventHandler`1<System.Object>
struct EventHandler_1_t1004265597;
// NendUnityPlugin.Platform.NendAdInterstitialInterface
struct NendAdInterstitialInterface_t638097589;
// System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs>
struct EventHandler_1_t3479984044;
// System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialClickEventArgs>
struct EventHandler_1_t3193289641;
// System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialShowEventArgs>
struct EventHandler_1_t1007381714;
// NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs
struct NendAdInterstitialLoadEventArgs_t1260857315;
// NendUnityPlugin.AD.NendAdInterstitialClickEventArgs
struct NendAdInterstitialClickEventArgs_t974162912;
// NendUnityPlugin.AD.NendAdInterstitialShowEventArgs
struct NendAdInterstitialShowEventArgs_t3083222281;
// NendUnityPlugin.AD.Video.ErrorVideoAdCallbackArgments
struct ErrorVideoAdCallbackArgments_t3139399691;
// NendUnityPlugin.AD.Video.VideoAdCallbackArgments
struct VideoAdCallbackArgments_t2567701888;
// NendUnityPlugin.AD.Video.NendAdInterstitialVideo
struct NendAdInterstitialVideo_t1383172729;
// NendUnityPlugin.AD.Video.NendAdVideo
struct NendAdVideo_t1781712043;
// NendUnityPlugin.AD.Video.NendAdRewardedItem
struct NendAdRewardedItem_t3964314288;
// NendUnityPlugin.AD.Video.NendAdRewardedVideo
struct NendAdRewardedVideo_t3161671531;
// NendUnityPlugin.AD.Video.NendAdRewardedVideo/NendAdVideoRewarded
struct NendAdVideoRewarded_t687064503;
// NendUnityPlugin.AD.Video.RewardedVideoAdCallbackArgments
struct RewardedVideoAdCallbackArgments_t3338436455;
// NendUnityPlugin.AD.Video.NendAdUserFeature
struct NendAdUserFeature_t4152365987;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3384741;
// System.Collections.Generic.Dictionary`2<System.String,System.Double>
struct Dictionary_2_t379921662;
// System.Collections.Generic.Dictionary`2<System.Object,System.Double>
struct Dictionary_2_t1942071647;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t4177511560;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1444694249;
// NendUnityPlugin.Platform.iOS.IOSUserFeature
struct IOSUserFeature_t2237018260;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoLoaded
struct NendAdVideoLoaded_t1357133087;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToLoad
struct NendAdVideoFailedToLoad_t1837550073;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToPlay
struct NendAdVideoFailedToPlay_t4265396701;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoShown
struct NendAdVideoShown_t3987677121;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick
struct NendAdVideoClick_t895736144;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClosed
struct NendAdVideoClosed_t464199100;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoCompleted
struct NendAdVideoCompleted_t3646835482;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoInformationClick
struct NendAdVideoInformationClick_t172780505;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStarted
struct NendAdVideoStarted_t2144365953;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStopped
struct NendAdVideoStopped_t3890769867;
extern const Il2CppType* BackButtonUtil_t2880819819_0_0_0_var;
extern Il2CppClass* BackButtonUtil_t2880819819_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t631007953_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4026408513;
extern const uint32_t BackButtonUtil_get_Instance_m1724009932_MetadataUsageId;
extern Il2CppClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern const uint32_t BackButtonUtil_GetBackKey_m3924949722_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisText_t1901882714_m2114913816_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1512031223;
extern const uint32_t BestScore_Start_m1526838621_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4110847719;
extern const uint32_t ButtonHome_OnMouseUpAsButton_m2474744170_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var;
extern const uint32_t ButtonHomeOver_Start_m1303276814_MetadataUsageId;
extern Il2CppClass* PlayerControl_t1977005294_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider2D_t2806799626_m3391829639_MethodInfo_var;
extern const uint32_t ButtonHomeOver_Update_m519570484_MetadataUsageId;
extern const uint32_t ButtonHomeOver_OnMouseExit_m2701229126_MetadataUsageId;
extern const uint32_t ButtonHomeOver_OnMouseDown_m1033544281_MetadataUsageId;
extern const uint32_t ButtonHomeOver_OnMouseUpAsButton_m3020000816_MetadataUsageId;
extern Il2CppClass* GameScreen_t1800529819_il2cpp_TypeInfo_var;
extern const uint32_t ButtonPause_OnMouseUpAsButton_m14291759_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3105844272;
extern const uint32_t ButtonPlay_OnMouseUpAsButton_m3412315649_MetadataUsageId;
extern const uint32_t ButtonReplay_OnMouseUpAsButton_m819792831_MetadataUsageId;
extern const uint32_t ButtonReplayOver_Start_m966175552_MetadataUsageId;
extern const uint32_t ButtonReplayOver_Update_m2671482342_MetadataUsageId;
extern const uint32_t ButtonReplayOver_OnMouseExit_m475027834_MetadataUsageId;
extern const uint32_t ButtonReplayOver_OnMouseDown_m2762209401_MetadataUsageId;
extern const uint32_t ButtonReplayOver_OnMouseUpAsButton_m3733030786_MetadataUsageId;
extern Il2CppClass* MenuScreen_t742580752_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral761323491;
extern const uint32_t ButtonResume_OnMouseUpAsButton_m4236015_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisRenderer_t2627027031_m2651633905_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2972356645;
extern const uint32_t ButtonSound_Start_m3676977402_MetadataUsageId;
extern const uint32_t ButtonSound_OnMouseUpAsButton_m3356435928_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisAudioSource_t3935305588_m2869327390_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2154226729;
extern const uint32_t Coin_Start_m2748608350_MetadataUsageId;
extern Il2CppClass* Physics2D_t1528932956_il2cpp_TypeInfo_var;
extern Il2CppClass* Score_t2516855617_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3128803744;
extern Il2CppCodeGenString* _stringLiteral2261822918;
extern Il2CppCodeGenString* _stringLiteral2228767570;
extern const uint32_t Coin_Update_m1064422578_MetadataUsageId;
extern const uint32_t DontDestroy_Awake_m3302783692_MetadataUsageId;
extern Il2CppClass* BoxCollider2D_t3581341831_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObjectU5BU5D_t3328599146_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1113636619_m3006960551_MethodInfo_var;
extern const uint32_t GameScreen_Start_m2902530691_MetadataUsageId;
extern const uint32_t GameScreen_getRandomObs_m1406040211_MetadataUsageId;
extern Il2CppClass* U3CchangeSpeedU3Ec__Iterator0_t85360554_il2cpp_TypeInfo_var;
extern const uint32_t GameScreen_changeSpeed_m1491875547_MetadataUsageId;
extern Il2CppClass* U3CchangeScoreU3Ec__Iterator1_t982768640_il2cpp_TypeInfo_var;
extern const uint32_t GameScreen_changeScore_m2232000133_MetadataUsageId;
extern Il2CppClass* U3ChideInfoU3Ec__Iterator2_t3795124997_il2cpp_TypeInfo_var;
extern const uint32_t GameScreen_hideInfo_m3665555316_MetadataUsageId;
extern Il2CppClass* U3CshowIfGameOverU3Ec__Iterator3_t966372778_il2cpp_TypeInfo_var;
extern const uint32_t GameScreen_showIfGameOver_m3115583330_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873_MethodInfo_var;
extern const uint32_t GameScreen_Update_m3339885975_MetadataUsageId;
extern Il2CppClass* NendAdInterstitial_t1417649638_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4053195286;
extern const uint32_t GameScreen_gameOverScene_m1998749941_MetadataUsageId;
extern const uint32_t GameScreen_RandomGap_m3185011755_MetadataUsageId;
extern const uint32_t GameScreen__cctor_m841907668_MetadataUsageId;
extern Il2CppClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern const uint32_t U3CchangeScoreU3Ec__Iterator1_MoveNext_m82251225_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const uint32_t U3CchangeScoreU3Ec__Iterator1_Reset_m2031744076_MetadataUsageId;
extern const uint32_t U3CchangeSpeedU3Ec__Iterator0_MoveNext_m2181632746_MetadataUsageId;
extern const uint32_t U3CchangeSpeedU3Ec__Iterator0_Reset_m1042803279_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1958592251;
extern const uint32_t U3ChideInfoU3Ec__Iterator2_MoveNext_m4283311606_MetadataUsageId;
extern const uint32_t U3ChideInfoU3Ec__Iterator2_Reset_m3294201329_MetadataUsageId;
extern const uint32_t U3CshowIfGameOverU3Ec__Iterator3_MoveNext_m904246463_MetadataUsageId;
extern const uint32_t U3CshowIfGameOverU3Ec__Iterator3_Reset_m1670965431_MetadataUsageId;
extern Il2CppClass* Ground_t4133628138_il2cpp_TypeInfo_var;
extern const uint32_t Ground_Start_m775848767_MetadataUsageId;
extern const uint32_t Ground_Update_m2247374405_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3197466134;
extern Il2CppCodeGenString* _stringLiteral1596542974;
extern const uint32_t HCAnimatedBanner__ctor_m2740142623_MetadataUsageId;
extern const uint32_t HCAnimatedBanner_get_transitionLink_m3352356618_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisRawImage_t3182918964_m527061191_MethodInfo_var;
extern const uint32_t HCAnimatedBanner_Awake_m3336125038_MetadataUsageId;
extern Il2CppClass* U3CLoadJsonDataU3Ec__Iterator0_t1810388169_il2cpp_TypeInfo_var;
extern const uint32_t HCAnimatedBanner_LoadJsonData_m2260823519_MetadataUsageId;
extern Il2CppClass* U3CLoadTextureU3Ec__Iterator1_t1051594053_il2cpp_TypeInfo_var;
extern const uint32_t HCAnimatedBanner_LoadTexture_m946452470_MetadataUsageId;
extern Il2CppClass* U3CAnimateBannerU3Ec__Iterator2_t688012631_il2cpp_TypeInfo_var;
extern const uint32_t HCAnimatedBanner_AnimateBanner_m3687925943_MetadataUsageId;
extern const uint32_t HCAnimatedBanner_ApplyTexture_m3563656807_MetadataUsageId;
extern Il2CppClass* U3CAnimateBannerU3Ec__AnonStorey3_t102123587_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern Il2CppClass* RectU5BU5D_t2936723554_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1820288298_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m335550220_MethodInfo_var;
extern const MethodInfo* U3CAnimateBannerU3Ec__AnonStorey3_U3CU3Em__0_m1863904840_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3848706282_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisRect_t2360479859_TisRect_t2360479859_m1309108196_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisRect_t2360479859_m2199875962_MethodInfo_var;
extern const uint32_t U3CAnimateBannerU3Ec__Iterator2_MoveNext_m987987274_MetadataUsageId;
extern const uint32_t U3CAnimateBannerU3Ec__Iterator2_Reset_m548285055_MetadataUsageId;
extern Il2CppClass* WWW_t3688466362_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t3815285241_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonMapper_ToObject_TisAnimBannerDataU5BU5D_t1736877454_m3809780039_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisAnimBannerData_t3749323191_m1433232041_MethodInfo_var;
extern const uint32_t U3CLoadJsonDataU3Ec__Iterator0_MoveNext_m859263122_MetadataUsageId;
extern const uint32_t U3CLoadJsonDataU3Ec__Iterator0_Reset_m735945691_MetadataUsageId;
extern Il2CppClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadJsonDataU3Ec__Iterator0_U3CU3E__Finally0_m3163288294_MetadataUsageId;
extern Il2CppClass* Texture2D_t3840446185_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m3914099077_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3196920305;
extern Il2CppCodeGenString* _stringLiteral3452614529;
extern const uint32_t U3CLoadTextureU3Ec__Iterator1_MoveNext_m89920283_MetadataUsageId;
extern const uint32_t U3CLoadTextureU3Ec__Iterator1_Reset_m3618031411_MetadataUsageId;
extern const uint32_t U3CLoadTextureU3Ec__Iterator1_U3CU3E__Finally0_m2209114808_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t3704657025_m3396022872_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral435220204;
extern const uint32_t IpadAspectRation_Start_m3895859990_MetadataUsageId;
extern const uint32_t MenuScreen_Start_m2494847976_MetadataUsageId;
extern const uint32_t MenuScreen_Update_m1428068712_MetadataUsageId;
extern const uint32_t MenuScreen__cctor_m3203852085_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral209761000;
extern const uint32_t MovingBall_Start_m2127453236_MetadataUsageId;
extern Il2CppClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2284871897;
extern const uint32_t MovingBall_Update_m513137904_MetadataUsageId;
extern const uint32_t MovingBall_MoveBall_m4217469841_MetadataUsageId;
extern const uint32_t MovingBg_Start_m608075872_MetadataUsageId;
extern const uint32_t MovingBg_Update_m3027059532_MetadataUsageId;
extern Il2CppClass* NendAdFullBoardLoaded_t3830347218_il2cpp_TypeInfo_var;
extern const uint32_t NendAdFullBoard_add_AdLoaded_m3684095882_MetadataUsageId;
extern const uint32_t NendAdFullBoard_remove_AdLoaded_m3648953114_MetadataUsageId;
extern Il2CppClass* NendAdFullBoardFailedToLoad_t4155484538_il2cpp_TypeInfo_var;
extern const uint32_t NendAdFullBoard_add_AdFailedToLoad_m1862710807_MetadataUsageId;
extern const uint32_t NendAdFullBoard_remove_AdFailedToLoad_m3107845605_MetadataUsageId;
extern Il2CppClass* NendAdFullBoardShown_t3059274368_il2cpp_TypeInfo_var;
extern const uint32_t NendAdFullBoard_add_AdShown_m388710219_MetadataUsageId;
extern const uint32_t NendAdFullBoard_remove_AdShown_m2186095239_MetadataUsageId;
extern Il2CppClass* NendAdFullBoardClick_t3385596145_il2cpp_TypeInfo_var;
extern const uint32_t NendAdFullBoard_add_AdClicked_m4163517606_MetadataUsageId;
extern const uint32_t NendAdFullBoard_remove_AdClicked_m1933847800_MetadataUsageId;
extern Il2CppClass* NendAdFullBoardDismiss_t136425400_il2cpp_TypeInfo_var;
extern const uint32_t NendAdFullBoard_add_AdDismissed_m2257091698_MetadataUsageId;
extern const uint32_t NendAdFullBoard_remove_AdDismissed_m899205779_MetadataUsageId;
extern Il2CppClass* IOSFullBoardAd_t1375839176_il2cpp_TypeInfo_var;
extern const uint32_t NendAdFullBoard_NewFullBoardAd_m1504519230_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern Il2CppClass* NendAdLogger_t117402011_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3606670000;
extern const uint32_t NendAdFullBoard_Load_m3769100623_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1566380239;
extern Il2CppCodeGenString* _stringLiteral1681923921;
extern const uint32_t NendAdFullBoard_Show_m3946874812_MetadataUsageId;
extern Il2CppClass* FullBoardAdErrorType_t1510350271_il2cpp_TypeInfo_var;
extern const uint32_t NendAdFullBoardFailedToLoad_BeginInvoke_m2707068486_MetadataUsageId;
extern const MethodInfo* UnityEvent_1__ctor_m1367642006_MethodInfo_var;
extern const uint32_t NativeAdViewEvent__ctor_m3216875244_MetadataUsageId;
extern const MethodInfo* UnityEvent_3__ctor_m2373477420_MethodInfo_var;
extern const uint32_t NativeAdViewFailedToLoadEvent__ctor_m2356888314_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral23062073;
extern const uint32_t ClickHandler_OnDestroy_m115998400_MetadataUsageId;
extern const uint32_t ClickHandler2D_IsClicked_m2801462155_MetadataUsageId;
extern Il2CppClass* RaycastHit_t1056001966_il2cpp_TypeInfo_var;
extern const uint32_t ClickHandler3D_IsClicked_m4226707129_MetadataUsageId;
extern const uint32_t ClickHandlerGO_Update_m2258281450_MetadataUsageId;
extern Il2CppClass* EventSystem_t1003666588_il2cpp_TypeInfo_var;
extern Il2CppClass* PointerEventData_t3807901092_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t537414295_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2049947431_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m4207101203_MethodInfo_var;
extern const uint32_t ClickHandlerGO_IsPointerOverUIObject_m204886395_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral788995291;
extern const uint32_t ImpressionHandler_OnDestroy_m3250580274_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral408008545;
extern Il2CppCodeGenString* _stringLiteral3219225869;
extern Il2CppCodeGenString* _stringLiteral1204392840;
extern Il2CppCodeGenString* _stringLiteral1548563522;
extern const uint32_t ImpressionHandler_CheckImpression_m1408940149_MetadataUsageId;
extern Il2CppClass* Rect_t2360479859_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3727356580;
extern Il2CppCodeGenString* _stringLiteral2418862726;
extern Il2CppCodeGenString* _stringLiteral1531524721;
extern Il2CppCodeGenString* _stringLiteral3481691899;
extern Il2CppCodeGenString* _stringLiteral1878511479;
extern Il2CppCodeGenString* _stringLiteral1491405942;
extern Il2CppCodeGenString* _stringLiteral2583144425;
extern Il2CppCodeGenString* _stringLiteral2946073682;
extern Il2CppCodeGenString* _stringLiteral2767064411;
extern const uint32_t ImpressionHandler_CheckViewablePercentage_m3622756226_MetadataUsageId;
extern Il2CppClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3054536847;
extern Il2CppCodeGenString* _stringLiteral1569617349;
extern const uint32_t ImpressionHandlerGO_IsViewable_m2408579625_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisCollider_t1773347010_m69509314_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3582570584;
extern Il2CppCodeGenString* _stringLiteral3450648441;
extern const uint32_t ImpressionHandlerGO_GetBounds_m2495302509_MetadataUsageId;
extern Il2CppClass* Vector2U5BU5D_t1457185986_il2cpp_TypeInfo_var;
extern const uint32_t ImpressionHandlerGO_GetObjectRect_m1352629183_MetadataUsageId;
extern Il2CppClass* Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var;
extern const uint32_t ImpressionHandlerUI__ctor_m1750883783_MetadataUsageId;
extern const MethodInfo* Component_GetComponentInParent_TisCanvas_t3310196443_m1339529724_MethodInfo_var;
extern const uint32_t ImpressionHandlerUI_Start_m3327745437_MetadataUsageId;
extern Il2CppClass* U3CIsViewableU3Ec__AnonStorey0_t3542390976_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransform_t3704657025_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t398065169_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvasGroup_t4083511760_m1030459026_MethodInfo_var;
extern const MethodInfo* U3CIsViewableU3Ec__AnonStorey0_U3CU3Em__0_m2538634596_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m134789189_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisVector3_t3722313464_TisVector2_t2156229523_m2491549993_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisVector2_t2156229523_m3384610930_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral361083404;
extern Il2CppCodeGenString* _stringLiteral3092120028;
extern Il2CppCodeGenString* _stringLiteral2129735177;
extern Il2CppCodeGenString* _stringLiteral3764436550;
extern const uint32_t ImpressionHandlerUI_IsViewable_m1581305922_MetadataUsageId;
extern Il2CppClass* RectTransformUtility_t1743242446_il2cpp_TypeInfo_var;
extern const uint32_t U3CIsViewableU3Ec__AnonStorey0_U3CU3Em__0_m2538634596_MetadataUsageId;
extern Il2CppClass* HashSet_1_t2527432003_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1__ctor_m4032723476_MethodInfo_var;
extern const uint32_t NativeAd__ctor_m646989586_MetadataUsageId;
extern Il2CppClass* EventHandler_t1348719766_il2cpp_TypeInfo_var;
extern const uint32_t NativeAd_add_AdClicked_m2857805307_MetadataUsageId;
extern const uint32_t NativeAd_remove_AdClicked_m170162276_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral996763665;
extern const uint32_t NativeAd_Dispose_m2731176596_MetadataUsageId;
extern Il2CppClass* NotImplementedException_t3489357830_il2cpp_TypeInfo_var;
extern const uint32_t NativeAd_get_ShortText_m3891001356_MetadataUsageId;
extern const uint32_t NativeAd_get_LongText_m2780726328_MetadataUsageId;
extern const uint32_t NativeAd_get_PromotionUrl_m3744682610_MetadataUsageId;
extern const uint32_t NativeAd_get_PromotionName_m3390834270_MetadataUsageId;
extern const uint32_t NativeAd_get_ActionButtonText_m3270850130_MetadataUsageId;
extern const uint32_t NativeAd_get_AdImageUrl_m71574607_MetadataUsageId;
extern const uint32_t NativeAd_get_LogoImageUrl_m438093028_MetadataUsageId;
extern const uint32_t NativeAd_GetAdvertisingExplicitlyText_m2921118262_MetadataUsageId;
extern const MethodInfo* Action_1_Invoke_m400472198_MethodInfo_var;
extern const uint32_t NativeAd_LoadAdImage_m3618624557_MetadataUsageId;
extern const uint32_t NativeAd_LoadLogoImage_m717757564_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t1264377477_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisClickHandler_t29098297_m4091602126_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImpressionHandler_t4278309981_m2269228889_MethodInfo_var;
extern const MethodInfo* NativeAd_U3CActivateU3Em__0_m3793495835_MethodInfo_var;
extern const MethodInfo* NativeAd_U3CActivateU3Em__1_m3793561371_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1094905266;
extern const uint32_t NativeAd_Activate_m1759400131_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1728766118;
extern const uint32_t NativeAd_Into_m1246524646_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponentInParent_TisCanvas_t3310196443_m3114266371_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisClickHandlerUI_t258087360_m1730712142_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisClickHandler3D_t283384251_m1758828144_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisClickHandler2D_t2622036411_m1758828177_MethodInfo_var;
extern const MethodInfo* HashSet_1_Add_m3879467893_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3039072531;
extern const uint32_t NativeAd_AddClickHandler_m3246524294_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisImpressionHandlerUI_t188317754_m2131962076_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisImpressionHandlerGO_t188448812_m1290658942_MethodInfo_var;
extern const MethodInfo* NativeAd_U3CAddImpressionHandlerU3Em__2_m2583096591_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2268994489;
extern const uint32_t NativeAd_AddImpressionHandler_m742031269_MetadataUsageId;
extern const MethodInfo* HashSet_1_Remove_m1688024103_MethodInfo_var;
extern const uint32_t NativeAd_DestroyHandler_m1214946435_MetadataUsageId;
extern const uint32_t NativeAd_DestroyHandlers_m3057256890_MetadataUsageId;
extern Il2CppClass* NativeAd_t775405424_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2922669422_il2cpp_TypeInfo_var;
extern const MethodInfo* NativeAd_U3CDestroyImpressionHandlersU3Em__3_m3316364388_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2243427934_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisMonoBehaviour_t3962482529_m3892073355_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisMonoBehaviour_t3962482529_m4146969897_MethodInfo_var;
extern const uint32_t NativeAd_DestroyImpressionHandlers_m3505163566_MetadataUsageId;
extern const uint32_t NativeAd_DestroyAllHandlers_m3923046273_MetadataUsageId;
extern Il2CppClass* EventArgs_t3591816995_il2cpp_TypeInfo_var;
extern const uint32_t NativeAd_U3CActivateU3Em__0_m3793495835_MetadataUsageId;
extern Il2CppClass* ImpressionHandler_t4278309981_il2cpp_TypeInfo_var;
extern const uint32_t NativeAd_U3CDestroyImpressionHandlersU3Em__3_m3316364388_MetadataUsageId;
extern Il2CppClass* Queue_1_t414173832_il2cpp_TypeInfo_var;
extern Il2CppClass* NendAdMainThreadWorker_t3569218590_il2cpp_TypeInfo_var;
extern Il2CppClass* SimpleTimer_t4075384413_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m2088921833_MethodInfo_var;
extern const MethodInfo* NativeAdClient_U3CNativeAdClientU3Em__0_m3135026674_MethodInfo_var;
extern const uint32_t NativeAdClient__ctor_m994092742_MetadataUsageId;
extern const MethodInfo* Queue_1_Clear_m3541320262_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4115096556;
extern const uint32_t NativeAdClient_Dispose_m1471321895_MetadataUsageId;
extern const uint32_t NativeAdClient_LoadNativeAd_m2739670713_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2269776268;
extern const uint32_t NativeAdClient_EnableAutoReload_m1436284653_MetadataUsageId;
extern const MethodInfo* Queue_1_Enqueue_m2253724761_MethodInfo_var;
extern const uint32_t NativeAdClient_EnqueueCallback_m1636518071_MetadataUsageId;
extern Il2CppClass* U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321_il2cpp_TypeInfo_var;
extern Il2CppClass* INativeAd_t3252869353_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_get_Count_m685314720_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m1251791087_MethodInfo_var;
extern const MethodInfo* U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_U3CU3Em__0_m3728349486_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1896225283;
extern const uint32_t NativeAdClient_DeliverResponseOnMainThread_m863452579_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3988015567;
extern const uint32_t NativeAdClient_IsGifImage_m3010410997_MetadataUsageId;
extern const MethodInfo* Action_3_Invoke_m1359513622_MethodInfo_var;
extern const uint32_t U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_U3CU3Em__0_m3728349486_MetadataUsageId;
extern Il2CppClass* IOSNativeAdClient_t1155913684_il2cpp_TypeInfo_var;
extern const uint32_t NativeAdClientFactory_NewClient_m1082690577_MetadataUsageId;
extern const MethodInfo* NendAdNative_U3CStartU3Em__0_m1547599261_MethodInfo_var;
extern const uint32_t NendAdNative_Start_m2400952537_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2657966006;
extern const uint32_t NendAdNative_OnDestroy_m1949950267_MetadataUsageId;
extern Il2CppClass* U3CRegisterAdViewU3Ec__AnonStorey0_t799839499_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t261507339_il2cpp_TypeInfo_var;
extern Il2CppClass* NendAdNativeViewU5BU5D_t3641134489_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CRegisterAdViewU3Ec__AnonStorey0_U3CU3Em__0_m790825469_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3969891916_MethodInfo_var;
extern const MethodInfo* Enumerable_All_TisNendAdNativeView_t2014609480_m2632065831_MethodInfo_var;
extern const MethodInfo* Enumerable_Concat_TisNendAdNativeView_t2014609480_m60162064_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisNendAdNativeView_t2014609480_m230526467_MethodInfo_var;
extern const uint32_t NendAdNative_RegisterAdView_m2253752544_MetadataUsageId;
extern const MethodInfo* Array_IndexOf_TisNendAdNativeView_t2014609480_m792692718_MethodInfo_var;
extern const uint32_t NendAdNative_UnregisterAdView_m3352783016_MetadataUsageId;
extern Il2CppClass* NendAdNative_t3381739006_il2cpp_TypeInfo_var;
extern const MethodInfo* NendAdNative_U3CLoadAdU3Em__1_m4053525341_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisNendAdNativeView_t2014609480_m876349505_MethodInfo_var;
extern const MethodInfo* Enumerable_DefaultIfEmpty_TisNendAdNativeView_t2014609480_m3560641153_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803801646;
extern const uint32_t NendAdNative_LoadAd_m89270426_MetadataUsageId;
extern Il2CppClass* U3CLoadAdU3Ec__AnonStorey1_t1337685977_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CLoadAdU3Ec__AnonStorey1_U3CU3Em__0_m2659668234_MethodInfo_var;
extern const uint32_t NendAdNative_LoadAd_m2968873072_MetadataUsageId;
extern const uint32_t NendAdNative_EnableAutoReload_m3562877551_MetadataUsageId;
extern Il2CppClass* U3CLoadAdU3Ec__AnonStorey2_t1337685978_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t567914338_il2cpp_TypeInfo_var;
extern Il2CppClass* INativeAdClient_t1973857504_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CLoadAdU3Ec__AnonStorey2_U3CU3Em__0_m1756703808_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1315864708_MethodInfo_var;
extern const uint32_t NendAdNative_LoadAd_m545678891_MetadataUsageId;
extern const MethodInfo* UnityEvent_1_Invoke_m2488769038_MethodInfo_var;
extern const uint32_t NendAdNative_PostAdLoaded_m3099863989_MetadataUsageId;
extern const MethodInfo* UnityEvent_3_Invoke_m1929593389_MethodInfo_var;
extern const uint32_t NendAdNative_PostAdFailedToReceive_m837728790_MetadataUsageId;
extern const uint32_t NendAdNative_U3CLoadAdU3Em__1_m4053525341_MetadataUsageId;
extern const uint32_t U3CLoadAdU3Ec__AnonStorey1_U3CU3Em__0_m2659668234_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1978750441;
extern Il2CppCodeGenString* _stringLiteral2588296612;
extern const uint32_t U3CLoadAdU3Ec__AnonStorey2_U3CU3Em__0_m1756703808_MetadataUsageId;
extern const uint32_t U3CRegisterAdViewU3Ec__AnonStorey0_U3CU3Em__0_m790825469_MetadataUsageId;
extern Il2CppClass* Text_t1901882714_il2cpp_TypeInfo_var;
extern const uint32_t NendAdNativeText__ctor_m988110989_MetadataUsageId;
extern const MethodInfo* NendAdNativeView_OnClickAd_m2443000380_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2455803803;
extern const uint32_t NendAdNativeView_set_ShowingNativeAd_m553901933_MetadataUsageId;
extern const uint32_t NendAdNativeView_OnDestroy_m2360927530_MetadataUsageId;
extern const uint32_t NendAdNativeView_PostAdShown_m2675772046_MetadataUsageId;
extern const uint32_t NendAdNativeView_PostAdClicked_m515887988_MetadataUsageId;
extern const uint32_t NendAdLogger_set_LogLevel_m867647294_MetadataUsageId;
extern const uint32_t SimpleTimer__ctor_m1198240476_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3634757765;
extern const uint32_t SimpleTimer_Finalize_m2217791589_MetadataUsageId;
extern Il2CppClass* Timer_t1767341190_il2cpp_TypeInfo_var;
extern Il2CppClass* ElapsedEventHandler_t976055006_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t594665363_il2cpp_TypeInfo_var;
extern const MethodInfo* SimpleTimer_OnTimedEvent_m459759611_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3853653976;
extern const uint32_t SimpleTimer_Start_m3197311368_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2899771905;
extern const uint32_t SimpleTimer_Stop_m1024974171_MetadataUsageId;
extern const uint32_t SimpleTimer_OnTimedEvent_m459759611_MetadataUsageId;
extern Il2CppClass* U3CLoadTextureU3Ec__Iterator0_t2469200630_il2cpp_TypeInfo_var;
extern const uint32_t TextureLoader_LoadTexture_m3236146968_MetadataUsageId;
extern Il2CppClass* TextureCache_t3323732960_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2273156437;
extern const uint32_t U3CLoadTextureU3Ec__Iterator0_MoveNext_m3749932940_MetadataUsageId;
extern const uint32_t U3CLoadTextureU3Ec__Iterator0_Reset_m127061503_MetadataUsageId;
extern const uint32_t U3CLoadTextureU3Ec__Iterator0_U3CU3E__Finally0_m3862613709_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t3625702484_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2275355514_MethodInfo_var;
extern const uint32_t TextureCache__ctor_m3754485370_MetadataUsageId;
extern const uint32_t TextureCache_get_Instance_m1280202405_MetadataUsageId;
extern const MethodInfo* Dictionary_2_ContainsKey_m1577318754_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2202019062_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2958555957;
extern const uint32_t TextureCache_Get_m1883981837_MetadataUsageId;
extern const MethodInfo* Dictionary_2_set_Item_m432567873_MethodInfo_var;
extern const uint32_t TextureCache_Put_m3192621705_MetadataUsageId;
extern const uint32_t UIRenderer_TryRenderText_m3187668248_MetadataUsageId;
extern Il2CppClass* U3CTryRenderImageU3Ec__AnonStorey0_t4270539830_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t4012913780_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CTryRenderImageU3Ec__AnonStorey0_U3CU3Em__0_m1165082455_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3185781518_MethodInfo_var;
extern const uint32_t UIRenderer_TryRenderImage_m737991264_MetadataUsageId;
extern const uint32_t UIRenderer_TryClearText_m4217291438_MetadataUsageId;
extern const uint32_t UIRenderer_TryClearImage_m1984313908_MetadataUsageId;
extern const uint32_t NendAd_Awake_m3509898713_MetadataUsageId;
extern const uint32_t NendAdBanner_add_AdLoaded_m3453699443_MetadataUsageId;
extern const uint32_t NendAdBanner_remove_AdLoaded_m4239923732_MetadataUsageId;
extern Il2CppClass* EventHandler_1_t1314708246_il2cpp_TypeInfo_var;
extern const uint32_t NendAdBanner_add_AdFailedToReceive_m3291298239_MetadataUsageId;
extern const uint32_t NendAdBanner_remove_AdFailedToReceive_m1066821576_MetadataUsageId;
extern const uint32_t NendAdBanner_add_AdReceived_m1155793081_MetadataUsageId;
extern const uint32_t NendAdBanner_remove_AdReceived_m1034858748_MetadataUsageId;
extern const uint32_t NendAdBanner_add_AdClicked_m4109704752_MetadataUsageId;
extern const uint32_t NendAdBanner_remove_AdClicked_m1818686474_MetadataUsageId;
extern const uint32_t NendAdBanner_add_AdBacked_m3794188279_MetadataUsageId;
extern const uint32_t NendAdBanner_remove_AdBacked_m2364434571_MetadataUsageId;
extern const uint32_t NendAdBanner_add_InformationClicked_m459008816_MetadataUsageId;
extern const uint32_t NendAdBanner_remove_InformationClicked_m3480265601_MetadataUsageId;
extern Il2CppClass* NendAdBannerInterface_t524056774_il2cpp_TypeInfo_var;
extern const uint32_t NendAdBanner_Create_m1966987288_MetadataUsageId;
extern const uint32_t NendAdBanner_Show_m756177697_MetadataUsageId;
extern const uint32_t NendAdBanner_Hide_m4189201796_MetadataUsageId;
extern const uint32_t NendAdBanner_Resume_m858612223_MetadataUsageId;
extern const uint32_t NendAdBanner_Pause_m3243031251_MetadataUsageId;
extern const uint32_t NendAdBanner_Destroy_m720695405_MetadataUsageId;
extern Il2CppClass* NendAdDefaultLayoutBuilder_t3894915050_il2cpp_TypeInfo_var;
extern Il2CppClass* NendAdLayoutBuilder_t1695755985_il2cpp_TypeInfo_var;
extern const uint32_t NendAdBanner_Layout_m328065890_MetadataUsageId;
extern Il2CppClass* StringBuilder_t1712802186_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3452614550;
extern Il2CppCodeGenString* _stringLiteral4002445229;
extern Il2CppCodeGenString* _stringLiteral3875954633;
extern const uint32_t NendAdBanner_MakeParams_m3373626820_MetadataUsageId;
extern const uint32_t NendAdBanner_NendAdView_OnFinishLoad_m362932734_MetadataUsageId;
extern Il2CppClass* CharU5BU5D_t3528271667_il2cpp_TypeInfo_var;
extern Il2CppClass* NendAdErrorEventArgs_t3390548813_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m1615702581_MethodInfo_var;
extern const uint32_t NendAdBanner_NendAdView_OnFailToReceiveAd_m3984165535_MetadataUsageId;
extern const uint32_t NendAdBanner_NendAdView_OnReceiveAd_m3550334308_MetadataUsageId;
extern const uint32_t NendAdBanner_NendAdView_OnClickAd_m2773684645_MetadataUsageId;
extern const uint32_t NendAdBanner_NendAdView_OnDismissScreen_m2389590666_MetadataUsageId;
extern const uint32_t NendAdBanner_NendAdView_OnClickInformation_m2444220285_MetadataUsageId;
extern const uint32_t NendAdInterstitial_get_Instance_m3378847411_MetadataUsageId;
extern Il2CppClass* NendAdInterstitialInterface_t638097589_il2cpp_TypeInfo_var;
extern const uint32_t NendAdInterstitial_set_IsAutoReloadEnabled_m1382953156_MetadataUsageId;
extern Il2CppClass* EventHandler_1_t3479984044_il2cpp_TypeInfo_var;
extern const uint32_t NendAdInterstitial_add_AdLoaded_m3211223500_MetadataUsageId;
extern const uint32_t NendAdInterstitial_remove_AdLoaded_m2017454671_MetadataUsageId;
extern Il2CppClass* EventHandler_1_t3193289641_il2cpp_TypeInfo_var;
extern const uint32_t NendAdInterstitial_add_AdClicked_m1765401919_MetadataUsageId;
extern const uint32_t NendAdInterstitial_remove_AdClicked_m3393357395_MetadataUsageId;
extern Il2CppClass* EventHandler_1_t1007381714_il2cpp_TypeInfo_var;
extern const uint32_t NendAdInterstitial_add_AdShown_m4252266033_MetadataUsageId;
extern const uint32_t NendAdInterstitial_remove_AdShown_m2632188688_MetadataUsageId;
extern const uint32_t NendAdInterstitial_Awake_m4029310310_MetadataUsageId;
extern const uint32_t NendAdInterstitial_Load_m3683620854_MetadataUsageId;
extern const uint32_t NendAdInterstitial_Show_m3619914196_MetadataUsageId;
extern const uint32_t NendAdInterstitial_Show_m2577433912_MetadataUsageId;
extern const uint32_t NendAdInterstitial_Dismiss_m326668577_MetadataUsageId;
extern Il2CppClass* NendAdInterstitialLoadEventArgs_t1260857315_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m1611654412_MethodInfo_var;
extern const uint32_t NendAdInterstitial_NendAdInterstitial_OnFinishLoad_m2262400418_MetadataUsageId;
extern Il2CppClass* NendAdInterstitialClickEventArgs_t974162912_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m2443591581_MethodInfo_var;
extern const uint32_t NendAdInterstitial_NendAdInterstitial_OnClickAd_m1739663766_MetadataUsageId;
extern Il2CppClass* NendAdInterstitialShowEventArgs_t3083222281_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m1676777043_MethodInfo_var;
extern const uint32_t NendAdInterstitial_NendAdInterstitial_OnShowAd_m1206226545_MetadataUsageId;
extern const uint32_t NendAdInterstitialClickEventArgs__ctor_m4030220703_MetadataUsageId;
extern const uint32_t NendAdInterstitialLoadEventArgs__ctor_m3921384839_MetadataUsageId;
extern const uint32_t NendAdInterstitialShowEventArgs__ctor_m4100517664_MetadataUsageId;
extern Il2CppClass* NendAdVideoRewarded_t687064503_il2cpp_TypeInfo_var;
extern const uint32_t NendAdRewardedVideo_add_Rewarded_m3408280237_MetadataUsageId;
extern const uint32_t NendAdRewardedVideo_remove_Rewarded_m1312421876_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t2736202052_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t379921662_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1632706988_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t4177511560_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3200964102_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m837984336_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2842384104_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m256458156_MethodInfo_var;
extern const uint32_t NendAdUserFeature__ctor_m941309312_MetadataUsageId;
extern const MethodInfo* Dictionary_2_set_Item_m3800595820_MethodInfo_var;
extern const uint32_t NendAdUserFeature_AddCustomFeature_m73758447_MetadataUsageId;
extern const MethodInfo* Dictionary_2_set_Item_m83463571_MethodInfo_var;
extern const uint32_t NendAdUserFeature_AddCustomFeature_m2842110208_MetadataUsageId;
extern const MethodInfo* Dictionary_2_set_Item_m1950792581_MethodInfo_var;
extern const uint32_t NendAdUserFeature_AddCustomFeature_m170006368_MetadataUsageId;
extern const MethodInfo* Dictionary_2_set_Item_m2347951726_MethodInfo_var;
extern const uint32_t NendAdUserFeature_AddCustomFeature_m4244280464_MetadataUsageId;
extern Il2CppClass* IOSUserFeature_t2237018260_il2cpp_TypeInfo_var;
extern const uint32_t NendAdUserFeature_NewNendAdUserFeature_m285187920_MetadataUsageId;
extern Il2CppClass* NendAdVideoLoaded_t1357133087_il2cpp_TypeInfo_var;
extern const uint32_t NendAdVideo_add_AdLoaded_m238795991_MetadataUsageId;
extern const uint32_t NendAdVideo_remove_AdLoaded_m1949947559_MetadataUsageId;
extern Il2CppClass* NendAdVideoFailedToLoad_t1837550073_il2cpp_TypeInfo_var;
extern const uint32_t NendAdVideo_add_AdFailedToLoad_m4143775789_MetadataUsageId;
extern const uint32_t NendAdVideo_remove_AdFailedToLoad_m3941862212_MetadataUsageId;
extern Il2CppClass* NendAdVideoFailedToPlay_t4265396701_il2cpp_TypeInfo_var;
extern const uint32_t NendAdVideo_add_AdFailedToPlay_m1928710374_MetadataUsageId;
extern const uint32_t NendAdVideo_remove_AdFailedToPlay_m4052357713_MetadataUsageId;
extern Il2CppClass* NendAdVideoShown_t3987677121_il2cpp_TypeInfo_var;
extern const uint32_t NendAdVideo_add_AdShown_m3117307321_MetadataUsageId;
extern const uint32_t NendAdVideo_remove_AdShown_m4142333737_MetadataUsageId;
extern Il2CppClass* NendAdVideoClick_t895736144_il2cpp_TypeInfo_var;
extern const uint32_t NendAdVideo_add_AdStarted_m459056747_MetadataUsageId;
extern const uint32_t NendAdVideo_remove_AdStarted_m2921569958_MetadataUsageId;
extern const uint32_t NendAdVideo_add_AdStopped_m2421838350_MetadataUsageId;
extern const uint32_t NendAdVideo_remove_AdStopped_m245435062_MetadataUsageId;
extern const uint32_t NendAdVideo_add_AdCompleted_m1655903682_MetadataUsageId;
extern const uint32_t NendAdVideo_remove_AdCompleted_m2522856396_MetadataUsageId;
extern const uint32_t NendAdVideo_add_AdClicked_m3092326558_MetadataUsageId;
extern const uint32_t NendAdVideo_remove_AdClicked_m3269521184_MetadataUsageId;
extern const uint32_t NendAdVideo_add_InformationClicked_m3340766747_MetadataUsageId;
extern const uint32_t NendAdVideo_remove_InformationClicked_m1284134629_MetadataUsageId;
extern Il2CppClass* NendAdVideoClosed_t464199100_il2cpp_TypeInfo_var;
extern const uint32_t NendAdVideo_add_AdClosed_m2694012098_MetadataUsageId;
extern const uint32_t NendAdVideo_remove_AdClosed_m3012898169_MetadataUsageId;
extern Il2CppClass* ErrorVideoAdCallbackArgments_t3139399691_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral207068603;
extern const uint32_t NendAdVideo_CallBack_m1751514092_MetadataUsageId;
extern const uint32_t NendAdVideoFailedToLoad_BeginInvoke_m56923607_MetadataUsageId;

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GameObject_t1113636619 * m_Items[1];

public:
	inline GameObject_t1113636619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1113636619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Rect[]
struct RectU5BU5D_t2936723554  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Rect_t2360479859  m_Items[1];

public:
	inline Rect_t2360479859  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Rect_t2360479859 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Rect_t2360479859  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Rect_t2360479859  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Rect_t2360479859 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Rect_t2360479859  value)
	{
		m_Items[index] = value;
	}
};
// HCAnimatedBanner/AnimBannerData[]
struct AnimBannerDataU5BU5D_t1736877454  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AnimBannerData_t3749323191 * m_Items[1];

public:
	inline AnimBannerData_t3749323191 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AnimBannerData_t3749323191 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AnimBannerData_t3749323191 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline AnimBannerData_t3749323191 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AnimBannerData_t3749323191 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AnimBannerData_t3749323191 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector2_t2156229523  m_Items[1];

public:
	inline Vector2_t2156229523  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t2156229523  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector3_t3722313464  m_Items[1];

public:
	inline Vector3_t3722313464  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3722313464  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MonoBehaviour_t3962482529 * m_Items[1];

public:
	inline MonoBehaviour_t3962482529 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MonoBehaviour_t3962482529 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MonoBehaviour_t3962482529 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MonoBehaviour_t3962482529 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MonoBehaviour_t3962482529 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MonoBehaviour_t3962482529 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// NendUnityPlugin.AD.Native.NendAdNativeView[]
struct NendAdNativeViewU5BU5D_t3641134489  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NendAdNativeView_t2014609480 * m_Items[1];

public:
	inline NendAdNativeView_t2014609480 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NendAdNativeView_t2014609480 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NendAdNativeView_t2014609480 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline NendAdNativeView_t2014609480 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NendAdNativeView_t2014609480 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NendAdNativeView_t2014609480 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// NendUnityPlugin.Common.Gravity[]
struct GravityU5BU5D_t868584943  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2049753423_gshared (GameObject_t1113636619 * __this, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m2735705429_gshared (Component_t1923634451 * __this, const MethodInfo* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3469369570_gshared (GameObject_t1113636619 * __this, const MethodInfo* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m1135049463_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Func`2<UnityEngine.Rect,UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3848706282_gshared (Func_2_t1820288298 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Rect,UnityEngine.Rect>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisRect_t2360479859_TisRect_t2360479859_m1309108196_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1820288298 * p1, const MethodInfo* method);
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Rect>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  RectU5BU5D_t2936723554* Enumerable_ToArray_TisRect_t2360479859_m2199875962_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
// !!0 LitJson.JsonMapper::ToObject<System.Object>(System.String)
extern "C"  Il2CppObject * JsonMapper_ToObject_TisIl2CppObject_m627569000_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t257213610 * Enumerable_ToList_TisIl2CppObject_m3517240806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern "C"  void UnityEvent_1__ctor_m4234511999_gshared (UnityEvent_1_t3961765668 * __this, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Int32,System.Object>::.ctor()
extern "C"  void UnityEvent_3__ctor_m4063448395_gshared (UnityEvent_3_t4241019141 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void List_1__ctor_m2049947431_gshared (List_1_t537414295 * __this, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C"  int32_t List_1_get_Count_m4207101203_gshared (List_1_t537414295 * __this, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m3491943679_gshared (Component_t1923634451 * __this, const MethodInfo* method);
// System.Void System.Func`2<UnityEngine.Vector3,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m134789189_gshared (Func_2_t398065169 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Vector3,UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisVector3_t3722313464_TisVector2_t2156229523_m2491549993_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t398065169 * p1, const MethodInfo* method);
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Vector2U5BU5D_t1457185986* Enumerable_ToArray_TisVector2_t2156229523_m3384610930_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C"  void HashSet_1__ctor_m4231804131_gshared (HashSet_1_t1645055638 * __this, const MethodInfo* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
extern "C"  void Action_1_Invoke_m2461023210_gshared (Action_1_t3252573759 * __this, Il2CppObject * p0, const MethodInfo* method);
// !!0 UnityEngine.GameObject::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInParent_TisIl2CppObject_m1039067679_gshared (GameObject_t1113636619 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(!0)
extern "C"  bool HashSet_1_Add_m1971460364_gshared (HashSet_1_t1645055638 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(!0)
extern "C"  bool HashSet_1_Remove_m709044238_gshared (HashSet_1_t1645055638 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3104565095_gshared (Func_2_t3759279471 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Enumerable_Where_TisIl2CppObject_m2166527282_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3759279471 * p1, const MethodInfo* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t2843939325* Enumerable_ToArray_TisIl2CppObject_m698722831_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
extern "C"  void Queue_1__ctor_m3749217910_gshared (Queue_1_t2926365658 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::Clear()
extern "C"  void Queue_1_Clear_m4070494218_gshared (Queue_1_t2926365658 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::Enqueue(!0)
extern "C"  void Queue_1_Enqueue_m1868480850_gshared (Queue_1_t2926365658 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m2496300460_gshared (Queue_1_t2926365658 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.Queue`1<System.Object>::Dequeue()
extern "C"  Il2CppObject * Queue_1_Dequeue_m3550993416_gshared (Queue_1_t2926365658 * __this, const MethodInfo* method);
// System.Void System.Action`3<System.Object,System.Int32,System.Object>::Invoke(!0,!1,!2)
extern "C"  void Action_3_Invoke_m3429238884_gshared (Action_3_t1173861992 * __this, Il2CppObject * p0, int32_t p1, Il2CppObject * p2, const MethodInfo* method);
// System.Boolean System.Linq.Enumerable::All<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  bool Enumerable_All_TisIl2CppObject_m1743658500_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3759279471 * p1, const MethodInfo* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject* Enumerable_Concat_TisIl2CppObject_m1054524938_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject* p1, const MethodInfo* method);
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m3944231312_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, Il2CppObject * p1, const MethodInfo* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::DefaultIfEmpty<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject* Enumerable_DefaultIfEmpty_TisIl2CppObject_m2788453171_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
// System.Void System.Action`3<System.Object,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m3182009741_gshared (Action_3_t1173861992 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(!0)
extern "C"  void UnityEvent_1_Invoke_m4196549529_gshared (UnityEvent_1_t3961765668 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Int32,System.Object>::Invoke(!0,!1,!2)
extern "C"  void UnityEvent_3_Invoke_m1708203693_gshared (UnityEvent_3_t4241019141 * __this, Il2CppObject * p0, int32_t p1, Il2CppObject * p2, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m2278349286_gshared (Dictionary_2_t132545152 * __this, Il2CppObject * p0, const MethodInfo* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m2714930061_gshared (Dictionary_2_t132545152 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m3474379962_gshared (Dictionary_2_t132545152 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m118522912_gshared (Action_1_t3252573759 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void System.EventHandler`1<System.Object>::Invoke(System.Object,!0)
extern "C"  void EventHandler_1_Invoke_m4124830036_gshared (EventHandler_1_t1004265597 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m2253601317_gshared (Dictionary_2_t3384741 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Double>::.ctor()
extern "C"  void Dictionary_2__ctor_m1071776077_gshared (Dictionary_2_t1942071647 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C"  void Dictionary_2__ctor_m236774955_gshared (Dictionary_2_t1444694249 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m411961606_gshared (Dictionary_2_t3384741 * __this, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Double>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m3116967895_gshared (Dictionary_2_t1942071647 * __this, Il2CppObject * p0, double p1, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m351745166_gshared (Dictionary_2_t1444694249 * __this, Il2CppObject * p0, bool p1, const MethodInfo* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1339182015 (MonoBehaviour_t3962482529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1454075600 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C"  Object_t631007953 * Object_FindObjectOfType_m1736538631 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m1780991845 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackButtonUtil::GetBackKey()
extern "C"  void BackButtonUtil_GetBackKey_m3924949722 (BackButtonUtil_t2880819819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m2296100099 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m1672997886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::LoadLevel(System.String)
extern "C"  void Application_LoadLevel_m1553385752 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C"  void UnityEvent_Invoke_m1072399001 (UnityEvent_t2581268647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m3274227256 (UnityEvent_t2581268647 * __this, UnityAction_t3245792599 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent::RemoveListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_RemoveListener_m369190218 (UnityEvent_t2581268647 * __this, UnityAction_t3245792599 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::RemoveAllListeners()
extern "C"  void UnityEventBase_RemoveAllListeners_m4146875668 (UnityEventBase_t3960448221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m2648350745 (Component_t1923634451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t1901882714_m2114913816(__this, method) ((  Text_t1901882714 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2049753423_gshared)(__this, method)
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m3299375436 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m141394615 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_timeScale()
extern "C"  float Time_get_timeScale_m466418001 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Time::set_timeScale(System.Single)
extern "C"  void Time_set_timeScale_m148135804 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m3660838673 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m1673432820 (Color_t2555686324 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(__this, method) ((  Renderer_t2627027031 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2049753423_gshared)(__this, method)
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t340375123 * Renderer_get_material_m909181218 (Renderer_t2627027031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Material::get_color()
extern "C"  Color_t2555686324  Material_get_color_m1173399845 (Material_t340375123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider2D>()
#define Component_GetComponent_TisCollider2D_t2806799626_m3391829639(__this, method) ((  Collider2D_t2806799626 * (*) (Component_t1923634451 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2735705429_gshared)(__this, method)
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m3107225489 (Behaviour_t1437897464 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern "C"  void Material_set_color_m3787036372 (Material_t340375123 * __this, Color_t2555686324  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::LoadLevel(System.Int32)
extern "C"  void Application_LoadLevel_m294528718 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Stop()
extern "C"  void AudioSource_Stop_m534887514 (AudioSource_t3935305588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m3735894026 (GameObject_t1113636619 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m2606248505 (AudioSource_t3935305588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1113636619 * GameObject_Find_m3458517891 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m2921103810 (Component_t1923634451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m102368104 (Transform_t3600365921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t4157153871 * Camera_get_main_m882983993 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Subtraction_m2566684344 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1197556204 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::ViewportToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Camera_ViewportToWorldPoint_m3480887959 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, method) ((  Renderer_t2627027031 * (*) (Component_t1923634451 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2735705429_gshared)(__this, method)
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C"  Bounds_t2266837910  Renderer_get_bounds_m2197226282 (Renderer_t2627027031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t3722313464  Bounds_get_size_m1171376090 (Bounds_t2266837910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2361854178 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m393750976 (GameObject_t1113636619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m2710705614 (Renderer_t2627027031 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
extern "C"  GameObject_t1113636619 * GameObject_get_gameObject_m1412666805 (GameObject_t1113636619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C"  Transform_t3600365921 * Transform_Find_m54355842 (Transform_t3600365921 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.AudioSource>()
#define GameObject_AddComponent_TisAudioSource_t3935305588_m2869327390(__this, method) ((  AudioSource_t3935305588 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
extern "C"  void AudioSource_set_playOnAwake_m3967847264 (AudioSource_t3935305588 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_rolloffMode(UnityEngine.AudioRolloffMode)
extern "C"  void AudioSource_set_rolloffMode_m1566118045 (AudioSource_t3935305588 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C"  void AudioSource_set_clip_m1255174161 (AudioSource_t3935305588 * __this, AudioClip_t3680889665 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  Vector2_op_Implicit_m1304503157 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern "C"  int32_t LayerMask_NameToLayer_m4006107104 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C"  RaycastHit2D_t2279581989  Physics2D_Linecast_m3585412810 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RaycastHit2D::op_Implicit(UnityEngine.RaycastHit2D)
extern "C"  bool RaycastHit2D_op_Implicit_m2486231880 (Il2CppObject * __this /* static, unused */, RaycastHit2D_t2279581989  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m3153277066 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider2D>()
#define GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(__this, method) ((  Collider2D_t2806799626 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2049753423_gshared)(__this, method)
// UnityEngine.GameObject GameScreen::getRandomObs()
extern "C"  GameObject_t1113636619 * GameScreen_getRandomObs_m1406040211 (GameScreen_t1800529819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t2301928331  Quaternion_get_identity_m3578010038 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1113636619_m3006960551(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1113636619 * (*) (Il2CppObject * /* static, unused */, GameObject_t1113636619 *, Vector3_t3722313464 , Quaternion_t2301928331 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m1135049463_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 GameScreen::RandomGap()
extern "C"  int32_t GameScreen_RandomGap_m3185011755 (GameScreen_t1800529819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameScreen::changeSpeed()
extern "C"  Il2CppObject * GameScreen_changeSpeed_m1491875547 (GameScreen_t1800529819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m4001331470 (MonoBehaviour_t3962482529 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameScreen::changeScore()
extern "C"  Il2CppObject * GameScreen_changeScore_m2232000133 (GameScreen_t1800529819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameScreen::showIfGameOver()
extern "C"  Il2CppObject * GameScreen_showIfGameOver_m3115583330 (GameScreen_t1800529819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameScreen::hideInfo()
extern "C"  Il2CppObject * GameScreen_hideInfo_m3665555316 (GameScreen_t1800529819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m110872341 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScreen/<changeSpeed>c__Iterator0::.ctor()
extern "C"  void U3CchangeSpeedU3Ec__Iterator0__ctor_m857096498 (U3CchangeSpeedU3Ec__Iterator0_t85360554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScreen/<changeScore>c__Iterator1::.ctor()
extern "C"  void U3CchangeScoreU3Ec__Iterator1__ctor_m2602245183 (U3CchangeScoreU3Ec__Iterator1_t982768640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScreen/<hideInfo>c__Iterator2::.ctor()
extern "C"  void U3ChideInfoU3Ec__Iterator2__ctor_m1502287029 (U3ChideInfoU3Ec__Iterator2_t3795124997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScreen/<showIfGameOver>c__Iterator3::.ctor()
extern "C"  void U3CshowIfGameOverU3Ec__Iterator3__ctor_m546072851 (U3CshowIfGameOverU3Ec__Iterator3_t966372778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<ObsDownCollisionCheck>()
#define GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(__this, method) ((  ObsDownCollisionCheck_t2541817184 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2049753423_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(__this, method) ((  SpriteRenderer_t3235626157 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2049753423_gshared)(__this, method)
// UnityEngine.Sprite UnityEngine.SpriteRenderer::get_sprite()
extern "C"  Sprite_t280657092 * SpriteRenderer_get_sprite_m688744963 (SpriteRenderer_t3235626157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_set_sprite_m3489358652 (SpriteRenderer_t3235626157 * __this, Sprite_t280657092 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.BoxCollider2D::get_size()
extern "C"  Vector2_t2156229523  BoxCollider2D_get_size_m2899090007 (BoxCollider2D_t3581341831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoxCollider2D::set_size(UnityEngine.Vector2)
extern "C"  void BoxCollider2D_set_size_m2456142094 (BoxCollider2D_t3581341831 * __this, Vector2_t2156229523  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<ObsUpCollisionCheck>()
#define GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(__this, method) ((  ObsUpCollisionCheck_t2909508583 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m2752645118 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<ObsDownCollisionCheck>()
#define GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560(__this, method) ((  ObsDownCollisionCheck_t2541817184 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3469369570_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<ObsUpCollisionCheck>()
#define GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873(__this, method) ((  ObsUpCollisionCheck_t2909508583 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m1898875631 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NendUnityPlugin.AD.NendAdInterstitial NendUnityPlugin.AD.NendAdInterstitial::get_Instance()
extern "C"  NendAdInterstitial_t1417649638 * NendAdInterstitial_get_Instance_m3378847411 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.NendAdInterstitial::Show()
extern "C"  void NendAdInterstitial_Show_m3619914196 (NendAdInterstitial_t1417649638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m1898094827 (WaitForSeconds_t1699091251 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScreen::gameOverScene()
extern "C"  void GameScreen_gameOverScene_m1998749941 (GameScreen_t1800529819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3506743150 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m3562456068 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C"  void Transform_Translate_m3317641446 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2969720369 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.RawImage>()
#define Component_GetComponent_TisRawImage_t3182918964_m527061191(__this, method) ((  RawImage_t3182918964 * (*) (Component_t1923634451 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2735705429_gshared)(__this, method)
// UnityEngine.Color UnityEngine.Color::get_clear()
extern "C"  Color_t2555686324  Color_get_clear_m1773884651 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HCAnimatedBanner::LoadJsonData()
extern "C"  Il2CppObject * HCAnimatedBanner_LoadJsonData_m2260823519 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HCAnimatedBanner::LoadTexture()
extern "C"  Il2CppObject * HCAnimatedBanner_LoadTexture_m946452470 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m1038915083 (MonoBehaviour_t3962482529 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HCAnimatedBanner/<LoadJsonData>c__Iterator0::.ctor()
extern "C"  void U3CLoadJsonDataU3Ec__Iterator0__ctor_m4017495395 (U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HCAnimatedBanner/<LoadTexture>c__Iterator1::.ctor()
extern "C"  void U3CLoadTextureU3Ec__Iterator1__ctor_m2885060348 (U3CLoadTextureU3Ec__Iterator1_t1051594053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HCAnimatedBanner/<AnimateBanner>c__Iterator2::.ctor()
extern "C"  void U3CAnimateBannerU3Ec__Iterator2__ctor_m2061546856 (U3CAnimateBannerU3Ec__Iterator2_t688012631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2555686324  Color_get_white_m3544547002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m487959476 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
extern "C"  void RawImage_set_texture_m415027901 (RawImage_t3182918964 * __this, Texture_t3661962703 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
extern "C"  int32_t Application_get_internetReachability_m2044109931 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HCAnimatedBanner::get_transitionLink()
extern "C"  String_t* HCAnimatedBanner_get_transitionLink_m3352356618 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::OpenURL(System.String)
extern "C"  void Application_OpenURL_m1638057963 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
extern "C"  AsyncOperation_t1445031843 * Resources_UnloadUnusedAssets_m2573856924 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HCAnimatedBanner/<AnimateBanner>c__Iterator2/<AnimateBanner>c__AnonStorey3::.ctor()
extern "C"  void U3CAnimateBannerU3Ec__AnonStorey3__ctor_m3844873053 (U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<HCAnimatedBanner/AnimBannerData>::get_Item(System.Int32)
#define List_1_get_Item_m335550220(__this, p0, method) ((  AnimBannerData_t3749323191 * (*) (List_1_t926430637 *, int32_t, const MethodInfo*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m717029688 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2635848439 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<UnityEngine.Rect,UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3848706282(__this, p0, p1, method) ((  void (*) (Func_2_t1820288298 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3848706282_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Rect,UnityEngine.Rect>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisRect_t2360479859_TisRect_t2360479859_m1309108196(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1820288298 *, const MethodInfo*))Enumerable_Select_TisRect_t2360479859_TisRect_t2360479859_m1309108196_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Rect>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisRect_t2360479859_m2199875962(__this /* static, unused */, p0, method) ((  RectU5BU5D_t2936723554* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisRect_t2360479859_m2199875962_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.UI.RawImage::set_uvRect(UnityEngine.Rect)
extern "C"  void RawImage_set_uvRect_m529943894 (RawImage_t3182918964 * __this, Rect_t2360479859  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m3218181674 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m3218181675 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m3421965717 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m977101306 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::.ctor(System.String)
extern "C"  void WWW__ctor_m1181807108 (WWW_t3688466362 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::get_error()
extern "C"  String_t* WWW_get_error_m4112162252 (WWW_t3688466362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::get_text()
extern "C"  String_t* WWW_get_text_m1327524635 (WWW_t3688466362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 LitJson.JsonMapper::ToObject<HCAnimatedBanner/AnimBannerData[]>(System.String)
#define JsonMapper_ToObject_TisAnimBannerDataU5BU5D_t1736877454_m3809780039(__this /* static, unused */, p0, method) ((  AnimBannerDataU5BU5D_t1736877454* (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonMapper_ToObject_TisIl2CppObject_m627569000_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<HCAnimatedBanner/AnimBannerData>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisAnimBannerData_t3749323191_m1433232041(__this /* static, unused */, p0, method) ((  List_1_t926430637 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3517240806_gshared)(__this /* static, unused */, p0, method)
// System.Void HCAnimatedBanner/<LoadJsonData>c__Iterator0::<>__Finally0()
extern "C"  void U3CLoadJsonDataU3Ec__Iterator0_U3CU3E__Finally0_m3163288294 (U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<HCAnimatedBanner/AnimBannerData>::get_Count()
#define List_1_get_Count_m3914099077(__this, method) ((  int32_t (*) (List_1_t926430637 *, const MethodInfo*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.String UnityEngine.Application::get_persistentDataPath()
extern "C"  String_t* Application_get_persistentDataPath_m1664523141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern "C"  void Texture2D__ctor_m1608908294 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.File::Exists(System.String)
extern "C"  bool File_Exists_m3943585060 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.WWW::get_bytes()
extern "C"  ByteU5BU5D_t4116647657* WWW_get_bytes_m4004069865 (WWW_t3688466362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllBytes(System.String,System.Byte[])
extern "C"  void File_WriteAllBytes_m4252682105 (Il2CppObject * __this /* static, unused */, String_t* p0, ByteU5BU5D_t4116647657* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HCAnimatedBanner/<LoadTexture>c__Iterator1::<>__Finally0()
extern "C"  void U3CLoadTextureU3Ec__Iterator1_U3CU3E__Finally0_m2209114808 (U3CLoadTextureU3Ec__Iterator1_t1051594053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.IO.File::ReadAllBytes(System.String)
extern "C"  ByteU5BU5D_t4116647657* File_ReadAllBytes_m1435775076 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Texture2D::LoadImage(System.Byte[])
extern "C"  bool Texture2D_LoadImage_m1056421096 (Texture2D_t3840446185 * __this, ByteU5BU5D_t4116647657* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HCAnimatedBanner::ApplyTexture(UnityEngine.Texture2D)
extern "C"  void HCAnimatedBanner_ApplyTexture_m3563656807 (HCAnimatedBanner_t2921968851 * __this, Texture2D_t3840446185 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HCAnimatedBanner::set_transitionLink(System.String)
extern "C"  void HCAnimatedBanner_set_transitionLink_m3657221653 (HCAnimatedBanner_t2921968851 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HCAnimatedBanner::AnimateBanner(System.Int32)
extern "C"  Il2CppObject * HCAnimatedBanner_AnimateBanner_m3687925943 (HCAnimatedBanner_t2921968851 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_deviceModel()
extern "C"  String_t* SystemInfo_get_deviceModel_m1299396108 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Contains(System.String)
extern "C"  bool String_Contains_m1147431944 (String_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, method) ((  RectTransform_t3704657025 * (*) (Component_t1923634451 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2735705429_gshared)(__this, method)
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m4060800441 (Vector2_t2156229523 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t3722313464  Vector2_op_Implicit_m1988559315 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m2795501682 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern "C"  void Transform_Rotate_m915426478 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingBall::MoveBall()
extern "C"  void MovingBall_MoveBall_m4217469841 (MovingBall_t2489706458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t3722313464  Transform_get_localPosition_m265057664 (Transform_t3600365921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m500974738 (Il2CppObject * __this /* static, unused */, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m3327877514 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single)
extern "C"  void Transform_Translate_m2342484445 (Transform_t3600365921 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2555686324  Color_get_black_m650597609 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardLoaded::Invoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard)
extern "C"  void NendAdFullBoardLoaded_Invoke_m2966266618 (NendAdFullBoardLoaded_t3830347218 * __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardShown::Invoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard)
extern "C"  void NendAdFullBoardShown_Invoke_m1454147902 (NendAdFullBoardShown_t3059274368 * __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardClick::Invoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard)
extern "C"  void NendAdFullBoardClick_Invoke_m949622431 (NendAdFullBoardClick_t3385596145 * __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardDismiss::Invoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard)
extern "C"  void NendAdFullBoardDismiss_Invoke_m481757421 (NendAdFullBoardDismiss_t136425400 * __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardFailedToLoad::Invoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard,NendUnityPlugin.AD.FullBoard.NendAdFullBoard/FullBoardAdErrorType)
extern "C"  void NendAdFullBoardFailedToLoad_Invoke_m3650888263 (NendAdFullBoardFailedToLoad_t4155484538 * __this, NendAdFullBoard_t270881991 * ___instance0, int32_t ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.Platform.iOS.IOSFullBoardAd::.ctor(System.String,System.String)
extern "C"  void IOSFullBoardAd__ctor_m3519029639 (IOSFullBoardAd_t1375839176 * __this, String_t* ___spotId0, String_t* ___apiKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.Common.NendAdLogger::W(System.String,System.Object[])
extern "C"  void NendAdLogger_W_m3326785758 (Il2CppObject * __this /* static, unused */, String_t* ___format0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<NendUnityPlugin.AD.Native.NendAdNativeView>::.ctor()
#define UnityEvent_1__ctor_m1367642006(__this, method) ((  void (*) (UnityEvent_1_t2896268984 *, const MethodInfo*))UnityEvent_1__ctor_m4234511999_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`3<NendUnityPlugin.AD.Native.NendAdNativeView,System.Int32,System.String>::.ctor()
#define UnityEvent_3__ctor_m2373477420(__this, method) ((  void (*) (UnityEvent_3_t1300214534 *, const MethodInfo*))UnityEvent_3__ctor_m4063448395_gshared)(__this, method)
// System.Void System.Action::Invoke()
extern "C"  void Action_Invoke_m937035532 (Action_t1264377477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m1414505214 (Object_t631007953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.Common.NendAdLogger::D(System.String,System.Object[])
extern "C"  void NendAdLogger_D_m2189594530 (Il2CppObject * __this /* static, unused */, String_t* ___format0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandlerGO::.ctor()
extern "C"  void ClickHandlerGO__ctor_m389639094 (ClickHandlerGO_t2934446534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t3785851493  Camera_ScreenPointToRay_m1522780915 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t3722313464  Ray_get_origin_m4290253200 (Ray_t3785851493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t3722313464  Ray_get_direction_m1991692996 (Ray_t3785851493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  RaycastHit2D_t2279581989  Physics2D_Raycast_m4254966674 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t2806799626 * RaycastHit2D_get_collider_m1860250292 (RaycastHit2D_t2279581989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m2500195530 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  p0, RaycastHit_t1056001966 * p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t1773347010 * RaycastHit_get_collider_m1442240336 (RaycastHit_t1056001966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandler::.ctor()
extern "C"  void ClickHandler__ctor_m2579410340 (ClickHandler_t29098297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C"  bool Input_GetMouseButtonUp_m3875270823 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t3722313464  Input_get_mousePosition_m1579656956 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NendUnityPlugin.AD.Native.Handlers.ClickHandlerGO::IsPointerOverUIObject(UnityEngine.Vector3)
extern "C"  bool ClickHandlerGO_IsPointerOverUIObject_m204886395 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandler::CallbackClick()
extern "C"  void ClickHandler_CallbackClick_m4050748451 (ClickHandler_t29098297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.EventSystem::get_current()
extern "C"  EventSystem_t1003666588 * EventSystem_get_current_m1416377559 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::.ctor(UnityEngine.EventSystems.EventSystem)
extern "C"  void PointerEventData__ctor_m2263609344 (PointerEventData_t3807901092 * __this, EventSystem_t1003666588 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_position(UnityEngine.Vector2)
extern "C"  void PointerEventData_set_position_m2060457995 (PointerEventData_t3807901092 * __this, Vector2_t2156229523  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
#define List_1__ctor_m2049947431(__this, method) ((  void (*) (List_1_t537414295 *, const MethodInfo*))List_1__ctor_m2049947431_gshared)(__this, method)
// System.Void UnityEngine.EventSystems.EventSystem::RaycastAll(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void EventSystem_RaycastAll_m523788254 (EventSystem_t1003666588 * __this, PointerEventData_t3807901092 * p0, List_1_t537414295 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
#define List_1_get_Count_m4207101203(__this, method) ((  int32_t (*) (List_1_t537414295 *, const MethodInfo*))List_1_get_Count_m4207101203_gshared)(__this, method)
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandler::CheckImpression()
extern "C"  void ImpressionHandler_CheckImpression_m1408940149 (ImpressionHandler_t4278309981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.Common.NendAdLogger::I(System.String,System.Object[])
extern "C"  void NendAdLogger_I_m2000148771 (Il2CppObject * __this /* static, unused */, String_t* ___format0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Transform_InverseTransformPoint_m1254110475 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t2360479859  Camera_get_pixelRect_m1345608344 (Camera_t4157153871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandler/Corner::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Corner__ctor_m3011766940 (Corner_t2201492025 * __this, float ___left0, float ___top1, float ___right2, float ___bottom3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandler::.ctor()
extern "C"  void ImpressionHandler__ctor_m2314060538 (ImpressionHandler_t4278309981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Camera_WorldToScreenPoint_m491883527 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerGO::GetBounds()
extern "C"  Bounds_t2266837910  ImpressionHandlerGO_GetBounds_m2495302509 (ImpressionHandlerGO_t188448812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerGO::GetObjectRect(UnityEngine.Bounds)
extern "C"  Rect_t2360479859  ImpressionHandlerGO_GetObjectRect_m1352629183 (Il2CppObject * __this /* static, unused */, Bounds_t2266837910  ___bounds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NendUnityPlugin.AD.Native.Handlers.ImpressionHandler::CheckViewablePercentage(NendUnityPlugin.AD.Native.Handlers.ImpressionHandler/Corner)
extern "C"  bool ImpressionHandler_CheckViewablePercentage_m3622756226 (ImpressionHandler_t4278309981 * __this, Corner_t2201492025  ___adCorner0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1920811489 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider>()
#define GameObject_GetComponent_TisCollider_t1773347010_m69509314(__this, method) ((  Collider_t1773347010 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2049753423_gshared)(__this, method)
// UnityEngine.Bounds UnityEngine.Collider::get_bounds()
extern "C"  Bounds_t2266837910  Collider_get_bounds_m518531398 (Collider_t1773347010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.Collider2D::get_bounds()
extern "C"  Bounds_t2266837910  Collider2D_get_bounds_m1523458546 (Collider2D_t2806799626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m237278729 (InvalidOperationException_t56020091 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t3722313464  Bounds_get_center_m1997663455 (Bounds_t2266837910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t3722313464  Bounds_get_extents_m3683565517 (Bounds_t2266837910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerGO::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  ImpressionHandlerGO_WorldToScreenPoint_m1334575203 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___world0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::Min(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_Min_m2207225457 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::Max(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_Max_m1487165843 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1569321028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInParent<UnityEngine.Canvas>()
#define Component_GetComponentInParent_TisCanvas_t3310196443_m1339529724(__this, method) ((  Canvas_t3310196443 * (*) (Component_t1923634451 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m3491943679_gshared)(__this, method)
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C"  Camera_t4157153871 * Canvas_get_worldCamera_m2289553767 (Canvas_t3310196443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI/<IsViewable>c__AnonStorey0::.ctor()
extern "C"  void U3CIsViewableU3Ec__AnonStorey0__ctor_m2474423893 (U3CIsViewableU3Ec__AnonStorey0_t3542390976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2156229523  RectTransform_get_sizeDelta_m2136908840 (RectTransform_t3704657025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t2360479859  RectTransform_get_rect_m1643570810 (RectTransform_t3704657025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t3722313464  Transform_get_localScale_m2509514663 (Transform_t3600365921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CanvasGroup>()
#define Component_GetComponent_TisCanvasGroup_t4083511760_m1030459026(__this, method) ((  CanvasGroup_t4083511760 * (*) (Component_t1923634451 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2735705429_gshared)(__this, method)
// System.Single UnityEngine.CanvasGroup::get_alpha()
extern "C"  float CanvasGroup_get_alpha_m913685212 (CanvasGroup_t4083511760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetWorldCorners_m2125351209 (RectTransform_t3704657025 * __this, Vector3U5BU5D_t1718750761* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C"  int32_t Canvas_get_renderMode_m449291776 (Canvas_t3310196443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<UnityEngine.Vector3,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m134789189(__this, p0, p1, method) ((  void (*) (Func_2_t398065169 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m134789189_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Vector3,UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisVector3_t3722313464_TisVector2_t2156229523_m2491549993(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t398065169 *, const MethodInfo*))Enumerable_Select_TisVector3_t3722313464_TisVector2_t2156229523_m2491549993_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisVector2_t2156229523_m3384610930(__this /* static, unused */, p0, method) ((  Vector2U5BU5D_t1457185986* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisVector2_t2156229523_m3384610930_gshared)(__this /* static, unused */, p0, method)
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1901831667 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m571965535 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  RectTransformUtility_WorldToScreenPoint_m4200566991 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * p0, Vector3_t3722313464  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MonoBehaviour>::.ctor()
#define HashSet_1__ctor_m4032723476(__this, method) ((  void (*) (HashSet_1_t2527432003 *, const MethodInfo*))HashSet_1__ctor_m4231804131_gshared)(__this, method)
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m3076187857 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NativeAd::DestroyAllHandlers()
extern "C"  void NativeAd_DestroyAllHandlers_m3923046273 (NativeAd_t775405424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor()
extern "C"  void NotImplementedException__ctor_m3058704252 (NotImplementedException_t3489357830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NendUnityPlugin.AD.Native.Utils.TextureLoader::LoadTexture(System.String,System.Action`1<UnityEngine.Texture2D>)
extern "C"  Il2CppObject * TextureLoader_LoadTexture_m3236146968 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Action_1_t4012913780 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.Texture2D>::Invoke(!0)
#define Action_1_Invoke_m400472198(__this, p0, method) ((  void (*) (Action_1_t4012913780 *, Texture2D_t3840446185 *, const MethodInfo*))Action_1_Invoke_m2461023210_gshared)(__this, p0, method)
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<NendUnityPlugin.AD.Native.Handlers.ClickHandler>()
#define GameObject_GetComponent_TisClickHandler_t29098297_m4091602126(__this, method) ((  ClickHandler_t29098297 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2049753423_gshared)(__this, method)
// System.Void NendUnityPlugin.AD.Native.NativeAd::DestroyHandler(UnityEngine.MonoBehaviour)
extern "C"  void NativeAd_DestroyHandler_m1214946435 (NativeAd_t775405424 * __this, MonoBehaviour_t3962482529 * ___handler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<NendUnityPlugin.AD.Native.Handlers.ImpressionHandler>()
#define GameObject_GetComponent_TisImpressionHandler_t4278309981_m2269228889(__this, method) ((  ImpressionHandler_t4278309981 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2049753423_gshared)(__this, method)
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m2994342681 (Action_t1264377477 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NativeAd::AddClickHandler(UnityEngine.GameObject,System.Action)
extern "C"  void NativeAd_AddClickHandler_m3246524294 (NativeAd_t775405424 * __this, GameObject_t1113636619 * ___target0, Action_t1264377477 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NativeAd::AddImpressionHandler(UnityEngine.GameObject)
extern "C"  void NativeAd_AddImpressionHandler_m742031269 (NativeAd_t775405424 * __this, GameObject_t1113636619 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::RenderAd(NendUnityPlugin.AD.Native.INativeAd)
extern "C"  void NendAdNativeView_RenderAd_m3300549469 (NendAdNativeView_t2014609480 * __this, Il2CppObject * ___ad0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponentInParent<UnityEngine.Canvas>()
#define GameObject_GetComponentInParent_TisCanvas_t3310196443_m3114266371(__this, method) ((  Canvas_t3310196443 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponentInParent_TisIl2CppObject_m1039067679_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<NendUnityPlugin.AD.Native.Handlers.ClickHandlerUI>()
#define GameObject_AddComponent_TisClickHandlerUI_t258087360_m1730712142(__this, method) ((  ClickHandlerUI_t258087360 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3469369570_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<NendUnityPlugin.AD.Native.Handlers.ClickHandler3D>()
#define GameObject_AddComponent_TisClickHandler3D_t283384251_m1758828144(__this, method) ((  ClickHandler3D_t283384251 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3469369570_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<NendUnityPlugin.AD.Native.Handlers.ClickHandler2D>()
#define GameObject_AddComponent_TisClickHandler2D_t2622036411_m1758828177(__this, method) ((  ClickHandler2D_t2622036411 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3469369570_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MonoBehaviour>::Add(!0)
#define HashSet_1_Add_m3879467893(__this, p0, method) ((  bool (*) (HashSet_1_t2527432003 *, MonoBehaviour_t3962482529 *, const MethodInfo*))HashSet_1_Add_m1971460364_gshared)(__this, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(__this, method) ((  RectTransform_t3704657025 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2049753423_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI>()
#define GameObject_AddComponent_TisImpressionHandlerUI_t188317754_m2131962076(__this, method) ((  ImpressionHandlerUI_t188317754 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3469369570_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerGO>()
#define GameObject_AddComponent_TisImpressionHandlerGO_t188448812_m1290658942(__this, method) ((  ImpressionHandlerGO_t188448812 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3469369570_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MonoBehaviour>::Remove(!0)
#define HashSet_1_Remove_m1688024103(__this, p0, method) ((  bool (*) (HashSet_1_t2527432003 *, MonoBehaviour_t3962482529 *, const MethodInfo*))HashSet_1_Remove_m709044238_gshared)(__this, p0, method)
// System.Void System.Func`2<UnityEngine.MonoBehaviour,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2243427934(__this, p0, p1, method) ((  void (*) (Func_2_t2922669422 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3104565095_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.MonoBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisMonoBehaviour_t3962482529_m3892073355(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2922669422 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m2166527282_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.MonoBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisMonoBehaviour_t3962482529_m4146969897(__this /* static, unused */, p0, method) ((  MonoBehaviourU5BU5D_t2007329276* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m698722831_gshared)(__this /* static, unused */, p0, method)
// System.Void NendUnityPlugin.AD.Native.NativeAd::DestroyHandlers(UnityEngine.MonoBehaviour[])
extern "C"  void NativeAd_DestroyHandlers_m3057256890 (NativeAd_t775405424 * __this, MonoBehaviourU5BU5D_t2007329276* ___handlers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler::Invoke(System.Object,System.EventArgs)
extern "C"  void EventHandler_Invoke_m2047579917 (EventHandler_t1348719766 * __this, Il2CppObject * p0, EventArgs_t3591816995 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NativeAd::DestroyImpressionHandlers()
extern "C"  void NativeAd_DestroyImpressionHandlers_m3505163566 (NativeAd_t775405424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Queue`1<System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>>::.ctor()
#define Queue_1__ctor_m2088921833(__this, method) ((  void (*) (Queue_1_t414173832 *, const MethodInfo*))Queue_1__ctor_m3749217910_gshared)(__this, method)
// System.Void NendUnityPlugin.Common.NendAdMainThreadWorker::Prepare()
extern "C"  void NendAdMainThreadWorker_Prepare_m3075374016 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Utils.SimpleTimer::.ctor()
extern "C"  void SimpleTimer__ctor_m1198240476 (SimpleTimer_t4075384413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Queue`1<System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>>::Clear()
#define Queue_1_Clear_m3541320262(__this, method) ((  void (*) (Queue_1_t414173832 *, const MethodInfo*))Queue_1_Clear_m4070494218_gshared)(__this, method)
// System.Void NendUnityPlugin.AD.Native.Utils.SimpleTimer::Dispose()
extern "C"  void SimpleTimer_Dispose_m1569670885 (SimpleTimer_t4075384413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Utils.SimpleTimer::Start(System.Double)
extern "C"  void SimpleTimer_Start_m3197311368 (SimpleTimer_t4075384413 * __this, double ___interval0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Utils.SimpleTimer::Stop()
extern "C"  void SimpleTimer_Stop_m1024974171 (SimpleTimer_t4075384413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2249409497 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Queue`1<System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>>::Enqueue(!0)
#define Queue_1_Enqueue_m2253724761(__this, p0, method) ((  void (*) (Queue_1_t414173832 *, Action_3_t567914338 *, const MethodInfo*))Queue_1_Enqueue_m1868480850_gshared)(__this, p0, method)
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m3585316909 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey1::.ctor()
extern "C"  void U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1__ctor_m1980408661 (U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Queue`1<System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>>::get_Count()
#define Queue_1_get_Count_m685314720(__this, method) ((  int32_t (*) (Queue_1_t414173832 *, const MethodInfo*))Queue_1_get_Count_m2496300460_gshared)(__this, method)
// System.Void NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey0::.ctor()
extern "C"  void U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0__ctor_m1857004373 (U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NendUnityPlugin.AD.Native.NativeAdClient::IsGifImage(System.String)
extern "C"  bool NativeAdClient_IsGifImage_m3010410997 (Il2CppObject * __this /* static, unused */, String_t* ___imageUrl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Queue`1<System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>>::Dequeue()
#define Queue_1_Dequeue_m1251791087(__this, method) ((  Action_3_t567914338 * (*) (Queue_1_t414173832 *, const MethodInfo*))Queue_1_Dequeue_m3550993416_gshared)(__this, method)
// NendUnityPlugin.Common.NendAdMainThreadWorker NendUnityPlugin.Common.NendAdMainThreadWorker::get_Instance()
extern "C"  NendAdMainThreadWorker_t3569218590 * NendAdMainThreadWorker_get_Instance_m443439756 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.Common.NendAdMainThreadWorker::Post(System.Action)
extern "C"  void NendAdMainThreadWorker_Post_m1274596709 (NendAdMainThreadWorker_t3569218590 * __this, Action_t1264377477 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::ToLower()
extern "C"  String_t* String_ToLower_m2029374922 (String_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::EndsWith(System.String)
extern "C"  bool String_EndsWith_m1901926500 (String_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>::Invoke(!0,!1,!2)
#define Action_3_Invoke_m1359513622(__this, p0, p1, p2, method) ((  void (*) (Action_3_t567914338 *, Il2CppObject *, int32_t, String_t*, const MethodInfo*))Action_3_Invoke_m3429238884_gshared)(__this, p0, p1, p2, method)
// System.Void NendUnityPlugin.Platform.iOS.IOSNativeAdClient::.ctor(System.String,System.String)
extern "C"  void IOSNativeAdClient__ctor_m3085587387 (IOSNativeAdClient_t1155913684 * __this, String_t* ___apiKey0, String_t* ___spotId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NendUnityPlugin.AD.Native.INativeAdClient NendUnityPlugin.AD.Native.NendAdNative::CreateClient()
extern "C"  Il2CppObject * NendAdNative_CreateClient_m4286713017 (NendAdNative_t3381739006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNative::LoadAd()
extern "C"  void NendAdNative_LoadAd_m89270426 (NendAdNative_t3381739006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNative::EnableAutoReload(System.Double)
extern "C"  void NendAdNative_EnableAutoReload_m3562877551 (NendAdNative_t3381739006 * __this, double ___interval0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNative/<RegisterAdView>c__AnonStorey0::.ctor()
extern "C"  void U3CRegisterAdViewU3Ec__AnonStorey0__ctor_m371496142 (U3CRegisterAdViewU3Ec__AnonStorey0_t799839499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<NendUnityPlugin.AD.Native.NendAdNativeView,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3969891916(__this, p0, p1, method) ((  void (*) (Func_2_t261507339 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3104565095_gshared)(__this, p0, p1, method)
// System.Boolean System.Linq.Enumerable::All<NendUnityPlugin.AD.Native.NendAdNativeView>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_All_TisNendAdNativeView_t2014609480_m2632065831(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t261507339 *, const MethodInfo*))Enumerable_All_TisIl2CppObject_m1743658500_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<NendUnityPlugin.AD.Native.NendAdNativeView>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Concat_TisNendAdNativeView_t2014609480_m60162064(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))Enumerable_Concat_TisIl2CppObject_m1054524938_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<NendUnityPlugin.AD.Native.NendAdNativeView>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisNendAdNativeView_t2014609480_m230526467(__this /* static, unused */, p0, method) ((  NendAdNativeViewU5BU5D_t3641134489* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m698722831_gshared)(__this /* static, unused */, p0, method)
// System.Int32 System.Array::IndexOf<NendUnityPlugin.AD.Native.NendAdNativeView>(!!0[],!!0)
#define Array_IndexOf_TisNendAdNativeView_t2014609480_m792692718(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, NendAdNativeViewU5BU5D_t3641134489*, NendAdNativeView_t2014609480 *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m3944231312_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
extern "C"  void Array_Clear_m2231608178 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<NendUnityPlugin.AD.Native.NendAdNativeView>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisNendAdNativeView_t2014609480_m876349505(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t261507339 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m2166527282_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::DefaultIfEmpty<NendUnityPlugin.AD.Native.NendAdNativeView>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_DefaultIfEmpty_TisNendAdNativeView_t2014609480_m3560641153(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_DefaultIfEmpty_TisIl2CppObject_m2788453171_gshared)(__this /* static, unused */, p0, method)
// System.Void NendUnityPlugin.AD.Native.NendAdNative::LoadAd(NendUnityPlugin.AD.Native.NendAdNativeView[])
extern "C"  void NendAdNative_LoadAd_m545678891 (NendAdNative_t3381739006 * __this, NendAdNativeViewU5BU5D_t3641134489* ___views0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey1::.ctor()
extern "C"  void U3CLoadAdU3Ec__AnonStorey1__ctor_m715144880 (U3CLoadAdU3Ec__AnonStorey1_t1337685977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey2::.ctor()
extern "C"  void U3CLoadAdU3Ec__AnonStorey2__ctor_m715147817 (U3CLoadAdU3Ec__AnonStorey2_t1337685978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NendUnityPlugin.AD.Native.INativeAdClient NendUnityPlugin.AD.Native.NendAdNative::get_Client()
extern "C"  Il2CppObject * NendAdNative_get_Client_m3224855641 (NendAdNative_t3381739006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m1315864708(__this, p0, p1, method) ((  void (*) (Action_3_t567914338 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m3182009741_gshared)(__this, p0, p1, method)
// NendUnityPlugin.AD.Native.INativeAdClient NendUnityPlugin.AD.Native.NativeAdClientFactory::NewClient(System.String,System.String)
extern "C"  Il2CppObject * NativeAdClientFactory_NewClient_m1082690577 (Il2CppObject * __this /* static, unused */, String_t* ___spotId0, String_t* ___apiKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<NendUnityPlugin.AD.Native.NendAdNativeView>::Invoke(!0)
#define UnityEvent_1_Invoke_m2488769038(__this, p0, method) ((  void (*) (UnityEvent_1_t2896268984 *, NendAdNativeView_t2014609480 *, const MethodInfo*))UnityEvent_1_Invoke_m4196549529_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEvent`3<NendUnityPlugin.AD.Native.NendAdNativeView,System.Int32,System.String>::Invoke(!0,!1,!2)
#define UnityEvent_3_Invoke_m1929593389(__this, p0, p1, p2, method) ((  void (*) (UnityEvent_3_t1300214534 *, NendAdNativeView_t2014609480 *, int32_t, String_t*, const MethodInfo*))UnityEvent_3_Invoke_m1708203693_gshared)(__this, p0, p1, p2, method)
// System.Int32 NendUnityPlugin.AD.Native.NendAdNativeView::get_ViewTag()
extern "C"  int32_t NendAdNativeView_get_ViewTag_m1321846746 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::OnLoadAd(NendUnityPlugin.AD.Native.INativeAd)
extern "C"  void NendAdNativeView_OnLoadAd_m3850891744 (NendAdNativeView_t2014609480 * __this, Il2CppObject * ___ad0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNative::PostAdLoaded(NendUnityPlugin.AD.Native.NendAdNativeView)
extern "C"  void NendAdNative_PostAdLoaded_m3099863989 (NendAdNative_t3381739006 * __this, NendAdNativeView_t2014609480 * ___view0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.Common.NendAdLogger::E(System.String,System.Object[])
extern "C"  void NendAdLogger_E_m3544237484 (Il2CppObject * __this /* static, unused */, String_t* ___format0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNative::PostAdFailedToReceive(NendUnityPlugin.AD.Native.NendAdNativeView,System.Int32,System.String)
extern "C"  void NendAdNative_PostAdFailedToReceive_m837728790 (NendAdNative_t3381739006 * __this, NendAdNativeView_t2014609480 * ___view0, int32_t ___code1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::.ctor()
extern "C"  void RawImage__ctor_m2348784658 (RawImage_t3182918964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Text::.ctor()
extern "C"  void Text__ctor_m1150387577 (Text_t1901882714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void EventHandler__ctor_m3449229857 (EventHandler_t1348719766 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer::TryRenderText(UnityEngine.UI.Text,System.String)
extern "C"  void UIRenderer_TryRenderText_m3187668248 (Il2CppObject * __this /* static, unused */, Text_t1901882714 * ___target0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer::TryRenderImage(UnityEngine.MonoBehaviour,UnityEngine.UI.RawImage,System.String)
extern "C"  void UIRenderer_TryRenderImage_m737991264 (Il2CppObject * __this /* static, unused */, MonoBehaviour_t3962482529 * ___behaviour0, RawImage_t3182918964 * ___target1, String_t* ___url2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::PostAdShown()
extern "C"  void NendAdNativeView_PostAdShown_m2675772046 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::Clear()
extern "C"  void NendAdNativeView_Clear_m2493923126 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NendUnityPlugin.AD.Native.NendAdNativeView::get_Loaded()
extern "C"  bool NendAdNativeView_get_Loaded_m2218797704 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NendUnityPlugin.AD.Native.NendAdNativeView::RenderAd()
extern "C"  bool NendAdNativeView_RenderAd_m3682669061 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::set_ShowingNativeAd(NendUnityPlugin.AD.Native.INativeAd)
extern "C"  void NendAdNativeView_set_ShowingNativeAd_m553901933 (NendAdNativeView_t2014609480 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer::TryClearText(UnityEngine.UI.Text)
extern "C"  void UIRenderer_TryClearText_m4217291438 (Il2CppObject * __this /* static, unused */, Text_t1901882714 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer::TryClearImage(UnityEngine.UI.RawImage)
extern "C"  void UIRenderer_TryClearImage_m1984313908 (Il2CppObject * __this /* static, unused */, RawImage_t3182918964 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::PostAdClicked()
extern "C"  void NendAdNativeView_PostAdClicked_m515887988 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.Common.NendAdLogger::set_LogLevel(NendUnityPlugin.Common.NendAdLogger/NendAdLogLevel)
extern "C"  void NendAdLogger_set_LogLevel_m3276973888 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Timers.Timer::.ctor(System.Double)
extern "C"  void Timer__ctor_m2308471781 (Timer_t1767341190 * __this, double p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Timers.ElapsedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ElapsedEventHandler__ctor_m3647598057 (ElapsedEventHandler_t976055006 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Timers.Timer::add_Elapsed(System.Timers.ElapsedEventHandler)
extern "C"  void Timer_add_Elapsed_m1727888252 (Timer_t1767341190 * __this, ElapsedEventHandler_t976055006 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Timers.Timer::Start()
extern "C"  void Timer_Start_m3010200494 (Timer_t1767341190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Timers.Timer::remove_Elapsed(System.Timers.ElapsedEventHandler)
extern "C"  void Timer_remove_Elapsed_m1866573542 (Timer_t1767341190 * __this, ElapsedEventHandler_t976055006 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Timers.Timer::Stop()
extern "C"  void Timer_Stop_m2096258851 (Timer_t1767341190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Timers.Timer::Close()
extern "C"  void Timer_Close_m1184068935 (Timer_t1767341190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::.ctor()
extern "C"  void U3CLoadTextureU3Ec__Iterator0__ctor_m1470171075 (U3CLoadTextureU3Ec__Iterator0_t2469200630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache::get_Instance()
extern "C"  TextureCache_t3323732960 * TextureCache_get_Instance_m1280202405 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache::Get(System.String)
extern "C"  Texture2D_t3840446185 * TextureCache_Get_m1883981837 (TextureCache_t3323732960 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.WWW::get_texture()
extern "C"  Texture2D_t3840446185 * WWW_get_texture_m1724981587 (WWW_t3688466362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache::Put(System.String,UnityEngine.Texture2D)
extern "C"  void TextureCache_Put_m3192621705 (TextureCache_t3323732960 * __this, String_t* ___url0, Texture2D_t3840446185 * ___texture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::<>__Finally0()
extern "C"  void U3CLoadTextureU3Ec__Iterator0_U3CU3E__Finally0_m3862613709 (U3CLoadTextureU3Ec__Iterator0_t2469200630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>::.ctor()
#define Dictionary_2__ctor_m2275355514(__this, method) ((  void (*) (Dictionary_2_t3625702484 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache::.ctor()
extern "C"  void TextureCache__ctor_m3754485370 (TextureCache_t3323732960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m1577318754(__this, p0, method) ((  bool (*) (Dictionary_2_t3625702484 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m2278349286_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>::get_Item(!0)
#define Dictionary_2_get_Item_m2202019062(__this, p0, method) ((  Texture2D_t3840446185 * (*) (Dictionary_2_t3625702484 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m2714930061_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m432567873(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3625702484 *, String_t*, Texture2D_t3840446185 *, const MethodInfo*))Dictionary_2_set_Item_m3474379962_gshared)(__this, p0, p1, method)
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer/<TryRenderImage>c__AnonStorey0::.ctor()
extern "C"  void U3CTryRenderImageU3Ec__AnonStorey0__ctor_m1291323508 (U3CTryRenderImageU3Ec__AnonStorey0_t4270539830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C"  bool Behaviour_get_isActiveAndEnabled_m3013577336 (Behaviour_t1437897464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.Texture2D>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m3185781518(__this, p0, p1, method) ((  void (*) (Action_1_t4012913780 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m118522912_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m395729791 (Object_t631007953 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.NendAd::.ctor()
extern "C"  void NendAd__ctor_m3419012545 (NendAd_t284737821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NendUnityPlugin.Platform.NendAdBannerInterface NendUnityPlugin.Platform.NendAdNativeInterfaceFactory::CreateBannerAdInterface()
extern "C"  Il2CppObject * NendAdNativeInterfaceFactory_CreateBannerAdInterface_m2322301420 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NendUnityPlugin.Platform.NendAdBannerInterface NendUnityPlugin.AD.NendAdBanner::get_Interface()
extern "C"  Il2CppObject * NendAdBanner_get_Interface_m1606256974 (NendAdBanner_t489953245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NendUnityPlugin.AD.NendAdBanner::MakeParams()
extern "C"  String_t* NendAdBanner_MakeParams_m3373626820 (NendAdBanner_t489953245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor()
extern "C"  void StringBuilder__ctor_m3121283359 (StringBuilder_t1712802186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Append_m1965104174 (StringBuilder_t1712802186 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Int32)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Append_m890240332 (StringBuilder_t1712802186 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NendUnityPlugin.AD.NendAd::GetBitGravity(NendUnityPlugin.Common.Gravity[])
extern "C"  int32_t NendAd_GetBitGravity_m3423304963 (NendAd_t284737821 * __this, GravityU5BU5D_t868584943* ___gravity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Single)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Append_m3191759736 (StringBuilder_t1712802186 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.Char[])
extern "C"  StringU5BU5D_t1281789340* String_Split_m3646115398 (String_t* __this, CharU5BU5D_t3528271667* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.Common.NendAdErrorEventArgs::.ctor()
extern "C"  void NendAdErrorEventArgs__ctor_m3623794569 (NendAdErrorEventArgs_t3390548813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::Parse(System.String)
extern "C"  int32_t Int32_Parse_m1033611559 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.Common.NendAdErrorEventArgs::set_ErrorCode(NendUnityPlugin.Common.NendErrorCode)
extern "C"  void NendAdErrorEventArgs_set_ErrorCode_m1342030409 (NendAdErrorEventArgs_t3390548813 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.Common.NendAdErrorEventArgs::set_Message(System.String)
extern "C"  void NendAdErrorEventArgs_set_Message_m1888969659 (NendAdErrorEventArgs_t3390548813 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<NendUnityPlugin.Common.NendAdErrorEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m1615702581(__this, p0, p1, method) ((  void (*) (EventHandler_1_t1314708246 *, Il2CppObject *, NendAdErrorEventArgs_t3390548813 *, const MethodInfo*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// NendUnityPlugin.Platform.NendAdInterstitialInterface NendUnityPlugin.AD.NendAdInterstitial::get_Interface()
extern "C"  Il2CppObject * NendAdInterstitial_get_Interface_m2607091446 (NendAdInterstitial_t1417649638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NendUnityPlugin.Platform.NendAdInterstitialInterface NendUnityPlugin.Platform.NendAdNativeInterfaceFactory::CreateInterstitialAdInterface()
extern "C"  Il2CppObject * NendAdNativeInterfaceFactory_CreateInterstitialAdInterface_m687083042 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.NendAdInterstitial::Load(System.String,System.String)
extern "C"  void NendAdInterstitial_Load_m3683620854 (NendAdInterstitial_t1417649638 * __this, String_t* ___apiKey0, String_t* ___spotId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs::.ctor()
extern "C"  void NendAdInterstitialLoadEventArgs__ctor_m3921384839 (NendAdInterstitialLoadEventArgs_t1260857315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs::set_StatusCode(NendUnityPlugin.Common.NendAdInterstitialStatusCode)
extern "C"  void NendAdInterstitialLoadEventArgs_set_StatusCode_m2181589091 (NendAdInterstitialLoadEventArgs_t1260857315 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs::set_SpotId(System.String)
extern "C"  void NendAdInterstitialLoadEventArgs_set_SpotId_m228308284 (NendAdInterstitialLoadEventArgs_t1260857315 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m1611654412(__this, p0, p1, method) ((  void (*) (EventHandler_1_t3479984044 *, Il2CppObject *, NendAdInterstitialLoadEventArgs_t1260857315 *, const MethodInfo*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// System.Void NendUnityPlugin.AD.NendAdInterstitialClickEventArgs::.ctor()
extern "C"  void NendAdInterstitialClickEventArgs__ctor_m4030220703 (NendAdInterstitialClickEventArgs_t974162912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.NendAdInterstitialClickEventArgs::set_ClickType(NendUnityPlugin.Common.NendAdInterstitialClickType)
extern "C"  void NendAdInterstitialClickEventArgs_set_ClickType_m2779883726 (NendAdInterstitialClickEventArgs_t974162912 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.NendAdInterstitialClickEventArgs::set_SpotId(System.String)
extern "C"  void NendAdInterstitialClickEventArgs_set_SpotId_m2251321628 (NendAdInterstitialClickEventArgs_t974162912 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialClickEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m2443591581(__this, p0, p1, method) ((  void (*) (EventHandler_1_t3193289641 *, Il2CppObject *, NendAdInterstitialClickEventArgs_t974162912 *, const MethodInfo*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// System.Void NendUnityPlugin.AD.NendAdInterstitialShowEventArgs::.ctor()
extern "C"  void NendAdInterstitialShowEventArgs__ctor_m4100517664 (NendAdInterstitialShowEventArgs_t3083222281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.NendAdInterstitialShowEventArgs::set_ShowResult(NendUnityPlugin.Common.NendAdInterstitialShowResult)
extern "C"  void NendAdInterstitialShowEventArgs_set_ShowResult_m1732319236 (NendAdInterstitialShowEventArgs_t3083222281 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.NendAdInterstitialShowEventArgs::set_SpotId(System.String)
extern "C"  void NendAdInterstitialShowEventArgs_set_SpotId_m2748712616 (NendAdInterstitialShowEventArgs_t3083222281 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialShowEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m1676777043(__this, p0, p1, method) ((  void (*) (EventHandler_1_t1007381714 *, Il2CppObject *, NendAdInterstitialShowEventArgs_t3083222281 *, const MethodInfo*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// System.Void System.EventArgs::.ctor()
extern "C"  void EventArgs__ctor_m32674013 (EventArgs_t3591816995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.VideoAdCallbackArgments::.ctor(NendUnityPlugin.AD.Video.NendAdVideo/VideoAdCallbackType)
extern "C"  void VideoAdCallbackArgments__ctor_m1675151393 (VideoAdCallbackArgments_t2567701888 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::.ctor()
extern "C"  void NendAdVideo__ctor_m333837435 (NendAdVideo_t1781712043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NendUnityPlugin.AD.Video.NendAdInterstitialVideo NendUnityPlugin.Platform.NendAdNativeInterfaceFactory::CreateInterstitialVideoAd(System.String,System.String)
extern "C"  NendAdInterstitialVideo_t1383172729 * NendAdNativeInterfaceFactory_CreateInterstitialVideoAd_m1938283520 (Il2CppObject * __this /* static, unused */, String_t* ___spotId0, String_t* ___apiKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdInterstitialVideo::AddFallbackFullboard(System.String,System.String,UnityEngine.Color)
extern "C"  void NendAdInterstitialVideo_AddFallbackFullboard_m2575308473 (NendAdInterstitialVideo_t1383172729 * __this, String_t* ___spotId0, String_t* ___apiKey1, Color_t2555686324  ___iOSBackgroundColor2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdRewardedVideo/NendAdVideoRewarded::Invoke(NendUnityPlugin.AD.Video.NendAdVideo,NendUnityPlugin.AD.Video.NendAdRewardedItem)
extern "C"  void NendAdVideoRewarded_Invoke_m656162502 (NendAdVideoRewarded_t687064503 * __this, NendAdVideo_t1781712043 * ___instance0, NendAdRewardedItem_t3964314288 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::CallBack(NendUnityPlugin.AD.Video.VideoAdCallbackArgments)
extern "C"  void NendAdVideo_CallBack_m1751514092 (NendAdVideo_t1781712043 * __this, VideoAdCallbackArgments_t2567701888 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NendUnityPlugin.AD.Video.NendAdRewardedVideo NendUnityPlugin.Platform.NendAdNativeInterfaceFactory::CreateRewardedVideoAd(System.String,System.String)
extern "C"  NendAdRewardedVideo_t3161671531 * NendAdNativeInterfaceFactory_CreateRewardedVideoAd_m3735613872 (Il2CppObject * __this /* static, unused */, String_t* ___spotId0, String_t* ___apiKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor()
#define Dictionary_2__ctor_m3200964102(__this, method) ((  void (*) (Dictionary_2_t2736202052 *, const MethodInfo*))Dictionary_2__ctor_m2253601317_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Double>::.ctor()
#define Dictionary_2__ctor_m837984336(__this, method) ((  void (*) (Dictionary_2_t379921662 *, const MethodInfo*))Dictionary_2__ctor_m1071776077_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
#define Dictionary_2__ctor_m2842384104(__this, method) ((  void (*) (Dictionary_2_t1632706988 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Boolean>::.ctor()
#define Dictionary_2__ctor_m256458156(__this, method) ((  void (*) (Dictionary_2_t4177511560 *, const MethodInfo*))Dictionary_2__ctor_m236774955_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m3800595820(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2736202052 *, String_t*, int32_t, const MethodInfo*))Dictionary_2_set_Item_m411961606_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Double>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m83463571(__this, p0, p1, method) ((  void (*) (Dictionary_2_t379921662 *, String_t*, double, const MethodInfo*))Dictionary_2_set_Item_m3116967895_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m1950792581(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1632706988 *, String_t*, String_t*, const MethodInfo*))Dictionary_2_set_Item_m3474379962_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Boolean>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m2347951726(__this, p0, p1, method) ((  void (*) (Dictionary_2_t4177511560 *, String_t*, bool, const MethodInfo*))Dictionary_2_set_Item_m351745166_gshared)(__this, p0, p1, method)
// System.Void NendUnityPlugin.Platform.iOS.IOSUserFeature::.ctor()
extern "C"  void IOSUserFeature__ctor_m2184262995 (IOSUserFeature_t2237018260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoLoaded::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoLoaded_Invoke_m2447413929 (NendAdVideoLoaded_t1357133087 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToLoad::Invoke(NendUnityPlugin.AD.Video.NendAdVideo,System.Int32)
extern "C"  void NendAdVideoFailedToLoad_Invoke_m3265524165 (NendAdVideoFailedToLoad_t1837550073 * __this, NendAdVideo_t1781712043 * ___instance0, int32_t ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToPlay::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoFailedToPlay_Invoke_m4000227672 (NendAdVideoFailedToPlay_t4265396701 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoShown::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoShown_Invoke_m4289821957 (NendAdVideoShown_t3987677121 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClosed::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoClosed_Invoke_m2544588046 (NendAdVideoClosed_t464199100 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoClick_Invoke_m359502320 (NendAdVideoClick_t895736144 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoCompleted::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoCompleted_Invoke_m4139560455 (NendAdVideoCompleted_t3646835482 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoInformationClick::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoInformationClick_Invoke_m2704544080 (NendAdVideoInformationClick_t172780505 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStarted::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoStarted_Invoke_m4278189045 (NendAdVideoStarted_t2144365953 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStopped::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoStopped_Invoke_m1177888485 (NendAdVideoStopped_t3890769867 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BackButtonUtil::.ctor()
extern "C"  void BackButtonUtil__ctor_m3057092809 (BackButtonUtil_t2880819819 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// BackButtonUtil BackButtonUtil::get_Instance()
extern "C"  BackButtonUtil_t2880819819 * BackButtonUtil_get_Instance_m1724009932 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BackButtonUtil_get_Instance_m1724009932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BackButtonUtil_t2880819819 * L_0 = ((BackButtonUtil_t2880819819_StaticFields*)BackButtonUtil_t2880819819_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(BackButtonUtil_t2880819819_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_3 = Object_FindObjectOfType_m1736538631(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		((BackButtonUtil_t2880819819_StaticFields*)BackButtonUtil_t2880819819_il2cpp_TypeInfo_var->static_fields)->set_instance_5(((BackButtonUtil_t2880819819 *)IsInstClass(L_3, BackButtonUtil_t2880819819_il2cpp_TypeInfo_var)));
		BackButtonUtil_t2880819819 * L_4 = ((BackButtonUtil_t2880819819_StaticFields*)BackButtonUtil_t2880819819_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		bool L_5 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m1780991845(NULL /*static, unused*/, _stringLiteral4026408513, /*hidden argument*/NULL);
	}

IL_0043:
	{
		BackButtonUtil_t2880819819 * L_6 = ((BackButtonUtil_t2880819819_StaticFields*)BackButtonUtil_t2880819819_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		return L_6;
	}
}
// System.Void BackButtonUtil::Update()
extern "C"  void BackButtonUtil_Update_m1361867723 (BackButtonUtil_t2880819819 * __this, const MethodInfo* method)
{
	{
		BackButtonUtil_GetBackKey_m3924949722(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BackButtonUtil::GetBackKey()
extern "C"  void BackButtonUtil_GetBackKey_m3924949722 (BackButtonUtil_t2880819819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BackButtonUtil_GetBackKey_m3924949722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m2296100099(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		int32_t L_1 = __this->get_backKeyAction_2();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0057;
	}

IL_002d:
	{
		Application_Quit_m1672997886(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0037:
	{
		String_t* L_5 = __this->get_nextSceneName_3();
		Application_LoadLevel_m1553385752(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0047:
	{
		UnityEvent_t2581268647 * L_6 = __this->get_onClickBack_4();
		NullCheck(L_6);
		UnityEvent_Invoke_m1072399001(L_6, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0057:
	{
		return;
	}
}
// System.Void BackButtonUtil::ChangeCommand(BackKeyAction)
extern "C"  void BackButtonUtil_ChangeCommand_m1611284073 (BackButtonUtil_t2880819819 * __this, int32_t ___action0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___action0;
		__this->set_backKeyAction_2(L_0);
		return;
	}
}
// System.Void BackButtonUtil::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void BackButtonUtil_AddListener_m2838866814 (BackButtonUtil_t2880819819 * __this, UnityAction_t3245792599 * ___action0, const MethodInfo* method)
{
	{
		UnityEvent_t2581268647 * L_0 = __this->get_onClickBack_4();
		UnityAction_t3245792599 * L_1 = ___action0;
		NullCheck(L_0);
		UnityEvent_AddListener_m3274227256(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BackButtonUtil::RemoveListener(UnityEngine.Events.UnityAction)
extern "C"  void BackButtonUtil_RemoveListener_m2446782985 (BackButtonUtil_t2880819819 * __this, UnityAction_t3245792599 * ___action0, const MethodInfo* method)
{
	{
		UnityEvent_t2581268647 * L_0 = __this->get_onClickBack_4();
		UnityAction_t3245792599 * L_1 = ___action0;
		NullCheck(L_0);
		UnityEvent_RemoveListener_m369190218(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BackButtonUtil::RemoveListenerAll()
extern "C"  void BackButtonUtil_RemoveListenerAll_m3158026792 (BackButtonUtil_t2880819819 * __this, const MethodInfo* method)
{
	{
		UnityEvent_t2581268647 * L_0 = __this->get_onClickBack_4();
		NullCheck(L_0);
		UnityEventBase_RemoveAllListeners_m4146875668(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BestScore::.ctor()
extern "C"  void BestScore__ctor_m4285772107 (BestScore_t1653437491 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BestScore::Start()
extern "C"  void BestScore_Start_m1526838621 (BestScore_t1653437491 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BestScore_Start_m1526838621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Text_t1901882714 * L_1 = GameObject_GetComponent_TisText_t1901882714_m2114913816(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m2114913816_MethodInfo_var);
		int32_t L_2 = PlayerPrefs_GetInt_m3299375436(NULL /*static, unused*/, _stringLiteral1512031223, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = Int32_ToString_m141394615((&V_0), /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_3);
		return;
	}
}
// System.Void BestScore::Update()
extern "C"  void BestScore_Update_m901154753 (BestScore_t1653437491 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ButtonHome::.ctor()
extern "C"  void ButtonHome__ctor_m1881016968 (ButtonHome_t643243358 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonHome::OnMouseUpAsButton()
extern "C"  void ButtonHome_OnMouseUpAsButton_m2474744170 (ButtonHome_t643243358 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonHome_OnMouseUpAsButton_m2474744170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_timeScale_m466418001(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)(1.0f))))
		{
			goto IL_0023;
		}
	}
	{
		Time_set_timeScale_m148135804(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		SceneManager_LoadScene_m3660838673(NULL /*static, unused*/, _stringLiteral4110847719, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void ButtonHomeOver::.ctor()
extern "C"  void ButtonHomeOver__ctor_m3234594708 (ButtonHomeOver_t1627401701 * __this, const MethodInfo* method)
{
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1673432820(&L_0, (0.66f), (0.66f), (0.48f), /*hidden argument*/NULL);
		__this->set_colorPushed_2(L_0);
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonHomeOver::Start()
extern "C"  void ButtonHomeOver_Start_m1303276814 (ButtonHomeOver_t1627401701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonHomeOver_Start_m1303276814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t2627027031 * L_1 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_1);
		Material_t340375123 * L_2 = Renderer_get_material_m909181218(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Color_t2555686324  L_3 = Material_get_color_m1173399845(L_2, /*hidden argument*/NULL);
		__this->set_originalColor_3(L_3);
		return;
	}
}
// System.Void ButtonHomeOver::Update()
extern "C"  void ButtonHomeOver_Update_m519570484 (ButtonHomeOver_t1627401701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonHomeOver_Update_m519570484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_isDead_7();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		Collider2D_t2806799626 * L_1 = Component_GetComponent_TisCollider2D_t2806799626_m3391829639(__this, /*hidden argument*/Component_GetComponent_TisCollider2D_t2806799626_m3391829639_MethodInfo_var);
		NullCheck(L_1);
		Behaviour_set_enabled_m3107225489(L_1, (bool)0, /*hidden argument*/NULL);
		goto IL_0027;
	}

IL_001b:
	{
		Collider2D_t2806799626 * L_2 = Component_GetComponent_TisCollider2D_t2806799626_m3391829639(__this, /*hidden argument*/Component_GetComponent_TisCollider2D_t2806799626_m3391829639_MethodInfo_var);
		NullCheck(L_2);
		Behaviour_set_enabled_m3107225489(L_2, (bool)1, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void ButtonHomeOver::OnMouseExit()
extern "C"  void ButtonHomeOver_OnMouseExit_m2701229126 (ButtonHomeOver_t1627401701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonHomeOver_OnMouseExit_m2701229126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t2627027031 * L_1 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_1);
		Material_t340375123 * L_2 = Renderer_get_material_m909181218(L_1, /*hidden argument*/NULL);
		Color_t2555686324  L_3 = __this->get_originalColor_3();
		NullCheck(L_2);
		Material_set_color_m3787036372(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonHomeOver::OnMouseDown()
extern "C"  void ButtonHomeOver_OnMouseDown_m1033544281 (ButtonHomeOver_t1627401701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonHomeOver_OnMouseDown_m1033544281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t2627027031 * L_1 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_1);
		Material_t340375123 * L_2 = Renderer_get_material_m909181218(L_1, /*hidden argument*/NULL);
		Color_t2555686324  L_3 = __this->get_colorPushed_2();
		NullCheck(L_2);
		Material_set_color_m3787036372(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonHomeOver::OnMouseUpAsButton()
extern "C"  void ButtonHomeOver_OnMouseUpAsButton_m3020000816 (ButtonHomeOver_t1627401701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonHomeOver_OnMouseUpAsButton_m3020000816_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_timeScale_m466418001(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)(1.0f))))
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		bool L_1 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_isDead_7();
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		Time_set_timeScale_m148135804(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		Application_LoadLevel_m294528718(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void ButtonPause::.ctor()
extern "C"  void ButtonPause__ctor_m3113433437 (ButtonPause_t3620264363 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonPause::OnMouseUpAsButton()
extern "C"  void ButtonPause_OnMouseUpAsButton_m14291759 (ButtonPause_t3620264363 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonPause_OnMouseUpAsButton_m14291759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Time_set_timeScale_m148135804(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameScreen_t1800529819_il2cpp_TypeInfo_var);
		((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->set_paused_15((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		AudioSource_t3935305588 * L_0 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_musicSource_18();
		NullCheck(L_0);
		AudioSource_Stop_m534887514(L_0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = __this->get_pause_screen_2();
		NullCheck(L_1);
		GameObject_SetActive_m3735894026(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonPlay::.ctor()
extern "C"  void ButtonPlay__ctor_m1200180964 (ButtonPlay_t1912942282 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonPlay::OnMouseUpAsButton()
extern "C"  void ButtonPlay_OnMouseUpAsButton_m3412315649 (ButtonPlay_t1912942282 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonPlay_OnMouseUpAsButton_m3412315649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m3660838673(NULL /*static, unused*/, _stringLiteral3105844272, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonReplay::.ctor()
extern "C"  void ButtonReplay__ctor_m2159060673 (ButtonReplay_t987972804 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonReplay::OnMouseUpAsButton()
extern "C"  void ButtonReplay_OnMouseUpAsButton_m819792831 (ButtonReplay_t987972804 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonReplay_OnMouseUpAsButton_m819792831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_timeScale_m466418001(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)(1.0f))))
		{
			goto IL_0023;
		}
	}
	{
		Time_set_timeScale_m148135804(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		SceneManager_LoadScene_m3660838673(NULL /*static, unused*/, _stringLiteral3105844272, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void ButtonReplayOver::.ctor()
extern "C"  void ButtonReplayOver__ctor_m818467342 (ButtonReplayOver_t967915415 * __this, const MethodInfo* method)
{
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1673432820(&L_0, (0.66f), (0.66f), (0.48f), /*hidden argument*/NULL);
		__this->set_colorPushed_2(L_0);
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonReplayOver::Start()
extern "C"  void ButtonReplayOver_Start_m966175552 (ButtonReplayOver_t967915415 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonReplayOver_Start_m966175552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t2627027031 * L_1 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_1);
		Material_t340375123 * L_2 = Renderer_get_material_m909181218(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Color_t2555686324  L_3 = Material_get_color_m1173399845(L_2, /*hidden argument*/NULL);
		__this->set_originalColor_3(L_3);
		return;
	}
}
// System.Void ButtonReplayOver::Update()
extern "C"  void ButtonReplayOver_Update_m2671482342 (ButtonReplayOver_t967915415 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonReplayOver_Update_m2671482342_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_isDead_7();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		Collider2D_t2806799626 * L_1 = Component_GetComponent_TisCollider2D_t2806799626_m3391829639(__this, /*hidden argument*/Component_GetComponent_TisCollider2D_t2806799626_m3391829639_MethodInfo_var);
		NullCheck(L_1);
		Behaviour_set_enabled_m3107225489(L_1, (bool)0, /*hidden argument*/NULL);
		goto IL_0027;
	}

IL_001b:
	{
		Collider2D_t2806799626 * L_2 = Component_GetComponent_TisCollider2D_t2806799626_m3391829639(__this, /*hidden argument*/Component_GetComponent_TisCollider2D_t2806799626_m3391829639_MethodInfo_var);
		NullCheck(L_2);
		Behaviour_set_enabled_m3107225489(L_2, (bool)1, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void ButtonReplayOver::OnMouseExit()
extern "C"  void ButtonReplayOver_OnMouseExit_m475027834 (ButtonReplayOver_t967915415 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonReplayOver_OnMouseExit_m475027834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t2627027031 * L_1 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_1);
		Material_t340375123 * L_2 = Renderer_get_material_m909181218(L_1, /*hidden argument*/NULL);
		Color_t2555686324  L_3 = __this->get_originalColor_3();
		NullCheck(L_2);
		Material_set_color_m3787036372(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonReplayOver::OnMouseDown()
extern "C"  void ButtonReplayOver_OnMouseDown_m2762209401 (ButtonReplayOver_t967915415 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonReplayOver_OnMouseDown_m2762209401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t2627027031 * L_1 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_1);
		Material_t340375123 * L_2 = Renderer_get_material_m909181218(L_1, /*hidden argument*/NULL);
		Color_t2555686324  L_3 = __this->get_colorPushed_2();
		NullCheck(L_2);
		Material_set_color_m3787036372(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonReplayOver::OnMouseUpAsButton()
extern "C"  void ButtonReplayOver_OnMouseUpAsButton_m3733030786 (ButtonReplayOver_t967915415 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonReplayOver_OnMouseUpAsButton_m3733030786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_timeScale_m466418001(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)(1.0f))))
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		bool L_1 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_isDead_7();
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		Time_set_timeScale_m148135804(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		Application_LoadLevel_m294528718(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void ButtonResume::.ctor()
extern "C"  void ButtonResume__ctor_m2371696234 (ButtonResume_t2840336175 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonResume::OnMouseUpAsButton()
extern "C"  void ButtonResume_OnMouseUpAsButton_m4236015 (ButtonResume_t2840336175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonResume_OnMouseUpAsButton_m4236015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MenuScreen_t742580752_il2cpp_TypeInfo_var);
		bool L_0 = ((MenuScreen_t742580752_StaticFields*)MenuScreen_t742580752_il2cpp_TypeInfo_var->static_fields)->get_isSoundOn_8();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		AudioSource_t3935305588 * L_1 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_musicSource_18();
		NullCheck(L_1);
		AudioSource_Play_m2606248505(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		Time_set_timeScale_m148135804(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameScreen_t1800529819_il2cpp_TypeInfo_var);
		((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->set_paused_15((bool)0);
		GameObject_t1113636619 * L_2 = GameObject_Find_m3458517891(NULL /*static, unused*/, _stringLiteral761323491, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m3735894026(L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonSound::.ctor()
extern "C"  void ButtonSound__ctor_m1598144856 (ButtonSound_t35905277 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonSound::Start()
extern "C"  void ButtonSound_Start_m3676977402 (ButtonSound_t35905277 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonSound_Start_m3676977402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Bounds_t2266837910  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Bounds_t2266837910  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t3722313464  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m102368104(L_0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_2 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m2921103810(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m102368104(L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = Vector3_op_Subtraction_m2566684344(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_z_3();
		V_0 = L_6;
		Camera_t4157153871 * L_7 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = V_0;
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m1197556204(&L_9, (0.0f), (0.0f), L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_10 = Camera_ViewportToWorldPoint_m3480887959(L_7, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		float L_11 = (&V_3)->get_x_1();
		V_2 = L_11;
		Camera_t4157153871 * L_12 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = V_0;
		Vector3_t3722313464  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m1197556204(&L_14, (1.0f), (0.0f), L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t3722313464  L_15 = Camera_ViewportToWorldPoint_m3480887959(L_12, L_14, /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = (&V_5)->get_x_1();
		V_4 = L_16;
		Camera_t4157153871 * L_17 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = V_0;
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m1197556204(&L_19, (0.0f), (0.0f), ((-L_18)), /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t3722313464  L_20 = Camera_ViewportToWorldPoint_m3480887959(L_17, L_19, /*hidden argument*/NULL);
		V_7 = L_20;
		float L_21 = (&V_7)->get_y_2();
		V_6 = L_21;
		Camera_t4157153871 * L_22 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = V_0;
		Vector3_t3722313464  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m1197556204(&L_24, (1.0f), (0.0f), L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t3722313464  L_25 = Camera_ViewportToWorldPoint_m3480887959(L_22, L_24, /*hidden argument*/NULL);
		V_9 = L_25;
		float L_26 = (&V_9)->get_y_2();
		V_8 = L_26;
		Transform_t3600365921 * L_27 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		float L_28 = V_4;
		Transform_t3600365921 * L_29 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Renderer_t2627027031 * L_30 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(L_29, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_MethodInfo_var);
		NullCheck(L_30);
		Bounds_t2266837910  L_31 = Renderer_get_bounds_m2197226282(L_30, /*hidden argument*/NULL);
		V_10 = L_31;
		Vector3_t3722313464  L_32 = Bounds_get_size_m1171376090((&V_10), /*hidden argument*/NULL);
		V_11 = L_32;
		float L_33 = (&V_11)->get_x_1();
		float L_34 = V_6;
		Transform_t3600365921 * L_35 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		Renderer_t2627027031 * L_36 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(L_35, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_MethodInfo_var);
		NullCheck(L_36);
		Bounds_t2266837910  L_37 = Renderer_get_bounds_m2197226282(L_36, /*hidden argument*/NULL);
		V_12 = L_37;
		Vector3_t3722313464  L_38 = Bounds_get_size_m1171376090((&V_12), /*hidden argument*/NULL);
		V_13 = L_38;
		float L_39 = (&V_13)->get_y_2();
		Vector3_t3722313464  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector3__ctor_m1197556204(&L_40, ((float)((float)L_28-(float)L_33)), ((float)((float)L_34-(float)L_39)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_position_m2361854178(L_27, L_40, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_41 = GameObject_Find_m3458517891(NULL /*static, unused*/, _stringLiteral2972356645, /*hidden argument*/NULL);
		__this->set_buttonOff_4(L_41);
		GameObject_t1113636619 * L_42 = __this->get_buttonOff_4();
		NullCheck(L_42);
		Transform_t3600365921 * L_43 = GameObject_get_transform_m393750976(L_42, /*hidden argument*/NULL);
		Transform_t3600365921 * L_44 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_t3722313464  L_45 = Transform_get_position_m102368104(L_44, /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_set_position_m2361854178(L_43, L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MenuScreen_t742580752_il2cpp_TypeInfo_var);
		bool L_46 = ((MenuScreen_t742580752_StaticFields*)MenuScreen_t742580752_il2cpp_TypeInfo_var->static_fields)->get_isSoundOn_8();
		if (!L_46)
		{
			goto IL_017b;
		}
	}
	{
		GameObject_t1113636619 * L_47 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Renderer_t2627027031 * L_48 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_47, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_48);
		Renderer_set_enabled_m2710705614(L_48, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_49 = __this->get_buttonOff_4();
		NullCheck(L_49);
		GameObject_t1113636619 * L_50 = GameObject_get_gameObject_m1412666805(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		Renderer_t2627027031 * L_51 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_50, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_51);
		Renderer_set_enabled_m2710705614(L_51, (bool)0, /*hidden argument*/NULL);
		goto IL_01a2;
	}

IL_017b:
	{
		GameObject_t1113636619 * L_52 = __this->get_buttonOff_4();
		NullCheck(L_52);
		GameObject_t1113636619 * L_53 = GameObject_get_gameObject_m1412666805(L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		Renderer_t2627027031 * L_54 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_53, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_54);
		Renderer_set_enabled_m2710705614(L_54, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_55 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_55);
		Renderer_t2627027031 * L_56 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_55, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_56);
		Renderer_set_enabled_m2710705614(L_56, (bool)0, /*hidden argument*/NULL);
	}

IL_01a2:
	{
		return;
	}
}
// System.Void ButtonSound::OnMouseUpAsButton()
extern "C"  void ButtonSound_OnMouseUpAsButton_m3356435928 (ButtonSound_t35905277 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonSound_OnMouseUpAsButton_m3356435928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MenuScreen_t742580752_il2cpp_TypeInfo_var);
		bool L_0 = ((MenuScreen_t742580752_StaticFields*)MenuScreen_t742580752_il2cpp_TypeInfo_var->static_fields)->get_isSoundOn_8();
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MenuScreen_t742580752_il2cpp_TypeInfo_var);
		((MenuScreen_t742580752_StaticFields*)MenuScreen_t742580752_il2cpp_TypeInfo_var->static_fields)->set_isSoundOn_8((bool)0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Renderer_t2627027031 * L_2 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_1, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_2);
		Renderer_set_enabled_m2710705614(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_buttonOff_4();
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = GameObject_get_gameObject_m1412666805(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t2627027031 * L_5 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_5);
		Renderer_set_enabled_m2710705614(L_5, (bool)1, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MenuScreen_t742580752_il2cpp_TypeInfo_var);
		((MenuScreen_t742580752_StaticFields*)MenuScreen_t742580752_il2cpp_TypeInfo_var->static_fields)->set_isSoundOn_8((bool)1);
		GameObject_t1113636619 * L_6 = __this->get_buttonOff_4();
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = GameObject_get_gameObject_m1412666805(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Renderer_t2627027031 * L_8 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_7, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_8);
		Renderer_set_enabled_m2710705614(L_8, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Renderer_t2627027031 * L_10 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_9, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_10);
		Renderer_set_enabled_m2710705614(L_10, (bool)1, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return;
	}
}
// System.Void Coin::.ctor()
extern "C"  void Coin__ctor_m1037002922 (Coin_t2227745140 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coin::Start()
extern "C"  void Coin_Start_m2748608350 (Coin_t2227745140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Coin_Start_m2748608350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_Find_m54355842(L_0, _stringLiteral2154226729, /*hidden argument*/NULL);
		__this->set_sideCheck_7(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		AudioSource_t3935305588 * L_3 = GameObject_AddComponent_TisAudioSource_t3935305588_m2869327390(L_2, /*hidden argument*/GameObject_AddComponent_TisAudioSource_t3935305588_m2869327390_MethodInfo_var);
		__this->set_coinPickSource_6(L_3);
		AudioSource_t3935305588 * L_4 = __this->get_coinPickSource_6();
		NullCheck(L_4);
		AudioSource_set_playOnAwake_m3967847264(L_4, (bool)0, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_5 = __this->get_coinPickSource_6();
		NullCheck(L_5);
		AudioSource_set_rolloffMode_m1566118045(L_5, 1, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_6 = __this->get_coinPickSource_6();
		AudioClip_t3680889665 * L_7 = __this->get_coinPickClip_5();
		NullCheck(L_6);
		AudioSource_set_clip_m1255174161(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coin::Update()
extern "C"  void Coin_Update_m1064422578 (Coin_t2227745140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Coin_Update_m1064422578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t3722313464  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m102368104(L_0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_2 = Vector2_op_Implicit_m1304503157(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = __this->get_sideCheck_7();
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m102368104(L_3, /*hidden argument*/NULL);
		Vector2_t2156229523  L_5 = Vector2_op_Implicit_m1304503157(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int32_t L_6 = LayerMask_NameToLayer_m4006107104(NULL /*static, unused*/, _stringLiteral3128803744, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_7 = Physics2D_Linecast_m3585412810(NULL /*static, unused*/, L_2, L_5, ((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)31))))), /*hidden argument*/NULL);
		bool L_8 = RaycastHit2D_op_Implicit_m2486231880(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set_inTrain_2(L_8);
		Transform_t3600365921 * L_9 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3722313464  L_10 = Transform_get_position_m102368104(L_9, /*hidden argument*/NULL);
		Vector2_t2156229523  L_11 = Vector2_op_Implicit_m1304503157(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Transform_t3600365921 * L_12 = __this->get_sideCheck_7();
		NullCheck(L_12);
		Vector3_t3722313464  L_13 = Transform_get_position_m102368104(L_12, /*hidden argument*/NULL);
		Vector2_t2156229523  L_14 = Vector2_op_Implicit_m1304503157(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		int32_t L_15 = LayerMask_NameToLayer_m4006107104(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		RaycastHit2D_t2279581989  L_16 = Physics2D_Linecast_m3585412810(NULL /*static, unused*/, L_11, L_14, ((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_15&(int32_t)((int32_t)31))))), /*hidden argument*/NULL);
		bool L_17 = RaycastHit2D_op_Implicit_m2486231880(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		__this->set_collided_3(L_17);
		Transform_t3600365921 * L_18 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t3722313464  L_19 = Transform_get_position_m102368104(L_18, /*hidden argument*/NULL);
		Vector2_t2156229523  L_20 = Vector2_op_Implicit_m1304503157(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Transform_t3600365921 * L_21 = __this->get_sideCheck_7();
		NullCheck(L_21);
		Vector3_t3722313464  L_22 = Transform_get_position_m102368104(L_21, /*hidden argument*/NULL);
		Vector2_t2156229523  L_23 = Vector2_op_Implicit_m1304503157(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		int32_t L_24 = LayerMask_NameToLayer_m4006107104(NULL /*static, unused*/, _stringLiteral2228767570, /*hidden argument*/NULL);
		RaycastHit2D_t2279581989  L_25 = Physics2D_Linecast_m3585412810(NULL /*static, unused*/, L_20, L_23, ((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_24&(int32_t)((int32_t)31))))), /*hidden argument*/NULL);
		bool L_26 = RaycastHit2D_op_Implicit_m2486231880(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		__this->set_inObsUp_4(L_26);
		bool L_27 = __this->get_inTrain_2();
		if (!L_27)
		{
			goto IL_0117;
		}
	}
	{
		Transform_t3600365921 * L_28 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_29 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t3722313464  L_30 = Transform_get_position_m102368104(L_29, /*hidden argument*/NULL);
		V_0 = L_30;
		float L_31 = (&V_0)->get_x_1();
		Transform_t3600365921 * L_32 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t3722313464  L_33 = Transform_get_position_m102368104(L_32, /*hidden argument*/NULL);
		V_1 = L_33;
		float L_34 = (&V_1)->get_y_2();
		Transform_t3600365921 * L_35 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t3722313464  L_36 = Transform_get_position_m102368104(L_35, /*hidden argument*/NULL);
		V_2 = L_36;
		float L_37 = (&V_2)->get_z_3();
		Vector3_t3722313464  L_38;
		memset(&L_38, 0, sizeof(L_38));
		Vector3__ctor_m1197556204(&L_38, L_31, ((float)((float)L_34+(float)(1.0f))), L_37, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_position_m2361854178(L_28, L_38, /*hidden argument*/NULL);
	}

IL_0117:
	{
		bool L_39 = __this->get_inObsUp_4();
		if (!L_39)
		{
			goto IL_0173;
		}
	}
	{
		Transform_t3600365921 * L_40 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_41 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t3722313464  L_42 = Transform_get_position_m102368104(L_41, /*hidden argument*/NULL);
		V_3 = L_42;
		float L_43 = (&V_3)->get_x_1();
		Transform_t3600365921 * L_44 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_t3722313464  L_45 = Transform_get_position_m102368104(L_44, /*hidden argument*/NULL);
		V_4 = L_45;
		float L_46 = (&V_4)->get_y_2();
		Transform_t3600365921 * L_47 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Vector3_t3722313464  L_48 = Transform_get_position_m102368104(L_47, /*hidden argument*/NULL);
		V_5 = L_48;
		float L_49 = (&V_5)->get_z_3();
		Vector3_t3722313464  L_50;
		memset(&L_50, 0, sizeof(L_50));
		Vector3__ctor_m1197556204(&L_50, L_43, ((float)((float)L_46-(float)(0.8f))), L_49, /*hidden argument*/NULL);
		NullCheck(L_40);
		Transform_set_position_m2361854178(L_40, L_50, /*hidden argument*/NULL);
	}

IL_0173:
	{
		bool L_51 = __this->get_collided_3();
		if (!L_51)
		{
			goto IL_01e7;
		}
	}
	{
		Transform_t3600365921 * L_52 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_53 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_53);
		Vector3_t3722313464  L_54 = Transform_get_position_m102368104(L_53, /*hidden argument*/NULL);
		V_6 = L_54;
		float L_55 = (&V_6)->get_x_1();
		Transform_t3600365921 * L_56 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		Vector3_t3722313464  L_57 = Transform_get_position_m102368104(L_56, /*hidden argument*/NULL);
		V_7 = L_57;
		float L_58 = (&V_7)->get_y_2();
		Transform_t3600365921 * L_59 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		Vector3_t3722313464  L_60 = Transform_get_position_m102368104(L_59, /*hidden argument*/NULL);
		V_8 = L_60;
		float L_61 = (&V_8)->get_z_3();
		Vector3_t3722313464  L_62;
		memset(&L_62, 0, sizeof(L_62));
		Vector3__ctor_m1197556204(&L_62, ((float)((float)L_55-(float)(25.0f))), L_58, L_61, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_set_position_m2361854178(L_52, L_62, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_63 = __this->get_coinPickSource_6();
		NullCheck(L_63);
		AudioSource_Play_m2606248505(L_63, /*hidden argument*/NULL);
		int32_t L_64 = ((Score_t2516855617_StaticFields*)Score_t2516855617_il2cpp_TypeInfo_var->static_fields)->get_scoreNum_2();
		((Score_t2516855617_StaticFields*)Score_t2516855617_il2cpp_TypeInfo_var->static_fields)->set_scoreNum_2(((int32_t)((int32_t)L_64+(int32_t)1)));
	}

IL_01e7:
	{
		return;
	}
}
// System.Void DontDestroy::.ctor()
extern "C"  void DontDestroy__ctor_m3070423980 (DontDestroy_t1446738193 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DontDestroy::Awake()
extern "C"  void DontDestroy_Awake_m3302783692 (DontDestroy_t1446738193 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DontDestroy_Awake_m3302783692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m3153277066(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameScreen::.ctor()
extern "C"  void GameScreen__ctor_m2718863737 (GameScreen_t1800529819 * __this, const MethodInfo* method)
{
	{
		__this->set_objectMax_27(6);
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameScreen::Start()
extern "C"  void GameScreen_Start_m2902530691 (GameScreen_t1800529819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen_Start_m2902530691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Bounds_t2266837910  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Bounds_t2266837910  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Bounds_t2266837910  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	GameObject_t1113636619 * V_12 = NULL;
	int16_t V_13 = 0;
	Vector3_t3722313464  V_14;
	memset(&V_14, 0, sizeof(V_14));
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->set_speed_9((-2.8f));
		IL2CPP_RUNTIME_CLASS_INIT(GameScreen_t1800529819_il2cpp_TypeInfo_var);
		((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->set_paused_15((bool)0);
		Transform_t3600365921 * L_0 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m102368104(L_0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_2 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m2921103810(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m102368104(L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = Vector3_op_Subtraction_m2566684344(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_z_3();
		V_0 = L_6;
		Camera_t4157153871 * L_7 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = V_0;
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m1197556204(&L_9, (0.0f), (0.0f), L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_10 = Camera_ViewportToWorldPoint_m3480887959(L_7, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = (&V_2)->get_x_1();
		__this->set_leftX_9(L_11);
		Camera_t4157153871 * L_12 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = V_0;
		Vector3_t3722313464  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m1197556204(&L_14, (1.0f), (0.0f), L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t3722313464  L_15 = Camera_ViewportToWorldPoint_m3480887959(L_12, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = (&V_3)->get_x_1();
		__this->set_rightX_11(L_16);
		Camera_t4157153871 * L_17 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = V_0;
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m1197556204(&L_19, (0.0f), (0.0f), ((-L_18)), /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t3722313464  L_20 = Camera_ViewportToWorldPoint_m3480887959(L_17, L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		float L_21 = (&V_4)->get_y_2();
		__this->set_topY_10(L_21);
		Camera_t4157153871 * L_22 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = V_0;
		Vector3_t3722313464  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m1197556204(&L_24, (1.0f), (0.0f), L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t3722313464  L_25 = Camera_ViewportToWorldPoint_m3480887959(L_22, L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		float L_26 = (&V_5)->get_y_2();
		__this->set_bottomY_8(L_26);
		GameObject_t1113636619 * L_27 = GameObject_Find_m3458517891(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		__this->set_player_7(L_27);
		GameObject_t1113636619 * L_28 = __this->get_obsPrefab_4();
		NullCheck(L_28);
		Renderer_t2627027031 * L_29 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_28, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_29);
		Bounds_t2266837910  L_30 = Renderer_get_bounds_m2197226282(L_29, /*hidden argument*/NULL);
		V_6 = L_30;
		Vector3_t3722313464  L_31 = Bounds_get_size_m1171376090((&V_6), /*hidden argument*/NULL);
		V_7 = L_31;
		float L_32 = (&V_7)->get_y_2();
		__this->set_obsTileH_12(L_32);
		GameObject_t1113636619 * L_33 = __this->get_obs2Prefab_5();
		NullCheck(L_33);
		Renderer_t2627027031 * L_34 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_33, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_34);
		Bounds_t2266837910  L_35 = Renderer_get_bounds_m2197226282(L_34, /*hidden argument*/NULL);
		V_8 = L_35;
		Vector3_t3722313464  L_36 = Bounds_get_size_m1171376090((&V_8), /*hidden argument*/NULL);
		V_9 = L_36;
		float L_37 = (&V_9)->get_y_2();
		__this->set_obs2TileH_13(L_37);
		GameObject_t1113636619 * L_38 = __this->get_groundPrefab_3();
		NullCheck(L_38);
		Renderer_t2627027031 * L_39 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_38, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_39);
		Bounds_t2266837910  L_40 = Renderer_get_bounds_m2197226282(L_39, /*hidden argument*/NULL);
		V_10 = L_40;
		Vector3_t3722313464  L_41 = Bounds_get_size_m1171376090((&V_10), /*hidden argument*/NULL);
		V_11 = L_41;
		float L_42 = (&V_11)->get_y_2();
		__this->set_groundTileH_14(L_42);
		GameObject_t1113636619 * L_43 = __this->get_obsPrefab_4();
		NullCheck(L_43);
		Collider2D_t2806799626 * L_44 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_43, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		__this->set_obsCollider_20(((BoxCollider2D_t3581341831 *)IsInstSealed(L_44, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var)));
		GameObject_t1113636619 * L_45 = __this->get_obs2Prefab_5();
		NullCheck(L_45);
		Collider2D_t2806799626 * L_46 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_45, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		__this->set_obs2Collider_21(((BoxCollider2D_t3581341831 *)IsInstSealed(L_46, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var)));
		__this->set_gap_17((10.0f));
		int32_t L_47 = __this->get_objectMax_27();
		__this->set_obs_6(((GameObjectU5BU5D_t3328599146*)SZArrayNew(GameObjectU5BU5D_t3328599146_il2cpp_TypeInfo_var, (uint32_t)L_47)));
		GameObject_t1113636619 * L_48 = GameScreen_getRandomObs_m1406040211(__this, /*hidden argument*/NULL);
		V_12 = L_48;
		GameObjectU5BU5D_t3328599146* L_49 = __this->get_obs_6();
		GameObject_t1113636619 * L_50 = V_12;
		float L_51 = __this->get_rightX_11();
		float L_52 = __this->get_bottomY_8();
		float L_53 = __this->get_groundTileH_14();
		float L_54 = __this->get_obsHeightScale_16();
		Vector3_t3722313464  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Vector3__ctor_m1197556204(&L_55, ((float)((float)L_51+(float)(5.0f))), ((float)((float)((float)((float)L_52+(float)((float)((float)L_53/(float)(2.0f)))))+(float)L_54)), (0.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_56 = Quaternion_get_identity_m3578010038(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_57 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_50, L_55, L_56, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_MethodInfo_var);
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, L_57);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(0), (GameObject_t1113636619 *)L_57);
		V_13 = (int16_t)1;
		goto IL_026d;
	}

IL_01ff:
	{
		GameObject_t1113636619 * L_58 = GameScreen_getRandomObs_m1406040211(__this, /*hidden argument*/NULL);
		V_12 = L_58;
		GameObjectU5BU5D_t3328599146* L_59 = __this->get_obs_6();
		int16_t L_60 = V_13;
		GameObject_t1113636619 * L_61 = V_12;
		GameObjectU5BU5D_t3328599146* L_62 = __this->get_obs_6();
		int16_t L_63 = V_13;
		NullCheck(L_62);
		int32_t L_64 = ((int32_t)((int32_t)L_63-(int32_t)1));
		GameObject_t1113636619 * L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		Transform_t3600365921 * L_66 = GameObject_get_transform_m393750976(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		Vector3_t3722313464  L_67 = Transform_get_position_m102368104(L_66, /*hidden argument*/NULL);
		V_14 = L_67;
		float L_68 = (&V_14)->get_x_1();
		int32_t L_69 = GameScreen_RandomGap_m3185011755(__this, /*hidden argument*/NULL);
		float L_70 = __this->get_bottomY_8();
		float L_71 = __this->get_groundTileH_14();
		float L_72 = __this->get_obsHeightScale_16();
		Vector3_t3722313464  L_73;
		memset(&L_73, 0, sizeof(L_73));
		Vector3__ctor_m1197556204(&L_73, ((float)((float)L_68+(float)(((float)((float)L_69))))), ((float)((float)((float)((float)L_70+(float)((float)((float)L_71/(float)(2.0f)))))+(float)L_72)), (0.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_74 = Quaternion_get_identity_m3578010038(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_75 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_61, L_73, L_74, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_MethodInfo_var);
		NullCheck(L_59);
		ArrayElementTypeCheck (L_59, L_75);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(L_60), (GameObject_t1113636619 *)L_75);
		int16_t L_76 = V_13;
		V_13 = (((int16_t)((int16_t)((int32_t)((int32_t)L_76+(int32_t)1)))));
	}

IL_026d:
	{
		int16_t L_77 = V_13;
		int32_t L_78 = __this->get_objectMax_27();
		if ((((int32_t)L_77) < ((int32_t)L_78)))
		{
			goto IL_01ff;
		}
	}
	{
		Il2CppObject * L_79 = GameScreen_changeSpeed_m1491875547(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m4001331470(__this, L_79, /*hidden argument*/NULL);
		Il2CppObject * L_80 = GameScreen_changeScore_m2232000133(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m4001331470(__this, L_80, /*hidden argument*/NULL);
		Il2CppObject * L_81 = GameScreen_showIfGameOver_m3115583330(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m4001331470(__this, L_81, /*hidden argument*/NULL);
		Il2CppObject * L_82 = GameScreen_hideInfo_m3665555316(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m4001331470(__this, L_82, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_83 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_83);
		AudioSource_t3935305588 * L_84 = GameObject_AddComponent_TisAudioSource_t3935305588_m2869327390(L_83, /*hidden argument*/GameObject_AddComponent_TisAudioSource_t3935305588_m2869327390_MethodInfo_var);
		__this->set_overSource_23(L_84);
		AudioSource_t3935305588 * L_85 = __this->get_overSource_23();
		NullCheck(L_85);
		AudioSource_set_playOnAwake_m3967847264(L_85, (bool)0, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_86 = __this->get_overSource_23();
		NullCheck(L_86);
		AudioSource_set_rolloffMode_m1566118045(L_86, 1, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_87 = __this->get_overSource_23();
		AudioClip_t3680889665 * L_88 = __this->get_overClip_22();
		NullCheck(L_87);
		AudioSource_set_clip_m1255174161(L_87, L_88, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_89 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_89);
		AudioSource_t3935305588 * L_90 = GameObject_AddComponent_TisAudioSource_t3935305588_m2869327390(L_89, /*hidden argument*/GameObject_AddComponent_TisAudioSource_t3935305588_m2869327390_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameScreen_t1800529819_il2cpp_TypeInfo_var);
		((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->set_deadSource_25(L_90);
		AudioSource_t3935305588 * L_91 = ((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->get_deadSource_25();
		NullCheck(L_91);
		AudioSource_set_playOnAwake_m3967847264(L_91, (bool)0, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_92 = ((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->get_deadSource_25();
		NullCheck(L_92);
		AudioSource_set_rolloffMode_m1566118045(L_92, 1, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_93 = ((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->get_deadSource_25();
		AudioClip_t3680889665 * L_94 = __this->get_deadClip_24();
		NullCheck(L_93);
		AudioSource_set_clip_m1255174161(L_93, L_94, /*hidden argument*/NULL);
		float L_95 = Time_get_timeScale_m466418001(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)L_95) == ((float)(1.0f))))
		{
			goto IL_0337;
		}
	}
	{
		Time_set_timeScale_m148135804(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
	}

IL_0337:
	{
		return;
	}
}
// UnityEngine.GameObject GameScreen::getRandomObs()
extern "C"  GameObject_t1113636619 * GameScreen_getRandomObs_m1406040211 (GameScreen_t1800529819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen_getRandomObs_m1406040211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	int32_t V_1 = 0;
	Bounds_t2266837910  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Bounds_t2266837910  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		int32_t L_0 = Random_Range_m110872341(NULL /*static, unused*/, 1, 6, /*hidden argument*/NULL);
		V_1 = L_0;
		int32_t L_1 = V_1;
		if ((((int32_t)L_1) > ((int32_t)3)))
		{
			goto IL_0039;
		}
	}
	{
		GameObject_t1113636619 * L_2 = __this->get_obsPrefab_4();
		V_0 = L_2;
		float L_3 = __this->get_obsTileH_12();
		__this->set_obsHeightScale_16(((float)((float)L_3/(float)(5.0f))));
		float L_4 = __this->get_obsHeightScale_16();
		__this->set_height1_18(L_4);
		goto IL_00a5;
	}

IL_0039:
	{
		GameObject_t1113636619 * L_5 = __this->get_obs2Prefab_5();
		V_0 = L_5;
		GameObject_t1113636619 * L_6 = __this->get_player_7();
		NullCheck(L_6);
		Transform_t3600365921 * L_7 = GameObject_get_transform_m393750976(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Renderer_t2627027031 * L_8 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(L_7, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_MethodInfo_var);
		NullCheck(L_8);
		Bounds_t2266837910  L_9 = Renderer_get_bounds_m2197226282(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Vector3_t3722313464  L_10 = Bounds_get_size_m1171376090((&V_2), /*hidden argument*/NULL);
		V_3 = L_10;
		float L_11 = (&V_3)->get_y_2();
		GameObject_t1113636619 * L_12 = __this->get_player_7();
		NullCheck(L_12);
		Transform_t3600365921 * L_13 = GameObject_get_transform_m393750976(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Renderer_t2627027031 * L_14 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(L_13, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_MethodInfo_var);
		NullCheck(L_14);
		Bounds_t2266837910  L_15 = Renderer_get_bounds_m2197226282(L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		Vector3_t3722313464  L_16 = Bounds_get_size_m1171376090((&V_4), /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = (&V_5)->get_y_2();
		__this->set_obsHeightScale_16(((float)((float)L_11+(float)((float)((float)L_17/(float)(30.0f))))));
		float L_18 = __this->get_obsHeightScale_16();
		__this->set_height2_19(L_18);
	}

IL_00a5:
	{
		GameObject_t1113636619 * L_19 = V_0;
		return L_19;
	}
}
// System.Collections.IEnumerator GameScreen::changeSpeed()
extern "C"  Il2CppObject * GameScreen_changeSpeed_m1491875547 (GameScreen_t1800529819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen_changeSpeed_m1491875547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CchangeSpeedU3Ec__Iterator0_t85360554 * V_0 = NULL;
	{
		U3CchangeSpeedU3Ec__Iterator0_t85360554 * L_0 = (U3CchangeSpeedU3Ec__Iterator0_t85360554 *)il2cpp_codegen_object_new(U3CchangeSpeedU3Ec__Iterator0_t85360554_il2cpp_TypeInfo_var);
		U3CchangeSpeedU3Ec__Iterator0__ctor_m857096498(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CchangeSpeedU3Ec__Iterator0_t85360554 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CchangeSpeedU3Ec__Iterator0_t85360554 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator GameScreen::changeScore()
extern "C"  Il2CppObject * GameScreen_changeScore_m2232000133 (GameScreen_t1800529819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen_changeScore_m2232000133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CchangeScoreU3Ec__Iterator1_t982768640 * V_0 = NULL;
	{
		U3CchangeScoreU3Ec__Iterator1_t982768640 * L_0 = (U3CchangeScoreU3Ec__Iterator1_t982768640 *)il2cpp_codegen_object_new(U3CchangeScoreU3Ec__Iterator1_t982768640_il2cpp_TypeInfo_var);
		U3CchangeScoreU3Ec__Iterator1__ctor_m2602245183(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CchangeScoreU3Ec__Iterator1_t982768640 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator GameScreen::hideInfo()
extern "C"  Il2CppObject * GameScreen_hideInfo_m3665555316 (GameScreen_t1800529819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen_hideInfo_m3665555316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3ChideInfoU3Ec__Iterator2_t3795124997 * V_0 = NULL;
	{
		U3ChideInfoU3Ec__Iterator2_t3795124997 * L_0 = (U3ChideInfoU3Ec__Iterator2_t3795124997 *)il2cpp_codegen_object_new(U3ChideInfoU3Ec__Iterator2_t3795124997_il2cpp_TypeInfo_var);
		U3ChideInfoU3Ec__Iterator2__ctor_m1502287029(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3ChideInfoU3Ec__Iterator2_t3795124997 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator GameScreen::showIfGameOver()
extern "C"  Il2CppObject * GameScreen_showIfGameOver_m3115583330 (GameScreen_t1800529819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen_showIfGameOver_m3115583330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CshowIfGameOverU3Ec__Iterator3_t966372778 * V_0 = NULL;
	{
		U3CshowIfGameOverU3Ec__Iterator3_t966372778 * L_0 = (U3CshowIfGameOverU3Ec__Iterator3_t966372778 *)il2cpp_codegen_object_new(U3CshowIfGameOverU3Ec__Iterator3_t966372778_il2cpp_TypeInfo_var);
		U3CshowIfGameOverU3Ec__Iterator3__ctor_m546072851(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CshowIfGameOverU3Ec__Iterator3_t966372778 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CshowIfGameOverU3Ec__Iterator3_t966372778 * L_2 = V_0;
		return L_2;
	}
}
// System.Void GameScreen::Update()
extern "C"  void GameScreen_Update_m3339885975 (GameScreen_t1800529819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen_Update_m3339885975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Bounds_t2266837910  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	ObsDownCollisionCheck_t2541817184 * V_4 = NULL;
	BoxCollider2D_t3581341831 * V_5 = NULL;
	ObsUpCollisionCheck_t2909508583 * V_6 = NULL;
	BoxCollider2D_t3581341831 * V_7 = NULL;
	Vector3_t3722313464  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Bounds_t2266837910  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	int32_t V_12 = 0;
	ObsDownCollisionCheck_t2541817184 * V_13 = NULL;
	BoxCollider2D_t3581341831 * V_14 = NULL;
	ObsUpCollisionCheck_t2909508583 * V_15 = NULL;
	BoxCollider2D_t3581341831 * V_16 = NULL;
	Vector3_t3722313464  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t3722313464  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Bounds_t2266837910  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Vector3_t3722313464  V_20;
	memset(&V_20, 0, sizeof(V_20));
	int32_t V_21 = 0;
	ObsDownCollisionCheck_t2541817184 * V_22 = NULL;
	BoxCollider2D_t3581341831 * V_23 = NULL;
	ObsUpCollisionCheck_t2909508583 * V_24 = NULL;
	BoxCollider2D_t3581341831 * V_25 = NULL;
	Vector3_t3722313464  V_26;
	memset(&V_26, 0, sizeof(V_26));
	Vector3_t3722313464  V_27;
	memset(&V_27, 0, sizeof(V_27));
	Bounds_t2266837910  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Vector3_t3722313464  V_29;
	memset(&V_29, 0, sizeof(V_29));
	int32_t V_30 = 0;
	ObsDownCollisionCheck_t2541817184 * V_31 = NULL;
	BoxCollider2D_t3581341831 * V_32 = NULL;
	ObsUpCollisionCheck_t2909508583 * V_33 = NULL;
	BoxCollider2D_t3581341831 * V_34 = NULL;
	Vector3_t3722313464  V_35;
	memset(&V_35, 0, sizeof(V_35));
	Vector3_t3722313464  V_36;
	memset(&V_36, 0, sizeof(V_36));
	Bounds_t2266837910  V_37;
	memset(&V_37, 0, sizeof(V_37));
	Vector3_t3722313464  V_38;
	memset(&V_38, 0, sizeof(V_38));
	int32_t V_39 = 0;
	ObsDownCollisionCheck_t2541817184 * V_40 = NULL;
	BoxCollider2D_t3581341831 * V_41 = NULL;
	ObsUpCollisionCheck_t2909508583 * V_42 = NULL;
	BoxCollider2D_t3581341831 * V_43 = NULL;
	Vector3_t3722313464  V_44;
	memset(&V_44, 0, sizeof(V_44));
	Vector3_t3722313464  V_45;
	memset(&V_45, 0, sizeof(V_45));
	Bounds_t2266837910  V_46;
	memset(&V_46, 0, sizeof(V_46));
	Vector3_t3722313464  V_47;
	memset(&V_47, 0, sizeof(V_47));
	int32_t V_48 = 0;
	ObsDownCollisionCheck_t2541817184 * V_49 = NULL;
	BoxCollider2D_t3581341831 * V_50 = NULL;
	ObsUpCollisionCheck_t2909508583 * V_51 = NULL;
	BoxCollider2D_t3581341831 * V_52 = NULL;
	Vector3_t3722313464  V_53;
	memset(&V_53, 0, sizeof(V_53));
	{
		GameObjectU5BU5D_t3328599146* L_0 = __this->get_obs_6();
		NullCheck(L_0);
		int32_t L_1 = 0;
		GameObject_t1113636619 * L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m393750976(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m102368104(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_1();
		GameObject_t1113636619 * L_6 = __this->get_obsPrefab_4();
		NullCheck(L_6);
		Renderer_t2627027031 * L_7 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_6, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_7);
		Bounds_t2266837910  L_8 = Renderer_get_bounds_m2197226282(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		Vector3_t3722313464  L_9 = Bounds_get_size_m1171376090((&V_1), /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_x_1();
		float L_11 = __this->get_leftX_9();
		if ((!(((float)((float)((float)L_5+(float)L_10))) < ((float)L_11))))
		{
			goto IL_0214;
		}
	}
	{
		int32_t L_12 = Random_Range_m110872341(NULL /*static, unused*/, 1, 6, /*hidden argument*/NULL);
		V_3 = L_12;
		int32_t L_13 = V_3;
		if ((((int32_t)L_13) > ((int32_t)3)))
		{
			goto IL_00ea;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_14 = __this->get_obs_6();
		NullCheck(L_14);
		int32_t L_15 = 0;
		GameObject_t1113636619 * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		ObsDownCollisionCheck_t2541817184 * L_17 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_16, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		V_4 = L_17;
		ObsDownCollisionCheck_t2541817184 * L_18 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_18, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00d9;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_20 = __this->get_obs_6();
		NullCheck(L_20);
		int32_t L_21 = 0;
		GameObject_t1113636619 * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		SpriteRenderer_t3235626157 * L_23 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_22, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_24 = __this->get_obsPrefab_4();
		NullCheck(L_24);
		SpriteRenderer_t3235626157 * L_25 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_24, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_25);
		Sprite_t280657092 * L_26 = SpriteRenderer_get_sprite_m688744963(L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		SpriteRenderer_set_sprite_m3489358652(L_23, L_26, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_27 = __this->get_obs_6();
		NullCheck(L_27);
		int32_t L_28 = 0;
		GameObject_t1113636619 * L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		Collider2D_t2806799626 * L_30 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_29, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_5 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_30, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_31 = V_5;
		BoxCollider2D_t3581341831 * L_32 = __this->get_obsCollider_20();
		NullCheck(L_32);
		Vector2_t2156229523  L_33 = BoxCollider2D_get_size_m2899090007(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		BoxCollider2D_set_size_m2456142094(L_31, L_33, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_34 = __this->get_obs_6();
		NullCheck(L_34);
		int32_t L_35 = 0;
		GameObject_t1113636619 * L_36 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_36);
		ObsUpCollisionCheck_t2909508583 * L_37 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_36, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_38 = __this->get_obs_6();
		NullCheck(L_38);
		int32_t L_39 = 0;
		GameObject_t1113636619 * L_40 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		NullCheck(L_40);
		GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560(L_40, /*hidden argument*/GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560_MethodInfo_var);
	}

IL_00d9:
	{
		float L_41 = __this->get_height1_18();
		__this->set_obsHeightScale_16(L_41);
		goto IL_0195;
	}

IL_00ea:
	{
		GameObjectU5BU5D_t3328599146* L_42 = __this->get_obs_6();
		NullCheck(L_42);
		int32_t L_43 = 0;
		GameObject_t1113636619 * L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		NullCheck(L_44);
		ObsUpCollisionCheck_t2909508583 * L_45 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_44, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		V_6 = L_45;
		ObsUpCollisionCheck_t2909508583 * L_46 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_47 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_46, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_016e;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_48 = __this->get_obs_6();
		NullCheck(L_48);
		int32_t L_49 = 0;
		GameObject_t1113636619 * L_50 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		NullCheck(L_50);
		SpriteRenderer_t3235626157 * L_51 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_50, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_52 = __this->get_obs2Prefab_5();
		NullCheck(L_52);
		SpriteRenderer_t3235626157 * L_53 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_52, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_53);
		Sprite_t280657092 * L_54 = SpriteRenderer_get_sprite_m688744963(L_53, /*hidden argument*/NULL);
		NullCheck(L_51);
		SpriteRenderer_set_sprite_m3489358652(L_51, L_54, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_55 = __this->get_obs_6();
		NullCheck(L_55);
		int32_t L_56 = 0;
		GameObject_t1113636619 * L_57 = (L_55)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		NullCheck(L_57);
		Collider2D_t2806799626 * L_58 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_57, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_7 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_58, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_59 = V_7;
		BoxCollider2D_t3581341831 * L_60 = __this->get_obs2Collider_21();
		NullCheck(L_60);
		Vector2_t2156229523  L_61 = BoxCollider2D_get_size_m2899090007(L_60, /*hidden argument*/NULL);
		NullCheck(L_59);
		BoxCollider2D_set_size_m2456142094(L_59, L_61, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_62 = __this->get_obs_6();
		NullCheck(L_62);
		int32_t L_63 = 0;
		GameObject_t1113636619 * L_64 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		NullCheck(L_64);
		ObsDownCollisionCheck_t2541817184 * L_65 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_64, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_66 = __this->get_obs_6();
		NullCheck(L_66);
		int32_t L_67 = 0;
		GameObject_t1113636619 * L_68 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		NullCheck(L_68);
		GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873(L_68, /*hidden argument*/GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873_MethodInfo_var);
	}

IL_016e:
	{
		float L_69 = __this->get_height2_19();
		__this->set_obsHeightScale_16(L_69);
		float L_70 = __this->get_obsHeightScale_16();
		if ((!(((float)L_70) < ((float)(0.0f)))))
		{
			goto IL_0195;
		}
	}
	{
		__this->set_obsHeightScale_16((1.11600018f));
	}

IL_0195:
	{
		GameObjectU5BU5D_t3328599146* L_71 = __this->get_obs_6();
		NullCheck(L_71);
		int32_t L_72 = 0;
		GameObject_t1113636619 * L_73 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		NullCheck(L_73);
		Transform_t3600365921 * L_74 = GameObject_get_transform_m393750976(L_73, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_75 = __this->get_obs_6();
		NullCheck(L_75);
		int32_t L_76 = 5;
		GameObject_t1113636619 * L_77 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		NullCheck(L_77);
		Transform_t3600365921 * L_78 = GameObject_get_transform_m393750976(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		Vector3_t3722313464  L_79 = Transform_get_position_m102368104(L_78, /*hidden argument*/NULL);
		V_8 = L_79;
		float L_80 = (&V_8)->get_x_1();
		int32_t L_81 = GameScreen_RandomGap_m3185011755(__this, /*hidden argument*/NULL);
		float L_82 = __this->get_bottomY_8();
		float L_83 = __this->get_groundTileH_14();
		float L_84 = __this->get_obsHeightScale_16();
		Vector3_t3722313464  L_85;
		memset(&L_85, 0, sizeof(L_85));
		Vector3__ctor_m1197556204(&L_85, ((float)((float)L_80+(float)(((float)((float)L_81))))), ((float)((float)((float)((float)L_82+(float)((float)((float)L_83/(float)(2.0f)))))+(float)L_84)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_74);
		Transform_set_position_m2361854178(L_74, L_85, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_86 = __this->get_obs_6();
		NullCheck(L_86);
		int32_t L_87 = 0;
		GameObject_t1113636619 * L_88 = (L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_87));
		NullCheck(L_88);
		Collider2D_t2806799626 * L_89 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_88, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		NullCheck(L_89);
		Behaviour_set_enabled_m3107225489(L_89, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_90 = __this->get_obs_6();
		NullCheck(L_90);
		int32_t L_91 = 0;
		GameObject_t1113636619 * L_92 = (L_90)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		NullCheck(L_92);
		Renderer_t2627027031 * L_93 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_92, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_93);
		Renderer_set_enabled_m2710705614(L_93, (bool)1, /*hidden argument*/NULL);
	}

IL_0214:
	{
		GameObjectU5BU5D_t3328599146* L_94 = __this->get_obs_6();
		NullCheck(L_94);
		int32_t L_95 = 1;
		GameObject_t1113636619 * L_96 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_95));
		NullCheck(L_96);
		Transform_t3600365921 * L_97 = GameObject_get_transform_m393750976(L_96, /*hidden argument*/NULL);
		NullCheck(L_97);
		Vector3_t3722313464  L_98 = Transform_get_position_m102368104(L_97, /*hidden argument*/NULL);
		V_9 = L_98;
		float L_99 = (&V_9)->get_x_1();
		GameObject_t1113636619 * L_100 = __this->get_obsPrefab_4();
		NullCheck(L_100);
		Renderer_t2627027031 * L_101 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_100, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_101);
		Bounds_t2266837910  L_102 = Renderer_get_bounds_m2197226282(L_101, /*hidden argument*/NULL);
		V_10 = L_102;
		Vector3_t3722313464  L_103 = Bounds_get_size_m1171376090((&V_10), /*hidden argument*/NULL);
		V_11 = L_103;
		float L_104 = (&V_11)->get_x_1();
		float L_105 = __this->get_leftX_9();
		if ((!(((float)((float)((float)L_99+(float)L_104))) < ((float)L_105))))
		{
			goto IL_042d;
		}
	}
	{
		int32_t L_106 = Random_Range_m110872341(NULL /*static, unused*/, 1, 6, /*hidden argument*/NULL);
		V_12 = L_106;
		int32_t L_107 = V_12;
		if ((((int32_t)L_107) > ((int32_t)3)))
		{
			goto IL_0303;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_108 = __this->get_obs_6();
		NullCheck(L_108);
		int32_t L_109 = 1;
		GameObject_t1113636619 * L_110 = (L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_109));
		NullCheck(L_110);
		ObsDownCollisionCheck_t2541817184 * L_111 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_110, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		V_13 = L_111;
		ObsDownCollisionCheck_t2541817184 * L_112 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_113 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_112, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_113)
		{
			goto IL_02f2;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_114 = __this->get_obs_6();
		NullCheck(L_114);
		int32_t L_115 = 1;
		GameObject_t1113636619 * L_116 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_115));
		NullCheck(L_116);
		SpriteRenderer_t3235626157 * L_117 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_116, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_118 = __this->get_obsPrefab_4();
		NullCheck(L_118);
		SpriteRenderer_t3235626157 * L_119 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_118, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_119);
		Sprite_t280657092 * L_120 = SpriteRenderer_get_sprite_m688744963(L_119, /*hidden argument*/NULL);
		NullCheck(L_117);
		SpriteRenderer_set_sprite_m3489358652(L_117, L_120, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_121 = __this->get_obs_6();
		NullCheck(L_121);
		int32_t L_122 = 1;
		GameObject_t1113636619 * L_123 = (L_121)->GetAt(static_cast<il2cpp_array_size_t>(L_122));
		NullCheck(L_123);
		Collider2D_t2806799626 * L_124 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_123, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_14 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_124, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_125 = V_14;
		BoxCollider2D_t3581341831 * L_126 = __this->get_obsCollider_20();
		NullCheck(L_126);
		Vector2_t2156229523  L_127 = BoxCollider2D_get_size_m2899090007(L_126, /*hidden argument*/NULL);
		NullCheck(L_125);
		BoxCollider2D_set_size_m2456142094(L_125, L_127, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_128 = __this->get_obs_6();
		NullCheck(L_128);
		int32_t L_129 = 1;
		GameObject_t1113636619 * L_130 = (L_128)->GetAt(static_cast<il2cpp_array_size_t>(L_129));
		NullCheck(L_130);
		ObsUpCollisionCheck_t2909508583 * L_131 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_130, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_131, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_132 = __this->get_obs_6();
		NullCheck(L_132);
		int32_t L_133 = 1;
		GameObject_t1113636619 * L_134 = (L_132)->GetAt(static_cast<il2cpp_array_size_t>(L_133));
		NullCheck(L_134);
		GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560(L_134, /*hidden argument*/GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560_MethodInfo_var);
	}

IL_02f2:
	{
		float L_135 = __this->get_height1_18();
		__this->set_obsHeightScale_16(L_135);
		goto IL_03ae;
	}

IL_0303:
	{
		GameObjectU5BU5D_t3328599146* L_136 = __this->get_obs_6();
		NullCheck(L_136);
		int32_t L_137 = 1;
		GameObject_t1113636619 * L_138 = (L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_137));
		NullCheck(L_138);
		ObsUpCollisionCheck_t2909508583 * L_139 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_138, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		V_15 = L_139;
		ObsUpCollisionCheck_t2909508583 * L_140 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_141 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_140, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_141)
		{
			goto IL_0387;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_142 = __this->get_obs_6();
		NullCheck(L_142);
		int32_t L_143 = 1;
		GameObject_t1113636619 * L_144 = (L_142)->GetAt(static_cast<il2cpp_array_size_t>(L_143));
		NullCheck(L_144);
		SpriteRenderer_t3235626157 * L_145 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_144, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_146 = __this->get_obs2Prefab_5();
		NullCheck(L_146);
		SpriteRenderer_t3235626157 * L_147 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_146, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_147);
		Sprite_t280657092 * L_148 = SpriteRenderer_get_sprite_m688744963(L_147, /*hidden argument*/NULL);
		NullCheck(L_145);
		SpriteRenderer_set_sprite_m3489358652(L_145, L_148, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_149 = __this->get_obs_6();
		NullCheck(L_149);
		int32_t L_150 = 1;
		GameObject_t1113636619 * L_151 = (L_149)->GetAt(static_cast<il2cpp_array_size_t>(L_150));
		NullCheck(L_151);
		Collider2D_t2806799626 * L_152 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_151, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_16 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_152, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_153 = V_16;
		BoxCollider2D_t3581341831 * L_154 = __this->get_obs2Collider_21();
		NullCheck(L_154);
		Vector2_t2156229523  L_155 = BoxCollider2D_get_size_m2899090007(L_154, /*hidden argument*/NULL);
		NullCheck(L_153);
		BoxCollider2D_set_size_m2456142094(L_153, L_155, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_156 = __this->get_obs_6();
		NullCheck(L_156);
		int32_t L_157 = 1;
		GameObject_t1113636619 * L_158 = (L_156)->GetAt(static_cast<il2cpp_array_size_t>(L_157));
		NullCheck(L_158);
		ObsDownCollisionCheck_t2541817184 * L_159 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_158, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_159, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_160 = __this->get_obs_6();
		NullCheck(L_160);
		int32_t L_161 = 1;
		GameObject_t1113636619 * L_162 = (L_160)->GetAt(static_cast<il2cpp_array_size_t>(L_161));
		NullCheck(L_162);
		GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873(L_162, /*hidden argument*/GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873_MethodInfo_var);
	}

IL_0387:
	{
		float L_163 = __this->get_height2_19();
		__this->set_obsHeightScale_16(L_163);
		float L_164 = __this->get_obsHeightScale_16();
		if ((!(((float)L_164) < ((float)(0.0f)))))
		{
			goto IL_03ae;
		}
	}
	{
		__this->set_obsHeightScale_16((1.11600018f));
	}

IL_03ae:
	{
		GameObjectU5BU5D_t3328599146* L_165 = __this->get_obs_6();
		NullCheck(L_165);
		int32_t L_166 = 1;
		GameObject_t1113636619 * L_167 = (L_165)->GetAt(static_cast<il2cpp_array_size_t>(L_166));
		NullCheck(L_167);
		Transform_t3600365921 * L_168 = GameObject_get_transform_m393750976(L_167, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_169 = __this->get_obs_6();
		NullCheck(L_169);
		int32_t L_170 = 0;
		GameObject_t1113636619 * L_171 = (L_169)->GetAt(static_cast<il2cpp_array_size_t>(L_170));
		NullCheck(L_171);
		Transform_t3600365921 * L_172 = GameObject_get_transform_m393750976(L_171, /*hidden argument*/NULL);
		NullCheck(L_172);
		Vector3_t3722313464  L_173 = Transform_get_position_m102368104(L_172, /*hidden argument*/NULL);
		V_17 = L_173;
		float L_174 = (&V_17)->get_x_1();
		int32_t L_175 = GameScreen_RandomGap_m3185011755(__this, /*hidden argument*/NULL);
		float L_176 = __this->get_bottomY_8();
		float L_177 = __this->get_groundTileH_14();
		float L_178 = __this->get_obsHeightScale_16();
		Vector3_t3722313464  L_179;
		memset(&L_179, 0, sizeof(L_179));
		Vector3__ctor_m1197556204(&L_179, ((float)((float)L_174+(float)(((float)((float)L_175))))), ((float)((float)((float)((float)L_176+(float)((float)((float)L_177/(float)(2.0f)))))+(float)L_178)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_168);
		Transform_set_position_m2361854178(L_168, L_179, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_180 = __this->get_obs_6();
		NullCheck(L_180);
		int32_t L_181 = 1;
		GameObject_t1113636619 * L_182 = (L_180)->GetAt(static_cast<il2cpp_array_size_t>(L_181));
		NullCheck(L_182);
		Collider2D_t2806799626 * L_183 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_182, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		NullCheck(L_183);
		Behaviour_set_enabled_m3107225489(L_183, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_184 = __this->get_obs_6();
		NullCheck(L_184);
		int32_t L_185 = 1;
		GameObject_t1113636619 * L_186 = (L_184)->GetAt(static_cast<il2cpp_array_size_t>(L_185));
		NullCheck(L_186);
		Renderer_t2627027031 * L_187 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_186, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_187);
		Renderer_set_enabled_m2710705614(L_187, (bool)1, /*hidden argument*/NULL);
	}

IL_042d:
	{
		GameObjectU5BU5D_t3328599146* L_188 = __this->get_obs_6();
		NullCheck(L_188);
		int32_t L_189 = 2;
		GameObject_t1113636619 * L_190 = (L_188)->GetAt(static_cast<il2cpp_array_size_t>(L_189));
		NullCheck(L_190);
		Transform_t3600365921 * L_191 = GameObject_get_transform_m393750976(L_190, /*hidden argument*/NULL);
		NullCheck(L_191);
		Vector3_t3722313464  L_192 = Transform_get_position_m102368104(L_191, /*hidden argument*/NULL);
		V_18 = L_192;
		float L_193 = (&V_18)->get_x_1();
		GameObject_t1113636619 * L_194 = __this->get_obsPrefab_4();
		NullCheck(L_194);
		Renderer_t2627027031 * L_195 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_194, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_195);
		Bounds_t2266837910  L_196 = Renderer_get_bounds_m2197226282(L_195, /*hidden argument*/NULL);
		V_19 = L_196;
		Vector3_t3722313464  L_197 = Bounds_get_size_m1171376090((&V_19), /*hidden argument*/NULL);
		V_20 = L_197;
		float L_198 = (&V_20)->get_x_1();
		float L_199 = __this->get_leftX_9();
		if ((!(((float)((float)((float)L_193+(float)L_198))) < ((float)L_199))))
		{
			goto IL_0646;
		}
	}
	{
		int32_t L_200 = Random_Range_m110872341(NULL /*static, unused*/, 1, 6, /*hidden argument*/NULL);
		V_21 = L_200;
		int32_t L_201 = V_21;
		if ((((int32_t)L_201) > ((int32_t)3)))
		{
			goto IL_051c;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_202 = __this->get_obs_6();
		NullCheck(L_202);
		int32_t L_203 = 2;
		GameObject_t1113636619 * L_204 = (L_202)->GetAt(static_cast<il2cpp_array_size_t>(L_203));
		NullCheck(L_204);
		ObsDownCollisionCheck_t2541817184 * L_205 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_204, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		V_22 = L_205;
		ObsDownCollisionCheck_t2541817184 * L_206 = V_22;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_207 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_206, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_207)
		{
			goto IL_050b;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_208 = __this->get_obs_6();
		NullCheck(L_208);
		int32_t L_209 = 2;
		GameObject_t1113636619 * L_210 = (L_208)->GetAt(static_cast<il2cpp_array_size_t>(L_209));
		NullCheck(L_210);
		SpriteRenderer_t3235626157 * L_211 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_210, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_212 = __this->get_obsPrefab_4();
		NullCheck(L_212);
		SpriteRenderer_t3235626157 * L_213 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_212, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_213);
		Sprite_t280657092 * L_214 = SpriteRenderer_get_sprite_m688744963(L_213, /*hidden argument*/NULL);
		NullCheck(L_211);
		SpriteRenderer_set_sprite_m3489358652(L_211, L_214, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_215 = __this->get_obs_6();
		NullCheck(L_215);
		int32_t L_216 = 2;
		GameObject_t1113636619 * L_217 = (L_215)->GetAt(static_cast<il2cpp_array_size_t>(L_216));
		NullCheck(L_217);
		Collider2D_t2806799626 * L_218 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_217, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_23 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_218, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_219 = V_23;
		BoxCollider2D_t3581341831 * L_220 = __this->get_obsCollider_20();
		NullCheck(L_220);
		Vector2_t2156229523  L_221 = BoxCollider2D_get_size_m2899090007(L_220, /*hidden argument*/NULL);
		NullCheck(L_219);
		BoxCollider2D_set_size_m2456142094(L_219, L_221, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_222 = __this->get_obs_6();
		NullCheck(L_222);
		int32_t L_223 = 2;
		GameObject_t1113636619 * L_224 = (L_222)->GetAt(static_cast<il2cpp_array_size_t>(L_223));
		NullCheck(L_224);
		ObsUpCollisionCheck_t2909508583 * L_225 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_224, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_225, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_226 = __this->get_obs_6();
		NullCheck(L_226);
		int32_t L_227 = 2;
		GameObject_t1113636619 * L_228 = (L_226)->GetAt(static_cast<il2cpp_array_size_t>(L_227));
		NullCheck(L_228);
		GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560(L_228, /*hidden argument*/GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560_MethodInfo_var);
	}

IL_050b:
	{
		float L_229 = __this->get_height1_18();
		__this->set_obsHeightScale_16(L_229);
		goto IL_05c7;
	}

IL_051c:
	{
		GameObjectU5BU5D_t3328599146* L_230 = __this->get_obs_6();
		NullCheck(L_230);
		int32_t L_231 = 2;
		GameObject_t1113636619 * L_232 = (L_230)->GetAt(static_cast<il2cpp_array_size_t>(L_231));
		NullCheck(L_232);
		ObsUpCollisionCheck_t2909508583 * L_233 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_232, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		V_24 = L_233;
		ObsUpCollisionCheck_t2909508583 * L_234 = V_24;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_235 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_234, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_235)
		{
			goto IL_05a0;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_236 = __this->get_obs_6();
		NullCheck(L_236);
		int32_t L_237 = 2;
		GameObject_t1113636619 * L_238 = (L_236)->GetAt(static_cast<il2cpp_array_size_t>(L_237));
		NullCheck(L_238);
		SpriteRenderer_t3235626157 * L_239 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_238, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_240 = __this->get_obs2Prefab_5();
		NullCheck(L_240);
		SpriteRenderer_t3235626157 * L_241 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_240, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_241);
		Sprite_t280657092 * L_242 = SpriteRenderer_get_sprite_m688744963(L_241, /*hidden argument*/NULL);
		NullCheck(L_239);
		SpriteRenderer_set_sprite_m3489358652(L_239, L_242, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_243 = __this->get_obs_6();
		NullCheck(L_243);
		int32_t L_244 = 2;
		GameObject_t1113636619 * L_245 = (L_243)->GetAt(static_cast<il2cpp_array_size_t>(L_244));
		NullCheck(L_245);
		Collider2D_t2806799626 * L_246 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_245, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_25 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_246, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_247 = V_25;
		BoxCollider2D_t3581341831 * L_248 = __this->get_obs2Collider_21();
		NullCheck(L_248);
		Vector2_t2156229523  L_249 = BoxCollider2D_get_size_m2899090007(L_248, /*hidden argument*/NULL);
		NullCheck(L_247);
		BoxCollider2D_set_size_m2456142094(L_247, L_249, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_250 = __this->get_obs_6();
		NullCheck(L_250);
		int32_t L_251 = 2;
		GameObject_t1113636619 * L_252 = (L_250)->GetAt(static_cast<il2cpp_array_size_t>(L_251));
		NullCheck(L_252);
		ObsDownCollisionCheck_t2541817184 * L_253 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_252, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_253, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_254 = __this->get_obs_6();
		NullCheck(L_254);
		int32_t L_255 = 2;
		GameObject_t1113636619 * L_256 = (L_254)->GetAt(static_cast<il2cpp_array_size_t>(L_255));
		NullCheck(L_256);
		GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873(L_256, /*hidden argument*/GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873_MethodInfo_var);
	}

IL_05a0:
	{
		float L_257 = __this->get_height2_19();
		__this->set_obsHeightScale_16(L_257);
		float L_258 = __this->get_obsHeightScale_16();
		if ((!(((float)L_258) < ((float)(0.0f)))))
		{
			goto IL_05c7;
		}
	}
	{
		__this->set_obsHeightScale_16((1.11600018f));
	}

IL_05c7:
	{
		GameObjectU5BU5D_t3328599146* L_259 = __this->get_obs_6();
		NullCheck(L_259);
		int32_t L_260 = 2;
		GameObject_t1113636619 * L_261 = (L_259)->GetAt(static_cast<il2cpp_array_size_t>(L_260));
		NullCheck(L_261);
		Transform_t3600365921 * L_262 = GameObject_get_transform_m393750976(L_261, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_263 = __this->get_obs_6();
		NullCheck(L_263);
		int32_t L_264 = 1;
		GameObject_t1113636619 * L_265 = (L_263)->GetAt(static_cast<il2cpp_array_size_t>(L_264));
		NullCheck(L_265);
		Transform_t3600365921 * L_266 = GameObject_get_transform_m393750976(L_265, /*hidden argument*/NULL);
		NullCheck(L_266);
		Vector3_t3722313464  L_267 = Transform_get_position_m102368104(L_266, /*hidden argument*/NULL);
		V_26 = L_267;
		float L_268 = (&V_26)->get_x_1();
		int32_t L_269 = GameScreen_RandomGap_m3185011755(__this, /*hidden argument*/NULL);
		float L_270 = __this->get_bottomY_8();
		float L_271 = __this->get_groundTileH_14();
		float L_272 = __this->get_obsHeightScale_16();
		Vector3_t3722313464  L_273;
		memset(&L_273, 0, sizeof(L_273));
		Vector3__ctor_m1197556204(&L_273, ((float)((float)L_268+(float)(((float)((float)L_269))))), ((float)((float)((float)((float)L_270+(float)((float)((float)L_271/(float)(2.0f)))))+(float)L_272)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_262);
		Transform_set_position_m2361854178(L_262, L_273, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_274 = __this->get_obs_6();
		NullCheck(L_274);
		int32_t L_275 = 2;
		GameObject_t1113636619 * L_276 = (L_274)->GetAt(static_cast<il2cpp_array_size_t>(L_275));
		NullCheck(L_276);
		Collider2D_t2806799626 * L_277 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_276, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		NullCheck(L_277);
		Behaviour_set_enabled_m3107225489(L_277, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_278 = __this->get_obs_6();
		NullCheck(L_278);
		int32_t L_279 = 2;
		GameObject_t1113636619 * L_280 = (L_278)->GetAt(static_cast<il2cpp_array_size_t>(L_279));
		NullCheck(L_280);
		Renderer_t2627027031 * L_281 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_280, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_281);
		Renderer_set_enabled_m2710705614(L_281, (bool)1, /*hidden argument*/NULL);
	}

IL_0646:
	{
		GameObjectU5BU5D_t3328599146* L_282 = __this->get_obs_6();
		NullCheck(L_282);
		int32_t L_283 = 3;
		GameObject_t1113636619 * L_284 = (L_282)->GetAt(static_cast<il2cpp_array_size_t>(L_283));
		NullCheck(L_284);
		Transform_t3600365921 * L_285 = GameObject_get_transform_m393750976(L_284, /*hidden argument*/NULL);
		NullCheck(L_285);
		Vector3_t3722313464  L_286 = Transform_get_position_m102368104(L_285, /*hidden argument*/NULL);
		V_27 = L_286;
		float L_287 = (&V_27)->get_x_1();
		GameObject_t1113636619 * L_288 = __this->get_obsPrefab_4();
		NullCheck(L_288);
		Renderer_t2627027031 * L_289 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_288, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_289);
		Bounds_t2266837910  L_290 = Renderer_get_bounds_m2197226282(L_289, /*hidden argument*/NULL);
		V_28 = L_290;
		Vector3_t3722313464  L_291 = Bounds_get_size_m1171376090((&V_28), /*hidden argument*/NULL);
		V_29 = L_291;
		float L_292 = (&V_29)->get_x_1();
		float L_293 = __this->get_leftX_9();
		if ((!(((float)((float)((float)L_287+(float)L_292))) < ((float)L_293))))
		{
			goto IL_085f;
		}
	}
	{
		int32_t L_294 = Random_Range_m110872341(NULL /*static, unused*/, 1, 6, /*hidden argument*/NULL);
		V_30 = L_294;
		int32_t L_295 = V_30;
		if ((((int32_t)L_295) > ((int32_t)3)))
		{
			goto IL_0735;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_296 = __this->get_obs_6();
		NullCheck(L_296);
		int32_t L_297 = 3;
		GameObject_t1113636619 * L_298 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_297));
		NullCheck(L_298);
		ObsDownCollisionCheck_t2541817184 * L_299 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_298, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		V_31 = L_299;
		ObsDownCollisionCheck_t2541817184 * L_300 = V_31;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_301 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_300, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_301)
		{
			goto IL_0724;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_302 = __this->get_obs_6();
		NullCheck(L_302);
		int32_t L_303 = 3;
		GameObject_t1113636619 * L_304 = (L_302)->GetAt(static_cast<il2cpp_array_size_t>(L_303));
		NullCheck(L_304);
		SpriteRenderer_t3235626157 * L_305 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_304, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_306 = __this->get_obsPrefab_4();
		NullCheck(L_306);
		SpriteRenderer_t3235626157 * L_307 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_306, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_307);
		Sprite_t280657092 * L_308 = SpriteRenderer_get_sprite_m688744963(L_307, /*hidden argument*/NULL);
		NullCheck(L_305);
		SpriteRenderer_set_sprite_m3489358652(L_305, L_308, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_309 = __this->get_obs_6();
		NullCheck(L_309);
		int32_t L_310 = 3;
		GameObject_t1113636619 * L_311 = (L_309)->GetAt(static_cast<il2cpp_array_size_t>(L_310));
		NullCheck(L_311);
		Collider2D_t2806799626 * L_312 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_311, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_32 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_312, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_313 = V_32;
		BoxCollider2D_t3581341831 * L_314 = __this->get_obsCollider_20();
		NullCheck(L_314);
		Vector2_t2156229523  L_315 = BoxCollider2D_get_size_m2899090007(L_314, /*hidden argument*/NULL);
		NullCheck(L_313);
		BoxCollider2D_set_size_m2456142094(L_313, L_315, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_316 = __this->get_obs_6();
		NullCheck(L_316);
		int32_t L_317 = 3;
		GameObject_t1113636619 * L_318 = (L_316)->GetAt(static_cast<il2cpp_array_size_t>(L_317));
		NullCheck(L_318);
		ObsUpCollisionCheck_t2909508583 * L_319 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_318, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_319, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_320 = __this->get_obs_6();
		NullCheck(L_320);
		int32_t L_321 = 3;
		GameObject_t1113636619 * L_322 = (L_320)->GetAt(static_cast<il2cpp_array_size_t>(L_321));
		NullCheck(L_322);
		GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560(L_322, /*hidden argument*/GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560_MethodInfo_var);
	}

IL_0724:
	{
		float L_323 = __this->get_height1_18();
		__this->set_obsHeightScale_16(L_323);
		goto IL_07e0;
	}

IL_0735:
	{
		GameObjectU5BU5D_t3328599146* L_324 = __this->get_obs_6();
		NullCheck(L_324);
		int32_t L_325 = 3;
		GameObject_t1113636619 * L_326 = (L_324)->GetAt(static_cast<il2cpp_array_size_t>(L_325));
		NullCheck(L_326);
		ObsUpCollisionCheck_t2909508583 * L_327 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_326, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		V_33 = L_327;
		ObsUpCollisionCheck_t2909508583 * L_328 = V_33;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_329 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_328, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_329)
		{
			goto IL_07b9;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_330 = __this->get_obs_6();
		NullCheck(L_330);
		int32_t L_331 = 3;
		GameObject_t1113636619 * L_332 = (L_330)->GetAt(static_cast<il2cpp_array_size_t>(L_331));
		NullCheck(L_332);
		SpriteRenderer_t3235626157 * L_333 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_332, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_334 = __this->get_obs2Prefab_5();
		NullCheck(L_334);
		SpriteRenderer_t3235626157 * L_335 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_334, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_335);
		Sprite_t280657092 * L_336 = SpriteRenderer_get_sprite_m688744963(L_335, /*hidden argument*/NULL);
		NullCheck(L_333);
		SpriteRenderer_set_sprite_m3489358652(L_333, L_336, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_337 = __this->get_obs_6();
		NullCheck(L_337);
		int32_t L_338 = 3;
		GameObject_t1113636619 * L_339 = (L_337)->GetAt(static_cast<il2cpp_array_size_t>(L_338));
		NullCheck(L_339);
		Collider2D_t2806799626 * L_340 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_339, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_34 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_340, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_341 = V_34;
		BoxCollider2D_t3581341831 * L_342 = __this->get_obs2Collider_21();
		NullCheck(L_342);
		Vector2_t2156229523  L_343 = BoxCollider2D_get_size_m2899090007(L_342, /*hidden argument*/NULL);
		NullCheck(L_341);
		BoxCollider2D_set_size_m2456142094(L_341, L_343, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_344 = __this->get_obs_6();
		NullCheck(L_344);
		int32_t L_345 = 3;
		GameObject_t1113636619 * L_346 = (L_344)->GetAt(static_cast<il2cpp_array_size_t>(L_345));
		NullCheck(L_346);
		ObsDownCollisionCheck_t2541817184 * L_347 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_346, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_347, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_348 = __this->get_obs_6();
		NullCheck(L_348);
		int32_t L_349 = 3;
		GameObject_t1113636619 * L_350 = (L_348)->GetAt(static_cast<il2cpp_array_size_t>(L_349));
		NullCheck(L_350);
		GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873(L_350, /*hidden argument*/GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873_MethodInfo_var);
	}

IL_07b9:
	{
		float L_351 = __this->get_height2_19();
		__this->set_obsHeightScale_16(L_351);
		float L_352 = __this->get_obsHeightScale_16();
		if ((!(((float)L_352) < ((float)(0.0f)))))
		{
			goto IL_07e0;
		}
	}
	{
		__this->set_obsHeightScale_16((1.11600018f));
	}

IL_07e0:
	{
		GameObjectU5BU5D_t3328599146* L_353 = __this->get_obs_6();
		NullCheck(L_353);
		int32_t L_354 = 3;
		GameObject_t1113636619 * L_355 = (L_353)->GetAt(static_cast<il2cpp_array_size_t>(L_354));
		NullCheck(L_355);
		Transform_t3600365921 * L_356 = GameObject_get_transform_m393750976(L_355, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_357 = __this->get_obs_6();
		NullCheck(L_357);
		int32_t L_358 = 2;
		GameObject_t1113636619 * L_359 = (L_357)->GetAt(static_cast<il2cpp_array_size_t>(L_358));
		NullCheck(L_359);
		Transform_t3600365921 * L_360 = GameObject_get_transform_m393750976(L_359, /*hidden argument*/NULL);
		NullCheck(L_360);
		Vector3_t3722313464  L_361 = Transform_get_position_m102368104(L_360, /*hidden argument*/NULL);
		V_35 = L_361;
		float L_362 = (&V_35)->get_x_1();
		int32_t L_363 = GameScreen_RandomGap_m3185011755(__this, /*hidden argument*/NULL);
		float L_364 = __this->get_bottomY_8();
		float L_365 = __this->get_groundTileH_14();
		float L_366 = __this->get_obsHeightScale_16();
		Vector3_t3722313464  L_367;
		memset(&L_367, 0, sizeof(L_367));
		Vector3__ctor_m1197556204(&L_367, ((float)((float)L_362+(float)(((float)((float)L_363))))), ((float)((float)((float)((float)L_364+(float)((float)((float)L_365/(float)(2.0f)))))+(float)L_366)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_356);
		Transform_set_position_m2361854178(L_356, L_367, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_368 = __this->get_obs_6();
		NullCheck(L_368);
		int32_t L_369 = 3;
		GameObject_t1113636619 * L_370 = (L_368)->GetAt(static_cast<il2cpp_array_size_t>(L_369));
		NullCheck(L_370);
		Collider2D_t2806799626 * L_371 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_370, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		NullCheck(L_371);
		Behaviour_set_enabled_m3107225489(L_371, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_372 = __this->get_obs_6();
		NullCheck(L_372);
		int32_t L_373 = 3;
		GameObject_t1113636619 * L_374 = (L_372)->GetAt(static_cast<il2cpp_array_size_t>(L_373));
		NullCheck(L_374);
		Renderer_t2627027031 * L_375 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_374, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_375);
		Renderer_set_enabled_m2710705614(L_375, (bool)1, /*hidden argument*/NULL);
	}

IL_085f:
	{
		GameObjectU5BU5D_t3328599146* L_376 = __this->get_obs_6();
		NullCheck(L_376);
		int32_t L_377 = 4;
		GameObject_t1113636619 * L_378 = (L_376)->GetAt(static_cast<il2cpp_array_size_t>(L_377));
		NullCheck(L_378);
		Transform_t3600365921 * L_379 = GameObject_get_transform_m393750976(L_378, /*hidden argument*/NULL);
		NullCheck(L_379);
		Vector3_t3722313464  L_380 = Transform_get_position_m102368104(L_379, /*hidden argument*/NULL);
		V_36 = L_380;
		float L_381 = (&V_36)->get_x_1();
		GameObject_t1113636619 * L_382 = __this->get_obsPrefab_4();
		NullCheck(L_382);
		Renderer_t2627027031 * L_383 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_382, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_383);
		Bounds_t2266837910  L_384 = Renderer_get_bounds_m2197226282(L_383, /*hidden argument*/NULL);
		V_37 = L_384;
		Vector3_t3722313464  L_385 = Bounds_get_size_m1171376090((&V_37), /*hidden argument*/NULL);
		V_38 = L_385;
		float L_386 = (&V_38)->get_x_1();
		float L_387 = __this->get_leftX_9();
		if ((!(((float)((float)((float)L_381+(float)L_386))) < ((float)L_387))))
		{
			goto IL_0a78;
		}
	}
	{
		int32_t L_388 = Random_Range_m110872341(NULL /*static, unused*/, 1, 6, /*hidden argument*/NULL);
		V_39 = L_388;
		int32_t L_389 = V_39;
		if ((((int32_t)L_389) > ((int32_t)3)))
		{
			goto IL_094e;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_390 = __this->get_obs_6();
		NullCheck(L_390);
		int32_t L_391 = 4;
		GameObject_t1113636619 * L_392 = (L_390)->GetAt(static_cast<il2cpp_array_size_t>(L_391));
		NullCheck(L_392);
		ObsDownCollisionCheck_t2541817184 * L_393 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_392, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		V_40 = L_393;
		ObsDownCollisionCheck_t2541817184 * L_394 = V_40;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_395 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_394, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_395)
		{
			goto IL_093d;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_396 = __this->get_obs_6();
		NullCheck(L_396);
		int32_t L_397 = 4;
		GameObject_t1113636619 * L_398 = (L_396)->GetAt(static_cast<il2cpp_array_size_t>(L_397));
		NullCheck(L_398);
		SpriteRenderer_t3235626157 * L_399 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_398, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_400 = __this->get_obsPrefab_4();
		NullCheck(L_400);
		SpriteRenderer_t3235626157 * L_401 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_400, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_401);
		Sprite_t280657092 * L_402 = SpriteRenderer_get_sprite_m688744963(L_401, /*hidden argument*/NULL);
		NullCheck(L_399);
		SpriteRenderer_set_sprite_m3489358652(L_399, L_402, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_403 = __this->get_obs_6();
		NullCheck(L_403);
		int32_t L_404 = 4;
		GameObject_t1113636619 * L_405 = (L_403)->GetAt(static_cast<il2cpp_array_size_t>(L_404));
		NullCheck(L_405);
		Collider2D_t2806799626 * L_406 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_405, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_41 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_406, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_407 = V_41;
		BoxCollider2D_t3581341831 * L_408 = __this->get_obsCollider_20();
		NullCheck(L_408);
		Vector2_t2156229523  L_409 = BoxCollider2D_get_size_m2899090007(L_408, /*hidden argument*/NULL);
		NullCheck(L_407);
		BoxCollider2D_set_size_m2456142094(L_407, L_409, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_410 = __this->get_obs_6();
		NullCheck(L_410);
		int32_t L_411 = 4;
		GameObject_t1113636619 * L_412 = (L_410)->GetAt(static_cast<il2cpp_array_size_t>(L_411));
		NullCheck(L_412);
		ObsUpCollisionCheck_t2909508583 * L_413 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_412, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_413, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_414 = __this->get_obs_6();
		NullCheck(L_414);
		int32_t L_415 = 4;
		GameObject_t1113636619 * L_416 = (L_414)->GetAt(static_cast<il2cpp_array_size_t>(L_415));
		NullCheck(L_416);
		GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560(L_416, /*hidden argument*/GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560_MethodInfo_var);
	}

IL_093d:
	{
		float L_417 = __this->get_height1_18();
		__this->set_obsHeightScale_16(L_417);
		goto IL_09f9;
	}

IL_094e:
	{
		GameObjectU5BU5D_t3328599146* L_418 = __this->get_obs_6();
		NullCheck(L_418);
		int32_t L_419 = 4;
		GameObject_t1113636619 * L_420 = (L_418)->GetAt(static_cast<il2cpp_array_size_t>(L_419));
		NullCheck(L_420);
		ObsUpCollisionCheck_t2909508583 * L_421 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_420, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		V_42 = L_421;
		ObsUpCollisionCheck_t2909508583 * L_422 = V_42;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_423 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_422, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_423)
		{
			goto IL_09d2;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_424 = __this->get_obs_6();
		NullCheck(L_424);
		int32_t L_425 = 4;
		GameObject_t1113636619 * L_426 = (L_424)->GetAt(static_cast<il2cpp_array_size_t>(L_425));
		NullCheck(L_426);
		SpriteRenderer_t3235626157 * L_427 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_426, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_428 = __this->get_obs2Prefab_5();
		NullCheck(L_428);
		SpriteRenderer_t3235626157 * L_429 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_428, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_429);
		Sprite_t280657092 * L_430 = SpriteRenderer_get_sprite_m688744963(L_429, /*hidden argument*/NULL);
		NullCheck(L_427);
		SpriteRenderer_set_sprite_m3489358652(L_427, L_430, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_431 = __this->get_obs_6();
		NullCheck(L_431);
		int32_t L_432 = 4;
		GameObject_t1113636619 * L_433 = (L_431)->GetAt(static_cast<il2cpp_array_size_t>(L_432));
		NullCheck(L_433);
		Collider2D_t2806799626 * L_434 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_433, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_43 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_434, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_435 = V_43;
		BoxCollider2D_t3581341831 * L_436 = __this->get_obs2Collider_21();
		NullCheck(L_436);
		Vector2_t2156229523  L_437 = BoxCollider2D_get_size_m2899090007(L_436, /*hidden argument*/NULL);
		NullCheck(L_435);
		BoxCollider2D_set_size_m2456142094(L_435, L_437, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_438 = __this->get_obs_6();
		NullCheck(L_438);
		int32_t L_439 = 4;
		GameObject_t1113636619 * L_440 = (L_438)->GetAt(static_cast<il2cpp_array_size_t>(L_439));
		NullCheck(L_440);
		ObsDownCollisionCheck_t2541817184 * L_441 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_440, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_441, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_442 = __this->get_obs_6();
		NullCheck(L_442);
		int32_t L_443 = 4;
		GameObject_t1113636619 * L_444 = (L_442)->GetAt(static_cast<il2cpp_array_size_t>(L_443));
		NullCheck(L_444);
		GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873(L_444, /*hidden argument*/GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873_MethodInfo_var);
	}

IL_09d2:
	{
		float L_445 = __this->get_height2_19();
		__this->set_obsHeightScale_16(L_445);
		float L_446 = __this->get_obsHeightScale_16();
		if ((!(((float)L_446) < ((float)(0.0f)))))
		{
			goto IL_09f9;
		}
	}
	{
		__this->set_obsHeightScale_16((1.11600018f));
	}

IL_09f9:
	{
		GameObjectU5BU5D_t3328599146* L_447 = __this->get_obs_6();
		NullCheck(L_447);
		int32_t L_448 = 4;
		GameObject_t1113636619 * L_449 = (L_447)->GetAt(static_cast<il2cpp_array_size_t>(L_448));
		NullCheck(L_449);
		Transform_t3600365921 * L_450 = GameObject_get_transform_m393750976(L_449, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_451 = __this->get_obs_6();
		NullCheck(L_451);
		int32_t L_452 = 3;
		GameObject_t1113636619 * L_453 = (L_451)->GetAt(static_cast<il2cpp_array_size_t>(L_452));
		NullCheck(L_453);
		Transform_t3600365921 * L_454 = GameObject_get_transform_m393750976(L_453, /*hidden argument*/NULL);
		NullCheck(L_454);
		Vector3_t3722313464  L_455 = Transform_get_position_m102368104(L_454, /*hidden argument*/NULL);
		V_44 = L_455;
		float L_456 = (&V_44)->get_x_1();
		int32_t L_457 = GameScreen_RandomGap_m3185011755(__this, /*hidden argument*/NULL);
		float L_458 = __this->get_bottomY_8();
		float L_459 = __this->get_groundTileH_14();
		float L_460 = __this->get_obsHeightScale_16();
		Vector3_t3722313464  L_461;
		memset(&L_461, 0, sizeof(L_461));
		Vector3__ctor_m1197556204(&L_461, ((float)((float)L_456+(float)(((float)((float)L_457))))), ((float)((float)((float)((float)L_458+(float)((float)((float)L_459/(float)(2.0f)))))+(float)L_460)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_450);
		Transform_set_position_m2361854178(L_450, L_461, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_462 = __this->get_obs_6();
		NullCheck(L_462);
		int32_t L_463 = 4;
		GameObject_t1113636619 * L_464 = (L_462)->GetAt(static_cast<il2cpp_array_size_t>(L_463));
		NullCheck(L_464);
		Collider2D_t2806799626 * L_465 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_464, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		NullCheck(L_465);
		Behaviour_set_enabled_m3107225489(L_465, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_466 = __this->get_obs_6();
		NullCheck(L_466);
		int32_t L_467 = 4;
		GameObject_t1113636619 * L_468 = (L_466)->GetAt(static_cast<il2cpp_array_size_t>(L_467));
		NullCheck(L_468);
		Renderer_t2627027031 * L_469 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_468, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_469);
		Renderer_set_enabled_m2710705614(L_469, (bool)1, /*hidden argument*/NULL);
	}

IL_0a78:
	{
		GameObjectU5BU5D_t3328599146* L_470 = __this->get_obs_6();
		NullCheck(L_470);
		int32_t L_471 = 5;
		GameObject_t1113636619 * L_472 = (L_470)->GetAt(static_cast<il2cpp_array_size_t>(L_471));
		NullCheck(L_472);
		Transform_t3600365921 * L_473 = GameObject_get_transform_m393750976(L_472, /*hidden argument*/NULL);
		NullCheck(L_473);
		Vector3_t3722313464  L_474 = Transform_get_position_m102368104(L_473, /*hidden argument*/NULL);
		V_45 = L_474;
		float L_475 = (&V_45)->get_x_1();
		GameObject_t1113636619 * L_476 = __this->get_obsPrefab_4();
		NullCheck(L_476);
		Renderer_t2627027031 * L_477 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_476, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_477);
		Bounds_t2266837910  L_478 = Renderer_get_bounds_m2197226282(L_477, /*hidden argument*/NULL);
		V_46 = L_478;
		Vector3_t3722313464  L_479 = Bounds_get_size_m1171376090((&V_46), /*hidden argument*/NULL);
		V_47 = L_479;
		float L_480 = (&V_47)->get_x_1();
		float L_481 = __this->get_leftX_9();
		if ((!(((float)((float)((float)L_475+(float)L_480))) < ((float)L_481))))
		{
			goto IL_0c91;
		}
	}
	{
		int32_t L_482 = Random_Range_m110872341(NULL /*static, unused*/, 1, 6, /*hidden argument*/NULL);
		V_48 = L_482;
		int32_t L_483 = V_48;
		if ((((int32_t)L_483) > ((int32_t)3)))
		{
			goto IL_0b67;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_484 = __this->get_obs_6();
		NullCheck(L_484);
		int32_t L_485 = 5;
		GameObject_t1113636619 * L_486 = (L_484)->GetAt(static_cast<il2cpp_array_size_t>(L_485));
		NullCheck(L_486);
		ObsDownCollisionCheck_t2541817184 * L_487 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_486, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		V_49 = L_487;
		ObsDownCollisionCheck_t2541817184 * L_488 = V_49;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_489 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_488, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_489)
		{
			goto IL_0b56;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_490 = __this->get_obs_6();
		NullCheck(L_490);
		int32_t L_491 = 5;
		GameObject_t1113636619 * L_492 = (L_490)->GetAt(static_cast<il2cpp_array_size_t>(L_491));
		NullCheck(L_492);
		SpriteRenderer_t3235626157 * L_493 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_492, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_494 = __this->get_obsPrefab_4();
		NullCheck(L_494);
		SpriteRenderer_t3235626157 * L_495 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_494, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_495);
		Sprite_t280657092 * L_496 = SpriteRenderer_get_sprite_m688744963(L_495, /*hidden argument*/NULL);
		NullCheck(L_493);
		SpriteRenderer_set_sprite_m3489358652(L_493, L_496, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_497 = __this->get_obs_6();
		NullCheck(L_497);
		int32_t L_498 = 5;
		GameObject_t1113636619 * L_499 = (L_497)->GetAt(static_cast<il2cpp_array_size_t>(L_498));
		NullCheck(L_499);
		Collider2D_t2806799626 * L_500 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_499, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_50 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_500, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_501 = V_50;
		BoxCollider2D_t3581341831 * L_502 = __this->get_obsCollider_20();
		NullCheck(L_502);
		Vector2_t2156229523  L_503 = BoxCollider2D_get_size_m2899090007(L_502, /*hidden argument*/NULL);
		NullCheck(L_501);
		BoxCollider2D_set_size_m2456142094(L_501, L_503, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_504 = __this->get_obs_6();
		NullCheck(L_504);
		int32_t L_505 = 5;
		GameObject_t1113636619 * L_506 = (L_504)->GetAt(static_cast<il2cpp_array_size_t>(L_505));
		NullCheck(L_506);
		ObsUpCollisionCheck_t2909508583 * L_507 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_506, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_507, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_508 = __this->get_obs_6();
		NullCheck(L_508);
		int32_t L_509 = 5;
		GameObject_t1113636619 * L_510 = (L_508)->GetAt(static_cast<il2cpp_array_size_t>(L_509));
		NullCheck(L_510);
		GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560(L_510, /*hidden argument*/GameObject_AddComponent_TisObsDownCollisionCheck_t2541817184_m3055128560_MethodInfo_var);
	}

IL_0b56:
	{
		float L_511 = __this->get_height1_18();
		__this->set_obsHeightScale_16(L_511);
		goto IL_0c12;
	}

IL_0b67:
	{
		GameObjectU5BU5D_t3328599146* L_512 = __this->get_obs_6();
		NullCheck(L_512);
		int32_t L_513 = 5;
		GameObject_t1113636619 * L_514 = (L_512)->GetAt(static_cast<il2cpp_array_size_t>(L_513));
		NullCheck(L_514);
		ObsUpCollisionCheck_t2909508583 * L_515 = GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691(L_514, /*hidden argument*/GameObject_GetComponent_TisObsUpCollisionCheck_t2909508583_m1466140691_MethodInfo_var);
		V_51 = L_515;
		ObsUpCollisionCheck_t2909508583 * L_516 = V_51;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_517 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_516, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_517)
		{
			goto IL_0beb;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_518 = __this->get_obs_6();
		NullCheck(L_518);
		int32_t L_519 = 5;
		GameObject_t1113636619 * L_520 = (L_518)->GetAt(static_cast<il2cpp_array_size_t>(L_519));
		NullCheck(L_520);
		SpriteRenderer_t3235626157 * L_521 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_520, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		GameObject_t1113636619 * L_522 = __this->get_obs2Prefab_5();
		NullCheck(L_522);
		SpriteRenderer_t3235626157 * L_523 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_522, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_MethodInfo_var);
		NullCheck(L_523);
		Sprite_t280657092 * L_524 = SpriteRenderer_get_sprite_m688744963(L_523, /*hidden argument*/NULL);
		NullCheck(L_521);
		SpriteRenderer_set_sprite_m3489358652(L_521, L_524, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_525 = __this->get_obs_6();
		NullCheck(L_525);
		int32_t L_526 = 5;
		GameObject_t1113636619 * L_527 = (L_525)->GetAt(static_cast<il2cpp_array_size_t>(L_526));
		NullCheck(L_527);
		Collider2D_t2806799626 * L_528 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_527, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_52 = ((BoxCollider2D_t3581341831 *)IsInstSealed(L_528, BoxCollider2D_t3581341831_il2cpp_TypeInfo_var));
		BoxCollider2D_t3581341831 * L_529 = V_52;
		BoxCollider2D_t3581341831 * L_530 = __this->get_obs2Collider_21();
		NullCheck(L_530);
		Vector2_t2156229523  L_531 = BoxCollider2D_get_size_m2899090007(L_530, /*hidden argument*/NULL);
		NullCheck(L_529);
		BoxCollider2D_set_size_m2456142094(L_529, L_531, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_532 = __this->get_obs_6();
		NullCheck(L_532);
		int32_t L_533 = 5;
		GameObject_t1113636619 * L_534 = (L_532)->GetAt(static_cast<il2cpp_array_size_t>(L_533));
		NullCheck(L_534);
		ObsDownCollisionCheck_t2541817184 * L_535 = GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091(L_534, /*hidden argument*/GameObject_GetComponent_TisObsDownCollisionCheck_t2541817184_m1543811091_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_535, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_536 = __this->get_obs_6();
		NullCheck(L_536);
		int32_t L_537 = 5;
		GameObject_t1113636619 * L_538 = (L_536)->GetAt(static_cast<il2cpp_array_size_t>(L_537));
		NullCheck(L_538);
		GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873(L_538, /*hidden argument*/GameObject_AddComponent_TisObsUpCollisionCheck_t2909508583_m2794664873_MethodInfo_var);
	}

IL_0beb:
	{
		float L_539 = __this->get_height2_19();
		__this->set_obsHeightScale_16(L_539);
		float L_540 = __this->get_obsHeightScale_16();
		if ((!(((float)L_540) < ((float)(0.0f)))))
		{
			goto IL_0c12;
		}
	}
	{
		__this->set_obsHeightScale_16((1.11600018f));
	}

IL_0c12:
	{
		GameObjectU5BU5D_t3328599146* L_541 = __this->get_obs_6();
		NullCheck(L_541);
		int32_t L_542 = 5;
		GameObject_t1113636619 * L_543 = (L_541)->GetAt(static_cast<il2cpp_array_size_t>(L_542));
		NullCheck(L_543);
		Transform_t3600365921 * L_544 = GameObject_get_transform_m393750976(L_543, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_545 = __this->get_obs_6();
		NullCheck(L_545);
		int32_t L_546 = 4;
		GameObject_t1113636619 * L_547 = (L_545)->GetAt(static_cast<il2cpp_array_size_t>(L_546));
		NullCheck(L_547);
		Transform_t3600365921 * L_548 = GameObject_get_transform_m393750976(L_547, /*hidden argument*/NULL);
		NullCheck(L_548);
		Vector3_t3722313464  L_549 = Transform_get_position_m102368104(L_548, /*hidden argument*/NULL);
		V_53 = L_549;
		float L_550 = (&V_53)->get_x_1();
		int32_t L_551 = GameScreen_RandomGap_m3185011755(__this, /*hidden argument*/NULL);
		float L_552 = __this->get_bottomY_8();
		float L_553 = __this->get_groundTileH_14();
		float L_554 = __this->get_obsHeightScale_16();
		Vector3_t3722313464  L_555;
		memset(&L_555, 0, sizeof(L_555));
		Vector3__ctor_m1197556204(&L_555, ((float)((float)L_550+(float)(((float)((float)L_551))))), ((float)((float)((float)((float)L_552+(float)((float)((float)L_553/(float)(2.0f)))))+(float)L_554)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_544);
		Transform_set_position_m2361854178(L_544, L_555, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_556 = __this->get_obs_6();
		NullCheck(L_556);
		int32_t L_557 = 5;
		GameObject_t1113636619 * L_558 = (L_556)->GetAt(static_cast<il2cpp_array_size_t>(L_557));
		NullCheck(L_558);
		Collider2D_t2806799626 * L_559 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_558, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		NullCheck(L_559);
		Behaviour_set_enabled_m3107225489(L_559, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_560 = __this->get_obs_6();
		NullCheck(L_560);
		int32_t L_561 = 5;
		GameObject_t1113636619 * L_562 = (L_560)->GetAt(static_cast<il2cpp_array_size_t>(L_561));
		NullCheck(L_562);
		Renderer_t2627027031 * L_563 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_562, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_563);
		Renderer_set_enabled_m2710705614(L_563, (bool)1, /*hidden argument*/NULL);
	}

IL_0c91:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_564 = Input_GetKeyDown_m2296100099(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_564)
		{
			goto IL_0d80;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameScreen_t1800529819_il2cpp_TypeInfo_var);
		bool L_565 = ((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->get_paused_15();
		if (L_565)
		{
			goto IL_0d0c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		bool L_566 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_isDead_7();
		if (L_566)
		{
			goto IL_0d0c;
		}
	}
	{
		Time_set_timeScale_m148135804(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameScreen_t1800529819_il2cpp_TypeInfo_var);
		((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->set_paused_15((bool)1);
		Renderer_t2627027031 * L_567 = __this->get_pauseBg_28();
		NullCheck(L_567);
		Renderer_set_enabled_m2710705614(L_567, (bool)1, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_568 = __this->get_btnResume_29();
		NullCheck(L_568);
		Renderer_set_enabled_m2710705614(L_568, (bool)1, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_569 = __this->get_btnHome_30();
		NullCheck(L_569);
		Renderer_set_enabled_m2710705614(L_569, (bool)1, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_570 = __this->get_btnReplay_31();
		NullCheck(L_570);
		Renderer_set_enabled_m2710705614(L_570, (bool)1, /*hidden argument*/NULL);
		GUIText_t402233326 * L_571 = __this->get_pauseText_32();
		NullCheck(L_571);
		Behaviour_set_enabled_m3107225489(L_571, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		AudioSource_t3935305588 * L_572 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_musicSource_18();
		NullCheck(L_572);
		AudioSource_Stop_m534887514(L_572, /*hidden argument*/NULL);
		goto IL_0d80;
	}

IL_0d0c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameScreen_t1800529819_il2cpp_TypeInfo_var);
		bool L_573 = ((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->get_paused_15();
		if (!L_573)
		{
			goto IL_0d80;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		bool L_574 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_isDead_7();
		if (L_574)
		{
			goto IL_0d80;
		}
	}
	{
		Time_set_timeScale_m148135804(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameScreen_t1800529819_il2cpp_TypeInfo_var);
		((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->set_paused_15((bool)0);
		Renderer_t2627027031 * L_575 = __this->get_pauseBg_28();
		NullCheck(L_575);
		Renderer_set_enabled_m2710705614(L_575, (bool)0, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_576 = __this->get_btnResume_29();
		NullCheck(L_576);
		Renderer_set_enabled_m2710705614(L_576, (bool)0, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_577 = __this->get_btnHome_30();
		NullCheck(L_577);
		Renderer_set_enabled_m2710705614(L_577, (bool)0, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_578 = __this->get_btnReplay_31();
		NullCheck(L_578);
		Renderer_set_enabled_m2710705614(L_578, (bool)0, /*hidden argument*/NULL);
		GUIText_t402233326 * L_579 = __this->get_pauseText_32();
		NullCheck(L_579);
		Behaviour_set_enabled_m3107225489(L_579, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MenuScreen_t742580752_il2cpp_TypeInfo_var);
		bool L_580 = ((MenuScreen_t742580752_StaticFields*)MenuScreen_t742580752_il2cpp_TypeInfo_var->static_fields)->get_isSoundOn_8();
		if (!L_580)
		{
			goto IL_0d80;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		AudioSource_t3935305588 * L_581 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_musicSource_18();
		NullCheck(L_581);
		AudioSource_Play_m2606248505(L_581, /*hidden argument*/NULL);
	}

IL_0d80:
	{
		return;
	}
}
// System.Void GameScreen::gameOverScene()
extern "C"  void GameScreen_gameOverScene_m1998749941 (GameScreen_t1800529819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen_gameOverScene_m1998749941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Time_set_timeScale_m148135804(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		AudioSource_t3935305588 * L_0 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_musicSource_18();
		NullCheck(L_0);
		AudioSource_Stop_m534887514(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MenuScreen_t742580752_il2cpp_TypeInfo_var);
		bool L_1 = ((MenuScreen_t742580752_StaticFields*)MenuScreen_t742580752_il2cpp_TypeInfo_var->static_fields)->get_isSoundOn_8();
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		AudioSource_t3935305588 * L_2 = __this->get_overSource_23();
		NullCheck(L_2);
		AudioSource_Play_m2606248505(L_2, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_3 = ((Score_t2516855617_StaticFields*)Score_t2516855617_il2cpp_TypeInfo_var->static_fields)->get_scoreNum_2();
		int32_t L_4 = ((Score_t2516855617_StaticFields*)Score_t2516855617_il2cpp_TypeInfo_var->static_fields)->get_bestNum_3();
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_5 = ((Score_t2516855617_StaticFields*)Score_t2516855617_il2cpp_TypeInfo_var->static_fields)->get_scoreNum_2();
		PlayerPrefs_SetInt_m1898875631(NULL /*static, unused*/, _stringLiteral1512031223, L_5, /*hidden argument*/NULL);
	}

IL_0047:
	{
		GameObject_t1113636619 * L_6 = __this->get_gameOverScreen_38();
		NullCheck(L_6);
		GameObject_SetActive_m3735894026(L_6, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_7 = GameObject_Find_m3458517891(NULL /*static, unused*/, _stringLiteral4053195286, /*hidden argument*/NULL);
		NullCheck(L_7);
		Text_t1901882714 * L_8 = GameObject_GetComponent_TisText_t1901882714_m2114913816(L_7, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m2114913816_MethodInfo_var);
		String_t* L_9 = Int32_ToString_m141394615((((Score_t2516855617_StaticFields*)Score_t2516855617_il2cpp_TypeInfo_var->static_fields)->get_address_of_scoreNum_2()), /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdInterstitial_t1417649638_il2cpp_TypeInfo_var);
		NendAdInterstitial_t1417649638 * L_10 = NendAdInterstitial_get_Instance_m3378847411(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		NendAdInterstitial_Show_m3619914196(L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 GameScreen::RandomGap()
extern "C"  int32_t GameScreen_RandomGap_m3185011755 (GameScreen_t1800529819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen_RandomGap_m3185011755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)10);
		int32_t L_0 = ((Score_t2516855617_StaticFields*)Score_t2516855617_il2cpp_TypeInfo_var->static_fields)->get_scoreNum_2();
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)40))))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_1 = ((Score_t2516855617_StaticFields*)Score_t2516855617_il2cpp_TypeInfo_var->static_fields)->get_scoreNum_2();
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)70))))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = Random_Range_m110872341(NULL /*static, unused*/, 5, ((int32_t)9), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_003d;
	}

IL_0029:
	{
		int32_t L_3 = ((Score_t2516855617_StaticFields*)Score_t2516855617_il2cpp_TypeInfo_var->static_fields)->get_scoreNum_2();
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)70))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = Random_Range_m110872341(NULL /*static, unused*/, 3, 7, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_003d:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Void GameScreen::.cctor()
extern "C"  void GameScreen__cctor_m841907668 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen__cctor_m841907668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->set_canControl_26((bool)1);
		return;
	}
}
// System.Void GameScreen/<changeScore>c__Iterator1::.ctor()
extern "C"  void U3CchangeScoreU3Ec__Iterator1__ctor_m2602245183 (U3CchangeScoreU3Ec__Iterator1_t982768640 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GameScreen/<changeScore>c__Iterator1::MoveNext()
extern "C"  bool U3CchangeScoreU3Ec__Iterator1_MoveNext_m82251225 (U3CchangeScoreU3Ec__Iterator1_t982768640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CchangeScoreU3Ec__Iterator1_MoveNext_m82251225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0045;
			}
		}
	}
	{
		goto IL_0067;
	}

IL_0021:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1898094827(L_2, (0.7f), /*hidden argument*/NULL);
		__this->set_U24current_0(L_2);
		bool L_3 = __this->get_U24disposing_1();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_2(1);
	}

IL_0040:
	{
		goto IL_0069;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		bool L_4 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_isDead_7();
		if (L_4)
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_5 = ((Score_t2516855617_StaticFields*)Score_t2516855617_il2cpp_TypeInfo_var->static_fields)->get_scoreNum_2();
		((Score_t2516855617_StaticFields*)Score_t2516855617_il2cpp_TypeInfo_var->static_fields)->set_scoreNum_2(((int32_t)((int32_t)L_5+(int32_t)1)));
	}

IL_005b:
	{
		goto IL_0021;
	}
	// Dead block : IL_0060: ldarg.0

IL_0067:
	{
		return (bool)0;
	}

IL_0069:
	{
		return (bool)1;
	}
}
// System.Object GameScreen/<changeScore>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CchangeScoreU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m348997444 (U3CchangeScoreU3Ec__Iterator1_t982768640 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Object GameScreen/<changeScore>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CchangeScoreU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3974086226 (U3CchangeScoreU3Ec__Iterator1_t982768640 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Void GameScreen/<changeScore>c__Iterator1::Dispose()
extern "C"  void U3CchangeScoreU3Ec__Iterator1_Dispose_m491501771 (U3CchangeScoreU3Ec__Iterator1_t982768640 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_1((bool)1);
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void GameScreen/<changeScore>c__Iterator1::Reset()
extern "C"  void U3CchangeScoreU3Ec__Iterator1_Reset_m2031744076 (U3CchangeScoreU3Ec__Iterator1_t982768640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CchangeScoreU3Ec__Iterator1_Reset_m2031744076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GameScreen/<changeSpeed>c__Iterator0::.ctor()
extern "C"  void U3CchangeSpeedU3Ec__Iterator0__ctor_m857096498 (U3CchangeSpeedU3Ec__Iterator0_t85360554 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GameScreen/<changeSpeed>c__Iterator0::MoveNext()
extern "C"  bool U3CchangeSpeedU3Ec__Iterator0_MoveNext_m2181632746 (U3CchangeSpeedU3Ec__Iterator0_t85360554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CchangeSpeedU3Ec__Iterator0_MoveNext_m2181632746_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0045;
			}
		}
	}
	{
		goto IL_0091;
	}

IL_0021:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1898094827(L_2, (5.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_0093;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		float L_4 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_speed_9();
		if ((!(((float)L_4) > ((float)(-4.0f)))))
		{
			goto IL_0085;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		bool L_5 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_isDead_7();
		if (L_5)
		{
			goto IL_0085;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		float L_6 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_speed_9();
		((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->set_speed_9(((float)((float)L_6-(float)(0.21f))));
		GameScreen_t1800529819 * L_7 = __this->get_U24this_0();
		GameScreen_t1800529819 * L_8 = L_7;
		NullCheck(L_8);
		float L_9 = L_8->get_gap_17();
		NullCheck(L_8);
		L_8->set_gap_17(((float)((float)L_9-(float)(0.99f))));
	}

IL_0085:
	{
		goto IL_0021;
	}
	// Dead block : IL_008a: ldarg.0

IL_0091:
	{
		return (bool)0;
	}

IL_0093:
	{
		return (bool)1;
	}
}
// System.Object GameScreen/<changeSpeed>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CchangeSpeedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3899942729 (U3CchangeSpeedU3Ec__Iterator0_t85360554 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object GameScreen/<changeSpeed>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CchangeSpeedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m486575212 (U3CchangeSpeedU3Ec__Iterator0_t85360554 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void GameScreen/<changeSpeed>c__Iterator0::Dispose()
extern "C"  void U3CchangeSpeedU3Ec__Iterator0_Dispose_m3925058397 (U3CchangeSpeedU3Ec__Iterator0_t85360554 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void GameScreen/<changeSpeed>c__Iterator0::Reset()
extern "C"  void U3CchangeSpeedU3Ec__Iterator0_Reset_m1042803279 (U3CchangeSpeedU3Ec__Iterator0_t85360554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CchangeSpeedU3Ec__Iterator0_Reset_m1042803279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GameScreen/<hideInfo>c__Iterator2::.ctor()
extern "C"  void U3ChideInfoU3Ec__Iterator2__ctor_m1502287029 (U3ChideInfoU3Ec__Iterator2_t3795124997 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GameScreen/<hideInfo>c__Iterator2::MoveNext()
extern "C"  bool U3ChideInfoU3Ec__Iterator2_MoveNext_m4283311606 (U3ChideInfoU3Ec__Iterator2_t3795124997 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3ChideInfoU3Ec__Iterator2_MoveNext_m4283311606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0045;
			}
		}
	}
	{
		goto IL_005c;
	}

IL_0021:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1898094827(L_2, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_0(L_2);
		bool L_3 = __this->get_U24disposing_1();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_2(1);
	}

IL_0040:
	{
		goto IL_005e;
	}

IL_0045:
	{
		GameObject_t1113636619 * L_4 = GameObject_Find_m3458517891(NULL /*static, unused*/, _stringLiteral1958592251, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m3735894026(L_4, (bool)0, /*hidden argument*/NULL);
		__this->set_U24PC_2((-1));
	}

IL_005c:
	{
		return (bool)0;
	}

IL_005e:
	{
		return (bool)1;
	}
}
// System.Object GameScreen/<hideInfo>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3ChideInfoU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4086586717 (U3ChideInfoU3Ec__Iterator2_t3795124997 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Object GameScreen/<hideInfo>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3ChideInfoU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2139478540 (U3ChideInfoU3Ec__Iterator2_t3795124997 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Void GameScreen/<hideInfo>c__Iterator2::Dispose()
extern "C"  void U3ChideInfoU3Ec__Iterator2_Dispose_m1031345680 (U3ChideInfoU3Ec__Iterator2_t3795124997 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_1((bool)1);
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void GameScreen/<hideInfo>c__Iterator2::Reset()
extern "C"  void U3ChideInfoU3Ec__Iterator2_Reset_m3294201329 (U3ChideInfoU3Ec__Iterator2_t3795124997 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3ChideInfoU3Ec__Iterator2_Reset_m3294201329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GameScreen/<showIfGameOver>c__Iterator3::.ctor()
extern "C"  void U3CshowIfGameOverU3Ec__Iterator3__ctor_m546072851 (U3CshowIfGameOverU3Ec__Iterator3_t966372778 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GameScreen/<showIfGameOver>c__Iterator3::MoveNext()
extern "C"  bool U3CshowIfGameOverU3Ec__Iterator3_MoveNext_m904246463 (U3CshowIfGameOverU3Ec__Iterator3_t966372778 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CshowIfGameOverU3Ec__Iterator3_MoveNext_m904246463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0045;
			}
		}
	}
	{
		goto IL_0066;
	}

IL_0021:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1898094827(L_2, (1.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_0068;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		bool L_4 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_isDead_7();
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		GameScreen_t1800529819 * L_5 = __this->get_U24this_0();
		NullCheck(L_5);
		GameScreen_gameOverScene_m1998749941(L_5, /*hidden argument*/NULL);
	}

IL_005a:
	{
		goto IL_0021;
	}
	// Dead block : IL_005f: ldarg.0

IL_0066:
	{
		return (bool)0;
	}

IL_0068:
	{
		return (bool)1;
	}
}
// System.Object GameScreen/<showIfGameOver>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CshowIfGameOverU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2384205177 (U3CshowIfGameOverU3Ec__Iterator3_t966372778 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object GameScreen/<showIfGameOver>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CshowIfGameOverU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m2551222715 (U3CshowIfGameOverU3Ec__Iterator3_t966372778 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void GameScreen/<showIfGameOver>c__Iterator3::Dispose()
extern "C"  void U3CshowIfGameOverU3Ec__Iterator3_Dispose_m381588494 (U3CshowIfGameOverU3Ec__Iterator3_t966372778 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void GameScreen/<showIfGameOver>c__Iterator3::Reset()
extern "C"  void U3CshowIfGameOverU3Ec__Iterator3_Reset_m1670965431 (U3CshowIfGameOverU3Ec__Iterator3_t966372778 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CshowIfGameOverU3Ec__Iterator3_Reset_m1670965431_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Ground::.ctor()
extern "C"  void Ground__ctor_m4181856099 (Ground_t4133628138 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ground::Start()
extern "C"  void Ground_Start_m775848767 (Ground_t4133628138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ground_Start_m775848767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Bounds_t2266837910  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Bounds_t2266837910  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t3722313464  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m102368104(L_0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_2 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m2921103810(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m102368104(L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = Vector3_op_Subtraction_m2566684344(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_z_3();
		V_0 = L_6;
		Camera_t4157153871 * L_7 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = V_0;
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m1197556204(&L_9, (0.0f), (0.0f), L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_10 = Camera_ViewportToWorldPoint_m3480887959(L_7, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = (&V_2)->get_x_1();
		__this->set_leftX_4(L_11);
		Camera_t4157153871 * L_12 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = V_0;
		Vector3_t3722313464  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m1197556204(&L_14, (1.0f), (0.0f), L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t3722313464  L_15 = Camera_ViewportToWorldPoint_m3480887959(L_12, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = (&V_3)->get_x_1();
		__this->set_rightX_6(L_16);
		Camera_t4157153871 * L_17 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = V_0;
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m1197556204(&L_19, (0.0f), (0.0f), ((-L_18)), /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t3722313464  L_20 = Camera_ViewportToWorldPoint_m3480887959(L_17, L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		float L_21 = (&V_4)->get_y_2();
		__this->set_topY_5(L_21);
		Camera_t4157153871 * L_22 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = V_0;
		Vector3_t3722313464  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m1197556204(&L_24, (1.0f), (0.0f), L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t3722313464  L_25 = Camera_ViewportToWorldPoint_m3480887959(L_22, L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		float L_26 = (&V_5)->get_y_2();
		__this->set_bottomY_3(L_26);
		GameObject_t1113636619 * L_27 = __this->get_tilePrefab_2();
		NullCheck(L_27);
		Renderer_t2627027031 * L_28 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_27, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_28);
		Bounds_t2266837910  L_29 = Renderer_get_bounds_m2197226282(L_28, /*hidden argument*/NULL);
		V_6 = L_29;
		Vector3_t3722313464  L_30 = Bounds_get_size_m1171376090((&V_6), /*hidden argument*/NULL);
		V_7 = L_30;
		float L_31 = (&V_7)->get_x_1();
		__this->set_groundTileW_7(L_31);
		GameObject_t1113636619 * L_32 = __this->get_tilePrefab_2();
		NullCheck(L_32);
		Renderer_t2627027031 * L_33 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_32, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_33);
		Bounds_t2266837910  L_34 = Renderer_get_bounds_m2197226282(L_33, /*hidden argument*/NULL);
		V_8 = L_34;
		Vector3_t3722313464  L_35 = Bounds_get_size_m1171376090((&V_8), /*hidden argument*/NULL);
		V_9 = L_35;
		float L_36 = (&V_9)->get_y_2();
		__this->set_groundTileH_8(L_36);
		GameObject_t1113636619 * L_37 = __this->get_tilePrefab_2();
		float L_38 = __this->get_leftX_4();
		float L_39 = __this->get_bottomY_3();
		float L_40 = __this->get_groundTileH_8();
		Vector3_t3722313464  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector3__ctor_m1197556204(&L_41, L_38, ((float)((float)L_39+(float)((float)((float)L_40/(float)(2.0f))))), (0.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_42 = Quaternion_get_identity_m3578010038(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_43 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_37, L_41, L_42, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_MethodInfo_var);
		__this->set_block1_9(L_43);
		GameObject_t1113636619 * L_44 = __this->get_tilePrefab_2();
		GameObject_t1113636619 * L_45 = __this->get_block1_9();
		NullCheck(L_45);
		Transform_t3600365921 * L_46 = GameObject_get_transform_m393750976(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_t3722313464  L_47 = Transform_get_position_m102368104(L_46, /*hidden argument*/NULL);
		V_10 = L_47;
		float L_48 = (&V_10)->get_x_1();
		float L_49 = __this->get_groundTileW_7();
		float L_50 = __this->get_bottomY_3();
		float L_51 = __this->get_groundTileH_8();
		Vector3_t3722313464  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector3__ctor_m1197556204(&L_52, ((float)((float)L_48+(float)L_49)), ((float)((float)L_50+(float)((float)((float)L_51/(float)(2.0f))))), (0.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_53 = Quaternion_get_identity_m3578010038(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_54 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_44, L_52, L_53, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_MethodInfo_var);
		__this->set_block2_10(L_54);
		GameObject_t1113636619 * L_55 = __this->get_block1_9();
		NullCheck(L_55);
		Transform_t3600365921 * L_56 = GameObject_get_transform_m393750976(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		Vector3_t3722313464  L_57 = Transform_get_position_m102368104(L_56, /*hidden argument*/NULL);
		V_11 = L_57;
		float L_58 = (&V_11)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Ground_t4133628138_il2cpp_TypeInfo_var);
		((Ground_t4133628138_StaticFields*)Ground_t4133628138_il2cpp_TypeInfo_var->static_fields)->set_posY_11(L_58);
		float L_59 = __this->get_groundTileH_8();
		((Ground_t4133628138_StaticFields*)Ground_t4133628138_il2cpp_TypeInfo_var->static_fields)->set_groundH_12(L_59);
		return;
	}
}
// System.Void Ground::Update()
extern "C"  void Ground_Update_m2247374405 (Ground_t4133628138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ground_Update_m2247374405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		Vector3__ctor_m1197556204((&V_0), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = __this->get_block1_9();
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = GameObject_get_transform_m393750976(L_0, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		float L_3 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_speed_9();
		Vector3_t3722313464  L_4 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		float L_5 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Translate_m3317641446(L_1, L_6, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_7 = __this->get_block2_10();
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = GameObject_get_transform_m393750976(L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = V_0;
		float L_10 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_speed_9();
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_Translate_m3317641446(L_8, L_13, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_14 = __this->get_block1_9();
		NullCheck(L_14);
		Transform_t3600365921 * L_15 = GameObject_get_transform_m393750976(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t3722313464  L_16 = Transform_get_position_m102368104(L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		float L_17 = (&V_1)->get_x_1();
		float L_18 = __this->get_groundTileW_7();
		float L_19 = __this->get_leftX_4();
		if ((!(((float)((float)((float)L_17+(float)((float)((float)L_18/(float)(2.0f)))))) < ((float)L_19))))
		{
			goto IL_00e1;
		}
	}
	{
		GameObject_t1113636619 * L_20 = __this->get_block1_9();
		NullCheck(L_20);
		Transform_t3600365921 * L_21 = GameObject_get_transform_m393750976(L_20, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_22 = __this->get_block2_10();
		NullCheck(L_22);
		Transform_t3600365921 * L_23 = GameObject_get_transform_m393750976(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t3722313464  L_24 = Transform_get_position_m102368104(L_23, /*hidden argument*/NULL);
		V_2 = L_24;
		float L_25 = (&V_2)->get_x_1();
		float L_26 = __this->get_groundTileW_7();
		GameObject_t1113636619 * L_27 = __this->get_block1_9();
		NullCheck(L_27);
		Transform_t3600365921 * L_28 = GameObject_get_transform_m393750976(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t3722313464  L_29 = Transform_get_position_m102368104(L_28, /*hidden argument*/NULL);
		V_3 = L_29;
		float L_30 = (&V_3)->get_y_2();
		Vector3_t3722313464  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector3__ctor_m1197556204(&L_31, ((float)((float)L_25+(float)L_26)), L_30, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_position_m2361854178(L_21, L_31, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		GameObject_t1113636619 * L_32 = __this->get_block2_10();
		NullCheck(L_32);
		Transform_t3600365921 * L_33 = GameObject_get_transform_m393750976(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t3722313464  L_34 = Transform_get_position_m102368104(L_33, /*hidden argument*/NULL);
		V_4 = L_34;
		float L_35 = (&V_4)->get_x_1();
		float L_36 = __this->get_groundTileW_7();
		float L_37 = __this->get_leftX_4();
		if ((!(((float)((float)((float)L_35+(float)((float)((float)L_36/(float)(2.0f)))))) < ((float)L_37))))
		{
			goto IL_0165;
		}
	}
	{
		GameObject_t1113636619 * L_38 = __this->get_block2_10();
		NullCheck(L_38);
		Transform_t3600365921 * L_39 = GameObject_get_transform_m393750976(L_38, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_40 = __this->get_block1_9();
		NullCheck(L_40);
		Transform_t3600365921 * L_41 = GameObject_get_transform_m393750976(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t3722313464  L_42 = Transform_get_position_m102368104(L_41, /*hidden argument*/NULL);
		V_5 = L_42;
		float L_43 = (&V_5)->get_x_1();
		float L_44 = __this->get_groundTileW_7();
		GameObject_t1113636619 * L_45 = __this->get_block2_10();
		NullCheck(L_45);
		Transform_t3600365921 * L_46 = GameObject_get_transform_m393750976(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_t3722313464  L_47 = Transform_get_position_m102368104(L_46, /*hidden argument*/NULL);
		V_6 = L_47;
		float L_48 = (&V_6)->get_y_2();
		Vector3_t3722313464  L_49;
		memset(&L_49, 0, sizeof(L_49));
		Vector3__ctor_m1197556204(&L_49, ((float)((float)L_43+(float)L_44)), L_48, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_set_position_m2361854178(L_39, L_49, /*hidden argument*/NULL);
	}

IL_0165:
	{
		return;
	}
}
// System.Void Ground::.cctor()
extern "C"  void Ground__cctor_m1362236200 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HCAnimatedBanner::.ctor()
extern "C"  void HCAnimatedBanner__ctor_m2740142623 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HCAnimatedBanner__ctor_m2740142623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_jsonLink_2(L_0);
		__this->set_defaultTLinkAndroid_9(_stringLiteral3197466134);
		__this->set_defaultTLinkIOS_10(_stringLiteral1596542974);
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String HCAnimatedBanner::get_transitionLink()
extern "C"  String_t* HCAnimatedBanner_get_transitionLink_m3352356618 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HCAnimatedBanner_get_transitionLink_m3352356618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get__transitionLink_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = __this->get_defaultTLinkIOS_10();
		__this->set__transitionLink_8(L_2);
	}

IL_001c:
	{
		String_t* L_3 = __this->get__transitionLink_8();
		return L_3;
	}
}
// System.Void HCAnimatedBanner::set_transitionLink(System.String)
extern "C"  void HCAnimatedBanner_set_transitionLink_m3657221653 (HCAnimatedBanner_t2921968851 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__transitionLink_8(L_0);
		return;
	}
}
// System.Void HCAnimatedBanner::Awake()
extern "C"  void HCAnimatedBanner_Awake_m3336125038 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HCAnimatedBanner_Awake_m3336125038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RawImage_t3182918964 * L_0 = Component_GetComponent_TisRawImage_t3182918964_m527061191(__this, /*hidden argument*/Component_GetComponent_TisRawImage_t3182918964_m527061191_MethodInfo_var);
		Color_t2555686324  L_1 = Color_get_clear_m1773884651(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		Il2CppObject * L_2 = HCAnimatedBanner_LoadJsonData_m2260823519(__this, /*hidden argument*/NULL);
		__this->set_jCoroutine_13(L_2);
		Il2CppObject * L_3 = HCAnimatedBanner_LoadTexture_m946452470(__this, /*hidden argument*/NULL);
		__this->set_lCoroutine_14(L_3);
		return;
	}
}
// System.Void HCAnimatedBanner::OnEnable()
extern "C"  void HCAnimatedBanner_OnEnable_m4207362778 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method)
{
	{
		List_1_t926430637 * L_0 = __this->get_bannerDataList_3();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_jCoroutine_13();
		MonoBehaviour_StartCoroutine_m4001331470(__this, L_1, /*hidden argument*/NULL);
		goto IL_002a;
	}

IL_001d:
	{
		Il2CppObject * L_2 = __this->get_lCoroutine_14();
		MonoBehaviour_StartCoroutine_m4001331470(__this, L_2, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void HCAnimatedBanner::OnDisable()
extern "C"  void HCAnimatedBanner_OnDisable_m1049953580 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method)
{
	{
		List_1_t926430637 * L_0 = __this->get_bannerDataList_3();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_jCoroutine_13();
		MonoBehaviour_StopCoroutine_m1038915083(__this, L_1, /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001c:
	{
		Il2CppObject * L_2 = __this->get_lCoroutine_14();
		MonoBehaviour_StopCoroutine_m1038915083(__this, L_2, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Collections.IEnumerator HCAnimatedBanner::LoadJsonData()
extern "C"  Il2CppObject * HCAnimatedBanner_LoadJsonData_m2260823519 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HCAnimatedBanner_LoadJsonData_m2260823519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * V_0 = NULL;
	{
		U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * L_0 = (U3CLoadJsonDataU3Ec__Iterator0_t1810388169 *)il2cpp_codegen_object_new(U3CLoadJsonDataU3Ec__Iterator0_t1810388169_il2cpp_TypeInfo_var);
		U3CLoadJsonDataU3Ec__Iterator0__ctor_m4017495395(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator HCAnimatedBanner::LoadTexture()
extern "C"  Il2CppObject * HCAnimatedBanner_LoadTexture_m946452470 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HCAnimatedBanner_LoadTexture_m946452470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadTextureU3Ec__Iterator1_t1051594053 * V_0 = NULL;
	{
		U3CLoadTextureU3Ec__Iterator1_t1051594053 * L_0 = (U3CLoadTextureU3Ec__Iterator1_t1051594053 *)il2cpp_codegen_object_new(U3CLoadTextureU3Ec__Iterator1_t1051594053_il2cpp_TypeInfo_var);
		U3CLoadTextureU3Ec__Iterator1__ctor_m2885060348(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadTextureU3Ec__Iterator1_t1051594053 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_5(__this);
		U3CLoadTextureU3Ec__Iterator1_t1051594053 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator HCAnimatedBanner::AnimateBanner(System.Int32)
extern "C"  Il2CppObject * HCAnimatedBanner_AnimateBanner_m3687925943 (HCAnimatedBanner_t2921968851 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HCAnimatedBanner_AnimateBanner_m3687925943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAnimateBannerU3Ec__Iterator2_t688012631 * V_0 = NULL;
	{
		U3CAnimateBannerU3Ec__Iterator2_t688012631 * L_0 = (U3CAnimateBannerU3Ec__Iterator2_t688012631 *)il2cpp_codegen_object_new(U3CAnimateBannerU3Ec__Iterator2_t688012631_il2cpp_TypeInfo_var);
		U3CAnimateBannerU3Ec__Iterator2__ctor_m2061546856(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAnimateBannerU3Ec__Iterator2_t688012631 * L_1 = V_0;
		int32_t L_2 = ___index0;
		NullCheck(L_1);
		L_1->set_index_0(L_2);
		U3CAnimateBannerU3Ec__Iterator2_t688012631 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_11(__this);
		U3CAnimateBannerU3Ec__Iterator2_t688012631 * L_4 = V_0;
		return L_4;
	}
}
// System.Void HCAnimatedBanner::ApplyTexture(UnityEngine.Texture2D)
extern "C"  void HCAnimatedBanner_ApplyTexture_m3563656807 (HCAnimatedBanner_t2921968851 * __this, Texture2D_t3840446185 * ___texture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HCAnimatedBanner_ApplyTexture_m3563656807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RawImage_t3182918964 * V_0 = NULL;
	Color_t2555686324  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RawImage_t3182918964 * L_0 = Component_GetComponent_TisRawImage_t3182918964_m527061191(__this, /*hidden argument*/Component_GetComponent_TisRawImage_t3182918964_m527061191_MethodInfo_var);
		V_0 = L_0;
		RawImage_t3182918964 * L_1 = V_0;
		NullCheck(L_1);
		Color_t2555686324  L_2 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_1);
		V_1 = L_2;
		float L_3 = (&V_1)->get_a_3();
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_002a;
		}
	}
	{
		RawImage_t3182918964 * L_4 = V_0;
		Color_t2555686324  L_5 = Color_get_white_m3544547002(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_4, L_5);
	}

IL_002a:
	{
		RawImage_t3182918964 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m487959476(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003d;
		}
	}
	{
		RawImage_t3182918964 * L_8 = V_0;
		Texture2D_t3840446185 * L_9 = ___texture0;
		NullCheck(L_8);
		RawImage_set_texture_m415027901(L_8, L_9, /*hidden argument*/NULL);
		return;
	}

IL_003d:
	{
		return;
	}
}
// System.Void HCAnimatedBanner::OpenTransitionURL()
extern "C"  void HCAnimatedBanner_OpenTransitionURL_m2909191181 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_internetReachability_m2044109931(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		String_t* L_1 = HCAnimatedBanner_get_transitionLink_m3352356618(__this, /*hidden argument*/NULL);
		Application_OpenURL_m1638057963(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void HCAnimatedBanner::OnDestroy()
extern "C"  void HCAnimatedBanner_OnDestroy_m2229280568 (HCAnimatedBanner_t2921968851 * __this, const MethodInfo* method)
{
	{
		Resources_UnloadUnusedAssets_m2573856924(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HCAnimatedBanner/<AnimateBanner>c__Iterator2::.ctor()
extern "C"  void U3CAnimateBannerU3Ec__Iterator2__ctor_m2061546856 (U3CAnimateBannerU3Ec__Iterator2_t688012631 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean HCAnimatedBanner/<AnimateBanner>c__Iterator2::MoveNext()
extern "C"  bool U3CAnimateBannerU3Ec__Iterator2_MoveNext_m987987274 (U3CAnimateBannerU3Ec__Iterator2_t688012631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimateBannerU3Ec__Iterator2_MoveNext_m987987274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = __this->get_U24PC_14();
		V_0 = L_0;
		__this->set_U24PC_14((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_02ae;
			}
		}
	}
	{
		goto IL_02ba;
	}

IL_0021:
	{
		U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * L_2 = (U3CAnimateBannerU3Ec__AnonStorey3_t102123587 *)il2cpp_codegen_object_new(U3CAnimateBannerU3Ec__AnonStorey3_t102123587_il2cpp_TypeInfo_var);
		U3CAnimateBannerU3Ec__AnonStorey3__ctor_m3844873053(L_2, /*hidden argument*/NULL);
		__this->set_U24locvar0_15(L_2);
		U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * L_3 = __this->get_U24locvar0_15();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__refU242_2(__this);
		U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * L_4 = __this->get_U24locvar0_15();
		HCAnimatedBanner_t2921968851 * L_5 = __this->get_U24this_11();
		NullCheck(L_5);
		List_1_t926430637 * L_6 = L_5->get_bannerDataList_3();
		int32_t L_7 = __this->get_index_0();
		NullCheck(L_6);
		AnimBannerData_t3749323191 * L_8 = List_1_get_Item_m335550220(L_6, L_7, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = L_8->get_textureWidth_2();
		NullCheck(L_4);
		L_4->set_textureWidth_0(L_9);
		U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * L_10 = __this->get_U24locvar0_15();
		HCAnimatedBanner_t2921968851 * L_11 = __this->get_U24this_11();
		NullCheck(L_11);
		List_1_t926430637 * L_12 = L_11->get_bannerDataList_3();
		int32_t L_13 = __this->get_index_0();
		NullCheck(L_12);
		AnimBannerData_t3749323191 * L_14 = List_1_get_Item_m335550220(L_12, L_13, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_14);
		int32_t L_15 = L_14->get_textureHeight_3();
		NullCheck(L_10);
		L_10->set_textureHeight_1(L_15);
		HCAnimatedBanner_t2921968851 * L_16 = __this->get_U24this_11();
		NullCheck(L_16);
		List_1_t926430637 * L_17 = L_16->get_bannerDataList_3();
		int32_t L_18 = __this->get_index_0();
		NullCheck(L_17);
		AnimBannerData_t3749323191 * L_19 = List_1_get_Item_m335550220(L_17, L_18, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_19);
		int32_t L_20 = L_19->get_spriteWidth_4();
		__this->set_U3CspriteWidthU3E__0_1(L_20);
		HCAnimatedBanner_t2921968851 * L_21 = __this->get_U24this_11();
		NullCheck(L_21);
		List_1_t926430637 * L_22 = L_21->get_bannerDataList_3();
		int32_t L_23 = __this->get_index_0();
		NullCheck(L_22);
		AnimBannerData_t3749323191 * L_24 = List_1_get_Item_m335550220(L_22, L_23, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_24);
		int32_t L_25 = L_24->get_spriteHeight_5();
		__this->set_U3CspriteHeightU3E__0_2(L_25);
		U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * L_26 = __this->get_U24locvar0_15();
		NullCheck(L_26);
		int32_t L_27 = L_26->get_textureWidth_0();
		int32_t L_28 = __this->get_U3CspriteWidthU3E__0_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_29 = Mathf_FloorToInt_m717029688(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_27/(int32_t)L_28))))), /*hidden argument*/NULL);
		__this->set_U3ChorCountU3E__0_3(L_29);
		U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * L_30 = __this->get_U24locvar0_15();
		NullCheck(L_30);
		int32_t L_31 = L_30->get_textureHeight_1();
		int32_t L_32 = __this->get_U3CspriteHeightU3E__0_2();
		int32_t L_33 = Mathf_FloorToInt_m717029688(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_31/(int32_t)L_32))))), /*hidden argument*/NULL);
		__this->set_U3CverCountU3E__0_4(L_33);
		HCAnimatedBanner_t2921968851 * L_34 = __this->get_U24this_11();
		NullCheck(L_34);
		List_1_t926430637 * L_35 = L_34->get_bannerDataList_3();
		int32_t L_36 = __this->get_index_0();
		NullCheck(L_35);
		AnimBannerData_t3749323191 * L_37 = List_1_get_Item_m335550220(L_35, L_36, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_37);
		int32_t L_38 = L_37->get_spriteCount_6();
		__this->set_U3CspriteCountU3E__0_5(L_38);
		HCAnimatedBanner_t2921968851 * L_39 = __this->get_U24this_11();
		NullCheck(L_39);
		List_1_t926430637 * L_40 = L_39->get_bannerDataList_3();
		int32_t L_41 = __this->get_index_0();
		NullCheck(L_40);
		AnimBannerData_t3749323191 * L_42 = List_1_get_Item_m335550220(L_40, L_41, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_42);
		int32_t L_43 = L_42->get_spriteCount_6();
		__this->set_U3CspriteRectsU3E__0_6(((RectU5BU5D_t2936723554*)SZArrayNew(RectU5BU5D_t2936723554_il2cpp_TypeInfo_var, (uint32_t)L_43)));
		V_1 = 0;
		goto IL_01d4;
	}

IL_0150:
	{
		V_2 = 0;
		goto IL_01c4;
	}

IL_0157:
	{
		int32_t L_44 = V_2;
		int32_t L_45 = __this->get_U3ChorCountU3E__0_3();
		int32_t L_46 = V_1;
		int32_t L_47 = __this->get_U3CspriteCountU3E__0_5();
		if ((((int32_t)((int32_t)((int32_t)L_44+(int32_t)((int32_t)((int32_t)L_45*(int32_t)L_46))))) <= ((int32_t)((int32_t)((int32_t)L_47-(int32_t)1)))))
		{
			goto IL_0173;
		}
	}
	{
		goto IL_01d0;
	}

IL_0173:
	{
		RectU5BU5D_t2936723554* L_48 = __this->get_U3CspriteRectsU3E__0_6();
		int32_t L_49 = V_2;
		int32_t L_50 = __this->get_U3ChorCountU3E__0_3();
		int32_t L_51 = V_1;
		NullCheck(L_48);
		int32_t L_52 = V_2;
		int32_t L_53 = __this->get_U3CspriteWidthU3E__0_1();
		U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * L_54 = __this->get_U24locvar0_15();
		NullCheck(L_54);
		int32_t L_55 = L_54->get_textureHeight_1();
		int32_t L_56 = V_1;
		int32_t L_57 = __this->get_U3CspriteHeightU3E__0_2();
		int32_t L_58 = __this->get_U3CspriteWidthU3E__0_1();
		int32_t L_59 = __this->get_U3CspriteHeightU3E__0_2();
		Rect_t2360479859  L_60;
		memset(&L_60, 0, sizeof(L_60));
		Rect__ctor_m2635848439(&L_60, (((float)((float)((int32_t)((int32_t)L_52*(int32_t)L_53))))), (((float)((float)((int32_t)((int32_t)L_55-(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_56+(int32_t)1))*(int32_t)L_57))))))), (((float)((float)L_58))), (((float)((float)L_59))), /*hidden argument*/NULL);
		(*(Rect_t2360479859 *)((L_48)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_49+(int32_t)((int32_t)((int32_t)L_50*(int32_t)L_51)))))))) = L_60;
		int32_t L_61 = V_2;
		V_2 = ((int32_t)((int32_t)L_61+(int32_t)1));
	}

IL_01c4:
	{
		int32_t L_62 = V_2;
		int32_t L_63 = __this->get_U3ChorCountU3E__0_3();
		if ((((int32_t)L_62) < ((int32_t)L_63)))
		{
			goto IL_0157;
		}
	}

IL_01d0:
	{
		int32_t L_64 = V_1;
		V_1 = ((int32_t)((int32_t)L_64+(int32_t)1));
	}

IL_01d4:
	{
		int32_t L_65 = V_1;
		int32_t L_66 = __this->get_U3CverCountU3E__0_4();
		if ((((int32_t)L_65) < ((int32_t)L_66)))
		{
			goto IL_0150;
		}
	}
	{
		RectU5BU5D_t2936723554* L_67 = __this->get_U3CspriteRectsU3E__0_6();
		U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * L_68 = __this->get_U24locvar0_15();
		IntPtr_t L_69;
		L_69.set_m_value_0((void*)(void*)U3CAnimateBannerU3Ec__AnonStorey3_U3CU3Em__0_m1863904840_MethodInfo_var);
		Func_2_t1820288298 * L_70 = (Func_2_t1820288298 *)il2cpp_codegen_object_new(Func_2_t1820288298_il2cpp_TypeInfo_var);
		Func_2__ctor_m3848706282(L_70, L_68, L_69, /*hidden argument*/Func_2__ctor_m3848706282_MethodInfo_var);
		Il2CppObject* L_71 = Enumerable_Select_TisRect_t2360479859_TisRect_t2360479859_m1309108196(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_67, L_70, /*hidden argument*/Enumerable_Select_TisRect_t2360479859_TisRect_t2360479859_m1309108196_MethodInfo_var);
		RectU5BU5D_t2936723554* L_72 = Enumerable_ToArray_TisRect_t2360479859_m2199875962(NULL /*static, unused*/, L_71, /*hidden argument*/Enumerable_ToArray_TisRect_t2360479859_m2199875962_MethodInfo_var);
		__this->set_U3CuvRectsU3E__0_7(L_72);
		HCAnimatedBanner_t2921968851 * L_73 = __this->get_U24this_11();
		NullCheck(L_73);
		RawImage_t3182918964 * L_74 = Component_GetComponent_TisRawImage_t3182918964_m527061191(L_73, /*hidden argument*/Component_GetComponent_TisRawImage_t3182918964_m527061191_MethodInfo_var);
		__this->set_U3CrImageU3E__0_8(L_74);
		__this->set_U3CrIndexU3E__0_9(0);
		HCAnimatedBanner_t2921968851 * L_75 = __this->get_U24this_11();
		NullCheck(L_75);
		List_1_t926430637 * L_76 = L_75->get_bannerDataList_3();
		int32_t L_77 = __this->get_index_0();
		NullCheck(L_76);
		AnimBannerData_t3749323191 * L_78 = List_1_get_Item_m335550220(L_76, L_77, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_78);
		double L_79 = L_78->get_animWaitTime_7();
		__this->set_U3CwaitU3E__0_10((((float)((float)L_79))));
	}

IL_0241:
	{
		RawImage_t3182918964 * L_80 = __this->get_U3CrImageU3E__0_8();
		RectU5BU5D_t2936723554* L_81 = __this->get_U3CuvRectsU3E__0_7();
		int32_t L_82 = __this->get_U3CrIndexU3E__0_9();
		NullCheck(L_81);
		NullCheck(L_80);
		RawImage_set_uvRect_m529943894(L_80, (*(Rect_t2360479859 *)((L_81)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_82)))), /*hidden argument*/NULL);
		int32_t L_83 = __this->get_U3CrIndexU3E__0_9();
		int32_t L_84 = ((int32_t)((int32_t)L_83+(int32_t)1));
		V_3 = L_84;
		__this->set_U3CrIndexU3E__0_9(L_84);
		int32_t L_85 = V_3;
		RectU5BU5D_t2936723554* L_86 = __this->get_U3CspriteRectsU3E__0_6();
		NullCheck(L_86);
		if ((((int32_t)L_85) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_86)->max_length))))-(int32_t)1)))))
		{
			goto IL_0289;
		}
	}
	{
		__this->set_U3CrIndexU3E__0_9(0);
	}

IL_0289:
	{
		float L_87 = __this->get_U3CwaitU3E__0_10();
		WaitForSeconds_t1699091251 * L_88 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1898094827(L_88, L_87, /*hidden argument*/NULL);
		__this->set_U24current_12(L_88);
		bool L_89 = __this->get_U24disposing_13();
		if (L_89)
		{
			goto IL_02a9;
		}
	}
	{
		__this->set_U24PC_14(1);
	}

IL_02a9:
	{
		goto IL_02bc;
	}

IL_02ae:
	{
		goto IL_0241;
	}
	// Dead block : IL_02b3: ldarg.0

IL_02ba:
	{
		return (bool)0;
	}

IL_02bc:
	{
		return (bool)1;
	}
}
// System.Object HCAnimatedBanner/<AnimateBanner>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAnimateBannerU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1888753483 (U3CAnimateBannerU3Ec__Iterator2_t688012631 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_12();
		return L_0;
	}
}
// System.Object HCAnimatedBanner/<AnimateBanner>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAnimateBannerU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2536922814 (U3CAnimateBannerU3Ec__Iterator2_t688012631 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_12();
		return L_0;
	}
}
// System.Void HCAnimatedBanner/<AnimateBanner>c__Iterator2::Dispose()
extern "C"  void U3CAnimateBannerU3Ec__Iterator2_Dispose_m1381739451 (U3CAnimateBannerU3Ec__Iterator2_t688012631 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_13((bool)1);
		__this->set_U24PC_14((-1));
		return;
	}
}
// System.Void HCAnimatedBanner/<AnimateBanner>c__Iterator2::Reset()
extern "C"  void U3CAnimateBannerU3Ec__Iterator2_Reset_m548285055 (U3CAnimateBannerU3Ec__Iterator2_t688012631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimateBannerU3Ec__Iterator2_Reset_m548285055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void HCAnimatedBanner/<AnimateBanner>c__Iterator2/<AnimateBanner>c__AnonStorey3::.ctor()
extern "C"  void U3CAnimateBannerU3Ec__AnonStorey3__ctor_m3844873053 (U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect HCAnimatedBanner/<AnimateBanner>c__Iterator2/<AnimateBanner>c__AnonStorey3::<>m__0(UnityEngine.Rect)
extern "C"  Rect_t2360479859  U3CAnimateBannerU3Ec__AnonStorey3_U3CU3Em__0_m1863904840 (U3CAnimateBannerU3Ec__AnonStorey3_t102123587 * __this, Rect_t2360479859  ___s0, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_x_m3218181674((&___s0), /*hidden argument*/NULL);
		int32_t L_1 = __this->get_textureWidth_0();
		float L_2 = Rect_get_y_m3218181675((&___s0), /*hidden argument*/NULL);
		int32_t L_3 = __this->get_textureHeight_1();
		float L_4 = Rect_get_width_m3421965717((&___s0), /*hidden argument*/NULL);
		int32_t L_5 = __this->get_textureWidth_0();
		float L_6 = Rect_get_height_m977101306((&___s0), /*hidden argument*/NULL);
		int32_t L_7 = __this->get_textureHeight_1();
		Rect_t2360479859  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Rect__ctor_m2635848439(&L_8, ((float)((float)L_0/(float)(((float)((float)L_1))))), ((float)((float)L_2/(float)(((float)((float)L_3))))), ((float)((float)L_4/(float)(((float)((float)L_5))))), ((float)((float)L_6/(float)(((float)((float)L_7))))), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void HCAnimatedBanner/<LoadJsonData>c__Iterator0::.ctor()
extern "C"  void U3CLoadJsonDataU3Ec__Iterator0__ctor_m4017495395 (U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean HCAnimatedBanner/<LoadJsonData>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadJsonDataU3Ec__Iterator0_MoveNext_m859263122 (U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadJsonDataU3Ec__Iterator0_MoveNext_m859263122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_003c;
			}
		}
	}
	{
		goto IL_00dd;
	}

IL_0023:
	{
		HCAnimatedBanner_t2921968851 * L_2 = __this->get_U24this_1();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_jsonLink_2();
		WWW_t3688466362 * L_4 = (WWW_t3688466362 *)il2cpp_codegen_object_new(WWW_t3688466362_il2cpp_TypeInfo_var);
		WWW__ctor_m1181807108(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__1_0(L_4);
		V_0 = ((int32_t)-3);
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			switch (((int32_t)((int32_t)L_5-(int32_t)1)))
			{
				case 0:
				{
					goto IL_006a;
				}
			}
		}

IL_0048:
		{
			WWW_t3688466362 * L_6 = __this->get_U3CwwwU3E__1_0();
			__this->set_U24current_2(L_6);
			bool L_7 = __this->get_U24disposing_3();
			if (L_7)
			{
				goto IL_0063;
			}
		}

IL_005c:
		{
			__this->set_U24PC_4(1);
		}

IL_0063:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xDF, FINALLY_009f);
		}

IL_006a:
		{
			WWW_t3688466362 * L_8 = __this->get_U3CwwwU3E__1_0();
			NullCheck(L_8);
			String_t* L_9 = WWW_get_error_m4112162252(L_8, /*hidden argument*/NULL);
			if (L_9)
			{
				goto IL_009a;
			}
		}

IL_007a:
		{
			HCAnimatedBanner_t2921968851 * L_10 = __this->get_U24this_1();
			WWW_t3688466362 * L_11 = __this->get_U3CwwwU3E__1_0();
			NullCheck(L_11);
			String_t* L_12 = WWW_get_text_m1327524635(L_11, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
			AnimBannerDataU5BU5D_t1736877454* L_13 = JsonMapper_ToObject_TisAnimBannerDataU5BU5D_t1736877454_m3809780039(NULL /*static, unused*/, L_12, /*hidden argument*/JsonMapper_ToObject_TisAnimBannerDataU5BU5D_t1736877454_m3809780039_MethodInfo_var);
			List_1_t926430637 * L_14 = Enumerable_ToList_TisAnimBannerData_t3749323191_m1433232041(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_13, /*hidden argument*/Enumerable_ToList_TisAnimBannerData_t3749323191_m1433232041_MethodInfo_var);
			NullCheck(L_10);
			L_10->set_bannerDataList_3(L_14);
		}

IL_009a:
		{
			IL2CPP_LEAVE(0xAA, FINALLY_009f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_009f;
	}

FINALLY_009f:
	{ // begin finally (depth: 1)
		{
			bool L_15 = V_1;
			if (!L_15)
			{
				goto IL_00a3;
			}
		}

IL_00a2:
		{
			IL2CPP_END_FINALLY(159)
		}

IL_00a3:
		{
			U3CLoadJsonDataU3Ec__Iterator0_U3CU3E__Finally0_m3163288294(__this, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(159)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(159)
	{
		IL2CPP_JUMP_TBL(0xDF, IL_00df)
		IL2CPP_JUMP_TBL(0xAA, IL_00aa)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_00aa:
	{
		HCAnimatedBanner_t2921968851 * L_16 = __this->get_U24this_1();
		NullCheck(L_16);
		List_1_t926430637 * L_17 = L_16->get_bannerDataList_3();
		if (L_17)
		{
			goto IL_00bf;
		}
	}
	{
		goto IL_00dd;
	}

IL_00bf:
	{
		HCAnimatedBanner_t2921968851 * L_18 = __this->get_U24this_1();
		HCAnimatedBanner_t2921968851 * L_19 = __this->get_U24this_1();
		NullCheck(L_19);
		Il2CppObject * L_20 = L_19->get_lCoroutine_14();
		NullCheck(L_18);
		MonoBehaviour_StartCoroutine_m4001331470(L_18, L_20, /*hidden argument*/NULL);
		__this->set_U24PC_4((-1));
	}

IL_00dd:
	{
		return (bool)0;
	}

IL_00df:
	{
		return (bool)1;
	}
}
// System.Object HCAnimatedBanner/<LoadJsonData>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadJsonDataU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1290891549 (U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object HCAnimatedBanner/<LoadJsonData>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadJsonDataU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3962886029 (U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void HCAnimatedBanner/<LoadJsonData>c__Iterator0::Dispose()
extern "C"  void U3CLoadJsonDataU3Ec__Iterator0_Dispose_m4242139427 (U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0034;
			}
			case 1:
			{
				goto IL_0028;
			}
		}
	}
	{
		goto IL_0034;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x34, FINALLY_002d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		U3CLoadJsonDataU3Ec__Iterator0_U3CU3E__Finally0_m3163288294(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(45)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x34, IL_0034)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0034:
	{
		return;
	}
}
// System.Void HCAnimatedBanner/<LoadJsonData>c__Iterator0::Reset()
extern "C"  void U3CLoadJsonDataU3Ec__Iterator0_Reset_m735945691 (U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadJsonDataU3Ec__Iterator0_Reset_m735945691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void HCAnimatedBanner/<LoadJsonData>c__Iterator0::<>__Finally0()
extern "C"  void U3CLoadJsonDataU3Ec__Iterator0_U3CU3E__Finally0_m3163288294 (U3CLoadJsonDataU3Ec__Iterator0_t1810388169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadJsonDataU3Ec__Iterator0_U3CU3E__Finally0_m3163288294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WWW_t3688466362 * L_0 = __this->get_U3CwwwU3E__1_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		WWW_t3688466362 * L_1 = __this->get_U3CwwwU3E__1_0();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void HCAnimatedBanner/<LoadTexture>c__Iterator1::.ctor()
extern "C"  void U3CLoadTextureU3Ec__Iterator1__ctor_m2885060348 (U3CLoadTextureU3Ec__Iterator1_t1051594053 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean HCAnimatedBanner/<LoadTexture>c__Iterator1::MoveNext()
extern "C"  bool U3CLoadTextureU3Ec__Iterator1_MoveNext_m89920283 (U3CLoadTextureU3Ec__Iterator1_t1051594053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadTextureU3Ec__Iterator1_MoveNext_m89920283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	ByteU5BU5D_t4116647657* V_2 = NULL;
	Texture2D_t3840446185 * V_3 = NULL;
	int32_t V_4 = 0;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24PC_8((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_002b;
			}
			case 1:
			{
				goto IL_016a;
			}
			case 2:
			{
				goto IL_01f7;
			}
			case 3:
			{
				goto IL_02f6;
			}
		}
	}
	{
		goto IL_0385;
	}

IL_002b:
	{
		int32_t L_2 = PlayerPrefs_GetInt_m3299375436(NULL /*static, unused*/, _stringLiteral3196920305, 0, /*hidden argument*/NULL);
		__this->set_U3CindexU3E__0_0(L_2);
		int32_t L_3 = __this->get_U3CindexU3E__0_0();
		HCAnimatedBanner_t2921968851 * L_4 = __this->get_U24this_5();
		NullCheck(L_4);
		List_1_t926430637 * L_5 = L_4->get_bannerDataList_3();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m3914099077(L_5, /*hidden argument*/List_1_get_Count_m3914099077_MethodInfo_var);
		if ((((int32_t)L_3) < ((int32_t)L_6)))
		{
			goto IL_005e;
		}
	}
	{
		__this->set_U3CindexU3E__0_0(0);
	}

IL_005e:
	{
		HCAnimatedBanner_t2921968851 * L_7 = __this->get_U24this_5();
		HCAnimatedBanner_t2921968851 * L_8 = __this->get_U24this_5();
		NullCheck(L_8);
		List_1_t926430637 * L_9 = L_8->get_bannerDataList_3();
		int32_t L_10 = __this->get_U3CindexU3E__0_0();
		NullCheck(L_9);
		AnimBannerData_t3749323191 * L_11 = List_1_get_Item_m335550220(L_9, L_10, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_11);
		String_t* L_12 = L_11->get_textureIdName_0();
		NullCheck(L_7);
		L_7->set_textureIdName_6(L_12);
		HCAnimatedBanner_t2921968851 * L_13 = __this->get_U24this_5();
		HCAnimatedBanner_t2921968851 * L_14 = __this->get_U24this_5();
		NullCheck(L_14);
		List_1_t926430637 * L_15 = L_14->get_bannerDataList_3();
		int32_t L_16 = __this->get_U3CindexU3E__0_0();
		NullCheck(L_15);
		AnimBannerData_t3749323191 * L_17 = List_1_get_Item_m335550220(L_15, L_16, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_17);
		String_t* L_18 = L_17->get_textureUrl_1();
		NullCheck(L_13);
		L_13->set_textureUrl_7(L_18);
		HCAnimatedBanner_t2921968851 * L_19 = __this->get_U24this_5();
		String_t* L_20 = Application_get_persistentDataPath_m1664523141(NULL /*static, unused*/, /*hidden argument*/NULL);
		HCAnimatedBanner_t2921968851 * L_21 = __this->get_U24this_5();
		NullCheck(L_21);
		String_t* L_22 = L_21->get_textureIdName_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m3755062657(NULL /*static, unused*/, L_20, _stringLiteral3452614529, L_22, /*hidden argument*/NULL);
		NullCheck(L_19);
		L_19->set_texturePath_11(L_23);
		HCAnimatedBanner_t2921968851 * L_24 = __this->get_U24this_5();
		NullCheck(L_24);
		List_1_t926430637 * L_25 = L_24->get_bannerDataList_3();
		int32_t L_26 = __this->get_U3CindexU3E__0_0();
		NullCheck(L_25);
		AnimBannerData_t3749323191 * L_27 = List_1_get_Item_m335550220(L_25, L_26, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_27);
		int32_t L_28 = L_27->get_textureWidth_2();
		__this->set_U3CtextureWidthU3E__0_1(L_28);
		HCAnimatedBanner_t2921968851 * L_29 = __this->get_U24this_5();
		NullCheck(L_29);
		List_1_t926430637 * L_30 = L_29->get_bannerDataList_3();
		int32_t L_31 = __this->get_U3CindexU3E__0_0();
		NullCheck(L_30);
		AnimBannerData_t3749323191 * L_32 = List_1_get_Item_m335550220(L_30, L_31, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_32);
		int32_t L_33 = L_32->get_textureHeight_3();
		__this->set_U3CtextureHeightU3E__0_2(L_33);
		int32_t L_34 = __this->get_U3CtextureWidthU3E__0_1();
		int32_t L_35 = __this->get_U3CtextureHeightU3E__0_2();
		Texture2D_t3840446185 * L_36 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1608908294(L_36, L_34, L_35, /*hidden argument*/NULL);
		__this->set_U3CtextureU3E__0_3(L_36);
		HCAnimatedBanner_t2921968851 * L_37 = __this->get_U24this_5();
		NullCheck(L_37);
		String_t* L_38 = L_37->get_texturePath_11();
		bool L_39 = File_Exists_m3943585060(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0142;
		}
	}
	{
		goto IL_01d3;
	}

IL_0142:
	{
		int32_t L_40 = Application_get_internetReachability_m2044109931(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_0151;
		}
	}
	{
		goto IL_01d3;
	}

IL_0151:
	{
		HCAnimatedBanner_t2921968851 * L_41 = __this->get_U24this_5();
		NullCheck(L_41);
		String_t* L_42 = L_41->get_textureUrl_7();
		WWW_t3688466362 * L_43 = (WWW_t3688466362 *)il2cpp_codegen_object_new(WWW_t3688466362_il2cpp_TypeInfo_var);
		WWW__ctor_m1181807108(L_43, L_42, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__1_4(L_43);
		V_0 = ((int32_t)-3);
	}

IL_016a:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_44 = V_0;
			switch (((int32_t)((int32_t)L_44-(int32_t)1)))
			{
				case 0:
				{
					goto IL_0198;
				}
			}
		}

IL_0176:
		{
			WWW_t3688466362 * L_45 = __this->get_U3CwwwU3E__1_4();
			__this->set_U24current_6(L_45);
			bool L_46 = __this->get_U24disposing_7();
			if (L_46)
			{
				goto IL_0191;
			}
		}

IL_018a:
		{
			__this->set_U24PC_8(1);
		}

IL_0191:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x387, FINALLY_01c8);
		}

IL_0198:
		{
			WWW_t3688466362 * L_47 = __this->get_U3CwwwU3E__1_4();
			NullCheck(L_47);
			String_t* L_48 = WWW_get_error_m4112162252(L_47, /*hidden argument*/NULL);
			if (L_48)
			{
				goto IL_01c3;
			}
		}

IL_01a8:
		{
			HCAnimatedBanner_t2921968851 * L_49 = __this->get_U24this_5();
			NullCheck(L_49);
			String_t* L_50 = L_49->get_texturePath_11();
			WWW_t3688466362 * L_51 = __this->get_U3CwwwU3E__1_4();
			NullCheck(L_51);
			ByteU5BU5D_t4116647657* L_52 = WWW_get_bytes_m4004069865(L_51, /*hidden argument*/NULL);
			File_WriteAllBytes_m4252682105(NULL /*static, unused*/, L_50, L_52, /*hidden argument*/NULL);
		}

IL_01c3:
		{
			IL2CPP_LEAVE(0x1D3, FINALLY_01c8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_01c8;
	}

FINALLY_01c8:
	{ // begin finally (depth: 1)
		{
			bool L_53 = V_1;
			if (!L_53)
			{
				goto IL_01cc;
			}
		}

IL_01cb:
		{
			IL2CPP_END_FINALLY(456)
		}

IL_01cc:
		{
			U3CLoadTextureU3Ec__Iterator1_U3CU3E__Finally0_m2209114808(__this, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(456)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(456)
	{
		IL2CPP_JUMP_TBL(0x387, IL_0387)
		IL2CPP_JUMP_TBL(0x1D3, IL_01d3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_01d3:
	{
		WaitForSeconds_t1699091251 * L_54 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1898094827(L_54, (0.2f), /*hidden argument*/NULL);
		__this->set_U24current_6(L_54);
		bool L_55 = __this->get_U24disposing_7();
		if (L_55)
		{
			goto IL_01f2;
		}
	}
	{
		__this->set_U24PC_8(2);
	}

IL_01f2:
	{
		goto IL_0387;
	}

IL_01f7:
	{
		HCAnimatedBanner_t2921968851 * L_56 = __this->get_U24this_5();
		NullCheck(L_56);
		String_t* L_57 = L_56->get_texturePath_11();
		bool L_58 = File_Exists_m3943585060(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		if (L_58)
		{
			goto IL_0211;
		}
	}
	{
		goto IL_0385;
	}

IL_0211:
	{
		HCAnimatedBanner_t2921968851 * L_59 = __this->get_U24this_5();
		NullCheck(L_59);
		String_t* L_60 = L_59->get_texturePath_11();
		ByteU5BU5D_t4116647657* L_61 = File_ReadAllBytes_m1435775076(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		V_2 = L_61;
		int32_t L_62 = __this->get_U3CtextureWidthU3E__0_1();
		int32_t L_63 = __this->get_U3CtextureHeightU3E__0_2();
		Texture2D_t3840446185 * L_64 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1608908294(L_64, L_62, L_63, /*hidden argument*/NULL);
		V_3 = L_64;
		Texture2D_t3840446185 * L_65 = V_3;
		ByteU5BU5D_t4116647657* L_66 = V_2;
		NullCheck(L_65);
		bool L_67 = Texture2D_LoadImage_m1056421096(L_65, L_66, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_024c;
		}
	}
	{
		Texture2D_t3840446185 * L_68 = V_3;
		__this->set_U3CtextureU3E__0_3(L_68);
		goto IL_0251;
	}

IL_024c:
	{
		goto IL_0385;
	}

IL_0251:
	{
		HCAnimatedBanner_t2921968851 * L_69 = __this->get_U24this_5();
		Texture2D_t3840446185 * L_70 = __this->get_U3CtextureU3E__0_3();
		NullCheck(L_69);
		HCAnimatedBanner_ApplyTexture_m3563656807(L_69, L_70, /*hidden argument*/NULL);
		HCAnimatedBanner_t2921968851 * L_71 = __this->get_U24this_5();
		HCAnimatedBanner_t2921968851 * L_72 = __this->get_U24this_5();
		NullCheck(L_72);
		List_1_t926430637 * L_73 = L_72->get_bannerDataList_3();
		int32_t L_74 = __this->get_U3CindexU3E__0_0();
		NullCheck(L_73);
		AnimBannerData_t3749323191 * L_75 = List_1_get_Item_m335550220(L_73, L_74, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_75);
		String_t* L_76 = L_75->get_transitionUrlIOS_9();
		NullCheck(L_71);
		HCAnimatedBanner_set_transitionLink_m3657221653(L_71, L_76, /*hidden argument*/NULL);
		HCAnimatedBanner_t2921968851 * L_77 = __this->get_U24this_5();
		HCAnimatedBanner_t2921968851 * L_78 = __this->get_U24this_5();
		int32_t L_79 = __this->get_U3CindexU3E__0_0();
		NullCheck(L_78);
		Il2CppObject * L_80 = HCAnimatedBanner_AnimateBanner_m3687925943(L_78, L_79, /*hidden argument*/NULL);
		NullCheck(L_77);
		MonoBehaviour_StartCoroutine_m4001331470(L_77, L_80, /*hidden argument*/NULL);
		HCAnimatedBanner_t2921968851 * L_81 = __this->get_U24this_5();
		HCAnimatedBanner_t2921968851 * L_82 = __this->get_U24this_5();
		NullCheck(L_82);
		List_1_t926430637 * L_83 = L_82->get_bannerDataList_3();
		int32_t L_84 = __this->get_U3CindexU3E__0_0();
		NullCheck(L_83);
		AnimBannerData_t3749323191 * L_85 = List_1_get_Item_m335550220(L_83, L_84, /*hidden argument*/List_1_get_Item_m335550220_MethodInfo_var);
		NullCheck(L_85);
		double L_86 = L_85->get_waitForNextAnimSec_10();
		NullCheck(L_81);
		L_81->set_count_12(L_86);
		HCAnimatedBanner_t2921968851 * L_87 = __this->get_U24this_5();
		NullCheck(L_87);
		double L_88 = L_87->get_count_12();
		WaitForSeconds_t1699091251 * L_89 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1898094827(L_89, (((float)((float)L_88))), /*hidden argument*/NULL);
		__this->set_U24current_6(L_89);
		bool L_90 = __this->get_U24disposing_7();
		if (L_90)
		{
			goto IL_02f1;
		}
	}
	{
		__this->set_U24PC_8(3);
	}

IL_02f1:
	{
		goto IL_0387;
	}

IL_02f6:
	{
		HCAnimatedBanner_t2921968851 * L_91 = __this->get_U24this_5();
		HCAnimatedBanner_t2921968851 * L_92 = __this->get_U24this_5();
		int32_t L_93 = __this->get_U3CindexU3E__0_0();
		NullCheck(L_92);
		Il2CppObject * L_94 = HCAnimatedBanner_AnimateBanner_m3687925943(L_92, L_93, /*hidden argument*/NULL);
		NullCheck(L_91);
		MonoBehaviour_StopCoroutine_m1038915083(L_91, L_94, /*hidden argument*/NULL);
		int32_t L_95 = __this->get_U3CindexU3E__0_0();
		int32_t L_96 = ((int32_t)((int32_t)L_95+(int32_t)1));
		V_4 = L_96;
		__this->set_U3CindexU3E__0_0(L_96);
		int32_t L_97 = V_4;
		HCAnimatedBanner_t2921968851 * L_98 = __this->get_U24this_5();
		NullCheck(L_98);
		List_1_t926430637 * L_99 = L_98->get_bannerDataList_3();
		NullCheck(L_99);
		int32_t L_100 = List_1_get_Count_m3914099077(L_99, /*hidden argument*/List_1_get_Count_m3914099077_MethodInfo_var);
		if ((((int32_t)L_97) < ((int32_t)L_100)))
		{
			goto IL_0341;
		}
	}
	{
		__this->set_U3CindexU3E__0_0(0);
	}

IL_0341:
	{
		int32_t L_101 = __this->get_U3CindexU3E__0_0();
		PlayerPrefs_SetInt_m1898875631(NULL /*static, unused*/, _stringLiteral3196920305, L_101, /*hidden argument*/NULL);
		HCAnimatedBanner_t2921968851 * L_102 = __this->get_U24this_5();
		NullCheck(L_102);
		List_1_t926430637 * L_103 = L_102->get_bannerDataList_3();
		NullCheck(L_103);
		int32_t L_104 = List_1_get_Count_m3914099077(L_103, /*hidden argument*/List_1_get_Count_m3914099077_MethodInfo_var);
		if ((((int32_t)L_104) < ((int32_t)2)))
		{
			goto IL_037e;
		}
	}
	{
		HCAnimatedBanner_t2921968851 * L_105 = __this->get_U24this_5();
		HCAnimatedBanner_t2921968851 * L_106 = __this->get_U24this_5();
		NullCheck(L_106);
		Il2CppObject * L_107 = HCAnimatedBanner_LoadTexture_m946452470(L_106, /*hidden argument*/NULL);
		NullCheck(L_105);
		MonoBehaviour_StartCoroutine_m4001331470(L_105, L_107, /*hidden argument*/NULL);
	}

IL_037e:
	{
		__this->set_U24PC_8((-1));
	}

IL_0385:
	{
		return (bool)0;
	}

IL_0387:
	{
		return (bool)1;
	}
}
// System.Object HCAnimatedBanner/<LoadTexture>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadTextureU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1650094239 (U3CLoadTextureU3Ec__Iterator1_t1051594053 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object HCAnimatedBanner/<LoadTexture>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadTextureU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1738875527 (U3CLoadTextureU3Ec__Iterator1_t1051594053 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Void HCAnimatedBanner/<LoadTexture>c__Iterator1::Dispose()
extern "C"  void U3CLoadTextureU3Ec__Iterator1_Dispose_m1000524212 (U3CLoadTextureU3Ec__Iterator1_t1051594053 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24disposing_7((bool)1);
		__this->set_U24PC_8((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003c;
			}
			case 1:
			{
				goto IL_0030;
			}
			case 2:
			{
				goto IL_003c;
			}
			case 3:
			{
				goto IL_003c;
			}
		}
	}
	{
		goto IL_003c;
	}

IL_0030:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3C, FINALLY_0035);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		U3CLoadTextureU3Ec__Iterator1_U3CU3E__Finally0_m2209114808(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_003c:
	{
		return;
	}
}
// System.Void HCAnimatedBanner/<LoadTexture>c__Iterator1::Reset()
extern "C"  void U3CLoadTextureU3Ec__Iterator1_Reset_m3618031411 (U3CLoadTextureU3Ec__Iterator1_t1051594053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadTextureU3Ec__Iterator1_Reset_m3618031411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void HCAnimatedBanner/<LoadTexture>c__Iterator1::<>__Finally0()
extern "C"  void U3CLoadTextureU3Ec__Iterator1_U3CU3E__Finally0_m2209114808 (U3CLoadTextureU3Ec__Iterator1_t1051594053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadTextureU3Ec__Iterator1_U3CU3E__Finally0_m2209114808_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WWW_t3688466362 * L_0 = __this->get_U3CwwwU3E__1_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		WWW_t3688466362 * L_1 = __this->get_U3CwwwU3E__1_4();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void HCAnimatedBanner/AnimBannerData::.ctor()
extern "C"  void AnimBannerData__ctor_m1479738887 (AnimBannerData_t3749323191 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IpadAspectRation::.ctor()
extern "C"  void IpadAspectRation__ctor_m2308842874 (IpadAspectRation_t3315943056 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IpadAspectRation::Start()
extern "C"  void IpadAspectRation_Start_m3895859990 (IpadAspectRation_t3315943056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IpadAspectRation_Start_m3895859990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = SystemInfo_get_deviceModel_m1299396108(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = String_Contains_m1147431944(L_0, _stringLiteral435220204, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		RectTransform_t3704657025 * L_2 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_MethodInfo_var);
		Vector2_t2156229523  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m4060800441(&L_3, (0.8f), (0.8f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Vector2_op_Implicit_m1988559315(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_localScale_m2795501682(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void MenuScreen::.ctor()
extern "C"  void MenuScreen__ctor_m1470552830 (MenuScreen_t742580752 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuScreen::Start()
extern "C"  void MenuScreen_Start_m2494847976 (MenuScreen_t742580752 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuScreen_Start_m2494847976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Bounds_t2266837910  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m102368104(L_0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_2 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m2921103810(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m102368104(L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = Vector3_op_Subtraction_m2566684344(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_z_3();
		V_0 = L_6;
		Camera_t4157153871 * L_7 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = V_0;
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m1197556204(&L_9, (0.0f), (0.0f), L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_10 = Camera_ViewportToWorldPoint_m3480887959(L_7, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = (&V_2)->get_x_1();
		__this->set_leftX_3(L_11);
		Camera_t4157153871 * L_12 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = V_0;
		Vector3_t3722313464  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m1197556204(&L_14, (1.0f), (0.0f), L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t3722313464  L_15 = Camera_ViewportToWorldPoint_m3480887959(L_12, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = (&V_3)->get_x_1();
		__this->set_rightX_5(L_16);
		Camera_t4157153871 * L_17 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = V_0;
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m1197556204(&L_19, (0.0f), (0.0f), ((-L_18)), /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t3722313464  L_20 = Camera_ViewportToWorldPoint_m3480887959(L_17, L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		float L_21 = (&V_4)->get_y_2();
		__this->set_topY_4(L_21);
		Camera_t4157153871 * L_22 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = V_0;
		Vector3_t3722313464  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m1197556204(&L_24, (1.0f), (0.0f), L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t3722313464  L_25 = Camera_ViewportToWorldPoint_m3480887959(L_22, L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		float L_26 = (&V_5)->get_y_2();
		__this->set_bottomY_2(L_26);
		GameObject_t1113636619 * L_27 = __this->get_groundPrefab_6();
		NullCheck(L_27);
		Renderer_t2627027031 * L_28 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_27, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_28);
		Bounds_t2266837910  L_29 = Renderer_get_bounds_m2197226282(L_28, /*hidden argument*/NULL);
		V_6 = L_29;
		Vector3_t3722313464  L_30 = Bounds_get_size_m1171376090((&V_6), /*hidden argument*/NULL);
		V_7 = L_30;
		float L_31 = (&V_7)->get_y_2();
		__this->set_groundTileH_7(L_31);
		return;
	}
}
// System.Void MenuScreen::Update()
extern "C"  void MenuScreen_Update_m1428068712 (MenuScreen_t742580752 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuScreen_Update_m1428068712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m2296100099(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Application_Quit_m1672997886(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void MenuScreen::.cctor()
extern "C"  void MenuScreen__cctor_m3203852085 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuScreen__cctor_m3203852085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((MenuScreen_t742580752_StaticFields*)MenuScreen_t742580752_il2cpp_TypeInfo_var->static_fields)->set_isSoundOn_8((bool)1);
		return;
	}
}
// System.Void MovingBall::.ctor()
extern "C"  void MovingBall__ctor_m1150672482 (MovingBall_t2489706458 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MovingBall::Start()
extern "C"  void MovingBall_Start_m2127453236 (MovingBall_t2489706458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MovingBall_Start_m2127453236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m3458517891(NULL /*static, unused*/, _stringLiteral209761000, /*hidden argument*/NULL);
		__this->set_soccerBall_7(L_0);
		__this->set_sign_2((1.0f));
		__this->set_rotateSpeed_5((int16_t)((int32_t)-1000));
		__this->set_nowTime_3(0);
		return;
	}
}
// System.Void MovingBall::Update()
extern "C"  void MovingBall_Update_m513137904 (MovingBall_t2489706458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MovingBall_Update_m513137904_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_soccerBall_7();
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = GameObject_get_transform_m393750976(L_0, /*hidden argument*/NULL);
		int16_t L_2 = __this->get_rotateSpeed_5();
		float L_3 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m1197556204(&L_4, (0.0f), (0.0f), ((float)((float)(((float)((float)L_2)))*(float)L_3)), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Rotate_m915426478(L_1, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameScreen_t1800529819_il2cpp_TypeInfo_var);
		bool L_5 = ((GameScreen_t1800529819_StaticFields*)GameScreen_t1800529819_il2cpp_TypeInfo_var->static_fields)->get_paused_15();
		if (L_5)
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		bool L_6 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_isDead_7();
		if (L_6)
		{
			goto IL_0094;
		}
	}
	{
		MovingBall_MoveBall_m4217469841(__this, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_nowTime_3();
		__this->set_nowTime_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = __this->get_nowTime_3();
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2284871897, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m1780991845(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		int32_t L_12 = __this->get_nowTime_3();
		if ((((int32_t)L_12) <= ((int32_t)((int32_t)15))))
		{
			goto IL_0094;
		}
	}
	{
		float L_13 = __this->get_sign_2();
		__this->set_sign_2(((float)((float)L_13*(float)(-1.0f))));
		__this->set_nowTime_3(0);
	}

IL_0094:
	{
		return;
	}
}
// System.Void MovingBall::MoveBall()
extern "C"  void MovingBall_MoveBall_m4217469841 (MovingBall_t2489706458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MovingBall_MoveBall_m4217469841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_1 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_localPosition_m265057664(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp_m500974738(NULL /*static, unused*/, L_3, (0.3f), (1.3f), /*hidden argument*/NULL);
		Vector2_t2156229523  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m4060800441(&L_5, L_4, (-1.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector2_op_Implicit_m1988559315(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localPosition_m3327877514(L_0, L_6, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		float L_8 = __this->get_moveSpeed_6();
		float L_9 = __this->get_sign_2();
		float L_10 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_Translate_m2342484445(L_7, ((float)((float)((float)((float)L_8*(float)L_9))*(float)L_10)), (0.0f), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void MovingBg::.ctor()
extern "C"  void MovingBg__ctor_m3955013136 (MovingBg_t2796125617 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MovingBg::Start()
extern "C"  void MovingBg_Start_m608075872 (MovingBg_t2796125617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MovingBg_Start_m608075872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Bounds_t2266837910  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t3722313464  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t3722313464  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m102368104(L_0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_2 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m2921103810(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m102368104(L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = Vector3_op_Subtraction_m2566684344(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_z_3();
		V_0 = L_6;
		Camera_t4157153871 * L_7 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = V_0;
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m1197556204(&L_9, (0.0f), (0.0f), L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_10 = Camera_ViewportToWorldPoint_m3480887959(L_7, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = (&V_2)->get_x_1();
		__this->set_leftX_4(L_11);
		Camera_t4157153871 * L_12 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = V_0;
		Vector3_t3722313464  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m1197556204(&L_14, (1.0f), (0.0f), L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t3722313464  L_15 = Camera_ViewportToWorldPoint_m3480887959(L_12, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = (&V_3)->get_x_1();
		__this->set_rightX_6(L_16);
		Camera_t4157153871 * L_17 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = V_0;
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m1197556204(&L_19, (0.0f), (0.0f), ((-L_18)), /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t3722313464  L_20 = Camera_ViewportToWorldPoint_m3480887959(L_17, L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		float L_21 = (&V_4)->get_y_2();
		__this->set_topY_5(L_21);
		Camera_t4157153871 * L_22 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = V_0;
		Vector3_t3722313464  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m1197556204(&L_24, (1.0f), (0.0f), L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t3722313464  L_25 = Camera_ViewportToWorldPoint_m3480887959(L_22, L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		float L_26 = (&V_5)->get_y_2();
		__this->set_bottomY_3(L_26);
		GameObject_t1113636619 * L_27 = __this->get_tilePrefab_2();
		NullCheck(L_27);
		Renderer_t2627027031 * L_28 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_27, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		NullCheck(L_28);
		Bounds_t2266837910  L_29 = Renderer_get_bounds_m2197226282(L_28, /*hidden argument*/NULL);
		V_6 = L_29;
		Vector3_t3722313464  L_30 = Bounds_get_size_m1171376090((&V_6), /*hidden argument*/NULL);
		V_7 = L_30;
		float L_31 = (&V_7)->get_x_1();
		__this->set_groundTileW_7(L_31);
		__this->set_groundTileH_8((0.98f));
		GameObject_t1113636619 * L_32 = __this->get_tilePrefab_2();
		float L_33 = __this->get_leftX_4();
		float L_34 = __this->get_groundTileH_8();
		Vector3_t3722313464  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector3__ctor_m1197556204(&L_35, L_33, L_34, (0.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_36 = Quaternion_get_identity_m3578010038(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_37 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_32, L_35, L_36, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_MethodInfo_var);
		__this->set_block1_9(L_37);
		GameObject_t1113636619 * L_38 = __this->get_tilePrefab_2();
		GameObject_t1113636619 * L_39 = __this->get_block1_9();
		NullCheck(L_39);
		Transform_t3600365921 * L_40 = GameObject_get_transform_m393750976(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t3722313464  L_41 = Transform_get_position_m102368104(L_40, /*hidden argument*/NULL);
		V_8 = L_41;
		float L_42 = (&V_8)->get_x_1();
		float L_43 = __this->get_groundTileW_7();
		float L_44 = __this->get_groundTileH_8();
		Vector3_t3722313464  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Vector3__ctor_m1197556204(&L_45, ((float)((float)((float)((float)L_42+(float)L_43))-(float)(0.01f))), L_44, (0.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_46 = Quaternion_get_identity_m3578010038(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_47 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_38, L_45, L_46, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_MethodInfo_var);
		__this->set_block2_10(L_47);
		GameObject_t1113636619 * L_48 = __this->get_tilePrefab_2();
		GameObject_t1113636619 * L_49 = __this->get_block2_10();
		NullCheck(L_49);
		Transform_t3600365921 * L_50 = GameObject_get_transform_m393750976(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		Vector3_t3722313464  L_51 = Transform_get_position_m102368104(L_50, /*hidden argument*/NULL);
		V_9 = L_51;
		float L_52 = (&V_9)->get_x_1();
		float L_53 = __this->get_groundTileW_7();
		float L_54 = __this->get_groundTileH_8();
		Vector3_t3722313464  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Vector3__ctor_m1197556204(&L_55, ((float)((float)((float)((float)L_52+(float)L_53))-(float)(0.01f))), L_54, (0.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_56 = Quaternion_get_identity_m3578010038(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_57 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_48, L_55, L_56, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_MethodInfo_var);
		__this->set_block3_11(L_57);
		GameObject_t1113636619 * L_58 = __this->get_tilePrefab_2();
		GameObject_t1113636619 * L_59 = __this->get_block3_11();
		NullCheck(L_59);
		Transform_t3600365921 * L_60 = GameObject_get_transform_m393750976(L_59, /*hidden argument*/NULL);
		NullCheck(L_60);
		Vector3_t3722313464  L_61 = Transform_get_position_m102368104(L_60, /*hidden argument*/NULL);
		V_10 = L_61;
		float L_62 = (&V_10)->get_x_1();
		float L_63 = __this->get_groundTileW_7();
		float L_64 = __this->get_groundTileH_8();
		Vector3_t3722313464  L_65;
		memset(&L_65, 0, sizeof(L_65));
		Vector3__ctor_m1197556204(&L_65, ((float)((float)((float)((float)L_62+(float)L_63))-(float)(0.01f))), L_64, (0.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_66 = Quaternion_get_identity_m3578010038(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_67 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_58, L_65, L_66, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_MethodInfo_var);
		__this->set_block4_12(L_67);
		GameObject_t1113636619 * L_68 = __this->get_tilePrefab_2();
		GameObject_t1113636619 * L_69 = __this->get_block4_12();
		NullCheck(L_69);
		Transform_t3600365921 * L_70 = GameObject_get_transform_m393750976(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		Vector3_t3722313464  L_71 = Transform_get_position_m102368104(L_70, /*hidden argument*/NULL);
		V_11 = L_71;
		float L_72 = (&V_11)->get_x_1();
		float L_73 = __this->get_groundTileW_7();
		float L_74 = __this->get_groundTileH_8();
		Vector3_t3722313464  L_75;
		memset(&L_75, 0, sizeof(L_75));
		Vector3__ctor_m1197556204(&L_75, ((float)((float)((float)((float)L_72+(float)L_73))-(float)(0.01f))), L_74, (0.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_76 = Quaternion_get_identity_m3578010038(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_77 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_68, L_75, L_76, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_MethodInfo_var);
		__this->set_block5_13(L_77);
		return;
	}
}
// System.Void MovingBg::Update()
extern "C"  void MovingBg_Update_m3027059532 (MovingBg_t2796125617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MovingBg_Update_m3027059532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t3722313464  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t3722313464  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t3722313464  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t3722313464  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t3722313464  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t3722313464  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t3722313464  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector3_t3722313464  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t3722313464  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t3722313464  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Vector3_t3722313464  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector3_t3722313464  V_21;
	memset(&V_21, 0, sizeof(V_21));
	Vector3_t3722313464  V_22;
	memset(&V_22, 0, sizeof(V_22));
	Vector3_t3722313464  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Vector3_t3722313464  V_24;
	memset(&V_24, 0, sizeof(V_24));
	Vector3_t3722313464  V_25;
	memset(&V_25, 0, sizeof(V_25));
	{
		Vector3__ctor_m1197556204((&V_0), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = __this->get_block1_9();
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = GameObject_get_transform_m393750976(L_0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_block1_9();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m393750976(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m102368104(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_x_1();
		float L_6 = __this->get_groundTileH_8();
		GameObject_t1113636619 * L_7 = __this->get_block1_9();
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = GameObject_get_transform_m393750976(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m102368104(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_z_3();
		Vector3_t3722313464  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m1197556204(&L_11, L_5, L_6, L_10, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m2361854178(L_1, L_11, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_12 = __this->get_block2_10();
		NullCheck(L_12);
		Transform_t3600365921 * L_13 = GameObject_get_transform_m393750976(L_12, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_14 = __this->get_block2_10();
		NullCheck(L_14);
		Transform_t3600365921 * L_15 = GameObject_get_transform_m393750976(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t3722313464  L_16 = Transform_get_position_m102368104(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		float L_17 = (&V_3)->get_x_1();
		float L_18 = __this->get_groundTileH_8();
		GameObject_t1113636619 * L_19 = __this->get_block2_10();
		NullCheck(L_19);
		Transform_t3600365921 * L_20 = GameObject_get_transform_m393750976(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t3722313464  L_21 = Transform_get_position_m102368104(L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		float L_22 = (&V_4)->get_z_3();
		Vector3_t3722313464  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector3__ctor_m1197556204(&L_23, L_17, L_18, L_22, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_position_m2361854178(L_13, L_23, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_24 = __this->get_block3_11();
		NullCheck(L_24);
		Transform_t3600365921 * L_25 = GameObject_get_transform_m393750976(L_24, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_26 = __this->get_block3_11();
		NullCheck(L_26);
		Transform_t3600365921 * L_27 = GameObject_get_transform_m393750976(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t3722313464  L_28 = Transform_get_position_m102368104(L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		float L_29 = (&V_5)->get_x_1();
		float L_30 = __this->get_groundTileH_8();
		GameObject_t1113636619 * L_31 = __this->get_block3_11();
		NullCheck(L_31);
		Transform_t3600365921 * L_32 = GameObject_get_transform_m393750976(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t3722313464  L_33 = Transform_get_position_m102368104(L_32, /*hidden argument*/NULL);
		V_6 = L_33;
		float L_34 = (&V_6)->get_z_3();
		Vector3_t3722313464  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector3__ctor_m1197556204(&L_35, L_29, L_30, L_34, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_position_m2361854178(L_25, L_35, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_36 = __this->get_block4_12();
		NullCheck(L_36);
		Transform_t3600365921 * L_37 = GameObject_get_transform_m393750976(L_36, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_38 = __this->get_block4_12();
		NullCheck(L_38);
		Transform_t3600365921 * L_39 = GameObject_get_transform_m393750976(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t3722313464  L_40 = Transform_get_position_m102368104(L_39, /*hidden argument*/NULL);
		V_7 = L_40;
		float L_41 = (&V_7)->get_x_1();
		float L_42 = __this->get_groundTileH_8();
		GameObject_t1113636619 * L_43 = __this->get_block4_12();
		NullCheck(L_43);
		Transform_t3600365921 * L_44 = GameObject_get_transform_m393750976(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_t3722313464  L_45 = Transform_get_position_m102368104(L_44, /*hidden argument*/NULL);
		V_8 = L_45;
		float L_46 = (&V_8)->get_z_3();
		Vector3_t3722313464  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector3__ctor_m1197556204(&L_47, L_41, L_42, L_46, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_set_position_m2361854178(L_37, L_47, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_48 = __this->get_block5_13();
		NullCheck(L_48);
		Transform_t3600365921 * L_49 = GameObject_get_transform_m393750976(L_48, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_50 = __this->get_block5_13();
		NullCheck(L_50);
		Transform_t3600365921 * L_51 = GameObject_get_transform_m393750976(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Vector3_t3722313464  L_52 = Transform_get_position_m102368104(L_51, /*hidden argument*/NULL);
		V_9 = L_52;
		float L_53 = (&V_9)->get_x_1();
		float L_54 = __this->get_groundTileH_8();
		GameObject_t1113636619 * L_55 = __this->get_block5_13();
		NullCheck(L_55);
		Transform_t3600365921 * L_56 = GameObject_get_transform_m393750976(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		Vector3_t3722313464  L_57 = Transform_get_position_m102368104(L_56, /*hidden argument*/NULL);
		V_10 = L_57;
		float L_58 = (&V_10)->get_z_3();
		Vector3_t3722313464  L_59;
		memset(&L_59, 0, sizeof(L_59));
		Vector3__ctor_m1197556204(&L_59, L_53, L_54, L_58, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_set_position_m2361854178(L_49, L_59, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_60 = __this->get_block1_9();
		NullCheck(L_60);
		Transform_t3600365921 * L_61 = GameObject_get_transform_m393750976(L_60, /*hidden argument*/NULL);
		Vector3_t3722313464  L_62 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayerControl_t1977005294_il2cpp_TypeInfo_var);
		float L_63 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_speed_9();
		Vector3_t3722313464  L_64 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_62, L_63, /*hidden argument*/NULL);
		float L_65 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_66 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_Translate_m3317641446(L_61, L_66, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_67 = __this->get_block2_10();
		NullCheck(L_67);
		Transform_t3600365921 * L_68 = GameObject_get_transform_m393750976(L_67, /*hidden argument*/NULL);
		Vector3_t3722313464  L_69 = V_0;
		float L_70 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_speed_9();
		Vector3_t3722313464  L_71 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_69, L_70, /*hidden argument*/NULL);
		float L_72 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_73 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_71, L_72, /*hidden argument*/NULL);
		NullCheck(L_68);
		Transform_Translate_m3317641446(L_68, L_73, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_74 = __this->get_block3_11();
		NullCheck(L_74);
		Transform_t3600365921 * L_75 = GameObject_get_transform_m393750976(L_74, /*hidden argument*/NULL);
		Vector3_t3722313464  L_76 = V_0;
		float L_77 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_speed_9();
		Vector3_t3722313464  L_78 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_76, L_77, /*hidden argument*/NULL);
		float L_79 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_80 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_78, L_79, /*hidden argument*/NULL);
		NullCheck(L_75);
		Transform_Translate_m3317641446(L_75, L_80, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_81 = __this->get_block4_12();
		NullCheck(L_81);
		Transform_t3600365921 * L_82 = GameObject_get_transform_m393750976(L_81, /*hidden argument*/NULL);
		Vector3_t3722313464  L_83 = V_0;
		float L_84 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_speed_9();
		Vector3_t3722313464  L_85 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_83, L_84, /*hidden argument*/NULL);
		float L_86 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_87 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_85, L_86, /*hidden argument*/NULL);
		NullCheck(L_82);
		Transform_Translate_m3317641446(L_82, L_87, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_88 = __this->get_block5_13();
		NullCheck(L_88);
		Transform_t3600365921 * L_89 = GameObject_get_transform_m393750976(L_88, /*hidden argument*/NULL);
		Vector3_t3722313464  L_90 = V_0;
		float L_91 = ((PlayerControl_t1977005294_StaticFields*)PlayerControl_t1977005294_il2cpp_TypeInfo_var->static_fields)->get_speed_9();
		Vector3_t3722313464  L_92 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_90, L_91, /*hidden argument*/NULL);
		float L_93 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_94 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_92, L_93, /*hidden argument*/NULL);
		NullCheck(L_89);
		Transform_Translate_m3317641446(L_89, L_94, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_95 = __this->get_block1_9();
		NullCheck(L_95);
		Transform_t3600365921 * L_96 = GameObject_get_transform_m393750976(L_95, /*hidden argument*/NULL);
		NullCheck(L_96);
		Vector3_t3722313464  L_97 = Transform_get_position_m102368104(L_96, /*hidden argument*/NULL);
		V_11 = L_97;
		float L_98 = (&V_11)->get_x_1();
		float L_99 = __this->get_groundTileW_7();
		float L_100 = __this->get_leftX_4();
		if ((!(((float)((float)((float)L_98+(float)L_99))) < ((float)L_100))))
		{
			goto IL_02d1;
		}
	}
	{
		GameObject_t1113636619 * L_101 = __this->get_block1_9();
		NullCheck(L_101);
		Transform_t3600365921 * L_102 = GameObject_get_transform_m393750976(L_101, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_103 = __this->get_block5_13();
		NullCheck(L_103);
		Transform_t3600365921 * L_104 = GameObject_get_transform_m393750976(L_103, /*hidden argument*/NULL);
		NullCheck(L_104);
		Vector3_t3722313464  L_105 = Transform_get_position_m102368104(L_104, /*hidden argument*/NULL);
		V_12 = L_105;
		float L_106 = (&V_12)->get_x_1();
		float L_107 = __this->get_groundTileW_7();
		GameObject_t1113636619 * L_108 = __this->get_block1_9();
		NullCheck(L_108);
		Transform_t3600365921 * L_109 = GameObject_get_transform_m393750976(L_108, /*hidden argument*/NULL);
		NullCheck(L_109);
		Vector3_t3722313464  L_110 = Transform_get_position_m102368104(L_109, /*hidden argument*/NULL);
		V_13 = L_110;
		float L_111 = (&V_13)->get_y_2();
		Vector3_t3722313464  L_112;
		memset(&L_112, 0, sizeof(L_112));
		Vector3__ctor_m1197556204(&L_112, ((float)((float)((float)((float)L_106+(float)L_107))-(float)(0.01f))), L_111, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_102);
		Transform_set_position_m2361854178(L_102, L_112, /*hidden argument*/NULL);
	}

IL_02d1:
	{
		GameObject_t1113636619 * L_113 = __this->get_block2_10();
		NullCheck(L_113);
		Transform_t3600365921 * L_114 = GameObject_get_transform_m393750976(L_113, /*hidden argument*/NULL);
		NullCheck(L_114);
		Vector3_t3722313464  L_115 = Transform_get_position_m102368104(L_114, /*hidden argument*/NULL);
		V_14 = L_115;
		float L_116 = (&V_14)->get_x_1();
		float L_117 = __this->get_groundTileW_7();
		float L_118 = __this->get_leftX_4();
		if ((!(((float)((float)((float)L_116+(float)L_117))) < ((float)L_118))))
		{
			goto IL_0355;
		}
	}
	{
		GameObject_t1113636619 * L_119 = __this->get_block2_10();
		NullCheck(L_119);
		Transform_t3600365921 * L_120 = GameObject_get_transform_m393750976(L_119, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_121 = __this->get_block1_9();
		NullCheck(L_121);
		Transform_t3600365921 * L_122 = GameObject_get_transform_m393750976(L_121, /*hidden argument*/NULL);
		NullCheck(L_122);
		Vector3_t3722313464  L_123 = Transform_get_position_m102368104(L_122, /*hidden argument*/NULL);
		V_15 = L_123;
		float L_124 = (&V_15)->get_x_1();
		float L_125 = __this->get_groundTileW_7();
		GameObject_t1113636619 * L_126 = __this->get_block2_10();
		NullCheck(L_126);
		Transform_t3600365921 * L_127 = GameObject_get_transform_m393750976(L_126, /*hidden argument*/NULL);
		NullCheck(L_127);
		Vector3_t3722313464  L_128 = Transform_get_position_m102368104(L_127, /*hidden argument*/NULL);
		V_16 = L_128;
		float L_129 = (&V_16)->get_y_2();
		Vector3_t3722313464  L_130;
		memset(&L_130, 0, sizeof(L_130));
		Vector3__ctor_m1197556204(&L_130, ((float)((float)((float)((float)L_124+(float)L_125))-(float)(0.01f))), L_129, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_120);
		Transform_set_position_m2361854178(L_120, L_130, /*hidden argument*/NULL);
	}

IL_0355:
	{
		GameObject_t1113636619 * L_131 = __this->get_block3_11();
		NullCheck(L_131);
		Transform_t3600365921 * L_132 = GameObject_get_transform_m393750976(L_131, /*hidden argument*/NULL);
		NullCheck(L_132);
		Vector3_t3722313464  L_133 = Transform_get_position_m102368104(L_132, /*hidden argument*/NULL);
		V_17 = L_133;
		float L_134 = (&V_17)->get_x_1();
		float L_135 = __this->get_groundTileW_7();
		float L_136 = __this->get_leftX_4();
		if ((!(((float)((float)((float)L_134+(float)L_135))) < ((float)L_136))))
		{
			goto IL_03d9;
		}
	}
	{
		GameObject_t1113636619 * L_137 = __this->get_block3_11();
		NullCheck(L_137);
		Transform_t3600365921 * L_138 = GameObject_get_transform_m393750976(L_137, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_139 = __this->get_block2_10();
		NullCheck(L_139);
		Transform_t3600365921 * L_140 = GameObject_get_transform_m393750976(L_139, /*hidden argument*/NULL);
		NullCheck(L_140);
		Vector3_t3722313464  L_141 = Transform_get_position_m102368104(L_140, /*hidden argument*/NULL);
		V_18 = L_141;
		float L_142 = (&V_18)->get_x_1();
		float L_143 = __this->get_groundTileW_7();
		GameObject_t1113636619 * L_144 = __this->get_block3_11();
		NullCheck(L_144);
		Transform_t3600365921 * L_145 = GameObject_get_transform_m393750976(L_144, /*hidden argument*/NULL);
		NullCheck(L_145);
		Vector3_t3722313464  L_146 = Transform_get_position_m102368104(L_145, /*hidden argument*/NULL);
		V_19 = L_146;
		float L_147 = (&V_19)->get_y_2();
		Vector3_t3722313464  L_148;
		memset(&L_148, 0, sizeof(L_148));
		Vector3__ctor_m1197556204(&L_148, ((float)((float)((float)((float)L_142+(float)L_143))-(float)(0.01f))), L_147, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_138);
		Transform_set_position_m2361854178(L_138, L_148, /*hidden argument*/NULL);
	}

IL_03d9:
	{
		GameObject_t1113636619 * L_149 = __this->get_block4_12();
		NullCheck(L_149);
		Transform_t3600365921 * L_150 = GameObject_get_transform_m393750976(L_149, /*hidden argument*/NULL);
		NullCheck(L_150);
		Vector3_t3722313464  L_151 = Transform_get_position_m102368104(L_150, /*hidden argument*/NULL);
		V_20 = L_151;
		float L_152 = (&V_20)->get_x_1();
		float L_153 = __this->get_groundTileW_7();
		float L_154 = __this->get_leftX_4();
		if ((!(((float)((float)((float)L_152+(float)L_153))) < ((float)L_154))))
		{
			goto IL_045d;
		}
	}
	{
		GameObject_t1113636619 * L_155 = __this->get_block4_12();
		NullCheck(L_155);
		Transform_t3600365921 * L_156 = GameObject_get_transform_m393750976(L_155, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_157 = __this->get_block3_11();
		NullCheck(L_157);
		Transform_t3600365921 * L_158 = GameObject_get_transform_m393750976(L_157, /*hidden argument*/NULL);
		NullCheck(L_158);
		Vector3_t3722313464  L_159 = Transform_get_position_m102368104(L_158, /*hidden argument*/NULL);
		V_21 = L_159;
		float L_160 = (&V_21)->get_x_1();
		float L_161 = __this->get_groundTileW_7();
		GameObject_t1113636619 * L_162 = __this->get_block4_12();
		NullCheck(L_162);
		Transform_t3600365921 * L_163 = GameObject_get_transform_m393750976(L_162, /*hidden argument*/NULL);
		NullCheck(L_163);
		Vector3_t3722313464  L_164 = Transform_get_position_m102368104(L_163, /*hidden argument*/NULL);
		V_22 = L_164;
		float L_165 = (&V_22)->get_y_2();
		Vector3_t3722313464  L_166;
		memset(&L_166, 0, sizeof(L_166));
		Vector3__ctor_m1197556204(&L_166, ((float)((float)((float)((float)L_160+(float)L_161))-(float)(0.01f))), L_165, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_156);
		Transform_set_position_m2361854178(L_156, L_166, /*hidden argument*/NULL);
	}

IL_045d:
	{
		GameObject_t1113636619 * L_167 = __this->get_block5_13();
		NullCheck(L_167);
		Transform_t3600365921 * L_168 = GameObject_get_transform_m393750976(L_167, /*hidden argument*/NULL);
		NullCheck(L_168);
		Vector3_t3722313464  L_169 = Transform_get_position_m102368104(L_168, /*hidden argument*/NULL);
		V_23 = L_169;
		float L_170 = (&V_23)->get_x_1();
		float L_171 = __this->get_groundTileW_7();
		float L_172 = __this->get_leftX_4();
		if ((!(((float)((float)((float)L_170+(float)L_171))) < ((float)L_172))))
		{
			goto IL_04e1;
		}
	}
	{
		GameObject_t1113636619 * L_173 = __this->get_block5_13();
		NullCheck(L_173);
		Transform_t3600365921 * L_174 = GameObject_get_transform_m393750976(L_173, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_175 = __this->get_block4_12();
		NullCheck(L_175);
		Transform_t3600365921 * L_176 = GameObject_get_transform_m393750976(L_175, /*hidden argument*/NULL);
		NullCheck(L_176);
		Vector3_t3722313464  L_177 = Transform_get_position_m102368104(L_176, /*hidden argument*/NULL);
		V_24 = L_177;
		float L_178 = (&V_24)->get_x_1();
		float L_179 = __this->get_groundTileW_7();
		GameObject_t1113636619 * L_180 = __this->get_block5_13();
		NullCheck(L_180);
		Transform_t3600365921 * L_181 = GameObject_get_transform_m393750976(L_180, /*hidden argument*/NULL);
		NullCheck(L_181);
		Vector3_t3722313464  L_182 = Transform_get_position_m102368104(L_181, /*hidden argument*/NULL);
		V_25 = L_182;
		float L_183 = (&V_25)->get_y_2();
		Vector3_t3722313464  L_184;
		memset(&L_184, 0, sizeof(L_184));
		Vector3__ctor_m1197556204(&L_184, ((float)((float)((float)((float)L_178+(float)L_179))-(float)(0.01f))), L_183, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_174);
		Transform_set_position_m2361854178(L_174, L_184, /*hidden argument*/NULL);
	}

IL_04e1:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::.ctor()
extern "C"  void NendAdFullBoard__ctor_m310133544 (NendAdFullBoard_t270881991 * __this, const MethodInfo* method)
{
	{
		Color_t2555686324  L_0 = Color_get_black_m650597609(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_backgroundColor_8(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::add_AdLoaded(NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardLoaded)
extern "C"  void NendAdFullBoard_add_AdLoaded_m3684095882 (NendAdFullBoard_t270881991 * __this, NendAdFullBoardLoaded_t3830347218 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_add_AdLoaded_m3684095882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdFullBoardLoaded_t3830347218 * V_0 = NULL;
	NendAdFullBoardLoaded_t3830347218 * V_1 = NULL;
	{
		NendAdFullBoardLoaded_t3830347218 * L_0 = __this->get_AdLoaded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdFullBoardLoaded_t3830347218 * L_1 = V_0;
		V_1 = L_1;
		NendAdFullBoardLoaded_t3830347218 ** L_2 = __this->get_address_of_AdLoaded_0();
		NendAdFullBoardLoaded_t3830347218 * L_3 = V_1;
		NendAdFullBoardLoaded_t3830347218 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdFullBoardLoaded_t3830347218 * L_6 = V_0;
		NendAdFullBoardLoaded_t3830347218 * L_7 = InterlockedCompareExchangeImpl<NendAdFullBoardLoaded_t3830347218 *>(L_2, ((NendAdFullBoardLoaded_t3830347218 *)CastclassSealed(L_5, NendAdFullBoardLoaded_t3830347218_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdFullBoardLoaded_t3830347218 * L_8 = V_0;
		NendAdFullBoardLoaded_t3830347218 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdFullBoardLoaded_t3830347218 *)L_8) == ((Il2CppObject*)(NendAdFullBoardLoaded_t3830347218 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::remove_AdLoaded(NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardLoaded)
extern "C"  void NendAdFullBoard_remove_AdLoaded_m3648953114 (NendAdFullBoard_t270881991 * __this, NendAdFullBoardLoaded_t3830347218 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_remove_AdLoaded_m3648953114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdFullBoardLoaded_t3830347218 * V_0 = NULL;
	NendAdFullBoardLoaded_t3830347218 * V_1 = NULL;
	{
		NendAdFullBoardLoaded_t3830347218 * L_0 = __this->get_AdLoaded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdFullBoardLoaded_t3830347218 * L_1 = V_0;
		V_1 = L_1;
		NendAdFullBoardLoaded_t3830347218 ** L_2 = __this->get_address_of_AdLoaded_0();
		NendAdFullBoardLoaded_t3830347218 * L_3 = V_1;
		NendAdFullBoardLoaded_t3830347218 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdFullBoardLoaded_t3830347218 * L_6 = V_0;
		NendAdFullBoardLoaded_t3830347218 * L_7 = InterlockedCompareExchangeImpl<NendAdFullBoardLoaded_t3830347218 *>(L_2, ((NendAdFullBoardLoaded_t3830347218 *)CastclassSealed(L_5, NendAdFullBoardLoaded_t3830347218_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdFullBoardLoaded_t3830347218 * L_8 = V_0;
		NendAdFullBoardLoaded_t3830347218 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdFullBoardLoaded_t3830347218 *)L_8) == ((Il2CppObject*)(NendAdFullBoardLoaded_t3830347218 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::add_AdFailedToLoad(NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardFailedToLoad)
extern "C"  void NendAdFullBoard_add_AdFailedToLoad_m1862710807 (NendAdFullBoard_t270881991 * __this, NendAdFullBoardFailedToLoad_t4155484538 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_add_AdFailedToLoad_m1862710807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdFullBoardFailedToLoad_t4155484538 * V_0 = NULL;
	NendAdFullBoardFailedToLoad_t4155484538 * V_1 = NULL;
	{
		NendAdFullBoardFailedToLoad_t4155484538 * L_0 = __this->get_AdFailedToLoad_1();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdFullBoardFailedToLoad_t4155484538 * L_1 = V_0;
		V_1 = L_1;
		NendAdFullBoardFailedToLoad_t4155484538 ** L_2 = __this->get_address_of_AdFailedToLoad_1();
		NendAdFullBoardFailedToLoad_t4155484538 * L_3 = V_1;
		NendAdFullBoardFailedToLoad_t4155484538 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdFullBoardFailedToLoad_t4155484538 * L_6 = V_0;
		NendAdFullBoardFailedToLoad_t4155484538 * L_7 = InterlockedCompareExchangeImpl<NendAdFullBoardFailedToLoad_t4155484538 *>(L_2, ((NendAdFullBoardFailedToLoad_t4155484538 *)CastclassSealed(L_5, NendAdFullBoardFailedToLoad_t4155484538_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdFullBoardFailedToLoad_t4155484538 * L_8 = V_0;
		NendAdFullBoardFailedToLoad_t4155484538 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdFullBoardFailedToLoad_t4155484538 *)L_8) == ((Il2CppObject*)(NendAdFullBoardFailedToLoad_t4155484538 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::remove_AdFailedToLoad(NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardFailedToLoad)
extern "C"  void NendAdFullBoard_remove_AdFailedToLoad_m3107845605 (NendAdFullBoard_t270881991 * __this, NendAdFullBoardFailedToLoad_t4155484538 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_remove_AdFailedToLoad_m3107845605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdFullBoardFailedToLoad_t4155484538 * V_0 = NULL;
	NendAdFullBoardFailedToLoad_t4155484538 * V_1 = NULL;
	{
		NendAdFullBoardFailedToLoad_t4155484538 * L_0 = __this->get_AdFailedToLoad_1();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdFullBoardFailedToLoad_t4155484538 * L_1 = V_0;
		V_1 = L_1;
		NendAdFullBoardFailedToLoad_t4155484538 ** L_2 = __this->get_address_of_AdFailedToLoad_1();
		NendAdFullBoardFailedToLoad_t4155484538 * L_3 = V_1;
		NendAdFullBoardFailedToLoad_t4155484538 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdFullBoardFailedToLoad_t4155484538 * L_6 = V_0;
		NendAdFullBoardFailedToLoad_t4155484538 * L_7 = InterlockedCompareExchangeImpl<NendAdFullBoardFailedToLoad_t4155484538 *>(L_2, ((NendAdFullBoardFailedToLoad_t4155484538 *)CastclassSealed(L_5, NendAdFullBoardFailedToLoad_t4155484538_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdFullBoardFailedToLoad_t4155484538 * L_8 = V_0;
		NendAdFullBoardFailedToLoad_t4155484538 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdFullBoardFailedToLoad_t4155484538 *)L_8) == ((Il2CppObject*)(NendAdFullBoardFailedToLoad_t4155484538 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::add_AdShown(NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardShown)
extern "C"  void NendAdFullBoard_add_AdShown_m388710219 (NendAdFullBoard_t270881991 * __this, NendAdFullBoardShown_t3059274368 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_add_AdShown_m388710219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdFullBoardShown_t3059274368 * V_0 = NULL;
	NendAdFullBoardShown_t3059274368 * V_1 = NULL;
	{
		NendAdFullBoardShown_t3059274368 * L_0 = __this->get_AdShown_2();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdFullBoardShown_t3059274368 * L_1 = V_0;
		V_1 = L_1;
		NendAdFullBoardShown_t3059274368 ** L_2 = __this->get_address_of_AdShown_2();
		NendAdFullBoardShown_t3059274368 * L_3 = V_1;
		NendAdFullBoardShown_t3059274368 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdFullBoardShown_t3059274368 * L_6 = V_0;
		NendAdFullBoardShown_t3059274368 * L_7 = InterlockedCompareExchangeImpl<NendAdFullBoardShown_t3059274368 *>(L_2, ((NendAdFullBoardShown_t3059274368 *)CastclassSealed(L_5, NendAdFullBoardShown_t3059274368_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdFullBoardShown_t3059274368 * L_8 = V_0;
		NendAdFullBoardShown_t3059274368 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdFullBoardShown_t3059274368 *)L_8) == ((Il2CppObject*)(NendAdFullBoardShown_t3059274368 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::remove_AdShown(NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardShown)
extern "C"  void NendAdFullBoard_remove_AdShown_m2186095239 (NendAdFullBoard_t270881991 * __this, NendAdFullBoardShown_t3059274368 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_remove_AdShown_m2186095239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdFullBoardShown_t3059274368 * V_0 = NULL;
	NendAdFullBoardShown_t3059274368 * V_1 = NULL;
	{
		NendAdFullBoardShown_t3059274368 * L_0 = __this->get_AdShown_2();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdFullBoardShown_t3059274368 * L_1 = V_0;
		V_1 = L_1;
		NendAdFullBoardShown_t3059274368 ** L_2 = __this->get_address_of_AdShown_2();
		NendAdFullBoardShown_t3059274368 * L_3 = V_1;
		NendAdFullBoardShown_t3059274368 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdFullBoardShown_t3059274368 * L_6 = V_0;
		NendAdFullBoardShown_t3059274368 * L_7 = InterlockedCompareExchangeImpl<NendAdFullBoardShown_t3059274368 *>(L_2, ((NendAdFullBoardShown_t3059274368 *)CastclassSealed(L_5, NendAdFullBoardShown_t3059274368_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdFullBoardShown_t3059274368 * L_8 = V_0;
		NendAdFullBoardShown_t3059274368 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdFullBoardShown_t3059274368 *)L_8) == ((Il2CppObject*)(NendAdFullBoardShown_t3059274368 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::add_AdClicked(NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardClick)
extern "C"  void NendAdFullBoard_add_AdClicked_m4163517606 (NendAdFullBoard_t270881991 * __this, NendAdFullBoardClick_t3385596145 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_add_AdClicked_m4163517606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdFullBoardClick_t3385596145 * V_0 = NULL;
	NendAdFullBoardClick_t3385596145 * V_1 = NULL;
	{
		NendAdFullBoardClick_t3385596145 * L_0 = __this->get_AdClicked_3();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdFullBoardClick_t3385596145 * L_1 = V_0;
		V_1 = L_1;
		NendAdFullBoardClick_t3385596145 ** L_2 = __this->get_address_of_AdClicked_3();
		NendAdFullBoardClick_t3385596145 * L_3 = V_1;
		NendAdFullBoardClick_t3385596145 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdFullBoardClick_t3385596145 * L_6 = V_0;
		NendAdFullBoardClick_t3385596145 * L_7 = InterlockedCompareExchangeImpl<NendAdFullBoardClick_t3385596145 *>(L_2, ((NendAdFullBoardClick_t3385596145 *)CastclassSealed(L_5, NendAdFullBoardClick_t3385596145_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdFullBoardClick_t3385596145 * L_8 = V_0;
		NendAdFullBoardClick_t3385596145 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdFullBoardClick_t3385596145 *)L_8) == ((Il2CppObject*)(NendAdFullBoardClick_t3385596145 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::remove_AdClicked(NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardClick)
extern "C"  void NendAdFullBoard_remove_AdClicked_m1933847800 (NendAdFullBoard_t270881991 * __this, NendAdFullBoardClick_t3385596145 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_remove_AdClicked_m1933847800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdFullBoardClick_t3385596145 * V_0 = NULL;
	NendAdFullBoardClick_t3385596145 * V_1 = NULL;
	{
		NendAdFullBoardClick_t3385596145 * L_0 = __this->get_AdClicked_3();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdFullBoardClick_t3385596145 * L_1 = V_0;
		V_1 = L_1;
		NendAdFullBoardClick_t3385596145 ** L_2 = __this->get_address_of_AdClicked_3();
		NendAdFullBoardClick_t3385596145 * L_3 = V_1;
		NendAdFullBoardClick_t3385596145 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdFullBoardClick_t3385596145 * L_6 = V_0;
		NendAdFullBoardClick_t3385596145 * L_7 = InterlockedCompareExchangeImpl<NendAdFullBoardClick_t3385596145 *>(L_2, ((NendAdFullBoardClick_t3385596145 *)CastclassSealed(L_5, NendAdFullBoardClick_t3385596145_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdFullBoardClick_t3385596145 * L_8 = V_0;
		NendAdFullBoardClick_t3385596145 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdFullBoardClick_t3385596145 *)L_8) == ((Il2CppObject*)(NendAdFullBoardClick_t3385596145 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::add_AdDismissed(NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardDismiss)
extern "C"  void NendAdFullBoard_add_AdDismissed_m2257091698 (NendAdFullBoard_t270881991 * __this, NendAdFullBoardDismiss_t136425400 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_add_AdDismissed_m2257091698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdFullBoardDismiss_t136425400 * V_0 = NULL;
	NendAdFullBoardDismiss_t136425400 * V_1 = NULL;
	{
		NendAdFullBoardDismiss_t136425400 * L_0 = __this->get_AdDismissed_4();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdFullBoardDismiss_t136425400 * L_1 = V_0;
		V_1 = L_1;
		NendAdFullBoardDismiss_t136425400 ** L_2 = __this->get_address_of_AdDismissed_4();
		NendAdFullBoardDismiss_t136425400 * L_3 = V_1;
		NendAdFullBoardDismiss_t136425400 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdFullBoardDismiss_t136425400 * L_6 = V_0;
		NendAdFullBoardDismiss_t136425400 * L_7 = InterlockedCompareExchangeImpl<NendAdFullBoardDismiss_t136425400 *>(L_2, ((NendAdFullBoardDismiss_t136425400 *)CastclassSealed(L_5, NendAdFullBoardDismiss_t136425400_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdFullBoardDismiss_t136425400 * L_8 = V_0;
		NendAdFullBoardDismiss_t136425400 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdFullBoardDismiss_t136425400 *)L_8) == ((Il2CppObject*)(NendAdFullBoardDismiss_t136425400 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::remove_AdDismissed(NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardDismiss)
extern "C"  void NendAdFullBoard_remove_AdDismissed_m899205779 (NendAdFullBoard_t270881991 * __this, NendAdFullBoardDismiss_t136425400 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_remove_AdDismissed_m899205779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdFullBoardDismiss_t136425400 * V_0 = NULL;
	NendAdFullBoardDismiss_t136425400 * V_1 = NULL;
	{
		NendAdFullBoardDismiss_t136425400 * L_0 = __this->get_AdDismissed_4();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdFullBoardDismiss_t136425400 * L_1 = V_0;
		V_1 = L_1;
		NendAdFullBoardDismiss_t136425400 ** L_2 = __this->get_address_of_AdDismissed_4();
		NendAdFullBoardDismiss_t136425400 * L_3 = V_1;
		NendAdFullBoardDismiss_t136425400 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdFullBoardDismiss_t136425400 * L_6 = V_0;
		NendAdFullBoardDismiss_t136425400 * L_7 = InterlockedCompareExchangeImpl<NendAdFullBoardDismiss_t136425400 *>(L_2, ((NendAdFullBoardDismiss_t136425400 *)CastclassSealed(L_5, NendAdFullBoardDismiss_t136425400_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdFullBoardDismiss_t136425400 * L_8 = V_0;
		NendAdFullBoardDismiss_t136425400 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdFullBoardDismiss_t136425400 *)L_8) == ((Il2CppObject*)(NendAdFullBoardDismiss_t136425400 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::CallBack(NendUnityPlugin.AD.FullBoard.NendAdFullBoard/FullBoardAdCallbackType)
extern "C"  void NendAdFullBoard_CallBack_m3737565486 (NendAdFullBoard_t270881991 * __this, int32_t ___type0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___type0;
		switch (((int32_t)((int32_t)L_0-(int32_t)4)))
		{
			case 0:
			{
				goto IL_0049;
			}
			case 1:
			{
				goto IL_0065;
			}
			case 2:
			{
				goto IL_0081;
			}
		}
	}
	{
		int32_t L_1 = ___type0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		goto IL_00a4;
	}

IL_001f:
	{
		__this->set_m_isLoadSuccess_7((bool)1);
		__this->set_m_isLoading_5((bool)0);
		NendAdFullBoardLoaded_t3830347218 * L_2 = __this->get_AdLoaded_0();
		if (!L_2)
		{
			goto IL_0044;
		}
	}
	{
		NendAdFullBoardLoaded_t3830347218 * L_3 = __this->get_AdLoaded_0();
		NullCheck(L_3);
		NendAdFullBoardLoaded_Invoke_m2966266618(L_3, __this, /*hidden argument*/NULL);
	}

IL_0044:
	{
		goto IL_00cf;
	}

IL_0049:
	{
		NendAdFullBoardShown_t3059274368 * L_4 = __this->get_AdShown_2();
		if (!L_4)
		{
			goto IL_0060;
		}
	}
	{
		NendAdFullBoardShown_t3059274368 * L_5 = __this->get_AdShown_2();
		NullCheck(L_5);
		NendAdFullBoardShown_Invoke_m1454147902(L_5, __this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		goto IL_00cf;
	}

IL_0065:
	{
		NendAdFullBoardClick_t3385596145 * L_6 = __this->get_AdClicked_3();
		if (!L_6)
		{
			goto IL_007c;
		}
	}
	{
		NendAdFullBoardClick_t3385596145 * L_7 = __this->get_AdClicked_3();
		NullCheck(L_7);
		NendAdFullBoardClick_Invoke_m949622431(L_7, __this, /*hidden argument*/NULL);
	}

IL_007c:
	{
		goto IL_00cf;
	}

IL_0081:
	{
		__this->set_m_isShowing_6((bool)0);
		NendAdFullBoardDismiss_t136425400 * L_8 = __this->get_AdDismissed_4();
		if (!L_8)
		{
			goto IL_009f;
		}
	}
	{
		NendAdFullBoardDismiss_t136425400 * L_9 = __this->get_AdDismissed_4();
		NullCheck(L_9);
		NendAdFullBoardDismiss_Invoke_m481757421(L_9, __this, /*hidden argument*/NULL);
	}

IL_009f:
	{
		goto IL_00cf;
	}

IL_00a4:
	{
		__this->set_m_isLoadSuccess_7((bool)0);
		__this->set_m_isLoading_5((bool)0);
		NendAdFullBoardFailedToLoad_t4155484538 * L_10 = __this->get_AdFailedToLoad_1();
		if (!L_10)
		{
			goto IL_00ca;
		}
	}
	{
		NendAdFullBoardFailedToLoad_t4155484538 * L_11 = __this->get_AdFailedToLoad_1();
		int32_t L_12 = ___type0;
		NullCheck(L_11);
		NendAdFullBoardFailedToLoad_Invoke_m3650888263(L_11, __this, L_12, /*hidden argument*/NULL);
	}

IL_00ca:
	{
		goto IL_00cf;
	}

IL_00cf:
	{
		return;
	}
}
// NendUnityPlugin.AD.FullBoard.NendAdFullBoard NendUnityPlugin.AD.FullBoard.NendAdFullBoard::NewFullBoardAd(System.String,System.String)
extern "C"  NendAdFullBoard_t270881991 * NendAdFullBoard_NewFullBoardAd_m1504519230 (Il2CppObject * __this /* static, unused */, String_t* ___spotId0, String_t* ___apiKey1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_NewFullBoardAd_m1504519230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___spotId0;
		String_t* L_1 = ___apiKey1;
		IOSFullBoardAd_t1375839176 * L_2 = (IOSFullBoardAd_t1375839176 *)il2cpp_codegen_object_new(IOSFullBoardAd_t1375839176_il2cpp_TypeInfo_var);
		IOSFullBoardAd__ctor_m3519029639(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::Load()
extern "C"  void NendAdFullBoard_Load_m3769100623 (NendAdFullBoard_t270881991 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_Load_m3769100623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_isLoading_5();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_W_m3326785758(NULL /*static, unused*/, _stringLiteral3606670000, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		VirtActionInvoker0::Invoke(5 /* System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::LoadInternal() */, __this);
		__this->set_m_isLoading_5((bool)1);
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::Show()
extern "C"  void NendAdFullBoard_Show_m3946874812 (NendAdFullBoard_t270881991 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoard_Show_m3946874812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_isShowing_6();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_W_m3326785758(NULL /*static, unused*/, _stringLiteral1566380239, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		bool L_1 = __this->get_m_isLoadSuccess_7();
		if (L_1)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_W_m3326785758(NULL /*static, unused*/, _stringLiteral1681923921, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}

IL_0038:
	{
		VirtActionInvoker0::Invoke(6 /* System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::ShowInternal() */, __this);
		__this->set_m_isShowing_6((bool)1);
		return;
	}
}
// UnityEngine.Color NendUnityPlugin.AD.FullBoard.NendAdFullBoard::get_IOSBackgroundColor()
extern "C"  Color_t2555686324  NendAdFullBoard_get_IOSBackgroundColor_m4097822977 (NendAdFullBoard_t270881991 * __this, const MethodInfo* method)
{
	{
		Color_t2555686324  L_0 = __this->get_backgroundColor_8();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard::set_IOSBackgroundColor(UnityEngine.Color)
extern "C"  void NendAdFullBoard_set_IOSBackgroundColor_m2085493828 (NendAdFullBoard_t270881991 * __this, Color_t2555686324  ___value0, const MethodInfo* method)
{
	{
		Color_t2555686324  L_0 = ___value0;
		__this->set_backgroundColor_8(L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardClick::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdFullBoardClick__ctor_m980673301 (NendAdFullBoardClick_t3385596145 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardClick::Invoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard)
extern "C"  void NendAdFullBoardClick_Invoke_m949622431 (NendAdFullBoardClick_t3385596145 * __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdFullBoardClick_Invoke_m949622431((NendAdFullBoardClick_t3385596145 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardClick::BeginInvoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdFullBoardClick_BeginInvoke_m3586765996 (NendAdFullBoardClick_t3385596145 * __this, NendAdFullBoard_t270881991 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardClick::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdFullBoardClick_EndInvoke_m3316987818 (NendAdFullBoardClick_t3385596145 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardDismiss::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdFullBoardDismiss__ctor_m3800636117 (NendAdFullBoardDismiss_t136425400 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardDismiss::Invoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard)
extern "C"  void NendAdFullBoardDismiss_Invoke_m481757421 (NendAdFullBoardDismiss_t136425400 * __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdFullBoardDismiss_Invoke_m481757421((NendAdFullBoardDismiss_t136425400 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardDismiss::BeginInvoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdFullBoardDismiss_BeginInvoke_m2451768576 (NendAdFullBoardDismiss_t136425400 * __this, NendAdFullBoard_t270881991 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardDismiss::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdFullBoardDismiss_EndInvoke_m3946175027 (NendAdFullBoardDismiss_t136425400 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardFailedToLoad::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdFullBoardFailedToLoad__ctor_m3020649667 (NendAdFullBoardFailedToLoad_t4155484538 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardFailedToLoad::Invoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard,NendUnityPlugin.AD.FullBoard.NendAdFullBoard/FullBoardAdErrorType)
extern "C"  void NendAdFullBoardFailedToLoad_Invoke_m3650888263 (NendAdFullBoardFailedToLoad_t4155484538 * __this, NendAdFullBoard_t270881991 * ___instance0, int32_t ___error1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdFullBoardFailedToLoad_Invoke_m3650888263((NendAdFullBoardFailedToLoad_t4155484538 *)__this->get_prev_9(),___instance0, ___error1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdFullBoard_t270881991 * ___instance0, int32_t ___error1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0, ___error1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdFullBoard_t270881991 * ___instance0, int32_t ___error1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0, ___error1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___error1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0, ___error1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardFailedToLoad::BeginInvoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard,NendUnityPlugin.AD.FullBoard.NendAdFullBoard/FullBoardAdErrorType,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdFullBoardFailedToLoad_BeginInvoke_m2707068486 (NendAdFullBoardFailedToLoad_t4155484538 * __this, NendAdFullBoard_t270881991 * ___instance0, int32_t ___error1, AsyncCallback_t3962456242 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdFullBoardFailedToLoad_BeginInvoke_m2707068486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___instance0;
	__d_args[1] = Box(FullBoardAdErrorType_t1510350271_il2cpp_TypeInfo_var, &___error1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardFailedToLoad::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdFullBoardFailedToLoad_EndInvoke_m558334276 (NendAdFullBoardFailedToLoad_t4155484538 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardLoaded::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdFullBoardLoaded__ctor_m1754823751 (NendAdFullBoardLoaded_t3830347218 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardLoaded::Invoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard)
extern "C"  void NendAdFullBoardLoaded_Invoke_m2966266618 (NendAdFullBoardLoaded_t3830347218 * __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdFullBoardLoaded_Invoke_m2966266618((NendAdFullBoardLoaded_t3830347218 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardLoaded::BeginInvoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdFullBoardLoaded_BeginInvoke_m233529145 (NendAdFullBoardLoaded_t3830347218 * __this, NendAdFullBoard_t270881991 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardLoaded::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdFullBoardLoaded_EndInvoke_m3532017627 (NendAdFullBoardLoaded_t3830347218 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardShown::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdFullBoardShown__ctor_m3778148041 (NendAdFullBoardShown_t3059274368 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardShown::Invoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard)
extern "C"  void NendAdFullBoardShown_Invoke_m1454147902 (NendAdFullBoardShown_t3059274368 * __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdFullBoardShown_Invoke_m1454147902((NendAdFullBoardShown_t3059274368 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdFullBoard_t270881991 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardShown::BeginInvoke(NendUnityPlugin.AD.FullBoard.NendAdFullBoard,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdFullBoardShown_BeginInvoke_m3664617990 (NendAdFullBoardShown_t3059274368 * __this, NendAdFullBoard_t270881991 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.FullBoard.NendAdFullBoard/NendAdFullBoardShown::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdFullBoardShown_EndInvoke_m2255462382 (NendAdFullBoardShown_t3059274368 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Native.Events.NativeAdViewEvent::.ctor()
extern "C"  void NativeAdViewEvent__ctor_m3216875244 (NativeAdViewEvent_t610464567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAdViewEvent__ctor_m3216875244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m1367642006(__this, /*hidden argument*/UnityEvent_1__ctor_m1367642006_MethodInfo_var);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Events.NativeAdViewFailedToLoadEvent::.ctor()
extern "C"  void NativeAdViewFailedToLoadEvent__ctor_m2356888314 (NativeAdViewFailedToLoadEvent_t3307153462 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAdViewFailedToLoadEvent__ctor_m2356888314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_3__ctor_m2373477420(__this, /*hidden argument*/UnityEvent_3__ctor_m2373477420_MethodInfo_var);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandler::.ctor()
extern "C"  void ClickHandler__ctor_m2579410340 (ClickHandler_t29098297 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandler::CallbackClick()
extern "C"  void ClickHandler_CallbackClick_m4050748451 (ClickHandler_t29098297 * __this, const MethodInfo* method)
{
	{
		Action_t1264377477 * L_0 = __this->get_OnClick_2();
		NullCheck(L_0);
		Action_Invoke_m937035532(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandler::OnDestroy()
extern "C"  void ClickHandler_OnDestroy_m115998400 (ClickHandler_t29098297 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClickHandler_OnDestroy_m115998400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m1414505214(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral23062073, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandler2D::.ctor()
extern "C"  void ClickHandler2D__ctor_m1951010670 (ClickHandler2D_t2622036411 * __this, const MethodInfo* method)
{
	{
		ClickHandlerGO__ctor_m389639094(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.Handlers.ClickHandler2D::IsClicked(UnityEngine.Vector3)
extern "C"  bool ClickHandler2D_IsClicked_m2801462155 (ClickHandler2D_t2622036411 * __this, Vector3_t3722313464  ___position0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClickHandler2D_IsClicked_m2801462155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t3785851493  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t2279581989  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_t4157153871 * L_0 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = ___position0;
		NullCheck(L_0);
		Ray_t3785851493  L_2 = Camera_ScreenPointToRay_m1522780915(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3722313464  L_3 = Ray_get_origin_m4290253200((&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_4 = Vector2_op_Implicit_m1304503157(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = Ray_get_direction_m1991692996((&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_6 = Vector2_op_Implicit_m1304503157(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_7 = Physics2D_Raycast_m4254966674(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		Collider2D_t2806799626 * L_9 = RaycastHit2D_get_collider_m1860250292((&V_1), /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_t1113636619 * L_10 = Component_get_gameObject_m2648350745(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandler3D::.ctor()
extern "C"  void ClickHandler3D__ctor_m2095386478 (ClickHandler3D_t283384251 * __this, const MethodInfo* method)
{
	{
		ClickHandlerGO__ctor_m389639094(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.Handlers.ClickHandler3D::IsClicked(UnityEngine.Vector3)
extern "C"  bool ClickHandler3D_IsClicked_m4226707129 (ClickHandler3D_t283384251 * __this, Vector3_t3722313464  ___position0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClickHandler3D_IsClicked_m4226707129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t3785851493  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit_t1056001966  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_t4157153871 * L_0 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = ___position0;
		NullCheck(L_0);
		Ray_t3785851493  L_2 = Camera_ScreenPointToRay_m1522780915(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Initobj (RaycastHit_t1056001966_il2cpp_TypeInfo_var, (&V_1));
		Ray_t3785851493  L_3 = V_0;
		bool L_4 = Physics_Raycast_m2500195530(NULL /*static, unused*/, L_3, (&V_1), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		Collider_t1773347010 * L_6 = RaycastHit_get_collider_m1442240336((&V_1), /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m2648350745(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0044;
		}
	}
	{
		return (bool)1;
	}

IL_0044:
	{
		return (bool)0;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandlerGO::.ctor()
extern "C"  void ClickHandlerGO__ctor_m389639094 (ClickHandlerGO_t2934446534 * __this, const MethodInfo* method)
{
	{
		ClickHandler__ctor_m2579410340(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandlerGO::Update()
extern "C"  void ClickHandlerGO_Update_m2258281450 (ClickHandlerGO_t2934446534 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClickHandlerGO_Update_m2258281450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonUp_m3875270823(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Input_get_mousePosition_m1579656956(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t3722313464  L_2 = V_0;
		bool L_3 = ClickHandlerGO_IsPointerOverUIObject_m204886395(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		return;
	}

IL_001d:
	{
		Vector3_t3722313464  L_4 = V_0;
		bool L_5 = VirtFuncInvoker1< bool, Vector3_t3722313464  >::Invoke(4 /* System.Boolean NendUnityPlugin.AD.Native.Handlers.ClickHandlerGO::IsClicked(UnityEngine.Vector3) */, __this, L_4);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		ClickHandler_CallbackClick_m4050748451(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.Handlers.ClickHandlerGO::IsPointerOverUIObject(UnityEngine.Vector3)
extern "C"  bool ClickHandlerGO_IsPointerOverUIObject_m204886395 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___position0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClickHandlerGO_IsPointerOverUIObject_m204886395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PointerEventData_t3807901092 * V_0 = NULL;
	List_1_t537414295 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t1003666588_il2cpp_TypeInfo_var);
		EventSystem_t1003666588 * L_0 = EventSystem_get_current_m1416377559(NULL /*static, unused*/, /*hidden argument*/NULL);
		PointerEventData_t3807901092 * L_1 = (PointerEventData_t3807901092 *)il2cpp_codegen_object_new(PointerEventData_t3807901092_il2cpp_TypeInfo_var);
		PointerEventData__ctor_m2263609344(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		PointerEventData_t3807901092 * L_2 = V_0;
		Vector3_t3722313464  L_3 = ___position0;
		Vector2_t2156229523  L_4 = Vector2_op_Implicit_m1304503157(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PointerEventData_set_position_m2060457995(L_2, L_4, /*hidden argument*/NULL);
		List_1_t537414295 * L_5 = (List_1_t537414295 *)il2cpp_codegen_object_new(List_1_t537414295_il2cpp_TypeInfo_var);
		List_1__ctor_m2049947431(L_5, /*hidden argument*/List_1__ctor_m2049947431_MethodInfo_var);
		V_1 = L_5;
		EventSystem_t1003666588 * L_6 = EventSystem_get_current_m1416377559(NULL /*static, unused*/, /*hidden argument*/NULL);
		PointerEventData_t3807901092 * L_7 = V_0;
		List_1_t537414295 * L_8 = V_1;
		NullCheck(L_6);
		EventSystem_RaycastAll_m523788254(L_6, L_7, L_8, /*hidden argument*/NULL);
		List_1_t537414295 * L_9 = V_1;
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m4207101203(L_9, /*hidden argument*/List_1_get_Count_m4207101203_MethodInfo_var);
		return (bool)((((int32_t)0) < ((int32_t)L_10))? 1 : 0);
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandlerUI::.ctor()
extern "C"  void ClickHandlerUI__ctor_m84383312 (ClickHandlerUI_t258087360 * __this, const MethodInfo* method)
{
	{
		ClickHandler__ctor_m2579410340(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ClickHandlerUI::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ClickHandlerUI_OnPointerClick_m1953313998 (ClickHandlerUI_t258087360 * __this, PointerEventData_t3807901092 * ___eventData0, const MethodInfo* method)
{
	{
		ClickHandler_CallbackClick_m4050748451(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandler::.ctor()
extern "C"  void ImpressionHandler__ctor_m2314060538 (ImpressionHandler_t4278309981 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandler::Update()
extern "C"  void ImpressionHandler_Update_m3823443372 (ImpressionHandler_t4278309981 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_TimeElapsed_4();
		float L_1 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_TimeElapsed_4(((float)((float)L_0+(float)L_1)));
		float L_2 = __this->get_m_TimeElapsed_4();
		if ((!(((float)L_2) >= ((float)(1.0f)))))
		{
			goto IL_0033;
		}
	}
	{
		ImpressionHandler_CheckImpression_m1408940149(__this, /*hidden argument*/NULL);
		__this->set_m_TimeElapsed_4((0.0f));
	}

IL_0033:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandler::OnDestroy()
extern "C"  void ImpressionHandler_OnDestroy_m3250580274 (ImpressionHandler_t4278309981 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImpressionHandler_OnDestroy_m3250580274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m1414505214(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral788995291, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandler::CheckImpression()
extern "C"  void ImpressionHandler_CheckImpression_m1408940149 (ImpressionHandler_t4278309981 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImpressionHandler_CheckImpression_m1408940149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m1414505214(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_I_m2000148771(NULL /*static, unused*/, _stringLiteral408008545, L_0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_3 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3600365921 * L_4 = Component_get_transform_m2921103810(L_3, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3600365921 * L_6 = GameObject_get_transform_m393750976(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t3722313464  L_7 = Transform_get_position_m102368104(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3722313464  L_8 = Transform_InverseTransformPoint_m1254110475(L_4, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		float L_9 = (&V_0)->get_z_3();
		if ((!(((float)(0.0f)) < ((float)L_9))))
		{
			goto IL_0072;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_10 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m1414505214(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral3219225869, L_10, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_0072:
	{
		ObjectU5BU5D_t2843939325* L_13 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_14 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m1414505214(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_15);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral1204392840, L_13, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		bool L_16 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean NendUnityPlugin.AD.Native.Handlers.ImpressionHandler::IsViewable() */, __this);
		if (!L_16)
		{
			goto IL_00c5;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_17 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_18 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m1414505214(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_19);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_I_m2000148771(NULL /*static, unused*/, _stringLiteral1548563522, L_17, /*hidden argument*/NULL);
		Action_t1264377477 * L_20 = __this->get_OnImpression_5();
		NullCheck(L_20);
		Action_Invoke_m937035532(L_20, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.Handlers.ImpressionHandler::CheckViewablePercentage(NendUnityPlugin.AD.Native.Handlers.ImpressionHandler/Corner)
extern "C"  bool ImpressionHandler_CheckViewablePercentage_m3622756226 (ImpressionHandler_t4278309981 * __this, Corner_t2201492025  ___adCorner0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImpressionHandler_CheckViewablePercentage_m3622756226_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t2360479859  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	{
		Camera_t4157153871 * L_0 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rect_t2360479859  L_1 = Camera_get_pixelRect_m1345608344(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t2843939325* L_2 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)5));
		Rect_t2360479859  L_3 = V_0;
		Rect_t2360479859  L_4 = L_3;
		Il2CppObject * L_5 = Box(Rect_t2360479859_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		ObjectU5BU5D_t2843939325* L_6 = L_2;
		float L_7 = (&___adCorner0)->get_left_0();
		float L_8 = L_7;
		Il2CppObject * L_9 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t2843939325* L_10 = L_6;
		float L_11 = (&___adCorner0)->get_top_1();
		float L_12 = L_11;
		Il2CppObject * L_13 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_13);
		ObjectU5BU5D_t2843939325* L_14 = L_10;
		float L_15 = (&___adCorner0)->get_right_2();
		float L_16 = L_15;
		Il2CppObject * L_17 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_17);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_17);
		ObjectU5BU5D_t2843939325* L_18 = L_14;
		float L_19 = (&___adCorner0)->get_bottom_3();
		float L_20 = L_19;
		Il2CppObject * L_21 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_21);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral3727356580, L_18, /*hidden argument*/NULL);
		float L_22 = (&___adCorner0)->get_right_2();
		if ((((float)(0.0f)) > ((float)L_22)))
		{
			goto IL_00a8;
		}
	}
	{
		float L_23 = Rect_get_height_m977101306((&V_0), /*hidden argument*/NULL);
		float L_24 = (&___adCorner0)->get_bottom_3();
		if ((((float)L_23) < ((float)L_24)))
		{
			goto IL_00a8;
		}
	}
	{
		float L_25 = Rect_get_width_m3421965717((&V_0), /*hidden argument*/NULL);
		float L_26 = (&___adCorner0)->get_left_0();
		if ((((float)L_25) < ((float)L_26)))
		{
			goto IL_00a8;
		}
	}
	{
		float L_27 = (&___adCorner0)->get_top_1();
		if ((!(((float)(0.0f)) > ((float)L_27))))
		{
			goto IL_00c8;
		}
	}

IL_00a8:
	{
		ObjectU5BU5D_t2843939325* L_28 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_29 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		String_t* L_30 = Object_get_name_m1414505214(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_30);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_30);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral2418862726, L_28, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_00c8:
	{
		V_1 = (0.0f);
		V_2 = (0.0f);
		float L_31 = (&___adCorner0)->get_left_0();
		if ((!(((float)(0.0f)) > ((float)L_31))))
		{
			goto IL_0123;
		}
	}
	{
		float L_32 = Rect_get_width_m3421965717((&V_0), /*hidden argument*/NULL);
		float L_33 = (&___adCorner0)->get_right_2();
		if ((!(((float)L_32) < ((float)L_33))))
		{
			goto IL_0123;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_34 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_35 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_36 = Object_get_name_m1414505214(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, L_36);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_36);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral1531524721, L_34, /*hidden argument*/NULL);
		float L_37 = Rect_get_width_m3421965717((&V_0), /*hidden argument*/NULL);
		V_1 = L_37;
		goto IL_01b5;
	}

IL_0123:
	{
		float L_38 = (&___adCorner0)->get_left_0();
		if ((!(((float)(0.0f)) > ((float)L_38))))
		{
			goto IL_015f;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_39 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_40 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		String_t* L_41 = Object_get_name_m1414505214(L_40, /*hidden argument*/NULL);
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, L_41);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_41);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral3481691899, L_39, /*hidden argument*/NULL);
		float L_42 = (&___adCorner0)->get_right_2();
		V_1 = L_42;
		goto IL_01b5;
	}

IL_015f:
	{
		float L_43 = Rect_get_width_m3421965717((&V_0), /*hidden argument*/NULL);
		float L_44 = (&___adCorner0)->get_right_2();
		if ((!(((float)L_43) < ((float)L_44))))
		{
			goto IL_01a5;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_45 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_46 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		String_t* L_47 = Object_get_name_m1414505214(L_46, /*hidden argument*/NULL);
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, L_47);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_47);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral1878511479, L_45, /*hidden argument*/NULL);
		float L_48 = Rect_get_width_m3421965717((&V_0), /*hidden argument*/NULL);
		float L_49 = (&___adCorner0)->get_left_0();
		V_1 = ((float)((float)L_48-(float)L_49));
		goto IL_01b5;
	}

IL_01a5:
	{
		float L_50 = (&___adCorner0)->get_right_2();
		float L_51 = (&___adCorner0)->get_left_0();
		V_1 = ((float)((float)L_50-(float)L_51));
	}

IL_01b5:
	{
		float L_52 = (&___adCorner0)->get_bottom_3();
		if ((!(((float)(0.0f)) > ((float)L_52))))
		{
			goto IL_0204;
		}
	}
	{
		float L_53 = Rect_get_height_m977101306((&V_0), /*hidden argument*/NULL);
		float L_54 = (&___adCorner0)->get_top_1();
		if ((!(((float)L_53) < ((float)L_54))))
		{
			goto IL_0204;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_55 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_56 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		String_t* L_57 = Object_get_name_m1414505214(L_56, /*hidden argument*/NULL);
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, L_57);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_57);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral1491405942, L_55, /*hidden argument*/NULL);
		float L_58 = Rect_get_height_m977101306((&V_0), /*hidden argument*/NULL);
		V_2 = L_58;
		goto IL_0296;
	}

IL_0204:
	{
		float L_59 = (&___adCorner0)->get_bottom_3();
		if ((!(((float)(0.0f)) > ((float)L_59))))
		{
			goto IL_0240;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_60 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_61 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_61);
		String_t* L_62 = Object_get_name_m1414505214(L_61, /*hidden argument*/NULL);
		NullCheck(L_60);
		ArrayElementTypeCheck (L_60, L_62);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_62);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral2583144425, L_60, /*hidden argument*/NULL);
		float L_63 = (&___adCorner0)->get_top_1();
		V_2 = L_63;
		goto IL_0296;
	}

IL_0240:
	{
		float L_64 = Rect_get_height_m977101306((&V_0), /*hidden argument*/NULL);
		float L_65 = (&___adCorner0)->get_top_1();
		if ((!(((float)L_64) < ((float)L_65))))
		{
			goto IL_0286;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_66 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_67 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_67);
		String_t* L_68 = Object_get_name_m1414505214(L_67, /*hidden argument*/NULL);
		NullCheck(L_66);
		ArrayElementTypeCheck (L_66, L_68);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_68);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral2946073682, L_66, /*hidden argument*/NULL);
		float L_69 = Rect_get_height_m977101306((&V_0), /*hidden argument*/NULL);
		float L_70 = (&___adCorner0)->get_bottom_3();
		V_2 = ((float)((float)L_69-(float)L_70));
		goto IL_0296;
	}

IL_0286:
	{
		float L_71 = (&___adCorner0)->get_top_1();
		float L_72 = (&___adCorner0)->get_bottom_3();
		V_2 = ((float)((float)L_71-(float)L_72));
	}

IL_0296:
	{
		float L_73 = (&___adCorner0)->get_right_2();
		float L_74 = (&___adCorner0)->get_left_0();
		V_3 = ((float)((float)L_73-(float)L_74));
		float L_75 = (&___adCorner0)->get_top_1();
		float L_76 = (&___adCorner0)->get_bottom_3();
		V_4 = ((float)((float)L_75-(float)L_76));
		float L_77 = V_1;
		float L_78 = V_2;
		V_5 = ((float)((float)L_77*(float)L_78));
		float L_79 = V_3;
		float L_80 = V_4;
		V_6 = ((float)((float)L_79*(float)L_80));
		ObjectU5BU5D_t2843939325* L_81 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		float L_82 = V_5;
		float L_83 = L_82;
		Il2CppObject * L_84 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_83);
		NullCheck(L_81);
		ArrayElementTypeCheck (L_81, L_84);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_84);
		ObjectU5BU5D_t2843939325* L_85 = L_81;
		float L_86 = V_6;
		float L_87 = L_86;
		Il2CppObject * L_88 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_87);
		NullCheck(L_85);
		ArrayElementTypeCheck (L_85, L_88);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_88);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral2767064411, L_85, /*hidden argument*/NULL);
		float L_89 = V_5;
		float L_90 = V_6;
		return (bool)((((int32_t)((!(((float)L_89) >= ((float)((float)((float)L_90*(float)(0.5f))))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandler/Corner::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Corner__ctor_m3011766940 (Corner_t2201492025 * __this, float ___left0, float ___top1, float ___right2, float ___bottom3, const MethodInfo* method)
{
	{
		float L_0 = ___left0;
		__this->set_left_0(L_0);
		float L_1 = ___top1;
		__this->set_top_1(L_1);
		float L_2 = ___right2;
		__this->set_right_2(L_2);
		float L_3 = ___bottom3;
		__this->set_bottom_3(L_3);
		return;
	}
}
extern "C"  void Corner__ctor_m3011766940_AdjustorThunk (Il2CppObject * __this, float ___left0, float ___top1, float ___right2, float ___bottom3, const MethodInfo* method)
{
	Corner_t2201492025 * _thisAdjusted = reinterpret_cast<Corner_t2201492025 *>(__this + 1);
	Corner__ctor_m3011766940(_thisAdjusted, ___left0, ___top1, ___right2, ___bottom3, method);
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerGO::.ctor()
extern "C"  void ImpressionHandlerGO__ctor_m2463536597 (ImpressionHandlerGO_t188448812 * __this, const MethodInfo* method)
{
	{
		ImpressionHandler__ctor_m2314060538(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerGO::IsViewable()
extern "C"  bool ImpressionHandlerGO_IsViewable_m2408579625 (ImpressionHandlerGO_t188448812 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImpressionHandlerGO_IsViewable_m2408579625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t4157153871 * V_0 = NULL;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Bounds_t2266837910  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t2360479859  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	bool V_8 = false;
	InvalidOperationException_t56020091 * V_9 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Camera_t4157153871 * L_0 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Camera_t4157153871 * L_1 = V_0;
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m393750976(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m102368104(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_5 = Camera_WorldToScreenPoint_m491883527(L_1, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t3722313464  L_6 = V_1;
		Vector3_t3722313464  L_7 = L_6;
		Il2CppObject * L_8 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3054536847, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, L_9, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_0038:
	try
	{ // begin try (depth: 1)
		Bounds_t2266837910  L_10 = ImpressionHandlerGO_GetBounds_m2495302509(__this, /*hidden argument*/NULL);
		V_2 = L_10;
		Bounds_t2266837910  L_11 = V_2;
		Rect_t2360479859  L_12 = ImpressionHandlerGO_GetObjectRect_m1352629183(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		Rect_t2360479859  L_13 = V_3;
		Rect_t2360479859  L_14 = L_13;
		Il2CppObject * L_15 = Box(Rect_t2360479859_il2cpp_TypeInfo_var, &L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1569617349, L_15, /*hidden argument*/NULL);
		ObjectU5BU5D_t2843939325* L_17 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_18 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m1414505214(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_19);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		float L_20 = (&V_1)->get_x_1();
		float L_21 = Rect_get_width_m3421965717((&V_3), /*hidden argument*/NULL);
		V_4 = ((float)((float)L_20-(float)((float)((float)L_21/(float)(2.0f)))));
		float L_22 = (&V_1)->get_x_1();
		float L_23 = Rect_get_width_m3421965717((&V_3), /*hidden argument*/NULL);
		V_5 = ((float)((float)L_22+(float)((float)((float)L_23/(float)(2.0f)))));
		float L_24 = (&V_1)->get_y_2();
		float L_25 = Rect_get_height_m977101306((&V_3), /*hidden argument*/NULL);
		V_6 = ((float)((float)L_24+(float)((float)((float)L_25/(float)(2.0f)))));
		float L_26 = (&V_1)->get_y_2();
		float L_27 = Rect_get_height_m977101306((&V_3), /*hidden argument*/NULL);
		V_7 = ((float)((float)L_26-(float)((float)((float)L_27/(float)(2.0f)))));
		float L_28 = V_4;
		float L_29 = V_6;
		float L_30 = V_5;
		float L_31 = V_7;
		Corner_t2201492025  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Corner__ctor_m3011766940(&L_32, L_28, L_29, L_30, L_31, /*hidden argument*/NULL);
		bool L_33 = ImpressionHandler_CheckViewablePercentage_m3622756226(__this, L_32, /*hidden argument*/NULL);
		V_8 = L_33;
		goto IL_0106;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (InvalidOperationException_t56020091_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_00e5;
		throw e;
	}

CATCH_00e5:
	{ // begin catch(System.InvalidOperationException)
		V_9 = ((InvalidOperationException_t56020091 *)__exception_local);
		InvalidOperationException_t56020091 * L_34 = V_9;
		NullCheck(L_34);
		String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_34);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_W_m3326785758(NULL /*static, unused*/, L_35, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_0104;
	} // end catch (depth: 1)

IL_0104:
	{
		return (bool)0;
	}

IL_0106:
	{
		bool L_36 = V_8;
		return L_36;
	}
}
// UnityEngine.Bounds NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerGO::GetBounds()
extern "C"  Bounds_t2266837910  ImpressionHandlerGO_GetBounds_m2495302509 (ImpressionHandlerGO_t188448812 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImpressionHandlerGO_GetBounds_m2495302509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Renderer_t2627027031 * V_0 = NULL;
	Collider_t1773347010 * V_1 = NULL;
	Collider2D_t2806799626 * V_2 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t2627027031 * L_1 = GameObject_GetComponent_TisRenderer_t2627027031_m1370005186(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1370005186_MethodInfo_var);
		V_0 = L_1;
		Renderer_t2627027031 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		Renderer_t2627027031 * L_4 = V_0;
		NullCheck(L_4);
		Bounds_t2266837910  L_5 = Renderer_get_bounds_m2197226282(L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001f:
	{
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Collider_t1773347010 * L_7 = GameObject_GetComponent_TisCollider_t1773347010_m69509314(L_6, /*hidden argument*/GameObject_GetComponent_TisCollider_t1773347010_m69509314_MethodInfo_var);
		V_1 = L_7;
		Collider_t1773347010 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		Collider_t1773347010 * L_10 = V_1;
		NullCheck(L_10);
		Bounds_t2266837910  L_11 = Collider_get_bounds_m518531398(L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_003e:
	{
		GameObject_t1113636619 * L_12 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Collider2D_t2806799626 * L_13 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_12, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_2 = L_13;
		Collider2D_t2806799626 * L_14 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_005d;
		}
	}
	{
		Collider2D_t2806799626 * L_16 = V_2;
		NullCheck(L_16);
		Bounds_t2266837910  L_17 = Collider2D_get_bounds_m1523458546(L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_005d:
	{
		GameObject_t1113636619 * L_18 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m1414505214(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3582570584, L_19, _stringLiteral3450648441, /*hidden argument*/NULL);
		InvalidOperationException_t56020091 * L_21 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// UnityEngine.Rect NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerGO::GetObjectRect(UnityEngine.Bounds)
extern "C"  Rect_t2360479859  ImpressionHandlerGO_GetObjectRect_m1352629183 (Il2CppObject * __this /* static, unused */, Bounds_t2266837910  ___bounds0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImpressionHandlerGO_GetObjectRect_m1352629183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2U5BU5D_t1457185986* V_2 = NULL;
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2156229523  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2U5BU5D_t1457185986* V_6 = NULL;
	int32_t V_7 = 0;
	{
		Vector3_t3722313464  L_0 = Bounds_get_center_m1997663455((&___bounds0), /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t3722313464  L_1 = Bounds_get_extents_m3683565517((&___bounds0), /*hidden argument*/NULL);
		V_1 = L_1;
		Vector2U5BU5D_t1457185986* L_2 = ((Vector2U5BU5D_t1457185986*)SZArrayNew(Vector2U5BU5D_t1457185986_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_2);
		float L_3 = (&V_0)->get_x_1();
		float L_4 = (&V_1)->get_x_1();
		float L_5 = (&V_0)->get_y_2();
		float L_6 = (&V_1)->get_y_2();
		float L_7 = (&V_0)->get_z_3();
		float L_8 = (&V_1)->get_z_3();
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m1197556204(&L_9, ((float)((float)L_3-(float)L_4)), ((float)((float)L_5-(float)L_6)), ((float)((float)L_7-(float)L_8)), /*hidden argument*/NULL);
		Vector2_t2156229523  L_10 = ImpressionHandlerGO_WorldToScreenPoint_m1334575203(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		(*(Vector2_t2156229523 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_10;
		Vector2U5BU5D_t1457185986* L_11 = L_2;
		NullCheck(L_11);
		float L_12 = (&V_0)->get_x_1();
		float L_13 = (&V_1)->get_x_1();
		float L_14 = (&V_0)->get_y_2();
		float L_15 = (&V_1)->get_y_2();
		float L_16 = (&V_0)->get_z_3();
		float L_17 = (&V_1)->get_z_3();
		Vector3_t3722313464  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m1197556204(&L_18, ((float)((float)L_12+(float)L_13)), ((float)((float)L_14-(float)L_15)), ((float)((float)L_16-(float)L_17)), /*hidden argument*/NULL);
		Vector2_t2156229523  L_19 = ImpressionHandlerGO_WorldToScreenPoint_m1334575203(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		(*(Vector2_t2156229523 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_19;
		Vector2U5BU5D_t1457185986* L_20 = L_11;
		NullCheck(L_20);
		float L_21 = (&V_0)->get_x_1();
		float L_22 = (&V_1)->get_x_1();
		float L_23 = (&V_0)->get_y_2();
		float L_24 = (&V_1)->get_y_2();
		float L_25 = (&V_0)->get_z_3();
		float L_26 = (&V_1)->get_z_3();
		Vector3_t3722313464  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m1197556204(&L_27, ((float)((float)L_21-(float)L_22)), ((float)((float)L_23-(float)L_24)), ((float)((float)L_25+(float)L_26)), /*hidden argument*/NULL);
		Vector2_t2156229523  L_28 = ImpressionHandlerGO_WorldToScreenPoint_m1334575203(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		(*(Vector2_t2156229523 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_28;
		Vector2U5BU5D_t1457185986* L_29 = L_20;
		NullCheck(L_29);
		float L_30 = (&V_0)->get_x_1();
		float L_31 = (&V_1)->get_x_1();
		float L_32 = (&V_0)->get_y_2();
		float L_33 = (&V_1)->get_y_2();
		float L_34 = (&V_0)->get_z_3();
		float L_35 = (&V_1)->get_z_3();
		Vector3_t3722313464  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector3__ctor_m1197556204(&L_36, ((float)((float)L_30+(float)L_31)), ((float)((float)L_32-(float)L_33)), ((float)((float)L_34+(float)L_35)), /*hidden argument*/NULL);
		Vector2_t2156229523  L_37 = ImpressionHandlerGO_WorldToScreenPoint_m1334575203(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		(*(Vector2_t2156229523 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_37;
		Vector2U5BU5D_t1457185986* L_38 = L_29;
		NullCheck(L_38);
		float L_39 = (&V_0)->get_x_1();
		float L_40 = (&V_1)->get_x_1();
		float L_41 = (&V_0)->get_y_2();
		float L_42 = (&V_1)->get_y_2();
		float L_43 = (&V_0)->get_z_3();
		float L_44 = (&V_1)->get_z_3();
		Vector3_t3722313464  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Vector3__ctor_m1197556204(&L_45, ((float)((float)L_39-(float)L_40)), ((float)((float)L_41+(float)L_42)), ((float)((float)L_43-(float)L_44)), /*hidden argument*/NULL);
		Vector2_t2156229523  L_46 = ImpressionHandlerGO_WorldToScreenPoint_m1334575203(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		(*(Vector2_t2156229523 *)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))) = L_46;
		Vector2U5BU5D_t1457185986* L_47 = L_38;
		NullCheck(L_47);
		float L_48 = (&V_0)->get_x_1();
		float L_49 = (&V_1)->get_x_1();
		float L_50 = (&V_0)->get_y_2();
		float L_51 = (&V_1)->get_y_2();
		float L_52 = (&V_0)->get_z_3();
		float L_53 = (&V_1)->get_z_3();
		Vector3_t3722313464  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Vector3__ctor_m1197556204(&L_54, ((float)((float)L_48+(float)L_49)), ((float)((float)L_50+(float)L_51)), ((float)((float)L_52-(float)L_53)), /*hidden argument*/NULL);
		Vector2_t2156229523  L_55 = ImpressionHandlerGO_WorldToScreenPoint_m1334575203(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		(*(Vector2_t2156229523 *)((L_47)->GetAddressAt(static_cast<il2cpp_array_size_t>(5)))) = L_55;
		Vector2U5BU5D_t1457185986* L_56 = L_47;
		NullCheck(L_56);
		float L_57 = (&V_0)->get_x_1();
		float L_58 = (&V_1)->get_x_1();
		float L_59 = (&V_0)->get_y_2();
		float L_60 = (&V_1)->get_y_2();
		float L_61 = (&V_0)->get_z_3();
		float L_62 = (&V_1)->get_z_3();
		Vector3_t3722313464  L_63;
		memset(&L_63, 0, sizeof(L_63));
		Vector3__ctor_m1197556204(&L_63, ((float)((float)L_57-(float)L_58)), ((float)((float)L_59+(float)L_60)), ((float)((float)L_61+(float)L_62)), /*hidden argument*/NULL);
		Vector2_t2156229523  L_64 = ImpressionHandlerGO_WorldToScreenPoint_m1334575203(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		(*(Vector2_t2156229523 *)((L_56)->GetAddressAt(static_cast<il2cpp_array_size_t>(6)))) = L_64;
		Vector2U5BU5D_t1457185986* L_65 = L_56;
		NullCheck(L_65);
		float L_66 = (&V_0)->get_x_1();
		float L_67 = (&V_1)->get_x_1();
		float L_68 = (&V_0)->get_y_2();
		float L_69 = (&V_1)->get_y_2();
		float L_70 = (&V_0)->get_z_3();
		float L_71 = (&V_1)->get_z_3();
		Vector3_t3722313464  L_72;
		memset(&L_72, 0, sizeof(L_72));
		Vector3__ctor_m1197556204(&L_72, ((float)((float)L_66+(float)L_67)), ((float)((float)L_68+(float)L_69)), ((float)((float)L_70+(float)L_71)), /*hidden argument*/NULL);
		Vector2_t2156229523  L_73 = ImpressionHandlerGO_WorldToScreenPoint_m1334575203(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
		(*(Vector2_t2156229523 *)((L_65)->GetAddressAt(static_cast<il2cpp_array_size_t>(7)))) = L_73;
		V_2 = L_65;
		Vector2U5BU5D_t1457185986* L_74 = V_2;
		NullCheck(L_74);
		V_3 = (*(Vector2_t2156229523 *)((L_74)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t1457185986* L_75 = V_2;
		NullCheck(L_75);
		V_4 = (*(Vector2_t2156229523 *)((L_75)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t1457185986* L_76 = V_2;
		V_6 = L_76;
		V_7 = 0;
		goto IL_027f;
	}

IL_0255:
	{
		Vector2U5BU5D_t1457185986* L_77 = V_6;
		int32_t L_78 = V_7;
		NullCheck(L_77);
		V_5 = (*(Vector2_t2156229523 *)((L_77)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_78))));
		Vector2_t2156229523  L_79 = V_3;
		Vector2_t2156229523  L_80 = V_5;
		Vector2_t2156229523  L_81 = Vector2_Min_m2207225457(NULL /*static, unused*/, L_79, L_80, /*hidden argument*/NULL);
		V_3 = L_81;
		Vector2_t2156229523  L_82 = V_4;
		Vector2_t2156229523  L_83 = V_5;
		Vector2_t2156229523  L_84 = Vector2_Max_m1487165843(NULL /*static, unused*/, L_82, L_83, /*hidden argument*/NULL);
		V_4 = L_84;
		int32_t L_85 = V_7;
		V_7 = ((int32_t)((int32_t)L_85+(int32_t)1));
	}

IL_027f:
	{
		int32_t L_86 = V_7;
		Vector2U5BU5D_t1457185986* L_87 = V_6;
		NullCheck(L_87);
		if ((((int32_t)L_86) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_87)->max_length)))))))
		{
			goto IL_0255;
		}
	}
	{
		float L_88 = (&V_3)->get_x_0();
		float L_89 = (&V_3)->get_y_1();
		float L_90 = (&V_4)->get_x_0();
		float L_91 = (&V_3)->get_x_0();
		float L_92 = (&V_4)->get_y_1();
		float L_93 = (&V_3)->get_y_1();
		Rect_t2360479859  L_94;
		memset(&L_94, 0, sizeof(L_94));
		Rect__ctor_m2635848439(&L_94, L_88, L_89, ((float)((float)L_90-(float)L_91)), ((float)((float)L_92-(float)L_93)), /*hidden argument*/NULL);
		return L_94;
	}
}
// UnityEngine.Vector2 NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerGO::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  ImpressionHandlerGO_WorldToScreenPoint_m1334575203 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___world0, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t4157153871 * L_0 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = ___world0;
		NullCheck(L_0);
		Vector3_t3722313464  L_2 = Camera_WorldToScreenPoint_m491883527(L_0, L_1, /*hidden argument*/NULL);
		Vector2_t2156229523  L_3 = Vector2_op_Implicit_m1304503157(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = Screen_get_height_m1569321028(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = (&V_0)->get_y_1();
		(&V_0)->set_y_1(((float)((float)(((float)((float)L_4)))-(float)L_5)));
		Vector2_t2156229523  L_6 = V_0;
		return L_6;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI::.ctor()
extern "C"  void ImpressionHandlerUI__ctor_m1750883783 (ImpressionHandlerUI_t188317754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImpressionHandlerUI__ctor_m1750883783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Corners_8(((Vector3U5BU5D_t1718750761*)SZArrayNew(Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var, (uint32_t)4)));
		ImpressionHandler__ctor_m2314060538(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI::Start()
extern "C"  void ImpressionHandlerUI_Start_m3327745437 (ImpressionHandlerUI_t188317754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImpressionHandlerUI_Start_m3327745437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Canvas_t3310196443 * L_0 = Component_GetComponentInParent_TisCanvas_t3310196443_m1339529724(__this, /*hidden argument*/Component_GetComponentInParent_TisCanvas_t3310196443_m1339529724_MethodInfo_var);
		__this->set_m_Canvas_7(L_0);
		Canvas_t3310196443 * L_1 = __this->get_m_Canvas_7();
		NullCheck(L_1);
		Camera_t4157153871 * L_2 = Canvas_get_worldCamera_m2289553767(L_1, /*hidden argument*/NULL);
		__this->set_m_Camera_6(L_2);
		Camera_t4157153871 * L_3 = __this->get_m_Camera_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0039;
		}
	}
	{
		Camera_t4157153871 * L_5 = Camera_get_main_m882983993(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Camera_6(L_5);
	}

IL_0039:
	{
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI::IsViewable()
extern "C"  bool ImpressionHandlerUI_IsViewable_m1581305922 (ImpressionHandlerUI_t188317754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImpressionHandlerUI_IsViewable_m1581305922_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CIsViewableU3Ec__AnonStorey0_t3542390976 * V_0 = NULL;
	RectTransform_t3704657025 * V_1 = NULL;
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t2360479859  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t2360479859  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	CanvasGroup_t4083511760 * V_8 = NULL;
	Vector2U5BU5D_t1457185986* V_9 = NULL;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	U3CIsViewableU3Ec__AnonStorey0_t3542390976 * G_B13_0 = NULL;
	U3CIsViewableU3Ec__AnonStorey0_t3542390976 * G_B11_0 = NULL;
	U3CIsViewableU3Ec__AnonStorey0_t3542390976 * G_B12_0 = NULL;
	Camera_t4157153871 * G_B14_0 = NULL;
	U3CIsViewableU3Ec__AnonStorey0_t3542390976 * G_B14_1 = NULL;
	{
		U3CIsViewableU3Ec__AnonStorey0_t3542390976 * L_0 = (U3CIsViewableU3Ec__AnonStorey0_t3542390976 *)il2cpp_codegen_object_new(U3CIsViewableU3Ec__AnonStorey0_t3542390976_il2cpp_TypeInfo_var);
		U3CIsViewableU3Ec__AnonStorey0__ctor_m2474423893(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3600365921 * L_2 = GameObject_get_transform_m393750976(L_1, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t3704657025 *)IsInstSealed(L_2, RectTransform_t3704657025_il2cpp_TypeInfo_var));
		RectTransform_t3704657025 * L_3 = V_1;
		NullCheck(L_3);
		Vector2_t2156229523  L_4 = RectTransform_get_sizeDelta_m2136908840(L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = (&V_2)->get_x_0();
		if ((((float)(0.0f)) >= ((float)L_5)))
		{
			goto IL_0047;
		}
	}
	{
		RectTransform_t3704657025 * L_6 = V_1;
		NullCheck(L_6);
		Vector2_t2156229523  L_7 = RectTransform_get_sizeDelta_m2136908840(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		float L_8 = (&V_3)->get_y_1();
		if ((!(((float)(0.0f)) >= ((float)L_8))))
		{
			goto IL_0079;
		}
	}

IL_0047:
	{
		RectTransform_t3704657025 * L_9 = V_1;
		NullCheck(L_9);
		Rect_t2360479859  L_10 = RectTransform_get_rect_m1643570810(L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = Rect_get_width_m3421965717((&V_4), /*hidden argument*/NULL);
		if ((((float)(0.0f)) >= ((float)L_11)))
		{
			goto IL_00ab;
		}
	}
	{
		RectTransform_t3704657025 * L_12 = V_1;
		NullCheck(L_12);
		Rect_t2360479859  L_13 = RectTransform_get_rect_m1643570810(L_12, /*hidden argument*/NULL);
		V_5 = L_13;
		float L_14 = Rect_get_height_m977101306((&V_5), /*hidden argument*/NULL);
		if ((((float)(0.0f)) >= ((float)L_14)))
		{
			goto IL_00ab;
		}
	}

IL_0079:
	{
		RectTransform_t3704657025 * L_15 = V_1;
		NullCheck(L_15);
		Vector3_t3722313464  L_16 = Transform_get_localScale_m2509514663(L_15, /*hidden argument*/NULL);
		V_6 = L_16;
		float L_17 = (&V_6)->get_x_1();
		if ((((float)(0.0f)) >= ((float)L_17)))
		{
			goto IL_00ab;
		}
	}
	{
		RectTransform_t3704657025 * L_18 = V_1;
		NullCheck(L_18);
		Vector3_t3722313464  L_19 = Transform_get_localScale_m2509514663(L_18, /*hidden argument*/NULL);
		V_7 = L_19;
		float L_20 = (&V_7)->get_y_2();
		if ((!(((float)(0.0f)) >= ((float)L_20))))
		{
			goto IL_00cb;
		}
	}

IL_00ab:
	{
		ObjectU5BU5D_t2843939325* L_21 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_22 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = Object_get_name_m1414505214(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_23);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral361083404, L_21, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_00cb:
	{
		RectTransform_t3704657025 * L_24 = V_1;
		NullCheck(L_24);
		CanvasGroup_t4083511760 * L_25 = Component_GetComponent_TisCanvasGroup_t4083511760_m1030459026(L_24, /*hidden argument*/Component_GetComponent_TisCanvasGroup_t4083511760_m1030459026_MethodInfo_var);
		V_8 = L_25;
		CanvasGroup_t4083511760 * L_26 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0111;
		}
	}
	{
		CanvasGroup_t4083511760 * L_28 = V_8;
		NullCheck(L_28);
		float L_29 = CanvasGroup_get_alpha_m913685212(L_28, /*hidden argument*/NULL);
		if ((!(((float)(0.0f)) >= ((float)L_29))))
		{
			goto IL_0111;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_30 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_31 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		String_t* L_32 = Object_get_name_m1414505214(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_32);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral3092120028, L_30, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0111:
	{
		RectTransform_t3704657025 * L_33 = V_1;
		Vector3U5BU5D_t1718750761* L_34 = __this->get_m_Corners_8();
		NullCheck(L_33);
		RectTransform_GetWorldCorners_m2125351209(L_33, L_34, /*hidden argument*/NULL);
		ObjectU5BU5D_t2843939325* L_35 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)5));
		Vector3U5BU5D_t1718750761* L_36 = __this->get_m_Corners_8();
		NullCheck(L_36);
		Vector3_t3722313464  L_37 = (*(Vector3_t3722313464 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Il2CppObject * L_38 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_37);
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_38);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_38);
		ObjectU5BU5D_t2843939325* L_39 = L_35;
		Vector3U5BU5D_t1718750761* L_40 = __this->get_m_Corners_8();
		NullCheck(L_40);
		Vector3_t3722313464  L_41 = (*(Vector3_t3722313464 *)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Il2CppObject * L_42 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, L_42);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_42);
		ObjectU5BU5D_t2843939325* L_43 = L_39;
		Vector3U5BU5D_t1718750761* L_44 = __this->get_m_Corners_8();
		NullCheck(L_44);
		Vector3_t3722313464  L_45 = (*(Vector3_t3722313464 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Il2CppObject * L_46 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, L_46);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_46);
		ObjectU5BU5D_t2843939325* L_47 = L_43;
		Vector3U5BU5D_t1718750761* L_48 = __this->get_m_Corners_8();
		NullCheck(L_48);
		Vector3_t3722313464  L_49 = (*(Vector3_t3722313464 *)((L_48)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Il2CppObject * L_50 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_47);
		ArrayElementTypeCheck (L_47, L_50);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_50);
		ObjectU5BU5D_t2843939325* L_51 = L_47;
		Camera_t4157153871 * L_52 = __this->get_m_Camera_6();
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, L_52);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_52);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral2129735177, L_51, /*hidden argument*/NULL);
		U3CIsViewableU3Ec__AnonStorey0_t3542390976 * L_53 = V_0;
		Canvas_t3310196443 * L_54 = __this->get_m_Canvas_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_55 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_54, /*hidden argument*/NULL);
		G_B11_0 = L_53;
		if (!L_55)
		{
			G_B13_0 = L_53;
			goto IL_01c2;
		}
	}
	{
		Canvas_t3310196443 * L_56 = __this->get_m_Canvas_7();
		NullCheck(L_56);
		int32_t L_57 = Canvas_get_renderMode_m449291776(L_56, /*hidden argument*/NULL);
		G_B12_0 = G_B11_0;
		if (L_57)
		{
			G_B13_0 = G_B11_0;
			goto IL_01c2;
		}
	}
	{
		G_B14_0 = ((Camera_t4157153871 *)(NULL));
		G_B14_1 = G_B12_0;
		goto IL_01c8;
	}

IL_01c2:
	{
		Camera_t4157153871 * L_58 = __this->get_m_Camera_6();
		G_B14_0 = L_58;
		G_B14_1 = G_B13_0;
	}

IL_01c8:
	{
		NullCheck(G_B14_1);
		G_B14_1->set_camera_0(G_B14_0);
		Vector3U5BU5D_t1718750761* L_59 = __this->get_m_Corners_8();
		U3CIsViewableU3Ec__AnonStorey0_t3542390976 * L_60 = V_0;
		IntPtr_t L_61;
		L_61.set_m_value_0((void*)(void*)U3CIsViewableU3Ec__AnonStorey0_U3CU3Em__0_m2538634596_MethodInfo_var);
		Func_2_t398065169 * L_62 = (Func_2_t398065169 *)il2cpp_codegen_object_new(Func_2_t398065169_il2cpp_TypeInfo_var);
		Func_2__ctor_m134789189(L_62, L_60, L_61, /*hidden argument*/Func_2__ctor_m134789189_MethodInfo_var);
		Il2CppObject* L_63 = Enumerable_Select_TisVector3_t3722313464_TisVector2_t2156229523_m2491549993(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_59, L_62, /*hidden argument*/Enumerable_Select_TisVector3_t3722313464_TisVector2_t2156229523_m2491549993_MethodInfo_var);
		Vector2U5BU5D_t1457185986* L_64 = Enumerable_ToArray_TisVector2_t2156229523_m3384610930(NULL /*static, unused*/, L_63, /*hidden argument*/Enumerable_ToArray_TisVector2_t2156229523_m3384610930_MethodInfo_var);
		V_9 = L_64;
		ObjectU5BU5D_t2843939325* L_65 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)5));
		Vector2U5BU5D_t1457185986* L_66 = V_9;
		NullCheck(L_66);
		Vector2_t2156229523  L_67 = (*(Vector2_t2156229523 *)((L_66)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Il2CppObject * L_68 = Box(Vector2_t2156229523_il2cpp_TypeInfo_var, &L_67);
		NullCheck(L_65);
		ArrayElementTypeCheck (L_65, L_68);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_68);
		ObjectU5BU5D_t2843939325* L_69 = L_65;
		Vector2U5BU5D_t1457185986* L_70 = V_9;
		NullCheck(L_70);
		Vector2_t2156229523  L_71 = (*(Vector2_t2156229523 *)((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Il2CppObject * L_72 = Box(Vector2_t2156229523_il2cpp_TypeInfo_var, &L_71);
		NullCheck(L_69);
		ArrayElementTypeCheck (L_69, L_72);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_72);
		ObjectU5BU5D_t2843939325* L_73 = L_69;
		Vector2U5BU5D_t1457185986* L_74 = V_9;
		NullCheck(L_74);
		Vector2_t2156229523  L_75 = (*(Vector2_t2156229523 *)((L_74)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Il2CppObject * L_76 = Box(Vector2_t2156229523_il2cpp_TypeInfo_var, &L_75);
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, L_76);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_76);
		ObjectU5BU5D_t2843939325* L_77 = L_73;
		Vector2U5BU5D_t1457185986* L_78 = V_9;
		NullCheck(L_78);
		Vector2_t2156229523  L_79 = (*(Vector2_t2156229523 *)((L_78)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Il2CppObject * L_80 = Box(Vector2_t2156229523_il2cpp_TypeInfo_var, &L_79);
		NullCheck(L_77);
		ArrayElementTypeCheck (L_77, L_80);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_80);
		ObjectU5BU5D_t2843939325* L_81 = L_77;
		Camera_t4157153871 * L_82 = __this->get_m_Camera_6();
		NullCheck(L_81);
		ArrayElementTypeCheck (L_81, L_82);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_82);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral3764436550, L_81, /*hidden argument*/NULL);
		Vector2U5BU5D_t1457185986* L_83 = V_9;
		NullCheck(L_83);
		float L_84 = ((L_83)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_0();
		Vector2U5BU5D_t1457185986* L_85 = V_9;
		NullCheck(L_85);
		float L_86 = ((L_85)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_87 = Mathf_Min_m1901831667(NULL /*static, unused*/, L_84, L_86, /*hidden argument*/NULL);
		V_10 = L_87;
		Vector2U5BU5D_t1457185986* L_88 = V_9;
		NullCheck(L_88);
		float L_89 = ((L_88)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_0();
		Vector2U5BU5D_t1457185986* L_90 = V_9;
		NullCheck(L_90);
		float L_91 = ((L_90)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_x_0();
		float L_92 = Mathf_Max_m571965535(NULL /*static, unused*/, L_89, L_91, /*hidden argument*/NULL);
		V_11 = L_92;
		Vector2U5BU5D_t1457185986* L_93 = V_9;
		NullCheck(L_93);
		float L_94 = ((L_93)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_1();
		Vector2U5BU5D_t1457185986* L_95 = V_9;
		NullCheck(L_95);
		float L_96 = ((L_95)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_y_1();
		float L_97 = Mathf_Max_m571965535(NULL /*static, unused*/, L_94, L_96, /*hidden argument*/NULL);
		V_12 = L_97;
		Vector2U5BU5D_t1457185986* L_98 = V_9;
		NullCheck(L_98);
		float L_99 = ((L_98)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_y_1();
		Vector2U5BU5D_t1457185986* L_100 = V_9;
		NullCheck(L_100);
		float L_101 = ((L_100)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_1();
		float L_102 = Mathf_Min_m1901831667(NULL /*static, unused*/, L_99, L_101, /*hidden argument*/NULL);
		V_13 = L_102;
		float L_103 = V_10;
		float L_104 = V_12;
		float L_105 = V_11;
		float L_106 = V_13;
		Corner_t2201492025  L_107;
		memset(&L_107, 0, sizeof(L_107));
		Corner__ctor_m3011766940(&L_107, L_103, L_104, L_105, L_106, /*hidden argument*/NULL);
		bool L_108 = ImpressionHandler_CheckViewablePercentage_m3622756226(__this, L_107, /*hidden argument*/NULL);
		return L_108;
	}
}
// System.Void NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI/<IsViewable>c__AnonStorey0::.ctor()
extern "C"  void U3CIsViewableU3Ec__AnonStorey0__ctor_m2474423893 (U3CIsViewableU3Ec__AnonStorey0_t3542390976 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI/<IsViewable>c__AnonStorey0::<>m__0(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  U3CIsViewableU3Ec__AnonStorey0_U3CU3Em__0_m2538634596 (U3CIsViewableU3Ec__AnonStorey0_t3542390976 * __this, Vector3_t3722313464  ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CIsViewableU3Ec__AnonStorey0_U3CU3Em__0_m2538634596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t4157153871 * L_0 = __this->get_camera_0();
		Vector3_t3722313464  L_1 = ___v0;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = RectTransformUtility_WorldToScreenPoint_m4200566991(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::.ctor()
extern "C"  void NativeAd__ctor_m646989586 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd__ctor_m646989586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HashSet_1_t2527432003 * L_0 = (HashSet_1_t2527432003 *)il2cpp_codegen_object_new(HashSet_1_t2527432003_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m4032723476(L_0, /*hidden argument*/HashSet_1__ctor_m4032723476_MethodInfo_var);
		__this->set_m_Handlers_2(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::add_AdClicked(System.EventHandler)
extern "C"  void NativeAd_add_AdClicked_m2857805307 (NativeAd_t775405424 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_add_AdClicked_m2857805307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdClicked_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_AdClicked_0();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::remove_AdClicked(System.EventHandler)
extern "C"  void NativeAd_remove_AdClicked_m170162276 (NativeAd_t775405424 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_remove_AdClicked_m170162276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdClicked_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_AdClicked_0();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::Finalize()
extern "C"  void NativeAd_Finalize_m867583946 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker0::Invoke(19 /* System.Void NendUnityPlugin.AD.Native.NativeAd::Dispose() */, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::Dispose()
extern "C"  void NativeAd_Dispose_m2731176596 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_Dispose_m2731176596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral996763665, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		NativeAd_DestroyAllHandlers_m3923046273(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String NendUnityPlugin.AD.Native.NativeAd::get_ShortText()
extern "C"  String_t* NativeAd_get_ShortText_m3891001356 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_get_ShortText_m3891001356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.String NendUnityPlugin.AD.Native.NativeAd::get_LongText()
extern "C"  String_t* NativeAd_get_LongText_m2780726328 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_get_LongText_m2780726328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.String NendUnityPlugin.AD.Native.NativeAd::get_PromotionUrl()
extern "C"  String_t* NativeAd_get_PromotionUrl_m3744682610 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_get_PromotionUrl_m3744682610_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.String NendUnityPlugin.AD.Native.NativeAd::get_PromotionName()
extern "C"  String_t* NativeAd_get_PromotionName_m3390834270 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_get_PromotionName_m3390834270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.String NendUnityPlugin.AD.Native.NativeAd::get_ActionButtonText()
extern "C"  String_t* NativeAd_get_ActionButtonText_m3270850130 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_get_ActionButtonText_m3270850130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.String NendUnityPlugin.AD.Native.NativeAd::get_AdImageUrl()
extern "C"  String_t* NativeAd_get_AdImageUrl_m71574607 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_get_AdImageUrl_m71574607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.String NendUnityPlugin.AD.Native.NativeAd::get_LogoImageUrl()
extern "C"  String_t* NativeAd_get_LogoImageUrl_m438093028 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_get_LogoImageUrl_m438093028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.String NendUnityPlugin.AD.Native.NativeAd::GetAdvertisingExplicitlyText(NendUnityPlugin.AD.Native.AdvertisingExplicitly)
extern "C"  String_t* NativeAd_GetAdvertisingExplicitlyText_m2921118262 (NativeAd_t775405424 * __this, int32_t ___advertisingExplicitly0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_GetAdvertisingExplicitlyText_m2921118262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator NendUnityPlugin.AD.Native.NativeAd::LoadAdImage(System.Action`1<UnityEngine.Texture2D>)
extern "C"  Il2CppObject * NativeAd_LoadAdImage_m3618624557 (NativeAd_t775405424 * __this, Action_1_t4012913780 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_LoadAdImage_m3618624557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String NendUnityPlugin.AD.Native.NativeAd::get_AdImageUrl() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String NendUnityPlugin.AD.Native.NativeAd::get_AdImageUrl() */, __this);
		Action_1_t4012913780 * L_3 = ___callback0;
		Il2CppObject * L_4 = TextureLoader_LoadTexture_m3236146968(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001d:
	{
		Action_1_t4012913780 * L_5 = ___callback0;
		NullCheck(L_5);
		Action_1_Invoke_m400472198(L_5, (Texture2D_t3840446185 *)NULL, /*hidden argument*/Action_1_Invoke_m400472198_MethodInfo_var);
		return (Il2CppObject *)NULL;
	}
}
// System.Collections.IEnumerator NendUnityPlugin.AD.Native.NativeAd::LoadLogoImage(System.Action`1<UnityEngine.Texture2D>)
extern "C"  Il2CppObject * NativeAd_LoadLogoImage_m717757564 (NativeAd_t775405424 * __this, Action_1_t4012913780 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_LoadLogoImage_m717757564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String NendUnityPlugin.AD.Native.NativeAd::get_LogoImageUrl() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String NendUnityPlugin.AD.Native.NativeAd::get_LogoImageUrl() */, __this);
		Action_1_t4012913780 * L_3 = ___callback0;
		Il2CppObject * L_4 = TextureLoader_LoadTexture_m3236146968(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001d:
	{
		Action_1_t4012913780 * L_5 = ___callback0;
		NullCheck(L_5);
		Action_1_Invoke_m400472198(L_5, (Texture2D_t3840446185 *)NULL, /*hidden argument*/Action_1_Invoke_m400472198_MethodInfo_var);
		return (Il2CppObject *)NULL;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::Activate(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void NativeAd_Activate_m1759400131 (NativeAd_t775405424 * __this, GameObject_t1113636619 * ___adGameObject0, GameObject_t1113636619 * ___prGameObject1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_Activate_m1759400131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = ___adGameObject0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		GameObject_t1113636619 * L_2 = ___prGameObject1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		ArgumentNullException_t1615371798 * L_4 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_4, _stringLiteral1094905266, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		GameObject_t1113636619 * L_5 = ___adGameObject0;
		NullCheck(L_5);
		ClickHandler_t29098297 * L_6 = GameObject_GetComponent_TisClickHandler_t29098297_m4091602126(L_5, /*hidden argument*/GameObject_GetComponent_TisClickHandler_t29098297_m4091602126_MethodInfo_var);
		NativeAd_DestroyHandler_m1214946435(__this, L_6, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_7 = ___prGameObject1;
		NullCheck(L_7);
		ClickHandler_t29098297 * L_8 = GameObject_GetComponent_TisClickHandler_t29098297_m4091602126(L_7, /*hidden argument*/GameObject_GetComponent_TisClickHandler_t29098297_m4091602126_MethodInfo_var);
		NativeAd_DestroyHandler_m1214946435(__this, L_8, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_9 = ___adGameObject0;
		NullCheck(L_9);
		ImpressionHandler_t4278309981 * L_10 = GameObject_GetComponent_TisImpressionHandler_t4278309981_m2269228889(L_9, /*hidden argument*/GameObject_GetComponent_TisImpressionHandler_t4278309981_m2269228889_MethodInfo_var);
		NativeAd_DestroyHandler_m1214946435(__this, L_10, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_11 = ___adGameObject0;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)NativeAd_U3CActivateU3Em__0_m3793495835_MethodInfo_var);
		Action_t1264377477 * L_13 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_13, __this, L_12, /*hidden argument*/NULL);
		NativeAd_AddClickHandler_m3246524294(__this, L_11, L_13, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_14 = ___prGameObject1;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)NativeAd_U3CActivateU3Em__1_m3793561371_MethodInfo_var);
		Action_t1264377477 * L_16 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_16, __this, L_15, /*hidden argument*/NULL);
		NativeAd_AddClickHandler_m3246524294(__this, L_14, L_16, /*hidden argument*/NULL);
		bool L_17 = __this->get_m_IsImpression_1();
		if (L_17)
		{
			goto IL_007f;
		}
	}
	{
		GameObject_t1113636619 * L_18 = ___adGameObject0;
		NativeAd_AddImpressionHandler_m742031269(__this, L_18, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::Into(NendUnityPlugin.AD.Native.NendAdNativeView)
extern "C"  void NativeAd_Into_m1246524646 (NativeAd_t775405424 * __this, NendAdNativeView_t2014609480 * ___adView0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_Into_m1246524646_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NendAdNativeView_t2014609480 * L_0 = ___adView0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_2, _stringLiteral1728766118, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		NendAdNativeView_t2014609480 * L_3 = ___adView0;
		NullCheck(L_3);
		NendAdNativeView_RenderAd_m3300549469(L_3, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::AddClickHandler(UnityEngine.GameObject,System.Action)
extern "C"  void NativeAd_AddClickHandler_m3246524294 (NativeAd_t775405424 * __this, GameObject_t1113636619 * ___target0, Action_t1264377477 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_AddClickHandler_m3246524294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ClickHandler_t29098297 * V_0 = NULL;
	Canvas_t3310196443 * V_1 = NULL;
	Collider_t1773347010 * V_2 = NULL;
	Collider2D_t2806799626 * V_3 = NULL;
	{
		V_0 = (ClickHandler_t29098297 *)NULL;
		GameObject_t1113636619 * L_0 = ___target0;
		NullCheck(L_0);
		Canvas_t3310196443 * L_1 = GameObject_GetComponentInParent_TisCanvas_t3310196443_m3114266371(L_0, /*hidden argument*/GameObject_GetComponentInParent_TisCanvas_t3310196443_m3114266371_MethodInfo_var);
		V_1 = L_1;
		Canvas_t3310196443 * L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		GameObject_t1113636619 * L_4 = ___target0;
		NullCheck(L_4);
		ClickHandlerUI_t258087360 * L_5 = GameObject_AddComponent_TisClickHandlerUI_t258087360_m1730712142(L_4, /*hidden argument*/GameObject_AddComponent_TisClickHandlerUI_t258087360_m1730712142_MethodInfo_var);
		V_0 = L_5;
		goto IL_005a;
	}

IL_0021:
	{
		GameObject_t1113636619 * L_6 = ___target0;
		NullCheck(L_6);
		Collider_t1773347010 * L_7 = GameObject_GetComponent_TisCollider_t1773347010_m69509314(L_6, /*hidden argument*/GameObject_GetComponent_TisCollider_t1773347010_m69509314_MethodInfo_var);
		V_2 = L_7;
		Collider_t1773347010 * L_8 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0040;
		}
	}
	{
		GameObject_t1113636619 * L_10 = ___target0;
		NullCheck(L_10);
		ClickHandler3D_t283384251 * L_11 = GameObject_AddComponent_TisClickHandler3D_t283384251_m1758828144(L_10, /*hidden argument*/GameObject_AddComponent_TisClickHandler3D_t283384251_m1758828144_MethodInfo_var);
		V_0 = L_11;
		goto IL_005a;
	}

IL_0040:
	{
		GameObject_t1113636619 * L_12 = ___target0;
		NullCheck(L_12);
		Collider2D_t2806799626 * L_13 = GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940(L_12, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t2806799626_m1184015940_MethodInfo_var);
		V_3 = L_13;
		Collider2D_t2806799626 * L_14 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t1113636619 * L_16 = ___target0;
		NullCheck(L_16);
		ClickHandler2D_t2622036411 * L_17 = GameObject_AddComponent_TisClickHandler2D_t2622036411_m1758828177(L_16, /*hidden argument*/GameObject_AddComponent_TisClickHandler2D_t2622036411_m1758828177_MethodInfo_var);
		V_0 = L_17;
	}

IL_005a:
	{
		ClickHandler_t29098297 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_007f;
		}
	}
	{
		ClickHandler_t29098297 * L_20 = V_0;
		Action_t1264377477 * L_21 = ___callback1;
		NullCheck(L_20);
		L_20->set_OnClick_2(L_21);
		HashSet_1_t2527432003 * L_22 = __this->get_m_Handlers_2();
		ClickHandler_t29098297 * L_23 = V_0;
		NullCheck(L_22);
		HashSet_1_Add_m3879467893(L_22, L_23, /*hidden argument*/HashSet_1_Add_m3879467893_MethodInfo_var);
		goto IL_0098;
	}

IL_007f:
	{
		ObjectU5BU5D_t2843939325* L_24 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_25 = ___target0;
		NullCheck(L_25);
		String_t* L_26 = Object_get_name_m1414505214(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_26);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_26);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_W_m3326785758(NULL /*static, unused*/, _stringLiteral3039072531, L_24, /*hidden argument*/NULL);
	}

IL_0098:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::AddImpressionHandler(UnityEngine.GameObject)
extern "C"  void NativeAd_AddImpressionHandler_m742031269 (NativeAd_t775405424 * __this, GameObject_t1113636619 * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_AddImpressionHandler_m742031269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ImpressionHandler_t4278309981 * V_0 = NULL;
	RectTransform_t3704657025 * V_1 = NULL;
	{
		V_0 = (ImpressionHandler_t4278309981 *)NULL;
		GameObject_t1113636619 * L_0 = ___target0;
		NullCheck(L_0);
		RectTransform_t3704657025 * L_1 = GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(L_0, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_MethodInfo_var);
		V_1 = L_1;
		RectTransform_t3704657025 * L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		GameObject_t1113636619 * L_4 = ___target0;
		NullCheck(L_4);
		ImpressionHandlerUI_t188317754 * L_5 = GameObject_AddComponent_TisImpressionHandlerUI_t188317754_m2131962076(L_4, /*hidden argument*/GameObject_AddComponent_TisImpressionHandlerUI_t188317754_m2131962076_MethodInfo_var);
		V_0 = L_5;
		goto IL_0028;
	}

IL_0021:
	{
		GameObject_t1113636619 * L_6 = ___target0;
		NullCheck(L_6);
		ImpressionHandlerGO_t188448812 * L_7 = GameObject_AddComponent_TisImpressionHandlerGO_t188448812_m1290658942(L_6, /*hidden argument*/GameObject_AddComponent_TisImpressionHandlerGO_t188448812_m1290658942_MethodInfo_var);
		V_0 = L_7;
	}

IL_0028:
	{
		ImpressionHandler_t4278309981 * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0058;
		}
	}
	{
		ImpressionHandler_t4278309981 * L_10 = V_0;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)NativeAd_U3CAddImpressionHandlerU3Em__2_m2583096591_MethodInfo_var);
		Action_t1264377477 * L_12 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_12, __this, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		L_10->set_OnImpression_5(L_12);
		HashSet_1_t2527432003 * L_13 = __this->get_m_Handlers_2();
		ImpressionHandler_t4278309981 * L_14 = V_0;
		NullCheck(L_13);
		HashSet_1_Add_m3879467893(L_13, L_14, /*hidden argument*/HashSet_1_Add_m3879467893_MethodInfo_var);
		goto IL_0071;
	}

IL_0058:
	{
		ObjectU5BU5D_t2843939325* L_15 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t1113636619 * L_16 = ___target0;
		NullCheck(L_16);
		String_t* L_17 = Object_get_name_m1414505214(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_17);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_W_m3326785758(NULL /*static, unused*/, _stringLiteral2268994489, L_15, /*hidden argument*/NULL);
	}

IL_0071:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::DestroyHandler(UnityEngine.MonoBehaviour)
extern "C"  void NativeAd_DestroyHandler_m1214946435 (NativeAd_t775405424 * __this, MonoBehaviour_t3962482529 * ___handler0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_DestroyHandler_m1214946435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t3962482529 * L_0 = ___handler0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		MonoBehaviour_t3962482529 * L_2 = ___handler0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		HashSet_1_t2527432003 * L_3 = __this->get_m_Handlers_2();
		MonoBehaviour_t3962482529 * L_4 = ___handler0;
		NullCheck(L_3);
		HashSet_1_Remove_m1688024103(L_3, L_4, /*hidden argument*/HashSet_1_Remove_m1688024103_MethodInfo_var);
	}

IL_001f:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::DestroyHandlers(UnityEngine.MonoBehaviour[])
extern "C"  void NativeAd_DestroyHandlers_m3057256890 (NativeAd_t775405424 * __this, MonoBehaviourU5BU5D_t2007329276* ___handlers0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_DestroyHandlers_m3057256890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MonoBehaviour_t3962482529 * V_0 = NULL;
	MonoBehaviourU5BU5D_t2007329276* V_1 = NULL;
	int32_t V_2 = 0;
	{
		MonoBehaviourU5BU5D_t2007329276* L_0 = ___handlers0;
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		MonoBehaviourU5BU5D_t2007329276* L_1 = ___handlers0;
		NullCheck(L_1);
		if ((((int32_t)0) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))
		{
			goto IL_003d;
		}
	}
	{
		MonoBehaviourU5BU5D_t2007329276* L_2 = ___handlers0;
		V_1 = L_2;
		V_2 = 0;
		goto IL_0034;
	}

IL_0018:
	{
		MonoBehaviourU5BU5D_t2007329276* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		MonoBehaviour_t3962482529 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_0 = L_6;
		MonoBehaviour_t3962482529 * L_7 = V_0;
		NativeAd_DestroyHandler_m1214946435(__this, L_7, /*hidden argument*/NULL);
		HashSet_1_t2527432003 * L_8 = __this->get_m_Handlers_2();
		MonoBehaviour_t3962482529 * L_9 = V_0;
		NullCheck(L_8);
		HashSet_1_Remove_m1688024103(L_8, L_9, /*hidden argument*/HashSet_1_Remove_m1688024103_MethodInfo_var);
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_11 = V_2;
		MonoBehaviourU5BU5D_t2007329276* L_12 = V_1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0018;
		}
	}

IL_003d:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::DestroyImpressionHandlers()
extern "C"  void NativeAd_DestroyImpressionHandlers_m3505163566 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_DestroyImpressionHandlers_m3505163566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HashSet_1_t2527432003 * G_B2_0 = NULL;
	NativeAd_t775405424 * G_B2_1 = NULL;
	HashSet_1_t2527432003 * G_B1_0 = NULL;
	NativeAd_t775405424 * G_B1_1 = NULL;
	{
		HashSet_1_t2527432003 * L_0 = __this->get_m_Handlers_2();
		Func_2_t2922669422 * L_1 = ((NativeAd_t775405424_StaticFields*)NativeAd_t775405424_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_3();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001f;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)NativeAd_U3CDestroyImpressionHandlersU3Em__3_m3316364388_MethodInfo_var);
		Func_2_t2922669422 * L_3 = (Func_2_t2922669422 *)il2cpp_codegen_object_new(Func_2_t2922669422_il2cpp_TypeInfo_var);
		Func_2__ctor_m2243427934(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2243427934_MethodInfo_var);
		((NativeAd_t775405424_StaticFields*)NativeAd_t775405424_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_3(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001f:
	{
		Func_2_t2922669422 * L_4 = ((NativeAd_t775405424_StaticFields*)NativeAd_t775405424_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_3();
		Il2CppObject* L_5 = Enumerable_Where_TisMonoBehaviour_t3962482529_m3892073355(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_Where_TisMonoBehaviour_t3962482529_m3892073355_MethodInfo_var);
		MonoBehaviourU5BU5D_t2007329276* L_6 = Enumerable_ToArray_TisMonoBehaviour_t3962482529_m4146969897(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_ToArray_TisMonoBehaviour_t3962482529_m4146969897_MethodInfo_var);
		NullCheck(G_B2_1);
		NativeAd_DestroyHandlers_m3057256890(G_B2_1, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::DestroyAllHandlers()
extern "C"  void NativeAd_DestroyAllHandlers_m3923046273 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_DestroyAllHandlers_m3923046273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HashSet_1_t2527432003 * L_0 = __this->get_m_Handlers_2();
		MonoBehaviourU5BU5D_t2007329276* L_1 = Enumerable_ToArray_TisMonoBehaviour_t3962482529_m4146969897(NULL /*static, unused*/, L_0, /*hidden argument*/Enumerable_ToArray_TisMonoBehaviour_t3962482529_m4146969897_MethodInfo_var);
		NativeAd_DestroyHandlers_m3057256890(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::<Activate>m__0()
extern "C"  void NativeAd_U3CActivateU3Em__0_m3793495835 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_U3CActivateU3Em__0_m3793495835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	{
		VirtActionInvoker0::Invoke(29 /* System.Void NendUnityPlugin.AD.Native.NativeAd::PerformAdClick() */, __this);
		EventHandler_t1348719766 * L_0 = __this->get_AdClicked_0();
		V_0 = L_0;
		EventHandler_t1348719766 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		EventHandler_t1348719766 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs_t3591816995 * L_3 = ((EventArgs_t3591816995_StaticFields*)EventArgs_t3591816995_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_2);
		EventHandler_Invoke_m2047579917(L_2, __this, L_3, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::<Activate>m__1()
extern "C"  void NativeAd_U3CActivateU3Em__1_m3793561371 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(30 /* System.Void NendUnityPlugin.AD.Native.NativeAd::PerformInformationClick() */, __this);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAd::<AddImpressionHandler>m__2()
extern "C"  void NativeAd_U3CAddImpressionHandlerU3Em__2_m2583096591 (NativeAd_t775405424 * __this, const MethodInfo* method)
{
	{
		__this->set_m_IsImpression_1((bool)1);
		VirtActionInvoker0::Invoke(28 /* System.Void NendUnityPlugin.AD.Native.NativeAd::SendImpression() */, __this);
		NativeAd_DestroyImpressionHandlers_m3505163566(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.NativeAd::<DestroyImpressionHandlers>m__3(UnityEngine.MonoBehaviour)
extern "C"  bool NativeAd_U3CDestroyImpressionHandlersU3Em__3_m3316364388 (Il2CppObject * __this /* static, unused */, MonoBehaviour_t3962482529 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAd_U3CDestroyImpressionHandlersU3Em__3_m3316364388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t3962482529 * L_0 = ___h0;
		return (bool)((!(((Il2CppObject*)(ImpressionHandler_t4278309981 *)((ImpressionHandler_t4278309981 *)IsInstClass(L_0, ImpressionHandler_t4278309981_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient::.ctor()
extern "C"  void NativeAdClient__ctor_m994092742 (NativeAdClient_t772434093 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAdClient__ctor_m994092742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Queue_1_t414173832 * L_0 = (Queue_1_t414173832 *)il2cpp_codegen_object_new(Queue_1_t414173832_il2cpp_TypeInfo_var);
		Queue_1__ctor_m2088921833(L_0, /*hidden argument*/Queue_1__ctor_m2088921833_MethodInfo_var);
		__this->set_m_Callbacks_3(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdMainThreadWorker_t3569218590_il2cpp_TypeInfo_var);
		NendAdMainThreadWorker_Prepare_m3075374016(NULL /*static, unused*/, /*hidden argument*/NULL);
		SimpleTimer_t4075384413 * L_1 = (SimpleTimer_t4075384413 *)il2cpp_codegen_object_new(SimpleTimer_t4075384413_il2cpp_TypeInfo_var);
		SimpleTimer__ctor_m1198240476(L_1, /*hidden argument*/NULL);
		__this->set_m_Timer_1(L_1);
		SimpleTimer_t4075384413 * L_2 = __this->get_m_Timer_1();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)NativeAdClient_U3CNativeAdClientU3Em__0_m3135026674_MethodInfo_var);
		Action_t1264377477 * L_4 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_OnFireEvent_0(L_4);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient::Finalize()
extern "C"  void NativeAdClient_Finalize_m2429945737 (NativeAdClient_t772434093 * __this, const MethodInfo* method)
{
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker0::Invoke(8 /* System.Void NendUnityPlugin.AD.Native.NativeAdClient::Dispose() */, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient::Dispose()
extern "C"  void NativeAdClient_Dispose_m1471321895 (NativeAdClient_t772434093 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAdClient_Dispose_m1471321895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral4115096556, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		Queue_1_t414173832 * L_0 = __this->get_m_Callbacks_3();
		NullCheck(L_0);
		Queue_1_Clear_m3541320262(L_0, /*hidden argument*/Queue_1_Clear_m3541320262_MethodInfo_var);
		SimpleTimer_t4075384413 * L_1 = __this->get_m_Timer_1();
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		SimpleTimer_t4075384413 * L_2 = __this->get_m_Timer_1();
		NullCheck(L_2);
		SimpleTimer_Dispose_m1569670885(L_2, /*hidden argument*/NULL);
		__this->set_m_Timer_1((SimpleTimer_t4075384413 *)NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient::LoadNativeAd(System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>)
extern "C"  void NativeAdClient_LoadNativeAd_m2739670713 (NativeAdClient_t772434093 * __this, Action_3_t567914338 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAdClient_LoadNativeAd_m2739670713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient::EnableAutoReload(System.Double,System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>)
extern "C"  void NativeAdClient_EnableAutoReload_m1436284653 (NativeAdClient_t772434093 * __this, double ___interval0, Action_3_t567914338 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAdClient_EnableAutoReload_m1436284653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = ___interval0;
		if ((!(((double)(30000.0)) <= ((double)L_0))))
		{
			goto IL_0027;
		}
	}
	{
		Action_3_t567914338 * L_1 = ___callback1;
		__this->set_m_ReloadCallback_2(L_1);
		SimpleTimer_t4075384413 * L_2 = __this->get_m_Timer_1();
		double L_3 = ___interval0;
		NullCheck(L_2);
		SimpleTimer_Start_m3197311368(L_2, L_3, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_W_m3326785758(NULL /*static, unused*/, _stringLiteral2269776268, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient::DisableAutoReload()
extern "C"  void NativeAdClient_DisableAutoReload_m2006054143 (NativeAdClient_t772434093 * __this, const MethodInfo* method)
{
	{
		SimpleTimer_t4075384413 * L_0 = __this->get_m_Timer_1();
		NullCheck(L_0);
		SimpleTimer_Stop_m1024974171(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient::EnqueueCallback(System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>)
extern "C"  void NativeAdClient_EnqueueCallback_m1636518071 (NativeAdClient_t772434093 * __this, Action_3_t567914338 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAdClient_EnqueueCallback_m1636518071_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Action_3_t567914338 * L_0 = ___callback0;
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		Queue_1_t414173832 * L_1 = __this->get_m_Callbacks_3();
		V_0 = L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		Queue_1_t414173832 * L_3 = __this->get_m_Callbacks_3();
		Action_3_t567914338 * L_4 = ___callback0;
		NullCheck(L_3);
		Queue_1_Enqueue_m2253724761(L_3, L_4, /*hidden argument*/Queue_1_Enqueue_m2253724761_MethodInfo_var);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient::DeliverResponseOnMainThread(NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String)
extern "C"  void NativeAdClient_DeliverResponseOnMainThread_m863452579 (NativeAdClient_t772434093 * __this, Il2CppObject * ___ad0, int32_t ___code1, String_t* ___message2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAdClient_DeliverResponseOnMainThread_m863452579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_0 = (U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 *)il2cpp_codegen_object_new(U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185_il2cpp_TypeInfo_var);
		U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1__ctor_m1980408661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_1 = V_0;
		Il2CppObject * L_2 = ___ad0;
		NullCheck(L_1);
		L_1->set_ad_0(L_2);
		U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_3 = V_0;
		int32_t L_4 = ___code1;
		NullCheck(L_3);
		L_3->set_code_1(L_4);
		U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_5 = V_0;
		String_t* L_6 = ___message2;
		NullCheck(L_5);
		L_5->set_message_2(L_6);
		Queue_1_t414173832 * L_7 = __this->get_m_Callbacks_3();
		V_1 = L_7;
		Il2CppObject * L_8 = V_1;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			Queue_1_t414173832 * L_9 = __this->get_m_Callbacks_3();
			NullCheck(L_9);
			int32_t L_10 = Queue_1_get_Count_m685314720(L_9, /*hidden argument*/Queue_1_get_Count_m685314720_MethodInfo_var);
			if ((((int32_t)0) >= ((int32_t)L_10)))
			{
				goto IL_00bf;
			}
		}

IL_0039:
		{
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321 * L_11 = (U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321 *)il2cpp_codegen_object_new(U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321_il2cpp_TypeInfo_var);
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0__ctor_m1857004373(L_11, /*hidden argument*/NULL);
			V_2 = L_11;
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321 * L_12 = V_2;
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_13 = V_0;
			NullCheck(L_12);
			L_12->set_U3CU3Ef__refU241_1(L_13);
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_14 = V_0;
			NullCheck(L_14);
			Il2CppObject * L_15 = L_14->get_ad_0();
			if (!L_15)
			{
				goto IL_0098;
			}
		}

IL_0051:
		{
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_16 = V_0;
			NullCheck(L_16);
			Il2CppObject * L_17 = L_16->get_ad_0();
			NullCheck(L_17);
			String_t* L_18 = InterfaceFuncInvoker0< String_t* >::Invoke(5 /* System.String NendUnityPlugin.AD.Native.INativeAd::get_AdImageUrl() */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_17);
			bool L_19 = NativeAdClient_IsGifImage_m3010410997(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			if (L_19)
			{
				goto IL_007b;
			}
		}

IL_0066:
		{
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_20 = V_0;
			NullCheck(L_20);
			Il2CppObject * L_21 = L_20->get_ad_0();
			NullCheck(L_21);
			String_t* L_22 = InterfaceFuncInvoker0< String_t* >::Invoke(6 /* System.String NendUnityPlugin.AD.Native.INativeAd::get_LogoImageUrl() */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_21);
			bool L_23 = NativeAdClient_IsGifImage_m3010410997(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_0098;
			}
		}

IL_007b:
		{
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_24 = V_0;
			NullCheck(L_24);
			L_24->set_ad_0((Il2CppObject *)NULL);
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_25 = V_0;
			NullCheck(L_25);
			L_25->set_code_1(((int32_t)342));
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_26 = V_0;
			NullCheck(L_26);
			L_26->set_message_2(_stringLiteral1896225283);
		}

IL_0098:
		{
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321 * L_27 = V_2;
			Queue_1_t414173832 * L_28 = __this->get_m_Callbacks_3();
			NullCheck(L_28);
			Action_3_t567914338 * L_29 = Queue_1_Dequeue_m1251791087(L_28, /*hidden argument*/Queue_1_Dequeue_m1251791087_MethodInfo_var);
			NullCheck(L_27);
			L_27->set_callback_0(L_29);
			IL2CPP_RUNTIME_CLASS_INIT(NendAdMainThreadWorker_t3569218590_il2cpp_TypeInfo_var);
			NendAdMainThreadWorker_t3569218590 * L_30 = NendAdMainThreadWorker_get_Instance_m443439756(NULL /*static, unused*/, /*hidden argument*/NULL);
			U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321 * L_31 = V_2;
			IntPtr_t L_32;
			L_32.set_m_value_0((void*)(void*)U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_U3CU3Em__0_m3728349486_MethodInfo_var);
			Action_t1264377477 * L_33 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
			Action__ctor_m2994342681(L_33, L_31, L_32, /*hidden argument*/NULL);
			NullCheck(L_30);
			NendAdMainThreadWorker_Post_m1274596709(L_30, L_33, /*hidden argument*/NULL);
		}

IL_00bf:
		{
			IL2CPP_LEAVE(0xCB, FINALLY_00c4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_00c4;
	}

FINALLY_00c4:
	{ // begin finally (depth: 1)
		Il2CppObject * L_34 = V_1;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(196)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(196)
	{
		IL2CPP_JUMP_TBL(0xCB, IL_00cb)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_00cb:
	{
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.NativeAdClient::IsGifImage(System.String)
extern "C"  bool NativeAdClient_IsGifImage_m3010410997 (Il2CppObject * __this /* static, unused */, String_t* ___imageUrl0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAdClient_IsGifImage_m3010410997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___imageUrl0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_2 = ___imageUrl0;
		NullCheck(L_2);
		String_t* L_3 = String_ToLower_m2029374922(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = String_EndsWith_m1901926500(L_3, _stringLiteral3988015567, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient::<NativeAdClient>m__0()
extern "C"  void NativeAdClient_U3CNativeAdClientU3Em__0_m3135026674 (NativeAdClient_t772434093 * __this, const MethodInfo* method)
{
	{
		Action_3_t567914338 * L_0 = __this->get_m_ReloadCallback_2();
		VirtActionInvoker1< Action_3_t567914338 * >::Invoke(9 /* System.Void NendUnityPlugin.AD.Native.NativeAdClient::LoadNativeAd(System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>) */, __this, L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey0::.ctor()
extern "C"  void U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0__ctor_m1857004373 (U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey0::<>m__0()
extern "C"  void U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_U3CU3Em__0_m3728349486 (U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_U3CU3Em__0_m3728349486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_3_t567914338 * L_0 = __this->get_callback_0();
		U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_1 = __this->get_U3CU3Ef__refU241_1();
		NullCheck(L_1);
		Il2CppObject * L_2 = L_1->get_ad_0();
		U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_3 = __this->get_U3CU3Ef__refU241_1();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_code_1();
		U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * L_5 = __this->get_U3CU3Ef__refU241_1();
		NullCheck(L_5);
		String_t* L_6 = L_5->get_message_2();
		NullCheck(L_0);
		Action_3_Invoke_m1359513622(L_0, L_2, L_4, L_6, /*hidden argument*/Action_3_Invoke_m1359513622_MethodInfo_var);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClient/<DeliverResponseOnMainThread>c__AnonStorey1::.ctor()
extern "C"  void U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1__ctor_m1980408661 (U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NativeAdClientFactory::.ctor()
extern "C"  void NativeAdClientFactory__ctor_m4219450854 (NativeAdClientFactory_t4254168860 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// NendUnityPlugin.AD.Native.INativeAdClient NendUnityPlugin.AD.Native.NativeAdClientFactory::NewClient(System.String,System.String)
extern "C"  Il2CppObject * NativeAdClientFactory_NewClient_m1082690577 (Il2CppObject * __this /* static, unused */, String_t* ___spotId0, String_t* ___apiKey1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeAdClientFactory_NewClient_m1082690577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___apiKey1;
		String_t* L_1 = ___spotId0;
		IOSNativeAdClient_t1155913684 * L_2 = (IOSNativeAdClient_t1155913684 *)il2cpp_codegen_object_new(IOSNativeAdClient_t1155913684_il2cpp_TypeInfo_var);
		IOSNativeAdClient__ctor_m3085587387(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::.ctor()
extern "C"  void NendAdNative__ctor_m2031526427 (NendAdNative_t3381739006 * __this, const MethodInfo* method)
{
	{
		__this->set_loadWhenActivated_3((bool)1);
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// NendUnityPlugin.AD.Native.INativeAdClient NendUnityPlugin.AD.Native.NendAdNative::get_Client()
extern "C"  Il2CppObject * NendAdNative_get_Client_m3224855641 (NendAdNative_t3381739006 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_m_Client_9();
		Il2CppObject * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_2 = NendAdNative_CreateClient_m4286713017(__this, /*hidden argument*/NULL);
		Il2CppObject * L_3 = L_2;
		V_0 = L_3;
		__this->set_m_Client_9(L_3);
		Il2CppObject * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001c:
	{
		return G_B2_0;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::Start()
extern "C"  void NendAdNative_Start_m2400952537 (NendAdNative_t3381739006 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNative_Start_m2400952537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SimpleTimer_t4075384413 * L_0 = (SimpleTimer_t4075384413 *)il2cpp_codegen_object_new(SimpleTimer_t4075384413_il2cpp_TypeInfo_var);
		SimpleTimer__ctor_m1198240476(L_0, /*hidden argument*/NULL);
		__this->set_m_Timer_10(L_0);
		SimpleTimer_t4075384413 * L_1 = __this->get_m_Timer_10();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)NendAdNative_U3CStartU3Em__0_m1547599261_MethodInfo_var);
		Action_t1264377477 * L_3 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_OnFireEvent_0(L_3);
		bool L_4 = __this->get_loadWhenActivated_3();
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		NendAdNative_LoadAd_m89270426(__this, /*hidden argument*/NULL);
	}

IL_0033:
	{
		bool L_5 = __this->get_enableAutoReload_4();
		if (!L_5)
		{
			goto IL_004a;
		}
	}
	{
		double L_6 = __this->get_reloadInterval_5();
		NendAdNative_EnableAutoReload_m3562877551(__this, L_6, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::OnDestroy()
extern "C"  void NendAdNative_OnDestroy_m1949950267 (NendAdNative_t3381739006 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNative_OnDestroy_m1949950267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_1 = Object_get_name_m1414505214(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral2657966006, L_0, /*hidden argument*/NULL);
		SimpleTimer_t4075384413 * L_2 = __this->get_m_Timer_10();
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		SimpleTimer_t4075384413 * L_3 = __this->get_m_Timer_10();
		NullCheck(L_3);
		SimpleTimer_Dispose_m1569670885(L_3, /*hidden argument*/NULL);
		__this->set_m_Timer_10((SimpleTimer_t4075384413 *)NULL);
	}

IL_0036:
	{
		__this->set_m_Client_9((Il2CppObject *)NULL);
		__this->set_views_6((NendAdNativeViewU5BU5D_t3641134489*)NULL);
		return;
	}
}
// NendUnityPlugin.AD.Native.NendAdNativeView[] NendUnityPlugin.AD.Native.NendAdNative::get_Views()
extern "C"  NendAdNativeViewU5BU5D_t3641134489* NendAdNative_get_Views_m3597242147 (NendAdNative_t3381739006 * __this, const MethodInfo* method)
{
	{
		NendAdNativeViewU5BU5D_t3641134489* L_0 = __this->get_views_6();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::RegisterAdView(NendUnityPlugin.AD.Native.NendAdNativeView)
extern "C"  void NendAdNative_RegisterAdView_m2253752544 (NendAdNative_t3381739006 * __this, NendAdNativeView_t2014609480 * ___view0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNative_RegisterAdView_m2253752544_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CRegisterAdViewU3Ec__AnonStorey0_t799839499 * V_0 = NULL;
	{
		U3CRegisterAdViewU3Ec__AnonStorey0_t799839499 * L_0 = (U3CRegisterAdViewU3Ec__AnonStorey0_t799839499 *)il2cpp_codegen_object_new(U3CRegisterAdViewU3Ec__AnonStorey0_t799839499_il2cpp_TypeInfo_var);
		U3CRegisterAdViewU3Ec__AnonStorey0__ctor_m371496142(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRegisterAdViewU3Ec__AnonStorey0_t799839499 * L_1 = V_0;
		NendAdNativeView_t2014609480 * L_2 = ___view0;
		NullCheck(L_1);
		L_1->set_view_0(L_2);
		U3CRegisterAdViewU3Ec__AnonStorey0_t799839499 * L_3 = V_0;
		NullCheck(L_3);
		NendAdNativeView_t2014609480 * L_4 = L_3->get_view_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005f;
		}
	}
	{
		NendAdNativeViewU5BU5D_t3641134489* L_6 = __this->get_views_6();
		U3CRegisterAdViewU3Ec__AnonStorey0_t799839499 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CRegisterAdViewU3Ec__AnonStorey0_U3CU3Em__0_m790825469_MethodInfo_var);
		Func_2_t261507339 * L_9 = (Func_2_t261507339 *)il2cpp_codegen_object_new(Func_2_t261507339_il2cpp_TypeInfo_var);
		Func_2__ctor_m3969891916(L_9, L_7, L_8, /*hidden argument*/Func_2__ctor_m3969891916_MethodInfo_var);
		bool L_10 = Enumerable_All_TisNendAdNativeView_t2014609480_m2632065831(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_6, L_9, /*hidden argument*/Enumerable_All_TisNendAdNativeView_t2014609480_m2632065831_MethodInfo_var);
		if (!L_10)
		{
			goto IL_005f;
		}
	}
	{
		NendAdNativeViewU5BU5D_t3641134489* L_11 = __this->get_views_6();
		NendAdNativeViewU5BU5D_t3641134489* L_12 = ((NendAdNativeViewU5BU5D_t3641134489*)SZArrayNew(NendAdNativeViewU5BU5D_t3641134489_il2cpp_TypeInfo_var, (uint32_t)1));
		U3CRegisterAdViewU3Ec__AnonStorey0_t799839499 * L_13 = V_0;
		NullCheck(L_13);
		NendAdNativeView_t2014609480 * L_14 = L_13->get_view_0();
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (NendAdNativeView_t2014609480 *)L_14);
		Il2CppObject* L_15 = Enumerable_Concat_TisNendAdNativeView_t2014609480_m60162064(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_11, (Il2CppObject*)(Il2CppObject*)L_12, /*hidden argument*/Enumerable_Concat_TisNendAdNativeView_t2014609480_m60162064_MethodInfo_var);
		NendAdNativeViewU5BU5D_t3641134489* L_16 = Enumerable_ToArray_TisNendAdNativeView_t2014609480_m230526467(NULL /*static, unused*/, L_15, /*hidden argument*/Enumerable_ToArray_TisNendAdNativeView_t2014609480_m230526467_MethodInfo_var);
		__this->set_views_6(L_16);
	}

IL_005f:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::UnregisterAdView(NendUnityPlugin.AD.Native.NendAdNativeView)
extern "C"  void NendAdNative_UnregisterAdView_m3352783016 (NendAdNative_t3381739006 * __this, NendAdNativeView_t2014609480 * ___view0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNative_UnregisterAdView_m3352783016_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		NendAdNativeView_t2014609480 * L_0 = ___view0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		NendAdNativeViewU5BU5D_t3641134489* L_2 = __this->get_views_6();
		NendAdNativeView_t2014609480 * L_3 = ___view0;
		int32_t L_4 = Array_IndexOf_TisNendAdNativeView_t2014609480_m792692718(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/Array_IndexOf_TisNendAdNativeView_t2014609480_m792692718_MethodInfo_var);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)(-1))))
		{
			goto IL_002d;
		}
	}
	{
		NendAdNativeViewU5BU5D_t3641134489* L_6 = __this->get_views_6();
		int32_t L_7 = V_0;
		Array_Clear_m2231608178(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, L_7, 1, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::LoadAd()
extern "C"  void NendAdNative_LoadAd_m89270426 (NendAdNative_t3381739006 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNative_LoadAd_m89270426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdNativeViewU5BU5D_t3641134489* G_B4_0 = NULL;
	NendAdNative_t3381739006 * G_B4_1 = NULL;
	NendAdNativeViewU5BU5D_t3641134489* G_B3_0 = NULL;
	NendAdNative_t3381739006 * G_B3_1 = NULL;
	{
		NendAdNativeViewU5BU5D_t3641134489* L_0 = __this->get_views_6();
		if (!L_0)
		{
			goto IL_0056;
		}
	}
	{
		NendAdNativeViewU5BU5D_t3641134489* L_1 = __this->get_views_6();
		NullCheck(L_1);
		if ((((int32_t)0) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))
		{
			goto IL_0056;
		}
	}
	{
		NendAdNativeViewU5BU5D_t3641134489* L_2 = __this->get_views_6();
		Func_2_t261507339 * L_3 = ((NendAdNative_t3381739006_StaticFields*)NendAdNative_t3381739006_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_11();
		G_B3_0 = L_2;
		G_B3_1 = __this;
		if (L_3)
		{
			G_B4_0 = L_2;
			G_B4_1 = __this;
			goto IL_0038;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)NendAdNative_U3CLoadAdU3Em__1_m4053525341_MethodInfo_var);
		Func_2_t261507339 * L_5 = (Func_2_t261507339 *)il2cpp_codegen_object_new(Func_2_t261507339_il2cpp_TypeInfo_var);
		Func_2__ctor_m3969891916(L_5, NULL, L_4, /*hidden argument*/Func_2__ctor_m3969891916_MethodInfo_var);
		((NendAdNative_t3381739006_StaticFields*)NendAdNative_t3381739006_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_11(L_5);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0038:
	{
		Func_2_t261507339 * L_6 = ((NendAdNative_t3381739006_StaticFields*)NendAdNative_t3381739006_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_11();
		Il2CppObject* L_7 = Enumerable_Where_TisNendAdNativeView_t2014609480_m876349505(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B4_0, L_6, /*hidden argument*/Enumerable_Where_TisNendAdNativeView_t2014609480_m876349505_MethodInfo_var);
		Il2CppObject* L_8 = Enumerable_DefaultIfEmpty_TisNendAdNativeView_t2014609480_m3560641153(NULL /*static, unused*/, L_7, /*hidden argument*/Enumerable_DefaultIfEmpty_TisNendAdNativeView_t2014609480_m3560641153_MethodInfo_var);
		NendAdNativeViewU5BU5D_t3641134489* L_9 = Enumerable_ToArray_TisNendAdNativeView_t2014609480_m230526467(NULL /*static, unused*/, L_8, /*hidden argument*/Enumerable_ToArray_TisNendAdNativeView_t2014609480_m230526467_MethodInfo_var);
		NullCheck(G_B4_1);
		NendAdNative_LoadAd_m545678891(G_B4_1, L_9, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_W_m3326785758(NULL /*static, unused*/, _stringLiteral1803801646, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::LoadAd(System.Int32)
extern "C"  void NendAdNative_LoadAd_m2968873072 (NendAdNative_t3381739006 * __this, int32_t ___tag0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNative_LoadAd_m2968873072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadAdU3Ec__AnonStorey1_t1337685977 * V_0 = NULL;
	{
		U3CLoadAdU3Ec__AnonStorey1_t1337685977 * L_0 = (U3CLoadAdU3Ec__AnonStorey1_t1337685977 *)il2cpp_codegen_object_new(U3CLoadAdU3Ec__AnonStorey1_t1337685977_il2cpp_TypeInfo_var);
		U3CLoadAdU3Ec__AnonStorey1__ctor_m715144880(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadAdU3Ec__AnonStorey1_t1337685977 * L_1 = V_0;
		int32_t L_2 = ___tag0;
		NullCheck(L_1);
		L_1->set_tag_0(L_2);
		NendAdNativeViewU5BU5D_t3641134489* L_3 = __this->get_views_6();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		NendAdNativeViewU5BU5D_t3641134489* L_4 = __this->get_views_6();
		NullCheck(L_4);
		if ((((int32_t)0) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		NendAdNativeViewU5BU5D_t3641134489* L_5 = __this->get_views_6();
		U3CLoadAdU3Ec__AnonStorey1_t1337685977 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CLoadAdU3Ec__AnonStorey1_U3CU3Em__0_m2659668234_MethodInfo_var);
		Func_2_t261507339 * L_8 = (Func_2_t261507339 *)il2cpp_codegen_object_new(Func_2_t261507339_il2cpp_TypeInfo_var);
		Func_2__ctor_m3969891916(L_8, L_6, L_7, /*hidden argument*/Func_2__ctor_m3969891916_MethodInfo_var);
		Il2CppObject* L_9 = Enumerable_Where_TisNendAdNativeView_t2014609480_m876349505(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_5, L_8, /*hidden argument*/Enumerable_Where_TisNendAdNativeView_t2014609480_m876349505_MethodInfo_var);
		Il2CppObject* L_10 = Enumerable_DefaultIfEmpty_TisNendAdNativeView_t2014609480_m3560641153(NULL /*static, unused*/, L_9, /*hidden argument*/Enumerable_DefaultIfEmpty_TisNendAdNativeView_t2014609480_m3560641153_MethodInfo_var);
		NendAdNativeViewU5BU5D_t3641134489* L_11 = Enumerable_ToArray_TisNendAdNativeView_t2014609480_m230526467(NULL /*static, unused*/, L_10, /*hidden argument*/Enumerable_ToArray_TisNendAdNativeView_t2014609480_m230526467_MethodInfo_var);
		NendAdNative_LoadAd_m545678891(__this, L_11, /*hidden argument*/NULL);
		goto IL_0062;
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_W_m3326785758(NULL /*static, unused*/, _stringLiteral1803801646, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::EnableAutoReload(System.Double)
extern "C"  void NendAdNative_EnableAutoReload_m3562877551 (NendAdNative_t3381739006 * __this, double ___interval0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNative_EnableAutoReload_m3562877551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = ___interval0;
		if ((!(((double)(30000.0)) <= ((double)L_0))))
		{
			goto IL_0020;
		}
	}
	{
		SimpleTimer_t4075384413 * L_1 = __this->get_m_Timer_10();
		double L_2 = ___interval0;
		NullCheck(L_1);
		SimpleTimer_Start_m3197311368(L_1, L_2, /*hidden argument*/NULL);
		goto IL_0030;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_W_m3326785758(NULL /*static, unused*/, _stringLiteral2269776268, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::DisableAutoReload()
extern "C"  void NendAdNative_DisableAutoReload_m2341228960 (NendAdNative_t3381739006 * __this, const MethodInfo* method)
{
	{
		SimpleTimer_t4075384413 * L_0 = __this->get_m_Timer_10();
		NullCheck(L_0);
		SimpleTimer_Stop_m1024974171(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::LoadAd(NendUnityPlugin.AD.Native.NendAdNativeView[])
extern "C"  void NendAdNative_LoadAd_m545678891 (NendAdNative_t3381739006 * __this, NendAdNativeViewU5BU5D_t3641134489* ___views0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNative_LoadAd_m545678891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdNativeView_t2014609480 * V_0 = NULL;
	NendAdNativeViewU5BU5D_t3641134489* V_1 = NULL;
	int32_t V_2 = 0;
	U3CLoadAdU3Ec__AnonStorey2_t1337685978 * V_3 = NULL;
	{
		NendAdNativeViewU5BU5D_t3641134489* L_0 = ___views0;
		V_1 = L_0;
		V_2 = 0;
		goto IL_004d;
	}

IL_0009:
	{
		NendAdNativeViewU5BU5D_t3641134489* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		NendAdNativeView_t2014609480 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		U3CLoadAdU3Ec__AnonStorey2_t1337685978 * L_5 = (U3CLoadAdU3Ec__AnonStorey2_t1337685978 *)il2cpp_codegen_object_new(U3CLoadAdU3Ec__AnonStorey2_t1337685978_il2cpp_TypeInfo_var);
		U3CLoadAdU3Ec__AnonStorey2__ctor_m715147817(L_5, /*hidden argument*/NULL);
		V_3 = L_5;
		U3CLoadAdU3Ec__AnonStorey2_t1337685978 * L_6 = V_3;
		NullCheck(L_6);
		L_6->set_U24this_1(__this);
		NendAdNativeView_t2014609480 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_0049;
	}

IL_002b:
	{
		U3CLoadAdU3Ec__AnonStorey2_t1337685978 * L_9 = V_3;
		NendAdNativeView_t2014609480 * L_10 = V_0;
		NullCheck(L_9);
		L_9->set_nativeAdView_0(L_10);
		Il2CppObject * L_11 = NendAdNative_get_Client_m3224855641(__this, /*hidden argument*/NULL);
		U3CLoadAdU3Ec__AnonStorey2_t1337685978 * L_12 = V_3;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)U3CLoadAdU3Ec__AnonStorey2_U3CU3Em__0_m1756703808_MethodInfo_var);
		Action_3_t567914338 * L_14 = (Action_3_t567914338 *)il2cpp_codegen_object_new(Action_3_t567914338_il2cpp_TypeInfo_var);
		Action_3__ctor_m1315864708(L_14, L_12, L_13, /*hidden argument*/Action_3__ctor_m1315864708_MethodInfo_var);
		NullCheck(L_11);
		InterfaceActionInvoker1< Action_3_t567914338 * >::Invoke(0 /* System.Void NendUnityPlugin.AD.Native.INativeAdClient::LoadNativeAd(System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>) */, INativeAdClient_t1973857504_il2cpp_TypeInfo_var, L_11, L_14);
	}

IL_0049:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_16 = V_2;
		NendAdNativeViewU5BU5D_t3641134489* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0009;
		}
	}
	{
		return;
	}
}
// NendUnityPlugin.AD.Native.INativeAdClient NendUnityPlugin.AD.Native.NendAdNative::CreateClient()
extern "C"  Il2CppObject * NendAdNative_CreateClient_m4286713017 (NendAdNative_t3381739006 * __this, const MethodInfo* method)
{
	{
		Account_t2230227361 * L_0 = __this->get_account_2();
		NullCheck(L_0);
		NendID_t3358092198 * L_1 = L_0->get_iOS_1();
		NullCheck(L_1);
		int32_t* L_2 = L_1->get_address_of_spotID_1();
		String_t* L_3 = Int32_ToString_m141394615(L_2, /*hidden argument*/NULL);
		Account_t2230227361 * L_4 = __this->get_account_2();
		NullCheck(L_4);
		NendID_t3358092198 * L_5 = L_4->get_iOS_1();
		NullCheck(L_5);
		String_t* L_6 = L_5->get_apiKey_0();
		Il2CppObject * L_7 = NativeAdClientFactory_NewClient_m1082690577(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::PostAdLoaded(NendUnityPlugin.AD.Native.NendAdNativeView)
extern "C"  void NendAdNative_PostAdLoaded_m3099863989 (NendAdNative_t3381739006 * __this, NendAdNativeView_t2014609480 * ___view0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNative_PostAdLoaded_m3099863989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeAdViewEvent_t610464567 * L_0 = __this->get_AdLoaded_7();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		NativeAdViewEvent_t610464567 * L_1 = __this->get_AdLoaded_7();
		NendAdNativeView_t2014609480 * L_2 = ___view0;
		NullCheck(L_1);
		UnityEvent_1_Invoke_m2488769038(L_1, L_2, /*hidden argument*/UnityEvent_1_Invoke_m2488769038_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::PostAdFailedToReceive(NendUnityPlugin.AD.Native.NendAdNativeView,System.Int32,System.String)
extern "C"  void NendAdNative_PostAdFailedToReceive_m837728790 (NendAdNative_t3381739006 * __this, NendAdNativeView_t2014609480 * ___view0, int32_t ___code1, String_t* ___message2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNative_PostAdFailedToReceive_m837728790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeAdViewFailedToLoadEvent_t3307153462 * L_0 = __this->get_AdFailedToReceive_8();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		NativeAdViewFailedToLoadEvent_t3307153462 * L_1 = __this->get_AdFailedToReceive_8();
		NendAdNativeView_t2014609480 * L_2 = ___view0;
		int32_t L_3 = ___code1;
		String_t* L_4 = ___message2;
		NullCheck(L_1);
		UnityEvent_3_Invoke_m1929593389(L_1, L_2, L_3, L_4, /*hidden argument*/UnityEvent_3_Invoke_m1929593389_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative::<Start>m__0()
extern "C"  void NendAdNative_U3CStartU3Em__0_m1547599261 (NendAdNative_t3381739006 * __this, const MethodInfo* method)
{
	{
		NendAdNative_LoadAd_m89270426(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.NendAdNative::<LoadAd>m__1(NendUnityPlugin.AD.Native.NendAdNativeView)
extern "C"  bool NendAdNative_U3CLoadAdU3Em__1_m4053525341 (Il2CppObject * __this /* static, unused */, NendAdNativeView_t2014609480 * ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNative_U3CLoadAdU3Em__1_m4053525341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NendAdNativeView_t2014609480 * L_0 = ___v0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey1::.ctor()
extern "C"  void U3CLoadAdU3Ec__AnonStorey1__ctor_m715144880 (U3CLoadAdU3Ec__AnonStorey1_t1337685977 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey1::<>m__0(NendUnityPlugin.AD.Native.NendAdNativeView)
extern "C"  bool U3CLoadAdU3Ec__AnonStorey1_U3CU3Em__0_m2659668234 (U3CLoadAdU3Ec__AnonStorey1_t1337685977 * __this, NendAdNativeView_t2014609480 * ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadAdU3Ec__AnonStorey1_U3CU3Em__0_m2659668234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		NendAdNativeView_t2014609480 * L_0 = ___v0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		NendAdNativeView_t2014609480 * L_2 = ___v0;
		NullCheck(L_2);
		int32_t L_3 = NendAdNativeView_get_ViewTag_m1321846746(L_2, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_tag_0();
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
		goto IL_001d;
	}

IL_001c:
	{
		G_B3_0 = 0;
	}

IL_001d:
	{
		return (bool)G_B3_0;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey2::.ctor()
extern "C"  void U3CLoadAdU3Ec__AnonStorey2__ctor_m715147817 (U3CLoadAdU3Ec__AnonStorey2_t1337685978 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey2::<>m__0(NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String)
extern "C"  void U3CLoadAdU3Ec__AnonStorey2_U3CU3Em__0_m1756703808 (U3CLoadAdU3Ec__AnonStorey2_t1337685978 * __this, Il2CppObject * ___nativeAd0, int32_t ___code1, String_t* ___message2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadAdU3Ec__AnonStorey2_U3CU3Em__0_m1756703808_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___nativeAd0;
		if (!L_0)
		{
			goto IL_004b;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_1 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		NendAdNativeView_t2014609480 * L_2 = __this->get_nativeAdView_0();
		NullCheck(L_2);
		int32_t L_3 = NendAdNativeView_get_ViewTag_m1321846746(L_2, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_I_m2000148771(NULL /*static, unused*/, _stringLiteral1978750441, L_1, /*hidden argument*/NULL);
		NendAdNativeView_t2014609480 * L_6 = __this->get_nativeAdView_0();
		Il2CppObject * L_7 = ___nativeAd0;
		NullCheck(L_6);
		NendAdNativeView_OnLoadAd_m3850891744(L_6, L_7, /*hidden argument*/NULL);
		NendAdNative_t3381739006 * L_8 = __this->get_U24this_1();
		NendAdNativeView_t2014609480 * L_9 = __this->get_nativeAdView_0();
		NullCheck(L_8);
		NendAdNative_PostAdLoaded_m3099863989(L_8, L_9, /*hidden argument*/NULL);
		goto IL_007b;
	}

IL_004b:
	{
		ObjectU5BU5D_t2843939325* L_10 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_11 = ___code1;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_13);
		ObjectU5BU5D_t2843939325* L_14 = L_10;
		String_t* L_15 = ___message2;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_E_m3544237484(NULL /*static, unused*/, _stringLiteral2588296612, L_14, /*hidden argument*/NULL);
		NendAdNative_t3381739006 * L_16 = __this->get_U24this_1();
		NendAdNativeView_t2014609480 * L_17 = __this->get_nativeAdView_0();
		int32_t L_18 = ___code1;
		String_t* L_19 = ___message2;
		NullCheck(L_16);
		NendAdNative_PostAdFailedToReceive_m837728790(L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNative/<RegisterAdView>c__AnonStorey0::.ctor()
extern "C"  void U3CRegisterAdViewU3Ec__AnonStorey0__ctor_m371496142 (U3CRegisterAdViewU3Ec__AnonStorey0_t799839499 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.NendAdNative/<RegisterAdView>c__AnonStorey0::<>m__0(NendUnityPlugin.AD.Native.NendAdNativeView)
extern "C"  bool U3CRegisterAdViewU3Ec__AnonStorey0_U3CU3Em__0_m790825469 (U3CRegisterAdViewU3Ec__AnonStorey0_t799839499 * __this, NendAdNativeView_t2014609480 * ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRegisterAdViewU3Ec__AnonStorey0_U3CU3Em__0_m790825469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NendAdNativeView_t2014609480 * L_0 = ___v0;
		NendAdNativeView_t2014609480 * L_1 = __this->get_view_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeImage::.ctor()
extern "C"  void NendAdNativeImage__ctor_m2060349043 (NendAdNativeImage_t2295075562 * __this, const MethodInfo* method)
{
	{
		RawImage__ctor_m2348784658(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeText::.ctor()
extern "C"  void NendAdNativeText__ctor_m988110989 (NendAdNativeText_t87332621 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNativeText__ctor_m988110989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Text_t1901882714_il2cpp_TypeInfo_var);
		Text__ctor_m1150387577(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::.ctor()
extern "C"  void NendAdNativeView__ctor_m216573258 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method)
{
	{
		__this->set_renderWhenLoaded_3((bool)1);
		__this->set_renderWhenActivated_4((bool)1);
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 NendUnityPlugin.AD.Native.NendAdNativeView::get_ViewTag()
extern "C"  int32_t NendAdNativeView_get_ViewTag_m1321846746 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_viewTag_2();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::set_ViewTag(System.Int32)
extern "C"  void NendAdNativeView_set_ViewTag_m1986678880 (NendAdNativeView_t2014609480 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_viewTag_2(L_0);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.NendAdNativeView::get_Loaded()
extern "C"  bool NendAdNativeView_get_Loaded_m2218797704 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_m_LoadedNativeAd_17();
		return (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject *)NULL) == ((Il2CppObject*)(Il2CppObject *)L_0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::set_ShowingNativeAd(NendUnityPlugin.AD.Native.INativeAd)
extern "C"  void NendAdNativeView_set_ShowingNativeAd_m553901933 (NendAdNativeView_t2014609480 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNativeView_set_ShowingNativeAd_m553901933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_m_ShowingNativeAd_18();
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_m_ShowingNativeAd_18();
		Il2CppObject * L_2 = ___value0;
		if ((((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2)))
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral2455803803, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		Il2CppObject * L_3 = __this->get_m_ShowingNativeAd_18();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)NendAdNativeView_OnClickAd_m2443000380_MethodInfo_var);
		EventHandler_t1348719766 * L_5 = (EventHandler_t1348719766 *)il2cpp_codegen_object_new(EventHandler_t1348719766_il2cpp_TypeInfo_var);
		EventHandler__ctor_m3449229857(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceActionInvoker1< EventHandler_t1348719766 * >::Invoke(13 /* System.Void NendUnityPlugin.AD.Native.INativeAd::remove_AdClicked(System.EventHandler) */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_3, L_5);
	}

IL_003e:
	{
		Il2CppObject * L_6 = ___value0;
		__this->set_m_ShowingNativeAd_18(L_6);
		Text_t1901882714 * L_7 = __this->get_prText_6();
		Il2CppObject * L_8 = __this->get_m_ShowingNativeAd_18();
		int32_t L_9 = __this->get_advertisingExplicitly_5();
		NullCheck(L_8);
		String_t* L_10 = InterfaceFuncInvoker1< String_t*, int32_t >::Invoke(7 /* System.String NendUnityPlugin.AD.Native.INativeAd::GetAdvertisingExplicitlyText(NendUnityPlugin.AD.Native.AdvertisingExplicitly) */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_8, L_9);
		UIRenderer_TryRenderText_m3187668248(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		Text_t1901882714 * L_11 = __this->get_shortText_7();
		Il2CppObject * L_12 = __this->get_m_ShowingNativeAd_18();
		NullCheck(L_12);
		String_t* L_13 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String NendUnityPlugin.AD.Native.INativeAd::get_ShortText() */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_12);
		UIRenderer_TryRenderText_m3187668248(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
		Text_t1901882714 * L_14 = __this->get_longText_8();
		Il2CppObject * L_15 = __this->get_m_ShowingNativeAd_18();
		NullCheck(L_15);
		String_t* L_16 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String NendUnityPlugin.AD.Native.INativeAd::get_LongText() */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_15);
		UIRenderer_TryRenderText_m3187668248(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
		Text_t1901882714 * L_17 = __this->get_promotionNameText_9();
		Il2CppObject * L_18 = __this->get_m_ShowingNativeAd_18();
		NullCheck(L_18);
		String_t* L_19 = InterfaceFuncInvoker0< String_t* >::Invoke(3 /* System.String NendUnityPlugin.AD.Native.INativeAd::get_PromotionName() */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_18);
		UIRenderer_TryRenderText_m3187668248(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		Text_t1901882714 * L_20 = __this->get_promotionUrlText_10();
		Il2CppObject * L_21 = __this->get_m_ShowingNativeAd_18();
		NullCheck(L_21);
		String_t* L_22 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String NendUnityPlugin.AD.Native.INativeAd::get_PromotionUrl() */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_21);
		UIRenderer_TryRenderText_m3187668248(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		Text_t1901882714 * L_23 = __this->get_actionButtonText_11();
		Il2CppObject * L_24 = __this->get_m_ShowingNativeAd_18();
		NullCheck(L_24);
		String_t* L_25 = InterfaceFuncInvoker0< String_t* >::Invoke(4 /* System.String NendUnityPlugin.AD.Native.INativeAd::get_ActionButtonText() */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_24);
		UIRenderer_TryRenderText_m3187668248(NULL /*static, unused*/, L_23, L_25, /*hidden argument*/NULL);
		RawImage_t3182918964 * L_26 = __this->get_adImage_12();
		Il2CppObject * L_27 = __this->get_m_ShowingNativeAd_18();
		NullCheck(L_27);
		String_t* L_28 = InterfaceFuncInvoker0< String_t* >::Invoke(5 /* System.String NendUnityPlugin.AD.Native.INativeAd::get_AdImageUrl() */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_27);
		UIRenderer_TryRenderImage_m737991264(NULL /*static, unused*/, __this, L_26, L_28, /*hidden argument*/NULL);
		RawImage_t3182918964 * L_29 = __this->get_logoImage_13();
		Il2CppObject * L_30 = __this->get_m_ShowingNativeAd_18();
		NullCheck(L_30);
		String_t* L_31 = InterfaceFuncInvoker0< String_t* >::Invoke(6 /* System.String NendUnityPlugin.AD.Native.INativeAd::get_LogoImageUrl() */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_30);
		UIRenderer_TryRenderImage_m737991264(NULL /*static, unused*/, __this, L_29, L_31, /*hidden argument*/NULL);
		Il2CppObject * L_32 = __this->get_m_ShowingNativeAd_18();
		GameObject_t1113636619 * L_33 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		Text_t1901882714 * L_34 = __this->get_prText_6();
		NullCheck(L_34);
		GameObject_t1113636619 * L_35 = Component_get_gameObject_m2648350745(L_34, /*hidden argument*/NULL);
		NullCheck(L_32);
		InterfaceActionInvoker2< GameObject_t1113636619 *, GameObject_t1113636619 * >::Invoke(10 /* System.Void NendUnityPlugin.AD.Native.INativeAd::Activate(UnityEngine.GameObject,UnityEngine.GameObject) */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_32, L_33, L_35);
		Il2CppObject * L_36 = __this->get_m_ShowingNativeAd_18();
		IntPtr_t L_37;
		L_37.set_m_value_0((void*)(void*)NendAdNativeView_OnClickAd_m2443000380_MethodInfo_var);
		EventHandler_t1348719766 * L_38 = (EventHandler_t1348719766 *)il2cpp_codegen_object_new(EventHandler_t1348719766_il2cpp_TypeInfo_var);
		EventHandler__ctor_m3449229857(L_38, __this, L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		InterfaceActionInvoker1< EventHandler_t1348719766 * >::Invoke(12 /* System.Void NendUnityPlugin.AD.Native.INativeAd::add_AdClicked(System.EventHandler) */, INativeAd_t3252869353_il2cpp_TypeInfo_var, L_36, L_38);
		NendAdNativeView_PostAdShown_m2675772046(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::Awake()
extern "C"  void NendAdNativeView_Awake_m3341910461 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method)
{
	{
		NendAdNativeView_Clear_m2493923126(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::OnEnable()
extern "C"  void NendAdNativeView_OnEnable_m2679496327 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method)
{
	{
		bool L_0 = NendAdNativeView_get_Loaded_m2218797704(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		bool L_1 = __this->get_renderWhenActivated_4();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		NendAdNativeView_RenderAd_m3682669061(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::OnDisable()
extern "C"  void NendAdNativeView_OnDisable_m2965758053 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method)
{
	{
		NendAdNativeView_Clear_m2493923126(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::OnDestroy()
extern "C"  void NendAdNativeView_OnDestroy_m2360927530 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNativeView_OnDestroy_m2360927530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_1 = Object_get_name_m1414505214(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral2657966006, L_0, /*hidden argument*/NULL);
		__this->set_m_LoadedNativeAd_17((Il2CppObject *)NULL);
		__this->set_m_ShowingNativeAd_18((Il2CppObject *)NULL);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.NendAdNativeView::RenderAd()
extern "C"  bool NendAdNativeView_RenderAd_m3682669061 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method)
{
	{
		bool L_0 = NendAdNativeView_get_Loaded_m2218797704(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_m_LoadedNativeAd_17();
		NendAdNativeView_set_ShowingNativeAd_m553901933(__this, L_1, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0019:
	{
		return (bool)0;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::OnLoadAd(NendUnityPlugin.AD.Native.INativeAd)
extern "C"  void NendAdNativeView_OnLoadAd_m3850891744 (NendAdNativeView_t2014609480 * __this, Il2CppObject * ___ad0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___ad0;
		__this->set_m_LoadedNativeAd_17(L_0);
		bool L_1 = __this->get_renderWhenLoaded_3();
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		NendAdNativeView_RenderAd_m3682669061(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::RenderAd(NendUnityPlugin.AD.Native.INativeAd)
extern "C"  void NendAdNativeView_RenderAd_m3300549469 (NendAdNativeView_t2014609480 * __this, Il2CppObject * ___ad0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___ad0;
		__this->set_m_LoadedNativeAd_17(L_0);
		NendAdNativeView_RenderAd_m3682669061(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::Clear()
extern "C"  void NendAdNativeView_Clear_m2493923126 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method)
{
	{
		Text_t1901882714 * L_0 = __this->get_prText_6();
		UIRenderer_TryClearText_m4217291438(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Text_t1901882714 * L_1 = __this->get_shortText_7();
		UIRenderer_TryClearText_m4217291438(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Text_t1901882714 * L_2 = __this->get_longText_8();
		UIRenderer_TryClearText_m4217291438(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Text_t1901882714 * L_3 = __this->get_promotionNameText_9();
		UIRenderer_TryClearText_m4217291438(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Text_t1901882714 * L_4 = __this->get_promotionUrlText_10();
		UIRenderer_TryClearText_m4217291438(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Text_t1901882714 * L_5 = __this->get_actionButtonText_11();
		UIRenderer_TryClearText_m4217291438(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		RawImage_t3182918964 * L_6 = __this->get_adImage_12();
		UIRenderer_TryClearImage_m1984313908(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		RawImage_t3182918964 * L_7 = __this->get_logoImage_13();
		UIRenderer_TryClearImage_m1984313908(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::OnClickAd(System.Object,System.EventArgs)
extern "C"  void NendAdNativeView_OnClickAd_m2443000380 (NendAdNativeView_t2014609480 * __this, Il2CppObject * ___sender0, EventArgs_t3591816995 * ___e1, const MethodInfo* method)
{
	{
		NendAdNativeView_PostAdClicked_m515887988(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::PostAdShown()
extern "C"  void NendAdNativeView_PostAdShown_m2675772046 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNativeView_PostAdShown_m2675772046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeAdViewEvent_t610464567 * L_0 = __this->get_AdShown_14();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		NativeAdViewEvent_t610464567 * L_1 = __this->get_AdShown_14();
		NullCheck(L_1);
		UnityEvent_1_Invoke_m2488769038(L_1, __this, /*hidden argument*/UnityEvent_1_Invoke_m2488769038_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.NendAdNativeView::PostAdClicked()
extern "C"  void NendAdNativeView_PostAdClicked_m515887988 (NendAdNativeView_t2014609480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdNativeView_PostAdClicked_m515887988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeAdViewEvent_t610464567 * L_0 = __this->get_AdClicked_16();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		NativeAdViewEvent_t610464567 * L_1 = __this->get_AdClicked_16();
		NullCheck(L_1);
		UnityEvent_1_Invoke_m2488769038(L_1, __this, /*hidden argument*/UnityEvent_1_Invoke_m2488769038_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.NendAdLogger::.ctor()
extern "C"  void NendAdLogger__ctor_m379635451 (NendAdLogger_t1147818846 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.NendAdLogger::set_LogLevel(NendUnityPlugin.AD.Native.Utils.NendAdLogger/NendAdLogLevel)
extern "C"  void NendAdLogger_set_LogLevel_m867647294 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdLogger_set_LogLevel_m867647294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_set_LogLevel_m3276973888(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.SimpleTimer::.ctor()
extern "C"  void SimpleTimer__ctor_m1198240476 (SimpleTimer_t4075384413 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleTimer__ctor_m1198240476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdMainThreadWorker_t3569218590_il2cpp_TypeInfo_var);
		NendAdMainThreadWorker_Prepare_m3075374016(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.SimpleTimer::Finalize()
extern "C"  void SimpleTimer_Finalize_m2217791589 (SimpleTimer_t4075384413 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleTimer_Finalize_m2217791589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral3634757765, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		SimpleTimer_Dispose_m1569670885(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0022:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.SimpleTimer::Dispose()
extern "C"  void SimpleTimer_Dispose_m1569670885 (SimpleTimer_t4075384413 * __this, const MethodInfo* method)
{
	{
		SimpleTimer_Stop_m1024974171(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.SimpleTimer::Start(System.Double)
extern "C"  void SimpleTimer_Start_m3197311368 (SimpleTimer_t4075384413 * __this, double ___interval0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleTimer_Start_m3197311368_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SimpleTimer_Stop_m1024974171(__this, /*hidden argument*/NULL);
		double L_0 = ___interval0;
		Timer_t1767341190 * L_1 = (Timer_t1767341190 *)il2cpp_codegen_object_new(Timer_t1767341190_il2cpp_TypeInfo_var);
		Timer__ctor_m2308471781(L_1, L_0, /*hidden argument*/NULL);
		__this->set_m_Timer_1(L_1);
		Timer_t1767341190 * L_2 = __this->get_m_Timer_1();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)SimpleTimer_OnTimedEvent_m459759611_MethodInfo_var);
		ElapsedEventHandler_t976055006 * L_4 = (ElapsedEventHandler_t976055006 *)il2cpp_codegen_object_new(ElapsedEventHandler_t976055006_il2cpp_TypeInfo_var);
		ElapsedEventHandler__ctor_m3647598057(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Timer_add_Elapsed_m1727888252(L_2, L_4, /*hidden argument*/NULL);
		Timer_t1767341190 * L_5 = __this->get_m_Timer_1();
		NullCheck(L_5);
		Timer_Start_m3010200494(L_5, /*hidden argument*/NULL);
		ObjectU5BU5D_t2843939325* L_6 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		double L_7 = ___interval0;
		double L_8 = L_7;
		Il2CppObject * L_9 = Box(Double_t594665363_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_I_m2000148771(NULL /*static, unused*/, _stringLiteral3853653976, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.SimpleTimer::Stop()
extern "C"  void SimpleTimer_Stop_m1024974171 (SimpleTimer_t4075384413 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleTimer_Stop_m1024974171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Timer_t1767341190 * L_0 = __this->get_m_Timer_1();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_I_m2000148771(NULL /*static, unused*/, _stringLiteral2899771905, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		Timer_t1767341190 * L_1 = __this->get_m_Timer_1();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)SimpleTimer_OnTimedEvent_m459759611_MethodInfo_var);
		ElapsedEventHandler_t976055006 * L_3 = (ElapsedEventHandler_t976055006 *)il2cpp_codegen_object_new(ElapsedEventHandler_t976055006_il2cpp_TypeInfo_var);
		ElapsedEventHandler__ctor_m3647598057(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Timer_remove_Elapsed_m1866573542(L_1, L_3, /*hidden argument*/NULL);
		Timer_t1767341190 * L_4 = __this->get_m_Timer_1();
		NullCheck(L_4);
		Timer_Stop_m2096258851(L_4, /*hidden argument*/NULL);
		Timer_t1767341190 * L_5 = __this->get_m_Timer_1();
		NullCheck(L_5);
		Timer_Close_m1184068935(L_5, /*hidden argument*/NULL);
		__this->set_m_Timer_1((Timer_t1767341190 *)NULL);
	}

IL_004f:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.SimpleTimer::OnTimedEvent(System.Object,System.Timers.ElapsedEventArgs)
extern "C"  void SimpleTimer_OnTimedEvent_m459759611 (SimpleTimer_t4075384413 * __this, Il2CppObject * ___source0, ElapsedEventArgs_t3048571484 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleTimer_OnTimedEvent_m459759611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdMainThreadWorker_t3569218590_il2cpp_TypeInfo_var);
		NendAdMainThreadWorker_t3569218590 * L_0 = NendAdMainThreadWorker_get_Instance_m443439756(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_t1264377477 * L_1 = __this->get_OnFireEvent_0();
		NullCheck(L_0);
		NendAdMainThreadWorker_Post_m1274596709(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader::.ctor()
extern "C"  void TextureLoader__ctor_m3506707789 (TextureLoader_t3414853624 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator NendUnityPlugin.AD.Native.Utils.TextureLoader::LoadTexture(System.String,System.Action`1<UnityEngine.Texture2D>)
extern "C"  Il2CppObject * TextureLoader_LoadTexture_m3236146968 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Action_1_t4012913780 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextureLoader_LoadTexture_m3236146968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadTextureU3Ec__Iterator0_t2469200630 * V_0 = NULL;
	{
		U3CLoadTextureU3Ec__Iterator0_t2469200630 * L_0 = (U3CLoadTextureU3Ec__Iterator0_t2469200630 *)il2cpp_codegen_object_new(U3CLoadTextureU3Ec__Iterator0_t2469200630_il2cpp_TypeInfo_var);
		U3CLoadTextureU3Ec__Iterator0__ctor_m1470171075(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadTextureU3Ec__Iterator0_t2469200630 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CLoadTextureU3Ec__Iterator0_t2469200630 * L_3 = V_0;
		Action_1_t4012913780 * L_4 = ___callback1;
		NullCheck(L_3);
		L_3->set_callback_2(L_4);
		U3CLoadTextureU3Ec__Iterator0_t2469200630 * L_5 = V_0;
		return L_5;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::.ctor()
extern "C"  void U3CLoadTextureU3Ec__Iterator0__ctor_m1470171075 (U3CLoadTextureU3Ec__Iterator0_t2469200630 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadTextureU3Ec__Iterator0_MoveNext_m3749932940 (U3CLoadTextureU3Ec__Iterator0_t2469200630 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadTextureU3Ec__Iterator0_MoveNext_m3749932940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0074;
			}
		}
	}
	{
		goto IL_0129;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TextureCache_t3323732960_il2cpp_TypeInfo_var);
		TextureCache_t3323732960 * L_2 = TextureCache_get_Instance_m1280202405(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = __this->get_url_0();
		NullCheck(L_2);
		Texture2D_t3840446185 * L_4 = TextureCache_Get_m1883981837(L_2, L_3, /*hidden argument*/NULL);
		__this->set_U3CtextureU3E__0_1(L_4);
		Texture2D_t3840446185 * L_5 = __this->get_U3CtextureU3E__0_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0060;
		}
	}
	{
		Action_1_t4012913780 * L_7 = __this->get_callback_2();
		Texture2D_t3840446185 * L_8 = __this->get_U3CtextureU3E__0_1();
		NullCheck(L_7);
		Action_1_Invoke_m400472198(L_7, L_8, /*hidden argument*/Action_1_Invoke_m400472198_MethodInfo_var);
		goto IL_0129;
	}

IL_0060:
	{
		String_t* L_9 = __this->get_url_0();
		WWW_t3688466362 * L_10 = (WWW_t3688466362 *)il2cpp_codegen_object_new(WWW_t3688466362_il2cpp_TypeInfo_var);
		WWW__ctor_m1181807108(L_10, L_9, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__1_3(L_10);
		V_0 = ((int32_t)-3);
	}

IL_0074:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_11 = V_0;
			switch (((int32_t)((int32_t)L_11-(int32_t)1)))
			{
				case 0:
				{
					goto IL_00a2;
				}
			}
		}

IL_0080:
		{
			WWW_t3688466362 * L_12 = __this->get_U3CwwwU3E__1_3();
			__this->set_U24current_4(L_12);
			bool L_13 = __this->get_U24disposing_5();
			if (L_13)
			{
				goto IL_009b;
			}
		}

IL_0094:
		{
			__this->set_U24PC_6(1);
		}

IL_009b:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x12B, FINALLY_0117);
		}

IL_00a2:
		{
			WWW_t3688466362 * L_14 = __this->get_U3CwwwU3E__1_3();
			NullCheck(L_14);
			String_t* L_15 = WWW_get_error_m4112162252(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_16 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
			if (!L_16)
			{
				goto IL_00e3;
			}
		}

IL_00b7:
		{
			WWW_t3688466362 * L_17 = __this->get_U3CwwwU3E__1_3();
			NullCheck(L_17);
			Texture2D_t3840446185 * L_18 = WWW_get_texture_m1724981587(L_17, /*hidden argument*/NULL);
			__this->set_U3CtextureU3E__0_1(L_18);
			IL2CPP_RUNTIME_CLASS_INIT(TextureCache_t3323732960_il2cpp_TypeInfo_var);
			TextureCache_t3323732960 * L_19 = TextureCache_get_Instance_m1280202405(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_20 = __this->get_url_0();
			Texture2D_t3840446185 * L_21 = __this->get_U3CtextureU3E__0_1();
			NullCheck(L_19);
			TextureCache_Put_m3192621705(L_19, L_20, L_21, /*hidden argument*/NULL);
			goto IL_0101;
		}

IL_00e3:
		{
			ObjectU5BU5D_t2843939325* L_22 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
			WWW_t3688466362 * L_23 = __this->get_U3CwwwU3E__1_3();
			NullCheck(L_23);
			String_t* L_24 = WWW_get_error_m4112162252(L_23, /*hidden argument*/NULL);
			NullCheck(L_22);
			ArrayElementTypeCheck (L_22, L_24);
			(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_24);
			IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
			NendAdLogger_E_m3544237484(NULL /*static, unused*/, _stringLiteral2273156437, L_22, /*hidden argument*/NULL);
		}

IL_0101:
		{
			Action_1_t4012913780 * L_25 = __this->get_callback_2();
			Texture2D_t3840446185 * L_26 = __this->get_U3CtextureU3E__0_1();
			NullCheck(L_25);
			Action_1_Invoke_m400472198(L_25, L_26, /*hidden argument*/Action_1_Invoke_m400472198_MethodInfo_var);
			IL2CPP_LEAVE(0x122, FINALLY_0117);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0117;
	}

FINALLY_0117:
	{ // begin finally (depth: 1)
		{
			bool L_27 = V_1;
			if (!L_27)
			{
				goto IL_011b;
			}
		}

IL_011a:
		{
			IL2CPP_END_FINALLY(279)
		}

IL_011b:
		{
			U3CLoadTextureU3Ec__Iterator0_U3CU3E__Finally0_m3862613709(__this, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(279)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(279)
	{
		IL2CPP_JUMP_TBL(0x12B, IL_012b)
		IL2CPP_JUMP_TBL(0x122, IL_0122)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0122:
	{
		__this->set_U24PC_6((-1));
	}

IL_0129:
	{
		return (bool)0;
	}

IL_012b:
	{
		return (bool)1;
	}
}
// System.Object NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadTextureU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3480587894 (U3CLoadTextureU3Ec__Iterator0_t2469200630 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadTextureU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3976223157 (U3CLoadTextureU3Ec__Iterator0_t2469200630 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::Dispose()
extern "C"  void U3CLoadTextureU3Ec__Iterator0_Dispose_m1191882826 (U3CLoadTextureU3Ec__Iterator0_t2469200630 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0034;
			}
			case 1:
			{
				goto IL_0028;
			}
		}
	}
	{
		goto IL_0034;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x34, FINALLY_002d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		U3CLoadTextureU3Ec__Iterator0_U3CU3E__Finally0_m3862613709(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(45)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x34, IL_0034)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0034:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::Reset()
extern "C"  void U3CLoadTextureU3Ec__Iterator0_Reset_m127061503 (U3CLoadTextureU3Ec__Iterator0_t2469200630 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadTextureU3Ec__Iterator0_Reset_m127061503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader/<LoadTexture>c__Iterator0::<>__Finally0()
extern "C"  void U3CLoadTextureU3Ec__Iterator0_U3CU3E__Finally0_m3862613709 (U3CLoadTextureU3Ec__Iterator0_t2469200630 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadTextureU3Ec__Iterator0_U3CU3E__Finally0_m3862613709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WWW_t3688466362 * L_0 = __this->get_U3CwwwU3E__1_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		WWW_t3688466362 * L_1 = __this->get_U3CwwwU3E__1_3();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache::.ctor()
extern "C"  void TextureCache__ctor_m3754485370 (TextureCache_t3323732960 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextureCache__ctor_m3754485370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Dictionary_2_t3625702484 * L_0 = (Dictionary_2_t3625702484 *)il2cpp_codegen_object_new(Dictionary_2_t3625702484_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2275355514(L_0, /*hidden argument*/Dictionary_2__ctor_m2275355514_MethodInfo_var);
		__this->set_m_Cache_1(L_0);
		return;
	}
}
// NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache::get_Instance()
extern "C"  TextureCache_t3323732960 * TextureCache_get_Instance_m1280202405 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextureCache_get_Instance_m1280202405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TextureCache_t3323732960 * G_B2_0 = NULL;
	TextureCache_t3323732960 * G_B1_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(TextureCache_t3323732960_il2cpp_TypeInfo_var);
		TextureCache_t3323732960 * L_0 = ((TextureCache_t3323732960_StaticFields*)TextureCache_t3323732960_il2cpp_TypeInfo_var->static_fields)->get_s_Instance_0();
		TextureCache_t3323732960 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0017;
		}
	}
	{
		TextureCache_t3323732960 * L_2 = (TextureCache_t3323732960 *)il2cpp_codegen_object_new(TextureCache_t3323732960_il2cpp_TypeInfo_var);
		TextureCache__ctor_m3754485370(L_2, /*hidden argument*/NULL);
		TextureCache_t3323732960 * L_3 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(TextureCache_t3323732960_il2cpp_TypeInfo_var);
		((TextureCache_t3323732960_StaticFields*)TextureCache_t3323732960_il2cpp_TypeInfo_var->static_fields)->set_s_Instance_0(L_3);
		G_B2_0 = L_3;
	}

IL_0017:
	{
		return G_B2_0;
	}
}
// UnityEngine.Texture2D NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache::Get(System.String)
extern "C"  Texture2D_t3840446185 * TextureCache_Get_m1883981837 (TextureCache_t3323732960 * __this, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextureCache_Get_m1883981837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3625702484 * L_0 = __this->get_m_Cache_1();
		String_t* L_1 = ___url0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1577318754(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1577318754_MethodInfo_var);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_D_m2189594530(NULL /*static, unused*/, _stringLiteral2958555957, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		Dictionary_2_t3625702484 * L_3 = __this->get_m_Cache_1();
		String_t* L_4 = ___url0;
		NullCheck(L_3);
		Texture2D_t3840446185 * L_5 = Dictionary_2_get_Item_m2202019062(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m2202019062_MethodInfo_var);
		return L_5;
	}

IL_002e:
	{
		return (Texture2D_t3840446185 *)NULL;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache::Put(System.String,UnityEngine.Texture2D)
extern "C"  void TextureCache_Put_m3192621705 (TextureCache_t3323732960 * __this, String_t* ___url0, Texture2D_t3840446185 * ___texture1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextureCache_Put_m3192621705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture2D_t3840446185 * L_0 = ___texture1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Dictionary_2_t3625702484 * L_2 = __this->get_m_Cache_1();
		String_t* L_3 = ___url0;
		Texture2D_t3840446185 * L_4 = ___texture1;
		NullCheck(L_2);
		Dictionary_2_set_Item_m432567873(L_2, L_3, L_4, /*hidden argument*/Dictionary_2_set_Item_m432567873_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache::.cctor()
extern "C"  void TextureCache__cctor_m2830682533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer::.ctor()
extern "C"  void UIRenderer__ctor_m2684495822 (UIRenderer_t575704595 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer::TryRenderText(UnityEngine.UI.Text,System.String)
extern "C"  void UIRenderer_TryRenderText_m3187668248 (Il2CppObject * __this /* static, unused */, Text_t1901882714 * ___target0, String_t* ___text1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIRenderer_TryRenderText_m3187668248_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t1901882714 * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Text_t1901882714 * L_2 = ___target0;
		String_t* L_3 = ___text1;
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_3);
	}

IL_0013:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer::TryRenderImage(UnityEngine.MonoBehaviour,UnityEngine.UI.RawImage,System.String)
extern "C"  void UIRenderer_TryRenderImage_m737991264 (Il2CppObject * __this /* static, unused */, MonoBehaviour_t3962482529 * ___behaviour0, RawImage_t3182918964 * ___target1, String_t* ___url2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIRenderer_TryRenderImage_m737991264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CTryRenderImageU3Ec__AnonStorey0_t4270539830 * V_0 = NULL;
	{
		U3CTryRenderImageU3Ec__AnonStorey0_t4270539830 * L_0 = (U3CTryRenderImageU3Ec__AnonStorey0_t4270539830 *)il2cpp_codegen_object_new(U3CTryRenderImageU3Ec__AnonStorey0_t4270539830_il2cpp_TypeInfo_var);
		U3CTryRenderImageU3Ec__AnonStorey0__ctor_m1291323508(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTryRenderImageU3Ec__AnonStorey0_t4270539830 * L_1 = V_0;
		RawImage_t3182918964 * L_2 = ___target1;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CTryRenderImageU3Ec__AnonStorey0_t4270539830 * L_3 = V_0;
		NullCheck(L_3);
		RawImage_t3182918964 * L_4 = L_3->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_6 = ___url2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004d;
		}
	}
	{
		MonoBehaviour_t3962482529 * L_8 = ___behaviour0;
		NullCheck(L_8);
		bool L_9 = Behaviour_get_isActiveAndEnabled_m3013577336(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004d;
		}
	}
	{
		MonoBehaviour_t3962482529 * L_10 = ___behaviour0;
		String_t* L_11 = ___url2;
		U3CTryRenderImageU3Ec__AnonStorey0_t4270539830 * L_12 = V_0;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)U3CTryRenderImageU3Ec__AnonStorey0_U3CU3Em__0_m1165082455_MethodInfo_var);
		Action_1_t4012913780 * L_14 = (Action_1_t4012913780 *)il2cpp_codegen_object_new(Action_1_t4012913780_il2cpp_TypeInfo_var);
		Action_1__ctor_m3185781518(L_14, L_12, L_13, /*hidden argument*/Action_1__ctor_m3185781518_MethodInfo_var);
		Il2CppObject * L_15 = TextureLoader_LoadTexture_m3236146968(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		NullCheck(L_10);
		MonoBehaviour_StartCoroutine_m4001331470(L_10, L_15, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer::TryClearText(UnityEngine.UI.Text)
extern "C"  void UIRenderer_TryClearText_m4217291438 (Il2CppObject * __this /* static, unused */, Text_t1901882714 * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIRenderer_TryClearText_m4217291438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t1901882714 * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		UIRenderer_TryRenderText_m3187668248(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer::TryClearImage(UnityEngine.UI.RawImage)
extern "C"  void UIRenderer_TryClearImage_m1984313908 (Il2CppObject * __this /* static, unused */, RawImage_t3182918964 * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIRenderer_TryClearImage_m1984313908_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RawImage_t3182918964 * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		RawImage_t3182918964 * L_2 = ___target0;
		NullCheck(L_2);
		RawImage_set_texture_m415027901(L_2, (Texture_t3661962703 *)NULL, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer/<TryRenderImage>c__AnonStorey0::.ctor()
extern "C"  void U3CTryRenderImageU3Ec__AnonStorey0__ctor_m1291323508 (U3CTryRenderImageU3Ec__AnonStorey0_t4270539830 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Native.Utils.UIRenderer/<TryRenderImage>c__AnonStorey0::<>m__0(UnityEngine.Texture2D)
extern "C"  void U3CTryRenderImageU3Ec__AnonStorey0_U3CU3Em__0_m1165082455 (U3CTryRenderImageU3Ec__AnonStorey0_t4270539830 * __this, Texture2D_t3840446185 * ___texture0, const MethodInfo* method)
{
	{
		RawImage_t3182918964 * L_0 = __this->get_target_0();
		Texture2D_t3840446185 * L_1 = ___texture0;
		NullCheck(L_0);
		RawImage_set_texture_m415027901(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAd::.ctor()
extern "C"  void NendAd__ctor_m3419012545 (NendAd_t284737821 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAd::Awake()
extern "C"  void NendAd_Awake_m3509898713 (NendAd_t284737821 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAd_Awake_m3509898713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_set_hideFlags_m395729791(L_0, ((int32_t)61), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m3153277066(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(4 /* System.Void NendUnityPlugin.AD.NendAd::Create() */, __this);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAd::OnDestroy()
extern "C"  void NendAd_OnDestroy_m3306255142 (NendAd_t284737821 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(9 /* System.Void NendUnityPlugin.AD.NendAd::Destroy() */, __this);
		return;
	}
}
// System.Int32 NendUnityPlugin.AD.NendAd::GetBitGravity(NendUnityPlugin.Common.Gravity[])
extern "C"  int32_t NendAd_GetBitGravity_m3423304963 (NendAd_t284737821 * __this, GravityU5BU5D_t868584943* ___gravity0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GravityU5BU5D_t868584943* V_2 = NULL;
	int32_t V_3 = 0;
	{
		V_0 = 0;
		GravityU5BU5D_t868584943* L_0 = ___gravity0;
		V_2 = L_0;
		V_3 = 0;
		goto IL_0017;
	}

IL_000b:
	{
		GravityU5BU5D_t868584943* L_1 = V_2;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		V_0 = ((int32_t)((int32_t)L_5|(int32_t)L_6));
		int32_t L_7 = V_3;
		V_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0017:
	{
		int32_t L_8 = V_3;
		GravityU5BU5D_t868584943* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Void NendUnityPlugin.AD.NendAd/Margin::.ctor()
extern "C"  void Margin__ctor_m2597277662 (Margin_t772722833 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::.ctor()
extern "C"  void NendAdBanner__ctor_m213822161 (NendAdBanner_t489953245 * __this, const MethodInfo* method)
{
	{
		__this->set_automaticDisplay_3((bool)1);
		Color_t2555686324  L_0 = Color_get_clear_m1773884651(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_backgroundColor_7(L_0);
		NendAd__ctor_m3419012545(__this, /*hidden argument*/NULL);
		return;
	}
}
// NendUnityPlugin.Platform.NendAdBannerInterface NendUnityPlugin.AD.NendAdBanner::get_Interface()
extern "C"  Il2CppObject * NendAdBanner_get_Interface_m1606256974 (NendAdBanner_t489953245 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get__interface_10();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = NendAdNativeInterfaceFactory_CreateBannerAdInterface_m2322301420(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__interface_10(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = __this->get__interface_10();
		return L_2;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::add_AdLoaded(System.EventHandler)
extern "C"  void NendAdBanner_add_AdLoaded_m3453699443 (NendAdBanner_t489953245 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_add_AdLoaded_m3453699443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdLoaded_11();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_AdLoaded_11();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::remove_AdLoaded(System.EventHandler)
extern "C"  void NendAdBanner_remove_AdLoaded_m4239923732 (NendAdBanner_t489953245 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_remove_AdLoaded_m4239923732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdLoaded_11();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_AdLoaded_11();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::add_AdFailedToReceive(System.EventHandler`1<NendUnityPlugin.Common.NendAdErrorEventArgs>)
extern "C"  void NendAdBanner_add_AdFailedToReceive_m3291298239 (NendAdBanner_t489953245 * __this, EventHandler_1_t1314708246 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_add_AdFailedToReceive_m3291298239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1314708246 * V_0 = NULL;
	EventHandler_1_t1314708246 * V_1 = NULL;
	{
		EventHandler_1_t1314708246 * L_0 = __this->get_AdFailedToReceive_12();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1314708246 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1314708246 ** L_2 = __this->get_address_of_AdFailedToReceive_12();
		EventHandler_1_t1314708246 * L_3 = V_1;
		EventHandler_1_t1314708246 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1314708246 * L_6 = V_0;
		EventHandler_1_t1314708246 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1314708246 *>(L_2, ((EventHandler_1_t1314708246 *)CastclassSealed(L_5, EventHandler_1_t1314708246_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1314708246 * L_8 = V_0;
		EventHandler_1_t1314708246 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1314708246 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1314708246 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::remove_AdFailedToReceive(System.EventHandler`1<NendUnityPlugin.Common.NendAdErrorEventArgs>)
extern "C"  void NendAdBanner_remove_AdFailedToReceive_m1066821576 (NendAdBanner_t489953245 * __this, EventHandler_1_t1314708246 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_remove_AdFailedToReceive_m1066821576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1314708246 * V_0 = NULL;
	EventHandler_1_t1314708246 * V_1 = NULL;
	{
		EventHandler_1_t1314708246 * L_0 = __this->get_AdFailedToReceive_12();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1314708246 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1314708246 ** L_2 = __this->get_address_of_AdFailedToReceive_12();
		EventHandler_1_t1314708246 * L_3 = V_1;
		EventHandler_1_t1314708246 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1314708246 * L_6 = V_0;
		EventHandler_1_t1314708246 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1314708246 *>(L_2, ((EventHandler_1_t1314708246 *)CastclassSealed(L_5, EventHandler_1_t1314708246_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1314708246 * L_8 = V_0;
		EventHandler_1_t1314708246 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1314708246 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1314708246 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::add_AdReceived(System.EventHandler)
extern "C"  void NendAdBanner_add_AdReceived_m1155793081 (NendAdBanner_t489953245 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_add_AdReceived_m1155793081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdReceived_13();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_AdReceived_13();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::remove_AdReceived(System.EventHandler)
extern "C"  void NendAdBanner_remove_AdReceived_m1034858748 (NendAdBanner_t489953245 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_remove_AdReceived_m1034858748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdReceived_13();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_AdReceived_13();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::add_AdClicked(System.EventHandler)
extern "C"  void NendAdBanner_add_AdClicked_m4109704752 (NendAdBanner_t489953245 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_add_AdClicked_m4109704752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdClicked_14();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_AdClicked_14();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::remove_AdClicked(System.EventHandler)
extern "C"  void NendAdBanner_remove_AdClicked_m1818686474 (NendAdBanner_t489953245 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_remove_AdClicked_m1818686474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdClicked_14();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_AdClicked_14();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::add_AdBacked(System.EventHandler)
extern "C"  void NendAdBanner_add_AdBacked_m3794188279 (NendAdBanner_t489953245 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_add_AdBacked_m3794188279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdBacked_15();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_AdBacked_15();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::remove_AdBacked(System.EventHandler)
extern "C"  void NendAdBanner_remove_AdBacked_m2364434571 (NendAdBanner_t489953245 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_remove_AdBacked_m2364434571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdBacked_15();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_AdBacked_15();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::add_InformationClicked(System.EventHandler)
extern "C"  void NendAdBanner_add_InformationClicked_m459008816 (NendAdBanner_t489953245 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_add_InformationClicked_m459008816_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_InformationClicked_16();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_InformationClicked_16();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::remove_InformationClicked(System.EventHandler)
extern "C"  void NendAdBanner_remove_InformationClicked_m3480265601 (NendAdBanner_t489953245 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_remove_InformationClicked_m3480265601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	EventHandler_t1348719766 * V_1 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_InformationClicked_16();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t1348719766 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t1348719766 ** L_2 = __this->get_address_of_InformationClicked_16();
		EventHandler_t1348719766 * L_3 = V_1;
		EventHandler_t1348719766 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t1348719766 * L_6 = V_0;
		EventHandler_t1348719766 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t1348719766 *>(L_2, ((EventHandler_t1348719766 *)CastclassSealed(L_5, EventHandler_t1348719766_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t1348719766 * L_8 = V_0;
		EventHandler_t1348719766 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t1348719766 *)L_8) == ((Il2CppObject*)(EventHandler_t1348719766 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::Start()
extern "C"  void NendAdBanner_Start_m975105965 (NendAdBanner_t489953245 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_automaticDisplay_3();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		VirtActionInvoker0::Invoke(5 /* System.Void NendUnityPlugin.AD.NendAd::Show() */, __this);
	}

IL_0011:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::Create()
extern "C"  void NendAdBanner_Create_m1966987288 (NendAdBanner_t489953245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_Create_m1966987288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = NendAdBanner_get_Interface_m1606256974(__this, /*hidden argument*/NULL);
		String_t* L_1 = NendAdBanner_MakeParams_m3373626820(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void NendUnityPlugin.Platform.NendAdBannerInterface::TryCreateBanner(System.String) */, NendAdBannerInterface_t524056774_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::Show()
extern "C"  void NendAdBanner_Show_m756177697 (NendAdBanner_t489953245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_Show_m756177697_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = NendAdBanner_get_Interface_m1606256974(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m1414505214(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(1 /* System.Void NendUnityPlugin.Platform.NendAdBannerInterface::ShowBanner(System.String) */, NendAdBannerInterface_t524056774_il2cpp_TypeInfo_var, L_0, L_2);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::Hide()
extern "C"  void NendAdBanner_Hide_m4189201796 (NendAdBanner_t489953245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_Hide_m4189201796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = NendAdBanner_get_Interface_m1606256974(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m1414505214(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(2 /* System.Void NendUnityPlugin.Platform.NendAdBannerInterface::HideBanner(System.String) */, NendAdBannerInterface_t524056774_il2cpp_TypeInfo_var, L_0, L_2);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::Resume()
extern "C"  void NendAdBanner_Resume_m858612223 (NendAdBanner_t489953245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_Resume_m858612223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = NendAdBanner_get_Interface_m1606256974(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m1414505214(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(3 /* System.Void NendUnityPlugin.Platform.NendAdBannerInterface::ResumeBanner(System.String) */, NendAdBannerInterface_t524056774_il2cpp_TypeInfo_var, L_0, L_2);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::Pause()
extern "C"  void NendAdBanner_Pause_m3243031251 (NendAdBanner_t489953245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_Pause_m3243031251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = NendAdBanner_get_Interface_m1606256974(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m1414505214(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(4 /* System.Void NendUnityPlugin.Platform.NendAdBannerInterface::PauseBanner(System.String) */, NendAdBannerInterface_t524056774_il2cpp_TypeInfo_var, L_0, L_2);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::Destroy()
extern "C"  void NendAdBanner_Destroy_m720695405 (NendAdBanner_t489953245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_Destroy_m720695405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = NendAdBanner_get_Interface_m1606256974(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m1414505214(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(5 /* System.Void NendUnityPlugin.Platform.NendAdBannerInterface::DestroyBanner(System.String) */, NendAdBannerInterface_t524056774_il2cpp_TypeInfo_var, L_0, L_2);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::Layout(NendUnityPlugin.Layout.NendAdLayoutBuilder)
extern "C"  void NendAdBanner_Layout_m328065890 (NendAdBanner_t489953245 * __this, Il2CppObject * ___builder0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_Layout_m328065890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___builder0;
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		Il2CppObject * L_1 = ___builder0;
		if (!((NendAdDefaultLayoutBuilder_t3894915050 *)IsInstClass(L_1, NendAdDefaultLayoutBuilder_t3894915050_il2cpp_TypeInfo_var)))
		{
			goto IL_002d;
		}
	}
	{
		Il2CppObject * L_2 = NendAdBanner_get_Interface_m1606256974(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m1414505214(L_3, /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___builder0;
		NullCheck(L_5);
		String_t* L_6 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String NendUnityPlugin.Layout.NendAdLayoutBuilder::Build() */, NendAdLayoutBuilder_t1695755985_il2cpp_TypeInfo_var, L_5);
		NullCheck(L_2);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(6 /* System.Void NendUnityPlugin.Platform.NendAdBannerInterface::LayoutBanner(System.String,System.String) */, NendAdBannerInterface_t524056774_il2cpp_TypeInfo_var, L_2, L_4, L_6);
	}

IL_002d:
	{
		return;
	}
}
// System.String NendUnityPlugin.AD.NendAdBanner::MakeParams()
extern "C"  String_t* NendAdBanner_MakeParams_m3373626820 (NendAdBanner_t489953245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_MakeParams_m3373626820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	StringBuilder_t1712802186 * G_B2_0 = NULL;
	StringBuilder_t1712802186 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	StringBuilder_t1712802186 * G_B3_1 = NULL;
	StringBuilder_t1712802186 * G_B5_0 = NULL;
	StringBuilder_t1712802186 * G_B4_0 = NULL;
	String_t* G_B6_0 = NULL;
	StringBuilder_t1712802186 * G_B6_1 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = (StringBuilder_t1712802186 *)il2cpp_codegen_object_new(StringBuilder_t1712802186_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m1414505214(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m1965104174(L_1, L_3, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_4 = V_0;
		NullCheck(L_4);
		StringBuilder_Append_m1965104174(L_4, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_5 = V_0;
		Account_t2230227361 * L_6 = __this->get_account_2();
		NullCheck(L_6);
		NendID_t3358092198 * L_7 = L_6->get_iOS_1();
		NullCheck(L_7);
		String_t* L_8 = L_7->get_apiKey_0();
		NullCheck(L_5);
		StringBuilder_Append_m1965104174(L_5, L_8, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_9 = V_0;
		NullCheck(L_9);
		StringBuilder_Append_m1965104174(L_9, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_10 = V_0;
		Account_t2230227361 * L_11 = __this->get_account_2();
		NullCheck(L_11);
		NendID_t3358092198 * L_12 = L_11->get_iOS_1();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_spotID_1();
		NullCheck(L_10);
		StringBuilder_Append_m890240332(L_10, L_13, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_14 = V_0;
		NullCheck(L_14);
		StringBuilder_Append_m1965104174(L_14, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_15 = V_0;
		bool L_16 = __this->get_outputLog_4();
		G_B1_0 = L_15;
		if (!L_16)
		{
			G_B2_0 = L_15;
			goto IL_0080;
		}
	}
	{
		G_B3_0 = _stringLiteral4002445229;
		G_B3_1 = G_B1_0;
		goto IL_0085;
	}

IL_0080:
	{
		G_B3_0 = _stringLiteral3875954633;
		G_B3_1 = G_B2_0;
	}

IL_0085:
	{
		NullCheck(G_B3_1);
		StringBuilder_Append_m1965104174(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m1965104174(L_17, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_18 = V_0;
		bool L_19 = __this->get_adjustSize_5();
		G_B4_0 = L_18;
		if (!L_19)
		{
			G_B5_0 = L_18;
			goto IL_00ad;
		}
	}
	{
		G_B6_0 = _stringLiteral4002445229;
		G_B6_1 = G_B4_0;
		goto IL_00b2;
	}

IL_00ad:
	{
		G_B6_0 = _stringLiteral3875954633;
		G_B6_1 = G_B5_0;
	}

IL_00b2:
	{
		NullCheck(G_B6_1);
		StringBuilder_Append_m1965104174(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_20 = V_0;
		NullCheck(L_20);
		StringBuilder_Append_m1965104174(L_20, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_21 = V_0;
		int32_t L_22 = __this->get_size_6();
		NullCheck(L_21);
		StringBuilder_Append_m890240332(L_21, L_22, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_23 = V_0;
		NullCheck(L_23);
		StringBuilder_Append_m1965104174(L_23, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_24 = V_0;
		GravityU5BU5D_t868584943* L_25 = __this->get_gravity_8();
		int32_t L_26 = NendAd_GetBitGravity_m3423304963(__this, L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		StringBuilder_Append_m890240332(L_24, L_26, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_27 = V_0;
		NullCheck(L_27);
		StringBuilder_Append_m1965104174(L_27, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_28 = V_0;
		Margin_t772722833 * L_29 = __this->get_margin_9();
		NullCheck(L_29);
		float L_30 = L_29->get_left_0();
		NullCheck(L_28);
		StringBuilder_Append_m3191759736(L_28, L_30, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_31 = V_0;
		NullCheck(L_31);
		StringBuilder_Append_m1965104174(L_31, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_32 = V_0;
		Margin_t772722833 * L_33 = __this->get_margin_9();
		NullCheck(L_33);
		float L_34 = L_33->get_top_1();
		NullCheck(L_32);
		StringBuilder_Append_m3191759736(L_32, L_34, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_35 = V_0;
		NullCheck(L_35);
		StringBuilder_Append_m1965104174(L_35, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_36 = V_0;
		Margin_t772722833 * L_37 = __this->get_margin_9();
		NullCheck(L_37);
		float L_38 = L_37->get_right_2();
		NullCheck(L_36);
		StringBuilder_Append_m3191759736(L_36, L_38, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_39 = V_0;
		NullCheck(L_39);
		StringBuilder_Append_m1965104174(L_39, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_40 = V_0;
		Margin_t772722833 * L_41 = __this->get_margin_9();
		NullCheck(L_41);
		float L_42 = L_41->get_bottom_3();
		NullCheck(L_40);
		StringBuilder_Append_m3191759736(L_40, L_42, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_43 = V_0;
		NullCheck(L_43);
		StringBuilder_Append_m1965104174(L_43, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_44 = V_0;
		Color_t2555686324 * L_45 = __this->get_address_of_backgroundColor_7();
		float L_46 = L_45->get_r_0();
		NullCheck(L_44);
		StringBuilder_Append_m3191759736(L_44, L_46, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_47 = V_0;
		NullCheck(L_47);
		StringBuilder_Append_m1965104174(L_47, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_48 = V_0;
		Color_t2555686324 * L_49 = __this->get_address_of_backgroundColor_7();
		float L_50 = L_49->get_g_1();
		NullCheck(L_48);
		StringBuilder_Append_m3191759736(L_48, L_50, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_51 = V_0;
		NullCheck(L_51);
		StringBuilder_Append_m1965104174(L_51, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_52 = V_0;
		Color_t2555686324 * L_53 = __this->get_address_of_backgroundColor_7();
		float L_54 = L_53->get_b_2();
		NullCheck(L_52);
		StringBuilder_Append_m3191759736(L_52, L_54, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_55 = V_0;
		NullCheck(L_55);
		StringBuilder_Append_m1965104174(L_55, _stringLiteral3452614550, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_56 = V_0;
		Color_t2555686324 * L_57 = __this->get_address_of_backgroundColor_7();
		float L_58 = L_57->get_a_3();
		NullCheck(L_56);
		StringBuilder_Append_m3191759736(L_56, L_58, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_59 = V_0;
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_59);
		return L_60;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::NendAdView_OnFinishLoad(System.String)
extern "C"  void NendAdBanner_NendAdView_OnFinishLoad_m362932734 (NendAdBanner_t489953245 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_NendAdView_OnFinishLoad_m362932734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdLoaded_11();
		V_0 = L_0;
		EventHandler_t1348719766 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		EventHandler_t1348719766 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs_t3591816995 * L_3 = ((EventArgs_t3591816995_StaticFields*)EventArgs_t3591816995_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_2);
		EventHandler_Invoke_m2047579917(L_2, __this, L_3, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::NendAdView_OnFailToReceiveAd(System.String)
extern "C"  void NendAdBanner_NendAdView_OnFailToReceiveAd_m3984165535 (NendAdBanner_t489953245 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_NendAdView_OnFailToReceiveAd_m3984165535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	EventHandler_1_t1314708246 * V_1 = NULL;
	NendAdErrorEventArgs_t3390548813 * V_2 = NULL;
	{
		String_t* L_0 = ___message0;
		CharU5BU5D_t3528271667* L_1 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
		NullCheck(L_0);
		StringU5BU5D_t1281789340* L_2 = String_Split_m3646115398(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		StringU5BU5D_t1281789340* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((int32_t)2)))
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		EventHandler_1_t1314708246 * L_4 = __this->get_AdFailedToReceive_12();
		V_1 = L_4;
		EventHandler_1_t1314708246 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_004e;
		}
	}
	{
		NendAdErrorEventArgs_t3390548813 * L_6 = (NendAdErrorEventArgs_t3390548813 *)il2cpp_codegen_object_new(NendAdErrorEventArgs_t3390548813_il2cpp_TypeInfo_var);
		NendAdErrorEventArgs__ctor_m3623794569(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		NendAdErrorEventArgs_t3390548813 * L_7 = V_2;
		StringU5BU5D_t1281789340* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		String_t* L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		int32_t L_11 = Int32_Parse_m1033611559(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		NendAdErrorEventArgs_set_ErrorCode_m1342030409(L_7, L_11, /*hidden argument*/NULL);
		NendAdErrorEventArgs_t3390548813 * L_12 = V_2;
		StringU5BU5D_t1281789340* L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = 1;
		String_t* L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_12);
		NendAdErrorEventArgs_set_Message_m1888969659(L_12, L_15, /*hidden argument*/NULL);
		EventHandler_1_t1314708246 * L_16 = V_1;
		NendAdErrorEventArgs_t3390548813 * L_17 = V_2;
		NullCheck(L_16);
		EventHandler_1_Invoke_m1615702581(L_16, __this, L_17, /*hidden argument*/EventHandler_1_Invoke_m1615702581_MethodInfo_var);
	}

IL_004e:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::NendAdView_OnReceiveAd(System.String)
extern "C"  void NendAdBanner_NendAdView_OnReceiveAd_m3550334308 (NendAdBanner_t489953245 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_NendAdView_OnReceiveAd_m3550334308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdReceived_13();
		V_0 = L_0;
		EventHandler_t1348719766 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		EventHandler_t1348719766 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs_t3591816995 * L_3 = ((EventArgs_t3591816995_StaticFields*)EventArgs_t3591816995_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_2);
		EventHandler_Invoke_m2047579917(L_2, __this, L_3, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::NendAdView_OnClickAd(System.String)
extern "C"  void NendAdBanner_NendAdView_OnClickAd_m2773684645 (NendAdBanner_t489953245 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_NendAdView_OnClickAd_m2773684645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdClicked_14();
		V_0 = L_0;
		EventHandler_t1348719766 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		EventHandler_t1348719766 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs_t3591816995 * L_3 = ((EventArgs_t3591816995_StaticFields*)EventArgs_t3591816995_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_2);
		EventHandler_Invoke_m2047579917(L_2, __this, L_3, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::NendAdView_OnDismissScreen(System.String)
extern "C"  void NendAdBanner_NendAdView_OnDismissScreen_m2389590666 (NendAdBanner_t489953245 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_NendAdView_OnDismissScreen_m2389590666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_AdBacked_15();
		V_0 = L_0;
		EventHandler_t1348719766 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		EventHandler_t1348719766 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs_t3591816995 * L_3 = ((EventArgs_t3591816995_StaticFields*)EventArgs_t3591816995_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_2);
		EventHandler_Invoke_m2047579917(L_2, __this, L_3, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdBanner::NendAdView_OnClickInformation(System.String)
extern "C"  void NendAdBanner_NendAdView_OnClickInformation_m2444220285 (NendAdBanner_t489953245 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdBanner_NendAdView_OnClickInformation_m2444220285_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t1348719766 * V_0 = NULL;
	{
		EventHandler_t1348719766 * L_0 = __this->get_InformationClicked_16();
		V_0 = L_0;
		EventHandler_t1348719766 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		EventHandler_t1348719766 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs_t3591816995 * L_3 = ((EventArgs_t3591816995_StaticFields*)EventArgs_t3591816995_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_2);
		EventHandler_Invoke_m2047579917(L_2, __this, L_3, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::.ctor()
extern "C"  void NendAdInterstitial__ctor_m161715398 (NendAdInterstitial_t1417649638 * __this, const MethodInfo* method)
{
	{
		__this->set_m_IsAutoReloadEnabled_8((bool)1);
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// NendUnityPlugin.AD.NendAdInterstitial NendUnityPlugin.AD.NendAdInterstitial::get_Instance()
extern "C"  NendAdInterstitial_t1417649638 * NendAdInterstitial_get_Instance_m3378847411 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_get_Instance_m3378847411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdInterstitial_t1417649638_il2cpp_TypeInfo_var);
		NendAdInterstitial_t1417649638 * L_0 = ((NendAdInterstitial_t1417649638_StaticFields*)NendAdInterstitial_t1417649638_il2cpp_TypeInfo_var->static_fields)->get__instance_7();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::set_IsAutoReloadEnabled(System.Boolean)
extern "C"  void NendAdInterstitial_set_IsAutoReloadEnabled_m1382953156 (NendAdInterstitial_t1417649638 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_set_IsAutoReloadEnabled_m1382953156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		__this->set_m_IsAutoReloadEnabled_8(L_0);
		Il2CppObject * L_1 = NendAdInterstitial_get_Interface_m2607091446(__this, /*hidden argument*/NULL);
		bool L_2 = __this->get_m_IsAutoReloadEnabled_8();
		NullCheck(L_1);
		InterfaceActionInvoker1< bool >::Invoke(3 /* System.Void NendUnityPlugin.Platform.NendAdInterstitialInterface::SetAutoReloadEnabled(System.Boolean) */, NendAdInterstitialInterface_t638097589_il2cpp_TypeInfo_var, L_1, L_2);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.NendAdInterstitial::get_IsAutoReloadEnabled()
extern "C"  bool NendAdInterstitial_get_IsAutoReloadEnabled_m987388302 (NendAdInterstitial_t1417649638 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_IsAutoReloadEnabled_8();
		return L_0;
	}
}
// NendUnityPlugin.Platform.NendAdInterstitialInterface NendUnityPlugin.AD.NendAdInterstitial::get_Interface()
extern "C"  Il2CppObject * NendAdInterstitial_get_Interface_m2607091446 (NendAdInterstitial_t1417649638 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get__interface_9();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = NendAdNativeInterfaceFactory_CreateInterstitialAdInterface_m687083042(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__interface_9(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = __this->get__interface_9();
		return L_2;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::add_AdLoaded(System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs>)
extern "C"  void NendAdInterstitial_add_AdLoaded_m3211223500 (NendAdInterstitial_t1417649638 * __this, EventHandler_1_t3479984044 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_add_AdLoaded_m3211223500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3479984044 * V_0 = NULL;
	EventHandler_1_t3479984044 * V_1 = NULL;
	{
		EventHandler_1_t3479984044 * L_0 = __this->get_AdLoaded_10();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3479984044 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3479984044 ** L_2 = __this->get_address_of_AdLoaded_10();
		EventHandler_1_t3479984044 * L_3 = V_1;
		EventHandler_1_t3479984044 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3479984044 * L_6 = V_0;
		EventHandler_1_t3479984044 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3479984044 *>(L_2, ((EventHandler_1_t3479984044 *)CastclassSealed(L_5, EventHandler_1_t3479984044_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3479984044 * L_8 = V_0;
		EventHandler_1_t3479984044 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t3479984044 *)L_8) == ((Il2CppObject*)(EventHandler_1_t3479984044 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::remove_AdLoaded(System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs>)
extern "C"  void NendAdInterstitial_remove_AdLoaded_m2017454671 (NendAdInterstitial_t1417649638 * __this, EventHandler_1_t3479984044 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_remove_AdLoaded_m2017454671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3479984044 * V_0 = NULL;
	EventHandler_1_t3479984044 * V_1 = NULL;
	{
		EventHandler_1_t3479984044 * L_0 = __this->get_AdLoaded_10();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3479984044 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3479984044 ** L_2 = __this->get_address_of_AdLoaded_10();
		EventHandler_1_t3479984044 * L_3 = V_1;
		EventHandler_1_t3479984044 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3479984044 * L_6 = V_0;
		EventHandler_1_t3479984044 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3479984044 *>(L_2, ((EventHandler_1_t3479984044 *)CastclassSealed(L_5, EventHandler_1_t3479984044_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3479984044 * L_8 = V_0;
		EventHandler_1_t3479984044 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t3479984044 *)L_8) == ((Il2CppObject*)(EventHandler_1_t3479984044 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::add_AdClicked(System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialClickEventArgs>)
extern "C"  void NendAdInterstitial_add_AdClicked_m1765401919 (NendAdInterstitial_t1417649638 * __this, EventHandler_1_t3193289641 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_add_AdClicked_m1765401919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3193289641 * V_0 = NULL;
	EventHandler_1_t3193289641 * V_1 = NULL;
	{
		EventHandler_1_t3193289641 * L_0 = __this->get_AdClicked_11();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3193289641 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3193289641 ** L_2 = __this->get_address_of_AdClicked_11();
		EventHandler_1_t3193289641 * L_3 = V_1;
		EventHandler_1_t3193289641 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3193289641 * L_6 = V_0;
		EventHandler_1_t3193289641 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3193289641 *>(L_2, ((EventHandler_1_t3193289641 *)CastclassSealed(L_5, EventHandler_1_t3193289641_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3193289641 * L_8 = V_0;
		EventHandler_1_t3193289641 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t3193289641 *)L_8) == ((Il2CppObject*)(EventHandler_1_t3193289641 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::remove_AdClicked(System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialClickEventArgs>)
extern "C"  void NendAdInterstitial_remove_AdClicked_m3393357395 (NendAdInterstitial_t1417649638 * __this, EventHandler_1_t3193289641 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_remove_AdClicked_m3393357395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3193289641 * V_0 = NULL;
	EventHandler_1_t3193289641 * V_1 = NULL;
	{
		EventHandler_1_t3193289641 * L_0 = __this->get_AdClicked_11();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3193289641 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3193289641 ** L_2 = __this->get_address_of_AdClicked_11();
		EventHandler_1_t3193289641 * L_3 = V_1;
		EventHandler_1_t3193289641 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3193289641 * L_6 = V_0;
		EventHandler_1_t3193289641 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3193289641 *>(L_2, ((EventHandler_1_t3193289641 *)CastclassSealed(L_5, EventHandler_1_t3193289641_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3193289641 * L_8 = V_0;
		EventHandler_1_t3193289641 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t3193289641 *)L_8) == ((Il2CppObject*)(EventHandler_1_t3193289641 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::add_AdShown(System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialShowEventArgs>)
extern "C"  void NendAdInterstitial_add_AdShown_m4252266033 (NendAdInterstitial_t1417649638 * __this, EventHandler_1_t1007381714 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_add_AdShown_m4252266033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1007381714 * V_0 = NULL;
	EventHandler_1_t1007381714 * V_1 = NULL;
	{
		EventHandler_1_t1007381714 * L_0 = __this->get_AdShown_12();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1007381714 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1007381714 ** L_2 = __this->get_address_of_AdShown_12();
		EventHandler_1_t1007381714 * L_3 = V_1;
		EventHandler_1_t1007381714 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1007381714 * L_6 = V_0;
		EventHandler_1_t1007381714 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1007381714 *>(L_2, ((EventHandler_1_t1007381714 *)CastclassSealed(L_5, EventHandler_1_t1007381714_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1007381714 * L_8 = V_0;
		EventHandler_1_t1007381714 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1007381714 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1007381714 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::remove_AdShown(System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialShowEventArgs>)
extern "C"  void NendAdInterstitial_remove_AdShown_m2632188688 (NendAdInterstitial_t1417649638 * __this, EventHandler_1_t1007381714 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_remove_AdShown_m2632188688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t1007381714 * V_0 = NULL;
	EventHandler_1_t1007381714 * V_1 = NULL;
	{
		EventHandler_1_t1007381714 * L_0 = __this->get_AdShown_12();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t1007381714 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t1007381714 ** L_2 = __this->get_address_of_AdShown_12();
		EventHandler_1_t1007381714 * L_3 = V_1;
		EventHandler_1_t1007381714 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t1007381714 * L_6 = V_0;
		EventHandler_1_t1007381714 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t1007381714 *>(L_2, ((EventHandler_1_t1007381714 *)CastclassSealed(L_5, EventHandler_1_t1007381714_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t1007381714 * L_8 = V_0;
		EventHandler_1_t1007381714 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_1_t1007381714 *)L_8) == ((Il2CppObject*)(EventHandler_1_t1007381714 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::Awake()
extern "C"  void NendAdInterstitial_Awake_m4029310310 (NendAdInterstitial_t1417649638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_Awake_m4029310310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdInterstitial_t1417649638_il2cpp_TypeInfo_var);
		NendAdInterstitial_t1417649638 * L_0 = ((NendAdInterstitial_t1417649638_StaticFields*)NendAdInterstitial_t1417649638_il2cpp_TypeInfo_var->static_fields)->get__instance_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NendAdInterstitial_t1417649638_il2cpp_TypeInfo_var);
		((NendAdInterstitial_t1417649638_StaticFields*)NendAdInterstitial_t1417649638_il2cpp_TypeInfo_var->static_fields)->set__instance_7(__this);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Object_set_hideFlags_m395729791(L_2, ((int32_t)61), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m3153277066(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_003e;
	}

IL_0033:
	{
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m2752645118(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_003e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NendAdInterstitial_Load_m3683620854(__this, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::Load(System.String,System.String)
extern "C"  void NendAdInterstitial_Load_m3683620854 (NendAdInterstitial_t1417649638 * __this, String_t* ___apiKey0, String_t* ___spotId1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_Load_m3683620854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_ios_key_5();
		___apiKey0 = L_0;
		String_t* L_1 = __this->get_ios_id_6();
		___spotId1 = L_1;
		Il2CppObject * L_2 = NendAdInterstitial_get_Interface_m2607091446(__this, /*hidden argument*/NULL);
		String_t* L_3 = ___apiKey0;
		String_t* L_4 = ___spotId1;
		bool L_5 = __this->get_outputLog_2();
		NullCheck(L_2);
		InterfaceActionInvoker3< String_t*, String_t*, bool >::Invoke(0 /* System.Void NendUnityPlugin.Platform.NendAdInterstitialInterface::LoadInterstitialAd(System.String,System.String,System.Boolean) */, NendAdInterstitialInterface_t638097589_il2cpp_TypeInfo_var, L_2, L_3, L_4, L_5);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::Show()
extern "C"  void NendAdInterstitial_Show_m3619914196 (NendAdInterstitial_t1417649638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_Show_m3619914196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = NendAdInterstitial_get_Interface_m2607091446(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(1 /* System.Void NendUnityPlugin.Platform.NendAdInterstitialInterface::ShowInterstitialAd(System.String) */, NendAdInterstitialInterface_t638097589_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::Show(System.String)
extern "C"  void NendAdInterstitial_Show_m2577433912 (NendAdInterstitial_t1417649638 * __this, String_t* ___spotId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_Show_m2577433912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = NendAdInterstitial_get_Interface_m2607091446(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___spotId0;
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(1 /* System.Void NendUnityPlugin.Platform.NendAdInterstitialInterface::ShowInterstitialAd(System.String) */, NendAdInterstitialInterface_t638097589_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::Dismiss()
extern "C"  void NendAdInterstitial_Dismiss_m326668577 (NendAdInterstitial_t1417649638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_Dismiss_m326668577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = NendAdInterstitial_get_Interface_m2607091446(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void NendUnityPlugin.Platform.NendAdInterstitialInterface::DismissInterstitialAd() */, NendAdInterstitialInterface_t638097589_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::NendAdInterstitial_OnFinishLoad(System.String)
extern "C"  void NendAdInterstitial_NendAdInterstitial_OnFinishLoad_m2262400418 (NendAdInterstitial_t1417649638 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_NendAdInterstitial_OnFinishLoad_m2262400418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	EventHandler_1_t3479984044 * V_3 = NULL;
	NendAdInterstitialLoadEventArgs_t1260857315 * V_4 = NULL;
	{
		String_t* L_0 = ___message0;
		CharU5BU5D_t3528271667* L_1 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
		NullCheck(L_0);
		StringU5BU5D_t1281789340* L_2 = String_Split_m3646115398(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		StringU5BU5D_t1281789340* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((int32_t)2)))
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		StringU5BU5D_t1281789340* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		String_t* L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int32_t L_7 = Int32_Parse_m1033611559(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		StringU5BU5D_t1281789340* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = 1;
		String_t* L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = L_10;
		EventHandler_1_t3479984044 * L_11 = __this->get_AdLoaded_10();
		V_3 = L_11;
		EventHandler_1_t3479984044 * L_12 = V_3;
		if (!L_12)
		{
			goto IL_0056;
		}
	}
	{
		NendAdInterstitialLoadEventArgs_t1260857315 * L_13 = (NendAdInterstitialLoadEventArgs_t1260857315 *)il2cpp_codegen_object_new(NendAdInterstitialLoadEventArgs_t1260857315_il2cpp_TypeInfo_var);
		NendAdInterstitialLoadEventArgs__ctor_m3921384839(L_13, /*hidden argument*/NULL);
		V_4 = L_13;
		NendAdInterstitialLoadEventArgs_t1260857315 * L_14 = V_4;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		NendAdInterstitialLoadEventArgs_set_StatusCode_m2181589091(L_14, L_15, /*hidden argument*/NULL);
		NendAdInterstitialLoadEventArgs_t1260857315 * L_16 = V_4;
		String_t* L_17 = V_2;
		NullCheck(L_16);
		NendAdInterstitialLoadEventArgs_set_SpotId_m228308284(L_16, L_17, /*hidden argument*/NULL);
		EventHandler_1_t3479984044 * L_18 = V_3;
		NendAdInterstitialLoadEventArgs_t1260857315 * L_19 = V_4;
		NullCheck(L_18);
		EventHandler_1_Invoke_m1611654412(L_18, __this, L_19, /*hidden argument*/EventHandler_1_Invoke_m1611654412_MethodInfo_var);
	}

IL_0056:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::NendAdInterstitial_OnClickAd(System.String)
extern "C"  void NendAdInterstitial_NendAdInterstitial_OnClickAd_m1739663766 (NendAdInterstitial_t1417649638 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_NendAdInterstitial_OnClickAd_m1739663766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	EventHandler_1_t3193289641 * V_3 = NULL;
	NendAdInterstitialClickEventArgs_t974162912 * V_4 = NULL;
	{
		String_t* L_0 = ___message0;
		CharU5BU5D_t3528271667* L_1 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
		NullCheck(L_0);
		StringU5BU5D_t1281789340* L_2 = String_Split_m3646115398(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		StringU5BU5D_t1281789340* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((int32_t)2)))
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		StringU5BU5D_t1281789340* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		String_t* L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int32_t L_7 = Int32_Parse_m1033611559(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		StringU5BU5D_t1281789340* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = 1;
		String_t* L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = L_10;
		EventHandler_1_t3193289641 * L_11 = __this->get_AdClicked_11();
		V_3 = L_11;
		EventHandler_1_t3193289641 * L_12 = V_3;
		if (!L_12)
		{
			goto IL_0056;
		}
	}
	{
		NendAdInterstitialClickEventArgs_t974162912 * L_13 = (NendAdInterstitialClickEventArgs_t974162912 *)il2cpp_codegen_object_new(NendAdInterstitialClickEventArgs_t974162912_il2cpp_TypeInfo_var);
		NendAdInterstitialClickEventArgs__ctor_m4030220703(L_13, /*hidden argument*/NULL);
		V_4 = L_13;
		NendAdInterstitialClickEventArgs_t974162912 * L_14 = V_4;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		NendAdInterstitialClickEventArgs_set_ClickType_m2779883726(L_14, L_15, /*hidden argument*/NULL);
		NendAdInterstitialClickEventArgs_t974162912 * L_16 = V_4;
		String_t* L_17 = V_2;
		NullCheck(L_16);
		NendAdInterstitialClickEventArgs_set_SpotId_m2251321628(L_16, L_17, /*hidden argument*/NULL);
		EventHandler_1_t3193289641 * L_18 = V_3;
		NendAdInterstitialClickEventArgs_t974162912 * L_19 = V_4;
		NullCheck(L_18);
		EventHandler_1_Invoke_m2443591581(L_18, __this, L_19, /*hidden argument*/EventHandler_1_Invoke_m2443591581_MethodInfo_var);
	}

IL_0056:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::NendAdInterstitial_OnShowAd(System.String)
extern "C"  void NendAdInterstitial_NendAdInterstitial_OnShowAd_m1206226545 (NendAdInterstitial_t1417649638 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitial_NendAdInterstitial_OnShowAd_m1206226545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	EventHandler_1_t1007381714 * V_3 = NULL;
	NendAdInterstitialShowEventArgs_t3083222281 * V_4 = NULL;
	{
		String_t* L_0 = ___message0;
		CharU5BU5D_t3528271667* L_1 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
		NullCheck(L_0);
		StringU5BU5D_t1281789340* L_2 = String_Split_m3646115398(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		StringU5BU5D_t1281789340* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((int32_t)2)))
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		StringU5BU5D_t1281789340* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		String_t* L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int32_t L_7 = Int32_Parse_m1033611559(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		StringU5BU5D_t1281789340* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = 1;
		String_t* L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_2 = L_10;
		EventHandler_1_t1007381714 * L_11 = __this->get_AdShown_12();
		V_3 = L_11;
		EventHandler_1_t1007381714 * L_12 = V_3;
		if (!L_12)
		{
			goto IL_0056;
		}
	}
	{
		NendAdInterstitialShowEventArgs_t3083222281 * L_13 = (NendAdInterstitialShowEventArgs_t3083222281 *)il2cpp_codegen_object_new(NendAdInterstitialShowEventArgs_t3083222281_il2cpp_TypeInfo_var);
		NendAdInterstitialShowEventArgs__ctor_m4100517664(L_13, /*hidden argument*/NULL);
		V_4 = L_13;
		NendAdInterstitialShowEventArgs_t3083222281 * L_14 = V_4;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		NendAdInterstitialShowEventArgs_set_ShowResult_m1732319236(L_14, L_15, /*hidden argument*/NULL);
		NendAdInterstitialShowEventArgs_t3083222281 * L_16 = V_4;
		String_t* L_17 = V_2;
		NullCheck(L_16);
		NendAdInterstitialShowEventArgs_set_SpotId_m2748712616(L_16, L_17, /*hidden argument*/NULL);
		EventHandler_1_t1007381714 * L_18 = V_3;
		NendAdInterstitialShowEventArgs_t3083222281 * L_19 = V_4;
		NullCheck(L_18);
		EventHandler_1_Invoke_m1676777043(L_18, __this, L_19, /*hidden argument*/EventHandler_1_Invoke_m1676777043_MethodInfo_var);
	}

IL_0056:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitial::.cctor()
extern "C"  void NendAdInterstitial__cctor_m4249062705 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitialClickEventArgs::.ctor()
extern "C"  void NendAdInterstitialClickEventArgs__ctor_m4030220703 (NendAdInterstitialClickEventArgs_t974162912 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitialClickEventArgs__ctor_m4030220703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		return;
	}
}
// NendUnityPlugin.Common.NendAdInterstitialClickType NendUnityPlugin.AD.NendAdInterstitialClickEventArgs::get_ClickType()
extern "C"  int32_t NendAdInterstitialClickEventArgs_get_ClickType_m2421430467 (NendAdInterstitialClickEventArgs_t974162912 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CClickTypeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitialClickEventArgs::set_ClickType(NendUnityPlugin.Common.NendAdInterstitialClickType)
extern "C"  void NendAdInterstitialClickEventArgs_set_ClickType_m2779883726 (NendAdInterstitialClickEventArgs_t974162912 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CClickTypeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String NendUnityPlugin.AD.NendAdInterstitialClickEventArgs::get_SpotId()
extern "C"  String_t* NendAdInterstitialClickEventArgs_get_SpotId_m441827006 (NendAdInterstitialClickEventArgs_t974162912 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSpotIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitialClickEventArgs::set_SpotId(System.String)
extern "C"  void NendAdInterstitialClickEventArgs_set_SpotId_m2251321628 (NendAdInterstitialClickEventArgs_t974162912 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSpotIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs::.ctor()
extern "C"  void NendAdInterstitialLoadEventArgs__ctor_m3921384839 (NendAdInterstitialLoadEventArgs_t1260857315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitialLoadEventArgs__ctor_m3921384839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		return;
	}
}
// NendUnityPlugin.Common.NendAdInterstitialStatusCode NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs::get_StatusCode()
extern "C"  int32_t NendAdInterstitialLoadEventArgs_get_StatusCode_m1583536616 (NendAdInterstitialLoadEventArgs_t1260857315 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CStatusCodeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs::set_StatusCode(NendUnityPlugin.Common.NendAdInterstitialStatusCode)
extern "C"  void NendAdInterstitialLoadEventArgs_set_StatusCode_m2181589091 (NendAdInterstitialLoadEventArgs_t1260857315 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CStatusCodeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs::get_SpotId()
extern "C"  String_t* NendAdInterstitialLoadEventArgs_get_SpotId_m3098161421 (NendAdInterstitialLoadEventArgs_t1260857315 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSpotIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs::set_SpotId(System.String)
extern "C"  void NendAdInterstitialLoadEventArgs_set_SpotId_m228308284 (NendAdInterstitialLoadEventArgs_t1260857315 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSpotIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitialShowEventArgs::.ctor()
extern "C"  void NendAdInterstitialShowEventArgs__ctor_m4100517664 (NendAdInterstitialShowEventArgs_t3083222281 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdInterstitialShowEventArgs__ctor_m4100517664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		return;
	}
}
// NendUnityPlugin.Common.NendAdInterstitialShowResult NendUnityPlugin.AD.NendAdInterstitialShowEventArgs::get_ShowResult()
extern "C"  int32_t NendAdInterstitialShowEventArgs_get_ShowResult_m541663705 (NendAdInterstitialShowEventArgs_t3083222281 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CShowResultU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitialShowEventArgs::set_ShowResult(NendUnityPlugin.Common.NendAdInterstitialShowResult)
extern "C"  void NendAdInterstitialShowEventArgs_set_ShowResult_m1732319236 (NendAdInterstitialShowEventArgs_t3083222281 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CShowResultU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String NendUnityPlugin.AD.NendAdInterstitialShowEventArgs::get_SpotId()
extern "C"  String_t* NendAdInterstitialShowEventArgs_get_SpotId_m4065482830 (NendAdInterstitialShowEventArgs_t3083222281 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSpotIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.NendAdInterstitialShowEventArgs::set_SpotId(System.String)
extern "C"  void NendAdInterstitialShowEventArgs_set_SpotId_m2748712616 (NendAdInterstitialShowEventArgs_t3083222281 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSpotIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.ErrorVideoAdCallbackArgments::.ctor(NendUnityPlugin.AD.Video.NendAdVideo/VideoAdCallbackType,System.Int32)
extern "C"  void ErrorVideoAdCallbackArgments__ctor_m1329348770 (ErrorVideoAdCallbackArgments_t3139399691 * __this, int32_t ___type0, int32_t ___code1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___type0;
		VideoAdCallbackArgments__ctor_m1675151393(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___code1;
		__this->set_errorCode_1(L_1);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdInterstitialVideo::.ctor()
extern "C"  void NendAdInterstitialVideo__ctor_m2873077802 (NendAdInterstitialVideo_t1383172729 * __this, const MethodInfo* method)
{
	{
		NendAdVideo__ctor_m333837435(__this, /*hidden argument*/NULL);
		return;
	}
}
// NendUnityPlugin.AD.Video.NendAdInterstitialVideo NendUnityPlugin.AD.Video.NendAdInterstitialVideo::NewVideoAd(System.String,System.String)
extern "C"  NendAdInterstitialVideo_t1383172729 * NendAdInterstitialVideo_NewVideoAd_m102810292 (Il2CppObject * __this /* static, unused */, String_t* ___spotId0, String_t* ___apiKey1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___spotId0;
		String_t* L_1 = ___apiKey1;
		NendAdInterstitialVideo_t1383172729 * L_2 = NendAdNativeInterfaceFactory_CreateInterstitialVideoAd_m1938283520(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdInterstitialVideo::AddFallbackFullboard(System.String,System.String)
extern "C"  void NendAdInterstitialVideo_AddFallbackFullboard_m661861104 (NendAdInterstitialVideo_t1383172729 * __this, String_t* ___spotId0, String_t* ___apiKey1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___spotId0;
		String_t* L_1 = ___apiKey1;
		Color_t2555686324  L_2 = Color_get_black_m650597609(NULL /*static, unused*/, /*hidden argument*/NULL);
		NendAdInterstitialVideo_AddFallbackFullboard_m2575308473(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdInterstitialVideo::AddFallbackFullboard(System.String,System.String,UnityEngine.Color)
extern "C"  void NendAdInterstitialVideo_AddFallbackFullboard_m2575308473 (NendAdInterstitialVideo_t1383172729 * __this, String_t* ___spotId0, String_t* ___apiKey1, Color_t2555686324  ___iOSBackgroundColor2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___spotId0;
		String_t* L_1 = ___apiKey1;
		float L_2 = (&___iOSBackgroundColor2)->get_r_0();
		float L_3 = (&___iOSBackgroundColor2)->get_g_1();
		float L_4 = (&___iOSBackgroundColor2)->get_b_2();
		float L_5 = (&___iOSBackgroundColor2)->get_a_3();
		VirtActionInvoker6< String_t*, String_t*, float, float, float, float >::Invoke(16 /* System.Void NendUnityPlugin.AD.Video.NendAdInterstitialVideo::AddFallbackFullboardInternal(System.String,System.String,System.Single,System.Single,System.Single,System.Single) */, __this, L_0, L_1, L_2, L_3, L_4, L_5);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdInterstitialVideo::set_IsMuteStartPlaying(System.Boolean)
extern "C"  void NendAdInterstitialVideo_set_IsMuteStartPlaying_m3713353518 (NendAdInterstitialVideo_t1383172729 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		VirtActionInvoker1< bool >::Invoke(17 /* System.Void NendUnityPlugin.AD.Video.NendAdInterstitialVideo::SetMuteStartPlayingInternal(System.Boolean) */, __this, L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdRewardedItem::.ctor(System.String,System.Int32)
extern "C"  void NendAdRewardedItem__ctor_m3313747121 (NendAdRewardedItem_t3964314288 * __this, String_t* ___name0, int32_t ___amount1, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_currencyName_0(L_0);
		int32_t L_1 = ___amount1;
		__this->set_currencyAmount_1(L_1);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdRewardedVideo::.ctor()
extern "C"  void NendAdRewardedVideo__ctor_m547447602 (NendAdRewardedVideo_t3161671531 * __this, const MethodInfo* method)
{
	{
		NendAdVideo__ctor_m333837435(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdRewardedVideo::add_Rewarded(NendUnityPlugin.AD.Video.NendAdRewardedVideo/NendAdVideoRewarded)
extern "C"  void NendAdRewardedVideo_add_Rewarded_m3408280237 (NendAdRewardedVideo_t3161671531 * __this, NendAdVideoRewarded_t687064503 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdRewardedVideo_add_Rewarded_m3408280237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoRewarded_t687064503 * V_0 = NULL;
	NendAdVideoRewarded_t687064503 * V_1 = NULL;
	{
		NendAdVideoRewarded_t687064503 * L_0 = __this->get_Rewarded_10();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoRewarded_t687064503 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoRewarded_t687064503 ** L_2 = __this->get_address_of_Rewarded_10();
		NendAdVideoRewarded_t687064503 * L_3 = V_1;
		NendAdVideoRewarded_t687064503 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoRewarded_t687064503 * L_6 = V_0;
		NendAdVideoRewarded_t687064503 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoRewarded_t687064503 *>(L_2, ((NendAdVideoRewarded_t687064503 *)CastclassSealed(L_5, NendAdVideoRewarded_t687064503_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoRewarded_t687064503 * L_8 = V_0;
		NendAdVideoRewarded_t687064503 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoRewarded_t687064503 *)L_8) == ((Il2CppObject*)(NendAdVideoRewarded_t687064503 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdRewardedVideo::remove_Rewarded(NendUnityPlugin.AD.Video.NendAdRewardedVideo/NendAdVideoRewarded)
extern "C"  void NendAdRewardedVideo_remove_Rewarded_m1312421876 (NendAdRewardedVideo_t3161671531 * __this, NendAdVideoRewarded_t687064503 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdRewardedVideo_remove_Rewarded_m1312421876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoRewarded_t687064503 * V_0 = NULL;
	NendAdVideoRewarded_t687064503 * V_1 = NULL;
	{
		NendAdVideoRewarded_t687064503 * L_0 = __this->get_Rewarded_10();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoRewarded_t687064503 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoRewarded_t687064503 ** L_2 = __this->get_address_of_Rewarded_10();
		NendAdVideoRewarded_t687064503 * L_3 = V_1;
		NendAdVideoRewarded_t687064503 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoRewarded_t687064503 * L_6 = V_0;
		NendAdVideoRewarded_t687064503 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoRewarded_t687064503 *>(L_2, ((NendAdVideoRewarded_t687064503 *)CastclassSealed(L_5, NendAdVideoRewarded_t687064503_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoRewarded_t687064503 * L_8 = V_0;
		NendAdVideoRewarded_t687064503 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoRewarded_t687064503 *)L_8) == ((Il2CppObject*)(NendAdVideoRewarded_t687064503 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdRewardedVideo::CallBack(NendUnityPlugin.AD.Video.RewardedVideoAdCallbackArgments)
extern "C"  void NendAdRewardedVideo_CallBack_m2970974791 (NendAdRewardedVideo_t3161671531 * __this, RewardedVideoAdCallbackArgments_t3338436455 * ___args0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		RewardedVideoAdCallbackArgments_t3338436455 * L_0 = ___args0;
		NullCheck(L_0);
		int32_t L_1 = ((VideoAdCallbackArgments_t2567701888 *)L_0)->get_videoAdCallbackType_0();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)10))))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0036;
	}

IL_0014:
	{
		NendAdVideoRewarded_t687064503 * L_3 = __this->get_Rewarded_10();
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		NendAdVideoRewarded_t687064503 * L_4 = __this->get_Rewarded_10();
		RewardedVideoAdCallbackArgments_t3338436455 * L_5 = ___args0;
		NullCheck(L_5);
		NendAdRewardedItem_t3964314288 * L_6 = L_5->get_rewardedItem_1();
		NullCheck(L_4);
		NendAdVideoRewarded_Invoke_m656162502(L_4, __this, L_6, /*hidden argument*/NULL);
	}

IL_0031:
	{
		goto IL_0042;
	}

IL_0036:
	{
		RewardedVideoAdCallbackArgments_t3338436455 * L_7 = ___args0;
		NendAdVideo_CallBack_m1751514092(__this, L_7, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_0042:
	{
		return;
	}
}
// NendUnityPlugin.AD.Video.NendAdRewardedVideo NendUnityPlugin.AD.Video.NendAdRewardedVideo::NewVideoAd(System.String,System.String)
extern "C"  NendAdRewardedVideo_t3161671531 * NendAdRewardedVideo_NewVideoAd_m2506089312 (Il2CppObject * __this /* static, unused */, String_t* ___spotId0, String_t* ___apiKey1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___spotId0;
		String_t* L_1 = ___apiKey1;
		NendAdRewardedVideo_t3161671531 * L_2 = NendAdNativeInterfaceFactory_CreateRewardedVideoAd_m3735613872(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdRewardedVideo/NendAdVideoRewarded::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdVideoRewarded__ctor_m2148184731 (NendAdVideoRewarded_t687064503 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdRewardedVideo/NendAdVideoRewarded::Invoke(NendUnityPlugin.AD.Video.NendAdVideo,NendUnityPlugin.AD.Video.NendAdRewardedItem)
extern "C"  void NendAdVideoRewarded_Invoke_m656162502 (NendAdVideoRewarded_t687064503 * __this, NendAdVideo_t1781712043 * ___instance0, NendAdRewardedItem_t3964314288 * ___item1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdVideoRewarded_Invoke_m656162502((NendAdVideoRewarded_t687064503 *)__this->get_prev_9(),___instance0, ___item1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdVideo_t1781712043 * ___instance0, NendAdRewardedItem_t3964314288 * ___item1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0, ___item1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdVideo_t1781712043 * ___instance0, NendAdRewardedItem_t3964314288 * ___item1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0, ___item1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdRewardedItem_t3964314288 * ___item1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0, ___item1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.Video.NendAdRewardedVideo/NendAdVideoRewarded::BeginInvoke(NendUnityPlugin.AD.Video.NendAdVideo,NendUnityPlugin.AD.Video.NendAdRewardedItem,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdVideoRewarded_BeginInvoke_m4191019868 (NendAdVideoRewarded_t687064503 * __this, NendAdVideo_t1781712043 * ___instance0, NendAdRewardedItem_t3964314288 * ___item1, AsyncCallback_t3962456242 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___instance0;
	__d_args[1] = ___item1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void NendUnityPlugin.AD.Video.NendAdRewardedVideo/NendAdVideoRewarded::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdVideoRewarded_EndInvoke_m1882857208 (NendAdVideoRewarded_t687064503 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdUserFeature::.ctor()
extern "C"  void NendAdUserFeature__ctor_m941309312 (NendAdUserFeature_t4152365987 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdUserFeature__ctor_m941309312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_age_4((-1));
		Dictionary_2_t2736202052 * L_0 = (Dictionary_2_t2736202052 *)il2cpp_codegen_object_new(Dictionary_2_t2736202052_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3200964102(L_0, /*hidden argument*/Dictionary_2__ctor_m3200964102_MethodInfo_var);
		__this->set_customFeaturesIntDic_5(L_0);
		Dictionary_2_t379921662 * L_1 = (Dictionary_2_t379921662 *)il2cpp_codegen_object_new(Dictionary_2_t379921662_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m837984336(L_1, /*hidden argument*/Dictionary_2__ctor_m837984336_MethodInfo_var);
		__this->set_customFeaturesDoubleDic_6(L_1);
		Dictionary_2_t1632706988 * L_2 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2842384104(L_2, /*hidden argument*/Dictionary_2__ctor_m2842384104_MethodInfo_var);
		__this->set_customFeaturesStringDic_7(L_2);
		Dictionary_2_t4177511560 * L_3 = (Dictionary_2_t4177511560 *)il2cpp_codegen_object_new(Dictionary_2_t4177511560_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m256458156(L_3, /*hidden argument*/Dictionary_2__ctor_m256458156_MethodInfo_var);
		__this->set_customFeaturesBoolDic_8(L_3);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdUserFeature::SetBirthday(System.Int32,System.Int32,System.Int32)
extern "C"  void NendAdUserFeature_SetBirthday_m893525444 (NendAdUserFeature_t4152365987 * __this, int32_t ___year0, int32_t ___month1, int32_t ___day2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___year0;
		__this->set_yearOfBirth_3(L_0);
		int32_t L_1 = ___month1;
		__this->set_monthOfBirth_2(L_1);
		int32_t L_2 = ___day2;
		__this->set_dayOfBirth_1(L_2);
		return;
	}
}
// System.Int32 NendUnityPlugin.AD.Video.NendAdUserFeature::get_YearOfBirth()
extern "C"  int32_t NendAdUserFeature_get_YearOfBirth_m736965014 (NendAdUserFeature_t4152365987 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_yearOfBirth_3();
		return L_0;
	}
}
// System.Int32 NendUnityPlugin.AD.Video.NendAdUserFeature::get_MonthOfBirth()
extern "C"  int32_t NendAdUserFeature_get_MonthOfBirth_m1404665578 (NendAdUserFeature_t4152365987 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_monthOfBirth_2();
		return L_0;
	}
}
// System.Int32 NendUnityPlugin.AD.Video.NendAdUserFeature::get_DayOfBirth()
extern "C"  int32_t NendAdUserFeature_get_DayOfBirth_m1804847905 (NendAdUserFeature_t4152365987 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_dayOfBirth_1();
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdUserFeature::AddCustomFeature(System.String,System.Int32)
extern "C"  void NendAdUserFeature_AddCustomFeature_m73758447 (NendAdUserFeature_t4152365987 * __this, String_t* ___key0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdUserFeature_AddCustomFeature_m73758447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2736202052 * L_0 = __this->get_customFeaturesIntDic_5();
		String_t* L_1 = ___key0;
		int32_t L_2 = ___value1;
		NullCheck(L_0);
		Dictionary_2_set_Item_m3800595820(L_0, L_1, L_2, /*hidden argument*/Dictionary_2_set_Item_m3800595820_MethodInfo_var);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdUserFeature::AddCustomFeature(System.String,System.Double)
extern "C"  void NendAdUserFeature_AddCustomFeature_m2842110208 (NendAdUserFeature_t4152365987 * __this, String_t* ___key0, double ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdUserFeature_AddCustomFeature_m2842110208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t379921662 * L_0 = __this->get_customFeaturesDoubleDic_6();
		String_t* L_1 = ___key0;
		double L_2 = ___value1;
		NullCheck(L_0);
		Dictionary_2_set_Item_m83463571(L_0, L_1, L_2, /*hidden argument*/Dictionary_2_set_Item_m83463571_MethodInfo_var);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdUserFeature::AddCustomFeature(System.String,System.String)
extern "C"  void NendAdUserFeature_AddCustomFeature_m170006368 (NendAdUserFeature_t4152365987 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdUserFeature_AddCustomFeature_m170006368_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1632706988 * L_0 = __this->get_customFeaturesStringDic_7();
		String_t* L_1 = ___key0;
		String_t* L_2 = ___value1;
		NullCheck(L_0);
		Dictionary_2_set_Item_m1950792581(L_0, L_1, L_2, /*hidden argument*/Dictionary_2_set_Item_m1950792581_MethodInfo_var);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdUserFeature::AddCustomFeature(System.String,System.Boolean)
extern "C"  void NendAdUserFeature_AddCustomFeature_m4244280464 (NendAdUserFeature_t4152365987 * __this, String_t* ___key0, bool ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdUserFeature_AddCustomFeature_m4244280464_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t4177511560 * L_0 = __this->get_customFeaturesBoolDic_8();
		String_t* L_1 = ___key0;
		bool L_2 = ___value1;
		NullCheck(L_0);
		Dictionary_2_set_Item_m2347951726(L_0, L_1, L_2, /*hidden argument*/Dictionary_2_set_Item_m2347951726_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> NendUnityPlugin.AD.Video.NendAdUserFeature::get_CustomFeaturesInt()
extern "C"  Dictionary_2_t2736202052 * NendAdUserFeature_get_CustomFeaturesInt_m3875867359 (NendAdUserFeature_t4152365987 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2736202052 * L_0 = __this->get_customFeaturesIntDic_5();
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Double> NendUnityPlugin.AD.Video.NendAdUserFeature::get_CustomFeaturesDouble()
extern "C"  Dictionary_2_t379921662 * NendAdUserFeature_get_CustomFeaturesDouble_m576465591 (NendAdUserFeature_t4152365987 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t379921662 * L_0 = __this->get_customFeaturesDoubleDic_6();
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> NendUnityPlugin.AD.Video.NendAdUserFeature::get_CustomFeaturesString()
extern "C"  Dictionary_2_t1632706988 * NendAdUserFeature_get_CustomFeaturesString_m4033287446 (NendAdUserFeature_t4152365987 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1632706988 * L_0 = __this->get_customFeaturesStringDic_7();
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> NendUnityPlugin.AD.Video.NendAdUserFeature::get_CustomFeaturesBool()
extern "C"  Dictionary_2_t4177511560 * NendAdUserFeature_get_CustomFeaturesBool_m1494354628 (NendAdUserFeature_t4152365987 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t4177511560 * L_0 = __this->get_customFeaturesBoolDic_8();
		return L_0;
	}
}
// NendUnityPlugin.AD.Video.NendAdUserFeature NendUnityPlugin.AD.Video.NendAdUserFeature::NewNendAdUserFeature()
extern "C"  NendAdUserFeature_t4152365987 * NendAdUserFeature_NewNendAdUserFeature_m285187920 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdUserFeature_NewNendAdUserFeature_m285187920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IOSUserFeature_t2237018260 * L_0 = (IOSUserFeature_t2237018260 *)il2cpp_codegen_object_new(IOSUserFeature_t2237018260_il2cpp_TypeInfo_var);
		IOSUserFeature__ctor_m2184262995(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::.ctor()
extern "C"  void NendAdVideo__ctor_m333837435 (NendAdVideo_t1781712043 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::add_AdLoaded(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoLoaded)
extern "C"  void NendAdVideo_add_AdLoaded_m238795991 (NendAdVideo_t1781712043 * __this, NendAdVideoLoaded_t1357133087 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_add_AdLoaded_m238795991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoLoaded_t1357133087 * V_0 = NULL;
	NendAdVideoLoaded_t1357133087 * V_1 = NULL;
	{
		NendAdVideoLoaded_t1357133087 * L_0 = __this->get_AdLoaded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoLoaded_t1357133087 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoLoaded_t1357133087 ** L_2 = __this->get_address_of_AdLoaded_0();
		NendAdVideoLoaded_t1357133087 * L_3 = V_1;
		NendAdVideoLoaded_t1357133087 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoLoaded_t1357133087 * L_6 = V_0;
		NendAdVideoLoaded_t1357133087 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoLoaded_t1357133087 *>(L_2, ((NendAdVideoLoaded_t1357133087 *)CastclassSealed(L_5, NendAdVideoLoaded_t1357133087_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoLoaded_t1357133087 * L_8 = V_0;
		NendAdVideoLoaded_t1357133087 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoLoaded_t1357133087 *)L_8) == ((Il2CppObject*)(NendAdVideoLoaded_t1357133087 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::remove_AdLoaded(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoLoaded)
extern "C"  void NendAdVideo_remove_AdLoaded_m1949947559 (NendAdVideo_t1781712043 * __this, NendAdVideoLoaded_t1357133087 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_remove_AdLoaded_m1949947559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoLoaded_t1357133087 * V_0 = NULL;
	NendAdVideoLoaded_t1357133087 * V_1 = NULL;
	{
		NendAdVideoLoaded_t1357133087 * L_0 = __this->get_AdLoaded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoLoaded_t1357133087 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoLoaded_t1357133087 ** L_2 = __this->get_address_of_AdLoaded_0();
		NendAdVideoLoaded_t1357133087 * L_3 = V_1;
		NendAdVideoLoaded_t1357133087 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoLoaded_t1357133087 * L_6 = V_0;
		NendAdVideoLoaded_t1357133087 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoLoaded_t1357133087 *>(L_2, ((NendAdVideoLoaded_t1357133087 *)CastclassSealed(L_5, NendAdVideoLoaded_t1357133087_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoLoaded_t1357133087 * L_8 = V_0;
		NendAdVideoLoaded_t1357133087 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoLoaded_t1357133087 *)L_8) == ((Il2CppObject*)(NendAdVideoLoaded_t1357133087 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::add_AdFailedToLoad(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToLoad)
extern "C"  void NendAdVideo_add_AdFailedToLoad_m4143775789 (NendAdVideo_t1781712043 * __this, NendAdVideoFailedToLoad_t1837550073 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_add_AdFailedToLoad_m4143775789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoFailedToLoad_t1837550073 * V_0 = NULL;
	NendAdVideoFailedToLoad_t1837550073 * V_1 = NULL;
	{
		NendAdVideoFailedToLoad_t1837550073 * L_0 = __this->get_AdFailedToLoad_1();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoFailedToLoad_t1837550073 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoFailedToLoad_t1837550073 ** L_2 = __this->get_address_of_AdFailedToLoad_1();
		NendAdVideoFailedToLoad_t1837550073 * L_3 = V_1;
		NendAdVideoFailedToLoad_t1837550073 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoFailedToLoad_t1837550073 * L_6 = V_0;
		NendAdVideoFailedToLoad_t1837550073 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoFailedToLoad_t1837550073 *>(L_2, ((NendAdVideoFailedToLoad_t1837550073 *)CastclassSealed(L_5, NendAdVideoFailedToLoad_t1837550073_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoFailedToLoad_t1837550073 * L_8 = V_0;
		NendAdVideoFailedToLoad_t1837550073 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoFailedToLoad_t1837550073 *)L_8) == ((Il2CppObject*)(NendAdVideoFailedToLoad_t1837550073 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::remove_AdFailedToLoad(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToLoad)
extern "C"  void NendAdVideo_remove_AdFailedToLoad_m3941862212 (NendAdVideo_t1781712043 * __this, NendAdVideoFailedToLoad_t1837550073 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_remove_AdFailedToLoad_m3941862212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoFailedToLoad_t1837550073 * V_0 = NULL;
	NendAdVideoFailedToLoad_t1837550073 * V_1 = NULL;
	{
		NendAdVideoFailedToLoad_t1837550073 * L_0 = __this->get_AdFailedToLoad_1();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoFailedToLoad_t1837550073 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoFailedToLoad_t1837550073 ** L_2 = __this->get_address_of_AdFailedToLoad_1();
		NendAdVideoFailedToLoad_t1837550073 * L_3 = V_1;
		NendAdVideoFailedToLoad_t1837550073 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoFailedToLoad_t1837550073 * L_6 = V_0;
		NendAdVideoFailedToLoad_t1837550073 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoFailedToLoad_t1837550073 *>(L_2, ((NendAdVideoFailedToLoad_t1837550073 *)CastclassSealed(L_5, NendAdVideoFailedToLoad_t1837550073_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoFailedToLoad_t1837550073 * L_8 = V_0;
		NendAdVideoFailedToLoad_t1837550073 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoFailedToLoad_t1837550073 *)L_8) == ((Il2CppObject*)(NendAdVideoFailedToLoad_t1837550073 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::add_AdFailedToPlay(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToPlay)
extern "C"  void NendAdVideo_add_AdFailedToPlay_m1928710374 (NendAdVideo_t1781712043 * __this, NendAdVideoFailedToPlay_t4265396701 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_add_AdFailedToPlay_m1928710374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoFailedToPlay_t4265396701 * V_0 = NULL;
	NendAdVideoFailedToPlay_t4265396701 * V_1 = NULL;
	{
		NendAdVideoFailedToPlay_t4265396701 * L_0 = __this->get_AdFailedToPlay_2();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoFailedToPlay_t4265396701 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoFailedToPlay_t4265396701 ** L_2 = __this->get_address_of_AdFailedToPlay_2();
		NendAdVideoFailedToPlay_t4265396701 * L_3 = V_1;
		NendAdVideoFailedToPlay_t4265396701 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoFailedToPlay_t4265396701 * L_6 = V_0;
		NendAdVideoFailedToPlay_t4265396701 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoFailedToPlay_t4265396701 *>(L_2, ((NendAdVideoFailedToPlay_t4265396701 *)CastclassSealed(L_5, NendAdVideoFailedToPlay_t4265396701_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoFailedToPlay_t4265396701 * L_8 = V_0;
		NendAdVideoFailedToPlay_t4265396701 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoFailedToPlay_t4265396701 *)L_8) == ((Il2CppObject*)(NendAdVideoFailedToPlay_t4265396701 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::remove_AdFailedToPlay(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToPlay)
extern "C"  void NendAdVideo_remove_AdFailedToPlay_m4052357713 (NendAdVideo_t1781712043 * __this, NendAdVideoFailedToPlay_t4265396701 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_remove_AdFailedToPlay_m4052357713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoFailedToPlay_t4265396701 * V_0 = NULL;
	NendAdVideoFailedToPlay_t4265396701 * V_1 = NULL;
	{
		NendAdVideoFailedToPlay_t4265396701 * L_0 = __this->get_AdFailedToPlay_2();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoFailedToPlay_t4265396701 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoFailedToPlay_t4265396701 ** L_2 = __this->get_address_of_AdFailedToPlay_2();
		NendAdVideoFailedToPlay_t4265396701 * L_3 = V_1;
		NendAdVideoFailedToPlay_t4265396701 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoFailedToPlay_t4265396701 * L_6 = V_0;
		NendAdVideoFailedToPlay_t4265396701 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoFailedToPlay_t4265396701 *>(L_2, ((NendAdVideoFailedToPlay_t4265396701 *)CastclassSealed(L_5, NendAdVideoFailedToPlay_t4265396701_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoFailedToPlay_t4265396701 * L_8 = V_0;
		NendAdVideoFailedToPlay_t4265396701 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoFailedToPlay_t4265396701 *)L_8) == ((Il2CppObject*)(NendAdVideoFailedToPlay_t4265396701 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::add_AdShown(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoShown)
extern "C"  void NendAdVideo_add_AdShown_m3117307321 (NendAdVideo_t1781712043 * __this, NendAdVideoShown_t3987677121 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_add_AdShown_m3117307321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoShown_t3987677121 * V_0 = NULL;
	NendAdVideoShown_t3987677121 * V_1 = NULL;
	{
		NendAdVideoShown_t3987677121 * L_0 = __this->get_AdShown_3();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoShown_t3987677121 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoShown_t3987677121 ** L_2 = __this->get_address_of_AdShown_3();
		NendAdVideoShown_t3987677121 * L_3 = V_1;
		NendAdVideoShown_t3987677121 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoShown_t3987677121 * L_6 = V_0;
		NendAdVideoShown_t3987677121 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoShown_t3987677121 *>(L_2, ((NendAdVideoShown_t3987677121 *)CastclassSealed(L_5, NendAdVideoShown_t3987677121_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoShown_t3987677121 * L_8 = V_0;
		NendAdVideoShown_t3987677121 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoShown_t3987677121 *)L_8) == ((Il2CppObject*)(NendAdVideoShown_t3987677121 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::remove_AdShown(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoShown)
extern "C"  void NendAdVideo_remove_AdShown_m4142333737 (NendAdVideo_t1781712043 * __this, NendAdVideoShown_t3987677121 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_remove_AdShown_m4142333737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoShown_t3987677121 * V_0 = NULL;
	NendAdVideoShown_t3987677121 * V_1 = NULL;
	{
		NendAdVideoShown_t3987677121 * L_0 = __this->get_AdShown_3();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoShown_t3987677121 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoShown_t3987677121 ** L_2 = __this->get_address_of_AdShown_3();
		NendAdVideoShown_t3987677121 * L_3 = V_1;
		NendAdVideoShown_t3987677121 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoShown_t3987677121 * L_6 = V_0;
		NendAdVideoShown_t3987677121 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoShown_t3987677121 *>(L_2, ((NendAdVideoShown_t3987677121 *)CastclassSealed(L_5, NendAdVideoShown_t3987677121_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoShown_t3987677121 * L_8 = V_0;
		NendAdVideoShown_t3987677121 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoShown_t3987677121 *)L_8) == ((Il2CppObject*)(NendAdVideoShown_t3987677121 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::add_AdStarted(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick)
extern "C"  void NendAdVideo_add_AdStarted_m459056747 (NendAdVideo_t1781712043 * __this, NendAdVideoClick_t895736144 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_add_AdStarted_m459056747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClick_t895736144 * V_0 = NULL;
	NendAdVideoClick_t895736144 * V_1 = NULL;
	{
		NendAdVideoClick_t895736144 * L_0 = __this->get_AdStarted_4();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClick_t895736144 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClick_t895736144 ** L_2 = __this->get_address_of_AdStarted_4();
		NendAdVideoClick_t895736144 * L_3 = V_1;
		NendAdVideoClick_t895736144 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClick_t895736144 * L_6 = V_0;
		NendAdVideoClick_t895736144 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClick_t895736144 *>(L_2, ((NendAdVideoClick_t895736144 *)CastclassSealed(L_5, NendAdVideoClick_t895736144_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClick_t895736144 * L_8 = V_0;
		NendAdVideoClick_t895736144 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_8) == ((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::remove_AdStarted(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick)
extern "C"  void NendAdVideo_remove_AdStarted_m2921569958 (NendAdVideo_t1781712043 * __this, NendAdVideoClick_t895736144 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_remove_AdStarted_m2921569958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClick_t895736144 * V_0 = NULL;
	NendAdVideoClick_t895736144 * V_1 = NULL;
	{
		NendAdVideoClick_t895736144 * L_0 = __this->get_AdStarted_4();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClick_t895736144 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClick_t895736144 ** L_2 = __this->get_address_of_AdStarted_4();
		NendAdVideoClick_t895736144 * L_3 = V_1;
		NendAdVideoClick_t895736144 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClick_t895736144 * L_6 = V_0;
		NendAdVideoClick_t895736144 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClick_t895736144 *>(L_2, ((NendAdVideoClick_t895736144 *)CastclassSealed(L_5, NendAdVideoClick_t895736144_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClick_t895736144 * L_8 = V_0;
		NendAdVideoClick_t895736144 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_8) == ((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::add_AdStopped(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick)
extern "C"  void NendAdVideo_add_AdStopped_m2421838350 (NendAdVideo_t1781712043 * __this, NendAdVideoClick_t895736144 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_add_AdStopped_m2421838350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClick_t895736144 * V_0 = NULL;
	NendAdVideoClick_t895736144 * V_1 = NULL;
	{
		NendAdVideoClick_t895736144 * L_0 = __this->get_AdStopped_5();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClick_t895736144 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClick_t895736144 ** L_2 = __this->get_address_of_AdStopped_5();
		NendAdVideoClick_t895736144 * L_3 = V_1;
		NendAdVideoClick_t895736144 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClick_t895736144 * L_6 = V_0;
		NendAdVideoClick_t895736144 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClick_t895736144 *>(L_2, ((NendAdVideoClick_t895736144 *)CastclassSealed(L_5, NendAdVideoClick_t895736144_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClick_t895736144 * L_8 = V_0;
		NendAdVideoClick_t895736144 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_8) == ((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::remove_AdStopped(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick)
extern "C"  void NendAdVideo_remove_AdStopped_m245435062 (NendAdVideo_t1781712043 * __this, NendAdVideoClick_t895736144 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_remove_AdStopped_m245435062_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClick_t895736144 * V_0 = NULL;
	NendAdVideoClick_t895736144 * V_1 = NULL;
	{
		NendAdVideoClick_t895736144 * L_0 = __this->get_AdStopped_5();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClick_t895736144 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClick_t895736144 ** L_2 = __this->get_address_of_AdStopped_5();
		NendAdVideoClick_t895736144 * L_3 = V_1;
		NendAdVideoClick_t895736144 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClick_t895736144 * L_6 = V_0;
		NendAdVideoClick_t895736144 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClick_t895736144 *>(L_2, ((NendAdVideoClick_t895736144 *)CastclassSealed(L_5, NendAdVideoClick_t895736144_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClick_t895736144 * L_8 = V_0;
		NendAdVideoClick_t895736144 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_8) == ((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::add_AdCompleted(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick)
extern "C"  void NendAdVideo_add_AdCompleted_m1655903682 (NendAdVideo_t1781712043 * __this, NendAdVideoClick_t895736144 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_add_AdCompleted_m1655903682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClick_t895736144 * V_0 = NULL;
	NendAdVideoClick_t895736144 * V_1 = NULL;
	{
		NendAdVideoClick_t895736144 * L_0 = __this->get_AdCompleted_6();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClick_t895736144 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClick_t895736144 ** L_2 = __this->get_address_of_AdCompleted_6();
		NendAdVideoClick_t895736144 * L_3 = V_1;
		NendAdVideoClick_t895736144 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClick_t895736144 * L_6 = V_0;
		NendAdVideoClick_t895736144 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClick_t895736144 *>(L_2, ((NendAdVideoClick_t895736144 *)CastclassSealed(L_5, NendAdVideoClick_t895736144_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClick_t895736144 * L_8 = V_0;
		NendAdVideoClick_t895736144 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_8) == ((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::remove_AdCompleted(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick)
extern "C"  void NendAdVideo_remove_AdCompleted_m2522856396 (NendAdVideo_t1781712043 * __this, NendAdVideoClick_t895736144 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_remove_AdCompleted_m2522856396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClick_t895736144 * V_0 = NULL;
	NendAdVideoClick_t895736144 * V_1 = NULL;
	{
		NendAdVideoClick_t895736144 * L_0 = __this->get_AdCompleted_6();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClick_t895736144 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClick_t895736144 ** L_2 = __this->get_address_of_AdCompleted_6();
		NendAdVideoClick_t895736144 * L_3 = V_1;
		NendAdVideoClick_t895736144 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClick_t895736144 * L_6 = V_0;
		NendAdVideoClick_t895736144 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClick_t895736144 *>(L_2, ((NendAdVideoClick_t895736144 *)CastclassSealed(L_5, NendAdVideoClick_t895736144_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClick_t895736144 * L_8 = V_0;
		NendAdVideoClick_t895736144 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_8) == ((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::add_AdClicked(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick)
extern "C"  void NendAdVideo_add_AdClicked_m3092326558 (NendAdVideo_t1781712043 * __this, NendAdVideoClick_t895736144 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_add_AdClicked_m3092326558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClick_t895736144 * V_0 = NULL;
	NendAdVideoClick_t895736144 * V_1 = NULL;
	{
		NendAdVideoClick_t895736144 * L_0 = __this->get_AdClicked_7();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClick_t895736144 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClick_t895736144 ** L_2 = __this->get_address_of_AdClicked_7();
		NendAdVideoClick_t895736144 * L_3 = V_1;
		NendAdVideoClick_t895736144 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClick_t895736144 * L_6 = V_0;
		NendAdVideoClick_t895736144 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClick_t895736144 *>(L_2, ((NendAdVideoClick_t895736144 *)CastclassSealed(L_5, NendAdVideoClick_t895736144_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClick_t895736144 * L_8 = V_0;
		NendAdVideoClick_t895736144 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_8) == ((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::remove_AdClicked(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick)
extern "C"  void NendAdVideo_remove_AdClicked_m3269521184 (NendAdVideo_t1781712043 * __this, NendAdVideoClick_t895736144 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_remove_AdClicked_m3269521184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClick_t895736144 * V_0 = NULL;
	NendAdVideoClick_t895736144 * V_1 = NULL;
	{
		NendAdVideoClick_t895736144 * L_0 = __this->get_AdClicked_7();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClick_t895736144 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClick_t895736144 ** L_2 = __this->get_address_of_AdClicked_7();
		NendAdVideoClick_t895736144 * L_3 = V_1;
		NendAdVideoClick_t895736144 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClick_t895736144 * L_6 = V_0;
		NendAdVideoClick_t895736144 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClick_t895736144 *>(L_2, ((NendAdVideoClick_t895736144 *)CastclassSealed(L_5, NendAdVideoClick_t895736144_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClick_t895736144 * L_8 = V_0;
		NendAdVideoClick_t895736144 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_8) == ((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::add_InformationClicked(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick)
extern "C"  void NendAdVideo_add_InformationClicked_m3340766747 (NendAdVideo_t1781712043 * __this, NendAdVideoClick_t895736144 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_add_InformationClicked_m3340766747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClick_t895736144 * V_0 = NULL;
	NendAdVideoClick_t895736144 * V_1 = NULL;
	{
		NendAdVideoClick_t895736144 * L_0 = __this->get_InformationClicked_8();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClick_t895736144 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClick_t895736144 ** L_2 = __this->get_address_of_InformationClicked_8();
		NendAdVideoClick_t895736144 * L_3 = V_1;
		NendAdVideoClick_t895736144 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClick_t895736144 * L_6 = V_0;
		NendAdVideoClick_t895736144 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClick_t895736144 *>(L_2, ((NendAdVideoClick_t895736144 *)CastclassSealed(L_5, NendAdVideoClick_t895736144_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClick_t895736144 * L_8 = V_0;
		NendAdVideoClick_t895736144 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_8) == ((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::remove_InformationClicked(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick)
extern "C"  void NendAdVideo_remove_InformationClicked_m1284134629 (NendAdVideo_t1781712043 * __this, NendAdVideoClick_t895736144 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_remove_InformationClicked_m1284134629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClick_t895736144 * V_0 = NULL;
	NendAdVideoClick_t895736144 * V_1 = NULL;
	{
		NendAdVideoClick_t895736144 * L_0 = __this->get_InformationClicked_8();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClick_t895736144 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClick_t895736144 ** L_2 = __this->get_address_of_InformationClicked_8();
		NendAdVideoClick_t895736144 * L_3 = V_1;
		NendAdVideoClick_t895736144 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClick_t895736144 * L_6 = V_0;
		NendAdVideoClick_t895736144 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClick_t895736144 *>(L_2, ((NendAdVideoClick_t895736144 *)CastclassSealed(L_5, NendAdVideoClick_t895736144_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClick_t895736144 * L_8 = V_0;
		NendAdVideoClick_t895736144 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_8) == ((Il2CppObject*)(NendAdVideoClick_t895736144 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::add_AdClosed(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClosed)
extern "C"  void NendAdVideo_add_AdClosed_m2694012098 (NendAdVideo_t1781712043 * __this, NendAdVideoClosed_t464199100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_add_AdClosed_m2694012098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClosed_t464199100 * V_0 = NULL;
	NendAdVideoClosed_t464199100 * V_1 = NULL;
	{
		NendAdVideoClosed_t464199100 * L_0 = __this->get_AdClosed_9();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClosed_t464199100 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClosed_t464199100 ** L_2 = __this->get_address_of_AdClosed_9();
		NendAdVideoClosed_t464199100 * L_3 = V_1;
		NendAdVideoClosed_t464199100 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClosed_t464199100 * L_6 = V_0;
		NendAdVideoClosed_t464199100 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClosed_t464199100 *>(L_2, ((NendAdVideoClosed_t464199100 *)CastclassSealed(L_5, NendAdVideoClosed_t464199100_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClosed_t464199100 * L_8 = V_0;
		NendAdVideoClosed_t464199100 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClosed_t464199100 *)L_8) == ((Il2CppObject*)(NendAdVideoClosed_t464199100 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::remove_AdClosed(NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClosed)
extern "C"  void NendAdVideo_remove_AdClosed_m3012898169 (NendAdVideo_t1781712043 * __this, NendAdVideoClosed_t464199100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_remove_AdClosed_m3012898169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NendAdVideoClosed_t464199100 * V_0 = NULL;
	NendAdVideoClosed_t464199100 * V_1 = NULL;
	{
		NendAdVideoClosed_t464199100 * L_0 = __this->get_AdClosed_9();
		V_0 = L_0;
	}

IL_0007:
	{
		NendAdVideoClosed_t464199100 * L_1 = V_0;
		V_1 = L_1;
		NendAdVideoClosed_t464199100 ** L_2 = __this->get_address_of_AdClosed_9();
		NendAdVideoClosed_t464199100 * L_3 = V_1;
		NendAdVideoClosed_t464199100 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NendAdVideoClosed_t464199100 * L_6 = V_0;
		NendAdVideoClosed_t464199100 * L_7 = InterlockedCompareExchangeImpl<NendAdVideoClosed_t464199100 *>(L_2, ((NendAdVideoClosed_t464199100 *)CastclassSealed(L_5, NendAdVideoClosed_t464199100_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		NendAdVideoClosed_t464199100 * L_8 = V_0;
		NendAdVideoClosed_t464199100 * L_9 = V_1;
		if ((!(((Il2CppObject*)(NendAdVideoClosed_t464199100 *)L_8) == ((Il2CppObject*)(NendAdVideoClosed_t464199100 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::CallBack(NendUnityPlugin.AD.Video.VideoAdCallbackArgments)
extern "C"  void NendAdVideo_CallBack_m1751514092 (NendAdVideo_t1781712043 * __this, VideoAdCallbackArgments_t2567701888 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideo_CallBack_m1751514092_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ErrorVideoAdCallbackArgments_t3139399691 * V_1 = NULL;
	{
		VideoAdCallbackArgments_t2567701888 * L_0 = ___args0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_videoAdCallbackType_0();
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_003a;
			}
			case 1:
			{
				goto IL_0056;
			}
			case 2:
			{
				goto IL_009f;
			}
			case 3:
			{
				goto IL_00bb;
			}
			case 4:
			{
				goto IL_00d7;
			}
			case 5:
			{
				goto IL_00f3;
			}
			case 6:
			{
				goto IL_010f;
			}
			case 7:
			{
				goto IL_012b;
			}
			case 8:
			{
				goto IL_0147;
			}
			case 9:
			{
				goto IL_0163;
			}
		}
	}
	{
		goto IL_017f;
	}

IL_003a:
	{
		NendAdVideoLoaded_t1357133087 * L_3 = __this->get_AdLoaded_0();
		if (!L_3)
		{
			goto IL_0051;
		}
	}
	{
		NendAdVideoLoaded_t1357133087 * L_4 = __this->get_AdLoaded_0();
		NullCheck(L_4);
		NendAdVideoLoaded_Invoke_m2447413929(L_4, __this, /*hidden argument*/NULL);
	}

IL_0051:
	{
		goto IL_017f;
	}

IL_0056:
	{
		NendAdVideoFailedToLoad_t1837550073 * L_5 = __this->get_AdFailedToLoad_1();
		if (!L_5)
		{
			goto IL_009a;
		}
	}
	{
		VideoAdCallbackArgments_t2567701888 * L_6 = ___args0;
		V_1 = ((ErrorVideoAdCallbackArgments_t3139399691 *)CastclassClass(L_6, ErrorVideoAdCallbackArgments_t3139399691_il2cpp_TypeInfo_var));
		ErrorVideoAdCallbackArgments_t3139399691 * L_7 = V_1;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_errorCode_1();
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral207068603, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NendAdLogger_t117402011_il2cpp_TypeInfo_var);
		NendAdLogger_E_m3544237484(NULL /*static, unused*/, L_11, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		NendAdVideoFailedToLoad_t1837550073 * L_12 = __this->get_AdFailedToLoad_1();
		ErrorVideoAdCallbackArgments_t3139399691 * L_13 = V_1;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_errorCode_1();
		NullCheck(L_12);
		NendAdVideoFailedToLoad_Invoke_m3265524165(L_12, __this, L_14, /*hidden argument*/NULL);
	}

IL_009a:
	{
		goto IL_017f;
	}

IL_009f:
	{
		NendAdVideoFailedToPlay_t4265396701 * L_15 = __this->get_AdFailedToPlay_2();
		if (!L_15)
		{
			goto IL_00b6;
		}
	}
	{
		NendAdVideoFailedToPlay_t4265396701 * L_16 = __this->get_AdFailedToPlay_2();
		NullCheck(L_16);
		NendAdVideoFailedToPlay_Invoke_m4000227672(L_16, __this, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		goto IL_017f;
	}

IL_00bb:
	{
		NendAdVideoShown_t3987677121 * L_17 = __this->get_AdShown_3();
		if (!L_17)
		{
			goto IL_00d2;
		}
	}
	{
		NendAdVideoShown_t3987677121 * L_18 = __this->get_AdShown_3();
		NullCheck(L_18);
		NendAdVideoShown_Invoke_m4289821957(L_18, __this, /*hidden argument*/NULL);
	}

IL_00d2:
	{
		goto IL_017f;
	}

IL_00d7:
	{
		NendAdVideoClosed_t464199100 * L_19 = __this->get_AdClosed_9();
		if (!L_19)
		{
			goto IL_00ee;
		}
	}
	{
		NendAdVideoClosed_t464199100 * L_20 = __this->get_AdClosed_9();
		NullCheck(L_20);
		NendAdVideoClosed_Invoke_m2544588046(L_20, __this, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		goto IL_017f;
	}

IL_00f3:
	{
		NendAdVideoClick_t895736144 * L_21 = __this->get_AdStarted_4();
		if (!L_21)
		{
			goto IL_010a;
		}
	}
	{
		NendAdVideoClick_t895736144 * L_22 = __this->get_AdStarted_4();
		NullCheck(L_22);
		NendAdVideoClick_Invoke_m359502320(L_22, __this, /*hidden argument*/NULL);
	}

IL_010a:
	{
		goto IL_017f;
	}

IL_010f:
	{
		NendAdVideoClick_t895736144 * L_23 = __this->get_AdStopped_5();
		if (!L_23)
		{
			goto IL_0126;
		}
	}
	{
		NendAdVideoClick_t895736144 * L_24 = __this->get_AdStopped_5();
		NullCheck(L_24);
		NendAdVideoClick_Invoke_m359502320(L_24, __this, /*hidden argument*/NULL);
	}

IL_0126:
	{
		goto IL_017f;
	}

IL_012b:
	{
		NendAdVideoClick_t895736144 * L_25 = __this->get_AdCompleted_6();
		if (!L_25)
		{
			goto IL_0142;
		}
	}
	{
		NendAdVideoClick_t895736144 * L_26 = __this->get_AdCompleted_6();
		NullCheck(L_26);
		NendAdVideoClick_Invoke_m359502320(L_26, __this, /*hidden argument*/NULL);
	}

IL_0142:
	{
		goto IL_017f;
	}

IL_0147:
	{
		NendAdVideoClick_t895736144 * L_27 = __this->get_AdClicked_7();
		if (!L_27)
		{
			goto IL_015e;
		}
	}
	{
		NendAdVideoClick_t895736144 * L_28 = __this->get_AdClicked_7();
		NullCheck(L_28);
		NendAdVideoClick_Invoke_m359502320(L_28, __this, /*hidden argument*/NULL);
	}

IL_015e:
	{
		goto IL_017f;
	}

IL_0163:
	{
		NendAdVideoClick_t895736144 * L_29 = __this->get_InformationClicked_8();
		if (!L_29)
		{
			goto IL_017a;
		}
	}
	{
		NendAdVideoClick_t895736144 * L_30 = __this->get_InformationClicked_8();
		NullCheck(L_30);
		NendAdVideoClick_Invoke_m359502320(L_30, __this, /*hidden argument*/NULL);
	}

IL_017a:
	{
		goto IL_017f;
	}

IL_017f:
	{
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::Load()
extern "C"  void NendAdVideo_Load_m1965670150 (NendAdVideo_t1781712043 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(6 /* System.Void NendUnityPlugin.AD.Video.NendAdVideo::LoadInternal() */, __this);
		return;
	}
}
// System.Boolean NendUnityPlugin.AD.Video.NendAdVideo::IsLoaded()
extern "C"  bool NendAdVideo_IsLoaded_m560562838 (NendAdVideo_t1781712043 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean NendUnityPlugin.AD.Video.NendAdVideo::IsLoadedInternal() */, __this);
		return L_0;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::Show()
extern "C"  void NendAdVideo_Show_m3680192343 (NendAdVideo_t1781712043 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(8 /* System.Void NendUnityPlugin.AD.Video.NendAdVideo::ShowInternal() */, __this);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::Release()
extern "C"  void NendAdVideo_Release_m3271842135 (NendAdVideo_t1781712043 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(9 /* System.Void NendUnityPlugin.AD.Video.NendAdVideo::ReleaseInternal() */, __this);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::set_MediationName(System.String)
extern "C"  void NendAdVideo_set_MediationName_m1906756176 (NendAdVideo_t1781712043 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void NendUnityPlugin.AD.Video.NendAdVideo::SetMediationNameInternal(System.String) */, __this, L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::set_UserId(System.String)
extern "C"  void NendAdVideo_set_UserId_m3732467957 (NendAdVideo_t1781712043 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void NendUnityPlugin.AD.Video.NendAdVideo::SetUserIdInternal(System.String) */, __this, L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::set_UserFeature(NendUnityPlugin.AD.Video.NendAdUserFeature)
extern "C"  void NendAdVideo_set_UserFeature_m2642842310 (NendAdVideo_t1781712043 * __this, NendAdUserFeature_t4152365987 * ___value0, const MethodInfo* method)
{
	{
		NendAdUserFeature_t4152365987 * L_0 = ___value0;
		VirtActionInvoker1< NendAdUserFeature_t4152365987 * >::Invoke(12 /* System.Void NendUnityPlugin.AD.Video.NendAdVideo::SetUserFeatureInternal(NendUnityPlugin.AD.Video.NendAdUserFeature) */, __this, L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::set_IsLocationEnabled(System.Boolean)
extern "C"  void NendAdVideo_set_IsLocationEnabled_m2567720394 (NendAdVideo_t1781712043 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		VirtActionInvoker1< bool >::Invoke(13 /* System.Void NendUnityPlugin.AD.Video.NendAdVideo::SetLocationEnabledInternal(System.Boolean) */, __this, L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo::set_IsOutputLog(System.Boolean)
extern "C"  void NendAdVideo_set_IsOutputLog_m1002727128 (NendAdVideo_t1781712043 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		VirtActionInvoker1< bool >::Invoke(14 /* System.Void NendUnityPlugin.AD.Video.NendAdVideo::SetOutputLog(System.Boolean) */, __this, L_0);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdVideoClick__ctor_m3342294915 (NendAdVideoClick_t895736144 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoClick_Invoke_m359502320 (NendAdVideoClick_t895736144 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdVideoClick_Invoke_m359502320((NendAdVideoClick_t895736144 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick::BeginInvoke(NendUnityPlugin.AD.Video.NendAdVideo,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdVideoClick_BeginInvoke_m1658886049 (NendAdVideoClick_t895736144 * __this, NendAdVideo_t1781712043 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdVideoClick_EndInvoke_m1072832313 (NendAdVideoClick_t895736144 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClosed::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdVideoClosed__ctor_m1399077679 (NendAdVideoClosed_t464199100 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClosed::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoClosed_Invoke_m2544588046 (NendAdVideoClosed_t464199100 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdVideoClosed_Invoke_m2544588046((NendAdVideoClosed_t464199100 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClosed::BeginInvoke(NendUnityPlugin.AD.Video.NendAdVideo,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdVideoClosed_BeginInvoke_m2275321964 (NendAdVideoClosed_t464199100 * __this, NendAdVideo_t1781712043 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClosed::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdVideoClosed_EndInvoke_m1988182832 (NendAdVideoClosed_t464199100 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoCompleted::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdVideoCompleted__ctor_m1586513543 (NendAdVideoCompleted_t3646835482 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoCompleted::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoCompleted_Invoke_m4139560455 (NendAdVideoCompleted_t3646835482 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdVideoCompleted_Invoke_m4139560455((NendAdVideoCompleted_t3646835482 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoCompleted::BeginInvoke(NendUnityPlugin.AD.Video.NendAdVideo,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdVideoCompleted_BeginInvoke_m410603740 (NendAdVideoCompleted_t3646835482 * __this, NendAdVideo_t1781712043 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoCompleted::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdVideoCompleted_EndInvoke_m503468519 (NendAdVideoCompleted_t3646835482 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToLoad::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdVideoFailedToLoad__ctor_m1569270846 (NendAdVideoFailedToLoad_t1837550073 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToLoad::Invoke(NendUnityPlugin.AD.Video.NendAdVideo,System.Int32)
extern "C"  void NendAdVideoFailedToLoad_Invoke_m3265524165 (NendAdVideoFailedToLoad_t1837550073 * __this, NendAdVideo_t1781712043 * ___instance0, int32_t ___errorCode1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdVideoFailedToLoad_Invoke_m3265524165((NendAdVideoFailedToLoad_t1837550073 *)__this->get_prev_9(),___instance0, ___errorCode1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdVideo_t1781712043 * ___instance0, int32_t ___errorCode1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0, ___errorCode1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdVideo_t1781712043 * ___instance0, int32_t ___errorCode1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0, ___errorCode1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___errorCode1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0, ___errorCode1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToLoad::BeginInvoke(NendUnityPlugin.AD.Video.NendAdVideo,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdVideoFailedToLoad_BeginInvoke_m56923607 (NendAdVideoFailedToLoad_t1837550073 * __this, NendAdVideo_t1781712043 * ___instance0, int32_t ___errorCode1, AsyncCallback_t3962456242 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NendAdVideoFailedToLoad_BeginInvoke_m56923607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___instance0;
	__d_args[1] = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &___errorCode1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToLoad::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdVideoFailedToLoad_EndInvoke_m3165059251 (NendAdVideoFailedToLoad_t1837550073 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToPlay::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdVideoFailedToPlay__ctor_m1520611151 (NendAdVideoFailedToPlay_t4265396701 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToPlay::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoFailedToPlay_Invoke_m4000227672 (NendAdVideoFailedToPlay_t4265396701 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdVideoFailedToPlay_Invoke_m4000227672((NendAdVideoFailedToPlay_t4265396701 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToPlay::BeginInvoke(NendUnityPlugin.AD.Video.NendAdVideo,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdVideoFailedToPlay_BeginInvoke_m2415273331 (NendAdVideoFailedToPlay_t4265396701 * __this, NendAdVideo_t1781712043 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToPlay::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdVideoFailedToPlay_EndInvoke_m3788578983 (NendAdVideoFailedToPlay_t4265396701 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoInformationClick::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdVideoInformationClick__ctor_m493783264 (NendAdVideoInformationClick_t172780505 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoInformationClick::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoInformationClick_Invoke_m2704544080 (NendAdVideoInformationClick_t172780505 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdVideoInformationClick_Invoke_m2704544080((NendAdVideoInformationClick_t172780505 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoInformationClick::BeginInvoke(NendUnityPlugin.AD.Video.NendAdVideo,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdVideoInformationClick_BeginInvoke_m1553040423 (NendAdVideoInformationClick_t172780505 * __this, NendAdVideo_t1781712043 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoInformationClick::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdVideoInformationClick_EndInvoke_m3889008221 (NendAdVideoInformationClick_t172780505 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoLoaded::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdVideoLoaded__ctor_m2053049693 (NendAdVideoLoaded_t1357133087 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoLoaded::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoLoaded_Invoke_m2447413929 (NendAdVideoLoaded_t1357133087 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdVideoLoaded_Invoke_m2447413929((NendAdVideoLoaded_t1357133087 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoLoaded::BeginInvoke(NendUnityPlugin.AD.Video.NendAdVideo,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdVideoLoaded_BeginInvoke_m3070610253 (NendAdVideoLoaded_t1357133087 * __this, NendAdVideo_t1781712043 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoLoaded::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdVideoLoaded_EndInvoke_m497703673 (NendAdVideoLoaded_t1357133087 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoShown::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdVideoShown__ctor_m1486727245 (NendAdVideoShown_t3987677121 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoShown::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoShown_Invoke_m4289821957 (NendAdVideoShown_t3987677121 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdVideoShown_Invoke_m4289821957((NendAdVideoShown_t3987677121 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoShown::BeginInvoke(NendUnityPlugin.AD.Video.NendAdVideo,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdVideoShown_BeginInvoke_m3559295619 (NendAdVideoShown_t3987677121 * __this, NendAdVideo_t1781712043 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoShown::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdVideoShown_EndInvoke_m900690952 (NendAdVideoShown_t3987677121 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStarted::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdVideoStarted__ctor_m2273351545 (NendAdVideoStarted_t2144365953 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStarted::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoStarted_Invoke_m4278189045 (NendAdVideoStarted_t2144365953 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdVideoStarted_Invoke_m4278189045((NendAdVideoStarted_t2144365953 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStarted::BeginInvoke(NendUnityPlugin.AD.Video.NendAdVideo,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdVideoStarted_BeginInvoke_m3546133477 (NendAdVideoStarted_t2144365953 * __this, NendAdVideo_t1781712043 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStarted::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdVideoStarted_EndInvoke_m365012604 (NendAdVideoStarted_t2144365953 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStopped::.ctor(System.Object,System.IntPtr)
extern "C"  void NendAdVideoStopped__ctor_m3390199973 (NendAdVideoStopped_t3890769867 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStopped::Invoke(NendUnityPlugin.AD.Video.NendAdVideo)
extern "C"  void NendAdVideoStopped_Invoke_m1177888485 (NendAdVideoStopped_t3890769867 * __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		NendAdVideoStopped_Invoke_m1177888485((NendAdVideoStopped_t3890769867 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, NendAdVideo_t1781712043 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStopped::BeginInvoke(NendUnityPlugin.AD.Video.NendAdVideo,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NendAdVideoStopped_BeginInvoke_m3571086520 (NendAdVideoStopped_t3890769867 * __this, NendAdVideo_t1781712043 * ___instance0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoStopped::EndInvoke(System.IAsyncResult)
extern "C"  void NendAdVideoStopped_EndInvoke_m1662490139 (NendAdVideoStopped_t3890769867 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void NendUnityPlugin.AD.Video.RewardedVideoAdCallbackArgments::.ctor(NendUnityPlugin.AD.Video.NendAdVideo/VideoAdCallbackType,NendUnityPlugin.AD.Video.NendAdRewardedItem)
extern "C"  void RewardedVideoAdCallbackArgments__ctor_m2520781640 (RewardedVideoAdCallbackArgments_t3338436455 * __this, int32_t ___type0, NendAdRewardedItem_t3964314288 * ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___type0;
		VideoAdCallbackArgments__ctor_m1675151393(__this, L_0, /*hidden argument*/NULL);
		NendAdRewardedItem_t3964314288 * L_1 = ___item1;
		__this->set_rewardedItem_1(L_1);
		return;
	}
}
// System.Void NendUnityPlugin.AD.Video.VideoAdCallbackArgments::.ctor(NendUnityPlugin.AD.Video.NendAdVideo/VideoAdCallbackType)
extern "C"  void VideoAdCallbackArgments__ctor_m1675151393 (VideoAdCallbackArgments_t2567701888 * __this, int32_t ___type0, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type0;
		__this->set_videoAdCallbackType_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
