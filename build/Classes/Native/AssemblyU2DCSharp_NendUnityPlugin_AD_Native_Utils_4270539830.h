﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.UI.RawImage
struct RawImage_t3182918964;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.Utils.UIRenderer/<TryRenderImage>c__AnonStorey0
struct  U3CTryRenderImageU3Ec__AnonStorey0_t4270539830  : public Il2CppObject
{
public:
	// UnityEngine.UI.RawImage NendUnityPlugin.AD.Native.Utils.UIRenderer/<TryRenderImage>c__AnonStorey0::target
	RawImage_t3182918964 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CTryRenderImageU3Ec__AnonStorey0_t4270539830, ___target_0)); }
	inline RawImage_t3182918964 * get_target_0() const { return ___target_0; }
	inline RawImage_t3182918964 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RawImage_t3182918964 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier(&___target_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
