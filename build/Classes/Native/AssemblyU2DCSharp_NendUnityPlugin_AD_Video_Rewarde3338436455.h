﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_VideoAd2567701888.h"

// NendUnityPlugin.AD.Video.NendAdRewardedItem
struct NendAdRewardedItem_t3964314288;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Video.RewardedVideoAdCallbackArgments
struct  RewardedVideoAdCallbackArgments_t3338436455  : public VideoAdCallbackArgments_t2567701888
{
public:
	// NendUnityPlugin.AD.Video.NendAdRewardedItem NendUnityPlugin.AD.Video.RewardedVideoAdCallbackArgments::rewardedItem
	NendAdRewardedItem_t3964314288 * ___rewardedItem_1;

public:
	inline static int32_t get_offset_of_rewardedItem_1() { return static_cast<int32_t>(offsetof(RewardedVideoAdCallbackArgments_t3338436455, ___rewardedItem_1)); }
	inline NendAdRewardedItem_t3964314288 * get_rewardedItem_1() const { return ___rewardedItem_1; }
	inline NendAdRewardedItem_t3964314288 ** get_address_of_rewardedItem_1() { return &___rewardedItem_1; }
	inline void set_rewardedItem_1(NendAdRewardedItem_t3964314288 * value)
	{
		___rewardedItem_1 = value;
		Il2CppCodeGenWriteBarrier(&___rewardedItem_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
