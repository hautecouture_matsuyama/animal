﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MovingBg
struct  MovingBg_t2796125617  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MovingBg::tilePrefab
	GameObject_t1113636619 * ___tilePrefab_2;
	// System.Single MovingBg::bottomY
	float ___bottomY_3;
	// System.Single MovingBg::leftX
	float ___leftX_4;
	// System.Single MovingBg::topY
	float ___topY_5;
	// System.Single MovingBg::rightX
	float ___rightX_6;
	// System.Single MovingBg::groundTileW
	float ___groundTileW_7;
	// System.Single MovingBg::groundTileH
	float ___groundTileH_8;
	// UnityEngine.GameObject MovingBg::block1
	GameObject_t1113636619 * ___block1_9;
	// UnityEngine.GameObject MovingBg::block2
	GameObject_t1113636619 * ___block2_10;
	// UnityEngine.GameObject MovingBg::block3
	GameObject_t1113636619 * ___block3_11;
	// UnityEngine.GameObject MovingBg::block4
	GameObject_t1113636619 * ___block4_12;
	// UnityEngine.GameObject MovingBg::block5
	GameObject_t1113636619 * ___block5_13;

public:
	inline static int32_t get_offset_of_tilePrefab_2() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___tilePrefab_2)); }
	inline GameObject_t1113636619 * get_tilePrefab_2() const { return ___tilePrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_tilePrefab_2() { return &___tilePrefab_2; }
	inline void set_tilePrefab_2(GameObject_t1113636619 * value)
	{
		___tilePrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___tilePrefab_2, value);
	}

	inline static int32_t get_offset_of_bottomY_3() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___bottomY_3)); }
	inline float get_bottomY_3() const { return ___bottomY_3; }
	inline float* get_address_of_bottomY_3() { return &___bottomY_3; }
	inline void set_bottomY_3(float value)
	{
		___bottomY_3 = value;
	}

	inline static int32_t get_offset_of_leftX_4() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___leftX_4)); }
	inline float get_leftX_4() const { return ___leftX_4; }
	inline float* get_address_of_leftX_4() { return &___leftX_4; }
	inline void set_leftX_4(float value)
	{
		___leftX_4 = value;
	}

	inline static int32_t get_offset_of_topY_5() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___topY_5)); }
	inline float get_topY_5() const { return ___topY_5; }
	inline float* get_address_of_topY_5() { return &___topY_5; }
	inline void set_topY_5(float value)
	{
		___topY_5 = value;
	}

	inline static int32_t get_offset_of_rightX_6() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___rightX_6)); }
	inline float get_rightX_6() const { return ___rightX_6; }
	inline float* get_address_of_rightX_6() { return &___rightX_6; }
	inline void set_rightX_6(float value)
	{
		___rightX_6 = value;
	}

	inline static int32_t get_offset_of_groundTileW_7() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___groundTileW_7)); }
	inline float get_groundTileW_7() const { return ___groundTileW_7; }
	inline float* get_address_of_groundTileW_7() { return &___groundTileW_7; }
	inline void set_groundTileW_7(float value)
	{
		___groundTileW_7 = value;
	}

	inline static int32_t get_offset_of_groundTileH_8() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___groundTileH_8)); }
	inline float get_groundTileH_8() const { return ___groundTileH_8; }
	inline float* get_address_of_groundTileH_8() { return &___groundTileH_8; }
	inline void set_groundTileH_8(float value)
	{
		___groundTileH_8 = value;
	}

	inline static int32_t get_offset_of_block1_9() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___block1_9)); }
	inline GameObject_t1113636619 * get_block1_9() const { return ___block1_9; }
	inline GameObject_t1113636619 ** get_address_of_block1_9() { return &___block1_9; }
	inline void set_block1_9(GameObject_t1113636619 * value)
	{
		___block1_9 = value;
		Il2CppCodeGenWriteBarrier(&___block1_9, value);
	}

	inline static int32_t get_offset_of_block2_10() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___block2_10)); }
	inline GameObject_t1113636619 * get_block2_10() const { return ___block2_10; }
	inline GameObject_t1113636619 ** get_address_of_block2_10() { return &___block2_10; }
	inline void set_block2_10(GameObject_t1113636619 * value)
	{
		___block2_10 = value;
		Il2CppCodeGenWriteBarrier(&___block2_10, value);
	}

	inline static int32_t get_offset_of_block3_11() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___block3_11)); }
	inline GameObject_t1113636619 * get_block3_11() const { return ___block3_11; }
	inline GameObject_t1113636619 ** get_address_of_block3_11() { return &___block3_11; }
	inline void set_block3_11(GameObject_t1113636619 * value)
	{
		___block3_11 = value;
		Il2CppCodeGenWriteBarrier(&___block3_11, value);
	}

	inline static int32_t get_offset_of_block4_12() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___block4_12)); }
	inline GameObject_t1113636619 * get_block4_12() const { return ___block4_12; }
	inline GameObject_t1113636619 ** get_address_of_block4_12() { return &___block4_12; }
	inline void set_block4_12(GameObject_t1113636619 * value)
	{
		___block4_12 = value;
		Il2CppCodeGenWriteBarrier(&___block4_12, value);
	}

	inline static int32_t get_offset_of_block5_13() { return static_cast<int32_t>(offsetof(MovingBg_t2796125617, ___block5_13)); }
	inline GameObject_t1113636619 * get_block5_13() const { return ___block5_13; }
	inline GameObject_t1113636619 ** get_address_of_block5_13() { return &___block5_13; }
	inline void set_block5_13(GameObject_t1113636619 * value)
	{
		___block5_13 = value;
		Il2CppCodeGenWriteBarrier(&___block5_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
