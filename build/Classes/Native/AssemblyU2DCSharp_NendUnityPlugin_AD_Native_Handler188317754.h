﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handle4278309981.h"

// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI
struct  ImpressionHandlerUI_t188317754  : public ImpressionHandler_t4278309981
{
public:
	// UnityEngine.Camera NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI::m_Camera
	Camera_t4157153871 * ___m_Camera_6;
	// UnityEngine.Canvas NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_7;
	// UnityEngine.Vector3[] NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_8;

public:
	inline static int32_t get_offset_of_m_Camera_6() { return static_cast<int32_t>(offsetof(ImpressionHandlerUI_t188317754, ___m_Camera_6)); }
	inline Camera_t4157153871 * get_m_Camera_6() const { return ___m_Camera_6; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_6() { return &___m_Camera_6; }
	inline void set_m_Camera_6(Camera_t4157153871 * value)
	{
		___m_Camera_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_Camera_6, value);
	}

	inline static int32_t get_offset_of_m_Canvas_7() { return static_cast<int32_t>(offsetof(ImpressionHandlerUI_t188317754, ___m_Canvas_7)); }
	inline Canvas_t3310196443 * get_m_Canvas_7() const { return ___m_Canvas_7; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_7() { return &___m_Canvas_7; }
	inline void set_m_Canvas_7(Canvas_t3310196443 * value)
	{
		___m_Canvas_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_Canvas_7, value);
	}

	inline static int32_t get_offset_of_m_Corners_8() { return static_cast<int32_t>(offsetof(ImpressionHandlerUI_t188317754, ___m_Corners_8)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_8() const { return ___m_Corners_8; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_8() { return &___m_Corners_8; }
	inline void set_m_Corners_8(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_Corners_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
