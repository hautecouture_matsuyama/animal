﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdU4152365987.h"
#include "mscorlib_System_IntPtr840150181.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Platform.iOS.IOSUserFeature
struct  IOSUserFeature_t2237018260  : public NendAdUserFeature_t4152365987
{
public:
	// System.IntPtr NendUnityPlugin.Platform.iOS.IOSUserFeature::m_iOSUserFeaturePtr
	IntPtr_t ___m_iOSUserFeaturePtr_10;

public:
	inline static int32_t get_offset_of_m_iOSUserFeaturePtr_10() { return static_cast<int32_t>(offsetof(IOSUserFeature_t2237018260, ___m_iOSUserFeaturePtr_10)); }
	inline IntPtr_t get_m_iOSUserFeaturePtr_10() const { return ___m_iOSUserFeaturePtr_10; }
	inline IntPtr_t* get_address_of_m_iOSUserFeaturePtr_10() { return &___m_iOSUserFeaturePtr_10; }
	inline void set_m_iOSUserFeaturePtr_10(IntPtr_t value)
	{
		___m_iOSUserFeaturePtr_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
