﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<HCAnimatedBanner/AnimBannerData>
struct List_1_t926430637;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HCAnimatedBanner
struct  HCAnimatedBanner_t2921968851  : public MonoBehaviour_t3962482529
{
public:
	// System.String HCAnimatedBanner::jsonLink
	String_t* ___jsonLink_2;
	// System.Collections.Generic.List`1<HCAnimatedBanner/AnimBannerData> HCAnimatedBanner::bannerDataList
	List_1_t926430637 * ___bannerDataList_3;
	// System.String HCAnimatedBanner::textureIdName
	String_t* ___textureIdName_6;
	// System.String HCAnimatedBanner::textureUrl
	String_t* ___textureUrl_7;
	// System.String HCAnimatedBanner::_transitionLink
	String_t* ____transitionLink_8;
	// System.String HCAnimatedBanner::defaultTLinkAndroid
	String_t* ___defaultTLinkAndroid_9;
	// System.String HCAnimatedBanner::defaultTLinkIOS
	String_t* ___defaultTLinkIOS_10;
	// System.String HCAnimatedBanner::texturePath
	String_t* ___texturePath_11;
	// System.Double HCAnimatedBanner::count
	double ___count_12;
	// System.Collections.IEnumerator HCAnimatedBanner::jCoroutine
	Il2CppObject * ___jCoroutine_13;
	// System.Collections.IEnumerator HCAnimatedBanner::lCoroutine
	Il2CppObject * ___lCoroutine_14;

public:
	inline static int32_t get_offset_of_jsonLink_2() { return static_cast<int32_t>(offsetof(HCAnimatedBanner_t2921968851, ___jsonLink_2)); }
	inline String_t* get_jsonLink_2() const { return ___jsonLink_2; }
	inline String_t** get_address_of_jsonLink_2() { return &___jsonLink_2; }
	inline void set_jsonLink_2(String_t* value)
	{
		___jsonLink_2 = value;
		Il2CppCodeGenWriteBarrier(&___jsonLink_2, value);
	}

	inline static int32_t get_offset_of_bannerDataList_3() { return static_cast<int32_t>(offsetof(HCAnimatedBanner_t2921968851, ___bannerDataList_3)); }
	inline List_1_t926430637 * get_bannerDataList_3() const { return ___bannerDataList_3; }
	inline List_1_t926430637 ** get_address_of_bannerDataList_3() { return &___bannerDataList_3; }
	inline void set_bannerDataList_3(List_1_t926430637 * value)
	{
		___bannerDataList_3 = value;
		Il2CppCodeGenWriteBarrier(&___bannerDataList_3, value);
	}

	inline static int32_t get_offset_of_textureIdName_6() { return static_cast<int32_t>(offsetof(HCAnimatedBanner_t2921968851, ___textureIdName_6)); }
	inline String_t* get_textureIdName_6() const { return ___textureIdName_6; }
	inline String_t** get_address_of_textureIdName_6() { return &___textureIdName_6; }
	inline void set_textureIdName_6(String_t* value)
	{
		___textureIdName_6 = value;
		Il2CppCodeGenWriteBarrier(&___textureIdName_6, value);
	}

	inline static int32_t get_offset_of_textureUrl_7() { return static_cast<int32_t>(offsetof(HCAnimatedBanner_t2921968851, ___textureUrl_7)); }
	inline String_t* get_textureUrl_7() const { return ___textureUrl_7; }
	inline String_t** get_address_of_textureUrl_7() { return &___textureUrl_7; }
	inline void set_textureUrl_7(String_t* value)
	{
		___textureUrl_7 = value;
		Il2CppCodeGenWriteBarrier(&___textureUrl_7, value);
	}

	inline static int32_t get_offset_of__transitionLink_8() { return static_cast<int32_t>(offsetof(HCAnimatedBanner_t2921968851, ____transitionLink_8)); }
	inline String_t* get__transitionLink_8() const { return ____transitionLink_8; }
	inline String_t** get_address_of__transitionLink_8() { return &____transitionLink_8; }
	inline void set__transitionLink_8(String_t* value)
	{
		____transitionLink_8 = value;
		Il2CppCodeGenWriteBarrier(&____transitionLink_8, value);
	}

	inline static int32_t get_offset_of_defaultTLinkAndroid_9() { return static_cast<int32_t>(offsetof(HCAnimatedBanner_t2921968851, ___defaultTLinkAndroid_9)); }
	inline String_t* get_defaultTLinkAndroid_9() const { return ___defaultTLinkAndroid_9; }
	inline String_t** get_address_of_defaultTLinkAndroid_9() { return &___defaultTLinkAndroid_9; }
	inline void set_defaultTLinkAndroid_9(String_t* value)
	{
		___defaultTLinkAndroid_9 = value;
		Il2CppCodeGenWriteBarrier(&___defaultTLinkAndroid_9, value);
	}

	inline static int32_t get_offset_of_defaultTLinkIOS_10() { return static_cast<int32_t>(offsetof(HCAnimatedBanner_t2921968851, ___defaultTLinkIOS_10)); }
	inline String_t* get_defaultTLinkIOS_10() const { return ___defaultTLinkIOS_10; }
	inline String_t** get_address_of_defaultTLinkIOS_10() { return &___defaultTLinkIOS_10; }
	inline void set_defaultTLinkIOS_10(String_t* value)
	{
		___defaultTLinkIOS_10 = value;
		Il2CppCodeGenWriteBarrier(&___defaultTLinkIOS_10, value);
	}

	inline static int32_t get_offset_of_texturePath_11() { return static_cast<int32_t>(offsetof(HCAnimatedBanner_t2921968851, ___texturePath_11)); }
	inline String_t* get_texturePath_11() const { return ___texturePath_11; }
	inline String_t** get_address_of_texturePath_11() { return &___texturePath_11; }
	inline void set_texturePath_11(String_t* value)
	{
		___texturePath_11 = value;
		Il2CppCodeGenWriteBarrier(&___texturePath_11, value);
	}

	inline static int32_t get_offset_of_count_12() { return static_cast<int32_t>(offsetof(HCAnimatedBanner_t2921968851, ___count_12)); }
	inline double get_count_12() const { return ___count_12; }
	inline double* get_address_of_count_12() { return &___count_12; }
	inline void set_count_12(double value)
	{
		___count_12 = value;
	}

	inline static int32_t get_offset_of_jCoroutine_13() { return static_cast<int32_t>(offsetof(HCAnimatedBanner_t2921968851, ___jCoroutine_13)); }
	inline Il2CppObject * get_jCoroutine_13() const { return ___jCoroutine_13; }
	inline Il2CppObject ** get_address_of_jCoroutine_13() { return &___jCoroutine_13; }
	inline void set_jCoroutine_13(Il2CppObject * value)
	{
		___jCoroutine_13 = value;
		Il2CppCodeGenWriteBarrier(&___jCoroutine_13, value);
	}

	inline static int32_t get_offset_of_lCoroutine_14() { return static_cast<int32_t>(offsetof(HCAnimatedBanner_t2921968851, ___lCoroutine_14)); }
	inline Il2CppObject * get_lCoroutine_14() const { return ___lCoroutine_14; }
	inline Il2CppObject ** get_address_of_lCoroutine_14() { return &___lCoroutine_14; }
	inline void set_lCoroutine_14(Il2CppObject * value)
	{
		___lCoroutine_14 = value;
		Il2CppCodeGenWriteBarrier(&___lCoroutine_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
