﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3591816995.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdInt2700663047.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs
struct  NendAdInterstitialLoadEventArgs_t1260857315  : public EventArgs_t3591816995
{
public:
	// NendUnityPlugin.Common.NendAdInterstitialStatusCode NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs::<StatusCode>k__BackingField
	int32_t ___U3CStatusCodeU3Ek__BackingField_1;
	// System.String NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs::<SpotId>k__BackingField
	String_t* ___U3CSpotIdU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CStatusCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NendAdInterstitialLoadEventArgs_t1260857315, ___U3CStatusCodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CStatusCodeU3Ek__BackingField_1() const { return ___U3CStatusCodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStatusCodeU3Ek__BackingField_1() { return &___U3CStatusCodeU3Ek__BackingField_1; }
	inline void set_U3CStatusCodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CStatusCodeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSpotIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NendAdInterstitialLoadEventArgs_t1260857315, ___U3CSpotIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CSpotIdU3Ek__BackingField_2() const { return ___U3CSpotIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CSpotIdU3Ek__BackingField_2() { return &___U3CSpotIdU3Ek__BackingField_2; }
	inline void set_U3CSpotIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CSpotIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSpotIdU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
