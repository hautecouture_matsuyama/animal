﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// NendUnityPlugin.Common.NendUtils/NendID
struct NendID_t3358092198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Common.NendUtils/Account
struct  Account_t2230227361  : public Il2CppObject
{
public:
	// NendUnityPlugin.Common.NendUtils/NendID NendUnityPlugin.Common.NendUtils/Account::android
	NendID_t3358092198 * ___android_0;
	// NendUnityPlugin.Common.NendUtils/NendID NendUnityPlugin.Common.NendUtils/Account::iOS
	NendID_t3358092198 * ___iOS_1;

public:
	inline static int32_t get_offset_of_android_0() { return static_cast<int32_t>(offsetof(Account_t2230227361, ___android_0)); }
	inline NendID_t3358092198 * get_android_0() const { return ___android_0; }
	inline NendID_t3358092198 ** get_address_of_android_0() { return &___android_0; }
	inline void set_android_0(NendID_t3358092198 * value)
	{
		___android_0 = value;
		Il2CppCodeGenWriteBarrier(&___android_0, value);
	}

	inline static int32_t get_offset_of_iOS_1() { return static_cast<int32_t>(offsetof(Account_t2230227361, ___iOS_1)); }
	inline NendID_t3358092198 * get_iOS_1() const { return ___iOS_1; }
	inline NendID_t3358092198 ** get_address_of_iOS_1() { return &___iOS_1; }
	inline void set_iOS_1(NendID_t3358092198 * value)
	{
		___iOS_1 = value;
		Il2CppCodeGenWriteBarrier(&___iOS_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
