﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Video.NendAdRewardedItem
struct  NendAdRewardedItem_t3964314288  : public Il2CppObject
{
public:
	// System.String NendUnityPlugin.AD.Video.NendAdRewardedItem::currencyName
	String_t* ___currencyName_0;
	// System.Int32 NendUnityPlugin.AD.Video.NendAdRewardedItem::currencyAmount
	int32_t ___currencyAmount_1;

public:
	inline static int32_t get_offset_of_currencyName_0() { return static_cast<int32_t>(offsetof(NendAdRewardedItem_t3964314288, ___currencyName_0)); }
	inline String_t* get_currencyName_0() const { return ___currencyName_0; }
	inline String_t** get_address_of_currencyName_0() { return &___currencyName_0; }
	inline void set_currencyName_0(String_t* value)
	{
		___currencyName_0 = value;
		Il2CppCodeGenWriteBarrier(&___currencyName_0, value);
	}

	inline static int32_t get_offset_of_currencyAmount_1() { return static_cast<int32_t>(offsetof(NendAdRewardedItem_t3964314288, ___currencyAmount_1)); }
	inline int32_t get_currencyAmount_1() const { return ___currencyAmount_1; }
	inline int32_t* get_address_of_currencyAmount_1() { return &___currencyAmount_1; }
	inline void set_currencyAmount_1(int32_t value)
	{
		___currencyAmount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
