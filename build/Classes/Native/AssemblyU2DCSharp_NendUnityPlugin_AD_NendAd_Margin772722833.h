﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.NendAd/Margin
struct  Margin_t772722833  : public Il2CppObject
{
public:
	// System.Single NendUnityPlugin.AD.NendAd/Margin::left
	float ___left_0;
	// System.Single NendUnityPlugin.AD.NendAd/Margin::top
	float ___top_1;
	// System.Single NendUnityPlugin.AD.NendAd/Margin::right
	float ___right_2;
	// System.Single NendUnityPlugin.AD.NendAd/Margin::bottom
	float ___bottom_3;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(Margin_t772722833, ___left_0)); }
	inline float get_left_0() const { return ___left_0; }
	inline float* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(float value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_top_1() { return static_cast<int32_t>(offsetof(Margin_t772722833, ___top_1)); }
	inline float get_top_1() const { return ___top_1; }
	inline float* get_address_of_top_1() { return &___top_1; }
	inline void set_top_1(float value)
	{
		___top_1 = value;
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(Margin_t772722833, ___right_2)); }
	inline float get_right_2() const { return ___right_2; }
	inline float* get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(float value)
	{
		___right_2 = value;
	}

	inline static int32_t get_offset_of_bottom_3() { return static_cast<int32_t>(offsetof(Margin_t772722833, ___bottom_3)); }
	inline float get_bottom_3() const { return ___bottom_3; }
	inline float* get_address_of_bottom_3() { return &___bottom_3; }
	inline void set_bottom_3(float value)
	{
		___bottom_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
