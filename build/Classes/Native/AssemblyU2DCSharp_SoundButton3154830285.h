﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Image
struct Image_t2670269651;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundButton
struct  SoundButton_t3154830285  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite SoundButton::sound_off
	Sprite_t280657092 * ___sound_off_2;
	// UnityEngine.Sprite SoundButton::sound_on
	Sprite_t280657092 * ___sound_on_3;
	// UnityEngine.UI.Image SoundButton::soundImage
	Image_t2670269651 * ___soundImage_4;

public:
	inline static int32_t get_offset_of_sound_off_2() { return static_cast<int32_t>(offsetof(SoundButton_t3154830285, ___sound_off_2)); }
	inline Sprite_t280657092 * get_sound_off_2() const { return ___sound_off_2; }
	inline Sprite_t280657092 ** get_address_of_sound_off_2() { return &___sound_off_2; }
	inline void set_sound_off_2(Sprite_t280657092 * value)
	{
		___sound_off_2 = value;
		Il2CppCodeGenWriteBarrier(&___sound_off_2, value);
	}

	inline static int32_t get_offset_of_sound_on_3() { return static_cast<int32_t>(offsetof(SoundButton_t3154830285, ___sound_on_3)); }
	inline Sprite_t280657092 * get_sound_on_3() const { return ___sound_on_3; }
	inline Sprite_t280657092 ** get_address_of_sound_on_3() { return &___sound_on_3; }
	inline void set_sound_on_3(Sprite_t280657092 * value)
	{
		___sound_on_3 = value;
		Il2CppCodeGenWriteBarrier(&___sound_on_3, value);
	}

	inline static int32_t get_offset_of_soundImage_4() { return static_cast<int32_t>(offsetof(SoundButton_t3154830285, ___soundImage_4)); }
	inline Image_t2670269651 * get_soundImage_4() const { return ___soundImage_4; }
	inline Image_t2670269651 ** get_address_of_soundImage_4() { return &___soundImage_4; }
	inline void set_soundImage_4(Image_t2670269651 * value)
	{
		___soundImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___soundImage_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
