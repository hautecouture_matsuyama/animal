﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ObsUpCollisionCheck2909508583.h"
#include "AssemblyU2DCSharp_PlayerControl1977005294.h"
#include "AssemblyU2DCSharp_PlayerControl_U3CSlideTimeU3Ec__I752433291.h"
#include "AssemblyU2DCSharp_RunObjects2121017103.h"
#include "AssemblyU2DCSharp_Score2516855617.h"
#include "AssemblyU2DCSharp_SherePlugins4073479905.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nend270881991.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen3830347218.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen4155484538.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen3059274368.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen3385596145.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nend136425400.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen2901615101.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_FullBoard_Nen1510350271.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Events_610464567.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Events3307153462.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handlers29098297.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handle2622036411.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handler283384251.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handle2934446534.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handler258087360.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handle4278309981.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handle2201492025.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handler188448812.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handler188317754.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Handle3542390976.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Advert2175459971.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NativeA775405424.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NativeA772434093.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Native1108917185.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Native3065232321.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Native4254168860.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAd3381739006.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAdN799839499.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAd1337685977.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAd1337685978.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAd2295075562.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAdNa87332621.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NendAd2014609480.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_1147818846.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_1611451218.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_4075384413.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_3414853624.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_3323732960.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_2469200630.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_U575704595.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_Utils_4270539830.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAd284737821.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAd_Margin772722833.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdBanner489953245.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdBanner_2479415987.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdInterst1417649638.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdInterst1260857315.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdInterst3083222281.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdIntersti974162912.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_ErrorVi3139399691.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdI1383172729.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdR3964314288.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdR3161671531.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdRe687064503.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdU4152365987.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdU3769286665.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV1781712043.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV1357133087.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV1837550073.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV4265396701.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV3987677121.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV2144365953.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV3890769867.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV3646835482.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdVi895736144.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdVi172780505.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdVi464199100.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV2883267092.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_Rewarde3338436455.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_VideoAd2567701888.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_Gravity3768410666.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdErr3390548813.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdInt2404809203.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdInt2700663047.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdInt1787058500.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdLogg117402011.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdLogg918333666.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendAdMai3569218590.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendError2215997335.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendUtils3369562899.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendUtils3358092198.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_NendUtils2230227361.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Layout_ComplexUn1764145246.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Layout_NendAdDef3894915050.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Layout_NendAdDefa129204534.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_NendAdN2472262152.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_NendAdN2404450899.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Platform_NendAdNa896923007.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (ObsUpCollisionCheck_t2909508583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[1] = 
{
	ObsUpCollisionCheck_t2909508583::get_offset_of_isDead_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (PlayerControl_t1977005294), -1, sizeof(PlayerControl_t1977005294_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1801[18] = 
{
	PlayerControl_t1977005294::get_offset_of_jump_2(),
	PlayerControl_t1977005294_StaticFields::get_offset_of_slide_3(),
	PlayerControl_t1977005294::get_offset_of_grounded_4(),
	PlayerControl_t1977005294::get_offset_of_leftTap_5(),
	PlayerControl_t1977005294::get_offset_of_rightTap_6(),
	PlayerControl_t1977005294_StaticFields::get_offset_of_isDead_7(),
	PlayerControl_t1977005294::get_offset_of_jumpForce_8(),
	PlayerControl_t1977005294_StaticFields::get_offset_of_speed_9(),
	PlayerControl_t1977005294::get_offset_of_groundCheck_10(),
	PlayerControl_t1977005294_StaticFields::get_offset_of_anim_11(),
	PlayerControl_t1977005294::get_offset_of_swipeDetector_12(),
	PlayerControl_t1977005294::get_offset_of_jumpClip_13(),
	PlayerControl_t1977005294::get_offset_of_slideClip_14(),
	PlayerControl_t1977005294::get_offset_of_musicClip_15(),
	PlayerControl_t1977005294::get_offset_of_jumpSource_16(),
	PlayerControl_t1977005294::get_offset_of_slideSource_17(),
	PlayerControl_t1977005294_StaticFields::get_offset_of_musicSource_18(),
	PlayerControl_t1977005294::get_offset_of_prevScore_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (U3CSlideTimeU3Ec__Iterator0_t752433291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[3] = 
{
	U3CSlideTimeU3Ec__Iterator0_t752433291::get_offset_of_U24current_0(),
	U3CSlideTimeU3Ec__Iterator0_t752433291::get_offset_of_U24disposing_1(),
	U3CSlideTimeU3Ec__Iterator0_t752433291::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (RunObjects_t2121017103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[1] = 
{
	RunObjects_t2121017103::get_offset_of_objctSprite_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (Score_t2516855617), -1, sizeof(Score_t2516855617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1804[2] = 
{
	Score_t2516855617_StaticFields::get_offset_of_scoreNum_2(),
	Score_t2516855617_StaticFields::get_offset_of_bestNum_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (SherePlugins_t4073479905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[1] = 
{
	SherePlugins_t4073479905::get_offset_of_shareTexture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (NendAdFullBoard_t270881991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[9] = 
{
	NendAdFullBoard_t270881991::get_offset_of_AdLoaded_0(),
	NendAdFullBoard_t270881991::get_offset_of_AdFailedToLoad_1(),
	NendAdFullBoard_t270881991::get_offset_of_AdShown_2(),
	NendAdFullBoard_t270881991::get_offset_of_AdClicked_3(),
	NendAdFullBoard_t270881991::get_offset_of_AdDismissed_4(),
	NendAdFullBoard_t270881991::get_offset_of_m_isLoading_5(),
	NendAdFullBoard_t270881991::get_offset_of_m_isShowing_6(),
	NendAdFullBoard_t270881991::get_offset_of_m_isLoadSuccess_7(),
	NendAdFullBoard_t270881991::get_offset_of_backgroundColor_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (NendAdFullBoardLoaded_t3830347218), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (NendAdFullBoardFailedToLoad_t4155484538), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (NendAdFullBoardShown_t3059274368), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (NendAdFullBoardClick_t3385596145), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (NendAdFullBoardDismiss_t136425400), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (FullBoardAdCallbackType_t2901615101)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1812[8] = 
{
	FullBoardAdCallbackType_t2901615101::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (FullBoardAdErrorType_t1510350271)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1813[4] = 
{
	FullBoardAdErrorType_t1510350271::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (NativeAdViewEvent_t610464567), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (NativeAdViewFailedToLoadEvent_t3307153462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (ClickHandler_t29098297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[1] = 
{
	ClickHandler_t29098297::get_offset_of_OnClick_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (ClickHandler2D_t2622036411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (ClickHandler3D_t283384251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (ClickHandlerGO_t2934446534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (ClickHandlerUI_t258087360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (ImpressionHandler_t4278309981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[4] = 
{
	0,
	0,
	ImpressionHandler_t4278309981::get_offset_of_m_TimeElapsed_4(),
	ImpressionHandler_t4278309981::get_offset_of_OnImpression_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (Corner_t2201492025)+ sizeof (Il2CppObject), sizeof(Corner_t2201492025 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1822[4] = 
{
	Corner_t2201492025::get_offset_of_left_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Corner_t2201492025::get_offset_of_top_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Corner_t2201492025::get_offset_of_right_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Corner_t2201492025::get_offset_of_bottom_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (ImpressionHandlerGO_t188448812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (ImpressionHandlerUI_t188317754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[3] = 
{
	ImpressionHandlerUI_t188317754::get_offset_of_m_Camera_6(),
	ImpressionHandlerUI_t188317754::get_offset_of_m_Canvas_7(),
	ImpressionHandlerUI_t188317754::get_offset_of_m_Corners_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (U3CIsViewableU3Ec__AnonStorey0_t3542390976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[1] = 
{
	U3CIsViewableU3Ec__AnonStorey0_t3542390976::get_offset_of_camera_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (AdvertisingExplicitly_t2175459971)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1826[5] = 
{
	AdvertisingExplicitly_t2175459971::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (NativeAd_t775405424), -1, sizeof(NativeAd_t775405424_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1829[4] = 
{
	NativeAd_t775405424::get_offset_of_AdClicked_0(),
	NativeAd_t775405424::get_offset_of_m_IsImpression_1(),
	NativeAd_t775405424::get_offset_of_m_Handlers_2(),
	NativeAd_t775405424_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (NativeAdClient_t772434093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[6] = 
{
	0,
	NativeAdClient_t772434093::get_offset_of_m_Timer_1(),
	NativeAdClient_t772434093::get_offset_of_m_ReloadCallback_2(),
	NativeAdClient_t772434093::get_offset_of_m_Callbacks_3(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[3] = 
{
	U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185::get_offset_of_ad_0(),
	U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185::get_offset_of_code_1(),
	U3CDeliverResponseOnMainThreadU3Ec__AnonStorey1_t1108917185::get_offset_of_message_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[2] = 
{
	U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321::get_offset_of_callback_0(),
	U3CDeliverResponseOnMainThreadU3Ec__AnonStorey0_t3065232321::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (NativeAdClientFactory_t4254168860), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (NendAdNative_t3381739006), -1, sizeof(NendAdNative_t3381739006_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1834[10] = 
{
	NendAdNative_t3381739006::get_offset_of_account_2(),
	NendAdNative_t3381739006::get_offset_of_loadWhenActivated_3(),
	NendAdNative_t3381739006::get_offset_of_enableAutoReload_4(),
	NendAdNative_t3381739006::get_offset_of_reloadInterval_5(),
	NendAdNative_t3381739006::get_offset_of_views_6(),
	NendAdNative_t3381739006::get_offset_of_AdLoaded_7(),
	NendAdNative_t3381739006::get_offset_of_AdFailedToReceive_8(),
	NendAdNative_t3381739006::get_offset_of_m_Client_9(),
	NendAdNative_t3381739006::get_offset_of_m_Timer_10(),
	NendAdNative_t3381739006_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (U3CRegisterAdViewU3Ec__AnonStorey0_t799839499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[1] = 
{
	U3CRegisterAdViewU3Ec__AnonStorey0_t799839499::get_offset_of_view_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (U3CLoadAdU3Ec__AnonStorey1_t1337685977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1836[1] = 
{
	U3CLoadAdU3Ec__AnonStorey1_t1337685977::get_offset_of_tag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (U3CLoadAdU3Ec__AnonStorey2_t1337685978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[2] = 
{
	U3CLoadAdU3Ec__AnonStorey2_t1337685978::get_offset_of_nativeAdView_0(),
	U3CLoadAdU3Ec__AnonStorey2_t1337685978::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (NendAdNativeImage_t2295075562), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (NendAdNativeText_t87332621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (NendAdNativeView_t2014609480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[17] = 
{
	NendAdNativeView_t2014609480::get_offset_of_viewTag_2(),
	NendAdNativeView_t2014609480::get_offset_of_renderWhenLoaded_3(),
	NendAdNativeView_t2014609480::get_offset_of_renderWhenActivated_4(),
	NendAdNativeView_t2014609480::get_offset_of_advertisingExplicitly_5(),
	NendAdNativeView_t2014609480::get_offset_of_prText_6(),
	NendAdNativeView_t2014609480::get_offset_of_shortText_7(),
	NendAdNativeView_t2014609480::get_offset_of_longText_8(),
	NendAdNativeView_t2014609480::get_offset_of_promotionNameText_9(),
	NendAdNativeView_t2014609480::get_offset_of_promotionUrlText_10(),
	NendAdNativeView_t2014609480::get_offset_of_actionButtonText_11(),
	NendAdNativeView_t2014609480::get_offset_of_adImage_12(),
	NendAdNativeView_t2014609480::get_offset_of_logoImage_13(),
	NendAdNativeView_t2014609480::get_offset_of_AdShown_14(),
	NendAdNativeView_t2014609480::get_offset_of_AdFailedToShow_15(),
	NendAdNativeView_t2014609480::get_offset_of_AdClicked_16(),
	NendAdNativeView_t2014609480::get_offset_of_m_LoadedNativeAd_17(),
	NendAdNativeView_t2014609480::get_offset_of_m_ShowingNativeAd_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (NendAdLogger_t1147818846), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (NendAdLogLevel_t1611451218)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1842[6] = 
{
	NendAdLogLevel_t1611451218::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (SimpleTimer_t4075384413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[2] = 
{
	SimpleTimer_t4075384413::get_offset_of_OnFireEvent_0(),
	SimpleTimer_t4075384413::get_offset_of_m_Timer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (TextureLoader_t3414853624), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (TextureCache_t3323732960), -1, sizeof(TextureCache_t3323732960_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1845[2] = 
{
	TextureCache_t3323732960_StaticFields::get_offset_of_s_Instance_0(),
	TextureCache_t3323732960::get_offset_of_m_Cache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (U3CLoadTextureU3Ec__Iterator0_t2469200630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[7] = 
{
	U3CLoadTextureU3Ec__Iterator0_t2469200630::get_offset_of_url_0(),
	U3CLoadTextureU3Ec__Iterator0_t2469200630::get_offset_of_U3CtextureU3E__0_1(),
	U3CLoadTextureU3Ec__Iterator0_t2469200630::get_offset_of_callback_2(),
	U3CLoadTextureU3Ec__Iterator0_t2469200630::get_offset_of_U3CwwwU3E__1_3(),
	U3CLoadTextureU3Ec__Iterator0_t2469200630::get_offset_of_U24current_4(),
	U3CLoadTextureU3Ec__Iterator0_t2469200630::get_offset_of_U24disposing_5(),
	U3CLoadTextureU3Ec__Iterator0_t2469200630::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (UIRenderer_t575704595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (U3CTryRenderImageU3Ec__AnonStorey0_t4270539830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[1] = 
{
	U3CTryRenderImageU3Ec__AnonStorey0_t4270539830::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (NendAd_t284737821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (Margin_t772722833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[4] = 
{
	Margin_t772722833::get_offset_of_left_0(),
	Margin_t772722833::get_offset_of_top_1(),
	Margin_t772722833::get_offset_of_right_2(),
	Margin_t772722833::get_offset_of_bottom_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (NendAdBanner_t489953245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[15] = 
{
	NendAdBanner_t489953245::get_offset_of_account_2(),
	NendAdBanner_t489953245::get_offset_of_automaticDisplay_3(),
	NendAdBanner_t489953245::get_offset_of_outputLog_4(),
	NendAdBanner_t489953245::get_offset_of_adjustSize_5(),
	NendAdBanner_t489953245::get_offset_of_size_6(),
	NendAdBanner_t489953245::get_offset_of_backgroundColor_7(),
	NendAdBanner_t489953245::get_offset_of_gravity_8(),
	NendAdBanner_t489953245::get_offset_of_margin_9(),
	NendAdBanner_t489953245::get_offset_of__interface_10(),
	NendAdBanner_t489953245::get_offset_of_AdLoaded_11(),
	NendAdBanner_t489953245::get_offset_of_AdFailedToReceive_12(),
	NendAdBanner_t489953245::get_offset_of_AdReceived_13(),
	NendAdBanner_t489953245::get_offset_of_AdClicked_14(),
	NendAdBanner_t489953245::get_offset_of_AdBacked_15(),
	NendAdBanner_t489953245::get_offset_of_InformationClicked_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (BannerSize_t2479415987)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1852[6] = 
{
	BannerSize_t2479415987::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (NendAdInterstitial_t1417649638), -1, sizeof(NendAdInterstitial_t1417649638_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1853[11] = 
{
	NendAdInterstitial_t1417649638::get_offset_of_outputLog_2(),
	NendAdInterstitial_t1417649638::get_offset_of_android_key_3(),
	NendAdInterstitial_t1417649638::get_offset_of_android_id_4(),
	NendAdInterstitial_t1417649638::get_offset_of_ios_key_5(),
	NendAdInterstitial_t1417649638::get_offset_of_ios_id_6(),
	NendAdInterstitial_t1417649638_StaticFields::get_offset_of__instance_7(),
	NendAdInterstitial_t1417649638::get_offset_of_m_IsAutoReloadEnabled_8(),
	NendAdInterstitial_t1417649638::get_offset_of__interface_9(),
	NendAdInterstitial_t1417649638::get_offset_of_AdLoaded_10(),
	NendAdInterstitial_t1417649638::get_offset_of_AdClicked_11(),
	NendAdInterstitial_t1417649638::get_offset_of_AdShown_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (NendAdInterstitialLoadEventArgs_t1260857315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[2] = 
{
	NendAdInterstitialLoadEventArgs_t1260857315::get_offset_of_U3CStatusCodeU3Ek__BackingField_1(),
	NendAdInterstitialLoadEventArgs_t1260857315::get_offset_of_U3CSpotIdU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (NendAdInterstitialShowEventArgs_t3083222281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[2] = 
{
	NendAdInterstitialShowEventArgs_t3083222281::get_offset_of_U3CShowResultU3Ek__BackingField_1(),
	NendAdInterstitialShowEventArgs_t3083222281::get_offset_of_U3CSpotIdU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (NendAdInterstitialClickEventArgs_t974162912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[2] = 
{
	NendAdInterstitialClickEventArgs_t974162912::get_offset_of_U3CClickTypeU3Ek__BackingField_1(),
	NendAdInterstitialClickEventArgs_t974162912::get_offset_of_U3CSpotIdU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (ErrorVideoAdCallbackArgments_t3139399691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[1] = 
{
	ErrorVideoAdCallbackArgments_t3139399691::get_offset_of_errorCode_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (NendAdInterstitialVideo_t1383172729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (NendAdRewardedItem_t3964314288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[2] = 
{
	NendAdRewardedItem_t3964314288::get_offset_of_currencyName_0(),
	NendAdRewardedItem_t3964314288::get_offset_of_currencyAmount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (NendAdRewardedVideo_t3161671531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1860[1] = 
{
	NendAdRewardedVideo_t3161671531::get_offset_of_Rewarded_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (NendAdVideoRewarded_t687064503), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (NendAdUserFeature_t4152365987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[10] = 
{
	NendAdUserFeature_t4152365987::get_offset_of_gender_0(),
	NendAdUserFeature_t4152365987::get_offset_of_dayOfBirth_1(),
	NendAdUserFeature_t4152365987::get_offset_of_monthOfBirth_2(),
	NendAdUserFeature_t4152365987::get_offset_of_yearOfBirth_3(),
	NendAdUserFeature_t4152365987::get_offset_of_age_4(),
	NendAdUserFeature_t4152365987::get_offset_of_customFeaturesIntDic_5(),
	NendAdUserFeature_t4152365987::get_offset_of_customFeaturesDoubleDic_6(),
	NendAdUserFeature_t4152365987::get_offset_of_customFeaturesStringDic_7(),
	NendAdUserFeature_t4152365987::get_offset_of_customFeaturesBoolDic_8(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (Gender_t3769286665)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1863[3] = 
{
	Gender_t3769286665::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (NendAdVideo_t1781712043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1864[10] = 
{
	NendAdVideo_t1781712043::get_offset_of_AdLoaded_0(),
	NendAdVideo_t1781712043::get_offset_of_AdFailedToLoad_1(),
	NendAdVideo_t1781712043::get_offset_of_AdFailedToPlay_2(),
	NendAdVideo_t1781712043::get_offset_of_AdShown_3(),
	NendAdVideo_t1781712043::get_offset_of_AdStarted_4(),
	NendAdVideo_t1781712043::get_offset_of_AdStopped_5(),
	NendAdVideo_t1781712043::get_offset_of_AdCompleted_6(),
	NendAdVideo_t1781712043::get_offset_of_AdClicked_7(),
	NendAdVideo_t1781712043::get_offset_of_InformationClicked_8(),
	NendAdVideo_t1781712043::get_offset_of_AdClosed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (NendAdVideoLoaded_t1357133087), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (NendAdVideoFailedToLoad_t1837550073), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (NendAdVideoFailedToPlay_t4265396701), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (NendAdVideoShown_t3987677121), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (NendAdVideoStarted_t2144365953), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (NendAdVideoStopped_t3890769867), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (NendAdVideoCompleted_t3646835482), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (NendAdVideoClick_t895736144), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (NendAdVideoInformationClick_t172780505), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (NendAdVideoClosed_t464199100), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (VideoAdCallbackType_t2883267092)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1875[14] = 
{
	VideoAdCallbackType_t2883267092::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (RewardedVideoAdCallbackArgments_t3338436455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[1] = 
{
	RewardedVideoAdCallbackArgments_t3338436455::get_offset_of_rewardedItem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (VideoAdCallbackArgments_t2567701888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[1] = 
{
	VideoAdCallbackArgments_t2567701888::get_offset_of_videoAdCallbackType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (Gravity_t3768410666)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1878[7] = 
{
	Gravity_t3768410666::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (NendAdErrorEventArgs_t3390548813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[2] = 
{
	NendAdErrorEventArgs_t3390548813::get_offset_of_U3CErrorCodeU3Ek__BackingField_1(),
	NendAdErrorEventArgs_t3390548813::get_offset_of_U3CMessageU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (NendAdInterstitialShowResult_t2404809203)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1880[8] = 
{
	NendAdInterstitialShowResult_t2404809203::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (NendAdInterstitialStatusCode_t2700663047)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1881[5] = 
{
	NendAdInterstitialStatusCode_t2700663047::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (NendAdInterstitialClickType_t1787058500)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1882[4] = 
{
	NendAdInterstitialClickType_t1787058500::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (NendAdLogger_t117402011), -1, sizeof(NendAdLogger_t117402011_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1883[2] = 
{
	NendAdLogger_t117402011_StaticFields::get_offset_of_s_LogLevel_0(),
	NendAdLogger_t117402011_StaticFields::get_offset_of__interface_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (NendAdLogLevel_t918333666)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1884[6] = 
{
	NendAdLogLevel_t918333666::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (NendAdMainThreadWorker_t3569218590), -1, sizeof(NendAdMainThreadWorker_t3569218590_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1885[2] = 
{
	NendAdMainThreadWorker_t3569218590_StaticFields::get_offset_of_s_ActionQueue_2(),
	NendAdMainThreadWorker_t3569218590_StaticFields::get_offset_of_s_Instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (NendErrorCode_t2215997335)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1886[6] = 
{
	NendErrorCode_t2215997335::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (NendUtils_t3369562899), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (NendID_t3358092198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[2] = 
{
	NendID_t3358092198::get_offset_of_apiKey_0(),
	NendID_t3358092198::get_offset_of_spotID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (Account_t2230227361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[2] = 
{
	Account_t2230227361::get_offset_of_android_0(),
	Account_t2230227361::get_offset_of_iOS_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (ComplexUnit_t1764145246)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1890[3] = 
{
	ComplexUnit_t1764145246::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (NendAdDefaultLayoutBuilder_t3894915050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[2] = 
{
	NendAdDefaultLayoutBuilder_t3894915050::get_offset_of_layoutParams_0(),
	NendAdDefaultLayoutBuilder_t3894915050::get_offset_of_unit_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (NendAdDefaultLayoutParams_t129204534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[5] = 
{
	NendAdDefaultLayoutParams_t129204534::get_offset_of_gravity_0(),
	NendAdDefaultLayoutParams_t129204534::get_offset_of_left_1(),
	NendAdDefaultLayoutParams_t129204534::get_offset_of_top_2(),
	NendAdDefaultLayoutParams_t129204534::get_offset_of_right_3(),
	NendAdDefaultLayoutParams_t129204534::get_offset_of_bottom_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (NendAdNativeInterfaceFactory_t2472262152), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (BannerStub_t2404450899), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (InterstitialStub_t896923007), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
