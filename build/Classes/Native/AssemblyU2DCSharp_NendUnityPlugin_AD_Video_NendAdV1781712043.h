﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoLoaded
struct NendAdVideoLoaded_t1357133087;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToLoad
struct NendAdVideoFailedToLoad_t1837550073;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToPlay
struct NendAdVideoFailedToPlay_t4265396701;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoShown
struct NendAdVideoShown_t3987677121;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick
struct NendAdVideoClick_t895736144;
// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClosed
struct NendAdVideoClosed_t464199100;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Video.NendAdVideo
struct  NendAdVideo_t1781712043  : public Il2CppObject
{
public:
	// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoLoaded NendUnityPlugin.AD.Video.NendAdVideo::AdLoaded
	NendAdVideoLoaded_t1357133087 * ___AdLoaded_0;
	// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToLoad NendUnityPlugin.AD.Video.NendAdVideo::AdFailedToLoad
	NendAdVideoFailedToLoad_t1837550073 * ___AdFailedToLoad_1;
	// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoFailedToPlay NendUnityPlugin.AD.Video.NendAdVideo::AdFailedToPlay
	NendAdVideoFailedToPlay_t4265396701 * ___AdFailedToPlay_2;
	// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoShown NendUnityPlugin.AD.Video.NendAdVideo::AdShown
	NendAdVideoShown_t3987677121 * ___AdShown_3;
	// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick NendUnityPlugin.AD.Video.NendAdVideo::AdStarted
	NendAdVideoClick_t895736144 * ___AdStarted_4;
	// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick NendUnityPlugin.AD.Video.NendAdVideo::AdStopped
	NendAdVideoClick_t895736144 * ___AdStopped_5;
	// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick NendUnityPlugin.AD.Video.NendAdVideo::AdCompleted
	NendAdVideoClick_t895736144 * ___AdCompleted_6;
	// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick NendUnityPlugin.AD.Video.NendAdVideo::AdClicked
	NendAdVideoClick_t895736144 * ___AdClicked_7;
	// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClick NendUnityPlugin.AD.Video.NendAdVideo::InformationClicked
	NendAdVideoClick_t895736144 * ___InformationClicked_8;
	// NendUnityPlugin.AD.Video.NendAdVideo/NendAdVideoClosed NendUnityPlugin.AD.Video.NendAdVideo::AdClosed
	NendAdVideoClosed_t464199100 * ___AdClosed_9;

public:
	inline static int32_t get_offset_of_AdLoaded_0() { return static_cast<int32_t>(offsetof(NendAdVideo_t1781712043, ___AdLoaded_0)); }
	inline NendAdVideoLoaded_t1357133087 * get_AdLoaded_0() const { return ___AdLoaded_0; }
	inline NendAdVideoLoaded_t1357133087 ** get_address_of_AdLoaded_0() { return &___AdLoaded_0; }
	inline void set_AdLoaded_0(NendAdVideoLoaded_t1357133087 * value)
	{
		___AdLoaded_0 = value;
		Il2CppCodeGenWriteBarrier(&___AdLoaded_0, value);
	}

	inline static int32_t get_offset_of_AdFailedToLoad_1() { return static_cast<int32_t>(offsetof(NendAdVideo_t1781712043, ___AdFailedToLoad_1)); }
	inline NendAdVideoFailedToLoad_t1837550073 * get_AdFailedToLoad_1() const { return ___AdFailedToLoad_1; }
	inline NendAdVideoFailedToLoad_t1837550073 ** get_address_of_AdFailedToLoad_1() { return &___AdFailedToLoad_1; }
	inline void set_AdFailedToLoad_1(NendAdVideoFailedToLoad_t1837550073 * value)
	{
		___AdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier(&___AdFailedToLoad_1, value);
	}

	inline static int32_t get_offset_of_AdFailedToPlay_2() { return static_cast<int32_t>(offsetof(NendAdVideo_t1781712043, ___AdFailedToPlay_2)); }
	inline NendAdVideoFailedToPlay_t4265396701 * get_AdFailedToPlay_2() const { return ___AdFailedToPlay_2; }
	inline NendAdVideoFailedToPlay_t4265396701 ** get_address_of_AdFailedToPlay_2() { return &___AdFailedToPlay_2; }
	inline void set_AdFailedToPlay_2(NendAdVideoFailedToPlay_t4265396701 * value)
	{
		___AdFailedToPlay_2 = value;
		Il2CppCodeGenWriteBarrier(&___AdFailedToPlay_2, value);
	}

	inline static int32_t get_offset_of_AdShown_3() { return static_cast<int32_t>(offsetof(NendAdVideo_t1781712043, ___AdShown_3)); }
	inline NendAdVideoShown_t3987677121 * get_AdShown_3() const { return ___AdShown_3; }
	inline NendAdVideoShown_t3987677121 ** get_address_of_AdShown_3() { return &___AdShown_3; }
	inline void set_AdShown_3(NendAdVideoShown_t3987677121 * value)
	{
		___AdShown_3 = value;
		Il2CppCodeGenWriteBarrier(&___AdShown_3, value);
	}

	inline static int32_t get_offset_of_AdStarted_4() { return static_cast<int32_t>(offsetof(NendAdVideo_t1781712043, ___AdStarted_4)); }
	inline NendAdVideoClick_t895736144 * get_AdStarted_4() const { return ___AdStarted_4; }
	inline NendAdVideoClick_t895736144 ** get_address_of_AdStarted_4() { return &___AdStarted_4; }
	inline void set_AdStarted_4(NendAdVideoClick_t895736144 * value)
	{
		___AdStarted_4 = value;
		Il2CppCodeGenWriteBarrier(&___AdStarted_4, value);
	}

	inline static int32_t get_offset_of_AdStopped_5() { return static_cast<int32_t>(offsetof(NendAdVideo_t1781712043, ___AdStopped_5)); }
	inline NendAdVideoClick_t895736144 * get_AdStopped_5() const { return ___AdStopped_5; }
	inline NendAdVideoClick_t895736144 ** get_address_of_AdStopped_5() { return &___AdStopped_5; }
	inline void set_AdStopped_5(NendAdVideoClick_t895736144 * value)
	{
		___AdStopped_5 = value;
		Il2CppCodeGenWriteBarrier(&___AdStopped_5, value);
	}

	inline static int32_t get_offset_of_AdCompleted_6() { return static_cast<int32_t>(offsetof(NendAdVideo_t1781712043, ___AdCompleted_6)); }
	inline NendAdVideoClick_t895736144 * get_AdCompleted_6() const { return ___AdCompleted_6; }
	inline NendAdVideoClick_t895736144 ** get_address_of_AdCompleted_6() { return &___AdCompleted_6; }
	inline void set_AdCompleted_6(NendAdVideoClick_t895736144 * value)
	{
		___AdCompleted_6 = value;
		Il2CppCodeGenWriteBarrier(&___AdCompleted_6, value);
	}

	inline static int32_t get_offset_of_AdClicked_7() { return static_cast<int32_t>(offsetof(NendAdVideo_t1781712043, ___AdClicked_7)); }
	inline NendAdVideoClick_t895736144 * get_AdClicked_7() const { return ___AdClicked_7; }
	inline NendAdVideoClick_t895736144 ** get_address_of_AdClicked_7() { return &___AdClicked_7; }
	inline void set_AdClicked_7(NendAdVideoClick_t895736144 * value)
	{
		___AdClicked_7 = value;
		Il2CppCodeGenWriteBarrier(&___AdClicked_7, value);
	}

	inline static int32_t get_offset_of_InformationClicked_8() { return static_cast<int32_t>(offsetof(NendAdVideo_t1781712043, ___InformationClicked_8)); }
	inline NendAdVideoClick_t895736144 * get_InformationClicked_8() const { return ___InformationClicked_8; }
	inline NendAdVideoClick_t895736144 ** get_address_of_InformationClicked_8() { return &___InformationClicked_8; }
	inline void set_InformationClicked_8(NendAdVideoClick_t895736144 * value)
	{
		___InformationClicked_8 = value;
		Il2CppCodeGenWriteBarrier(&___InformationClicked_8, value);
	}

	inline static int32_t get_offset_of_AdClosed_9() { return static_cast<int32_t>(offsetof(NendAdVideo_t1781712043, ___AdClosed_9)); }
	inline NendAdVideoClosed_t464199100 * get_AdClosed_9() const { return ___AdClosed_9; }
	inline NendAdVideoClosed_t464199100 ** get_address_of_AdClosed_9() { return &___AdClosed_9; }
	inline void set_AdClosed_9(NendAdVideoClosed_t464199100 * value)
	{
		___AdClosed_9 = value;
		Il2CppCodeGenWriteBarrier(&___AdClosed_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
