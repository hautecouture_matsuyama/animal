﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NativeA772434093.h"
#include "mscorlib_System_IntPtr840150181.h"

// NendUnityPlugin.Platform.iOS.IOSNativeAdClient/NendUnityNativeAdCallback
struct NendUnityNativeAdCallback_t2120700280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Platform.iOS.IOSNativeAdClient
struct  IOSNativeAdClient_t1155913684  : public NativeAdClient_t772434093
{
public:
	// System.IntPtr NendUnityPlugin.Platform.iOS.IOSNativeAdClient::m_ClientPtr
	IntPtr_t ___m_ClientPtr_6;

public:
	inline static int32_t get_offset_of_m_ClientPtr_6() { return static_cast<int32_t>(offsetof(IOSNativeAdClient_t1155913684, ___m_ClientPtr_6)); }
	inline IntPtr_t get_m_ClientPtr_6() const { return ___m_ClientPtr_6; }
	inline IntPtr_t* get_address_of_m_ClientPtr_6() { return &___m_ClientPtr_6; }
	inline void set_m_ClientPtr_6(IntPtr_t value)
	{
		___m_ClientPtr_6 = value;
	}
};

struct IOSNativeAdClient_t1155913684_StaticFields
{
public:
	// NendUnityPlugin.Platform.iOS.IOSNativeAdClient/NendUnityNativeAdCallback NendUnityPlugin.Platform.iOS.IOSNativeAdClient::<>f__mg$cache0
	NendUnityNativeAdCallback_t2120700280 * ___U3CU3Ef__mgU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(IOSNativeAdClient_t1155913684_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline NendUnityNativeAdCallback_t2120700280 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline NendUnityNativeAdCallback_t2120700280 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(NendUnityNativeAdCallback_t2120700280 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
