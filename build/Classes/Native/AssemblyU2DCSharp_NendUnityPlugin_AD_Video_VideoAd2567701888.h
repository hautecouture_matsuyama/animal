﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV2883267092.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Video.VideoAdCallbackArgments
struct  VideoAdCallbackArgments_t2567701888  : public Il2CppObject
{
public:
	// NendUnityPlugin.AD.Video.NendAdVideo/VideoAdCallbackType NendUnityPlugin.AD.Video.VideoAdCallbackArgments::videoAdCallbackType
	int32_t ___videoAdCallbackType_0;

public:
	inline static int32_t get_offset_of_videoAdCallbackType_0() { return static_cast<int32_t>(offsetof(VideoAdCallbackArgments_t2567701888, ___videoAdCallbackType_0)); }
	inline int32_t get_videoAdCallbackType_0() const { return ___videoAdCallbackType_0; }
	inline int32_t* get_address_of_videoAdCallbackType_0() { return &___videoAdCallbackType_0; }
	inline void set_videoAdCallbackType_0(int32_t value)
	{
		___videoAdCallbackType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
