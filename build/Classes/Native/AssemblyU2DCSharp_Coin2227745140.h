﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Transform
struct Transform_t3600365921;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Coin
struct  Coin_t2227745140  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Coin::inTrain
	bool ___inTrain_2;
	// System.Boolean Coin::collided
	bool ___collided_3;
	// System.Boolean Coin::inObsUp
	bool ___inObsUp_4;
	// UnityEngine.AudioClip Coin::coinPickClip
	AudioClip_t3680889665 * ___coinPickClip_5;
	// UnityEngine.AudioSource Coin::coinPickSource
	AudioSource_t3935305588 * ___coinPickSource_6;
	// UnityEngine.Transform Coin::sideCheck
	Transform_t3600365921 * ___sideCheck_7;

public:
	inline static int32_t get_offset_of_inTrain_2() { return static_cast<int32_t>(offsetof(Coin_t2227745140, ___inTrain_2)); }
	inline bool get_inTrain_2() const { return ___inTrain_2; }
	inline bool* get_address_of_inTrain_2() { return &___inTrain_2; }
	inline void set_inTrain_2(bool value)
	{
		___inTrain_2 = value;
	}

	inline static int32_t get_offset_of_collided_3() { return static_cast<int32_t>(offsetof(Coin_t2227745140, ___collided_3)); }
	inline bool get_collided_3() const { return ___collided_3; }
	inline bool* get_address_of_collided_3() { return &___collided_3; }
	inline void set_collided_3(bool value)
	{
		___collided_3 = value;
	}

	inline static int32_t get_offset_of_inObsUp_4() { return static_cast<int32_t>(offsetof(Coin_t2227745140, ___inObsUp_4)); }
	inline bool get_inObsUp_4() const { return ___inObsUp_4; }
	inline bool* get_address_of_inObsUp_4() { return &___inObsUp_4; }
	inline void set_inObsUp_4(bool value)
	{
		___inObsUp_4 = value;
	}

	inline static int32_t get_offset_of_coinPickClip_5() { return static_cast<int32_t>(offsetof(Coin_t2227745140, ___coinPickClip_5)); }
	inline AudioClip_t3680889665 * get_coinPickClip_5() const { return ___coinPickClip_5; }
	inline AudioClip_t3680889665 ** get_address_of_coinPickClip_5() { return &___coinPickClip_5; }
	inline void set_coinPickClip_5(AudioClip_t3680889665 * value)
	{
		___coinPickClip_5 = value;
		Il2CppCodeGenWriteBarrier(&___coinPickClip_5, value);
	}

	inline static int32_t get_offset_of_coinPickSource_6() { return static_cast<int32_t>(offsetof(Coin_t2227745140, ___coinPickSource_6)); }
	inline AudioSource_t3935305588 * get_coinPickSource_6() const { return ___coinPickSource_6; }
	inline AudioSource_t3935305588 ** get_address_of_coinPickSource_6() { return &___coinPickSource_6; }
	inline void set_coinPickSource_6(AudioSource_t3935305588 * value)
	{
		___coinPickSource_6 = value;
		Il2CppCodeGenWriteBarrier(&___coinPickSource_6, value);
	}

	inline static int32_t get_offset_of_sideCheck_7() { return static_cast<int32_t>(offsetof(Coin_t2227745140, ___sideCheck_7)); }
	inline Transform_t3600365921 * get_sideCheck_7() const { return ___sideCheck_7; }
	inline Transform_t3600365921 ** get_address_of_sideCheck_7() { return &___sideCheck_7; }
	inline void set_sideCheck_7(Transform_t3600365921 * value)
	{
		___sideCheck_7 = value;
		Il2CppCodeGenWriteBarrier(&___sideCheck_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
