﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste1003666588.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1076084509.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3867320123.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3344766165.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger55832929.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve3484638744.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1216237838.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM2536340562.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3360306849.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3495933518.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD2331243652.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv4171500731.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3903027533.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3807901092.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3704011348.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3039385657.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInput3630163547.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM2019268878.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3453173740.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInpu857139936.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInpu384203932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3190347560.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone2760469101.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone3382566315.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInput4248229598.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas4150874583.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DR3382992964.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRayc437419520.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT809614380.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color1000778859.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color1121741130.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float1274330004.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float1856710240.h"
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggers2532145056.h"
#include "UnityEngine_UI_UnityEngine_UI_Button4055032469.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClickedEv48803504.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_U3COnFinishSu3413438900.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate2572322932.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistry2720824592.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock2139031574.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls4098465386.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls_Reso1597885468.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown2274391225.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownIte1451952895.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData3270282352.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionDataL1438173104.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEve4040729994.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CShowU3Ec1106527198.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CDelayedD3853912249.h"
#include "UnityEngine_UI_UnityEngine_UI_FontData746620069.h"
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTracker1839077620.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic1660335611.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster2999697109.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_Bloc612090948.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRegistry3479976429.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2670269651.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Type1152881528.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod1167457570.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginHorizont1174417785.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginVertical2256455259.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin901855765812.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin180325369132.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin360707706162.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField3762917431.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1787303396.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType1770400679.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_Character4051914437.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType4214648469.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnValidat2355412304.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_SubmitEven648412432.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnChangeEv467195904.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState3741896775.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CCaretB2589889038.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CMouseD3909241878.h"
#include "UnityEngine_UI_UnityEngine_UI_Mask1803652131.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic3839221559.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic_Cull3661388177.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskUtilities4151184739.h"
#include "UnityEngine_UI_UnityEngine_UI_Misc1447421763.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation3049316579.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode1066900953.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage3182918964.h"
#include "UnityEngine_UI_UnityEngine_UI_RectMask2D3474889437.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar1494447233.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction3470714353.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (EventSystem_t1003666588), -1, sizeof(EventSystem_t1003666588_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1607[12] = 
{
	EventSystem_t1003666588::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t1003666588::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t1003666588_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t1003666588::get_offset_of_m_FirstSelected_5(),
	EventSystem_t1003666588::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t1003666588::get_offset_of_m_DragThreshold_7(),
	EventSystem_t1003666588::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t1003666588::get_offset_of_m_HasFocus_9(),
	EventSystem_t1003666588::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t1003666588::get_offset_of_m_DummyData_11(),
	EventSystem_t1003666588_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t1003666588_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (EventTrigger_t1076084509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1608[2] = 
{
	EventTrigger_t1076084509::get_offset_of_m_Delegates_2(),
	EventTrigger_t1076084509::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (TriggerEvent_t3867320123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (Entry_t3344766165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1610[2] = 
{
	Entry_t3344766165::get_offset_of_eventID_0(),
	Entry_t3344766165::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (EventTriggerType_t55832929)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1611[18] = 
{
	EventTriggerType_t55832929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (ExecuteEvents_t3484638744), -1, sizeof(ExecuteEvents_t3484638744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1612[36] = 
{
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (MoveDirection_t1216237838)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1614[6] = 
{
	MoveDirection_t1216237838::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (RaycasterManager_t2536340562), -1, sizeof(RaycasterManager_t2536340562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1615[1] = 
{
	RaycasterManager_t2536340562_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (RaycastResult_t3360306849)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1616[10] = 
{
	RaycastResult_t3360306849::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (UIBehaviour_t3495933518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (AxisEventData_t2331243652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1618[2] = 
{
	AxisEventData_t2331243652::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t2331243652::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (AbstractEventData_t4171500731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1619[1] = 
{
	AbstractEventData_t4171500731::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (BaseEventData_t3903027533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1620[1] = 
{
	BaseEventData_t3903027533::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (PointerEventData_t3807901092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1621[21] = 
{
	PointerEventData_t3807901092::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t3807901092::get_offset_of_m_PointerPress_3(),
	PointerEventData_t3807901092::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t3807901092::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t3807901092::get_offset_of_hovered_9(),
	PointerEventData_t3807901092::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t3807901092::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t3807901092::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t3807901092::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t3807901092::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t3807901092::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t3807901092::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t3807901092::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t3807901092::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t3807901092::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t3807901092::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t3807901092::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (InputButton_t3704011348)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1622[4] = 
{
	InputButton_t3704011348::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (FramePressState_t3039385657)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1623[5] = 
{
	FramePressState_t3039385657::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (BaseInput_t3630163547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (BaseInputModule_t2019268878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1625[6] = 
{
	BaseInputModule_t2019268878::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t2019268878::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t2019268878::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t2019268878::get_offset_of_m_BaseEventData_5(),
	BaseInputModule_t2019268878::get_offset_of_m_InputOverride_6(),
	BaseInputModule_t2019268878::get_offset_of_m_DefaultInput_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (PointerInputModule_t3453173740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1626[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t3453173740::get_offset_of_m_PointerData_12(),
	PointerInputModule_t3453173740::get_offset_of_m_MouseState_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (ButtonState_t857139936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1627[2] = 
{
	ButtonState_t857139936::get_offset_of_m_Button_0(),
	ButtonState_t857139936::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (MouseState_t384203932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[1] = 
{
	MouseState_t384203932::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (MouseButtonEventData_t3190347560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[2] = 
{
	MouseButtonEventData_t3190347560::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3190347560::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (StandaloneInputModule_t2760469101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1630[13] = 
{
	StandaloneInputModule_t2760469101::get_offset_of_m_PrevActionTime_14(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMoveVector_15(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ConsecutiveMoveCount_16(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMousePosition_17(),
	StandaloneInputModule_t2760469101::get_offset_of_m_MousePosition_18(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CurrentFocusedGameObject_19(),
	StandaloneInputModule_t2760469101::get_offset_of_m_HorizontalAxis_20(),
	StandaloneInputModule_t2760469101::get_offset_of_m_VerticalAxis_21(),
	StandaloneInputModule_t2760469101::get_offset_of_m_SubmitButton_22(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CancelButton_23(),
	StandaloneInputModule_t2760469101::get_offset_of_m_InputActionsPerSecond_24(),
	StandaloneInputModule_t2760469101::get_offset_of_m_RepeatDelay_25(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ForceModuleActive_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (InputMode_t3382566315)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1631[3] = 
{
	InputMode_t3382566315::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (TouchInputModule_t4248229598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1632[3] = 
{
	TouchInputModule_t4248229598::get_offset_of_m_LastMousePosition_14(),
	TouchInputModule_t4248229598::get_offset_of_m_MousePosition_15(),
	TouchInputModule_t4248229598::get_offset_of_m_ForceModuleActive_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (BaseRaycaster_t4150874583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (Physics2DRaycaster_t3382992964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (PhysicsRaycaster_t437419520), -1, sizeof(PhysicsRaycaster_t437419520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1635[4] = 
{
	0,
	PhysicsRaycaster_t437419520::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t437419520::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t437419520_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (ColorTween_t809614380)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1637[6] = 
{
	ColorTween_t809614380::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (ColorTweenMode_t1000778859)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1638[4] = 
{
	ColorTweenMode_t1000778859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (ColorTweenCallback_t1121741130), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (FloatTween_t1274330004)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1640[5] = 
{
	FloatTween_t1274330004::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (FloatTweenCallback_t1856710240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1642[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1643[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (AnimationTriggers_t2532145056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1644[8] = 
{
	0,
	0,
	0,
	0,
	AnimationTriggers_t2532145056::get_offset_of_m_NormalTrigger_4(),
	AnimationTriggers_t2532145056::get_offset_of_m_HighlightedTrigger_5(),
	AnimationTriggers_t2532145056::get_offset_of_m_PressedTrigger_6(),
	AnimationTriggers_t2532145056::get_offset_of_m_DisabledTrigger_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (Button_t4055032469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1645[1] = 
{
	Button_t4055032469::get_offset_of_m_OnClick_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (ButtonClickedEvent_t48803504), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (U3COnFinishSubmitU3Ec__Iterator0_t3413438900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1647[6] = 
{
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U3CfadeTimeU3E__0_0(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U3CelapsedTimeU3E__0_1(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24this_2(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24current_3(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24disposing_4(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (CanvasUpdate_t2572322932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1648[7] = 
{
	CanvasUpdate_t2572322932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (CanvasUpdateRegistry_t2720824592), -1, sizeof(CanvasUpdateRegistry_t2720824592_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1650[7] = 
{
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_s_Instance_0(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_PerformingLayoutUpdate_1(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_PerformingGraphicUpdate_2(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_LayoutRebuildQueue_3(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_GraphicRebuildQueue_4(),
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_s_SortLayoutFunction_5(),
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (ColorBlock_t2139031574)+ sizeof (Il2CppObject), sizeof(ColorBlock_t2139031574 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1651[6] = 
{
	ColorBlock_t2139031574::get_offset_of_m_NormalColor_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_HighlightedColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_PressedColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_DisabledColor_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_ColorMultiplier_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_FadeDuration_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (DefaultControls_t4098465386), -1, sizeof(DefaultControls_t4098465386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1652[9] = 
{
	0,
	0,
	0,
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ThickElementSize_3(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ThinElementSize_4(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ImageElementSize_5(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_DefaultSelectableColor_6(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_PanelColor_7(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_TextColor_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (Resources_t1597885468)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[7] = 
{
	Resources_t1597885468::get_offset_of_standard_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_background_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_inputField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_knob_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_checkmark_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_dropdown_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_mask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (Dropdown_t2274391225), -1, sizeof(Dropdown_t2274391225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1654[14] = 
{
	Dropdown_t2274391225::get_offset_of_m_Template_16(),
	Dropdown_t2274391225::get_offset_of_m_CaptionText_17(),
	Dropdown_t2274391225::get_offset_of_m_CaptionImage_18(),
	Dropdown_t2274391225::get_offset_of_m_ItemText_19(),
	Dropdown_t2274391225::get_offset_of_m_ItemImage_20(),
	Dropdown_t2274391225::get_offset_of_m_Value_21(),
	Dropdown_t2274391225::get_offset_of_m_Options_22(),
	Dropdown_t2274391225::get_offset_of_m_OnValueChanged_23(),
	Dropdown_t2274391225::get_offset_of_m_Dropdown_24(),
	Dropdown_t2274391225::get_offset_of_m_Blocker_25(),
	Dropdown_t2274391225::get_offset_of_m_Items_26(),
	Dropdown_t2274391225::get_offset_of_m_AlphaTweenRunner_27(),
	Dropdown_t2274391225::get_offset_of_validTemplate_28(),
	Dropdown_t2274391225_StaticFields::get_offset_of_s_NoOptionData_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (DropdownItem_t1451952895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1655[4] = 
{
	DropdownItem_t1451952895::get_offset_of_m_Text_2(),
	DropdownItem_t1451952895::get_offset_of_m_Image_3(),
	DropdownItem_t1451952895::get_offset_of_m_RectTransform_4(),
	DropdownItem_t1451952895::get_offset_of_m_Toggle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (OptionData_t3270282352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[2] = 
{
	OptionData_t3270282352::get_offset_of_m_Text_0(),
	OptionData_t3270282352::get_offset_of_m_Image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (OptionDataList_t1438173104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1657[1] = 
{
	OptionDataList_t1438173104::get_offset_of_m_Options_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (DropdownEvent_t4040729994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (U3CShowU3Ec__AnonStorey1_t1106527198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1659[2] = 
{
	U3CShowU3Ec__AnonStorey1_t1106527198::get_offset_of_item_0(),
	U3CShowU3Ec__AnonStorey1_t1106527198::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1660[5] = 
{
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_delay_0(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24this_1(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24current_2(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24disposing_3(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (FontData_t746620069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1661[12] = 
{
	FontData_t746620069::get_offset_of_m_Font_0(),
	FontData_t746620069::get_offset_of_m_FontSize_1(),
	FontData_t746620069::get_offset_of_m_FontStyle_2(),
	FontData_t746620069::get_offset_of_m_BestFit_3(),
	FontData_t746620069::get_offset_of_m_MinSize_4(),
	FontData_t746620069::get_offset_of_m_MaxSize_5(),
	FontData_t746620069::get_offset_of_m_Alignment_6(),
	FontData_t746620069::get_offset_of_m_AlignByGeometry_7(),
	FontData_t746620069::get_offset_of_m_RichText_8(),
	FontData_t746620069::get_offset_of_m_HorizontalOverflow_9(),
	FontData_t746620069::get_offset_of_m_VerticalOverflow_10(),
	FontData_t746620069::get_offset_of_m_LineSpacing_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (FontUpdateTracker_t1839077620), -1, sizeof(FontUpdateTracker_t1839077620_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1662[3] = 
{
	FontUpdateTracker_t1839077620_StaticFields::get_offset_of_m_Tracked_0(),
	FontUpdateTracker_t1839077620_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
	FontUpdateTracker_t1839077620_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (Graphic_t1660335611), -1, sizeof(Graphic_t1660335611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1663[17] = 
{
	Graphic_t1660335611_StaticFields::get_offset_of_s_DefaultUI_2(),
	Graphic_t1660335611_StaticFields::get_offset_of_s_WhiteTexture_3(),
	Graphic_t1660335611::get_offset_of_m_Material_4(),
	Graphic_t1660335611::get_offset_of_m_Color_5(),
	Graphic_t1660335611::get_offset_of_m_RaycastTarget_6(),
	Graphic_t1660335611::get_offset_of_m_RectTransform_7(),
	Graphic_t1660335611::get_offset_of_m_CanvasRender_8(),
	Graphic_t1660335611::get_offset_of_m_Canvas_9(),
	Graphic_t1660335611::get_offset_of_m_VertsDirty_10(),
	Graphic_t1660335611::get_offset_of_m_MaterialDirty_11(),
	Graphic_t1660335611::get_offset_of_m_OnDirtyLayoutCallback_12(),
	Graphic_t1660335611::get_offset_of_m_OnDirtyVertsCallback_13(),
	Graphic_t1660335611::get_offset_of_m_OnDirtyMaterialCallback_14(),
	Graphic_t1660335611_StaticFields::get_offset_of_s_Mesh_15(),
	Graphic_t1660335611_StaticFields::get_offset_of_s_VertexHelper_16(),
	Graphic_t1660335611::get_offset_of_m_ColorTweenRunner_17(),
	Graphic_t1660335611::get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (GraphicRaycaster_t2999697109), -1, sizeof(GraphicRaycaster_t2999697109_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1664[8] = 
{
	0,
	GraphicRaycaster_t2999697109::get_offset_of_m_IgnoreReversedGraphics_3(),
	GraphicRaycaster_t2999697109::get_offset_of_m_BlockingObjects_4(),
	GraphicRaycaster_t2999697109::get_offset_of_m_BlockingMask_5(),
	GraphicRaycaster_t2999697109::get_offset_of_m_Canvas_6(),
	GraphicRaycaster_t2999697109::get_offset_of_m_RaycastResults_7(),
	GraphicRaycaster_t2999697109_StaticFields::get_offset_of_s_SortedGraphics_8(),
	GraphicRaycaster_t2999697109_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (BlockingObjects_t612090948)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1665[5] = 
{
	BlockingObjects_t612090948::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (GraphicRegistry_t3479976429), -1, sizeof(GraphicRegistry_t3479976429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1666[3] = 
{
	GraphicRegistry_t3479976429_StaticFields::get_offset_of_s_Instance_0(),
	GraphicRegistry_t3479976429::get_offset_of_m_Graphics_1(),
	GraphicRegistry_t3479976429_StaticFields::get_offset_of_s_EmptyList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (Image_t2670269651), -1, sizeof(Image_t2670269651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1668[15] = 
{
	Image_t2670269651_StaticFields::get_offset_of_s_ETC1DefaultUI_28(),
	Image_t2670269651::get_offset_of_m_Sprite_29(),
	Image_t2670269651::get_offset_of_m_OverrideSprite_30(),
	Image_t2670269651::get_offset_of_m_Type_31(),
	Image_t2670269651::get_offset_of_m_PreserveAspect_32(),
	Image_t2670269651::get_offset_of_m_FillCenter_33(),
	Image_t2670269651::get_offset_of_m_FillMethod_34(),
	Image_t2670269651::get_offset_of_m_FillAmount_35(),
	Image_t2670269651::get_offset_of_m_FillClockwise_36(),
	Image_t2670269651::get_offset_of_m_FillOrigin_37(),
	Image_t2670269651::get_offset_of_m_AlphaHitTestMinimumThreshold_38(),
	Image_t2670269651_StaticFields::get_offset_of_s_VertScratch_39(),
	Image_t2670269651_StaticFields::get_offset_of_s_UVScratch_40(),
	Image_t2670269651_StaticFields::get_offset_of_s_Xy_41(),
	Image_t2670269651_StaticFields::get_offset_of_s_Uv_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (Type_t1152881528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1669[5] = 
{
	Type_t1152881528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (FillMethod_t1167457570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1670[6] = 
{
	FillMethod_t1167457570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (OriginHorizontal_t1174417785)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1671[3] = 
{
	OriginHorizontal_t1174417785::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (OriginVertical_t2256455259)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1672[3] = 
{
	OriginVertical_t2256455259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (Origin90_t1855765812)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1673[5] = 
{
	Origin90_t1855765812::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (Origin180_t325369132)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1674[5] = 
{
	Origin180_t325369132::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (Origin360_t707706162)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1675[5] = 
{
	Origin360_t707706162::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (InputField_t3762917431), -1, sizeof(InputField_t3762917431_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1678[47] = 
{
	InputField_t3762917431::get_offset_of_m_Keyboard_16(),
	InputField_t3762917431_StaticFields::get_offset_of_kSeparators_17(),
	InputField_t3762917431::get_offset_of_m_TextComponent_18(),
	InputField_t3762917431::get_offset_of_m_Placeholder_19(),
	InputField_t3762917431::get_offset_of_m_ContentType_20(),
	InputField_t3762917431::get_offset_of_m_InputType_21(),
	InputField_t3762917431::get_offset_of_m_AsteriskChar_22(),
	InputField_t3762917431::get_offset_of_m_KeyboardType_23(),
	InputField_t3762917431::get_offset_of_m_LineType_24(),
	InputField_t3762917431::get_offset_of_m_HideMobileInput_25(),
	InputField_t3762917431::get_offset_of_m_CharacterValidation_26(),
	InputField_t3762917431::get_offset_of_m_CharacterLimit_27(),
	InputField_t3762917431::get_offset_of_m_OnEndEdit_28(),
	InputField_t3762917431::get_offset_of_m_OnValueChanged_29(),
	InputField_t3762917431::get_offset_of_m_OnValidateInput_30(),
	InputField_t3762917431::get_offset_of_m_CaretColor_31(),
	InputField_t3762917431::get_offset_of_m_CustomCaretColor_32(),
	InputField_t3762917431::get_offset_of_m_SelectionColor_33(),
	InputField_t3762917431::get_offset_of_m_Text_34(),
	InputField_t3762917431::get_offset_of_m_CaretBlinkRate_35(),
	InputField_t3762917431::get_offset_of_m_CaretWidth_36(),
	InputField_t3762917431::get_offset_of_m_ReadOnly_37(),
	InputField_t3762917431::get_offset_of_m_CaretPosition_38(),
	InputField_t3762917431::get_offset_of_m_CaretSelectPosition_39(),
	InputField_t3762917431::get_offset_of_caretRectTrans_40(),
	InputField_t3762917431::get_offset_of_m_CursorVerts_41(),
	InputField_t3762917431::get_offset_of_m_InputTextCache_42(),
	InputField_t3762917431::get_offset_of_m_CachedInputRenderer_43(),
	InputField_t3762917431::get_offset_of_m_PreventFontCallback_44(),
	InputField_t3762917431::get_offset_of_m_Mesh_45(),
	InputField_t3762917431::get_offset_of_m_AllowInput_46(),
	InputField_t3762917431::get_offset_of_m_ShouldActivateNextUpdate_47(),
	InputField_t3762917431::get_offset_of_m_UpdateDrag_48(),
	InputField_t3762917431::get_offset_of_m_DragPositionOutOfBounds_49(),
	0,
	0,
	InputField_t3762917431::get_offset_of_m_CaretVisible_52(),
	InputField_t3762917431::get_offset_of_m_BlinkCoroutine_53(),
	InputField_t3762917431::get_offset_of_m_BlinkStartTime_54(),
	InputField_t3762917431::get_offset_of_m_DrawStart_55(),
	InputField_t3762917431::get_offset_of_m_DrawEnd_56(),
	InputField_t3762917431::get_offset_of_m_DragCoroutine_57(),
	InputField_t3762917431::get_offset_of_m_OriginalText_58(),
	InputField_t3762917431::get_offset_of_m_WasCanceled_59(),
	InputField_t3762917431::get_offset_of_m_HasDoneFocusTransition_60(),
	0,
	InputField_t3762917431::get_offset_of_m_ProcessingEvent_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (ContentType_t1787303396)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1679[11] = 
{
	ContentType_t1787303396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (InputType_t1770400679)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1680[4] = 
{
	InputType_t1770400679::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (CharacterValidation_t4051914437)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1681[7] = 
{
	CharacterValidation_t4051914437::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (LineType_t4214648469)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1682[4] = 
{
	LineType_t4214648469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (OnValidateInput_t2355412304), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (SubmitEvent_t648412432), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (OnChangeEvent_t467195904), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (EditState_t3741896775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1686[3] = 
{
	EditState_t3741896775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (U3CCaretBlinkU3Ec__Iterator0_t2589889038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1687[6] = 
{
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U3CblinkPeriodU3E__1_0(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U3CblinkStateU3E__1_1(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24this_2(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24current_3(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24disposing_4(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1688[8] = 
{
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_eventData_0(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U3ClocalMousePosU3E__1_1(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U3CrectU3E__1_2(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U3CdelayU3E__1_3(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24this_4(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24current_5(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24disposing_6(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (Mask_t1803652131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1689[5] = 
{
	Mask_t1803652131::get_offset_of_m_RectTransform_2(),
	Mask_t1803652131::get_offset_of_m_ShowMaskGraphic_3(),
	Mask_t1803652131::get_offset_of_m_Graphic_4(),
	Mask_t1803652131::get_offset_of_m_MaskMaterial_5(),
	Mask_t1803652131::get_offset_of_m_UnmaskMaterial_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (MaskableGraphic_t3839221559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1690[9] = 
{
	MaskableGraphic_t3839221559::get_offset_of_m_ShouldRecalculateStencil_19(),
	MaskableGraphic_t3839221559::get_offset_of_m_MaskMaterial_20(),
	MaskableGraphic_t3839221559::get_offset_of_m_ParentMask_21(),
	MaskableGraphic_t3839221559::get_offset_of_m_Maskable_22(),
	MaskableGraphic_t3839221559::get_offset_of_m_IncludeForMasking_23(),
	MaskableGraphic_t3839221559::get_offset_of_m_OnCullStateChanged_24(),
	MaskableGraphic_t3839221559::get_offset_of_m_ShouldRecalculate_25(),
	MaskableGraphic_t3839221559::get_offset_of_m_StencilValue_26(),
	MaskableGraphic_t3839221559::get_offset_of_m_Corners_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (CullStateChangedEvent_t3661388177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (MaskUtilities_t4151184739), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (Misc_t1447421763), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (Navigation_t3049316579)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[5] = 
{
	Navigation_t3049316579::get_offset_of_m_Mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnUp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (Mode_t1066900953)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1695[6] = 
{
	Mode_t1066900953::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (RawImage_t3182918964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1696[2] = 
{
	RawImage_t3182918964::get_offset_of_m_Texture_28(),
	RawImage_t3182918964::get_offset_of_m_UVRect_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (RectMask2D_t3474889437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1697[8] = 
{
	RectMask2D_t3474889437::get_offset_of_m_VertexClipper_2(),
	RectMask2D_t3474889437::get_offset_of_m_RectTransform_3(),
	RectMask2D_t3474889437::get_offset_of_m_ClipTargets_4(),
	RectMask2D_t3474889437::get_offset_of_m_ShouldRecalculateClipRects_5(),
	RectMask2D_t3474889437::get_offset_of_m_Clippers_6(),
	RectMask2D_t3474889437::get_offset_of_m_LastClipRectCanvasSpace_7(),
	RectMask2D_t3474889437::get_offset_of_m_LastValidClipRect_8(),
	RectMask2D_t3474889437::get_offset_of_m_ForceClip_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (Scrollbar_t1494447233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1698[11] = 
{
	Scrollbar_t1494447233::get_offset_of_m_HandleRect_16(),
	Scrollbar_t1494447233::get_offset_of_m_Direction_17(),
	Scrollbar_t1494447233::get_offset_of_m_Value_18(),
	Scrollbar_t1494447233::get_offset_of_m_Size_19(),
	Scrollbar_t1494447233::get_offset_of_m_NumberOfSteps_20(),
	Scrollbar_t1494447233::get_offset_of_m_OnValueChanged_21(),
	Scrollbar_t1494447233::get_offset_of_m_ContainerRect_22(),
	Scrollbar_t1494447233::get_offset_of_m_Offset_23(),
	Scrollbar_t1494447233::get_offset_of_m_Tracker_24(),
	Scrollbar_t1494447233::get_offset_of_m_PointerDownRepeat_25(),
	Scrollbar_t1494447233::get_offset_of_isPointerDownAndNotDragging_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (Direction_t3470714353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1699[5] = 
{
	Direction_t3470714353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
