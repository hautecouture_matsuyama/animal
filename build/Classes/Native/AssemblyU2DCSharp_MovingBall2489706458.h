﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MovingBall
struct  MovingBall_t2489706458  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MovingBall::sign
	float ___sign_2;
	// System.Int32 MovingBall::nowTime
	int32_t ___nowTime_3;
	// System.Int16 MovingBall::rotateSpeed
	int16_t ___rotateSpeed_5;
	// System.Single MovingBall::moveSpeed
	float ___moveSpeed_6;
	// UnityEngine.GameObject MovingBall::soccerBall
	GameObject_t1113636619 * ___soccerBall_7;

public:
	inline static int32_t get_offset_of_sign_2() { return static_cast<int32_t>(offsetof(MovingBall_t2489706458, ___sign_2)); }
	inline float get_sign_2() const { return ___sign_2; }
	inline float* get_address_of_sign_2() { return &___sign_2; }
	inline void set_sign_2(float value)
	{
		___sign_2 = value;
	}

	inline static int32_t get_offset_of_nowTime_3() { return static_cast<int32_t>(offsetof(MovingBall_t2489706458, ___nowTime_3)); }
	inline int32_t get_nowTime_3() const { return ___nowTime_3; }
	inline int32_t* get_address_of_nowTime_3() { return &___nowTime_3; }
	inline void set_nowTime_3(int32_t value)
	{
		___nowTime_3 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_5() { return static_cast<int32_t>(offsetof(MovingBall_t2489706458, ___rotateSpeed_5)); }
	inline int16_t get_rotateSpeed_5() const { return ___rotateSpeed_5; }
	inline int16_t* get_address_of_rotateSpeed_5() { return &___rotateSpeed_5; }
	inline void set_rotateSpeed_5(int16_t value)
	{
		___rotateSpeed_5 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_6() { return static_cast<int32_t>(offsetof(MovingBall_t2489706458, ___moveSpeed_6)); }
	inline float get_moveSpeed_6() const { return ___moveSpeed_6; }
	inline float* get_address_of_moveSpeed_6() { return &___moveSpeed_6; }
	inline void set_moveSpeed_6(float value)
	{
		___moveSpeed_6 = value;
	}

	inline static int32_t get_offset_of_soccerBall_7() { return static_cast<int32_t>(offsetof(MovingBall_t2489706458, ___soccerBall_7)); }
	inline GameObject_t1113636619 * get_soccerBall_7() const { return ___soccerBall_7; }
	inline GameObject_t1113636619 ** get_address_of_soccerBall_7() { return &___soccerBall_7; }
	inline void set_soccerBall_7(GameObject_t1113636619 * value)
	{
		___soccerBall_7 = value;
		Il2CppCodeGenWriteBarrier(&___soccerBall_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
