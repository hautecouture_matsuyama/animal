﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// NendUnityPlugin.AD.Native.NendAdNativeView
struct NendAdNativeView_t2014609480;
// NendUnityPlugin.AD.Native.NendAdNative
struct NendAdNative_t3381739006;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey2
struct  U3CLoadAdU3Ec__AnonStorey2_t1337685978  : public Il2CppObject
{
public:
	// NendUnityPlugin.AD.Native.NendAdNativeView NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey2::nativeAdView
	NendAdNativeView_t2014609480 * ___nativeAdView_0;
	// NendUnityPlugin.AD.Native.NendAdNative NendUnityPlugin.AD.Native.NendAdNative/<LoadAd>c__AnonStorey2::$this
	NendAdNative_t3381739006 * ___U24this_1;

public:
	inline static int32_t get_offset_of_nativeAdView_0() { return static_cast<int32_t>(offsetof(U3CLoadAdU3Ec__AnonStorey2_t1337685978, ___nativeAdView_0)); }
	inline NendAdNativeView_t2014609480 * get_nativeAdView_0() const { return ___nativeAdView_0; }
	inline NendAdNativeView_t2014609480 ** get_address_of_nativeAdView_0() { return &___nativeAdView_0; }
	inline void set_nativeAdView_0(NendAdNativeView_t2014609480 * value)
	{
		___nativeAdView_0 = value;
		Il2CppCodeGenWriteBarrier(&___nativeAdView_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadAdU3Ec__AnonStorey2_t1337685978, ___U24this_1)); }
	inline NendAdNative_t3381739006 * get_U24this_1() const { return ___U24this_1; }
	inline NendAdNative_t3381739006 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(NendAdNative_t3381739006 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
