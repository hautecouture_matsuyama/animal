﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String
struct String_t;
// NendUnityPlugin.AD.NendAdInterstitial
struct NendAdInterstitial_t1417649638;
// NendUnityPlugin.Platform.NendAdInterstitialInterface
struct NendAdInterstitialInterface_t638097589;
// System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs>
struct EventHandler_1_t3479984044;
// System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialClickEventArgs>
struct EventHandler_1_t3193289641;
// System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialShowEventArgs>
struct EventHandler_1_t1007381714;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.NendAdInterstitial
struct  NendAdInterstitial_t1417649638  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean NendUnityPlugin.AD.NendAdInterstitial::outputLog
	bool ___outputLog_2;
	// System.String NendUnityPlugin.AD.NendAdInterstitial::android_key
	String_t* ___android_key_3;
	// System.String NendUnityPlugin.AD.NendAdInterstitial::android_id
	String_t* ___android_id_4;
	// System.String NendUnityPlugin.AD.NendAdInterstitial::ios_key
	String_t* ___ios_key_5;
	// System.String NendUnityPlugin.AD.NendAdInterstitial::ios_id
	String_t* ___ios_id_6;
	// System.Boolean NendUnityPlugin.AD.NendAdInterstitial::m_IsAutoReloadEnabled
	bool ___m_IsAutoReloadEnabled_8;
	// NendUnityPlugin.Platform.NendAdInterstitialInterface NendUnityPlugin.AD.NendAdInterstitial::_interface
	Il2CppObject * ____interface_9;
	// System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialLoadEventArgs> NendUnityPlugin.AD.NendAdInterstitial::AdLoaded
	EventHandler_1_t3479984044 * ___AdLoaded_10;
	// System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialClickEventArgs> NendUnityPlugin.AD.NendAdInterstitial::AdClicked
	EventHandler_1_t3193289641 * ___AdClicked_11;
	// System.EventHandler`1<NendUnityPlugin.AD.NendAdInterstitialShowEventArgs> NendUnityPlugin.AD.NendAdInterstitial::AdShown
	EventHandler_1_t1007381714 * ___AdShown_12;

public:
	inline static int32_t get_offset_of_outputLog_2() { return static_cast<int32_t>(offsetof(NendAdInterstitial_t1417649638, ___outputLog_2)); }
	inline bool get_outputLog_2() const { return ___outputLog_2; }
	inline bool* get_address_of_outputLog_2() { return &___outputLog_2; }
	inline void set_outputLog_2(bool value)
	{
		___outputLog_2 = value;
	}

	inline static int32_t get_offset_of_android_key_3() { return static_cast<int32_t>(offsetof(NendAdInterstitial_t1417649638, ___android_key_3)); }
	inline String_t* get_android_key_3() const { return ___android_key_3; }
	inline String_t** get_address_of_android_key_3() { return &___android_key_3; }
	inline void set_android_key_3(String_t* value)
	{
		___android_key_3 = value;
		Il2CppCodeGenWriteBarrier(&___android_key_3, value);
	}

	inline static int32_t get_offset_of_android_id_4() { return static_cast<int32_t>(offsetof(NendAdInterstitial_t1417649638, ___android_id_4)); }
	inline String_t* get_android_id_4() const { return ___android_id_4; }
	inline String_t** get_address_of_android_id_4() { return &___android_id_4; }
	inline void set_android_id_4(String_t* value)
	{
		___android_id_4 = value;
		Il2CppCodeGenWriteBarrier(&___android_id_4, value);
	}

	inline static int32_t get_offset_of_ios_key_5() { return static_cast<int32_t>(offsetof(NendAdInterstitial_t1417649638, ___ios_key_5)); }
	inline String_t* get_ios_key_5() const { return ___ios_key_5; }
	inline String_t** get_address_of_ios_key_5() { return &___ios_key_5; }
	inline void set_ios_key_5(String_t* value)
	{
		___ios_key_5 = value;
		Il2CppCodeGenWriteBarrier(&___ios_key_5, value);
	}

	inline static int32_t get_offset_of_ios_id_6() { return static_cast<int32_t>(offsetof(NendAdInterstitial_t1417649638, ___ios_id_6)); }
	inline String_t* get_ios_id_6() const { return ___ios_id_6; }
	inline String_t** get_address_of_ios_id_6() { return &___ios_id_6; }
	inline void set_ios_id_6(String_t* value)
	{
		___ios_id_6 = value;
		Il2CppCodeGenWriteBarrier(&___ios_id_6, value);
	}

	inline static int32_t get_offset_of_m_IsAutoReloadEnabled_8() { return static_cast<int32_t>(offsetof(NendAdInterstitial_t1417649638, ___m_IsAutoReloadEnabled_8)); }
	inline bool get_m_IsAutoReloadEnabled_8() const { return ___m_IsAutoReloadEnabled_8; }
	inline bool* get_address_of_m_IsAutoReloadEnabled_8() { return &___m_IsAutoReloadEnabled_8; }
	inline void set_m_IsAutoReloadEnabled_8(bool value)
	{
		___m_IsAutoReloadEnabled_8 = value;
	}

	inline static int32_t get_offset_of__interface_9() { return static_cast<int32_t>(offsetof(NendAdInterstitial_t1417649638, ____interface_9)); }
	inline Il2CppObject * get__interface_9() const { return ____interface_9; }
	inline Il2CppObject ** get_address_of__interface_9() { return &____interface_9; }
	inline void set__interface_9(Il2CppObject * value)
	{
		____interface_9 = value;
		Il2CppCodeGenWriteBarrier(&____interface_9, value);
	}

	inline static int32_t get_offset_of_AdLoaded_10() { return static_cast<int32_t>(offsetof(NendAdInterstitial_t1417649638, ___AdLoaded_10)); }
	inline EventHandler_1_t3479984044 * get_AdLoaded_10() const { return ___AdLoaded_10; }
	inline EventHandler_1_t3479984044 ** get_address_of_AdLoaded_10() { return &___AdLoaded_10; }
	inline void set_AdLoaded_10(EventHandler_1_t3479984044 * value)
	{
		___AdLoaded_10 = value;
		Il2CppCodeGenWriteBarrier(&___AdLoaded_10, value);
	}

	inline static int32_t get_offset_of_AdClicked_11() { return static_cast<int32_t>(offsetof(NendAdInterstitial_t1417649638, ___AdClicked_11)); }
	inline EventHandler_1_t3193289641 * get_AdClicked_11() const { return ___AdClicked_11; }
	inline EventHandler_1_t3193289641 ** get_address_of_AdClicked_11() { return &___AdClicked_11; }
	inline void set_AdClicked_11(EventHandler_1_t3193289641 * value)
	{
		___AdClicked_11 = value;
		Il2CppCodeGenWriteBarrier(&___AdClicked_11, value);
	}

	inline static int32_t get_offset_of_AdShown_12() { return static_cast<int32_t>(offsetof(NendAdInterstitial_t1417649638, ___AdShown_12)); }
	inline EventHandler_1_t1007381714 * get_AdShown_12() const { return ___AdShown_12; }
	inline EventHandler_1_t1007381714 ** get_address_of_AdShown_12() { return &___AdShown_12; }
	inline void set_AdShown_12(EventHandler_1_t1007381714 * value)
	{
		___AdShown_12 = value;
		Il2CppCodeGenWriteBarrier(&___AdShown_12, value);
	}
};

struct NendAdInterstitial_t1417649638_StaticFields
{
public:
	// NendUnityPlugin.AD.NendAdInterstitial NendUnityPlugin.AD.NendAdInterstitial::_instance
	NendAdInterstitial_t1417649638 * ____instance_7;

public:
	inline static int32_t get_offset_of__instance_7() { return static_cast<int32_t>(offsetof(NendAdInterstitial_t1417649638_StaticFields, ____instance_7)); }
	inline NendAdInterstitial_t1417649638 * get__instance_7() const { return ____instance_7; }
	inline NendAdInterstitial_t1417649638 ** get_address_of__instance_7() { return &____instance_7; }
	inline void set__instance_7(NendAdInterstitial_t1417649638 * value)
	{
		____instance_7 = value;
		Il2CppCodeGenWriteBarrier(&____instance_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
