﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen1300214534.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.Events.NativeAdViewFailedToLoadEvent
struct  NativeAdViewFailedToLoadEvent_t3307153462  : public UnityEvent_3_t1300214534
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
