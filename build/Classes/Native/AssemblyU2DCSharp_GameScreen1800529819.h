﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t3581341831;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.GUIText
struct GUIText_t402233326;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameScreen
struct  GameScreen_t1800529819  : public MonoBehaviour_t3962482529
{
public:
	// System.String GameScreen::platformType
	String_t* ___platformType_2;
	// UnityEngine.GameObject GameScreen::groundPrefab
	GameObject_t1113636619 * ___groundPrefab_3;
	// UnityEngine.GameObject GameScreen::obsPrefab
	GameObject_t1113636619 * ___obsPrefab_4;
	// UnityEngine.GameObject GameScreen::obs2Prefab
	GameObject_t1113636619 * ___obs2Prefab_5;
	// UnityEngine.GameObject[] GameScreen::obs
	GameObjectU5BU5D_t3328599146* ___obs_6;
	// UnityEngine.GameObject GameScreen::player
	GameObject_t1113636619 * ___player_7;
	// System.Single GameScreen::bottomY
	float ___bottomY_8;
	// System.Single GameScreen::leftX
	float ___leftX_9;
	// System.Single GameScreen::topY
	float ___topY_10;
	// System.Single GameScreen::rightX
	float ___rightX_11;
	// System.Single GameScreen::obsTileH
	float ___obsTileH_12;
	// System.Single GameScreen::obs2TileH
	float ___obs2TileH_13;
	// System.Single GameScreen::groundTileH
	float ___groundTileH_14;
	// System.Single GameScreen::obsHeightScale
	float ___obsHeightScale_16;
	// System.Single GameScreen::gap
	float ___gap_17;
	// System.Single GameScreen::height1
	float ___height1_18;
	// System.Single GameScreen::height2
	float ___height2_19;
	// UnityEngine.BoxCollider2D GameScreen::obsCollider
	BoxCollider2D_t3581341831 * ___obsCollider_20;
	// UnityEngine.BoxCollider2D GameScreen::obs2Collider
	BoxCollider2D_t3581341831 * ___obs2Collider_21;
	// UnityEngine.AudioClip GameScreen::overClip
	AudioClip_t3680889665 * ___overClip_22;
	// UnityEngine.AudioSource GameScreen::overSource
	AudioSource_t3935305588 * ___overSource_23;
	// UnityEngine.AudioClip GameScreen::deadClip
	AudioClip_t3680889665 * ___deadClip_24;
	// System.Int32 GameScreen::objectMax
	int32_t ___objectMax_27;
	// UnityEngine.Renderer GameScreen::pauseBg
	Renderer_t2627027031 * ___pauseBg_28;
	// UnityEngine.Renderer GameScreen::btnResume
	Renderer_t2627027031 * ___btnResume_29;
	// UnityEngine.Renderer GameScreen::btnHome
	Renderer_t2627027031 * ___btnHome_30;
	// UnityEngine.Renderer GameScreen::btnReplay
	Renderer_t2627027031 * ___btnReplay_31;
	// UnityEngine.GUIText GameScreen::pauseText
	GUIText_t402233326 * ___pauseText_32;
	// UnityEngine.Renderer GameScreen::overBg
	Renderer_t2627027031 * ___overBg_33;
	// UnityEngine.Renderer GameScreen::btnHomeOver
	Renderer_t2627027031 * ___btnHomeOver_34;
	// UnityEngine.Renderer GameScreen::btnReplayOver
	Renderer_t2627027031 * ___btnReplayOver_35;
	// UnityEngine.Renderer GameScreen::btnPause
	Renderer_t2627027031 * ___btnPause_36;
	// UnityEngine.GameObject GameScreen::SherePlugins
	GameObject_t1113636619 * ___SherePlugins_37;
	// UnityEngine.GameObject GameScreen::gameOverScreen
	GameObject_t1113636619 * ___gameOverScreen_38;

public:
	inline static int32_t get_offset_of_platformType_2() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___platformType_2)); }
	inline String_t* get_platformType_2() const { return ___platformType_2; }
	inline String_t** get_address_of_platformType_2() { return &___platformType_2; }
	inline void set_platformType_2(String_t* value)
	{
		___platformType_2 = value;
		Il2CppCodeGenWriteBarrier(&___platformType_2, value);
	}

	inline static int32_t get_offset_of_groundPrefab_3() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___groundPrefab_3)); }
	inline GameObject_t1113636619 * get_groundPrefab_3() const { return ___groundPrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_groundPrefab_3() { return &___groundPrefab_3; }
	inline void set_groundPrefab_3(GameObject_t1113636619 * value)
	{
		___groundPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___groundPrefab_3, value);
	}

	inline static int32_t get_offset_of_obsPrefab_4() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___obsPrefab_4)); }
	inline GameObject_t1113636619 * get_obsPrefab_4() const { return ___obsPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_obsPrefab_4() { return &___obsPrefab_4; }
	inline void set_obsPrefab_4(GameObject_t1113636619 * value)
	{
		___obsPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___obsPrefab_4, value);
	}

	inline static int32_t get_offset_of_obs2Prefab_5() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___obs2Prefab_5)); }
	inline GameObject_t1113636619 * get_obs2Prefab_5() const { return ___obs2Prefab_5; }
	inline GameObject_t1113636619 ** get_address_of_obs2Prefab_5() { return &___obs2Prefab_5; }
	inline void set_obs2Prefab_5(GameObject_t1113636619 * value)
	{
		___obs2Prefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___obs2Prefab_5, value);
	}

	inline static int32_t get_offset_of_obs_6() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___obs_6)); }
	inline GameObjectU5BU5D_t3328599146* get_obs_6() const { return ___obs_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_obs_6() { return &___obs_6; }
	inline void set_obs_6(GameObjectU5BU5D_t3328599146* value)
	{
		___obs_6 = value;
		Il2CppCodeGenWriteBarrier(&___obs_6, value);
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___player_7)); }
	inline GameObject_t1113636619 * get_player_7() const { return ___player_7; }
	inline GameObject_t1113636619 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(GameObject_t1113636619 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier(&___player_7, value);
	}

	inline static int32_t get_offset_of_bottomY_8() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___bottomY_8)); }
	inline float get_bottomY_8() const { return ___bottomY_8; }
	inline float* get_address_of_bottomY_8() { return &___bottomY_8; }
	inline void set_bottomY_8(float value)
	{
		___bottomY_8 = value;
	}

	inline static int32_t get_offset_of_leftX_9() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___leftX_9)); }
	inline float get_leftX_9() const { return ___leftX_9; }
	inline float* get_address_of_leftX_9() { return &___leftX_9; }
	inline void set_leftX_9(float value)
	{
		___leftX_9 = value;
	}

	inline static int32_t get_offset_of_topY_10() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___topY_10)); }
	inline float get_topY_10() const { return ___topY_10; }
	inline float* get_address_of_topY_10() { return &___topY_10; }
	inline void set_topY_10(float value)
	{
		___topY_10 = value;
	}

	inline static int32_t get_offset_of_rightX_11() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___rightX_11)); }
	inline float get_rightX_11() const { return ___rightX_11; }
	inline float* get_address_of_rightX_11() { return &___rightX_11; }
	inline void set_rightX_11(float value)
	{
		___rightX_11 = value;
	}

	inline static int32_t get_offset_of_obsTileH_12() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___obsTileH_12)); }
	inline float get_obsTileH_12() const { return ___obsTileH_12; }
	inline float* get_address_of_obsTileH_12() { return &___obsTileH_12; }
	inline void set_obsTileH_12(float value)
	{
		___obsTileH_12 = value;
	}

	inline static int32_t get_offset_of_obs2TileH_13() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___obs2TileH_13)); }
	inline float get_obs2TileH_13() const { return ___obs2TileH_13; }
	inline float* get_address_of_obs2TileH_13() { return &___obs2TileH_13; }
	inline void set_obs2TileH_13(float value)
	{
		___obs2TileH_13 = value;
	}

	inline static int32_t get_offset_of_groundTileH_14() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___groundTileH_14)); }
	inline float get_groundTileH_14() const { return ___groundTileH_14; }
	inline float* get_address_of_groundTileH_14() { return &___groundTileH_14; }
	inline void set_groundTileH_14(float value)
	{
		___groundTileH_14 = value;
	}

	inline static int32_t get_offset_of_obsHeightScale_16() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___obsHeightScale_16)); }
	inline float get_obsHeightScale_16() const { return ___obsHeightScale_16; }
	inline float* get_address_of_obsHeightScale_16() { return &___obsHeightScale_16; }
	inline void set_obsHeightScale_16(float value)
	{
		___obsHeightScale_16 = value;
	}

	inline static int32_t get_offset_of_gap_17() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___gap_17)); }
	inline float get_gap_17() const { return ___gap_17; }
	inline float* get_address_of_gap_17() { return &___gap_17; }
	inline void set_gap_17(float value)
	{
		___gap_17 = value;
	}

	inline static int32_t get_offset_of_height1_18() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___height1_18)); }
	inline float get_height1_18() const { return ___height1_18; }
	inline float* get_address_of_height1_18() { return &___height1_18; }
	inline void set_height1_18(float value)
	{
		___height1_18 = value;
	}

	inline static int32_t get_offset_of_height2_19() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___height2_19)); }
	inline float get_height2_19() const { return ___height2_19; }
	inline float* get_address_of_height2_19() { return &___height2_19; }
	inline void set_height2_19(float value)
	{
		___height2_19 = value;
	}

	inline static int32_t get_offset_of_obsCollider_20() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___obsCollider_20)); }
	inline BoxCollider2D_t3581341831 * get_obsCollider_20() const { return ___obsCollider_20; }
	inline BoxCollider2D_t3581341831 ** get_address_of_obsCollider_20() { return &___obsCollider_20; }
	inline void set_obsCollider_20(BoxCollider2D_t3581341831 * value)
	{
		___obsCollider_20 = value;
		Il2CppCodeGenWriteBarrier(&___obsCollider_20, value);
	}

	inline static int32_t get_offset_of_obs2Collider_21() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___obs2Collider_21)); }
	inline BoxCollider2D_t3581341831 * get_obs2Collider_21() const { return ___obs2Collider_21; }
	inline BoxCollider2D_t3581341831 ** get_address_of_obs2Collider_21() { return &___obs2Collider_21; }
	inline void set_obs2Collider_21(BoxCollider2D_t3581341831 * value)
	{
		___obs2Collider_21 = value;
		Il2CppCodeGenWriteBarrier(&___obs2Collider_21, value);
	}

	inline static int32_t get_offset_of_overClip_22() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___overClip_22)); }
	inline AudioClip_t3680889665 * get_overClip_22() const { return ___overClip_22; }
	inline AudioClip_t3680889665 ** get_address_of_overClip_22() { return &___overClip_22; }
	inline void set_overClip_22(AudioClip_t3680889665 * value)
	{
		___overClip_22 = value;
		Il2CppCodeGenWriteBarrier(&___overClip_22, value);
	}

	inline static int32_t get_offset_of_overSource_23() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___overSource_23)); }
	inline AudioSource_t3935305588 * get_overSource_23() const { return ___overSource_23; }
	inline AudioSource_t3935305588 ** get_address_of_overSource_23() { return &___overSource_23; }
	inline void set_overSource_23(AudioSource_t3935305588 * value)
	{
		___overSource_23 = value;
		Il2CppCodeGenWriteBarrier(&___overSource_23, value);
	}

	inline static int32_t get_offset_of_deadClip_24() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___deadClip_24)); }
	inline AudioClip_t3680889665 * get_deadClip_24() const { return ___deadClip_24; }
	inline AudioClip_t3680889665 ** get_address_of_deadClip_24() { return &___deadClip_24; }
	inline void set_deadClip_24(AudioClip_t3680889665 * value)
	{
		___deadClip_24 = value;
		Il2CppCodeGenWriteBarrier(&___deadClip_24, value);
	}

	inline static int32_t get_offset_of_objectMax_27() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___objectMax_27)); }
	inline int32_t get_objectMax_27() const { return ___objectMax_27; }
	inline int32_t* get_address_of_objectMax_27() { return &___objectMax_27; }
	inline void set_objectMax_27(int32_t value)
	{
		___objectMax_27 = value;
	}

	inline static int32_t get_offset_of_pauseBg_28() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___pauseBg_28)); }
	inline Renderer_t2627027031 * get_pauseBg_28() const { return ___pauseBg_28; }
	inline Renderer_t2627027031 ** get_address_of_pauseBg_28() { return &___pauseBg_28; }
	inline void set_pauseBg_28(Renderer_t2627027031 * value)
	{
		___pauseBg_28 = value;
		Il2CppCodeGenWriteBarrier(&___pauseBg_28, value);
	}

	inline static int32_t get_offset_of_btnResume_29() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___btnResume_29)); }
	inline Renderer_t2627027031 * get_btnResume_29() const { return ___btnResume_29; }
	inline Renderer_t2627027031 ** get_address_of_btnResume_29() { return &___btnResume_29; }
	inline void set_btnResume_29(Renderer_t2627027031 * value)
	{
		___btnResume_29 = value;
		Il2CppCodeGenWriteBarrier(&___btnResume_29, value);
	}

	inline static int32_t get_offset_of_btnHome_30() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___btnHome_30)); }
	inline Renderer_t2627027031 * get_btnHome_30() const { return ___btnHome_30; }
	inline Renderer_t2627027031 ** get_address_of_btnHome_30() { return &___btnHome_30; }
	inline void set_btnHome_30(Renderer_t2627027031 * value)
	{
		___btnHome_30 = value;
		Il2CppCodeGenWriteBarrier(&___btnHome_30, value);
	}

	inline static int32_t get_offset_of_btnReplay_31() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___btnReplay_31)); }
	inline Renderer_t2627027031 * get_btnReplay_31() const { return ___btnReplay_31; }
	inline Renderer_t2627027031 ** get_address_of_btnReplay_31() { return &___btnReplay_31; }
	inline void set_btnReplay_31(Renderer_t2627027031 * value)
	{
		___btnReplay_31 = value;
		Il2CppCodeGenWriteBarrier(&___btnReplay_31, value);
	}

	inline static int32_t get_offset_of_pauseText_32() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___pauseText_32)); }
	inline GUIText_t402233326 * get_pauseText_32() const { return ___pauseText_32; }
	inline GUIText_t402233326 ** get_address_of_pauseText_32() { return &___pauseText_32; }
	inline void set_pauseText_32(GUIText_t402233326 * value)
	{
		___pauseText_32 = value;
		Il2CppCodeGenWriteBarrier(&___pauseText_32, value);
	}

	inline static int32_t get_offset_of_overBg_33() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___overBg_33)); }
	inline Renderer_t2627027031 * get_overBg_33() const { return ___overBg_33; }
	inline Renderer_t2627027031 ** get_address_of_overBg_33() { return &___overBg_33; }
	inline void set_overBg_33(Renderer_t2627027031 * value)
	{
		___overBg_33 = value;
		Il2CppCodeGenWriteBarrier(&___overBg_33, value);
	}

	inline static int32_t get_offset_of_btnHomeOver_34() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___btnHomeOver_34)); }
	inline Renderer_t2627027031 * get_btnHomeOver_34() const { return ___btnHomeOver_34; }
	inline Renderer_t2627027031 ** get_address_of_btnHomeOver_34() { return &___btnHomeOver_34; }
	inline void set_btnHomeOver_34(Renderer_t2627027031 * value)
	{
		___btnHomeOver_34 = value;
		Il2CppCodeGenWriteBarrier(&___btnHomeOver_34, value);
	}

	inline static int32_t get_offset_of_btnReplayOver_35() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___btnReplayOver_35)); }
	inline Renderer_t2627027031 * get_btnReplayOver_35() const { return ___btnReplayOver_35; }
	inline Renderer_t2627027031 ** get_address_of_btnReplayOver_35() { return &___btnReplayOver_35; }
	inline void set_btnReplayOver_35(Renderer_t2627027031 * value)
	{
		___btnReplayOver_35 = value;
		Il2CppCodeGenWriteBarrier(&___btnReplayOver_35, value);
	}

	inline static int32_t get_offset_of_btnPause_36() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___btnPause_36)); }
	inline Renderer_t2627027031 * get_btnPause_36() const { return ___btnPause_36; }
	inline Renderer_t2627027031 ** get_address_of_btnPause_36() { return &___btnPause_36; }
	inline void set_btnPause_36(Renderer_t2627027031 * value)
	{
		___btnPause_36 = value;
		Il2CppCodeGenWriteBarrier(&___btnPause_36, value);
	}

	inline static int32_t get_offset_of_SherePlugins_37() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___SherePlugins_37)); }
	inline GameObject_t1113636619 * get_SherePlugins_37() const { return ___SherePlugins_37; }
	inline GameObject_t1113636619 ** get_address_of_SherePlugins_37() { return &___SherePlugins_37; }
	inline void set_SherePlugins_37(GameObject_t1113636619 * value)
	{
		___SherePlugins_37 = value;
		Il2CppCodeGenWriteBarrier(&___SherePlugins_37, value);
	}

	inline static int32_t get_offset_of_gameOverScreen_38() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___gameOverScreen_38)); }
	inline GameObject_t1113636619 * get_gameOverScreen_38() const { return ___gameOverScreen_38; }
	inline GameObject_t1113636619 ** get_address_of_gameOverScreen_38() { return &___gameOverScreen_38; }
	inline void set_gameOverScreen_38(GameObject_t1113636619 * value)
	{
		___gameOverScreen_38 = value;
		Il2CppCodeGenWriteBarrier(&___gameOverScreen_38, value);
	}
};

struct GameScreen_t1800529819_StaticFields
{
public:
	// System.Boolean GameScreen::paused
	bool ___paused_15;
	// UnityEngine.AudioSource GameScreen::deadSource
	AudioSource_t3935305588 * ___deadSource_25;
	// System.Boolean GameScreen::canControl
	bool ___canControl_26;

public:
	inline static int32_t get_offset_of_paused_15() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819_StaticFields, ___paused_15)); }
	inline bool get_paused_15() const { return ___paused_15; }
	inline bool* get_address_of_paused_15() { return &___paused_15; }
	inline void set_paused_15(bool value)
	{
		___paused_15 = value;
	}

	inline static int32_t get_offset_of_deadSource_25() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819_StaticFields, ___deadSource_25)); }
	inline AudioSource_t3935305588 * get_deadSource_25() const { return ___deadSource_25; }
	inline AudioSource_t3935305588 ** get_address_of_deadSource_25() { return &___deadSource_25; }
	inline void set_deadSource_25(AudioSource_t3935305588 * value)
	{
		___deadSource_25 = value;
		Il2CppCodeGenWriteBarrier(&___deadSource_25, value);
	}

	inline static int32_t get_offset_of_canControl_26() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819_StaticFields, ___canControl_26)); }
	inline bool get_canControl_26() const { return ___canControl_26; }
	inline bool* get_address_of_canControl_26() { return &___canControl_26; }
	inline void set_canControl_26(bool value)
	{
		___canControl_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
