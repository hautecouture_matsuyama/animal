﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_VideoAd2567701888.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Video.ErrorVideoAdCallbackArgments
struct  ErrorVideoAdCallbackArgments_t3139399691  : public VideoAdCallbackArgments_t2567701888
{
public:
	// System.Int32 NendUnityPlugin.AD.Video.ErrorVideoAdCallbackArgments::errorCode
	int32_t ___errorCode_1;

public:
	inline static int32_t get_offset_of_errorCode_1() { return static_cast<int32_t>(offsetof(ErrorVideoAdCallbackArgments_t3139399691, ___errorCode_1)); }
	inline int32_t get_errorCode_1() const { return ___errorCode_1; }
	inline int32_t* get_address_of_errorCode_1() { return &___errorCode_1; }
	inline void set_errorCode_1(int32_t value)
	{
		___errorCode_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
