﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// NendUnityPlugin.AD.Native.Utils.SimpleTimer
struct SimpleTimer_t4075384413;
// System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>
struct Action_3_t567914338;
// System.Collections.Generic.Queue`1<System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>>
struct Queue_1_t414173832;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.NativeAdClient
struct  NativeAdClient_t772434093  : public Il2CppObject
{
public:
	// NendUnityPlugin.AD.Native.Utils.SimpleTimer NendUnityPlugin.AD.Native.NativeAdClient::m_Timer
	SimpleTimer_t4075384413 * ___m_Timer_1;
	// System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String> NendUnityPlugin.AD.Native.NativeAdClient::m_ReloadCallback
	Action_3_t567914338 * ___m_ReloadCallback_2;
	// System.Collections.Generic.Queue`1<System.Action`3<NendUnityPlugin.AD.Native.INativeAd,System.Int32,System.String>> NendUnityPlugin.AD.Native.NativeAdClient::m_Callbacks
	Queue_1_t414173832 * ___m_Callbacks_3;

public:
	inline static int32_t get_offset_of_m_Timer_1() { return static_cast<int32_t>(offsetof(NativeAdClient_t772434093, ___m_Timer_1)); }
	inline SimpleTimer_t4075384413 * get_m_Timer_1() const { return ___m_Timer_1; }
	inline SimpleTimer_t4075384413 ** get_address_of_m_Timer_1() { return &___m_Timer_1; }
	inline void set_m_Timer_1(SimpleTimer_t4075384413 * value)
	{
		___m_Timer_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Timer_1, value);
	}

	inline static int32_t get_offset_of_m_ReloadCallback_2() { return static_cast<int32_t>(offsetof(NativeAdClient_t772434093, ___m_ReloadCallback_2)); }
	inline Action_3_t567914338 * get_m_ReloadCallback_2() const { return ___m_ReloadCallback_2; }
	inline Action_3_t567914338 ** get_address_of_m_ReloadCallback_2() { return &___m_ReloadCallback_2; }
	inline void set_m_ReloadCallback_2(Action_3_t567914338 * value)
	{
		___m_ReloadCallback_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_ReloadCallback_2, value);
	}

	inline static int32_t get_offset_of_m_Callbacks_3() { return static_cast<int32_t>(offsetof(NativeAdClient_t772434093, ___m_Callbacks_3)); }
	inline Queue_1_t414173832 * get_m_Callbacks_3() const { return ___m_Callbacks_3; }
	inline Queue_1_t414173832 ** get_address_of_m_Callbacks_3() { return &___m_Callbacks_3; }
	inline void set_m_Callbacks_3(Queue_1_t414173832 * value)
	{
		___m_Callbacks_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Callbacks_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
