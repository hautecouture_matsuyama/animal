﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "LitJson_U3CModuleU3E692745525.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db53336159630.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db53636912669.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db52672650908.h"
#include "LitJson_LitJson_ArrayMetadata894288939.h"
#include "mscorlib_System_Type2483944760.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Boolean97287965.h"
#include "LitJson_LitJson_Condition3240125930.h"
#include "LitJson_LitJson_ExporterFunc1851311465.h"
#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "LitJson_LitJson_JsonWriter3570089748.h"
#include "mscorlib_System_AsyncCallback3962456242.h"
#include "LitJson_LitJson_FsmContext2331368794.h"
#include "LitJson_LitJson_ImporterFunc3630937194.h"
#include "LitJson_LitJson_JsonData1524858407.h"
#include "mscorlib_System_Int322950945753.h"
#include "mscorlib_System_String1847450689.h"
#include "mscorlib_System_ArgumentException132251570.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23707786873.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22530217319.h"
#include "LitJson_LitJson_JsonType2731125707.h"
#include "mscorlib_System_Double594665363.h"
#include "mscorlib_System_Int643736567304.h"
#include "LitJson_LitJson_OrderedDictionaryEnumerator386339177.h"
#include "mscorlib_System_InvalidOperationException56020091.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1310114706.h"
#include "mscorlib_System_Collections_Generic_List_1_gen884894319.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2996933149.h"
#include "LitJson_LitJson_JsonException3682484112.h"
#include "LitJson_LitJson_ParserToken2380208742.h"
#include "mscorlib_System_Exception1436737249.h"
#include "mscorlib_System_ApplicationException2339761290.h"
#include "mscorlib_System_Char3634460470.h"
#include "LitJson_LitJson_JsonMapper3815285241.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3338636003.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge935204471.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1715664290.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3692140024.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2405853701.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen691233.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2688515417.h"
#include "mscorlib_System_Reflection_PropertyInfo687341951.h"
#include "mscorlib_System_Reflection_ParameterInfo1861056598.h"
#include "mscorlib_System_Reflection_MemberInfo3380001741.h"
#include "LitJson_LitJson_ObjectMetadata3566284522.h"
#include "LitJson_LitJson_PropertyMetadata3727440473.h"
#include "mscorlib_System_Reflection_FieldInfo4181702585.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3512696772.h"
#include "mscorlib_System_Reflection_MethodInfo1877626248.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen27006016.h"
#include "LitJson_LitJson_JsonReader836887441.h"
#include "LitJson_LitJson_JsonToken3605538862.h"
#include "mscorlib_System_Reflection_MethodBase609368412.h"
#include "mscorlib_System_Collections_ArrayList2718874744.h"
#include "LitJson_LitJson_WrapperFactory2158548929.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1780316962.h"
#include "LitJson_LitJson_JsonMockWrapper82875095.h"
#include "mscorlib_System_Byte1134296376.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "mscorlib_System_Decimal2948259380.h"
#include "mscorlib_System_SByte1669577662.h"
#include "mscorlib_System_Int162552820387.h"
#include "mscorlib_System_UInt162177724958.h"
#include "mscorlib_System_UInt322560061978.h"
#include "mscorlib_System_UInt644134040092.h"
#include "mscorlib_System_Single1397266774.h"
#include "mscorlib_System_IO_TextReader283511965.h"
#include "mscorlib_System_IO_StringReader3465604688.h"
#include "mscorlib_System_ArgumentNullException1615371798.h"
#include "System_System_Collections_Generic_Stack_1_gen3794335208.h"
#include "LitJson_LitJson_Lexer1514038666.h"
#include "mscorlib_System_RuntimeFieldHandle1871169219.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge921491721.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3568926999.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExc2292407383.h"
#include "mscorlib_System_Globalization_NumberFormatInfo435877138.h"
#include "mscorlib_System_Text_StringBuilder1712802186.h"
#include "mscorlib_System_IO_StringWriter802263757.h"
#include "mscorlib_System_IO_TextWriter3478189236.h"
#include "LitJson_LitJson_WriterContext1011093999.h"
#include "System_System_Collections_Generic_Stack_1_gen1854483454.h"
#include "LitJson_LitJson_Lexer_StateHandler105866779.h"
#include "mscorlib_System_Collections_DictionaryEntry3123975638.h"

// System.Type
struct Type_t;
// LitJson.ExporterFunc
struct ExporterFunc_t1851311465;
// System.Object
struct Il2CppObject;
// LitJson.JsonWriter
struct JsonWriter_t3570089748;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// LitJson.FsmContext
struct FsmContext_t2331368794;
// LitJson.ImporterFunc
struct ImporterFunc_t3630937194;
// LitJson.JsonData
struct JsonData_t1524858407;
// System.Collections.ICollection
struct ICollection_t3904884886;
// System.Collections.IList
struct IList_t2094931216;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.String
struct String_t;
// System.Array
struct Il2CppArray;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1693217257;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// LitJson.OrderedDictionaryEnumerator
struct OrderedDictionaryEnumerator_t386339177;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>
struct IEnumerator_1_t4140357341;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// System.Collections.Generic.Dictionary`2<System.String,LitJson.JsonData>
struct Dictionary_2_t1310114706;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>
struct List_1_t884894319;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t4002292061;
// System.Collections.Generic.List`1<LitJson.JsonData>
struct List_1_t2996933149;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// LitJson.JsonException
struct JsonException_t3682484112;
// System.Exception
struct Exception_t1436737249;
// System.ApplicationException
struct ApplicationException_t2339761290;
// System.Collections.Generic.Dictionary`2<System.Type,LitJson.ArrayMetadata>
struct Dictionary_2_t3338636003;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.ArrayMetadata>
struct Dictionary_2_t2241695223;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>
struct Dictionary_2_t935204471;
// System.Collections.Generic.Dictionary`2<System.Type,LitJson.ObjectMetadata>
struct Dictionary_2_t1715664290;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.ObjectMetadata>
struct Dictionary_2_t618723510;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>>
struct Dictionary_2_t3692140024;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t2405853701;
// System.Collections.Generic.Dictionary`2<System.Type,LitJson.ExporterFunc>
struct Dictionary_2_t691233;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>
struct Dictionary_2_t2688515417;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1461822886;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.ParameterInfo
struct ParameterInfo_t1861056598;
// System.Collections.Generic.Dictionary`2<System.String,LitJson.PropertyMetadata>
struct Dictionary_2_t3512696772;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.PropertyMetadata>
struct Dictionary_2_t779879461;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>
struct IDictionary_2_t1976548163;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t846150980;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.MethodInfo>
struct Dictionary_2_t27006016;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// LitJson.JsonReader
struct JsonReader_t836887441;
// System.Reflection.MethodBase
struct MethodBase_t609368412;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// LitJson.IJsonWrapper
struct IJsonWrapper_t1028825384;
// LitJson.WrapperFactory
struct WrapperFactory_t2158548929;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>
struct IDictionary_2_t1152366808;
// System.Collections.Generic.Dictionary`2<System.Type,LitJson.ImporterFunc>
struct Dictionary_2_t1780316962;
// LitJson.JsonMockWrapper
struct JsonMockWrapper_t82875095;
// System.IFormatProvider
struct IFormatProvider_t2518567562;
// System.IO.StringReader
struct StringReader_t3465604688;
// System.IO.TextReader
struct TextReader_t283511965;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t3794335208;
// LitJson.Lexer
struct Lexer_t1514038666;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>
struct Dictionary_2_t921491721;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1968819495;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32[]>
struct Dictionary_2_t3568926999;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t435877138;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// System.IO.StringWriter
struct StringWriter_t802263757;
// System.Collections.Generic.Stack`1<LitJson.WriterContext>
struct Stack_1_t1854483454;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3923495619;
// LitJson.WriterContext
struct WriterContext_t1011093999;
// System.Char[]
struct CharU5BU5D_t3528271667;
// LitJson.Lexer/StateHandler
struct StateHandler_t105866779;
extern const Il2CppType* JsonData_t1524858407_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ArrayMetadata_get_ElementType_m2810930502_MetadataUsageId;
extern Il2CppClass* ICollection_t3904884886_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_ICollection_get_IsSynchronized_m1459292420_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_ICollection_get_SyncRoot_m1695797096_MetadataUsageId;
extern Il2CppClass* IList_t2094931216_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_get_IsFixedSize_m2437965165_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_IList_get_IsReadOnly_m978904392_MetadataUsageId;
extern Il2CppClass* IDictionary_t1363984059_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_get_Item_m2981515460_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral349688317;
extern const uint32_t JsonData_System_Collections_IDictionary_set_Item_m3224697051_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_IList_get_Item_m2045844139_MetadataUsageId;
extern const uint32_t JsonData_get_Count_m3094119778_MetadataUsageId;
extern Il2CppClass* IDictionary_2_t4068933393_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t1228139360_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t2240971811_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2__ctor_m630695613_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m913646779_MethodInfo_var;
extern const uint32_t JsonData_set_Item_m1920107000_MetadataUsageId;
extern Il2CppClass* IList_1_t3340178190_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_set_Item_m1179907761_MetadataUsageId;
extern Il2CppClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t594665363_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t3736567304_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2958118195;
extern const uint32_t JsonData__ctor_m3866765393_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_ICollection_CopyTo_m2772442918_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_IDictionary_Add_m4101376796_MetadataUsageId;
extern Il2CppClass* IOrderedDictionary_t2745322917_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_GetEnumerator_m2090743371_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_IDictionary_Remove_m4104093953_MetadataUsageId;
extern Il2CppClass* IEnumerable_t1941168011_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IEnumerable_GetEnumerator_m3857574006_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_IList_Clear_m3703745914_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_IList_Contains_m1075980415_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_IList_IndexOf_m1757109292_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_IList_Insert_m557679721_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_IList_Remove_m77578819_MetadataUsageId;
extern const uint32_t JsonData_System_Collections_IList_RemoveAt_m2329997688_MetadataUsageId;
extern Il2CppClass* IEnumerable_1_t2687639762_il2cpp_TypeInfo_var;
extern Il2CppClass* OrderedDictionaryEnumerator_t386339177_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m2093370556_MetadataUsageId;
extern Il2CppClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2524214959;
extern const uint32_t JsonData_EnsureCollection_m1446037538_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t1310114706_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t884894319_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1325424069_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2572897465_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2604237559;
extern const uint32_t JsonData_EnsureDictionary_m3057468608_MetadataUsageId;
extern Il2CppClass* List_1_t2996933149_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1532675647_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4137426702;
extern const uint32_t JsonData_EnsureList_m1549285368_MetadataUsageId;
extern Il2CppClass* JsonData_t1524858407_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_ToJsonData_m69846785_MetadataUsageId;
extern const uint32_t JsonData_Add_m2952029759_MetadataUsageId;
extern const uint32_t JsonData_SetJsonType_m3288213674_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4244825595;
extern Il2CppCodeGenString* _stringLiteral3670642506;
extern Il2CppCodeGenString* _stringLiteral2138300441;
extern const uint32_t JsonData_ToString_m160346085_MetadataUsageId;
extern Il2CppClass* ParserToken_t2380208742_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral917256838;
extern const uint32_t JsonException__ctor_m4037883734_MetadataUsageId;
extern Il2CppClass* Char_t3634460470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4006081049;
extern const uint32_t JsonException__ctor_m2210441219_MetadataUsageId;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t3815285241_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3338636003_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t935204471_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1715664290_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3692140024_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t3570089748_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeFormatInfo_t2405853701_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t691233_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2688515417_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2431295564_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2076397116_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1674229861_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2292515434_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2735992426_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1523576929_MethodInfo_var;
extern const uint32_t JsonMapper__cctor_m850028620_MetadataUsageId;
extern const Il2CppType* Int32_t2950945753_0_0_0_var;
extern Il2CppClass* IDictionary_2_t1802487394_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayMetadata_t894288939_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2211611910;
extern Il2CppCodeGenString* _stringLiteral1949155704;
extern const uint32_t JsonMapper_AddArrayMetadata_m3968694626_MetadataUsageId;
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* IDictionary_2_t179515681_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectMetadata_t3566284522_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3512696772_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyMetadata_t3727440473_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1976548163_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3146315127_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1480664753;
extern const uint32_t JsonMapper_AddObjectMetadata_m194540947_MetadataUsageId;
extern Il2CppClass* IDictionary_2_t3694023158_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t27006016_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2785824703_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m635195254_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3306367446;
extern const uint32_t JsonMapper_GetConvOp_m1492689601_MetadataUsageId;
extern Il2CppClass* JsonException_t3682484112_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1152366808_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t244168353_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t4135868527_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2718874744_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppArray_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2156791391;
extern Il2CppCodeGenString* _stringLiteral57478429;
extern Il2CppCodeGenString* _stringLiteral3271142035;
extern Il2CppCodeGenString* _stringLiteral888960820;
extern const uint32_t JsonMapper_ReadValue_m1193038115_MetadataUsageId;
extern Il2CppClass* IJsonWrapper_t1028825384_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_ReadValue_m3060360668_MetadataUsageId;
extern Il2CppClass* WrapperFactory_t2158548929_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonMapper_U3CReadSkipU3Em__0_m139886084_MethodInfo_var;
extern const uint32_t JsonMapper_ReadSkip_m3223759513_MetadataUsageId;
extern const Il2CppType* Byte_t1134296376_0_0_0_var;
extern const Il2CppType* Char_t3634460470_0_0_0_var;
extern const Il2CppType* DateTime_t3738529785_0_0_0_var;
extern const Il2CppType* Decimal_t2948259380_0_0_0_var;
extern const Il2CppType* SByte_t1669577662_0_0_0_var;
extern const Il2CppType* Int16_t2552820387_0_0_0_var;
extern const Il2CppType* UInt16_t2177724958_0_0_0_var;
extern const Il2CppType* UInt32_t2560061978_0_0_0_var;
extern const Il2CppType* UInt64_t4134040092_0_0_0_var;
extern Il2CppClass* ExporterFunc_t1851311465_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2759509920_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__1_m3570407405_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__2_m600765210_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__3_m939732965_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__4_m266458088_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__5_m3328515821_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__6_m312889811_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__7_m3877741659_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__8_m225163408_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__9_m1344668283_MethodInfo_var;
extern const uint32_t JsonMapper_RegisterBaseExporters_m1355390223_MetadataUsageId;
extern const Il2CppType* Single_t1397266774_0_0_0_var;
extern const Il2CppType* Double_t594665363_0_0_0_var;
extern const Il2CppType* Int64_t3736567304_0_0_0_var;
extern Il2CppClass* ImporterFunc_t3630937194_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__A_m3123255486_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__B_m3123219883_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__C_m3123314944_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__D_m3123024917_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__E_m3123374154_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__F_m3123338535_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__10_m2509882514_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__11_m613999264_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__12_m1159193330_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__13_m1427563268_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__14_m1419109062_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__15_m1703207628_MethodInfo_var;
extern const uint32_t JsonMapper_RegisterBaseImporters_m623152139_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t1780316962_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1305840448_MethodInfo_var;
extern const uint32_t JsonMapper_RegisterImporter_m3334975862_MetadataUsageId;
extern const uint32_t JsonMapper_ToWrapper_m1467426324_MetadataUsageId;
extern Il2CppClass* JsonMockWrapper_t82875095_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CReadSkipU3Em__0_m139886084_MetadataUsageId;
extern Il2CppClass* Byte_t1134296376_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2465617642_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__1_m3570407405_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__2_m600765210_MetadataUsageId;
extern Il2CppClass* DateTime_t3738529785_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__3_m939732965_MetadataUsageId;
extern Il2CppClass* Decimal_t2948259380_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__4_m266458088_MetadataUsageId;
extern Il2CppClass* SByte_t1669577662_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__5_m3328515821_MetadataUsageId;
extern Il2CppClass* Int16_t2552820387_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__6_m312889811_MetadataUsageId;
extern Il2CppClass* UInt16_t2177724958_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__7_m3877741659_MetadataUsageId;
extern Il2CppClass* UInt32_t2560061978_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__8_m225163408_MetadataUsageId;
extern Il2CppClass* UInt64_t4134040092_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__9_m1344668283_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__A_m3123255486_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__B_m3123219883_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__C_m3123314944_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__D_m3123024917_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__E_m3123374154_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__F_m3123338535_MetadataUsageId;
extern Il2CppClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__10_m2509882514_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__11_m613999264_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__12_m1159193330_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__13_m1427563268_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__14_m1419109062_MetadataUsageId;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__15_m1703207628_MetadataUsageId;
extern Il2CppClass* JsonReader_t836887441_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader__cctor_m1456809606_MetadataUsageId;
extern Il2CppClass* StringReader_t3465604688_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader__ctor_m333115852_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern Il2CppClass* Stack_1_t3794335208_il2cpp_TypeInfo_var;
extern Il2CppClass* Lexer_t1514038666_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m1742569847_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m399625717_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3529812268;
extern const uint32_t JsonReader__ctor_m601545185_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t921491721_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t385246372_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2631748502_MethodInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D0_0_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D1_1_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D2_2_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D3_3_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D4_4_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D5_5_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D6_6_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D7_7_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D8_8_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D9_9_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2DA_10_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2DB_11_FieldInfo_var;
extern const uint32_t JsonReader_PopulateParseTable_m350999017_MetadataUsageId;
extern Il2CppClass* IDictionary_2_t3680310408_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2032778390_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader_TableAddCol_m3551170424_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t3568926999_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3739908635_MethodInfo_var;
extern const uint32_t JsonReader_TableAddRow_m1968124223_MetadataUsageId;
extern const uint32_t JsonReader_ProcessNumber_m2606343002_MetadataUsageId;
extern const uint32_t JsonReader_ProcessSymbol_m137365372_MetadataUsageId;
extern Il2CppClass* KeyNotFoundException_t2292407383_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Clear_m1908282093_MethodInfo_var;
extern const MethodInfo* Stack_1_Peek_m10351423_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m681651409_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral320911726;
extern const uint32_t JsonReader_Read_m1579829852_MetadataUsageId;
extern Il2CppClass* NumberFormatInfo_t435877138_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter__cctor_m2709308669_MetadataUsageId;
extern Il2CppClass* StringBuilder_t1712802186_il2cpp_TypeInfo_var;
extern Il2CppClass* StringWriter_t802263757_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter__ctor_m3035373812_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3717129058;
extern Il2CppCodeGenString* _stringLiteral4103647921;
extern Il2CppCodeGenString* _stringLiteral1740842079;
extern Il2CppCodeGenString* _stringLiteral2856579791;
extern Il2CppCodeGenString* _stringLiteral393113856;
extern Il2CppCodeGenString* _stringLiteral4135352181;
extern const uint32_t JsonWriter_DoValidation_m2027671802_MetadataUsageId;
extern Il2CppClass* CharU5BU5D_t3528271667_il2cpp_TypeInfo_var;
extern Il2CppClass* Stack_1_t1854483454_il2cpp_TypeInfo_var;
extern Il2CppClass* WriterContext_t1011093999_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m227092194_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m1814405731_MethodInfo_var;
extern const uint32_t JsonWriter_Init_m1392617240_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3454842868;
extern Il2CppCodeGenString* _stringLiteral3455629300;
extern Il2CppCodeGenString* _stringLiteral3455498228;
extern Il2CppCodeGenString* _stringLiteral3454318580;
extern Il2CppCodeGenString* _stringLiteral3454580724;
extern Il2CppCodeGenString* _stringLiteral3455432692;
extern const uint32_t JsonWriter_PutString_m890241341_MetadataUsageId;
extern const uint32_t JsonWriter_ToString_m1507950332_MetadataUsageId;
extern const uint32_t JsonWriter_Write_m934090602_MetadataUsageId;
extern const uint32_t JsonWriter_Write_m2431965710_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1202628576;
extern const uint32_t JsonWriter_Write_m3439490280_MetadataUsageId;
extern const uint32_t JsonWriter_Write_m2044706063_MetadataUsageId;
extern const uint32_t Lexer__cctor_m3533321053_MetadataUsageId;
extern Il2CppClass* FsmContext_t2331368794_il2cpp_TypeInfo_var;
extern const uint32_t Lexer__ctor_m465008812_MetadataUsageId;
extern Il2CppClass* StateHandlerU5BU5D_t1323790106_il2cpp_TypeInfo_var;
extern Il2CppClass* StateHandler_t105866779_il2cpp_TypeInfo_var;
extern const MethodInfo* Lexer_State1_m551089695_MethodInfo_var;
extern const MethodInfo* Lexer_State2_m803286364_MethodInfo_var;
extern const MethodInfo* Lexer_State3_m1508224687_MethodInfo_var;
extern const MethodInfo* Lexer_State4_m1356451111_MethodInfo_var;
extern const MethodInfo* Lexer_State5_m4046667347_MethodInfo_var;
extern const MethodInfo* Lexer_State6_m1276813619_MethodInfo_var;
extern const MethodInfo* Lexer_State7_m1103110371_MethodInfo_var;
extern const MethodInfo* Lexer_State8_m4126769589_MethodInfo_var;
extern const MethodInfo* Lexer_State9_m3407663359_MethodInfo_var;
extern const MethodInfo* Lexer_State10_m3929854498_MethodInfo_var;
extern const MethodInfo* Lexer_State11_m3893158315_MethodInfo_var;
extern const MethodInfo* Lexer_State12_m4025570652_MethodInfo_var;
extern const MethodInfo* Lexer_State13_m3988562965_MethodInfo_var;
extern const MethodInfo* Lexer_State14_m3354309022_MethodInfo_var;
extern const MethodInfo* Lexer_State15_m3741500279_MethodInfo_var;
extern const MethodInfo* Lexer_State16_m3863621896_MethodInfo_var;
extern const MethodInfo* Lexer_State17_m3264355937_MethodInfo_var;
extern const MethodInfo* Lexer_State18_m3616214826_MethodInfo_var;
extern const MethodInfo* Lexer_State19_m3579281075_MethodInfo_var;
extern const MethodInfo* Lexer_State20_m3151936696_MethodInfo_var;
extern const MethodInfo* Lexer_State21_m3115240513_MethodInfo_var;
extern const MethodInfo* Lexer_State22_m3247652850_MethodInfo_var;
extern const MethodInfo* Lexer_State23_m3210645163_MethodInfo_var;
extern const MethodInfo* Lexer_State24_m2576391220_MethodInfo_var;
extern const MethodInfo* Lexer_State25_m2963582477_MethodInfo_var;
extern const MethodInfo* Lexer_State26_m3085704094_MethodInfo_var;
extern const MethodInfo* Lexer_State27_m2486438135_MethodInfo_var;
extern const MethodInfo* Lexer_State28_m2838297024_MethodInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2DC_12_FieldInfo_var;
extern const uint32_t Lexer_PopulateFsmTables_m1759663568_MetadataUsageId;
extern const uint32_t Lexer_ProcessEscChar_m4259284362_MetadataUsageId;
extern const uint32_t Lexer_State21_m3115240513_MetadataUsageId;
extern const uint32_t Lexer_State22_m3247652850_MetadataUsageId;
extern const uint32_t Lexer_NextToken_m2425003787_MetadataUsageId;
extern const uint32_t ObjectMetadata_get_ElementType_m991775292_MetadataUsageId;
extern Il2CppClass* DictionaryEntry_t3123975638_il2cpp_TypeInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_get_Current_m1860742452_MetadataUsageId;
extern Il2CppClass* IEnumerator_1_t4140357341_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m4274503699_MethodInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_get_Entry_m2900755332_MetadataUsageId;
extern const uint32_t OrderedDictionaryEnumerator_get_Key_m3653726345_MetadataUsageId;
extern const uint32_t OrderedDictionaryEnumerator_get_Value_m2497007576_MetadataUsageId;
extern Il2CppClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_MoveNext_m1132456593_MetadataUsageId;
extern const uint32_t OrderedDictionaryEnumerator_Reset_m1965424276_MetadataUsageId;

// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1461822886  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropertyInfo_t * m_Items[1];

public:
	inline PropertyInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PropertyInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PropertyInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline PropertyInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PropertyInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PropertyInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t390618515  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t1861056598 * m_Items[1];

public:
	inline ParameterInfo_t1861056598 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t1861056598 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t1861056598 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t1861056598 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t1861056598 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t1861056598 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t846150980  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FieldInfo_t * m_Items[1];

public:
	inline FieldInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FieldInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FieldInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FieldInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t3940880105  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t385246372  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// LitJson.Lexer/StateHandler[]
struct StateHandlerU5BU5D_t1323790106  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StateHandler_t105866779 * m_Items[1];

public:
	inline StateHandler_t105866779 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline StateHandler_t105866779 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, StateHandler_t105866779 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline StateHandler_t105866779 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline StateHandler_t105866779 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, StateHandler_t105866779 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(!0,!1)
extern "C"  void KeyValuePair_2__ctor_m727165395_gshared (KeyValuePair_2_t2530217319 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1328507389_gshared (KeyValuePair_2_t2530217319 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void List_1__ctor_m2524100478_gshared (List_1_t4002292061 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,LitJson.ArrayMetadata>::.ctor()
extern "C"  void Dictionary_2__ctor_m1364587687_gshared (Dictionary_2_t2241695223 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,LitJson.ObjectMetadata>::.ctor()
extern "C"  void Dictionary_2__ctor_m1704891675_gshared (Dictionary_2_t618723510 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,LitJson.PropertyMetadata>::.ctor()
extern "C"  void Dictionary_2__ctor_m254134071_gshared (Dictionary_2_t779879461 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Stack`1<System.Int32>::.ctor()
extern "C"  void Stack_1__ctor_m1742569847_gshared (Stack_1_t3794335208 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Stack`1<System.Int32>::Push(!0)
extern "C"  void Stack_1_Push_m399625717_gshared (Stack_1_t3794335208 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2601736566_gshared (Dictionary_2_t1968819495 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Stack`1<System.Int32>::Clear()
extern "C"  void Stack_1_Clear_m1908282093_gshared (Stack_1_t3794335208 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.Stack`1<System.Int32>::Peek()
extern "C"  int32_t Stack_1_Peek_m10351423_gshared (Stack_1_t3794335208 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.Stack`1<System.Int32>::Pop()
extern "C"  int32_t Stack_1_Pop_m681651409_gshared (Stack_1_t3794335208 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m3164958980_gshared (Stack_1_t3923495619 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C"  void Stack_1_Push_m1669856732_gshared (Stack_1_t3923495619 * __this, Il2CppObject * p0, const MethodInfo* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3464904234_gshared (KeyValuePair_2_t2530217319 * __this, const MethodInfo* method);

// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type LitJson.ArrayMetadata::get_ElementType()
extern "C"  Type_t * ArrayMetadata_get_ElementType_m2810930502 (ArrayMetadata_t894288939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ArrayMetadata::set_ElementType(System.Type)
extern "C"  void ArrayMetadata_set_ElementType_m2992145657 (ArrayMetadata_t894288939 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.ArrayMetadata::get_IsArray()
extern "C"  bool ArrayMetadata_get_IsArray_m3389222635 (ArrayMetadata_t894288939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ArrayMetadata::set_IsArray(System.Boolean)
extern "C"  void ArrayMetadata_set_IsArray_m2441146849 (ArrayMetadata_t894288939 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.ArrayMetadata::get_IsList()
extern "C"  bool ArrayMetadata_get_IsList_m2672124584 (ArrayMetadata_t894288939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ArrayMetadata::set_IsList(System.Boolean)
extern "C"  void ArrayMetadata_set_IsList_m3822380729 (ArrayMetadata_t894288939 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ExporterFunc::Invoke(System.Object,LitJson.JsonWriter)
extern "C"  void ExporterFunc_Invoke_m443196456 (ExporterFunc_t1851311465 * __this, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.ImporterFunc::Invoke(System.Object)
extern "C"  Il2CppObject * ImporterFunc_Invoke_m1838490826 (ImporterFunc_t3630937194 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonData::get_Count()
extern "C"  int32_t JsonData_get_Count_m3094119778 (JsonData_t1524858407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection LitJson.JsonData::EnsureCollection()
extern "C"  Il2CppObject * JsonData_EnsureCollection_m1446037538 (JsonData_t1524858407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList LitJson.JsonData::EnsureList()
extern "C"  Il2CppObject * JsonData_EnsureList_m1549285368 (JsonData_t1524858407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary LitJson.JsonData::EnsureDictionary()
extern "C"  Il2CppObject * JsonData_EnsureDictionary_m3057468608 (JsonData_t1524858407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonData::ToJsonData(System.Object)
extern "C"  JsonData_t1524858407 * JsonData_ToJsonData_m69846785 (JsonData_t1524858407 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::set_Item(System.String,LitJson.JsonData)
extern "C"  void JsonData_set_Item_m1920107000 (JsonData_t1524858407 * __this, String_t* ___prop_name0, JsonData_t1524858407 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::set_Item(System.Int32,LitJson.JsonData)
extern "C"  void JsonData_set_Item_m1179907761 (JsonData_t1524858407 * __this, int32_t ___index0, JsonData_t1524858407 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>::.ctor(!0,!1)
#define KeyValuePair_2__ctor_m630695613(__this, p0, p1, method) ((  void (*) (KeyValuePair_2_t3707786873 *, String_t*, JsonData_t1524858407 *, const MethodInfo*))KeyValuePair_2__ctor_m727165395_gshared)(__this, p0, p1, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>::get_Key()
#define KeyValuePair_2_get_Key_m913646779(__this, method) ((  String_t* (*) (KeyValuePair_2_t3707786873 *, const MethodInfo*))KeyValuePair_2_get_Key_m1328507389_gshared)(__this, method)
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonData::Add(System.Object)
extern "C"  int32_t JsonData_Add_m2952029759 (JsonData_t1524858407 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.OrderedDictionaryEnumerator::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>)
extern "C"  void OrderedDictionaryEnumerator__ctor_m3267133839 (OrderedDictionaryEnumerator_t386339177 * __this, Il2CppObject* ___enumerator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m237278729 (InvalidOperationException_t56020091 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,LitJson.JsonData>::.ctor()
#define Dictionary_2__ctor_m1325424069(__this, method) ((  void (*) (Dictionary_2_t1310114706 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::.ctor()
#define List_1__ctor_m2572897465(__this, method) ((  void (*) (List_1_t884894319 *, const MethodInfo*))List_1__ctor_m2524100478_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<LitJson.JsonData>::.ctor()
#define List_1__ctor_m1532675647(__this, method) ((  void (*) (List_1_t2996933149 *, const MethodInfo*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void LitJson.JsonData::.ctor(System.Object)
extern "C"  void JsonData__ctor_m3866765393 (JsonData_t1524858407 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Equals(System.String)
extern "C"  bool String_Equals_m2270643605 (String_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int32::Equals(System.Int32)
extern "C"  bool Int32_Equals_m2976157357 (int32_t* __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int64::Equals(System.Int64)
extern "C"  bool Int64_Equals_m680137412 (int64_t* __this, int64_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Double::Equals(System.Double)
extern "C"  bool Double_Equals_m2309369974 (double* __this, double p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Boolean::Equals(System.Boolean)
extern "C"  bool Boolean_Equals_m535526264 (bool* __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m2664721875 (bool* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Double::ToString()
extern "C"  String_t* Double_ToString_m1229922074 (double* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m141394615 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Int64::ToString()
extern "C"  String_t* Int64_ToString_m2986581816 (int64_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2844511972 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ApplicationException::.ctor(System.String,System.Exception)
extern "C"  void ApplicationException__ctor_m692455299 (ApplicationException_t2339761290 * __this, String_t* p0, Exception_t1436737249 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ApplicationException::.ctor(System.String)
extern "C"  void ApplicationException__ctor_m2517758450 (ApplicationException_t2339761290 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Type,LitJson.ArrayMetadata>::.ctor()
#define Dictionary_2__ctor_m2431295564(__this, method) ((  void (*) (Dictionary_2_t3338636003 *, const MethodInfo*))Dictionary_2__ctor_m1364587687_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::.ctor()
#define Dictionary_2__ctor_m2076397116(__this, method) ((  void (*) (Dictionary_2_t935204471 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,LitJson.ObjectMetadata>::.ctor()
#define Dictionary_2__ctor_m1674229861(__this, method) ((  void (*) (Dictionary_2_t1715664290 *, const MethodInfo*))Dictionary_2__ctor_m1704891675_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>>::.ctor()
#define Dictionary_2__ctor_m2292515434(__this, method) ((  void (*) (Dictionary_2_t3692140024 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void LitJson.JsonWriter::.ctor()
extern "C"  void JsonWriter__ctor_m3035373812 (JsonWriter_t3570089748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.DateTimeFormatInfo System.Globalization.DateTimeFormatInfo::get_InvariantInfo()
extern "C"  DateTimeFormatInfo_t2405853701 * DateTimeFormatInfo_get_InvariantInfo_m2329875772 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Type,LitJson.ExporterFunc>::.ctor()
#define Dictionary_2__ctor_m2735992426(__this, method) ((  void (*) (Dictionary_2_t691233 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::.ctor()
#define Dictionary_2__ctor_m1523576929(__this, method) ((  void (*) (Dictionary_2_t2688515417 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void LitJson.JsonMapper::RegisterBaseExporters()
extern "C"  void JsonMapper_RegisterBaseExporters_m1355390223 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::RegisterBaseImporters()
extern "C"  void JsonMapper_RegisterBaseImporters_m623152139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsArray()
extern "C"  bool Type_get_IsArray_m2591212821 (Type_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetInterface(System.String)
extern "C"  Type_t * Type_GetInterface_m23870712 (Type_t * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo[] System.Type::GetProperties()
extern "C"  PropertyInfoU5BU5D_t1461822886* Type_GetProperties_m1538559489 (Type_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m215368492 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2249409497 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m3585316909 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ObjectMetadata::set_IsDictionary(System.Boolean)
extern "C"  void ObjectMetadata_set_IsDictionary_m2176094566 (ObjectMetadata_t3566284522 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,LitJson.PropertyMetadata>::.ctor()
#define Dictionary_2__ctor_m3146315127(__this, method) ((  void (*) (Dictionary_2_t3512696772 *, const MethodInfo*))Dictionary_2__ctor_m254134071_gshared)(__this, method)
// System.Void LitJson.ObjectMetadata::set_Properties(System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>)
extern "C"  void ObjectMetadata_set_Properties_m3670768576 (ObjectMetadata_t3566284522 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ObjectMetadata::set_ElementType(System.Type)
extern "C"  void ObjectMetadata_set_ElementType_m1776330551 (ObjectMetadata_t3566284522 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata> LitJson.ObjectMetadata::get_Properties()
extern "C"  Il2CppObject* ObjectMetadata_get_Properties_m3723938162 (ObjectMetadata_t3566284522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] System.Type::GetFields()
extern "C"  FieldInfoU5BU5D_t846150980* Type_GetFields_m3709891696 (Type_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.MethodInfo>::.ctor()
#define Dictionary_2__ctor_m635195254(__this, method) ((  void (*) (Dictionary_2_t27006016 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Type[])
extern "C"  MethodInfo_t * Type_GetMethod_m1479779718 (Type_t * __this, String_t* p0, TypeU5BU5D_t3940880105* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonReader::Read()
extern "C"  bool JsonReader_Read_m1579829852 (JsonReader_t836887441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonToken LitJson.JsonReader::get_Token()
extern "C"  int32_t JsonReader_get_Token_m1333614796 (JsonReader_t836887441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Nullable::GetUnderlyingType(System.Type)
extern "C"  Type_t * Nullable_GetUnderlyingType_m3905033790 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsClass()
extern "C"  bool Type_get_IsClass_m589177581 (Type_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonException::.ctor(System.String)
extern "C"  void JsonException__ctor_m2214533437 (JsonException_t3682484112 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonReader::get_Value()
extern "C"  Il2CppObject * JsonReader_get_Value_m3034661202 (JsonReader_t836887441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsEnum()
extern "C"  bool Type_get_IsEnum_m208091508 (Type_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Enum::ToObject(System.Type,System.Object)
extern "C"  Il2CppObject * Enum_ToObject_m1628250250 (Il2CppObject * __this /* static, unused */, Type_t * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo LitJson.JsonMapper::GetConvOp(System.Type,System.Type)
extern "C"  MethodInfo_t * JsonMapper_GetConvOp_m1492689601 (Il2CppObject * __this /* static, unused */, Type_t * ___t10, Type_t * ___t21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[])
extern "C"  Il2CppObject * MethodBase_Invoke_m1776411915 (MethodBase_t609368412 * __this, Il2CppObject * p0, ObjectU5BU5D_t2843939325* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
extern "C"  String_t* String_Format_m3339413201 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, Il2CppObject * p2, Il2CppObject * p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::AddArrayMetadata(System.Type)
extern "C"  void JsonMapper_AddArrayMetadata_m3968694626 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type)
extern "C"  Il2CppObject * Activator_CreateInstance_m3631483688 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor()
extern "C"  void ArrayList__ctor_m4254721275 (ArrayList_t2718874744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::ReadValue(System.Type,LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ReadValue_m1193038115 (Il2CppObject * __this /* static, unused */, Type_t * ___inst_type0, JsonReader_t836887441 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array System.Array::CreateInstance(System.Type,System.Int32)
extern "C"  Il2CppArray * Array_CreateInstance_m2750085942 (Il2CppObject * __this /* static, unused */, Type_t * p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::SetValue(System.Object,System.Int32)
extern "C"  void Array_SetValue_m3412255035 (Il2CppArray * __this, Il2CppObject * p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::AddObjectMetadata(System.Type)
extern "C"  void JsonMapper_AddObjectMetadata_m194540947 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object)
extern "C"  void FieldInfo_SetValue_m2460171138 (FieldInfo_t * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.ObjectMetadata::get_IsDictionary()
extern "C"  bool ObjectMetadata_get_IsDictionary_m1062828228 (ObjectMetadata_t3566284522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonReader::get_SkipNonMembers()
extern "C"  bool JsonReader_get_SkipNonMembers_m3135707401 (JsonReader_t836887441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C"  String_t* String_Format_m2556382932 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::ReadSkip(LitJson.JsonReader)
extern "C"  void JsonMapper_ReadSkip_m3223759513 (Il2CppObject * __this /* static, unused */, JsonReader_t836887441 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type LitJson.ObjectMetadata::get_ElementType()
extern "C"  Type_t * ObjectMetadata_get_ElementType_m991775292 (ObjectMetadata_t3566284522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.IJsonWrapper LitJson.WrapperFactory::Invoke()
extern "C"  Il2CppObject * WrapperFactory_Invoke_m1494838554 (WrapperFactory_t2158548929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.IJsonWrapper LitJson.JsonMapper::ReadValue(LitJson.WrapperFactory,LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ReadValue_m3060360668 (Il2CppObject * __this /* static, unused */, WrapperFactory_t2158548929 * ___factory0, JsonReader_t836887441 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.WrapperFactory::.ctor(System.Object,System.IntPtr)
extern "C"  void WrapperFactory__ctor_m283261507 (WrapperFactory_t2158548929 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.IJsonWrapper LitJson.JsonMapper::ToWrapper(LitJson.WrapperFactory,LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ToWrapper_m1467426324 (Il2CppObject * __this /* static, unused */, WrapperFactory_t2158548929 * ___factory0, JsonReader_t836887441 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ExporterFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc__ctor_m1737319807 (ExporterFunc_t1851311465 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ImporterFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void ImporterFunc__ctor_m1884320379 (ImporterFunc_t3630937194 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::RegisterImporter(System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>,System.Type,System.Type,LitJson.ImporterFunc)
extern "C"  void JsonMapper_RegisterImporter_m3334975862 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___table0, Type_t * ___json_type1, Type_t * ___value_type2, ImporterFunc_t3630937194 * ___importer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Type,LitJson.ImporterFunc>::.ctor()
#define Dictionary_2__ctor_m1305840448(__this, method) ((  void (*) (Dictionary_2_t1780316962 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void LitJson.JsonMockWrapper::.ctor()
extern "C"  void JsonMockWrapper__ctor_m969609950 (JsonMockWrapper_t82875095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Convert::ToInt32(System.Byte)
extern "C"  int32_t Convert_ToInt32_m2505564049 (Il2CppObject * __this /* static, unused */, uint8_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonWriter::Write(System.Int32)
extern "C"  void JsonWriter_Write_m2431965710 (JsonWriter_t3570089748 * __this, int32_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToString(System.Char)
extern "C"  String_t* Convert_ToString_m1553911740 (Il2CppObject * __this /* static, unused */, Il2CppChar p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonWriter::Write(System.String)
extern "C"  void JsonWriter_Write_m3439490280 (JsonWriter_t3570089748 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToString(System.DateTime,System.IFormatProvider)
extern "C"  String_t* Convert_ToString_m41256750 (Il2CppObject * __this /* static, unused */, DateTime_t3738529785  p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonWriter::Write(System.Decimal)
extern "C"  void JsonWriter_Write_m934090602 (JsonWriter_t3570089748 * __this, Decimal_t2948259380  ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Convert::ToInt32(System.SByte)
extern "C"  int32_t Convert_ToInt32_m1405693041 (Il2CppObject * __this /* static, unused */, int8_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Convert::ToInt32(System.Int16)
extern "C"  int32_t Convert_ToInt32_m1085744762 (Il2CppObject * __this /* static, unused */, int16_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Convert::ToInt32(System.UInt16)
extern "C"  int32_t Convert_ToInt32_m1987758323 (Il2CppObject * __this /* static, unused */, uint16_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Convert::ToUInt64(System.UInt32)
extern "C"  uint64_t Convert_ToUInt64_m1745056470 (Il2CppObject * __this /* static, unused */, uint32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonWriter::Write(System.UInt64)
extern "C"  void JsonWriter_Write_m2044706063 (JsonWriter_t3570089748 * __this, uint64_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Convert::ToByte(System.Int32)
extern "C"  uint8_t Convert_ToByte_m1734770211 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Convert::ToUInt64(System.Int32)
extern "C"  uint64_t Convert_ToUInt64_m786726853 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Convert::ToSByte(System.Int32)
extern "C"  int8_t Convert_ToSByte_m2653418303 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Convert::ToInt16(System.Int32)
extern "C"  int16_t Convert_ToInt16_m4174308322 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Convert::ToUInt16(System.Int32)
extern "C"  uint16_t Convert_ToUInt16_m3515425647 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Convert::ToUInt32(System.Int32)
extern "C"  uint32_t Convert_ToUInt32_m2215525276 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Convert::ToSingle(System.Int32)
extern "C"  float Convert_ToSingle_m3635698920 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Convert::ToDouble(System.Int32)
extern "C"  double Convert_ToDouble_m2924063577 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Convert::ToDecimal(System.Double)
extern "C"  Decimal_t2948259380  Convert_ToDecimal_m841368097 (Il2CppObject * __this /* static, unused */, double p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Convert::ToUInt32(System.Int64)
extern "C"  uint32_t Convert_ToUInt32_m2194577773 (Il2CppObject * __this /* static, unused */, int64_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Convert::ToChar(System.String)
extern "C"  Il2CppChar Convert_ToChar_m85718752 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Convert::ToDateTime(System.String,System.IFormatProvider)
extern "C"  DateTime_t3738529785  Convert_ToDateTime_m3802186295 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonReader::PopulateParseTable()
extern "C"  void JsonReader_PopulateParseTable_m350999017 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StringReader::.ctor(System.String)
extern "C"  void StringReader__ctor_m126993932 (StringReader_t3465604688 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonReader::.ctor(System.IO.TextReader,System.Boolean)
extern "C"  void JsonReader__ctor_m601545185 (JsonReader_t836887441 * __this, TextReader_t283511965 * ___reader0, bool ___owned1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1<System.Int32>::.ctor()
#define Stack_1__ctor_m1742569847(__this, method) ((  void (*) (Stack_1_t3794335208 *, const MethodInfo*))Stack_1__ctor_m1742569847_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Int32>::Push(!0)
#define Stack_1_Push_m399625717(__this, p0, method) ((  void (*) (Stack_1_t3794335208 *, int32_t, const MethodInfo*))Stack_1_Push_m399625717_gshared)(__this, p0, method)
// System.Void LitJson.Lexer::.ctor(System.IO.TextReader)
extern "C"  void Lexer__ctor_m465008812 (Lexer_t1514038666 * __this, TextReader_t283511965 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>::.ctor()
#define Dictionary_2__ctor_m2631748502(__this, method) ((  void (*) (Dictionary_2_t921491721 *, const MethodInfo*))Dictionary_2__ctor_m2601736566_gshared)(__this, method)
// System.Void LitJson.JsonReader::TableAddRow(LitJson.ParserToken)
extern "C"  void JsonReader_TableAddRow_m1968124223 (Il2CppObject * __this /* static, unused */, int32_t ___rule0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonReader::TableAddCol(LitJson.ParserToken,System.Int32,System.Int32[])
extern "C"  void JsonReader_TableAddCol_m3551170424 (Il2CppObject * __this /* static, unused */, int32_t ___row0, int32_t ___col1, Int32U5BU5D_t385246372* ___symbols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3117905507 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, RuntimeFieldHandle_t1871169219  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32[]>::.ctor()
#define Dictionary_2__ctor_m3739908635(__this, method) ((  void (*) (Dictionary_2_t3568926999 *, const MethodInfo*))Dictionary_2__ctor_m2601736566_gshared)(__this, method)
// System.Int32 System.String::IndexOf(System.Char)
extern "C"  int32_t String_IndexOf_m363431711 (String_t* __this, Il2CppChar p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Double::TryParse(System.String,System.Double&)
extern "C"  bool Double_TryParse_m3021978240 (Il2CppObject * __this /* static, unused */, String_t* p0, double* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int32::TryParse(System.String,System.Int32&)
extern "C"  bool Int32_TryParse_m2404707562 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int64::TryParse(System.String,System.Int64&)
extern "C"  bool Int64_TryParse_m2208578514 (Il2CppObject * __this /* static, unused */, String_t* p0, int64_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt64::TryParse(System.String,System.UInt64&)
extern "C"  bool UInt64_TryParse_m2263420204 (Il2CppObject * __this /* static, unused */, String_t* p0, uint64_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.Lexer::get_StringValue()
extern "C"  String_t* Lexer_get_StringValue_m673320728 (Lexer_t1514038666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonReader::ProcessNumber(System.String)
extern "C"  void JsonReader_ProcessNumber_m2606343002 (JsonReader_t836887441 * __this, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.Lexer::NextToken()
extern "C"  bool Lexer_NextToken_m2425003787 (Lexer_t1514038666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.Lexer::get_EndOfInput()
extern "C"  bool Lexer_get_EndOfInput_m1249127404 (Lexer_t1514038666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonReader::Close()
extern "C"  void JsonReader_Close_m364037064 (JsonReader_t836887441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.Lexer::get_Token()
extern "C"  int32_t Lexer_get_Token_m1219310079 (Lexer_t1514038666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1<System.Int32>::Clear()
#define Stack_1_Clear_m1908282093(__this, method) ((  void (*) (Stack_1_t3794335208 *, const MethodInfo*))Stack_1_Clear_m1908282093_gshared)(__this, method)
// System.Boolean LitJson.JsonReader::ReadToken()
extern "C"  bool JsonReader_ReadToken_m67845004 (JsonReader_t836887441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Stack`1<System.Int32>::Peek()
#define Stack_1_Peek_m10351423(__this, method) ((  int32_t (*) (Stack_1_t3794335208 *, const MethodInfo*))Stack_1_Peek_m10351423_gshared)(__this, method)
// !0 System.Collections.Generic.Stack`1<System.Int32>::Pop()
#define Stack_1_Pop_m681651409(__this, method) ((  int32_t (*) (Stack_1_t3794335208 *, const MethodInfo*))Stack_1_Pop_m681651409_gshared)(__this, method)
// System.Void LitJson.JsonReader::ProcessSymbol()
extern "C"  void JsonReader_ProcessSymbol_m137365372 (JsonReader_t836887441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonException::.ctor(LitJson.ParserToken,System.Exception)
extern "C"  void JsonException__ctor_m4037883734 (JsonException_t3682484112 * __this, int32_t ___token0, Exception_t1436737249 * ___inner_exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.NumberFormatInfo System.Globalization.NumberFormatInfo::get_InvariantInfo()
extern "C"  NumberFormatInfo_t435877138 * NumberFormatInfo_get_InvariantInfo_m349577018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor()
extern "C"  void StringBuilder__ctor_m3121283359 (StringBuilder_t1712802186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StringWriter::.ctor(System.Text.StringBuilder)
extern "C"  void StringWriter__ctor_m1259274362 (StringWriter_t802263757 * __this, StringBuilder_t1712802186 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonWriter::Init()
extern "C"  void JsonWriter_Init_m1392617240 (JsonWriter_t3570089748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1<LitJson.WriterContext>::.ctor()
#define Stack_1__ctor_m227092194(__this, method) ((  void (*) (Stack_1_t1854483454 *, const MethodInfo*))Stack_1__ctor_m3164958980_gshared)(__this, method)
// System.Void LitJson.WriterContext::.ctor()
extern "C"  void WriterContext__ctor_m2317705773 (WriterContext_t1011093999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1<LitJson.WriterContext>::Push(!0)
#define Stack_1_Push_m1814405731(__this, p0, method) ((  void (*) (Stack_1_t1854483454 *, WriterContext_t1011093999 *, const MethodInfo*))Stack_1_Push_m1669856732_gshared)(__this, p0, method)
// System.Void LitJson.JsonWriter::PutNewline(System.Boolean)
extern "C"  void JsonWriter_PutNewline_m1383209577 (JsonWriter_t3570089748 * __this, bool ___add_comma0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonWriter::Put(System.String)
extern "C"  void JsonWriter_Put_m2154223978 (JsonWriter_t3570089748 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m2986988803 (String_t* __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonWriter::IntToHex(System.Int32,System.Char[])
extern "C"  void JsonWriter_IntToHex_m1207120023 (Il2CppObject * __this /* static, unused */, int32_t ___n0, CharU5BU5D_t3528271667* ___hex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonWriter::DoValidation(LitJson.Condition)
extern "C"  void JsonWriter_DoValidation_m2027671802 (JsonWriter_t3570089748 * __this, int32_t ___cond0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonWriter::PutNewline()
extern "C"  void JsonWriter_PutNewline_m2576790467 (JsonWriter_t3570089748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToString(System.Decimal,System.IFormatProvider)
extern "C"  String_t* Convert_ToString_m3734943936 (Il2CppObject * __this /* static, unused */, Decimal_t2948259380  p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToString(System.Int32,System.IFormatProvider)
extern "C"  String_t* Convert_ToString_m2614817407 (Il2CppObject * __this /* static, unused */, int32_t p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonWriter::PutString(System.String)
extern "C"  void JsonWriter_PutString_m890241341 (JsonWriter_t3570089748 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToString(System.UInt64,System.IFormatProvider)
extern "C"  String_t* Convert_ToString_m301771913 (Il2CppObject * __this /* static, unused */, uint64_t p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.Lexer::PopulateFsmTables()
extern "C"  void Lexer_PopulateFsmTables_m1759663568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
extern "C"  void StringBuilder__ctor_m2367297767 (StringBuilder_t1712802186 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.FsmContext::.ctor()
extern "C"  void FsmContext__ctor_m1086078664 (FsmContext_t2331368794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.Lexer/StateHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void StateHandler__ctor_m2721920384 (StateHandler_t105866779 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Convert::ToChar(System.Int32)
extern "C"  Il2CppChar Convert_ToChar_m4189066566 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Append_m2383614642 (StringBuilder_t1712802186 * __this, Il2CppChar p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.Lexer::GetChar()
extern "C"  bool Lexer_GetChar_m657915143 (Lexer_t1514038666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.Lexer::UngetChar()
extern "C"  void Lexer_UngetChar_m1644879734 (Lexer_t1514038666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char LitJson.Lexer::ProcessEscChar(System.Int32)
extern "C"  Il2CppChar Lexer_ProcessEscChar_m4259284362 (Il2CppObject * __this /* static, unused */, int32_t ___esc_char0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.Lexer::HexValue(System.Int32)
extern "C"  int32_t Lexer_HexValue_m4177870290 (Il2CppObject * __this /* static, unused */, int32_t ___digit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.Lexer::NextChar()
extern "C"  int32_t Lexer_NextChar_m2082205690 (Lexer_t1514038666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.Lexer/StateHandler::Invoke(LitJson.FsmContext)
extern "C"  bool StateHandler_Invoke_m2992258958 (StateHandler_t105866779 * __this, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonException::.ctor(System.Int32)
extern "C"  void JsonException__ctor_m2210441219 (JsonException_t3682484112 * __this, int32_t ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.StringBuilder::get_Length()
extern "C"  int32_t StringBuilder_get_Length_m3238060835 (StringBuilder_t1712802186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Remove(System.Int32,System.Int32)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Remove_m940064945 (StringBuilder_t1712802186 * __this, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry LitJson.OrderedDictionaryEnumerator::get_Entry()
extern "C"  DictionaryEntry_t3123975638  OrderedDictionaryEnumerator_get_Entry_m2900755332 (OrderedDictionaryEnumerator_t386339177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !1 System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>::get_Value()
#define KeyValuePair_2_get_Value_m4274503699(__this, method) ((  JsonData_t1524858407 * (*) (KeyValuePair_2_t3707786873 *, const MethodInfo*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// System.Void System.Collections.DictionaryEntry::.ctor(System.Object,System.Object)
extern "C"  void DictionaryEntry__ctor_m2585376310 (DictionaryEntry_t3123975638 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t894288939_marshal_pinvoke(const ArrayMetadata_t894288939& unmarshaled, ArrayMetadata_t894288939_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ArrayMetadata_t894288939_marshal_pinvoke_back(const ArrayMetadata_t894288939_marshaled_pinvoke& marshaled, ArrayMetadata_t894288939& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t894288939_marshal_pinvoke_cleanup(ArrayMetadata_t894288939_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t894288939_marshal_com(const ArrayMetadata_t894288939& unmarshaled, ArrayMetadata_t894288939_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ArrayMetadata_t894288939_marshal_com_back(const ArrayMetadata_t894288939_marshaled_com& marshaled, ArrayMetadata_t894288939& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t894288939_marshal_com_cleanup(ArrayMetadata_t894288939_marshaled_com& marshaled)
{
}
// System.Type LitJson.ArrayMetadata::get_ElementType()
extern "C"  Type_t * ArrayMetadata_get_ElementType_m2810930502 (ArrayMetadata_t894288939 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayMetadata_get_ElementType_m2810930502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = __this->get_element_type_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(JsonData_t1524858407_0_0_0_var), /*hidden argument*/NULL);
		return L_1;
	}

IL_0016:
	{
		Type_t * L_2 = __this->get_element_type_0();
		return L_2;
	}
}
extern "C"  Type_t * ArrayMetadata_get_ElementType_m2810930502_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArrayMetadata_t894288939 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t894288939 *>(__this + 1);
	return ArrayMetadata_get_ElementType_m2810930502(_thisAdjusted, method);
}
// System.Void LitJson.ArrayMetadata::set_ElementType(System.Type)
extern "C"  void ArrayMetadata_set_ElementType_m2992145657 (ArrayMetadata_t894288939 * __this, Type_t * ___value0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set_element_type_0(L_0);
		return;
	}
}
extern "C"  void ArrayMetadata_set_ElementType_m2992145657_AdjustorThunk (Il2CppObject * __this, Type_t * ___value0, const MethodInfo* method)
{
	ArrayMetadata_t894288939 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t894288939 *>(__this + 1);
	ArrayMetadata_set_ElementType_m2992145657(_thisAdjusted, ___value0, method);
}
// System.Boolean LitJson.ArrayMetadata::get_IsArray()
extern "C"  bool ArrayMetadata_get_IsArray_m3389222635 (ArrayMetadata_t894288939 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_is_array_1();
		return L_0;
	}
}
extern "C"  bool ArrayMetadata_get_IsArray_m3389222635_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArrayMetadata_t894288939 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t894288939 *>(__this + 1);
	return ArrayMetadata_get_IsArray_m3389222635(_thisAdjusted, method);
}
// System.Void LitJson.ArrayMetadata::set_IsArray(System.Boolean)
extern "C"  void ArrayMetadata_set_IsArray_m2441146849 (ArrayMetadata_t894288939 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_is_array_1(L_0);
		return;
	}
}
extern "C"  void ArrayMetadata_set_IsArray_m2441146849_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	ArrayMetadata_t894288939 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t894288939 *>(__this + 1);
	ArrayMetadata_set_IsArray_m2441146849(_thisAdjusted, ___value0, method);
}
// System.Boolean LitJson.ArrayMetadata::get_IsList()
extern "C"  bool ArrayMetadata_get_IsList_m2672124584 (ArrayMetadata_t894288939 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_is_list_2();
		return L_0;
	}
}
extern "C"  bool ArrayMetadata_get_IsList_m2672124584_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArrayMetadata_t894288939 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t894288939 *>(__this + 1);
	return ArrayMetadata_get_IsList_m2672124584(_thisAdjusted, method);
}
// System.Void LitJson.ArrayMetadata::set_IsList(System.Boolean)
extern "C"  void ArrayMetadata_set_IsList_m3822380729 (ArrayMetadata_t894288939 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_is_list_2(L_0);
		return;
	}
}
extern "C"  void ArrayMetadata_set_IsList_m3822380729_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	ArrayMetadata_t894288939 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t894288939 *>(__this + 1);
	ArrayMetadata_set_IsList_m3822380729(_thisAdjusted, ___value0, method);
}
// System.Void LitJson.ExporterFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc__ctor_m1737319807 (ExporterFunc_t1851311465 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc::Invoke(System.Object,LitJson.JsonWriter)
extern "C"  void ExporterFunc_Invoke_m443196456 (ExporterFunc_t1851311465 * __this, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_Invoke_m443196456((ExporterFunc_t1851311465 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc::BeginInvoke(System.Object,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExporterFunc_BeginInvoke_m2744656880 (ExporterFunc_t1851311465 * __this, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, AsyncCallback_t3962456242 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___obj0;
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_EndInvoke_m2774891957 (ExporterFunc_t1851311465 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.FsmContext::.ctor()
extern "C"  void FsmContext__ctor_m1086078664 (FsmContext_t2331368794 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.ImporterFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void ImporterFunc__ctor_m1884320379 (ImporterFunc_t3630937194 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object LitJson.ImporterFunc::Invoke(System.Object)
extern "C"  Il2CppObject * ImporterFunc_Invoke_m1838490826 (ImporterFunc_t3630937194 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ImporterFunc_Invoke_m1838490826((ImporterFunc_t3630937194 *)__this->get_prev_9(),___input0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ImporterFunc::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ImporterFunc_BeginInvoke_m2499074257 (ImporterFunc_t3630937194 * __this, Il2CppObject * ___input0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___input0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Object LitJson.ImporterFunc::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ImporterFunc_EndInvoke_m513546293 (ImporterFunc_t3630937194 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Int32 LitJson.JsonData::System.Collections.ICollection.get_Count()
extern "C"  int32_t JsonData_System_Collections_ICollection_get_Count_m3235476973 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = JsonData_get_Count_m3094119778(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean LitJson.JsonData::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool JsonData_System_Collections_ICollection_get_IsSynchronized_m1459292420 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_ICollection_get_IsSynchronized_m1459292420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m1446037538(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.ICollection::get_IsSynchronized() */, ICollection_t3904884886_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Object LitJson.JsonData::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * JsonData_System_Collections_ICollection_get_SyncRoot_m1695797096 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_ICollection_get_SyncRoot_m1695797096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m1446037538(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t3904884886_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean LitJson.JsonData::System.Collections.IList.get_IsFixedSize()
extern "C"  bool JsonData_System_Collections_IList_get_IsFixedSize_m2437965165 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_get_IsFixedSize_m2437965165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m1549285368(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IList::get_IsFixedSize() */, IList_t2094931216_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean LitJson.JsonData::System.Collections.IList.get_IsReadOnly()
extern "C"  bool JsonData_System_Collections_IList_get_IsReadOnly_m978904392 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_get_IsReadOnly_m978904392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m1549285368(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IList::get_IsReadOnly() */, IList_t2094931216_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Object LitJson.JsonData::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_get_Item_m2981515460 (JsonData_t1524858407 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_get_Item_m2981515460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureDictionary_m3057468608(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		NullCheck(L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1363984059_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void LitJson.JsonData::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void JsonData_System_Collections_IDictionary_set_Item_m3224697051 (JsonData_t1524858407 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_set_Item_m3224697051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonData_t1524858407 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (((String_t*)IsInstSealed(L_0, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_1, _stringLiteral349688317, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___value1;
		JsonData_t1524858407 * L_3 = JsonData_ToJsonData_m69846785(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Il2CppObject * L_4 = ___key0;
		JsonData_t1524858407 * L_5 = V_0;
		JsonData_set_Item_m1920107000(__this, ((String_t*)CastclassSealed(L_4, String_t_il2cpp_TypeInfo_var)), L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Object LitJson.JsonData::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * JsonData_System_Collections_IList_get_Item_m2045844139 (JsonData_t1524858407 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_get_Item_m2045844139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m1549285368(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t2094931216_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void LitJson.JsonData::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void JsonData_System_Collections_IList_set_Item_m337029626 (JsonData_t1524858407 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	JsonData_t1524858407 * V_0 = NULL;
	{
		JsonData_EnsureList_m1549285368(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___value1;
		JsonData_t1524858407 * L_1 = JsonData_ToJsonData_m69846785(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___index0;
		JsonData_t1524858407 * L_3 = V_0;
		JsonData_set_Item_m1179907761(__this, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 LitJson.JsonData::get_Count()
extern "C"  int32_t JsonData_get_Count_m3094119778 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_get_Count_m3094119778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m1446037538(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t3904884886_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void LitJson.JsonData::set_Item(System.String,LitJson.JsonData)
extern "C"  void JsonData_set_Item_m1920107000 (JsonData_t1524858407 * __this, String_t* ___prop_name0, JsonData_t1524858407 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_set_Item_m1920107000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3707786873  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	KeyValuePair_2_t3707786873  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		JsonData_EnsureDictionary_m3057468608(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___prop_name0;
		JsonData_t1524858407 * L_1 = ___value1;
		KeyValuePair_2__ctor_m630695613((&V_0), L_0, L_1, /*hidden argument*/KeyValuePair_2__ctor_m630695613_MethodInfo_var);
		Il2CppObject* L_2 = __this->get_inst_object_5();
		String_t* L_3 = ___prop_name0;
		NullCheck(L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData>::ContainsKey(!0) */, IDictionary_2_t4068933393_il2cpp_TypeInfo_var, L_2, L_3);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		V_1 = 0;
		goto IL_005d;
	}

IL_0028:
	{
		Il2CppObject* L_5 = __this->get_object_list_9();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		KeyValuePair_2_t3707786873  L_7 = InterfaceFuncInvoker1< KeyValuePair_2_t3707786873 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Item(System.Int32) */, IList_1_t1228139360_il2cpp_TypeInfo_var, L_5, L_6);
		V_2 = L_7;
		String_t* L_8 = KeyValuePair_2_get_Key_m913646779((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m913646779_MethodInfo_var);
		String_t* L_9 = ___prop_name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m920492651(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = __this->get_object_list_9();
		int32_t L_12 = V_1;
		KeyValuePair_2_t3707786873  L_13 = V_0;
		NullCheck(L_11);
		InterfaceActionInvoker2< int32_t, KeyValuePair_2_t3707786873  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::set_Item(System.Int32,!0) */, IList_1_t1228139360_il2cpp_TypeInfo_var, L_11, L_12, L_13);
		goto IL_006e;
	}

IL_0059:
	{
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_15 = V_1;
		Il2CppObject* L_16 = __this->get_object_list_9();
		NullCheck(L_16);
		int32_t L_17 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Count() */, ICollection_1_t2240971811_il2cpp_TypeInfo_var, L_16);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0028;
		}
	}

IL_006e:
	{
		goto IL_007f;
	}

IL_0073:
	{
		Il2CppObject* L_18 = __this->get_object_list_9();
		KeyValuePair_2_t3707786873  L_19 = V_0;
		NullCheck(L_18);
		InterfaceActionInvoker1< KeyValuePair_2_t3707786873  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::Add(!0) */, ICollection_1_t2240971811_il2cpp_TypeInfo_var, L_18, L_19);
	}

IL_007f:
	{
		Il2CppObject* L_20 = __this->get_inst_object_5();
		String_t* L_21 = ___prop_name0;
		JsonData_t1524858407 * L_22 = ___value1;
		NullCheck(L_20);
		InterfaceActionInvoker2< String_t*, JsonData_t1524858407 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData>::set_Item(!0,!1) */, IDictionary_2_t4068933393_il2cpp_TypeInfo_var, L_20, L_21, L_22);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::set_Item(System.Int32,LitJson.JsonData)
extern "C"  void JsonData_set_Item_m1179907761 (JsonData_t1524858407 * __this, int32_t ___index0, JsonData_t1524858407 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_set_Item_m1179907761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3707786873  V_0;
	memset(&V_0, 0, sizeof(V_0));
	KeyValuePair_2_t3707786873  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		JsonData_EnsureCollection_m1446037538(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_type_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_array_0();
		int32_t L_2 = ___index0;
		JsonData_t1524858407 * L_3 = ___value1;
		NullCheck(L_1);
		InterfaceActionInvoker2< int32_t, JsonData_t1524858407 * >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<LitJson.JsonData>::set_Item(System.Int32,!0) */, IList_1_t3340178190_il2cpp_TypeInfo_var, L_1, L_2, L_3);
		goto IL_0061;
	}

IL_0025:
	{
		Il2CppObject* L_4 = __this->get_object_list_9();
		int32_t L_5 = ___index0;
		NullCheck(L_4);
		KeyValuePair_2_t3707786873  L_6 = InterfaceFuncInvoker1< KeyValuePair_2_t3707786873 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Item(System.Int32) */, IList_1_t1228139360_il2cpp_TypeInfo_var, L_4, L_5);
		V_0 = L_6;
		String_t* L_7 = KeyValuePair_2_get_Key_m913646779((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m913646779_MethodInfo_var);
		JsonData_t1524858407 * L_8 = ___value1;
		KeyValuePair_2__ctor_m630695613((&V_1), L_7, L_8, /*hidden argument*/KeyValuePair_2__ctor_m630695613_MethodInfo_var);
		Il2CppObject* L_9 = __this->get_object_list_9();
		int32_t L_10 = ___index0;
		KeyValuePair_2_t3707786873  L_11 = V_1;
		NullCheck(L_9);
		InterfaceActionInvoker2< int32_t, KeyValuePair_2_t3707786873  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::set_Item(System.Int32,!0) */, IList_1_t1228139360_il2cpp_TypeInfo_var, L_9, L_10, L_11);
		Il2CppObject* L_12 = __this->get_inst_object_5();
		String_t* L_13 = KeyValuePair_2_get_Key_m913646779((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m913646779_MethodInfo_var);
		JsonData_t1524858407 * L_14 = ___value1;
		NullCheck(L_12);
		InterfaceActionInvoker2< String_t*, JsonData_t1524858407 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData>::set_Item(!0,!1) */, IDictionary_2_t4068933393_il2cpp_TypeInfo_var, L_12, L_13, L_14);
	}

IL_0061:
	{
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::.ctor(System.Object)
extern "C"  void JsonData__ctor_m3866765393 (JsonData_t1524858407 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData__ctor_m3866765393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_0, Boolean_t97287965_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		__this->set_type_8(7);
		Il2CppObject * L_1 = ___obj0;
		__this->set_inst_boolean_1(((*(bool*)((bool*)UnBox(L_1, Boolean_t97287965_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0025:
	{
		Il2CppObject * L_2 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_2, Double_t594665363_il2cpp_TypeInfo_var)))
		{
			goto IL_0044;
		}
	}
	{
		__this->set_type_8(6);
		Il2CppObject * L_3 = ___obj0;
		__this->set_inst_double_2(((*(double*)((double*)UnBox(L_3, Double_t594665363_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0044:
	{
		Il2CppObject * L_4 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_4, Int32_t2950945753_il2cpp_TypeInfo_var)))
		{
			goto IL_0063;
		}
	}
	{
		__this->set_type_8(4);
		Il2CppObject * L_5 = ___obj0;
		__this->set_inst_int_3(((*(int32_t*)((int32_t*)UnBox(L_5, Int32_t2950945753_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0063:
	{
		Il2CppObject * L_6 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_6, Int64_t3736567304_il2cpp_TypeInfo_var)))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_type_8(5);
		Il2CppObject * L_7 = ___obj0;
		__this->set_inst_long_4(((*(int64_t*)((int64_t*)UnBox(L_7, Int64_t3736567304_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0082:
	{
		Il2CppObject * L_8 = ___obj0;
		if (!((String_t*)IsInstSealed(L_8, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_00a1;
		}
	}
	{
		__this->set_type_8(3);
		Il2CppObject * L_9 = ___obj0;
		__this->set_inst_string_6(((String_t*)CastclassSealed(L_9, String_t_il2cpp_TypeInfo_var)));
		return;
	}

IL_00a1:
	{
		ArgumentException_t132251570 * L_10 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_10, _stringLiteral2958118195, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}
}
// System.Void LitJson.JsonData::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void JsonData_System_Collections_ICollection_CopyTo_m2772442918 (JsonData_t1524858407 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_ICollection_CopyTo_m2772442918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m1446037538(__this, /*hidden argument*/NULL);
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t3904884886_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void LitJson.JsonData::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void JsonData_System_Collections_IDictionary_Add_m4101376796 (JsonData_t1524858407 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_Add_m4101376796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonData_t1524858407 * V_0 = NULL;
	KeyValuePair_2_t3707786873  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___value1;
		JsonData_t1524858407 * L_1 = JsonData_ToJsonData_m69846785(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = JsonData_EnsureDictionary_m3057468608(__this, /*hidden argument*/NULL);
		Il2CppObject * L_3 = ___key0;
		JsonData_t1524858407 * L_4 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t1363984059_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		Il2CppObject * L_5 = ___key0;
		JsonData_t1524858407 * L_6 = V_0;
		KeyValuePair_2__ctor_m630695613((&V_1), ((String_t*)CastclassSealed(L_5, String_t_il2cpp_TypeInfo_var)), L_6, /*hidden argument*/KeyValuePair_2__ctor_m630695613_MethodInfo_var);
		Il2CppObject* L_7 = __this->get_object_list_9();
		KeyValuePair_2_t3707786873  L_8 = V_1;
		NullCheck(L_7);
		InterfaceActionInvoker1< KeyValuePair_2_t3707786873  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::Add(!0) */, ICollection_1_t2240971811_il2cpp_TypeInfo_var, L_7, L_8);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Collections.IDictionaryEnumerator LitJson.JsonData::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_GetEnumerator_m2090743371 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_GetEnumerator_m2090743371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IDictionaryEnumerator System.Collections.Specialized.IOrderedDictionary::GetEnumerator() */, IOrderedDictionary_t2745322917_il2cpp_TypeInfo_var, __this);
		return L_0;
	}
}
// System.Void LitJson.JsonData::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void JsonData_System_Collections_IDictionary_Remove_m4104093953 (JsonData_t1524858407 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_Remove_m4104093953_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t3707786873  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = JsonData_EnsureDictionary_m3057468608(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(4 /* System.Void System.Collections.IDictionary::Remove(System.Object) */, IDictionary_t1363984059_il2cpp_TypeInfo_var, L_0, L_1);
		V_0 = 0;
		goto IL_004c;
	}

IL_0013:
	{
		Il2CppObject* L_2 = __this->get_object_list_9();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		KeyValuePair_2_t3707786873  L_4 = InterfaceFuncInvoker1< KeyValuePair_2_t3707786873 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Item(System.Int32) */, IList_1_t1228139360_il2cpp_TypeInfo_var, L_2, L_3);
		V_1 = L_4;
		String_t* L_5 = KeyValuePair_2_get_Key_m913646779((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m913646779_MethodInfo_var);
		Il2CppObject * L_6 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_5, ((String_t*)CastclassSealed(L_6, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		Il2CppObject* L_8 = __this->get_object_list_9();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::RemoveAt(System.Int32) */, IList_1_t1228139360_il2cpp_TypeInfo_var, L_8, L_9);
		goto IL_005d;
	}

IL_0048:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_004c:
	{
		int32_t L_11 = V_0;
		Il2CppObject* L_12 = __this->get_object_list_9();
		NullCheck(L_12);
		int32_t L_13 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Count() */, ICollection_1_t2240971811_il2cpp_TypeInfo_var, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0013;
		}
	}

IL_005d:
	{
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Collections.IEnumerator LitJson.JsonData::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JsonData_System_Collections_IEnumerable_GetEnumerator_m3857574006 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IEnumerable_GetEnumerator_m3857574006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m1446037538(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1941168011_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.SetBoolean(System.Boolean)
extern "C"  void JsonData_LitJson_IJsonWrapper_SetBoolean_m2421428309 (JsonData_t1524858407 * __this, bool ___val0, const MethodInfo* method)
{
	{
		__this->set_type_8(7);
		bool L_0 = ___val0;
		__this->set_inst_boolean_1(L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.SetDouble(System.Double)
extern "C"  void JsonData_LitJson_IJsonWrapper_SetDouble_m2414892330 (JsonData_t1524858407 * __this, double ___val0, const MethodInfo* method)
{
	{
		__this->set_type_8(6);
		double L_0 = ___val0;
		__this->set_inst_double_2(L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.SetInt(System.Int32)
extern "C"  void JsonData_LitJson_IJsonWrapper_SetInt_m3344807977 (JsonData_t1524858407 * __this, int32_t ___val0, const MethodInfo* method)
{
	{
		__this->set_type_8(4);
		int32_t L_0 = ___val0;
		__this->set_inst_int_3(L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.SetLong(System.Int64)
extern "C"  void JsonData_LitJson_IJsonWrapper_SetLong_m2212179795 (JsonData_t1524858407 * __this, int64_t ___val0, const MethodInfo* method)
{
	{
		__this->set_type_8(5);
		int64_t L_0 = ___val0;
		__this->set_inst_long_4(L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.SetString(System.String)
extern "C"  void JsonData_LitJson_IJsonWrapper_SetString_m947456696 (JsonData_t1524858407 * __this, String_t* ___val0, const MethodInfo* method)
{
	{
		__this->set_type_8(3);
		String_t* L_0 = ___val0;
		__this->set_inst_string_6(L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Int32 LitJson.JsonData::System.Collections.IList.Add(System.Object)
extern "C"  int32_t JsonData_System_Collections_IList_Add_m4232755141 (JsonData_t1524858407 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		int32_t L_1 = JsonData_Add_m2952029759(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void LitJson.JsonData::System.Collections.IList.Clear()
extern "C"  void JsonData_System_Collections_IList_Clear_m3703745914 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Clear_m3703745914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m1549285368(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(5 /* System.Void System.Collections.IList::Clear() */, IList_t2094931216_il2cpp_TypeInfo_var, L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Boolean LitJson.JsonData::System.Collections.IList.Contains(System.Object)
extern "C"  bool JsonData_System_Collections_IList_Contains_m1075980415 (JsonData_t1524858407 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Contains_m1075980415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m1549285368(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IList::Contains(System.Object) */, IList_t2094931216_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Int32 LitJson.JsonData::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t JsonData_System_Collections_IList_IndexOf_m1757109292 (JsonData_t1524858407 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_IndexOf_m1757109292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m1549285368(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(7 /* System.Int32 System.Collections.IList::IndexOf(System.Object) */, IList_t2094931216_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void LitJson.JsonData::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void JsonData_System_Collections_IList_Insert_m557679721 (JsonData_t1524858407 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Insert_m557679721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m1549285368(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IList::Insert(System.Int32,System.Object) */, IList_t2094931216_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::System.Collections.IList.Remove(System.Object)
extern "C"  void JsonData_System_Collections_IList_Remove_m77578819 (JsonData_t1524858407 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Remove_m77578819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m1549285368(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void System.Collections.IList::Remove(System.Object) */, IList_t2094931216_il2cpp_TypeInfo_var, L_0, L_1);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void JsonData_System_Collections_IList_RemoveAt_m2329997688 (JsonData_t1524858407 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_RemoveAt_m2329997688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m1549285368(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Collections.IList::RemoveAt(System.Int32) */, IList_t2094931216_il2cpp_TypeInfo_var, L_0, L_1);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Collections.IDictionaryEnumerator LitJson.JsonData::System.Collections.Specialized.IOrderedDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m2093370556 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m2093370556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonData_EnsureDictionary_m3057468608(__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = __this->get_object_list_9();
		NullCheck(L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::GetEnumerator() */, IEnumerable_1_t2687639762_il2cpp_TypeInfo_var, L_0);
		OrderedDictionaryEnumerator_t386339177 * L_2 = (OrderedDictionaryEnumerator_t386339177 *)il2cpp_codegen_object_new(OrderedDictionaryEnumerator_t386339177_il2cpp_TypeInfo_var);
		OrderedDictionaryEnumerator__ctor_m3267133839(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.ICollection LitJson.JsonData::EnsureCollection()
extern "C"  Il2CppObject * JsonData_EnsureCollection_m1446037538 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_EnsureCollection_m1446037538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_array_0();
		return ((Il2CppObject *)Castclass(L_1, ICollection_t3904884886_il2cpp_TypeInfo_var));
	}

IL_0018:
	{
		int32_t L_2 = __this->get_type_8();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0030;
		}
	}
	{
		Il2CppObject* L_3 = __this->get_inst_object_5();
		return ((Il2CppObject *)Castclass(L_3, ICollection_t3904884886_il2cpp_TypeInfo_var));
	}

IL_0030:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, _stringLiteral2524214959, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Collections.IDictionary LitJson.JsonData::EnsureDictionary()
extern "C"  Il2CppObject * JsonData_EnsureDictionary_m3057468608 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_EnsureDictionary_m3057468608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_object_5();
		return ((Il2CppObject *)Castclass(L_1, IDictionary_t1363984059_il2cpp_TypeInfo_var));
	}

IL_0018:
	{
		int32_t L_2 = __this->get_type_8();
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, _stringLiteral2604237559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002e:
	{
		__this->set_type_8(1);
		Dictionary_2_t1310114706 * L_4 = (Dictionary_2_t1310114706 *)il2cpp_codegen_object_new(Dictionary_2_t1310114706_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1325424069(L_4, /*hidden argument*/Dictionary_2__ctor_m1325424069_MethodInfo_var);
		__this->set_inst_object_5(L_4);
		List_1_t884894319 * L_5 = (List_1_t884894319 *)il2cpp_codegen_object_new(List_1_t884894319_il2cpp_TypeInfo_var);
		List_1__ctor_m2572897465(L_5, /*hidden argument*/List_1__ctor_m2572897465_MethodInfo_var);
		__this->set_object_list_9(L_5);
		Il2CppObject* L_6 = __this->get_inst_object_5();
		return ((Il2CppObject *)Castclass(L_6, IDictionary_t1363984059_il2cpp_TypeInfo_var));
	}
}
// System.Collections.IList LitJson.JsonData::EnsureList()
extern "C"  Il2CppObject * JsonData_EnsureList_m1549285368 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_EnsureList_m1549285368_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_array_0();
		return ((Il2CppObject *)Castclass(L_1, IList_t2094931216_il2cpp_TypeInfo_var));
	}

IL_0018:
	{
		int32_t L_2 = __this->get_type_8();
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, _stringLiteral4137426702, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002e:
	{
		__this->set_type_8(2);
		List_1_t2996933149 * L_4 = (List_1_t2996933149 *)il2cpp_codegen_object_new(List_1_t2996933149_il2cpp_TypeInfo_var);
		List_1__ctor_m1532675647(L_4, /*hidden argument*/List_1__ctor_m1532675647_MethodInfo_var);
		__this->set_inst_array_0(L_4);
		Il2CppObject* L_5 = __this->get_inst_array_0();
		return ((Il2CppObject *)Castclass(L_5, IList_t2094931216_il2cpp_TypeInfo_var));
	}
}
// LitJson.JsonData LitJson.JsonData::ToJsonData(System.Object)
extern "C"  JsonData_t1524858407 * JsonData_ToJsonData_m69846785 (JsonData_t1524858407 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_ToJsonData_m69846785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (JsonData_t1524858407 *)NULL;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((JsonData_t1524858407 *)IsInstClass(L_1, JsonData_t1524858407_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		return ((JsonData_t1524858407 *)CastclassClass(L_2, JsonData_t1524858407_il2cpp_TypeInfo_var));
	}

IL_001a:
	{
		Il2CppObject * L_3 = ___obj0;
		JsonData_t1524858407 * L_4 = (JsonData_t1524858407 *)il2cpp_codegen_object_new(JsonData_t1524858407_il2cpp_TypeInfo_var);
		JsonData__ctor_m3866765393(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 LitJson.JsonData::Add(System.Object)
extern "C"  int32_t JsonData_Add_m2952029759 (JsonData_t1524858407 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_Add_m2952029759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonData_t1524858407 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		JsonData_t1524858407 * L_1 = JsonData_ToJsonData_m69846785(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		__this->set_json_7((String_t*)NULL);
		Il2CppObject * L_2 = JsonData_EnsureList_m1549285368(__this, /*hidden argument*/NULL);
		JsonData_t1524858407 * L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t2094931216_il2cpp_TypeInfo_var, L_2, L_3);
		return L_4;
	}
}
// System.Boolean LitJson.JsonData::Equals(LitJson.JsonData)
extern "C"  bool JsonData_Equals_m4285343643 (JsonData_t1524858407 * __this, JsonData_t1524858407 * ___x0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		JsonData_t1524858407 * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		JsonData_t1524858407 * L_1 = ___x0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_type_8();
		int32_t L_3 = __this->get_type_8();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_001b;
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		int32_t L_4 = __this->get_type_8();
		V_0 = L_4;
		int32_t L_5 = V_0;
		switch (L_5)
		{
			case 0:
			{
				goto IL_004d;
			}
			case 1:
			{
				goto IL_004f;
			}
			case 2:
			{
				goto IL_0061;
			}
			case 3:
			{
				goto IL_0073;
			}
			case 4:
			{
				goto IL_0085;
			}
			case 5:
			{
				goto IL_0097;
			}
			case 6:
			{
				goto IL_00a9;
			}
			case 7:
			{
				goto IL_00bb;
			}
		}
	}
	{
		goto IL_00cd;
	}

IL_004d:
	{
		return (bool)1;
	}

IL_004f:
	{
		Il2CppObject* L_6 = __this->get_inst_object_5();
		JsonData_t1524858407 * L_7 = ___x0;
		NullCheck(L_7);
		Il2CppObject* L_8 = L_7->get_inst_object_5();
		NullCheck(L_6);
		bool L_9 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_6, L_8);
		return L_9;
	}

IL_0061:
	{
		Il2CppObject* L_10 = __this->get_inst_array_0();
		JsonData_t1524858407 * L_11 = ___x0;
		NullCheck(L_11);
		Il2CppObject* L_12 = L_11->get_inst_array_0();
		NullCheck(L_10);
		bool L_13 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_10, L_12);
		return L_13;
	}

IL_0073:
	{
		String_t* L_14 = __this->get_inst_string_6();
		JsonData_t1524858407 * L_15 = ___x0;
		NullCheck(L_15);
		String_t* L_16 = L_15->get_inst_string_6();
		NullCheck(L_14);
		bool L_17 = String_Equals_m2270643605(L_14, L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_0085:
	{
		int32_t* L_18 = __this->get_address_of_inst_int_3();
		JsonData_t1524858407 * L_19 = ___x0;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_inst_int_3();
		bool L_21 = Int32_Equals_m2976157357(L_18, L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_0097:
	{
		int64_t* L_22 = __this->get_address_of_inst_long_4();
		JsonData_t1524858407 * L_23 = ___x0;
		NullCheck(L_23);
		int64_t L_24 = L_23->get_inst_long_4();
		bool L_25 = Int64_Equals_m680137412(L_22, L_24, /*hidden argument*/NULL);
		return L_25;
	}

IL_00a9:
	{
		double* L_26 = __this->get_address_of_inst_double_2();
		JsonData_t1524858407 * L_27 = ___x0;
		NullCheck(L_27);
		double L_28 = L_27->get_inst_double_2();
		bool L_29 = Double_Equals_m2309369974(L_26, L_28, /*hidden argument*/NULL);
		return L_29;
	}

IL_00bb:
	{
		bool* L_30 = __this->get_address_of_inst_boolean_1();
		JsonData_t1524858407 * L_31 = ___x0;
		NullCheck(L_31);
		bool L_32 = L_31->get_inst_boolean_1();
		bool L_33 = Boolean_Equals_m535526264(L_30, L_32, /*hidden argument*/NULL);
		return L_33;
	}

IL_00cd:
	{
		return (bool)0;
	}
}
// System.Void LitJson.JsonData::SetJsonType(LitJson.JsonType)
extern "C"  void JsonData_SetJsonType_m3288213674 (JsonData_t1524858407 * __this, int32_t ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_SetJsonType_m3288213674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		int32_t L_1 = ___type0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		int32_t L_2 = ___type0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0038;
			}
			case 1:
			{
				goto IL_003d;
			}
			case 2:
			{
				goto IL_0058;
			}
			case 3:
			{
				goto IL_0068;
			}
			case 4:
			{
				goto IL_0074;
			}
			case 5:
			{
				goto IL_0080;
			}
			case 6:
			{
				goto IL_008d;
			}
			case 7:
			{
				goto IL_00a1;
			}
		}
	}
	{
		goto IL_00ad;
	}

IL_0038:
	{
		goto IL_00ad;
	}

IL_003d:
	{
		Dictionary_2_t1310114706 * L_3 = (Dictionary_2_t1310114706 *)il2cpp_codegen_object_new(Dictionary_2_t1310114706_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1325424069(L_3, /*hidden argument*/Dictionary_2__ctor_m1325424069_MethodInfo_var);
		__this->set_inst_object_5(L_3);
		List_1_t884894319 * L_4 = (List_1_t884894319 *)il2cpp_codegen_object_new(List_1_t884894319_il2cpp_TypeInfo_var);
		List_1__ctor_m2572897465(L_4, /*hidden argument*/List_1__ctor_m2572897465_MethodInfo_var);
		__this->set_object_list_9(L_4);
		goto IL_00ad;
	}

IL_0058:
	{
		List_1_t2996933149 * L_5 = (List_1_t2996933149 *)il2cpp_codegen_object_new(List_1_t2996933149_il2cpp_TypeInfo_var);
		List_1__ctor_m1532675647(L_5, /*hidden argument*/List_1__ctor_m1532675647_MethodInfo_var);
		__this->set_inst_array_0(L_5);
		goto IL_00ad;
	}

IL_0068:
	{
		__this->set_inst_string_6((String_t*)NULL);
		goto IL_00ad;
	}

IL_0074:
	{
		__this->set_inst_int_3(0);
		goto IL_00ad;
	}

IL_0080:
	{
		__this->set_inst_long_4((((int64_t)((int64_t)0))));
		goto IL_00ad;
	}

IL_008d:
	{
		__this->set_inst_double_2((0.0));
		goto IL_00ad;
	}

IL_00a1:
	{
		__this->set_inst_boolean_1((bool)0);
		goto IL_00ad;
	}

IL_00ad:
	{
		int32_t L_6 = ___type0;
		__this->set_type_8(L_6);
		return;
	}
}
// System.String LitJson.JsonData::ToString()
extern "C"  String_t* JsonData_ToString_m160346085 (JsonData_t1524858407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonData_ToString_m160346085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_8();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)1)))
		{
			case 0:
			{
				goto IL_007e;
			}
			case 1:
			{
				goto IL_0030;
			}
			case 2:
			{
				goto IL_0084;
			}
			case 3:
			{
				goto IL_005a;
			}
			case 4:
			{
				goto IL_006c;
			}
			case 5:
			{
				goto IL_0048;
			}
			case 6:
			{
				goto IL_0036;
			}
		}
	}
	{
		goto IL_008b;
	}

IL_0030:
	{
		return _stringLiteral4244825595;
	}

IL_0036:
	{
		bool* L_2 = __this->get_address_of_inst_boolean_1();
		String_t* L_3 = Boolean_ToString_m2664721875(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0048:
	{
		double* L_4 = __this->get_address_of_inst_double_2();
		String_t* L_5 = Double_ToString_m1229922074(L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_005a:
	{
		int32_t* L_6 = __this->get_address_of_inst_int_3();
		String_t* L_7 = Int32_ToString_m141394615(L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_006c:
	{
		int64_t* L_8 = __this->get_address_of_inst_long_4();
		String_t* L_9 = Int64_ToString_m2986581816(L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_007e:
	{
		return _stringLiteral3670642506;
	}

IL_0084:
	{
		String_t* L_10 = __this->get_inst_string_6();
		return L_10;
	}

IL_008b:
	{
		return _stringLiteral2138300441;
	}
}
// System.Void LitJson.JsonException::.ctor(LitJson.ParserToken,System.Exception)
extern "C"  void JsonException__ctor_m4037883734 (JsonException_t3682484112 * __this, int32_t ___token0, Exception_t1436737249 * ___inner_exception1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonException__ctor_m4037883734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___token0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(ParserToken_t2380208742_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral917256838, L_2, /*hidden argument*/NULL);
		Exception_t1436737249 * L_4 = ___inner_exception1;
		ApplicationException__ctor_m692455299(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonException::.ctor(System.Int32)
extern "C"  void JsonException__ctor_m2210441219 (JsonException_t3682484112 * __this, int32_t ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonException__ctor_m2210441219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___c0;
		Il2CppChar L_1 = ((Il2CppChar)(((int32_t)((uint16_t)L_0))));
		Il2CppObject * L_2 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral4006081049, L_2, /*hidden argument*/NULL);
		ApplicationException__ctor_m2517758450(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonException::.ctor(System.String)
extern "C"  void JsonException__ctor_m2214533437 (JsonException_t3682484112 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ApplicationException__ctor_m2517758450(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::.cctor()
extern "C"  void JsonMapper__cctor_m850028620 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper__cctor_m850028620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_0, /*hidden argument*/NULL);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_array_metadata_lock_7(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_1, /*hidden argument*/NULL);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_conv_ops_lock_9(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_2, /*hidden argument*/NULL);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_object_metadata_lock_11(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_3, /*hidden argument*/NULL);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_type_properties_lock_13(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_4, /*hidden argument*/NULL);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_static_writer_lock_15(L_4);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_max_nesting_depth_0(((int32_t)100));
		Dictionary_2_t3338636003 * L_5 = (Dictionary_2_t3338636003 *)il2cpp_codegen_object_new(Dictionary_2_t3338636003_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2431295564(L_5, /*hidden argument*/Dictionary_2__ctor_m2431295564_MethodInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_array_metadata_6(L_5);
		Dictionary_2_t935204471 * L_6 = (Dictionary_2_t935204471 *)il2cpp_codegen_object_new(Dictionary_2_t935204471_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2076397116(L_6, /*hidden argument*/Dictionary_2__ctor_m2076397116_MethodInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_conv_ops_8(L_6);
		Dictionary_2_t1715664290 * L_7 = (Dictionary_2_t1715664290 *)il2cpp_codegen_object_new(Dictionary_2_t1715664290_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1674229861(L_7, /*hidden argument*/Dictionary_2__ctor_m1674229861_MethodInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_object_metadata_10(L_7);
		Dictionary_2_t3692140024 * L_8 = (Dictionary_2_t3692140024 *)il2cpp_codegen_object_new(Dictionary_2_t3692140024_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2292515434(L_8, /*hidden argument*/Dictionary_2__ctor_m2292515434_MethodInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_type_properties_12(L_8);
		JsonWriter_t3570089748 * L_9 = (JsonWriter_t3570089748 *)il2cpp_codegen_object_new(JsonWriter_t3570089748_il2cpp_TypeInfo_var);
		JsonWriter__ctor_m3035373812(L_9, /*hidden argument*/NULL);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_static_writer_14(L_9);
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeFormatInfo_t2405853701_il2cpp_TypeInfo_var);
		DateTimeFormatInfo_t2405853701 * L_10 = DateTimeFormatInfo_get_InvariantInfo_m2329875772(NULL /*static, unused*/, /*hidden argument*/NULL);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_datetime_format_1(L_10);
		Dictionary_2_t691233 * L_11 = (Dictionary_2_t691233 *)il2cpp_codegen_object_new(Dictionary_2_t691233_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2735992426(L_11, /*hidden argument*/Dictionary_2__ctor_m2735992426_MethodInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_base_exporters_table_2(L_11);
		Dictionary_2_t691233 * L_12 = (Dictionary_2_t691233 *)il2cpp_codegen_object_new(Dictionary_2_t691233_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2735992426(L_12, /*hidden argument*/Dictionary_2__ctor_m2735992426_MethodInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_custom_exporters_table_3(L_12);
		Dictionary_2_t2688515417 * L_13 = (Dictionary_2_t2688515417 *)il2cpp_codegen_object_new(Dictionary_2_t2688515417_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1523576929(L_13, /*hidden argument*/Dictionary_2__ctor_m1523576929_MethodInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_base_importers_table_4(L_13);
		Dictionary_2_t2688515417 * L_14 = (Dictionary_2_t2688515417 *)il2cpp_codegen_object_new(Dictionary_2_t2688515417_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1523576929(L_14, /*hidden argument*/Dictionary_2__ctor_m1523576929_MethodInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_custom_importers_table_5(L_14);
		JsonMapper_RegisterBaseExporters_m1355390223(NULL /*static, unused*/, /*hidden argument*/NULL);
		JsonMapper_RegisterBaseImporters_m623152139(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::AddArrayMetadata(System.Type)
extern "C"  void JsonMapper_AddArrayMetadata_m3968694626 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_AddArrayMetadata_m3968694626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayMetadata_t894288939  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PropertyInfo_t * V_1 = NULL;
	PropertyInfoU5BU5D_t1461822886* V_2 = NULL;
	int32_t V_3 = 0;
	ParameterInfoU5BU5D_t390618515* V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_array_metadata_6();
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata>::ContainsKey(!0) */, IDictionary_2_t1802487394_il2cpp_TypeInfo_var, L_0, L_1);
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Initobj (ArrayMetadata_t894288939_il2cpp_TypeInfo_var, (&V_0));
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		bool L_4 = Type_get_IsArray_m2591212821(L_3, /*hidden argument*/NULL);
		ArrayMetadata_set_IsArray_m2441146849((&V_0), L_4, /*hidden argument*/NULL);
		Type_t * L_5 = ___type0;
		NullCheck(L_5);
		Type_t * L_6 = Type_GetInterface_m23870712(L_5, _stringLiteral2211611910, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003e;
		}
	}
	{
		ArrayMetadata_set_IsList_m3822380729((&V_0), (bool)1, /*hidden argument*/NULL);
	}

IL_003e:
	{
		Type_t * L_7 = ___type0;
		NullCheck(L_7);
		PropertyInfoU5BU5D_t1461822886* L_8 = Type_GetProperties_m1538559489(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		V_3 = 0;
		goto IL_00aa;
	}

IL_004c:
	{
		PropertyInfoU5BU5D_t1461822886* L_9 = V_2;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		PropertyInfo_t * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_1 = L_12;
		PropertyInfo_t * L_13 = V_1;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_14, _stringLiteral1949155704, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006a;
		}
	}
	{
		goto IL_00a6;
	}

IL_006a:
	{
		PropertyInfo_t * L_16 = V_1;
		NullCheck(L_16);
		ParameterInfoU5BU5D_t390618515* L_17 = VirtFuncInvoker0< ParameterInfoU5BU5D_t390618515* >::Invoke(20 /* System.Reflection.ParameterInfo[] System.Reflection.PropertyInfo::GetIndexParameters() */, L_16);
		V_4 = L_17;
		ParameterInfoU5BU5D_t390618515* L_18 = V_4;
		NullCheck(L_18);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0081;
		}
	}
	{
		goto IL_00a6;
	}

IL_0081:
	{
		ParameterInfoU5BU5D_t390618515* L_19 = V_4;
		NullCheck(L_19);
		int32_t L_20 = 0;
		ParameterInfo_t1861056598 * L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		Type_t * L_22 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_21);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_23 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int32_t2950945753_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_22) == ((Il2CppObject*)(Type_t *)L_23))))
		{
			goto IL_00a6;
		}
	}
	{
		PropertyInfo_t * L_24 = V_1;
		NullCheck(L_24);
		Type_t * L_25 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_24);
		ArrayMetadata_set_ElementType_m2992145657((&V_0), L_25, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		int32_t L_26 = V_3;
		V_3 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00aa:
	{
		int32_t L_27 = V_3;
		PropertyInfoU5BU5D_t1461822886* L_28 = V_2;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_array_metadata_lock_7();
		V_5 = L_29;
		Il2CppObject * L_30 = V_5;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
	}

IL_00c1:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
			Il2CppObject* L_31 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_array_metadata_6();
			Type_t * L_32 = ___type0;
			ArrayMetadata_t894288939  L_33 = V_0;
			NullCheck(L_31);
			InterfaceActionInvoker2< Type_t *, ArrayMetadata_t894288939  >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata>::Add(!0,!1) */, IDictionary_2_t1802487394_il2cpp_TypeInfo_var, L_31, L_32, L_33);
			goto IL_00d8;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1436737249 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t132251570_il2cpp_TypeInfo_var, e.ex->klass))
				goto CATCH_00d2;
			throw e;
		}

CATCH_00d2:
		{ // begin catch(System.ArgumentException)
			IL2CPP_LEAVE(0xE5, FINALLY_00dd);
		} // end catch (depth: 2)

IL_00d8:
		{
			IL2CPP_LEAVE(0xE5, FINALLY_00dd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_00dd;
	}

FINALLY_00dd:
	{ // begin finally (depth: 1)
		Il2CppObject * L_34 = V_5;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(221)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(221)
	{
		IL2CPP_JUMP_TBL(0xE5, IL_00e5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_00e5:
	{
		return;
	}
}
// System.Void LitJson.JsonMapper::AddObjectMetadata(System.Type)
extern "C"  void JsonMapper_AddObjectMetadata_m194540947 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_AddObjectMetadata_m194540947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectMetadata_t3566284522  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PropertyInfo_t * V_1 = NULL;
	PropertyInfoU5BU5D_t1461822886* V_2 = NULL;
	int32_t V_3 = 0;
	ParameterInfoU5BU5D_t390618515* V_4 = NULL;
	PropertyMetadata_t3727440473  V_5;
	memset(&V_5, 0, sizeof(V_5));
	FieldInfo_t * V_6 = NULL;
	FieldInfoU5BU5D_t846150980* V_7 = NULL;
	int32_t V_8 = 0;
	PropertyMetadata_t3727440473  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Il2CppObject * V_10 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_object_metadata_10();
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata>::ContainsKey(!0) */, IDictionary_2_t179515681_il2cpp_TypeInfo_var, L_0, L_1);
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Initobj (ObjectMetadata_t3566284522_il2cpp_TypeInfo_var, (&V_0));
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = Type_GetInterface_m23870712(L_3, _stringLiteral1480664753, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		ObjectMetadata_set_IsDictionary_m2176094566((&V_0), (bool)1, /*hidden argument*/NULL);
	}

IL_0031:
	{
		Dictionary_2_t3512696772 * L_5 = (Dictionary_2_t3512696772 *)il2cpp_codegen_object_new(Dictionary_2_t3512696772_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3146315127(L_5, /*hidden argument*/Dictionary_2__ctor_m3146315127_MethodInfo_var);
		ObjectMetadata_set_Properties_m3670768576((&V_0), L_5, /*hidden argument*/NULL);
		Type_t * L_6 = ___type0;
		NullCheck(L_6);
		PropertyInfoU5BU5D_t1461822886* L_7 = Type_GetProperties_m1538559489(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		V_3 = 0;
		goto IL_00da;
	}

IL_004b:
	{
		PropertyInfoU5BU5D_t1461822886* L_8 = V_2;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		PropertyInfo_t * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_1 = L_11;
		PropertyInfo_t * L_12 = V_1;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m920492651(NULL /*static, unused*/, L_13, _stringLiteral1949155704, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00a5;
		}
	}
	{
		PropertyInfo_t * L_15 = V_1;
		NullCheck(L_15);
		ParameterInfoU5BU5D_t390618515* L_16 = VirtFuncInvoker0< ParameterInfoU5BU5D_t390618515* >::Invoke(20 /* System.Reflection.ParameterInfo[] System.Reflection.PropertyInfo::GetIndexParameters() */, L_15);
		V_4 = L_16;
		ParameterInfoU5BU5D_t390618515* L_17 = V_4;
		NullCheck(L_17);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length))))) == ((int32_t)1)))
		{
			goto IL_007b;
		}
	}
	{
		goto IL_00d6;
	}

IL_007b:
	{
		ParameterInfoU5BU5D_t390618515* L_18 = V_4;
		NullCheck(L_18);
		int32_t L_19 = 0;
		ParameterInfo_t1861056598 * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		Type_t * L_21 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_20);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_21) == ((Il2CppObject*)(Type_t *)L_22))))
		{
			goto IL_00a0;
		}
	}
	{
		PropertyInfo_t * L_23 = V_1;
		NullCheck(L_23);
		Type_t * L_24 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_23);
		ObjectMetadata_set_ElementType_m1776330551((&V_0), L_24, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		goto IL_00d6;
	}

IL_00a5:
	{
		Initobj (PropertyMetadata_t3727440473_il2cpp_TypeInfo_var, (&V_5));
		PropertyInfo_t * L_25 = V_1;
		(&V_5)->set_Info_0(L_25);
		PropertyInfo_t * L_26 = V_1;
		NullCheck(L_26);
		Type_t * L_27 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_26);
		(&V_5)->set_Type_2(L_27);
		Il2CppObject* L_28 = ObjectMetadata_get_Properties_m3723938162((&V_0), /*hidden argument*/NULL);
		PropertyInfo_t * L_29 = V_1;
		NullCheck(L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_29);
		PropertyMetadata_t3727440473  L_31 = V_5;
		NullCheck(L_28);
		InterfaceActionInvoker2< String_t*, PropertyMetadata_t3727440473  >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>::Add(!0,!1) */, IDictionary_2_t1976548163_il2cpp_TypeInfo_var, L_28, L_30, L_31);
	}

IL_00d6:
	{
		int32_t L_32 = V_3;
		V_3 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00da:
	{
		int32_t L_33 = V_3;
		PropertyInfoU5BU5D_t1461822886* L_34 = V_2;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length)))))))
		{
			goto IL_004b;
		}
	}
	{
		Type_t * L_35 = ___type0;
		NullCheck(L_35);
		FieldInfoU5BU5D_t846150980* L_36 = Type_GetFields_m3709891696(L_35, /*hidden argument*/NULL);
		V_7 = L_36;
		V_8 = 0;
		goto IL_013c;
	}

IL_00f3:
	{
		FieldInfoU5BU5D_t846150980* L_37 = V_7;
		int32_t L_38 = V_8;
		NullCheck(L_37);
		int32_t L_39 = L_38;
		FieldInfo_t * L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		V_6 = L_40;
		Initobj (PropertyMetadata_t3727440473_il2cpp_TypeInfo_var, (&V_9));
		FieldInfo_t * L_41 = V_6;
		(&V_9)->set_Info_0(L_41);
		(&V_9)->set_IsField_1((bool)1);
		FieldInfo_t * L_42 = V_6;
		NullCheck(L_42);
		Type_t * L_43 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_42);
		(&V_9)->set_Type_2(L_43);
		Il2CppObject* L_44 = ObjectMetadata_get_Properties_m3723938162((&V_0), /*hidden argument*/NULL);
		FieldInfo_t * L_45 = V_6;
		NullCheck(L_45);
		String_t* L_46 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_45);
		PropertyMetadata_t3727440473  L_47 = V_9;
		NullCheck(L_44);
		InterfaceActionInvoker2< String_t*, PropertyMetadata_t3727440473  >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>::Add(!0,!1) */, IDictionary_2_t1976548163_il2cpp_TypeInfo_var, L_44, L_46, L_47);
		int32_t L_48 = V_8;
		V_8 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_013c:
	{
		int32_t L_49 = V_8;
		FieldInfoU5BU5D_t846150980* L_50 = V_7;
		NullCheck(L_50);
		if ((((int32_t)L_49) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_50)->max_length)))))))
		{
			goto IL_00f3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_51 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_object_metadata_lock_11();
		V_10 = L_51;
		Il2CppObject * L_52 = V_10;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
	}

IL_0155:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
			Il2CppObject* L_53 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_object_metadata_10();
			Type_t * L_54 = ___type0;
			ObjectMetadata_t3566284522  L_55 = V_0;
			NullCheck(L_53);
			InterfaceActionInvoker2< Type_t *, ObjectMetadata_t3566284522  >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata>::Add(!0,!1) */, IDictionary_2_t179515681_il2cpp_TypeInfo_var, L_53, L_54, L_55);
			goto IL_016c;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1436737249 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t132251570_il2cpp_TypeInfo_var, e.ex->klass))
				goto CATCH_0166;
			throw e;
		}

CATCH_0166:
		{ // begin catch(System.ArgumentException)
			IL2CPP_LEAVE(0x179, FINALLY_0171);
		} // end catch (depth: 2)

IL_016c:
		{
			IL2CPP_LEAVE(0x179, FINALLY_0171);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0171;
	}

FINALLY_0171:
	{ // begin finally (depth: 1)
		Il2CppObject * L_56 = V_10;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(369)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(369)
	{
		IL2CPP_JUMP_TBL(0x179, IL_0179)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0179:
	{
		return;
	}
}
// System.Reflection.MethodInfo LitJson.JsonMapper::GetConvOp(System.Type,System.Type)
extern "C"  MethodInfo_t * JsonMapper_GetConvOp_m1492689601 (Il2CppObject * __this /* static, unused */, Type_t * ___t10, Type_t * ___t21, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_GetConvOp_m1492689601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	MethodInfo_t * V_3 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_lock_9();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
			Il2CppObject* L_2 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
			Type_t * L_3 = ___t10;
			NullCheck(L_2);
			bool L_4 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::ContainsKey(!0) */, IDictionary_2_t3694023158_il2cpp_TypeInfo_var, L_2, L_3);
			if (L_4)
			{
				goto IL_002c;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
			Il2CppObject* L_5 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
			Type_t * L_6 = ___t10;
			Dictionary_2_t27006016 * L_7 = (Dictionary_2_t27006016 *)il2cpp_codegen_object_new(Dictionary_2_t27006016_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m635195254(L_7, /*hidden argument*/Dictionary_2__ctor_m635195254_MethodInfo_var);
			NullCheck(L_5);
			InterfaceActionInvoker2< Type_t *, Il2CppObject* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::Add(!0,!1) */, IDictionary_2_t3694023158_il2cpp_TypeInfo_var, L_5, L_6, L_7);
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject* L_9 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
		Type_t * L_10 = ___t10;
		NullCheck(L_9);
		Il2CppObject* L_11 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::get_Item(!0) */, IDictionary_2_t3694023158_il2cpp_TypeInfo_var, L_9, L_10);
		Type_t * L_12 = ___t21;
		NullCheck(L_11);
		bool L_13 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>::ContainsKey(!0) */, IDictionary_2_t2785824703_il2cpp_TypeInfo_var, L_11, L_12);
		if (!L_13)
		{
			goto IL_0060;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject* L_14 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
		Type_t * L_15 = ___t10;
		NullCheck(L_14);
		Il2CppObject* L_16 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::get_Item(!0) */, IDictionary_2_t3694023158_il2cpp_TypeInfo_var, L_14, L_15);
		Type_t * L_17 = ___t21;
		NullCheck(L_16);
		MethodInfo_t * L_18 = InterfaceFuncInvoker1< MethodInfo_t *, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>::get_Item(!0) */, IDictionary_2_t2785824703_il2cpp_TypeInfo_var, L_16, L_17);
		return L_18;
	}

IL_0060:
	{
		Type_t * L_19 = ___t10;
		TypeU5BU5D_t3940880105* L_20 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_21 = ___t21;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_21);
		NullCheck(L_19);
		MethodInfo_t * L_22 = Type_GetMethod_m1479779718(L_19, _stringLiteral3306367446, L_20, /*hidden argument*/NULL);
		V_1 = L_22;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_23 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_lock_9();
		V_2 = L_23;
		Il2CppObject * L_24 = V_2;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
			Il2CppObject* L_25 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
			Type_t * L_26 = ___t10;
			NullCheck(L_25);
			Il2CppObject* L_27 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::get_Item(!0) */, IDictionary_2_t3694023158_il2cpp_TypeInfo_var, L_25, L_26);
			Type_t * L_28 = ___t21;
			MethodInfo_t * L_29 = V_1;
			NullCheck(L_27);
			InterfaceActionInvoker2< Type_t *, MethodInfo_t * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>::Add(!0,!1) */, IDictionary_2_t2785824703_il2cpp_TypeInfo_var, L_27, L_28, L_29);
			goto IL_00b1;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1436737249 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t132251570_il2cpp_TypeInfo_var, e.ex->klass))
				goto CATCH_0099;
			throw e;
		}

CATCH_0099:
		{ // begin catch(System.ArgumentException)
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
			Il2CppObject* L_30 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
			Type_t * L_31 = ___t10;
			NullCheck(L_30);
			Il2CppObject* L_32 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::get_Item(!0) */, IDictionary_2_t3694023158_il2cpp_TypeInfo_var, L_30, L_31);
			Type_t * L_33 = ___t21;
			NullCheck(L_32);
			MethodInfo_t * L_34 = InterfaceFuncInvoker1< MethodInfo_t *, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>::get_Item(!0) */, IDictionary_2_t2785824703_il2cpp_TypeInfo_var, L_32, L_33);
			V_3 = L_34;
			IL2CPP_LEAVE(0xBF, FINALLY_00b6);
		} // end catch (depth: 2)

IL_00b1:
		{
			IL2CPP_LEAVE(0xBD, FINALLY_00b6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_00b6;
	}

FINALLY_00b6:
	{ // begin finally (depth: 1)
		Il2CppObject * L_35 = V_2;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(182)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(182)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_JUMP_TBL(0xBD, IL_00bd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_00bd:
	{
		MethodInfo_t * L_36 = V_1;
		return L_36;
	}

IL_00bf:
	{
		MethodInfo_t * L_37 = V_3;
		return L_37;
	}
}
// System.Object LitJson.JsonMapper::ReadValue(System.Type,LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ReadValue_m1193038115 (Il2CppObject * __this /* static, unused */, Type_t * ___inst_type0, JsonReader_t836887441 * ___reader1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ReadValue_m1193038115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Type_t * V_2 = NULL;
	ImporterFunc_t3630937194 * V_3 = NULL;
	ImporterFunc_t3630937194 * V_4 = NULL;
	MethodInfo_t * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	ArrayMetadata_t894288939  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Il2CppObject * V_8 = NULL;
	Type_t * V_9 = NULL;
	Il2CppObject * V_10 = NULL;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	ObjectMetadata_t3566284522  V_13;
	memset(&V_13, 0, sizeof(V_13));
	String_t* V_14 = NULL;
	PropertyMetadata_t3727440473  V_15;
	memset(&V_15, 0, sizeof(V_15));
	PropertyInfo_t * V_16 = NULL;
	Type_t * G_B4_0 = NULL;
	Type_t * G_B3_0 = NULL;
	{
		JsonReader_t836887441 * L_0 = ___reader1;
		NullCheck(L_0);
		JsonReader_Read_m1579829852(L_0, /*hidden argument*/NULL);
		JsonReader_t836887441 * L_1 = ___reader1;
		NullCheck(L_1);
		int32_t L_2 = JsonReader_get_Token_m1333614796(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)5))))
		{
			goto IL_0015;
		}
	}
	{
		return NULL;
	}

IL_0015:
	{
		Type_t * L_3 = ___inst_type0;
		Type_t * L_4 = Nullable_GetUnderlyingType_m3905033790(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Type_t * L_5 = V_0;
		Type_t * L_6 = L_5;
		G_B3_0 = L_6;
		if (L_6)
		{
			G_B4_0 = L_6;
			goto IL_0025;
		}
	}
	{
		Type_t * L_7 = ___inst_type0;
		G_B4_0 = L_7;
	}

IL_0025:
	{
		V_1 = G_B4_0;
		JsonReader_t836887441 * L_8 = ___reader1;
		NullCheck(L_8);
		int32_t L_9 = JsonReader_get_Token_m1333614796(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0057;
		}
	}
	{
		Type_t * L_10 = ___inst_type0;
		NullCheck(L_10);
		bool L_11 = Type_get_IsClass_m589177581(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0044;
		}
	}
	{
		Type_t * L_12 = V_0;
		if (!L_12)
		{
			goto IL_0046;
		}
	}

IL_0044:
	{
		return NULL;
	}

IL_0046:
	{
		Type_t * L_13 = ___inst_type0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral2156791391, L_13, /*hidden argument*/NULL);
		JsonException_t3682484112 * L_15 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2214533437(L_15, L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0057:
	{
		JsonReader_t836887441 * L_16 = ___reader1;
		NullCheck(L_16);
		int32_t L_17 = JsonReader_get_Token_m1333614796(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) == ((int32_t)8)))
		{
			goto IL_0095;
		}
	}
	{
		JsonReader_t836887441 * L_18 = ___reader1;
		NullCheck(L_18);
		int32_t L_19 = JsonReader_get_Token_m1333614796(L_18, /*hidden argument*/NULL);
		if ((((int32_t)L_19) == ((int32_t)6)))
		{
			goto IL_0095;
		}
	}
	{
		JsonReader_t836887441 * L_20 = ___reader1;
		NullCheck(L_20);
		int32_t L_21 = JsonReader_get_Token_m1333614796(L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_21) == ((int32_t)7)))
		{
			goto IL_0095;
		}
	}
	{
		JsonReader_t836887441 * L_22 = ___reader1;
		NullCheck(L_22);
		int32_t L_23 = JsonReader_get_Token_m1333614796(L_22, /*hidden argument*/NULL);
		if ((((int32_t)L_23) == ((int32_t)((int32_t)9))))
		{
			goto IL_0095;
		}
	}
	{
		JsonReader_t836887441 * L_24 = ___reader1;
		NullCheck(L_24);
		int32_t L_25 = JsonReader_get_Token_m1333614796(L_24, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0198;
		}
	}

IL_0095:
	{
		JsonReader_t836887441 * L_26 = ___reader1;
		NullCheck(L_26);
		Il2CppObject * L_27 = JsonReader_get_Value_m3034661202(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Type_t * L_28 = Object_GetType_m88164663(L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		Type_t * L_29 = V_1;
		Type_t * L_30 = V_2;
		NullCheck(L_29);
		bool L_31 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_29, L_30);
		if (!L_31)
		{
			goto IL_00b4;
		}
	}
	{
		JsonReader_t836887441 * L_32 = ___reader1;
		NullCheck(L_32);
		Il2CppObject * L_33 = JsonReader_get_Value_m3034661202(L_32, /*hidden argument*/NULL);
		return L_33;
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject* L_34 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_custom_importers_table_5();
		Type_t * L_35 = V_2;
		NullCheck(L_34);
		bool L_36 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::ContainsKey(!0) */, IDictionary_2_t1152366808_il2cpp_TypeInfo_var, L_34, L_35);
		if (!L_36)
		{
			goto IL_00f9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject* L_37 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_custom_importers_table_5();
		Type_t * L_38 = V_2;
		NullCheck(L_37);
		Il2CppObject* L_39 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::get_Item(!0) */, IDictionary_2_t1152366808_il2cpp_TypeInfo_var, L_37, L_38);
		Type_t * L_40 = V_1;
		NullCheck(L_39);
		bool L_41 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>::ContainsKey(!0) */, IDictionary_2_t244168353_il2cpp_TypeInfo_var, L_39, L_40);
		if (!L_41)
		{
			goto IL_00f9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject* L_42 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_custom_importers_table_5();
		Type_t * L_43 = V_2;
		NullCheck(L_42);
		Il2CppObject* L_44 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::get_Item(!0) */, IDictionary_2_t1152366808_il2cpp_TypeInfo_var, L_42, L_43);
		Type_t * L_45 = V_1;
		NullCheck(L_44);
		ImporterFunc_t3630937194 * L_46 = InterfaceFuncInvoker1< ImporterFunc_t3630937194 *, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>::get_Item(!0) */, IDictionary_2_t244168353_il2cpp_TypeInfo_var, L_44, L_45);
		V_3 = L_46;
		ImporterFunc_t3630937194 * L_47 = V_3;
		JsonReader_t836887441 * L_48 = ___reader1;
		NullCheck(L_48);
		Il2CppObject * L_49 = JsonReader_get_Value_m3034661202(L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		Il2CppObject * L_50 = ImporterFunc_Invoke_m1838490826(L_47, L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_00f9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject* L_51 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		Type_t * L_52 = V_2;
		NullCheck(L_51);
		bool L_53 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::ContainsKey(!0) */, IDictionary_2_t1152366808_il2cpp_TypeInfo_var, L_51, L_52);
		if (!L_53)
		{
			goto IL_0140;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject* L_54 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		Type_t * L_55 = V_2;
		NullCheck(L_54);
		Il2CppObject* L_56 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::get_Item(!0) */, IDictionary_2_t1152366808_il2cpp_TypeInfo_var, L_54, L_55);
		Type_t * L_57 = V_1;
		NullCheck(L_56);
		bool L_58 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>::ContainsKey(!0) */, IDictionary_2_t244168353_il2cpp_TypeInfo_var, L_56, L_57);
		if (!L_58)
		{
			goto IL_0140;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject* L_59 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		Type_t * L_60 = V_2;
		NullCheck(L_59);
		Il2CppObject* L_61 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::get_Item(!0) */, IDictionary_2_t1152366808_il2cpp_TypeInfo_var, L_59, L_60);
		Type_t * L_62 = V_1;
		NullCheck(L_61);
		ImporterFunc_t3630937194 * L_63 = InterfaceFuncInvoker1< ImporterFunc_t3630937194 *, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>::get_Item(!0) */, IDictionary_2_t244168353_il2cpp_TypeInfo_var, L_61, L_62);
		V_4 = L_63;
		ImporterFunc_t3630937194 * L_64 = V_4;
		JsonReader_t836887441 * L_65 = ___reader1;
		NullCheck(L_65);
		Il2CppObject * L_66 = JsonReader_get_Value_m3034661202(L_65, /*hidden argument*/NULL);
		NullCheck(L_64);
		Il2CppObject * L_67 = ImporterFunc_Invoke_m1838490826(L_64, L_66, /*hidden argument*/NULL);
		return L_67;
	}

IL_0140:
	{
		Type_t * L_68 = V_1;
		NullCheck(L_68);
		bool L_69 = Type_get_IsEnum_m208091508(L_68, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_0158;
		}
	}
	{
		Type_t * L_70 = V_1;
		JsonReader_t836887441 * L_71 = ___reader1;
		NullCheck(L_71);
		Il2CppObject * L_72 = JsonReader_get_Value_m3034661202(L_71, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t4135868527_il2cpp_TypeInfo_var);
		Il2CppObject * L_73 = Enum_ToObject_m1628250250(NULL /*static, unused*/, L_70, L_72, /*hidden argument*/NULL);
		return L_73;
	}

IL_0158:
	{
		Type_t * L_74 = V_1;
		Type_t * L_75 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		MethodInfo_t * L_76 = JsonMapper_GetConvOp_m1492689601(NULL /*static, unused*/, L_74, L_75, /*hidden argument*/NULL);
		V_5 = L_76;
		MethodInfo_t * L_77 = V_5;
		if (!L_77)
		{
			goto IL_0180;
		}
	}
	{
		MethodInfo_t * L_78 = V_5;
		ObjectU5BU5D_t2843939325* L_79 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		JsonReader_t836887441 * L_80 = ___reader1;
		NullCheck(L_80);
		Il2CppObject * L_81 = JsonReader_get_Value_m3034661202(L_80, /*hidden argument*/NULL);
		NullCheck(L_79);
		ArrayElementTypeCheck (L_79, L_81);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_81);
		NullCheck(L_78);
		Il2CppObject * L_82 = MethodBase_Invoke_m1776411915(L_78, NULL, L_79, /*hidden argument*/NULL);
		return L_82;
	}

IL_0180:
	{
		JsonReader_t836887441 * L_83 = ___reader1;
		NullCheck(L_83);
		Il2CppObject * L_84 = JsonReader_get_Value_m3034661202(L_83, /*hidden argument*/NULL);
		Type_t * L_85 = V_2;
		Type_t * L_86 = ___inst_type0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_87 = String_Format_m3339413201(NULL /*static, unused*/, _stringLiteral57478429, L_84, L_85, L_86, /*hidden argument*/NULL);
		JsonException_t3682484112 * L_88 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2214533437(L_88, L_87, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_88);
	}

IL_0198:
	{
		V_6 = NULL;
		JsonReader_t836887441 * L_89 = ___reader1;
		NullCheck(L_89);
		int32_t L_90 = JsonReader_get_Token_m1333614796(L_89, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_90) == ((uint32_t)4))))
		{
			goto IL_02a6;
		}
	}
	{
		Type_t * L_91 = ___inst_type0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		JsonMapper_AddArrayMetadata_m3968694626(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		Il2CppObject* L_92 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_array_metadata_6();
		Type_t * L_93 = ___inst_type0;
		NullCheck(L_92);
		ArrayMetadata_t894288939  L_94 = InterfaceFuncInvoker1< ArrayMetadata_t894288939 , Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata>::get_Item(!0) */, IDictionary_2_t1802487394_il2cpp_TypeInfo_var, L_92, L_93);
		V_7 = L_94;
		bool L_95 = ArrayMetadata_get_IsArray_m3389222635((&V_7), /*hidden argument*/NULL);
		if (L_95)
		{
			goto IL_01e3;
		}
	}
	{
		bool L_96 = ArrayMetadata_get_IsList_m2672124584((&V_7), /*hidden argument*/NULL);
		if (L_96)
		{
			goto IL_01e3;
		}
	}
	{
		Type_t * L_97 = ___inst_type0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_98 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral3271142035, L_97, /*hidden argument*/NULL);
		JsonException_t3682484112 * L_99 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2214533437(L_99, L_98, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_99);
	}

IL_01e3:
	{
		bool L_100 = ArrayMetadata_get_IsArray_m3389222635((&V_7), /*hidden argument*/NULL);
		if (L_100)
		{
			goto IL_020a;
		}
	}
	{
		Type_t * L_101 = ___inst_type0;
		Il2CppObject * L_102 = Activator_CreateInstance_m3631483688(NULL /*static, unused*/, L_101, /*hidden argument*/NULL);
		V_8 = ((Il2CppObject *)Castclass(L_102, IList_t2094931216_il2cpp_TypeInfo_var));
		Type_t * L_103 = ArrayMetadata_get_ElementType_m2810930502((&V_7), /*hidden argument*/NULL);
		V_9 = L_103;
		goto IL_0219;
	}

IL_020a:
	{
		ArrayList_t2718874744 * L_104 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4254721275(L_104, /*hidden argument*/NULL);
		V_8 = L_104;
		Type_t * L_105 = ___inst_type0;
		NullCheck(L_105);
		Type_t * L_106 = VirtFuncInvoker0< Type_t * >::Invoke(44 /* System.Type System.Type::GetElementType() */, L_105);
		V_9 = L_106;
	}

IL_0219:
	{
		Type_t * L_107 = V_9;
		JsonReader_t836887441 * L_108 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_109 = JsonMapper_ReadValue_m1193038115(NULL /*static, unused*/, L_107, L_108, /*hidden argument*/NULL);
		V_10 = L_109;
		Il2CppObject * L_110 = V_10;
		if (L_110)
		{
			goto IL_023b;
		}
	}
	{
		JsonReader_t836887441 * L_111 = ___reader1;
		NullCheck(L_111);
		int32_t L_112 = JsonReader_get_Token_m1333614796(L_111, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_112) == ((uint32_t)5))))
		{
			goto IL_023b;
		}
	}
	{
		goto IL_024a;
	}

IL_023b:
	{
		Il2CppObject * L_113 = V_8;
		Il2CppObject * L_114 = V_10;
		NullCheck(L_113);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t2094931216_il2cpp_TypeInfo_var, L_113, L_114);
		goto IL_0219;
	}

IL_024a:
	{
		bool L_115 = ArrayMetadata_get_IsArray_m3389222635((&V_7), /*hidden argument*/NULL);
		if (!L_115)
		{
			goto IL_029d;
		}
	}
	{
		Il2CppObject * L_116 = V_8;
		NullCheck(L_116);
		int32_t L_117 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t3904884886_il2cpp_TypeInfo_var, L_116);
		V_11 = L_117;
		Type_t * L_118 = V_9;
		int32_t L_119 = V_11;
		Il2CppArray * L_120 = Array_CreateInstance_m2750085942(NULL /*static, unused*/, L_118, L_119, /*hidden argument*/NULL);
		V_6 = L_120;
		V_12 = 0;
		goto IL_028f;
	}

IL_0272:
	{
		Il2CppObject * L_121 = V_6;
		Il2CppObject * L_122 = V_8;
		int32_t L_123 = V_12;
		NullCheck(L_122);
		Il2CppObject * L_124 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t2094931216_il2cpp_TypeInfo_var, L_122, L_123);
		int32_t L_125 = V_12;
		NullCheck(((Il2CppArray *)CastclassClass(L_121, Il2CppArray_il2cpp_TypeInfo_var)));
		Array_SetValue_m3412255035(((Il2CppArray *)CastclassClass(L_121, Il2CppArray_il2cpp_TypeInfo_var)), L_124, L_125, /*hidden argument*/NULL);
		int32_t L_126 = V_12;
		V_12 = ((int32_t)((int32_t)L_126+(int32_t)1));
	}

IL_028f:
	{
		int32_t L_127 = V_12;
		int32_t L_128 = V_11;
		if ((((int32_t)L_127) < ((int32_t)L_128)))
		{
			goto IL_0272;
		}
	}
	{
		goto IL_02a1;
	}

IL_029d:
	{
		Il2CppObject * L_129 = V_8;
		V_6 = L_129;
	}

IL_02a1:
	{
		goto IL_03e4;
	}

IL_02a6:
	{
		JsonReader_t836887441 * L_130 = ___reader1;
		NullCheck(L_130);
		int32_t L_131 = JsonReader_get_Token_m1333614796(L_130, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_131) == ((uint32_t)1))))
		{
			goto IL_03e4;
		}
	}
	{
		Type_t * L_132 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		JsonMapper_AddObjectMetadata_m194540947(NULL /*static, unused*/, L_132, /*hidden argument*/NULL);
		Il2CppObject* L_133 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_object_metadata_10();
		Type_t * L_134 = V_1;
		NullCheck(L_133);
		ObjectMetadata_t3566284522  L_135 = InterfaceFuncInvoker1< ObjectMetadata_t3566284522 , Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata>::get_Item(!0) */, IDictionary_2_t179515681_il2cpp_TypeInfo_var, L_133, L_134);
		V_13 = L_135;
		Type_t * L_136 = V_1;
		Il2CppObject * L_137 = Activator_CreateInstance_m3631483688(NULL /*static, unused*/, L_136, /*hidden argument*/NULL);
		V_6 = L_137;
	}

IL_02cd:
	{
		JsonReader_t836887441 * L_138 = ___reader1;
		NullCheck(L_138);
		JsonReader_Read_m1579829852(L_138, /*hidden argument*/NULL);
		JsonReader_t836887441 * L_139 = ___reader1;
		NullCheck(L_139);
		int32_t L_140 = JsonReader_get_Token_m1333614796(L_139, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_140) == ((uint32_t)3))))
		{
			goto IL_02e5;
		}
	}
	{
		goto IL_03e4;
	}

IL_02e5:
	{
		JsonReader_t836887441 * L_141 = ___reader1;
		NullCheck(L_141);
		Il2CppObject * L_142 = JsonReader_get_Value_m3034661202(L_141, /*hidden argument*/NULL);
		V_14 = ((String_t*)CastclassSealed(L_142, String_t_il2cpp_TypeInfo_var));
		Il2CppObject* L_143 = ObjectMetadata_get_Properties_m3723938162((&V_13), /*hidden argument*/NULL);
		String_t* L_144 = V_14;
		NullCheck(L_143);
		bool L_145 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>::ContainsKey(!0) */, IDictionary_2_t1976548163_il2cpp_TypeInfo_var, L_143, L_144);
		if (!L_145)
		{
			goto IL_038f;
		}
	}
	{
		Il2CppObject* L_146 = ObjectMetadata_get_Properties_m3723938162((&V_13), /*hidden argument*/NULL);
		String_t* L_147 = V_14;
		NullCheck(L_146);
		PropertyMetadata_t3727440473  L_148 = InterfaceFuncInvoker1< PropertyMetadata_t3727440473 , String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>::get_Item(!0) */, IDictionary_2_t1976548163_il2cpp_TypeInfo_var, L_146, L_147);
		V_15 = L_148;
		bool L_149 = (&V_15)->get_IsField_1();
		if (!L_149)
		{
			goto IL_0346;
		}
	}
	{
		MemberInfo_t * L_150 = (&V_15)->get_Info_0();
		Il2CppObject * L_151 = V_6;
		Type_t * L_152 = (&V_15)->get_Type_2();
		JsonReader_t836887441 * L_153 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_154 = JsonMapper_ReadValue_m1193038115(NULL /*static, unused*/, L_152, L_153, /*hidden argument*/NULL);
		NullCheck(((FieldInfo_t *)CastclassClass(L_150, FieldInfo_t_il2cpp_TypeInfo_var)));
		FieldInfo_SetValue_m2460171138(((FieldInfo_t *)CastclassClass(L_150, FieldInfo_t_il2cpp_TypeInfo_var)), L_151, L_154, /*hidden argument*/NULL);
		goto IL_038a;
	}

IL_0346:
	{
		MemberInfo_t * L_155 = (&V_15)->get_Info_0();
		V_16 = ((PropertyInfo_t *)CastclassClass(L_155, PropertyInfo_t_il2cpp_TypeInfo_var));
		PropertyInfo_t * L_156 = V_16;
		NullCheck(L_156);
		bool L_157 = VirtFuncInvoker0< bool >::Invoke(16 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_156);
		if (!L_157)
		{
			goto IL_037c;
		}
	}
	{
		PropertyInfo_t * L_158 = V_16;
		Il2CppObject * L_159 = V_6;
		Type_t * L_160 = (&V_15)->get_Type_2();
		JsonReader_t836887441 * L_161 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_162 = JsonMapper_ReadValue_m1193038115(NULL /*static, unused*/, L_160, L_161, /*hidden argument*/NULL);
		NullCheck(L_158);
		VirtActionInvoker3< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t2843939325* >::Invoke(24 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[]) */, L_158, L_159, L_162, (ObjectU5BU5D_t2843939325*)(ObjectU5BU5D_t2843939325*)NULL);
		goto IL_038a;
	}

IL_037c:
	{
		Type_t * L_163 = (&V_15)->get_Type_2();
		JsonReader_t836887441 * L_164 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		JsonMapper_ReadValue_m1193038115(NULL /*static, unused*/, L_163, L_164, /*hidden argument*/NULL);
	}

IL_038a:
	{
		goto IL_03df;
	}

IL_038f:
	{
		bool L_165 = ObjectMetadata_get_IsDictionary_m1062828228((&V_13), /*hidden argument*/NULL);
		if (L_165)
		{
			goto IL_03c4;
		}
	}
	{
		JsonReader_t836887441 * L_166 = ___reader1;
		NullCheck(L_166);
		bool L_167 = JsonReader_get_SkipNonMembers_m3135707401(L_166, /*hidden argument*/NULL);
		if (L_167)
		{
			goto IL_03b9;
		}
	}
	{
		Type_t * L_168 = ___inst_type0;
		String_t* L_169 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_170 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral888960820, L_168, L_169, /*hidden argument*/NULL);
		JsonException_t3682484112 * L_171 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2214533437(L_171, L_170, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_171);
	}

IL_03b9:
	{
		JsonReader_t836887441 * L_172 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		JsonMapper_ReadSkip_m3223759513(NULL /*static, unused*/, L_172, /*hidden argument*/NULL);
		goto IL_02cd;
	}

IL_03c4:
	{
		Il2CppObject * L_173 = V_6;
		String_t* L_174 = V_14;
		Type_t * L_175 = ObjectMetadata_get_ElementType_m991775292((&V_13), /*hidden argument*/NULL);
		JsonReader_t836887441 * L_176 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_177 = JsonMapper_ReadValue_m1193038115(NULL /*static, unused*/, L_175, L_176, /*hidden argument*/NULL);
		NullCheck(((Il2CppObject *)Castclass(L_173, IDictionary_t1363984059_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t1363984059_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_173, IDictionary_t1363984059_il2cpp_TypeInfo_var)), L_174, L_177);
	}

IL_03df:
	{
		goto IL_02cd;
	}

IL_03e4:
	{
		Il2CppObject * L_178 = V_6;
		return L_178;
	}
}
// LitJson.IJsonWrapper LitJson.JsonMapper::ReadValue(LitJson.WrapperFactory,LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ReadValue_m3060360668 (Il2CppObject * __this /* static, unused */, WrapperFactory_t2158548929 * ___factory0, JsonReader_t836887441 * ___reader1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ReadValue_m3060360668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	String_t* V_2 = NULL;
	{
		JsonReader_t836887441 * L_0 = ___reader1;
		NullCheck(L_0);
		JsonReader_Read_m1579829852(L_0, /*hidden argument*/NULL);
		JsonReader_t836887441 * L_1 = ___reader1;
		NullCheck(L_1);
		int32_t L_2 = JsonReader_get_Token_m1333614796(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)5)))
		{
			goto IL_0020;
		}
	}
	{
		JsonReader_t836887441 * L_3 = ___reader1;
		NullCheck(L_3);
		int32_t L_4 = JsonReader_get_Token_m1333614796(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (Il2CppObject *)NULL;
	}

IL_0022:
	{
		WrapperFactory_t2158548929 * L_5 = ___factory0;
		NullCheck(L_5);
		Il2CppObject * L_6 = WrapperFactory_Invoke_m1494838554(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		JsonReader_t836887441 * L_7 = ___reader1;
		NullCheck(L_7);
		int32_t L_8 = JsonReader_get_Token_m1333614796(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_0049;
		}
	}
	{
		Il2CppObject * L_9 = V_0;
		JsonReader_t836887441 * L_10 = ___reader1;
		NullCheck(L_10);
		Il2CppObject * L_11 = JsonReader_get_Value_m3034661202(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		InterfaceActionInvoker1< String_t* >::Invoke(5 /* System.Void LitJson.IJsonWrapper::SetString(System.String) */, IJsonWrapper_t1028825384_il2cpp_TypeInfo_var, L_9, ((String_t*)CastclassSealed(L_11, String_t_il2cpp_TypeInfo_var)));
		Il2CppObject * L_12 = V_0;
		return L_12;
	}

IL_0049:
	{
		JsonReader_t836887441 * L_13 = ___reader1;
		NullCheck(L_13);
		int32_t L_14 = JsonReader_get_Token_m1333614796(L_13, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)8))))
		{
			goto IL_0068;
		}
	}
	{
		Il2CppObject * L_15 = V_0;
		JsonReader_t836887441 * L_16 = ___reader1;
		NullCheck(L_16);
		Il2CppObject * L_17 = JsonReader_get_Value_m3034661202(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		InterfaceActionInvoker1< double >::Invoke(1 /* System.Void LitJson.IJsonWrapper::SetDouble(System.Double) */, IJsonWrapper_t1028825384_il2cpp_TypeInfo_var, L_15, ((*(double*)((double*)UnBox(L_17, Double_t594665363_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_18 = V_0;
		return L_18;
	}

IL_0068:
	{
		JsonReader_t836887441 * L_19 = ___reader1;
		NullCheck(L_19);
		int32_t L_20 = JsonReader_get_Token_m1333614796(L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)6))))
		{
			goto IL_0087;
		}
	}
	{
		Il2CppObject * L_21 = V_0;
		JsonReader_t836887441 * L_22 = ___reader1;
		NullCheck(L_22);
		Il2CppObject * L_23 = JsonReader_get_Value_m3034661202(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void LitJson.IJsonWrapper::SetInt(System.Int32) */, IJsonWrapper_t1028825384_il2cpp_TypeInfo_var, L_21, ((*(int32_t*)((int32_t*)UnBox(L_23, Int32_t2950945753_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_24 = V_0;
		return L_24;
	}

IL_0087:
	{
		JsonReader_t836887441 * L_25 = ___reader1;
		NullCheck(L_25);
		int32_t L_26 = JsonReader_get_Token_m1333614796(L_25, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)7))))
		{
			goto IL_00a6;
		}
	}
	{
		Il2CppObject * L_27 = V_0;
		JsonReader_t836887441 * L_28 = ___reader1;
		NullCheck(L_28);
		Il2CppObject * L_29 = JsonReader_get_Value_m3034661202(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		InterfaceActionInvoker1< int64_t >::Invoke(4 /* System.Void LitJson.IJsonWrapper::SetLong(System.Int64) */, IJsonWrapper_t1028825384_il2cpp_TypeInfo_var, L_27, ((*(int64_t*)((int64_t*)UnBox(L_29, Int64_t3736567304_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_30 = V_0;
		return L_30;
	}

IL_00a6:
	{
		JsonReader_t836887441 * L_31 = ___reader1;
		NullCheck(L_31);
		int32_t L_32 = JsonReader_get_Token_m1333614796(L_31, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00c6;
		}
	}
	{
		Il2CppObject * L_33 = V_0;
		JsonReader_t836887441 * L_34 = ___reader1;
		NullCheck(L_34);
		Il2CppObject * L_35 = JsonReader_get_Value_m3034661202(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		InterfaceActionInvoker1< bool >::Invoke(0 /* System.Void LitJson.IJsonWrapper::SetBoolean(System.Boolean) */, IJsonWrapper_t1028825384_il2cpp_TypeInfo_var, L_33, ((*(bool*)((bool*)UnBox(L_35, Boolean_t97287965_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_36 = V_0;
		return L_36;
	}

IL_00c6:
	{
		JsonReader_t836887441 * L_37 = ___reader1;
		NullCheck(L_37);
		int32_t L_38 = JsonReader_get_Token_m1333614796(L_37, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_38) == ((uint32_t)4))))
		{
			goto IL_010a;
		}
	}
	{
		Il2CppObject * L_39 = V_0;
		NullCheck(L_39);
		InterfaceActionInvoker1< int32_t >::Invoke(3 /* System.Void LitJson.IJsonWrapper::SetJsonType(LitJson.JsonType) */, IJsonWrapper_t1028825384_il2cpp_TypeInfo_var, L_39, 2);
	}

IL_00d9:
	{
		WrapperFactory_t2158548929 * L_40 = ___factory0;
		JsonReader_t836887441 * L_41 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_42 = JsonMapper_ReadValue_m3060360668(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		V_1 = L_42;
		Il2CppObject * L_43 = V_1;
		if (L_43)
		{
			goto IL_00f8;
		}
	}
	{
		JsonReader_t836887441 * L_44 = ___reader1;
		NullCheck(L_44);
		int32_t L_45 = JsonReader_get_Token_m1333614796(L_44, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_45) == ((uint32_t)5))))
		{
			goto IL_00f8;
		}
	}
	{
		goto IL_0105;
	}

IL_00f8:
	{
		Il2CppObject * L_46 = V_0;
		Il2CppObject * L_47 = V_1;
		NullCheck(L_46);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t2094931216_il2cpp_TypeInfo_var, L_46, L_47);
		goto IL_00d9;
	}

IL_0105:
	{
		goto IL_0154;
	}

IL_010a:
	{
		JsonReader_t836887441 * L_48 = ___reader1;
		NullCheck(L_48);
		int32_t L_49 = JsonReader_get_Token_m1333614796(L_48, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_49) == ((uint32_t)1))))
		{
			goto IL_0154;
		}
	}
	{
		Il2CppObject * L_50 = V_0;
		NullCheck(L_50);
		InterfaceActionInvoker1< int32_t >::Invoke(3 /* System.Void LitJson.IJsonWrapper::SetJsonType(LitJson.JsonType) */, IJsonWrapper_t1028825384_il2cpp_TypeInfo_var, L_50, 1);
	}

IL_011d:
	{
		JsonReader_t836887441 * L_51 = ___reader1;
		NullCheck(L_51);
		JsonReader_Read_m1579829852(L_51, /*hidden argument*/NULL);
		JsonReader_t836887441 * L_52 = ___reader1;
		NullCheck(L_52);
		int32_t L_53 = JsonReader_get_Token_m1333614796(L_52, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_53) == ((uint32_t)3))))
		{
			goto IL_0135;
		}
	}
	{
		goto IL_0154;
	}

IL_0135:
	{
		JsonReader_t836887441 * L_54 = ___reader1;
		NullCheck(L_54);
		Il2CppObject * L_55 = JsonReader_get_Value_m3034661202(L_54, /*hidden argument*/NULL);
		V_2 = ((String_t*)CastclassSealed(L_55, String_t_il2cpp_TypeInfo_var));
		Il2CppObject * L_56 = V_0;
		String_t* L_57 = V_2;
		WrapperFactory_t2158548929 * L_58 = ___factory0;
		JsonReader_t836887441 * L_59 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_60 = JsonMapper_ReadValue_m3060360668(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
		NullCheck(L_56);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(1 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1363984059_il2cpp_TypeInfo_var, L_56, L_57, L_60);
		goto IL_011d;
	}

IL_0154:
	{
		Il2CppObject * L_61 = V_0;
		return L_61;
	}
}
// System.Void LitJson.JsonMapper::ReadSkip(LitJson.JsonReader)
extern "C"  void JsonMapper_ReadSkip_m3223759513 (Il2CppObject * __this /* static, unused */, JsonReader_t836887441 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ReadSkip_m3223759513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		WrapperFactory_t2158548929 * L_0 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_16();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)JsonMapper_U3CReadSkipU3Em__0_m139886084_MethodInfo_var);
		WrapperFactory_t2158548929 * L_2 = (WrapperFactory_t2158548929 *)il2cpp_codegen_object_new(WrapperFactory_t2158548929_il2cpp_TypeInfo_var);
		WrapperFactory__ctor_m283261507(L_2, NULL, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_16(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		WrapperFactory_t2158548929 * L_3 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_16();
		JsonReader_t836887441 * L_4 = ___reader0;
		JsonMapper_ToWrapper_m1467426324(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::RegisterBaseExporters()
extern "C"  void JsonMapper_RegisterBaseExporters_m1355390223 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_RegisterBaseExporters_m1355390223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * G_B2_0 = NULL;
	Il2CppObject* G_B2_1 = NULL;
	Type_t * G_B1_0 = NULL;
	Il2CppObject* G_B1_1 = NULL;
	Type_t * G_B4_0 = NULL;
	Il2CppObject* G_B4_1 = NULL;
	Type_t * G_B3_0 = NULL;
	Il2CppObject* G_B3_1 = NULL;
	Type_t * G_B6_0 = NULL;
	Il2CppObject* G_B6_1 = NULL;
	Type_t * G_B5_0 = NULL;
	Il2CppObject* G_B5_1 = NULL;
	Type_t * G_B8_0 = NULL;
	Il2CppObject* G_B8_1 = NULL;
	Type_t * G_B7_0 = NULL;
	Il2CppObject* G_B7_1 = NULL;
	Type_t * G_B10_0 = NULL;
	Il2CppObject* G_B10_1 = NULL;
	Type_t * G_B9_0 = NULL;
	Il2CppObject* G_B9_1 = NULL;
	Type_t * G_B12_0 = NULL;
	Il2CppObject* G_B12_1 = NULL;
	Type_t * G_B11_0 = NULL;
	Il2CppObject* G_B11_1 = NULL;
	Type_t * G_B14_0 = NULL;
	Il2CppObject* G_B14_1 = NULL;
	Type_t * G_B13_0 = NULL;
	Il2CppObject* G_B13_1 = NULL;
	Type_t * G_B16_0 = NULL;
	Il2CppObject* G_B16_1 = NULL;
	Type_t * G_B15_0 = NULL;
	Il2CppObject* G_B15_1 = NULL;
	Type_t * G_B18_0 = NULL;
	Il2CppObject* G_B18_1 = NULL;
	Type_t * G_B17_0 = NULL;
	Il2CppObject* G_B17_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Byte_t1134296376_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t1851311465 * L_2 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_17();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_0027;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__1_m3570407405_MethodInfo_var);
		ExporterFunc_t1851311465 * L_4 = (ExporterFunc_t1851311465 *)il2cpp_codegen_object_new(ExporterFunc_t1851311465_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1737319807(L_4, NULL, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_17(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ExporterFunc_t1851311465 * L_5 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_17();
		NullCheck(G_B2_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t1851311465 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t2759509920_il2cpp_TypeInfo_var, G_B2_1, G_B2_0, L_5);
		Il2CppObject* L_6 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Char_t3634460470_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t1851311465 * L_8 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_18();
		G_B3_0 = L_7;
		G_B3_1 = L_6;
		if (L_8)
		{
			G_B4_0 = L_7;
			G_B4_1 = L_6;
			goto IL_0058;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__2_m600765210_MethodInfo_var);
		ExporterFunc_t1851311465 * L_10 = (ExporterFunc_t1851311465 *)il2cpp_codegen_object_new(ExporterFunc_t1851311465_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1737319807(L_10, NULL, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_18(L_10);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ExporterFunc_t1851311465 * L_11 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_18();
		NullCheck(G_B4_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t1851311465 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t2759509920_il2cpp_TypeInfo_var, G_B4_1, G_B4_0, L_11);
		Il2CppObject* L_12 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(DateTime_t3738529785_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t1851311465 * L_14 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_19();
		G_B5_0 = L_13;
		G_B5_1 = L_12;
		if (L_14)
		{
			G_B6_0 = L_13;
			G_B6_1 = L_12;
			goto IL_0089;
		}
	}
	{
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__3_m939732965_MethodInfo_var);
		ExporterFunc_t1851311465 * L_16 = (ExporterFunc_t1851311465 *)il2cpp_codegen_object_new(ExporterFunc_t1851311465_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1737319807(L_16, NULL, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_19(L_16);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
	}

IL_0089:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ExporterFunc_t1851311465 * L_17 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_19();
		NullCheck(G_B6_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t1851311465 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t2759509920_il2cpp_TypeInfo_var, G_B6_1, G_B6_0, L_17);
		Il2CppObject* L_18 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Decimal_t2948259380_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t1851311465 * L_20 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_20();
		G_B7_0 = L_19;
		G_B7_1 = L_18;
		if (L_20)
		{
			G_B8_0 = L_19;
			G_B8_1 = L_18;
			goto IL_00ba;
		}
	}
	{
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__4_m266458088_MethodInfo_var);
		ExporterFunc_t1851311465 * L_22 = (ExporterFunc_t1851311465 *)il2cpp_codegen_object_new(ExporterFunc_t1851311465_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1737319807(L_22, NULL, L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_20(L_22);
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
	}

IL_00ba:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ExporterFunc_t1851311465 * L_23 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_20();
		NullCheck(G_B8_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t1851311465 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t2759509920_il2cpp_TypeInfo_var, G_B8_1, G_B8_0, L_23);
		Il2CppObject* L_24 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(SByte_t1669577662_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t1851311465 * L_26 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_21();
		G_B9_0 = L_25;
		G_B9_1 = L_24;
		if (L_26)
		{
			G_B10_0 = L_25;
			G_B10_1 = L_24;
			goto IL_00eb;
		}
	}
	{
		IntPtr_t L_27;
		L_27.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__5_m3328515821_MethodInfo_var);
		ExporterFunc_t1851311465 * L_28 = (ExporterFunc_t1851311465 *)il2cpp_codegen_object_new(ExporterFunc_t1851311465_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1737319807(L_28, NULL, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_21(L_28);
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
	}

IL_00eb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ExporterFunc_t1851311465 * L_29 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_21();
		NullCheck(G_B10_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t1851311465 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t2759509920_il2cpp_TypeInfo_var, G_B10_1, G_B10_0, L_29);
		Il2CppObject* L_30 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_31 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int16_t2552820387_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t1851311465 * L_32 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_22();
		G_B11_0 = L_31;
		G_B11_1 = L_30;
		if (L_32)
		{
			G_B12_0 = L_31;
			G_B12_1 = L_30;
			goto IL_011c;
		}
	}
	{
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__6_m312889811_MethodInfo_var);
		ExporterFunc_t1851311465 * L_34 = (ExporterFunc_t1851311465 *)il2cpp_codegen_object_new(ExporterFunc_t1851311465_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1737319807(L_34, NULL, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache6_22(L_34);
		G_B12_0 = G_B11_0;
		G_B12_1 = G_B11_1;
	}

IL_011c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ExporterFunc_t1851311465 * L_35 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_22();
		NullCheck(G_B12_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t1851311465 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t2759509920_il2cpp_TypeInfo_var, G_B12_1, G_B12_0, L_35);
		Il2CppObject* L_36 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_37 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(UInt16_t2177724958_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t1851311465 * L_38 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_23();
		G_B13_0 = L_37;
		G_B13_1 = L_36;
		if (L_38)
		{
			G_B14_0 = L_37;
			G_B14_1 = L_36;
			goto IL_014d;
		}
	}
	{
		IntPtr_t L_39;
		L_39.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__7_m3877741659_MethodInfo_var);
		ExporterFunc_t1851311465 * L_40 = (ExporterFunc_t1851311465 *)il2cpp_codegen_object_new(ExporterFunc_t1851311465_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1737319807(L_40, NULL, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache7_23(L_40);
		G_B14_0 = G_B13_0;
		G_B14_1 = G_B13_1;
	}

IL_014d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ExporterFunc_t1851311465 * L_41 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_23();
		NullCheck(G_B14_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t1851311465 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t2759509920_il2cpp_TypeInfo_var, G_B14_1, G_B14_0, L_41);
		Il2CppObject* L_42 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(UInt32_t2560061978_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t1851311465 * L_44 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache8_24();
		G_B15_0 = L_43;
		G_B15_1 = L_42;
		if (L_44)
		{
			G_B16_0 = L_43;
			G_B16_1 = L_42;
			goto IL_017e;
		}
	}
	{
		IntPtr_t L_45;
		L_45.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__8_m225163408_MethodInfo_var);
		ExporterFunc_t1851311465 * L_46 = (ExporterFunc_t1851311465 *)il2cpp_codegen_object_new(ExporterFunc_t1851311465_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1737319807(L_46, NULL, L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache8_24(L_46);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
	}

IL_017e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ExporterFunc_t1851311465 * L_47 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache8_24();
		NullCheck(G_B16_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t1851311465 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t2759509920_il2cpp_TypeInfo_var, G_B16_1, G_B16_0, L_47);
		Il2CppObject* L_48 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_49 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(UInt64_t4134040092_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t1851311465 * L_50 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_25();
		G_B17_0 = L_49;
		G_B17_1 = L_48;
		if (L_50)
		{
			G_B18_0 = L_49;
			G_B18_1 = L_48;
			goto IL_01af;
		}
	}
	{
		IntPtr_t L_51;
		L_51.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__9_m1344668283_MethodInfo_var);
		ExporterFunc_t1851311465 * L_52 = (ExporterFunc_t1851311465 *)il2cpp_codegen_object_new(ExporterFunc_t1851311465_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m1737319807(L_52, NULL, L_51, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache9_25(L_52);
		G_B18_0 = G_B17_0;
		G_B18_1 = G_B17_1;
	}

IL_01af:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ExporterFunc_t1851311465 * L_53 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_25();
		NullCheck(G_B18_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t1851311465 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t2759509920_il2cpp_TypeInfo_var, G_B18_1, G_B18_0, L_53);
		return;
	}
}
// System.Void LitJson.JsonMapper::RegisterBaseImporters()
extern "C"  void JsonMapper_RegisterBaseImporters_m623152139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_RegisterBaseImporters_m623152139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ImporterFunc_t3630937194 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_0 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheA_26();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__A_m3123255486_MethodInfo_var);
		ImporterFunc_t3630937194 * L_2 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_2, NULL, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheA_26(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_3 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheA_26();
		V_0 = L_3;
		Il2CppObject* L_4 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int32_t2950945753_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Byte_t1134296376_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_7 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_8 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheB_27();
		if (L_8)
		{
			goto IL_0055;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__B_m3123219883_MethodInfo_var);
		ImporterFunc_t3630937194 * L_10 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_10, NULL, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheB_27(L_10);
	}

IL_0055:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_11 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheB_27();
		V_0 = L_11;
		Il2CppObject* L_12 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int32_t2950945753_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_14 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(UInt64_t4134040092_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_15 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_12, L_13, L_14, L_15, /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_16 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheC_28();
		if (L_16)
		{
			goto IL_0092;
		}
	}
	{
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__C_m3123314944_MethodInfo_var);
		ImporterFunc_t3630937194 * L_18 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_18, NULL, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheC_28(L_18);
	}

IL_0092:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_19 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheC_28();
		V_0 = L_19;
		Il2CppObject* L_20 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int32_t2950945753_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_22 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(SByte_t1669577662_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_23 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_20, L_21, L_22, L_23, /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_24 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheD_29();
		if (L_24)
		{
			goto IL_00cf;
		}
	}
	{
		IntPtr_t L_25;
		L_25.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__D_m3123024917_MethodInfo_var);
		ImporterFunc_t3630937194 * L_26 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_26, NULL, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheD_29(L_26);
	}

IL_00cf:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_27 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheD_29();
		V_0 = L_27;
		Il2CppObject* L_28 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_29 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int32_t2950945753_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_30 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int16_t2552820387_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_31 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_28, L_29, L_30, L_31, /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_32 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheE_30();
		if (L_32)
		{
			goto IL_010c;
		}
	}
	{
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__E_m3123374154_MethodInfo_var);
		ImporterFunc_t3630937194 * L_34 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_34, NULL, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheE_30(L_34);
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_35 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheE_30();
		V_0 = L_35;
		Il2CppObject* L_36 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_37 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int32_t2950945753_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_38 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(UInt16_t2177724958_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_39 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_36, L_37, L_38, L_39, /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_40 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheF_31();
		if (L_40)
		{
			goto IL_0149;
		}
	}
	{
		IntPtr_t L_41;
		L_41.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__F_m3123338535_MethodInfo_var);
		ImporterFunc_t3630937194 * L_42 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_42, NULL, L_41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheF_31(L_42);
	}

IL_0149:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_43 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheF_31();
		V_0 = L_43;
		Il2CppObject* L_44 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int32_t2950945753_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_46 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(UInt32_t2560061978_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_47 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_44, L_45, L_46, L_47, /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_48 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache10_32();
		if (L_48)
		{
			goto IL_0186;
		}
	}
	{
		IntPtr_t L_49;
		L_49.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__10_m2509882514_MethodInfo_var);
		ImporterFunc_t3630937194 * L_50 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_50, NULL, L_49, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache10_32(L_50);
	}

IL_0186:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_51 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache10_32();
		V_0 = L_51;
		Il2CppObject* L_52 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_53 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int32_t2950945753_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_54 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Single_t1397266774_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_55 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_52, L_53, L_54, L_55, /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_56 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache11_33();
		if (L_56)
		{
			goto IL_01c3;
		}
	}
	{
		IntPtr_t L_57;
		L_57.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__11_m613999264_MethodInfo_var);
		ImporterFunc_t3630937194 * L_58 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_58, NULL, L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache11_33(L_58);
	}

IL_01c3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_59 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache11_33();
		V_0 = L_59;
		Il2CppObject* L_60 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_61 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int32_t2950945753_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_62 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Double_t594665363_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_63 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_60, L_61, L_62, L_63, /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_64 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache12_34();
		if (L_64)
		{
			goto IL_0200;
		}
	}
	{
		IntPtr_t L_65;
		L_65.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__12_m1159193330_MethodInfo_var);
		ImporterFunc_t3630937194 * L_66 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_66, NULL, L_65, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache12_34(L_66);
	}

IL_0200:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_67 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache12_34();
		V_0 = L_67;
		Il2CppObject* L_68 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_69 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Double_t594665363_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_70 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Decimal_t2948259380_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_71 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_68, L_69, L_70, L_71, /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_72 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache13_35();
		if (L_72)
		{
			goto IL_023d;
		}
	}
	{
		IntPtr_t L_73;
		L_73.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__13_m1427563268_MethodInfo_var);
		ImporterFunc_t3630937194 * L_74 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_74, NULL, L_73, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache13_35(L_74);
	}

IL_023d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_75 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache13_35();
		V_0 = L_75;
		Il2CppObject* L_76 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_77 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int64_t3736567304_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_78 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(UInt32_t2560061978_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_79 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_76, L_77, L_78, L_79, /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_80 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache14_36();
		if (L_80)
		{
			goto IL_027a;
		}
	}
	{
		IntPtr_t L_81;
		L_81.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__14_m1419109062_MethodInfo_var);
		ImporterFunc_t3630937194 * L_82 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_82, NULL, L_81, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache14_36(L_82);
	}

IL_027a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_83 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache14_36();
		V_0 = L_83;
		Il2CppObject* L_84 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_85 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_86 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Char_t3634460470_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_87 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_84, L_85, L_86, L_87, /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_88 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache15_37();
		if (L_88)
		{
			goto IL_02b7;
		}
	}
	{
		IntPtr_t L_89;
		L_89.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__15_m1703207628_MethodInfo_var);
		ImporterFunc_t3630937194 * L_90 = (ImporterFunc_t3630937194 *)il2cpp_codegen_object_new(ImporterFunc_t3630937194_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m1884320379(L_90, NULL, L_89, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache15_37(L_90);
	}

IL_02b7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		ImporterFunc_t3630937194 * L_91 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache15_37();
		V_0 = L_91;
		Il2CppObject* L_92 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_93 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_94 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(DateTime_t3738529785_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t3630937194 * L_95 = V_0;
		JsonMapper_RegisterImporter_m3334975862(NULL /*static, unused*/, L_92, L_93, L_94, L_95, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::RegisterImporter(System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>,System.Type,System.Type,LitJson.ImporterFunc)
extern "C"  void JsonMapper_RegisterImporter_m3334975862 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___table0, Type_t * ___json_type1, Type_t * ___value_type2, ImporterFunc_t3630937194 * ___importer3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_RegisterImporter_m3334975862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = ___table0;
		Type_t * L_1 = ___json_type1;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::ContainsKey(!0) */, IDictionary_2_t1152366808_il2cpp_TypeInfo_var, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_3 = ___table0;
		Type_t * L_4 = ___json_type1;
		Dictionary_2_t1780316962 * L_5 = (Dictionary_2_t1780316962 *)il2cpp_codegen_object_new(Dictionary_2_t1780316962_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1305840448(L_5, /*hidden argument*/Dictionary_2__ctor_m1305840448_MethodInfo_var);
		NullCheck(L_3);
		InterfaceActionInvoker2< Type_t *, Il2CppObject* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::Add(!0,!1) */, IDictionary_2_t1152366808_il2cpp_TypeInfo_var, L_3, L_4, L_5);
	}

IL_0018:
	{
		Il2CppObject* L_6 = ___table0;
		Type_t * L_7 = ___json_type1;
		NullCheck(L_6);
		Il2CppObject* L_8 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::get_Item(!0) */, IDictionary_2_t1152366808_il2cpp_TypeInfo_var, L_6, L_7);
		Type_t * L_9 = ___value_type2;
		ImporterFunc_t3630937194 * L_10 = ___importer3;
		NullCheck(L_8);
		InterfaceActionInvoker2< Type_t *, ImporterFunc_t3630937194 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>::set_Item(!0,!1) */, IDictionary_2_t244168353_il2cpp_TypeInfo_var, L_8, L_9, L_10);
		return;
	}
}
// LitJson.IJsonWrapper LitJson.JsonMapper::ToWrapper(LitJson.WrapperFactory,LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ToWrapper_m1467426324 (Il2CppObject * __this /* static, unused */, WrapperFactory_t2158548929 * ___factory0, JsonReader_t836887441 * ___reader1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ToWrapper_m1467426324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WrapperFactory_t2158548929 * L_0 = ___factory0;
		JsonReader_t836887441 * L_1 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = JsonMapper_ReadValue_m3060360668(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// LitJson.IJsonWrapper LitJson.JsonMapper::<ReadSkip>m__0()
extern "C"  Il2CppObject * JsonMapper_U3CReadSkipU3Em__0_m139886084 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CReadSkipU3Em__0_m139886084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonMockWrapper_t82875095 * L_0 = (JsonMockWrapper_t82875095 *)il2cpp_codegen_object_new(JsonMockWrapper_t82875095_il2cpp_TypeInfo_var);
		JsonMockWrapper__ctor_m969609950(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__1(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__1_m3570407405 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__1_m3570407405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3570089748 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m2505564049(NULL /*static, unused*/, ((*(uint8_t*)((uint8_t*)UnBox(L_1, Byte_t1134296376_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m2431965710(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__2(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__2_m600765210 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__2_m600765210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3570089748 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m1553911740(NULL /*static, unused*/, ((*(Il2CppChar*)((Il2CppChar*)UnBox(L_1, Char_t3634460470_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m3439490280(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__3(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__3_m939732965 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__3_m939732965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3570089748 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_datetime_format_1();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_3 = Convert_ToString_m41256750(NULL /*static, unused*/, ((*(DateTime_t3738529785 *)((DateTime_t3738529785 *)UnBox(L_1, DateTime_t3738529785_il2cpp_TypeInfo_var)))), L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m3439490280(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__4(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__4_m266458088 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__4_m266458088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3570089748 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_0);
		JsonWriter_Write_m934090602(L_0, ((*(Decimal_t2948259380 *)((Decimal_t2948259380 *)UnBox(L_1, Decimal_t2948259380_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__5(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__5_m3328515821 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__5_m3328515821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3570089748 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m1405693041(NULL /*static, unused*/, ((*(int8_t*)((int8_t*)UnBox(L_1, SByte_t1669577662_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m2431965710(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__6(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__6_m312889811 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__6_m312889811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3570089748 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m1085744762(NULL /*static, unused*/, ((*(int16_t*)((int16_t*)UnBox(L_1, Int16_t2552820387_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m2431965710(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__7(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__7_m3877741659 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__7_m3877741659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3570089748 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m1987758323(NULL /*static, unused*/, ((*(uint16_t*)((uint16_t*)UnBox(L_1, UInt16_t2177724958_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m2431965710(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__8(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__8_m225163408 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__8_m225163408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3570089748 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		uint64_t L_2 = Convert_ToUInt64_m1745056470(NULL /*static, unused*/, ((*(uint32_t*)((uint32_t*)UnBox(L_1, UInt32_t2560061978_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m2044706063(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__9(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__9_m1344668283 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t3570089748 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__9_m1344668283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_t3570089748 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_0);
		JsonWriter_Write_m2044706063(L_0, ((*(uint64_t*)((uint64_t*)UnBox(L_1, UInt64_t4134040092_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__A(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__A_m3123255486 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__A_m3123255486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		uint8_t L_1 = Convert_ToByte_m1734770211(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox(L_0, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Byte_t1134296376_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__B(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__B_m3123219883 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__B_m3123219883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		uint64_t L_1 = Convert_ToUInt64_m786726853(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox(L_0, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt64_t4134040092_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__C(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__C_m3123314944 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__C_m3123314944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		int8_t L_1 = Convert_ToSByte_m2653418303(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox(L_0, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		int8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(SByte_t1669577662_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__D(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__D_m3123024917 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__D_m3123024917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		int16_t L_1 = Convert_ToInt16_m4174308322(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox(L_0, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		int16_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int16_t2552820387_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__E(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__E_m3123374154 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__E_m3123374154_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		uint16_t L_1 = Convert_ToUInt16_m3515425647(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox(L_0, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint16_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt16_t2177724958_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__F(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__F_m3123338535 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__F_m3123338535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		uint32_t L_1 = Convert_ToUInt32_m2215525276(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox(L_0, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt32_t2560061978_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__10(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__10_m2509882514 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__10_m2509882514_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		float L_1 = Convert_ToSingle_m3635698920(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox(L_0, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__11(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__11_m613999264 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__11_m613999264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		double L_1 = Convert_ToDouble_m2924063577(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox(L_0, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		double L_2 = L_1;
		Il2CppObject * L_3 = Box(Double_t594665363_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__12(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__12_m1159193330 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__12_m1159193330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		Decimal_t2948259380  L_1 = Convert_ToDecimal_m841368097(NULL /*static, unused*/, ((*(double*)((double*)UnBox(L_0, Double_t594665363_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		Decimal_t2948259380  L_2 = L_1;
		Il2CppObject * L_3 = Box(Decimal_t2948259380_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__13(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__13_m1427563268 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__13_m1427563268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		uint32_t L_1 = Convert_ToUInt32_m2194577773(NULL /*static, unused*/, ((*(int64_t*)((int64_t*)UnBox(L_0, Int64_t3736567304_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt32_t2560061978_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__14(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__14_m1419109062 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__14_m1419109062_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		Il2CppChar L_1 = Convert_ToChar_m85718752(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Il2CppChar L_2 = L_1;
		Il2CppObject * L_3 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__15(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__15_m1703207628 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__15_m1703207628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((JsonMapper_t3815285241_StaticFields*)JsonMapper_t3815285241_il2cpp_TypeInfo_var->static_fields)->get_datetime_format_1();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_2 = Convert_ToDateTime_m3802186295(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), L_1, /*hidden argument*/NULL);
		DateTime_t3738529785  L_3 = L_2;
		Il2CppObject * L_4 = Box(DateTime_t3738529785_il2cpp_TypeInfo_var, &L_3);
		return L_4;
	}
}
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.get_IsFixedSize()
extern "C"  bool JsonMockWrapper_System_Collections_IList_get_IsFixedSize_m2828148728 (JsonMockWrapper_t82875095 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.get_IsReadOnly()
extern "C"  bool JsonMockWrapper_System_Collections_IList_get_IsReadOnly_m3379768422 (JsonMockWrapper_t82875095 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Object LitJson.JsonMockWrapper::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IList_get_Item_m3083138291 (JsonMockWrapper_t82875095 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_set_Item_m1713899299 (JsonMockWrapper_t82875095 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 LitJson.JsonMockWrapper::System.Collections.ICollection.get_Count()
extern "C"  int32_t JsonMockWrapper_System_Collections_ICollection_get_Count_m3336301494 (JsonMockWrapper_t82875095 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean LitJson.JsonMockWrapper::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool JsonMockWrapper_System_Collections_ICollection_get_IsSynchronized_m3103134983 (JsonMockWrapper_t82875095 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object LitJson.JsonMockWrapper::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_ICollection_get_SyncRoot_m2800848447 (JsonMockWrapper_t82875095 * __this, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Object LitJson.JsonMockWrapper::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IDictionary_get_Item_m3015986061 (JsonMockWrapper_t82875095 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_set_Item_m1105580651 (JsonMockWrapper_t82875095 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::.ctor()
extern "C"  void JsonMockWrapper__ctor_m969609950 (JsonMockWrapper_t82875095 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::SetBoolean(System.Boolean)
extern "C"  void JsonMockWrapper_SetBoolean_m3123959673 (JsonMockWrapper_t82875095 * __this, bool ___val0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::SetDouble(System.Double)
extern "C"  void JsonMockWrapper_SetDouble_m3162130743 (JsonMockWrapper_t82875095 * __this, double ___val0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::SetInt(System.Int32)
extern "C"  void JsonMockWrapper_SetInt_m3114687387 (JsonMockWrapper_t82875095 * __this, int32_t ___val0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::SetJsonType(LitJson.JsonType)
extern "C"  void JsonMockWrapper_SetJsonType_m2186279011 (JsonMockWrapper_t82875095 * __this, int32_t ___type0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::SetLong(System.Int64)
extern "C"  void JsonMockWrapper_SetLong_m1165256783 (JsonMockWrapper_t82875095 * __this, int64_t ___val0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::SetString(System.String)
extern "C"  void JsonMockWrapper_SetString_m2405247157 (JsonMockWrapper_t82875095 * __this, String_t* ___val0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 LitJson.JsonMockWrapper::System.Collections.IList.Add(System.Object)
extern "C"  int32_t JsonMockWrapper_System_Collections_IList_Add_m1963355617 (JsonMockWrapper_t82875095 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Clear()
extern "C"  void JsonMockWrapper_System_Collections_IList_Clear_m3302467094 (JsonMockWrapper_t82875095 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.Contains(System.Object)
extern "C"  bool JsonMockWrapper_System_Collections_IList_Contains_m3059650441 (JsonMockWrapper_t82875095 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int32 LitJson.JsonMockWrapper::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t JsonMockWrapper_System_Collections_IList_IndexOf_m1437702743 (JsonMockWrapper_t82875095 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return (-1);
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_Insert_m2229969382 (JsonMockWrapper_t82875095 * __this, int32_t ___i0, Il2CppObject * ___v1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Remove(System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_Remove_m405129660 (JsonMockWrapper_t82875095 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void JsonMockWrapper_System_Collections_IList_RemoveAt_m4126689523 (JsonMockWrapper_t82875095 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void JsonMockWrapper_System_Collections_ICollection_CopyTo_m3218328807 (JsonMockWrapper_t82875095 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.IEnumerator LitJson.JsonMockWrapper::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IEnumerable_GetEnumerator_m1999468993 (JsonMockWrapper_t82875095 * __this, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_Add_m2687507294 (JsonMockWrapper_t82875095 * __this, Il2CppObject * ___k0, Il2CppObject * ___v1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_Remove_m85542273 (JsonMockWrapper_t82875095 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.IDictionaryEnumerator LitJson.JsonMockWrapper::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IDictionary_GetEnumerator_m909965981 (JsonMockWrapper_t82875095 * __this, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Collections.IDictionaryEnumerator LitJson.JsonMockWrapper::System.Collections.Specialized.IOrderedDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m800645499 (JsonMockWrapper_t82875095 * __this, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Boolean LitJson.JsonReader::get_SkipNonMembers()
extern "C"  bool JsonReader_get_SkipNonMembers_m3135707401 (JsonReader_t836887441 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_skip_non_members_12();
		return L_0;
	}
}
// LitJson.JsonToken LitJson.JsonReader::get_Token()
extern "C"  int32_t JsonReader_get_Token_m1333614796 (JsonReader_t836887441 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_token_14();
		return L_0;
	}
}
// System.Object LitJson.JsonReader::get_Value()
extern "C"  Il2CppObject * JsonReader_get_Value_m3034661202 (JsonReader_t836887441 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_token_value_13();
		return L_0;
	}
}
// System.Void LitJson.JsonReader::.cctor()
extern "C"  void JsonReader__cctor_m1456809606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__cctor_m1456809606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonReader_PopulateParseTable_m350999017(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonReader::.ctor(System.String)
extern "C"  void JsonReader__ctor_m333115852 (JsonReader_t836887441 * __this, String_t* ___json_text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__ctor_m333115852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___json_text0;
		StringReader_t3465604688 * L_1 = (StringReader_t3465604688 *)il2cpp_codegen_object_new(StringReader_t3465604688_il2cpp_TypeInfo_var);
		StringReader__ctor_m126993932(L_1, L_0, /*hidden argument*/NULL);
		JsonReader__ctor_m601545185(__this, L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonReader::.ctor(System.IO.TextReader,System.Boolean)
extern "C"  void JsonReader__ctor_m601545185 (JsonReader_t836887441 * __this, TextReader_t283511965 * ___reader0, bool ___owned1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__ctor_m601545185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		TextReader_t283511965 * L_0 = ___reader0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3529812268, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		__this->set_parser_in_string_7((bool)0);
		__this->set_parser_return_8((bool)0);
		__this->set_read_started_9((bool)0);
		Stack_1_t3794335208 * L_2 = (Stack_1_t3794335208 *)il2cpp_codegen_object_new(Stack_1_t3794335208_il2cpp_TypeInfo_var);
		Stack_1__ctor_m1742569847(L_2, /*hidden argument*/Stack_1__ctor_m1742569847_MethodInfo_var);
		__this->set_automaton_stack_1(L_2);
		Stack_1_t3794335208 * L_3 = __this->get_automaton_stack_1();
		NullCheck(L_3);
		Stack_1_Push_m399625717(L_3, ((int32_t)65553), /*hidden argument*/Stack_1_Push_m399625717_MethodInfo_var);
		Stack_1_t3794335208 * L_4 = __this->get_automaton_stack_1();
		NullCheck(L_4);
		Stack_1_Push_m399625717(L_4, ((int32_t)65543), /*hidden argument*/Stack_1_Push_m399625717_MethodInfo_var);
		TextReader_t283511965 * L_5 = ___reader0;
		Lexer_t1514038666 * L_6 = (Lexer_t1514038666 *)il2cpp_codegen_object_new(Lexer_t1514038666_il2cpp_TypeInfo_var);
		Lexer__ctor_m465008812(L_6, L_5, /*hidden argument*/NULL);
		__this->set_lexer_6(L_6);
		__this->set_end_of_input_5((bool)0);
		__this->set_end_of_json_4((bool)0);
		__this->set_skip_non_members_12((bool)1);
		TextReader_t283511965 * L_7 = ___reader0;
		__this->set_reader_10(L_7);
		bool L_8 = ___owned1;
		__this->set_reader_is_owned_11(L_8);
		return;
	}
}
// System.Void LitJson.JsonReader::PopulateParseTable()
extern "C"  void JsonReader_PopulateParseTable_m350999017 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_PopulateParseTable_m350999017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t921491721 * L_0 = (Dictionary_2_t921491721 *)il2cpp_codegen_object_new(Dictionary_2_t921491721_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2631748502(L_0, /*hidden argument*/Dictionary_2__ctor_m2631748502_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t836887441_il2cpp_TypeInfo_var);
		((JsonReader_t836887441_StaticFields*)JsonReader_t836887441_il2cpp_TypeInfo_var->static_fields)->set_parse_table_0(L_0);
		JsonReader_TableAddRow_m1968124223(NULL /*static, unused*/, ((int32_t)65548), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_1 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)91));
		Int32U5BU5D_t385246372* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)((int32_t)65549));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65548), ((int32_t)91), L_2, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m1968124223(NULL /*static, unused*/, ((int32_t)65549), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_3 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D0_0_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)34), L_3, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_4 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D1_1_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)91), L_4, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_5 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)93));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)93), L_5, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_6 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D2_2_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)123), L_6, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_7 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_7, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D3_3_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)65537), L_7, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_8 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_8, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D4_4_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)65538), L_8, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_9 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_9, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D5_5_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)65539), L_9, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_10 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_10, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D6_6_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)65540), L_10, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m1968124223(NULL /*static, unused*/, ((int32_t)65544), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_11 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)123));
		Int32U5BU5D_t385246372* L_12 = L_11;
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)((int32_t)65545));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65544), ((int32_t)123), L_12, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m1968124223(NULL /*static, unused*/, ((int32_t)65545), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_13 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_13, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D7_7_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65545), ((int32_t)34), L_13, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_14 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)125));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65545), ((int32_t)125), L_14, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m1968124223(NULL /*static, unused*/, ((int32_t)65546), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_15 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D8_8_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65546), ((int32_t)34), L_15, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m1968124223(NULL /*static, unused*/, ((int32_t)65547), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_16 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_16, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2D9_9_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65547), ((int32_t)44), L_16, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_17 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65554));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65547), ((int32_t)125), L_17, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m1968124223(NULL /*static, unused*/, ((int32_t)65552), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_18 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_18, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2DA_10_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65552), ((int32_t)34), L_18, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m1968124223(NULL /*static, unused*/, ((int32_t)65543), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_19 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65548));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65543), ((int32_t)91), L_19, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_20 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65544));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65543), ((int32_t)123), L_20, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m1968124223(NULL /*static, unused*/, ((int32_t)65550), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_21 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65552));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)34), L_21, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_22 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65548));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)91), L_22, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_23 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65544));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)123), L_23, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_24 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65537));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)65537), L_24, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_25 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65538));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)65538), L_25, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_26 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_26);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65539));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)65539), L_26, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_27 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65540));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)65540), L_27, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m1968124223(NULL /*static, unused*/, ((int32_t)65551), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_28 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_28, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2DB_11_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65551), ((int32_t)44), L_28, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_29 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_29);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65554));
		JsonReader_TableAddCol_m3551170424(NULL /*static, unused*/, ((int32_t)65551), ((int32_t)93), L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonReader::TableAddCol(LitJson.ParserToken,System.Int32,System.Int32[])
extern "C"  void JsonReader_TableAddCol_m3551170424 (Il2CppObject * __this /* static, unused */, int32_t ___row0, int32_t ___col1, Int32U5BU5D_t385246372* ___symbols2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_TableAddCol_m3551170424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t836887441_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonReader_t836887441_StaticFields*)JsonReader_t836887441_il2cpp_TypeInfo_var->static_fields)->get_parse_table_0();
		int32_t L_1 = ___row0;
		NullCheck(L_0);
		Il2CppObject* L_2 = InterfaceFuncInvoker1< Il2CppObject*, int32_t >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>::get_Item(!0) */, IDictionary_2_t3680310408_il2cpp_TypeInfo_var, L_0, L_1);
		int32_t L_3 = ___col1;
		Int32U5BU5D_t385246372* L_4 = ___symbols2;
		NullCheck(L_2);
		InterfaceActionInvoker2< int32_t, Int32U5BU5D_t385246372* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>::Add(!0,!1) */, IDictionary_2_t2032778390_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		return;
	}
}
// System.Void LitJson.JsonReader::TableAddRow(LitJson.ParserToken)
extern "C"  void JsonReader_TableAddRow_m1968124223 (Il2CppObject * __this /* static, unused */, int32_t ___rule0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_TableAddRow_m1968124223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t836887441_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonReader_t836887441_StaticFields*)JsonReader_t836887441_il2cpp_TypeInfo_var->static_fields)->get_parse_table_0();
		int32_t L_1 = ___rule0;
		Dictionary_2_t3568926999 * L_2 = (Dictionary_2_t3568926999 *)il2cpp_codegen_object_new(Dictionary_2_t3568926999_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3739908635(L_2, /*hidden argument*/Dictionary_2__ctor_m3739908635_MethodInfo_var);
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>::Add(!0,!1) */, IDictionary_2_t3680310408_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void LitJson.JsonReader::ProcessNumber(System.String)
extern "C"  void JsonReader_ProcessNumber_m2606343002 (JsonReader_t836887441 * __this, String_t* ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ProcessNumber_m2606343002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	int32_t V_1 = 0;
	int64_t V_2 = 0;
	uint64_t V_3 = 0;
	{
		String_t* L_0 = ___number0;
		NullCheck(L_0);
		int32_t L_1 = String_IndexOf_m363431711(L_0, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_2 = ___number0;
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m363431711(L_2, ((int32_t)101), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_4 = ___number0;
		NullCheck(L_4);
		int32_t L_5 = String_IndexOf_m363431711(L_4, ((int32_t)69), /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)(-1))))
		{
			goto IL_004b;
		}
	}

IL_002a:
	{
		String_t* L_6 = ___number0;
		bool L_7 = Double_TryParse_m3021978240(NULL /*static, unused*/, L_6, (&V_0), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		__this->set_token_14(8);
		double L_8 = V_0;
		double L_9 = L_8;
		Il2CppObject * L_10 = Box(Double_t594665363_il2cpp_TypeInfo_var, &L_9);
		__this->set_token_value_13(L_10);
		return;
	}

IL_004b:
	{
		String_t* L_11 = ___number0;
		bool L_12 = Int32_TryParse_m2404707562(NULL /*static, unused*/, L_11, (&V_1), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006c;
		}
	}
	{
		__this->set_token_14(6);
		int32_t L_13 = V_1;
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_14);
		__this->set_token_value_13(L_15);
		return;
	}

IL_006c:
	{
		String_t* L_16 = ___number0;
		bool L_17 = Int64_TryParse_m2208578514(NULL /*static, unused*/, L_16, (&V_2), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008d;
		}
	}
	{
		__this->set_token_14(7);
		int64_t L_18 = V_2;
		int64_t L_19 = L_18;
		Il2CppObject * L_20 = Box(Int64_t3736567304_il2cpp_TypeInfo_var, &L_19);
		__this->set_token_value_13(L_20);
		return;
	}

IL_008d:
	{
		String_t* L_21 = ___number0;
		bool L_22 = UInt64_TryParse_m2263420204(NULL /*static, unused*/, L_21, (&V_3), /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00ae;
		}
	}
	{
		__this->set_token_14(7);
		uint64_t L_23 = V_3;
		uint64_t L_24 = L_23;
		Il2CppObject * L_25 = Box(UInt64_t4134040092_il2cpp_TypeInfo_var, &L_24);
		__this->set_token_value_13(L_25);
		return;
	}

IL_00ae:
	{
		__this->set_token_14(6);
		int32_t L_26 = 0;
		Il2CppObject * L_27 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_26);
		__this->set_token_value_13(L_27);
		return;
	}
}
// System.Void LitJson.JsonReader::ProcessSymbol()
extern "C"  void JsonReader_ProcessSymbol_m137365372 (JsonReader_t836887441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ProcessSymbol_m137365372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)91)))))
		{
			goto IL_0020;
		}
	}
	{
		__this->set_token_14(4);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0020:
	{
		int32_t L_1 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)93)))))
		{
			goto IL_0040;
		}
	}
	{
		__this->set_token_14(5);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0040:
	{
		int32_t L_2 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)123)))))
		{
			goto IL_0060;
		}
	}
	{
		__this->set_token_14(1);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0060:
	{
		int32_t L_3 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_0080;
		}
	}
	{
		__this->set_token_14(3);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0080:
	{
		int32_t L_4 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_00ca;
		}
	}
	{
		bool L_5 = __this->get_parser_in_string_7();
		if (!L_5)
		{
			goto IL_00ab;
		}
	}
	{
		__this->set_parser_in_string_7((bool)0);
		__this->set_parser_return_8((bool)1);
		goto IL_00c5;
	}

IL_00ab:
	{
		int32_t L_6 = __this->get_token_14();
		if (L_6)
		{
			goto IL_00be;
		}
	}
	{
		__this->set_token_14(((int32_t)9));
	}

IL_00be:
	{
		__this->set_parser_in_string_7((bool)1);
	}

IL_00c5:
	{
		goto IL_01b8;
	}

IL_00ca:
	{
		int32_t L_7 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)65541)))))
		{
			goto IL_00f0;
		}
	}
	{
		Lexer_t1514038666 * L_8 = __this->get_lexer_6();
		NullCheck(L_8);
		String_t* L_9 = Lexer_get_StringValue_m673320728(L_8, /*hidden argument*/NULL);
		__this->set_token_value_13(L_9);
		goto IL_01b8;
	}

IL_00f0:
	{
		int32_t L_10 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)65539)))))
		{
			goto IL_0120;
		}
	}
	{
		__this->set_token_14(((int32_t)10));
		bool L_11 = ((bool)0);
		Il2CppObject * L_12 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_11);
		__this->set_token_value_13(L_12);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0120:
	{
		int32_t L_13 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)65540)))))
		{
			goto IL_0144;
		}
	}
	{
		__this->set_token_14(((int32_t)11));
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0144:
	{
		int32_t L_14 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)65537)))))
		{
			goto IL_0171;
		}
	}
	{
		Lexer_t1514038666 * L_15 = __this->get_lexer_6();
		NullCheck(L_15);
		String_t* L_16 = Lexer_get_StringValue_m673320728(L_15, /*hidden argument*/NULL);
		JsonReader_ProcessNumber_m2606343002(__this, L_16, /*hidden argument*/NULL);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0171:
	{
		int32_t L_17 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)65546)))))
		{
			goto IL_018d;
		}
	}
	{
		__this->set_token_14(2);
		goto IL_01b8;
	}

IL_018d:
	{
		int32_t L_18 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)65538)))))
		{
			goto IL_01b8;
		}
	}
	{
		__this->set_token_14(((int32_t)10));
		bool L_19 = ((bool)1);
		Il2CppObject * L_20 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_19);
		__this->set_token_value_13(L_20);
		__this->set_parser_return_8((bool)1);
	}

IL_01b8:
	{
		return;
	}
}
// System.Boolean LitJson.JsonReader::ReadToken()
extern "C"  bool JsonReader_ReadToken_m67845004 (JsonReader_t836887441 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_end_of_input_5();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Lexer_t1514038666 * L_1 = __this->get_lexer_6();
		NullCheck(L_1);
		Lexer_NextToken_m2425003787(L_1, /*hidden argument*/NULL);
		Lexer_t1514038666 * L_2 = __this->get_lexer_6();
		NullCheck(L_2);
		bool L_3 = Lexer_get_EndOfInput_m1249127404(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		JsonReader_Close_m364037064(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0031:
	{
		Lexer_t1514038666 * L_4 = __this->get_lexer_6();
		NullCheck(L_4);
		int32_t L_5 = Lexer_get_Token_m1219310079(L_4, /*hidden argument*/NULL);
		__this->set_current_input_2(L_5);
		return (bool)1;
	}
}
// System.Void LitJson.JsonReader::Close()
extern "C"  void JsonReader_Close_m364037064 (JsonReader_t836887441 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_end_of_input_5();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_end_of_input_5((bool)1);
		__this->set_end_of_json_4((bool)1);
		bool L_1 = __this->get_reader_is_owned_11();
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		TextReader_t283511965 * L_2 = __this->get_reader_10();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(5 /* System.Void System.IO.TextReader::Close() */, L_2);
	}

IL_0030:
	{
		__this->set_reader_10((TextReader_t283511965 *)NULL);
		return;
	}
}
// System.Boolean LitJson.JsonReader::Read()
extern "C"  bool JsonReader_Read_m1579829852 (JsonReader_t836887441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_Read_m1579829852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t385246372* V_0 = NULL;
	KeyNotFoundException_t2292407383 * V_1 = NULL;
	int32_t V_2 = 0;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_end_of_input_5();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		bool L_1 = __this->get_end_of_json_4();
		if (!L_1)
		{
			goto IL_004a;
		}
	}
	{
		__this->set_end_of_json_4((bool)0);
		Stack_1_t3794335208 * L_2 = __this->get_automaton_stack_1();
		NullCheck(L_2);
		Stack_1_Clear_m1908282093(L_2, /*hidden argument*/Stack_1_Clear_m1908282093_MethodInfo_var);
		Stack_1_t3794335208 * L_3 = __this->get_automaton_stack_1();
		NullCheck(L_3);
		Stack_1_Push_m399625717(L_3, ((int32_t)65553), /*hidden argument*/Stack_1_Push_m399625717_MethodInfo_var);
		Stack_1_t3794335208 * L_4 = __this->get_automaton_stack_1();
		NullCheck(L_4);
		Stack_1_Push_m399625717(L_4, ((int32_t)65543), /*hidden argument*/Stack_1_Push_m399625717_MethodInfo_var);
	}

IL_004a:
	{
		__this->set_parser_in_string_7((bool)0);
		__this->set_parser_return_8((bool)0);
		__this->set_token_14(0);
		__this->set_token_value_13(NULL);
		bool L_5 = __this->get_read_started_9();
		if (L_5)
		{
			goto IL_0085;
		}
	}
	{
		__this->set_read_started_9((bool)1);
		bool L_6 = JsonReader_ReadToken_m67845004(__this, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0085;
		}
	}
	{
		return (bool)0;
	}

IL_0085:
	{
		bool L_7 = __this->get_parser_return_8();
		if (!L_7)
		{
			goto IL_00ae;
		}
	}
	{
		Stack_1_t3794335208 * L_8 = __this->get_automaton_stack_1();
		NullCheck(L_8);
		int32_t L_9 = Stack_1_Peek_m10351423(L_8, /*hidden argument*/Stack_1_Peek_m10351423_MethodInfo_var);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)65553)))))
		{
			goto IL_00ac;
		}
	}
	{
		__this->set_end_of_json_4((bool)1);
	}

IL_00ac:
	{
		return (bool)1;
	}

IL_00ae:
	{
		Stack_1_t3794335208 * L_10 = __this->get_automaton_stack_1();
		NullCheck(L_10);
		int32_t L_11 = Stack_1_Pop_m681651409(L_10, /*hidden argument*/Stack_1_Pop_m681651409_MethodInfo_var);
		__this->set_current_symbol_3(L_11);
		JsonReader_ProcessSymbol_m137365372(__this, /*hidden argument*/NULL);
		int32_t L_12 = __this->get_current_symbol_3();
		int32_t L_13 = __this->get_current_input_2();
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0115;
		}
	}
	{
		bool L_14 = JsonReader_ReadToken_m67845004(__this, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0110;
		}
	}
	{
		Stack_1_t3794335208 * L_15 = __this->get_automaton_stack_1();
		NullCheck(L_15);
		int32_t L_16 = Stack_1_Peek_m10351423(L_15, /*hidden argument*/Stack_1_Peek_m10351423_MethodInfo_var);
		if ((((int32_t)L_16) == ((int32_t)((int32_t)65553))))
		{
			goto IL_0101;
		}
	}
	{
		JsonException_t3682484112 * L_17 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2214533437(L_17, _stringLiteral320911726, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0101:
	{
		bool L_18 = __this->get_parser_return_8();
		if (!L_18)
		{
			goto IL_010e;
		}
	}
	{
		return (bool)1;
	}

IL_010e:
	{
		return (bool)0;
	}

IL_0110:
	{
		goto IL_0085;
	}

IL_0115:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t836887441_il2cpp_TypeInfo_var);
		Il2CppObject* L_19 = ((JsonReader_t836887441_StaticFields*)JsonReader_t836887441_il2cpp_TypeInfo_var->static_fields)->get_parse_table_0();
		int32_t L_20 = __this->get_current_symbol_3();
		NullCheck(L_19);
		Il2CppObject* L_21 = InterfaceFuncInvoker1< Il2CppObject*, int32_t >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>::get_Item(!0) */, IDictionary_2_t3680310408_il2cpp_TypeInfo_var, L_19, L_20);
		int32_t L_22 = __this->get_current_input_2();
		NullCheck(L_21);
		Int32U5BU5D_t385246372* L_23 = InterfaceFuncInvoker1< Int32U5BU5D_t385246372*, int32_t >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>::get_Item(!0) */, IDictionary_2_t2032778390_il2cpp_TypeInfo_var, L_21, L_22);
		V_0 = L_23;
		goto IL_0144;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (KeyNotFoundException_t2292407383_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0136;
		throw e;
	}

CATCH_0136:
	{ // begin catch(System.Collections.Generic.KeyNotFoundException)
		V_1 = ((KeyNotFoundException_t2292407383 *)__exception_local);
		int32_t L_24 = __this->get_current_input_2();
		KeyNotFoundException_t2292407383 * L_25 = V_1;
		JsonException_t3682484112 * L_26 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m4037883734(L_26, L_24, L_25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	} // end catch (depth: 1)

IL_0144:
	{
		Int32U5BU5D_t385246372* L_27 = V_0;
		NullCheck(L_27);
		int32_t L_28 = 0;
		int32_t L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)65554)))))
		{
			goto IL_0156;
		}
	}
	{
		goto IL_0085;
	}

IL_0156:
	{
		Int32U5BU5D_t385246372* L_30 = V_0;
		NullCheck(L_30);
		V_2 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))-(int32_t)1));
		goto IL_0173;
	}

IL_0161:
	{
		Stack_1_t3794335208 * L_31 = __this->get_automaton_stack_1();
		Int32U5BU5D_t385246372* L_32 = V_0;
		int32_t L_33 = V_2;
		NullCheck(L_32);
		int32_t L_34 = L_33;
		int32_t L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		NullCheck(L_31);
		Stack_1_Push_m399625717(L_31, L_35, /*hidden argument*/Stack_1_Push_m399625717_MethodInfo_var);
		int32_t L_36 = V_2;
		V_2 = ((int32_t)((int32_t)L_36-(int32_t)1));
	}

IL_0173:
	{
		int32_t L_37 = V_2;
		if ((((int32_t)L_37) >= ((int32_t)0)))
		{
			goto IL_0161;
		}
	}
	{
		goto IL_0085;
	}
}
// System.Void LitJson.JsonWriter::.cctor()
extern "C"  void JsonWriter__cctor_m2709308669 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__cctor_m2709308669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t435877138_il2cpp_TypeInfo_var);
		NumberFormatInfo_t435877138 * L_0 = NumberFormatInfo_get_InvariantInfo_m349577018(NULL /*static, unused*/, /*hidden argument*/NULL);
		((JsonWriter_t3570089748_StaticFields*)JsonWriter_t3570089748_il2cpp_TypeInfo_var->static_fields)->set_number_format_0(L_0);
		return;
	}
}
// System.Void LitJson.JsonWriter::.ctor()
extern "C"  void JsonWriter__ctor_m3035373812 (JsonWriter_t3570089748 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__ctor_m3035373812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_0 = (StringBuilder_t1712802186 *)il2cpp_codegen_object_new(StringBuilder_t1712802186_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_0, /*hidden argument*/NULL);
		__this->set_inst_string_builder_7(L_0);
		StringBuilder_t1712802186 * L_1 = __this->get_inst_string_builder_7();
		StringWriter_t802263757 * L_2 = (StringWriter_t802263757 *)il2cpp_codegen_object_new(StringWriter_t802263757_il2cpp_TypeInfo_var);
		StringWriter__ctor_m1259274362(L_2, L_1, /*hidden argument*/NULL);
		__this->set_writer_10(L_2);
		JsonWriter_Init_m1392617240(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonWriter::DoValidation(LitJson.Condition)
extern "C"  void JsonWriter_DoValidation_m2027671802 (JsonWriter_t3570089748 * __this, int32_t ___cond0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_DoValidation_m2027671802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WriterContext_t1011093999 * L_0 = __this->get_context_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_ExpectingValue_3();
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		WriterContext_t1011093999 * L_2 = __this->get_context_1();
		WriterContext_t1011093999 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_Count_0();
		NullCheck(L_3);
		L_3->set_Count_0(((int32_t)((int32_t)L_4+(int32_t)1)));
	}

IL_0023:
	{
		bool L_5 = __this->get_validate_9();
		if (L_5)
		{
			goto IL_002f;
		}
	}
	{
		return;
	}

IL_002f:
	{
		bool L_6 = __this->get_has_reached_end_3();
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		JsonException_t3682484112 * L_7 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2214533437(L_7, _stringLiteral3717129058, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0045:
	{
		int32_t L_8 = ___cond0;
		switch (L_8)
		{
			case 0:
			{
				goto IL_0064;
			}
			case 1:
			{
				goto IL_0084;
			}
			case 2:
			{
				goto IL_00b4;
			}
			case 3:
			{
				goto IL_00e4;
			}
			case 4:
			{
				goto IL_0114;
			}
		}
	}
	{
		goto IL_0154;
	}

IL_0064:
	{
		WriterContext_t1011093999 * L_9 = __this->get_context_1();
		NullCheck(L_9);
		bool L_10 = L_9->get_InArray_1();
		if (L_10)
		{
			goto IL_007f;
		}
	}
	{
		JsonException_t3682484112 * L_11 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2214533437(L_11, _stringLiteral4103647921, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_007f:
	{
		goto IL_0154;
	}

IL_0084:
	{
		WriterContext_t1011093999 * L_12 = __this->get_context_1();
		NullCheck(L_12);
		bool L_13 = L_12->get_InObject_2();
		if (!L_13)
		{
			goto IL_00a4;
		}
	}
	{
		WriterContext_t1011093999 * L_14 = __this->get_context_1();
		NullCheck(L_14);
		bool L_15 = L_14->get_ExpectingValue_3();
		if (!L_15)
		{
			goto IL_00af;
		}
	}

IL_00a4:
	{
		JsonException_t3682484112 * L_16 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2214533437(L_16, _stringLiteral1740842079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_00af:
	{
		goto IL_0154;
	}

IL_00b4:
	{
		WriterContext_t1011093999 * L_17 = __this->get_context_1();
		NullCheck(L_17);
		bool L_18 = L_17->get_InObject_2();
		if (!L_18)
		{
			goto IL_00df;
		}
	}
	{
		WriterContext_t1011093999 * L_19 = __this->get_context_1();
		NullCheck(L_19);
		bool L_20 = L_19->get_ExpectingValue_3();
		if (L_20)
		{
			goto IL_00df;
		}
	}
	{
		JsonException_t3682484112 * L_21 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2214533437(L_21, _stringLiteral2856579791, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00df:
	{
		goto IL_0154;
	}

IL_00e4:
	{
		WriterContext_t1011093999 * L_22 = __this->get_context_1();
		NullCheck(L_22);
		bool L_23 = L_22->get_InObject_2();
		if (!L_23)
		{
			goto IL_0104;
		}
	}
	{
		WriterContext_t1011093999 * L_24 = __this->get_context_1();
		NullCheck(L_24);
		bool L_25 = L_24->get_ExpectingValue_3();
		if (!L_25)
		{
			goto IL_010f;
		}
	}

IL_0104:
	{
		JsonException_t3682484112 * L_26 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2214533437(L_26, _stringLiteral393113856, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_010f:
	{
		goto IL_0154;
	}

IL_0114:
	{
		WriterContext_t1011093999 * L_27 = __this->get_context_1();
		NullCheck(L_27);
		bool L_28 = L_27->get_InArray_1();
		if (L_28)
		{
			goto IL_014f;
		}
	}
	{
		WriterContext_t1011093999 * L_29 = __this->get_context_1();
		NullCheck(L_29);
		bool L_30 = L_29->get_InObject_2();
		if (!L_30)
		{
			goto IL_0144;
		}
	}
	{
		WriterContext_t1011093999 * L_31 = __this->get_context_1();
		NullCheck(L_31);
		bool L_32 = L_31->get_ExpectingValue_3();
		if (L_32)
		{
			goto IL_014f;
		}
	}

IL_0144:
	{
		JsonException_t3682484112 * L_33 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2214533437(L_33, _stringLiteral4135352181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_014f:
	{
		goto IL_0154;
	}

IL_0154:
	{
		return;
	}
}
// System.Void LitJson.JsonWriter::Init()
extern "C"  void JsonWriter_Init_m1392617240 (JsonWriter_t3570089748 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Init_m1392617240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_has_reached_end_3((bool)0);
		__this->set_hex_seq_4(((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_indentation_5(0);
		__this->set_indent_value_6(4);
		__this->set_pretty_print_8((bool)0);
		__this->set_validate_9((bool)1);
		Stack_1_t1854483454 * L_0 = (Stack_1_t1854483454 *)il2cpp_codegen_object_new(Stack_1_t1854483454_il2cpp_TypeInfo_var);
		Stack_1__ctor_m227092194(L_0, /*hidden argument*/Stack_1__ctor_m227092194_MethodInfo_var);
		__this->set_ctx_stack_2(L_0);
		WriterContext_t1011093999 * L_1 = (WriterContext_t1011093999 *)il2cpp_codegen_object_new(WriterContext_t1011093999_il2cpp_TypeInfo_var);
		WriterContext__ctor_m2317705773(L_1, /*hidden argument*/NULL);
		__this->set_context_1(L_1);
		Stack_1_t1854483454 * L_2 = __this->get_ctx_stack_2();
		WriterContext_t1011093999 * L_3 = __this->get_context_1();
		NullCheck(L_2);
		Stack_1_Push_m1814405731(L_2, L_3, /*hidden argument*/Stack_1_Push_m1814405731_MethodInfo_var);
		return;
	}
}
// System.Void LitJson.JsonWriter::IntToHex(System.Int32,System.Char[])
extern "C"  void JsonWriter_IntToHex_m1207120023 (Il2CppObject * __this /* static, unused */, int32_t ___n0, CharU5BU5D_t3528271667* ___hex1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_1 = 0;
		goto IL_0039;
	}

IL_0007:
	{
		int32_t L_0 = ___n0;
		V_0 = ((int32_t)((int32_t)L_0%(int32_t)((int32_t)16)));
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0023;
		}
	}
	{
		CharU5BU5D_t3528271667* L_2 = ___hex1;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)3-(int32_t)L_3))), (Il2CppChar)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)48)+(int32_t)L_4))))));
		goto IL_0030;
	}

IL_0023:
	{
		CharU5BU5D_t3528271667* L_5 = ___hex1;
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)3-(int32_t)L_6))), (Il2CppChar)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)65)+(int32_t)((int32_t)((int32_t)L_7-(int32_t)((int32_t)10)))))))));
	}

IL_0030:
	{
		int32_t L_8 = ___n0;
		___n0 = ((int32_t)((int32_t)L_8>>(int32_t)4));
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) < ((int32_t)4)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void LitJson.JsonWriter::Put(System.String)
extern "C"  void JsonWriter_Put_m2154223978 (JsonWriter_t3570089748 * __this, String_t* ___str0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_pretty_print_8();
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		WriterContext_t1011093999 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		bool L_2 = L_1->get_ExpectingValue_3();
		if (L_2)
		{
			goto IL_003f;
		}
	}
	{
		V_0 = 0;
		goto IL_0033;
	}

IL_0022:
	{
		TextWriter_t3478189236 * L_3 = __this->get_writer_10();
		NullCheck(L_3);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_3, ((int32_t)32));
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = __this->get_indentation_5();
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0022;
		}
	}

IL_003f:
	{
		TextWriter_t3478189236 * L_7 = __this->get_writer_10();
		String_t* L_8 = ___str0;
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_7, L_8);
		return;
	}
}
// System.Void LitJson.JsonWriter::PutNewline()
extern "C"  void JsonWriter_PutNewline_m2576790467 (JsonWriter_t3570089748 * __this, const MethodInfo* method)
{
	{
		JsonWriter_PutNewline_m1383209577(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonWriter::PutNewline(System.Boolean)
extern "C"  void JsonWriter_PutNewline_m1383209577 (JsonWriter_t3570089748 * __this, bool ___add_comma0, const MethodInfo* method)
{
	{
		bool L_0 = ___add_comma0;
		if (!L_0)
		{
			goto IL_0034;
		}
	}
	{
		WriterContext_t1011093999 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		bool L_2 = L_1->get_ExpectingValue_3();
		if (L_2)
		{
			goto IL_0034;
		}
	}
	{
		WriterContext_t1011093999 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_Count_0();
		if ((((int32_t)L_4) <= ((int32_t)1)))
		{
			goto IL_0034;
		}
	}
	{
		TextWriter_t3478189236 * L_5 = __this->get_writer_10();
		NullCheck(L_5);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_5, ((int32_t)44));
	}

IL_0034:
	{
		bool L_6 = __this->get_pretty_print_8();
		if (!L_6)
		{
			goto IL_005c;
		}
	}
	{
		WriterContext_t1011093999 * L_7 = __this->get_context_1();
		NullCheck(L_7);
		bool L_8 = L_7->get_ExpectingValue_3();
		if (L_8)
		{
			goto IL_005c;
		}
	}
	{
		TextWriter_t3478189236 * L_9 = __this->get_writer_10();
		NullCheck(L_9);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_9, ((int32_t)10));
	}

IL_005c:
	{
		return;
	}
}
// System.Void LitJson.JsonWriter::PutString(System.String)
extern "C"  void JsonWriter_PutString_m890241341 (JsonWriter_t3570089748 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_PutString_m890241341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		JsonWriter_Put_m2154223978(__this, L_0, /*hidden argument*/NULL);
		TextWriter_t3478189236 * L_1 = __this->get_writer_10();
		NullCheck(L_1);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_1, ((int32_t)34));
		String_t* L_2 = ___str0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m3847582255(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_015a;
	}

IL_0026:
	{
		String_t* L_4 = ___str0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Il2CppChar L_6 = String_get_Chars_m2986988803(L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Il2CppChar L_7 = V_2;
		switch (((int32_t)((int32_t)L_7-(int32_t)8)))
		{
			case 0:
			{
				goto IL_00db;
			}
			case 1:
			{
				goto IL_008d;
			}
			case 2:
			{
				goto IL_0063;
			}
			case 3:
			{
				goto IL_004e;
			}
			case 4:
			{
				goto IL_00c6;
			}
			case 5:
			{
				goto IL_0078;
			}
		}
	}

IL_004e:
	{
		Il2CppChar L_8 = V_2;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)34))))
		{
			goto IL_00a2;
		}
	}
	{
		Il2CppChar L_9 = V_2;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)92))))
		{
			goto IL_00a2;
		}
	}
	{
		goto IL_00f0;
	}

IL_0063:
	{
		TextWriter_t3478189236 * L_10 = __this->get_writer_10();
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_10, _stringLiteral3454842868);
		goto IL_0156;
	}

IL_0078:
	{
		TextWriter_t3478189236 * L_11 = __this->get_writer_10();
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_11, _stringLiteral3455629300);
		goto IL_0156;
	}

IL_008d:
	{
		TextWriter_t3478189236 * L_12 = __this->get_writer_10();
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_12, _stringLiteral3455498228);
		goto IL_0156;
	}

IL_00a2:
	{
		TextWriter_t3478189236 * L_13 = __this->get_writer_10();
		NullCheck(L_13);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_13, ((int32_t)92));
		TextWriter_t3478189236 * L_14 = __this->get_writer_10();
		String_t* L_15 = ___str0;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		Il2CppChar L_17 = String_get_Chars_m2986988803(L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_14, L_17);
		goto IL_0156;
	}

IL_00c6:
	{
		TextWriter_t3478189236 * L_18 = __this->get_writer_10();
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_18, _stringLiteral3454318580);
		goto IL_0156;
	}

IL_00db:
	{
		TextWriter_t3478189236 * L_19 = __this->get_writer_10();
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_19, _stringLiteral3454580724);
		goto IL_0156;
	}

IL_00f0:
	{
		String_t* L_20 = ___str0;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		Il2CppChar L_22 = String_get_Chars_m2986988803(L_20, L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)32))))
		{
			goto IL_0123;
		}
	}
	{
		String_t* L_23 = ___str0;
		int32_t L_24 = V_1;
		NullCheck(L_23);
		Il2CppChar L_25 = String_get_Chars_m2986988803(L_23, L_24, /*hidden argument*/NULL);
		if ((((int32_t)L_25) > ((int32_t)((int32_t)126))))
		{
			goto IL_0123;
		}
	}
	{
		TextWriter_t3478189236 * L_26 = __this->get_writer_10();
		String_t* L_27 = ___str0;
		int32_t L_28 = V_1;
		NullCheck(L_27);
		Il2CppChar L_29 = String_get_Chars_m2986988803(L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_26, L_29);
		goto IL_0156;
	}

IL_0123:
	{
		String_t* L_30 = ___str0;
		int32_t L_31 = V_1;
		NullCheck(L_30);
		Il2CppChar L_32 = String_get_Chars_m2986988803(L_30, L_31, /*hidden argument*/NULL);
		CharU5BU5D_t3528271667* L_33 = __this->get_hex_seq_4();
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t3570089748_il2cpp_TypeInfo_var);
		JsonWriter_IntToHex_m1207120023(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		TextWriter_t3478189236 * L_34 = __this->get_writer_10();
		NullCheck(L_34);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_34, _stringLiteral3455432692);
		TextWriter_t3478189236 * L_35 = __this->get_writer_10();
		CharU5BU5D_t3528271667* L_36 = __this->get_hex_seq_4();
		NullCheck(L_35);
		VirtActionInvoker1< CharU5BU5D_t3528271667* >::Invoke(9 /* System.Void System.IO.TextWriter::Write(System.Char[]) */, L_35, L_36);
	}

IL_0156:
	{
		int32_t L_37 = V_1;
		V_1 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_015a:
	{
		int32_t L_38 = V_1;
		int32_t L_39 = V_0;
		if ((((int32_t)L_38) < ((int32_t)L_39)))
		{
			goto IL_0026;
		}
	}
	{
		TextWriter_t3478189236 * L_40 = __this->get_writer_10();
		NullCheck(L_40);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_40, ((int32_t)34));
		return;
	}
}
// System.String LitJson.JsonWriter::ToString()
extern "C"  String_t* JsonWriter_ToString_m1507950332 (JsonWriter_t3570089748 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_ToString_m1507950332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t1712802186 * L_0 = __this->get_inst_string_builder_7();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0011:
	{
		StringBuilder_t1712802186 * L_2 = __this->get_inst_string_builder_7();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		return L_3;
	}
}
// System.Void LitJson.JsonWriter::Write(System.Decimal)
extern "C"  void JsonWriter_Write_m934090602 (JsonWriter_t3570089748 * __this, Decimal_t2948259380  ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m934090602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2027671802(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m2576790467(__this, /*hidden argument*/NULL);
		Decimal_t2948259380  L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t3570089748_il2cpp_TypeInfo_var);
		NumberFormatInfo_t435877138 * L_1 = ((JsonWriter_t3570089748_StaticFields*)JsonWriter_t3570089748_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m3734943936(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		JsonWriter_Put_m2154223978(__this, L_2, /*hidden argument*/NULL);
		WriterContext_t1011093999 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		L_3->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void LitJson.JsonWriter::Write(System.Int32)
extern "C"  void JsonWriter_Write_m2431965710 (JsonWriter_t3570089748 * __this, int32_t ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2431965710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2027671802(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m2576790467(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t3570089748_il2cpp_TypeInfo_var);
		NumberFormatInfo_t435877138 * L_1 = ((JsonWriter_t3570089748_StaticFields*)JsonWriter_t3570089748_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m2614817407(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		JsonWriter_Put_m2154223978(__this, L_2, /*hidden argument*/NULL);
		WriterContext_t1011093999 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		L_3->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void LitJson.JsonWriter::Write(System.String)
extern "C"  void JsonWriter_Write_m3439490280 (JsonWriter_t3570089748 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3439490280_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2027671802(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m2576790467(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___str0;
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		JsonWriter_Put_m2154223978(__this, _stringLiteral1202628576, /*hidden argument*/NULL);
		goto IL_002a;
	}

IL_0023:
	{
		String_t* L_1 = ___str0;
		JsonWriter_PutString_m890241341(__this, L_1, /*hidden argument*/NULL);
	}

IL_002a:
	{
		WriterContext_t1011093999 * L_2 = __this->get_context_1();
		NullCheck(L_2);
		L_2->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void LitJson.JsonWriter::Write(System.UInt64)
extern "C"  void JsonWriter_Write_m2044706063 (JsonWriter_t3570089748 * __this, uint64_t ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2044706063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonWriter_DoValidation_m2027671802(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m2576790467(__this, /*hidden argument*/NULL);
		uint64_t L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t3570089748_il2cpp_TypeInfo_var);
		NumberFormatInfo_t435877138 * L_1 = ((JsonWriter_t3570089748_StaticFields*)JsonWriter_t3570089748_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m301771913(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		JsonWriter_Put_m2154223978(__this, L_2, /*hidden argument*/NULL);
		WriterContext_t1011093999 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		L_3->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Boolean LitJson.Lexer::get_EndOfInput()
extern "C"  bool Lexer_get_EndOfInput_m1249127404 (Lexer_t1514038666 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_end_of_input_4();
		return L_0;
	}
}
// System.Int32 LitJson.Lexer::get_Token()
extern "C"  int32_t Lexer_get_Token_m1219310079 (Lexer_t1514038666 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_token_12();
		return L_0;
	}
}
// System.String LitJson.Lexer::get_StringValue()
extern "C"  String_t* Lexer_get_StringValue_m673320728 (Lexer_t1514038666 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_string_value_11();
		return L_0;
	}
}
// System.Void LitJson.Lexer::.cctor()
extern "C"  void Lexer__cctor_m3533321053 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer__cctor_m3533321053_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Lexer_PopulateFsmTables_m1759663568(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.Lexer::.ctor(System.IO.TextReader)
extern "C"  void Lexer__ctor_m465008812 (Lexer_t1514038666 * __this, TextReader_t283511965 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer__ctor_m465008812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		__this->set_allow_comments_2((bool)1);
		__this->set_allow_single_quoted_strings_3((bool)1);
		__this->set_input_buffer_6(0);
		StringBuilder_t1712802186 * L_0 = (StringBuilder_t1712802186 *)il2cpp_codegen_object_new(StringBuilder_t1712802186_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m2367297767(L_0, ((int32_t)128), /*hidden argument*/NULL);
		__this->set_string_buffer_10(L_0);
		__this->set_state_9(1);
		__this->set_end_of_input_4((bool)0);
		TextReader_t283511965 * L_1 = ___reader0;
		__this->set_reader_8(L_1);
		FsmContext_t2331368794 * L_2 = (FsmContext_t2331368794 *)il2cpp_codegen_object_new(FsmContext_t2331368794_il2cpp_TypeInfo_var);
		FsmContext__ctor_m1086078664(L_2, /*hidden argument*/NULL);
		__this->set_fsm_context_5(L_2);
		FsmContext_t2331368794 * L_3 = __this->get_fsm_context_5();
		NullCheck(L_3);
		L_3->set_L_2(__this);
		return;
	}
}
// System.Int32 LitJson.Lexer::HexValue(System.Int32)
extern "C"  int32_t Lexer_HexValue_m4177870290 (Il2CppObject * __this /* static, unused */, int32_t ___digit0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___digit0;
		switch (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))))
		{
			case 0:
			{
				goto IL_0047;
			}
			case 1:
			{
				goto IL_004a;
			}
			case 2:
			{
				goto IL_004d;
			}
			case 3:
			{
				goto IL_0050;
			}
			case 4:
			{
				goto IL_0053;
			}
			case 5:
			{
				goto IL_0056;
			}
		}
	}
	{
		int32_t L_1 = ___digit0;
		switch (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))))
		{
			case 0:
			{
				goto IL_0047;
			}
			case 1:
			{
				goto IL_004a;
			}
			case 2:
			{
				goto IL_004d;
			}
			case 3:
			{
				goto IL_0050;
			}
			case 4:
			{
				goto IL_0053;
			}
			case 5:
			{
				goto IL_0056;
			}
		}
	}
	{
		goto IL_0059;
	}

IL_0047:
	{
		return ((int32_t)10);
	}

IL_004a:
	{
		return ((int32_t)11);
	}

IL_004d:
	{
		return ((int32_t)12);
	}

IL_0050:
	{
		return ((int32_t)13);
	}

IL_0053:
	{
		return ((int32_t)14);
	}

IL_0056:
	{
		return ((int32_t)15);
	}

IL_0059:
	{
		int32_t L_2 = ___digit0;
		return ((int32_t)((int32_t)L_2-(int32_t)((int32_t)48)));
	}
}
// System.Void LitJson.Lexer::PopulateFsmTables()
extern "C"  void Lexer_PopulateFsmTables_m1759663568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer_PopulateFsmTables_m1759663568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B2_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B1_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B1_2 = NULL;
	int32_t G_B4_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B4_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B4_2 = NULL;
	int32_t G_B3_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B3_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B3_2 = NULL;
	int32_t G_B6_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B6_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B6_2 = NULL;
	int32_t G_B5_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B5_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B5_2 = NULL;
	int32_t G_B8_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B8_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B8_2 = NULL;
	int32_t G_B7_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B7_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B7_2 = NULL;
	int32_t G_B10_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B10_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B10_2 = NULL;
	int32_t G_B9_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B9_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B9_2 = NULL;
	int32_t G_B12_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B12_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B12_2 = NULL;
	int32_t G_B11_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B11_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B11_2 = NULL;
	int32_t G_B14_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B14_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B14_2 = NULL;
	int32_t G_B13_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B13_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B13_2 = NULL;
	int32_t G_B16_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B16_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B16_2 = NULL;
	int32_t G_B15_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B15_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B15_2 = NULL;
	int32_t G_B18_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B18_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B18_2 = NULL;
	int32_t G_B17_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B17_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B17_2 = NULL;
	int32_t G_B20_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B20_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B20_2 = NULL;
	int32_t G_B19_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B19_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B19_2 = NULL;
	int32_t G_B22_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B22_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B22_2 = NULL;
	int32_t G_B21_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B21_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B21_2 = NULL;
	int32_t G_B24_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B24_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B24_2 = NULL;
	int32_t G_B23_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B23_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B23_2 = NULL;
	int32_t G_B26_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B26_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B26_2 = NULL;
	int32_t G_B25_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B25_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B25_2 = NULL;
	int32_t G_B28_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B28_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B28_2 = NULL;
	int32_t G_B27_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B27_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B27_2 = NULL;
	int32_t G_B30_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B30_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B30_2 = NULL;
	int32_t G_B29_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B29_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B29_2 = NULL;
	int32_t G_B32_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B32_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B32_2 = NULL;
	int32_t G_B31_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B31_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B31_2 = NULL;
	int32_t G_B34_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B34_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B34_2 = NULL;
	int32_t G_B33_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B33_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B33_2 = NULL;
	int32_t G_B36_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B36_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B36_2 = NULL;
	int32_t G_B35_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B35_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B35_2 = NULL;
	int32_t G_B38_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B38_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B38_2 = NULL;
	int32_t G_B37_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B37_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B37_2 = NULL;
	int32_t G_B40_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B40_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B40_2 = NULL;
	int32_t G_B39_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B39_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B39_2 = NULL;
	int32_t G_B42_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B42_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B42_2 = NULL;
	int32_t G_B41_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B41_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B41_2 = NULL;
	int32_t G_B44_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B44_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B44_2 = NULL;
	int32_t G_B43_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B43_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B43_2 = NULL;
	int32_t G_B46_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B46_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B46_2 = NULL;
	int32_t G_B45_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B45_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B45_2 = NULL;
	int32_t G_B48_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B48_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B48_2 = NULL;
	int32_t G_B47_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B47_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B47_2 = NULL;
	int32_t G_B50_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B50_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B50_2 = NULL;
	int32_t G_B49_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B49_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B49_2 = NULL;
	int32_t G_B52_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B52_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B52_2 = NULL;
	int32_t G_B51_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B51_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B51_2 = NULL;
	int32_t G_B54_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B54_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B54_2 = NULL;
	int32_t G_B53_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B53_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B53_2 = NULL;
	int32_t G_B56_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B56_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B56_2 = NULL;
	int32_t G_B55_0 = 0;
	StateHandlerU5BU5D_t1323790106* G_B55_1 = NULL;
	StateHandlerU5BU5D_t1323790106* G_B55_2 = NULL;
	{
		StateHandlerU5BU5D_t1323790106* L_0 = ((StateHandlerU5BU5D_t1323790106*)SZArrayNew(StateHandlerU5BU5D_t1323790106_il2cpp_TypeInfo_var, (uint32_t)((int32_t)28)));
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_1 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_14();
		G_B1_0 = 0;
		G_B1_1 = L_0;
		G_B1_2 = L_0;
		if (L_1)
		{
			G_B2_0 = 0;
			G_B2_1 = L_0;
			G_B2_2 = L_0;
			goto IL_0021;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Lexer_State1_m551089695_MethodInfo_var);
		StateHandler_t105866779 * L_3 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_3, NULL, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_14(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_4 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_14();
		NullCheck(G_B2_1);
		ArrayElementTypeCheck (G_B2_1, L_4);
		(G_B2_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B2_0), (StateHandler_t105866779 *)L_4);
		StateHandlerU5BU5D_t1323790106* L_5 = G_B2_2;
		StateHandler_t105866779 * L_6 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_15();
		G_B3_0 = 1;
		G_B3_1 = L_5;
		G_B3_2 = L_5;
		if (L_6)
		{
			G_B4_0 = 1;
			G_B4_1 = L_5;
			G_B4_2 = L_5;
			goto IL_0041;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)Lexer_State2_m803286364_MethodInfo_var);
		StateHandler_t105866779 * L_8 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_8, NULL, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache1_15(L_8);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_9 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_15();
		NullCheck(G_B4_1);
		ArrayElementTypeCheck (G_B4_1, L_9);
		(G_B4_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B4_0), (StateHandler_t105866779 *)L_9);
		StateHandlerU5BU5D_t1323790106* L_10 = G_B4_2;
		StateHandler_t105866779 * L_11 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_16();
		G_B5_0 = 2;
		G_B5_1 = L_10;
		G_B5_2 = L_10;
		if (L_11)
		{
			G_B6_0 = 2;
			G_B6_1 = L_10;
			G_B6_2 = L_10;
			goto IL_0061;
		}
	}
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)Lexer_State3_m1508224687_MethodInfo_var);
		StateHandler_t105866779 * L_13 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_13, NULL, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache2_16(L_13);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
		G_B6_2 = G_B5_2;
	}

IL_0061:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_14 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_16();
		NullCheck(G_B6_1);
		ArrayElementTypeCheck (G_B6_1, L_14);
		(G_B6_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_0), (StateHandler_t105866779 *)L_14);
		StateHandlerU5BU5D_t1323790106* L_15 = G_B6_2;
		StateHandler_t105866779 * L_16 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache3_17();
		G_B7_0 = 3;
		G_B7_1 = L_15;
		G_B7_2 = L_15;
		if (L_16)
		{
			G_B8_0 = 3;
			G_B8_1 = L_15;
			G_B8_2 = L_15;
			goto IL_0081;
		}
	}
	{
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)Lexer_State4_m1356451111_MethodInfo_var);
		StateHandler_t105866779 * L_18 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_18, NULL, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache3_17(L_18);
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
		G_B8_2 = G_B7_2;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_19 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache3_17();
		NullCheck(G_B8_1);
		ArrayElementTypeCheck (G_B8_1, L_19);
		(G_B8_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_0), (StateHandler_t105866779 *)L_19);
		StateHandlerU5BU5D_t1323790106* L_20 = G_B8_2;
		StateHandler_t105866779 * L_21 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache4_18();
		G_B9_0 = 4;
		G_B9_1 = L_20;
		G_B9_2 = L_20;
		if (L_21)
		{
			G_B10_0 = 4;
			G_B10_1 = L_20;
			G_B10_2 = L_20;
			goto IL_00a1;
		}
	}
	{
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)Lexer_State5_m4046667347_MethodInfo_var);
		StateHandler_t105866779 * L_23 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_23, NULL, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache4_18(L_23);
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_24 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache4_18();
		NullCheck(G_B10_1);
		ArrayElementTypeCheck (G_B10_1, L_24);
		(G_B10_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B10_0), (StateHandler_t105866779 *)L_24);
		StateHandlerU5BU5D_t1323790106* L_25 = G_B10_2;
		StateHandler_t105866779 * L_26 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache5_19();
		G_B11_0 = 5;
		G_B11_1 = L_25;
		G_B11_2 = L_25;
		if (L_26)
		{
			G_B12_0 = 5;
			G_B12_1 = L_25;
			G_B12_2 = L_25;
			goto IL_00c1;
		}
	}
	{
		IntPtr_t L_27;
		L_27.set_m_value_0((void*)(void*)Lexer_State6_m1276813619_MethodInfo_var);
		StateHandler_t105866779 * L_28 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_28, NULL, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache5_19(L_28);
		G_B12_0 = G_B11_0;
		G_B12_1 = G_B11_1;
		G_B12_2 = G_B11_2;
	}

IL_00c1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_29 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache5_19();
		NullCheck(G_B12_1);
		ArrayElementTypeCheck (G_B12_1, L_29);
		(G_B12_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B12_0), (StateHandler_t105866779 *)L_29);
		StateHandlerU5BU5D_t1323790106* L_30 = G_B12_2;
		StateHandler_t105866779 * L_31 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache6_20();
		G_B13_0 = 6;
		G_B13_1 = L_30;
		G_B13_2 = L_30;
		if (L_31)
		{
			G_B14_0 = 6;
			G_B14_1 = L_30;
			G_B14_2 = L_30;
			goto IL_00e1;
		}
	}
	{
		IntPtr_t L_32;
		L_32.set_m_value_0((void*)(void*)Lexer_State7_m1103110371_MethodInfo_var);
		StateHandler_t105866779 * L_33 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_33, NULL, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache6_20(L_33);
		G_B14_0 = G_B13_0;
		G_B14_1 = G_B13_1;
		G_B14_2 = G_B13_2;
	}

IL_00e1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_34 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache6_20();
		NullCheck(G_B14_1);
		ArrayElementTypeCheck (G_B14_1, L_34);
		(G_B14_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_0), (StateHandler_t105866779 *)L_34);
		StateHandlerU5BU5D_t1323790106* L_35 = G_B14_2;
		StateHandler_t105866779 * L_36 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache7_21();
		G_B15_0 = 7;
		G_B15_1 = L_35;
		G_B15_2 = L_35;
		if (L_36)
		{
			G_B16_0 = 7;
			G_B16_1 = L_35;
			G_B16_2 = L_35;
			goto IL_0101;
		}
	}
	{
		IntPtr_t L_37;
		L_37.set_m_value_0((void*)(void*)Lexer_State8_m4126769589_MethodInfo_var);
		StateHandler_t105866779 * L_38 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_38, NULL, L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache7_21(L_38);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
		G_B16_2 = G_B15_2;
	}

IL_0101:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_39 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache7_21();
		NullCheck(G_B16_1);
		ArrayElementTypeCheck (G_B16_1, L_39);
		(G_B16_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B16_0), (StateHandler_t105866779 *)L_39);
		StateHandlerU5BU5D_t1323790106* L_40 = G_B16_2;
		StateHandler_t105866779 * L_41 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache8_22();
		G_B17_0 = 8;
		G_B17_1 = L_40;
		G_B17_2 = L_40;
		if (L_41)
		{
			G_B18_0 = 8;
			G_B18_1 = L_40;
			G_B18_2 = L_40;
			goto IL_0121;
		}
	}
	{
		IntPtr_t L_42;
		L_42.set_m_value_0((void*)(void*)Lexer_State9_m3407663359_MethodInfo_var);
		StateHandler_t105866779 * L_43 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_43, NULL, L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache8_22(L_43);
		G_B18_0 = G_B17_0;
		G_B18_1 = G_B17_1;
		G_B18_2 = G_B17_2;
	}

IL_0121:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_44 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache8_22();
		NullCheck(G_B18_1);
		ArrayElementTypeCheck (G_B18_1, L_44);
		(G_B18_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B18_0), (StateHandler_t105866779 *)L_44);
		StateHandlerU5BU5D_t1323790106* L_45 = G_B18_2;
		StateHandler_t105866779 * L_46 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache9_23();
		G_B19_0 = ((int32_t)9);
		G_B19_1 = L_45;
		G_B19_2 = L_45;
		if (L_46)
		{
			G_B20_0 = ((int32_t)9);
			G_B20_1 = L_45;
			G_B20_2 = L_45;
			goto IL_0142;
		}
	}
	{
		IntPtr_t L_47;
		L_47.set_m_value_0((void*)(void*)Lexer_State10_m3929854498_MethodInfo_var);
		StateHandler_t105866779 * L_48 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_48, NULL, L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache9_23(L_48);
		G_B20_0 = G_B19_0;
		G_B20_1 = G_B19_1;
		G_B20_2 = G_B19_2;
	}

IL_0142:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_49 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache9_23();
		NullCheck(G_B20_1);
		ArrayElementTypeCheck (G_B20_1, L_49);
		(G_B20_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_0), (StateHandler_t105866779 *)L_49);
		StateHandlerU5BU5D_t1323790106* L_50 = G_B20_2;
		StateHandler_t105866779 * L_51 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheA_24();
		G_B21_0 = ((int32_t)10);
		G_B21_1 = L_50;
		G_B21_2 = L_50;
		if (L_51)
		{
			G_B22_0 = ((int32_t)10);
			G_B22_1 = L_50;
			G_B22_2 = L_50;
			goto IL_0163;
		}
	}
	{
		IntPtr_t L_52;
		L_52.set_m_value_0((void*)(void*)Lexer_State11_m3893158315_MethodInfo_var);
		StateHandler_t105866779 * L_53 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_53, NULL, L_52, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheA_24(L_53);
		G_B22_0 = G_B21_0;
		G_B22_1 = G_B21_1;
		G_B22_2 = G_B21_2;
	}

IL_0163:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_54 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheA_24();
		NullCheck(G_B22_1);
		ArrayElementTypeCheck (G_B22_1, L_54);
		(G_B22_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B22_0), (StateHandler_t105866779 *)L_54);
		StateHandlerU5BU5D_t1323790106* L_55 = G_B22_2;
		StateHandler_t105866779 * L_56 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheB_25();
		G_B23_0 = ((int32_t)11);
		G_B23_1 = L_55;
		G_B23_2 = L_55;
		if (L_56)
		{
			G_B24_0 = ((int32_t)11);
			G_B24_1 = L_55;
			G_B24_2 = L_55;
			goto IL_0184;
		}
	}
	{
		IntPtr_t L_57;
		L_57.set_m_value_0((void*)(void*)Lexer_State12_m4025570652_MethodInfo_var);
		StateHandler_t105866779 * L_58 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_58, NULL, L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheB_25(L_58);
		G_B24_0 = G_B23_0;
		G_B24_1 = G_B23_1;
		G_B24_2 = G_B23_2;
	}

IL_0184:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_59 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheB_25();
		NullCheck(G_B24_1);
		ArrayElementTypeCheck (G_B24_1, L_59);
		(G_B24_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B24_0), (StateHandler_t105866779 *)L_59);
		StateHandlerU5BU5D_t1323790106* L_60 = G_B24_2;
		StateHandler_t105866779 * L_61 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheC_26();
		G_B25_0 = ((int32_t)12);
		G_B25_1 = L_60;
		G_B25_2 = L_60;
		if (L_61)
		{
			G_B26_0 = ((int32_t)12);
			G_B26_1 = L_60;
			G_B26_2 = L_60;
			goto IL_01a5;
		}
	}
	{
		IntPtr_t L_62;
		L_62.set_m_value_0((void*)(void*)Lexer_State13_m3988562965_MethodInfo_var);
		StateHandler_t105866779 * L_63 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_63, NULL, L_62, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheC_26(L_63);
		G_B26_0 = G_B25_0;
		G_B26_1 = G_B25_1;
		G_B26_2 = G_B25_2;
	}

IL_01a5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_64 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheC_26();
		NullCheck(G_B26_1);
		ArrayElementTypeCheck (G_B26_1, L_64);
		(G_B26_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B26_0), (StateHandler_t105866779 *)L_64);
		StateHandlerU5BU5D_t1323790106* L_65 = G_B26_2;
		StateHandler_t105866779 * L_66 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheD_27();
		G_B27_0 = ((int32_t)13);
		G_B27_1 = L_65;
		G_B27_2 = L_65;
		if (L_66)
		{
			G_B28_0 = ((int32_t)13);
			G_B28_1 = L_65;
			G_B28_2 = L_65;
			goto IL_01c6;
		}
	}
	{
		IntPtr_t L_67;
		L_67.set_m_value_0((void*)(void*)Lexer_State14_m3354309022_MethodInfo_var);
		StateHandler_t105866779 * L_68 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_68, NULL, L_67, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheD_27(L_68);
		G_B28_0 = G_B27_0;
		G_B28_1 = G_B27_1;
		G_B28_2 = G_B27_2;
	}

IL_01c6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_69 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheD_27();
		NullCheck(G_B28_1);
		ArrayElementTypeCheck (G_B28_1, L_69);
		(G_B28_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B28_0), (StateHandler_t105866779 *)L_69);
		StateHandlerU5BU5D_t1323790106* L_70 = G_B28_2;
		StateHandler_t105866779 * L_71 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheE_28();
		G_B29_0 = ((int32_t)14);
		G_B29_1 = L_70;
		G_B29_2 = L_70;
		if (L_71)
		{
			G_B30_0 = ((int32_t)14);
			G_B30_1 = L_70;
			G_B30_2 = L_70;
			goto IL_01e7;
		}
	}
	{
		IntPtr_t L_72;
		L_72.set_m_value_0((void*)(void*)Lexer_State15_m3741500279_MethodInfo_var);
		StateHandler_t105866779 * L_73 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_73, NULL, L_72, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheE_28(L_73);
		G_B30_0 = G_B29_0;
		G_B30_1 = G_B29_1;
		G_B30_2 = G_B29_2;
	}

IL_01e7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_74 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheE_28();
		NullCheck(G_B30_1);
		ArrayElementTypeCheck (G_B30_1, L_74);
		(G_B30_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B30_0), (StateHandler_t105866779 *)L_74);
		StateHandlerU5BU5D_t1323790106* L_75 = G_B30_2;
		StateHandler_t105866779 * L_76 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheF_29();
		G_B31_0 = ((int32_t)15);
		G_B31_1 = L_75;
		G_B31_2 = L_75;
		if (L_76)
		{
			G_B32_0 = ((int32_t)15);
			G_B32_1 = L_75;
			G_B32_2 = L_75;
			goto IL_0208;
		}
	}
	{
		IntPtr_t L_77;
		L_77.set_m_value_0((void*)(void*)Lexer_State16_m3863621896_MethodInfo_var);
		StateHandler_t105866779 * L_78 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_78, NULL, L_77, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheF_29(L_78);
		G_B32_0 = G_B31_0;
		G_B32_1 = G_B31_1;
		G_B32_2 = G_B31_2;
	}

IL_0208:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_79 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheF_29();
		NullCheck(G_B32_1);
		ArrayElementTypeCheck (G_B32_1, L_79);
		(G_B32_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B32_0), (StateHandler_t105866779 *)L_79);
		StateHandlerU5BU5D_t1323790106* L_80 = G_B32_2;
		StateHandler_t105866779 * L_81 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache10_30();
		G_B33_0 = ((int32_t)16);
		G_B33_1 = L_80;
		G_B33_2 = L_80;
		if (L_81)
		{
			G_B34_0 = ((int32_t)16);
			G_B34_1 = L_80;
			G_B34_2 = L_80;
			goto IL_0229;
		}
	}
	{
		IntPtr_t L_82;
		L_82.set_m_value_0((void*)(void*)Lexer_State17_m3264355937_MethodInfo_var);
		StateHandler_t105866779 * L_83 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_83, NULL, L_82, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache10_30(L_83);
		G_B34_0 = G_B33_0;
		G_B34_1 = G_B33_1;
		G_B34_2 = G_B33_2;
	}

IL_0229:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_84 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache10_30();
		NullCheck(G_B34_1);
		ArrayElementTypeCheck (G_B34_1, L_84);
		(G_B34_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B34_0), (StateHandler_t105866779 *)L_84);
		StateHandlerU5BU5D_t1323790106* L_85 = G_B34_2;
		StateHandler_t105866779 * L_86 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache11_31();
		G_B35_0 = ((int32_t)17);
		G_B35_1 = L_85;
		G_B35_2 = L_85;
		if (L_86)
		{
			G_B36_0 = ((int32_t)17);
			G_B36_1 = L_85;
			G_B36_2 = L_85;
			goto IL_024a;
		}
	}
	{
		IntPtr_t L_87;
		L_87.set_m_value_0((void*)(void*)Lexer_State18_m3616214826_MethodInfo_var);
		StateHandler_t105866779 * L_88 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_88, NULL, L_87, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache11_31(L_88);
		G_B36_0 = G_B35_0;
		G_B36_1 = G_B35_1;
		G_B36_2 = G_B35_2;
	}

IL_024a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_89 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache11_31();
		NullCheck(G_B36_1);
		ArrayElementTypeCheck (G_B36_1, L_89);
		(G_B36_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B36_0), (StateHandler_t105866779 *)L_89);
		StateHandlerU5BU5D_t1323790106* L_90 = G_B36_2;
		StateHandler_t105866779 * L_91 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache12_32();
		G_B37_0 = ((int32_t)18);
		G_B37_1 = L_90;
		G_B37_2 = L_90;
		if (L_91)
		{
			G_B38_0 = ((int32_t)18);
			G_B38_1 = L_90;
			G_B38_2 = L_90;
			goto IL_026b;
		}
	}
	{
		IntPtr_t L_92;
		L_92.set_m_value_0((void*)(void*)Lexer_State19_m3579281075_MethodInfo_var);
		StateHandler_t105866779 * L_93 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_93, NULL, L_92, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache12_32(L_93);
		G_B38_0 = G_B37_0;
		G_B38_1 = G_B37_1;
		G_B38_2 = G_B37_2;
	}

IL_026b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_94 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache12_32();
		NullCheck(G_B38_1);
		ArrayElementTypeCheck (G_B38_1, L_94);
		(G_B38_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B38_0), (StateHandler_t105866779 *)L_94);
		StateHandlerU5BU5D_t1323790106* L_95 = G_B38_2;
		StateHandler_t105866779 * L_96 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache13_33();
		G_B39_0 = ((int32_t)19);
		G_B39_1 = L_95;
		G_B39_2 = L_95;
		if (L_96)
		{
			G_B40_0 = ((int32_t)19);
			G_B40_1 = L_95;
			G_B40_2 = L_95;
			goto IL_028c;
		}
	}
	{
		IntPtr_t L_97;
		L_97.set_m_value_0((void*)(void*)Lexer_State20_m3151936696_MethodInfo_var);
		StateHandler_t105866779 * L_98 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_98, NULL, L_97, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache13_33(L_98);
		G_B40_0 = G_B39_0;
		G_B40_1 = G_B39_1;
		G_B40_2 = G_B39_2;
	}

IL_028c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_99 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache13_33();
		NullCheck(G_B40_1);
		ArrayElementTypeCheck (G_B40_1, L_99);
		(G_B40_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B40_0), (StateHandler_t105866779 *)L_99);
		StateHandlerU5BU5D_t1323790106* L_100 = G_B40_2;
		StateHandler_t105866779 * L_101 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache14_34();
		G_B41_0 = ((int32_t)20);
		G_B41_1 = L_100;
		G_B41_2 = L_100;
		if (L_101)
		{
			G_B42_0 = ((int32_t)20);
			G_B42_1 = L_100;
			G_B42_2 = L_100;
			goto IL_02ad;
		}
	}
	{
		IntPtr_t L_102;
		L_102.set_m_value_0((void*)(void*)Lexer_State21_m3115240513_MethodInfo_var);
		StateHandler_t105866779 * L_103 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_103, NULL, L_102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache14_34(L_103);
		G_B42_0 = G_B41_0;
		G_B42_1 = G_B41_1;
		G_B42_2 = G_B41_2;
	}

IL_02ad:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_104 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache14_34();
		NullCheck(G_B42_1);
		ArrayElementTypeCheck (G_B42_1, L_104);
		(G_B42_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B42_0), (StateHandler_t105866779 *)L_104);
		StateHandlerU5BU5D_t1323790106* L_105 = G_B42_2;
		StateHandler_t105866779 * L_106 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache15_35();
		G_B43_0 = ((int32_t)21);
		G_B43_1 = L_105;
		G_B43_2 = L_105;
		if (L_106)
		{
			G_B44_0 = ((int32_t)21);
			G_B44_1 = L_105;
			G_B44_2 = L_105;
			goto IL_02ce;
		}
	}
	{
		IntPtr_t L_107;
		L_107.set_m_value_0((void*)(void*)Lexer_State22_m3247652850_MethodInfo_var);
		StateHandler_t105866779 * L_108 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_108, NULL, L_107, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache15_35(L_108);
		G_B44_0 = G_B43_0;
		G_B44_1 = G_B43_1;
		G_B44_2 = G_B43_2;
	}

IL_02ce:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_109 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache15_35();
		NullCheck(G_B44_1);
		ArrayElementTypeCheck (G_B44_1, L_109);
		(G_B44_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B44_0), (StateHandler_t105866779 *)L_109);
		StateHandlerU5BU5D_t1323790106* L_110 = G_B44_2;
		StateHandler_t105866779 * L_111 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache16_36();
		G_B45_0 = ((int32_t)22);
		G_B45_1 = L_110;
		G_B45_2 = L_110;
		if (L_111)
		{
			G_B46_0 = ((int32_t)22);
			G_B46_1 = L_110;
			G_B46_2 = L_110;
			goto IL_02ef;
		}
	}
	{
		IntPtr_t L_112;
		L_112.set_m_value_0((void*)(void*)Lexer_State23_m3210645163_MethodInfo_var);
		StateHandler_t105866779 * L_113 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_113, NULL, L_112, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache16_36(L_113);
		G_B46_0 = G_B45_0;
		G_B46_1 = G_B45_1;
		G_B46_2 = G_B45_2;
	}

IL_02ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_114 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache16_36();
		NullCheck(G_B46_1);
		ArrayElementTypeCheck (G_B46_1, L_114);
		(G_B46_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B46_0), (StateHandler_t105866779 *)L_114);
		StateHandlerU5BU5D_t1323790106* L_115 = G_B46_2;
		StateHandler_t105866779 * L_116 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache17_37();
		G_B47_0 = ((int32_t)23);
		G_B47_1 = L_115;
		G_B47_2 = L_115;
		if (L_116)
		{
			G_B48_0 = ((int32_t)23);
			G_B48_1 = L_115;
			G_B48_2 = L_115;
			goto IL_0310;
		}
	}
	{
		IntPtr_t L_117;
		L_117.set_m_value_0((void*)(void*)Lexer_State24_m2576391220_MethodInfo_var);
		StateHandler_t105866779 * L_118 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_118, NULL, L_117, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache17_37(L_118);
		G_B48_0 = G_B47_0;
		G_B48_1 = G_B47_1;
		G_B48_2 = G_B47_2;
	}

IL_0310:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_119 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache17_37();
		NullCheck(G_B48_1);
		ArrayElementTypeCheck (G_B48_1, L_119);
		(G_B48_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B48_0), (StateHandler_t105866779 *)L_119);
		StateHandlerU5BU5D_t1323790106* L_120 = G_B48_2;
		StateHandler_t105866779 * L_121 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache18_38();
		G_B49_0 = ((int32_t)24);
		G_B49_1 = L_120;
		G_B49_2 = L_120;
		if (L_121)
		{
			G_B50_0 = ((int32_t)24);
			G_B50_1 = L_120;
			G_B50_2 = L_120;
			goto IL_0331;
		}
	}
	{
		IntPtr_t L_122;
		L_122.set_m_value_0((void*)(void*)Lexer_State25_m2963582477_MethodInfo_var);
		StateHandler_t105866779 * L_123 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_123, NULL, L_122, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache18_38(L_123);
		G_B50_0 = G_B49_0;
		G_B50_1 = G_B49_1;
		G_B50_2 = G_B49_2;
	}

IL_0331:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_124 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache18_38();
		NullCheck(G_B50_1);
		ArrayElementTypeCheck (G_B50_1, L_124);
		(G_B50_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B50_0), (StateHandler_t105866779 *)L_124);
		StateHandlerU5BU5D_t1323790106* L_125 = G_B50_2;
		StateHandler_t105866779 * L_126 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache19_39();
		G_B51_0 = ((int32_t)25);
		G_B51_1 = L_125;
		G_B51_2 = L_125;
		if (L_126)
		{
			G_B52_0 = ((int32_t)25);
			G_B52_1 = L_125;
			G_B52_2 = L_125;
			goto IL_0352;
		}
	}
	{
		IntPtr_t L_127;
		L_127.set_m_value_0((void*)(void*)Lexer_State26_m3085704094_MethodInfo_var);
		StateHandler_t105866779 * L_128 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_128, NULL, L_127, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache19_39(L_128);
		G_B52_0 = G_B51_0;
		G_B52_1 = G_B51_1;
		G_B52_2 = G_B51_2;
	}

IL_0352:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_129 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache19_39();
		NullCheck(G_B52_1);
		ArrayElementTypeCheck (G_B52_1, L_129);
		(G_B52_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B52_0), (StateHandler_t105866779 *)L_129);
		StateHandlerU5BU5D_t1323790106* L_130 = G_B52_2;
		StateHandler_t105866779 * L_131 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1A_40();
		G_B53_0 = ((int32_t)26);
		G_B53_1 = L_130;
		G_B53_2 = L_130;
		if (L_131)
		{
			G_B54_0 = ((int32_t)26);
			G_B54_1 = L_130;
			G_B54_2 = L_130;
			goto IL_0373;
		}
	}
	{
		IntPtr_t L_132;
		L_132.set_m_value_0((void*)(void*)Lexer_State27_m2486438135_MethodInfo_var);
		StateHandler_t105866779 * L_133 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_133, NULL, L_132, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache1A_40(L_133);
		G_B54_0 = G_B53_0;
		G_B54_1 = G_B53_1;
		G_B54_2 = G_B53_2;
	}

IL_0373:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_134 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1A_40();
		NullCheck(G_B54_1);
		ArrayElementTypeCheck (G_B54_1, L_134);
		(G_B54_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B54_0), (StateHandler_t105866779 *)L_134);
		StateHandlerU5BU5D_t1323790106* L_135 = G_B54_2;
		StateHandler_t105866779 * L_136 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1B_41();
		G_B55_0 = ((int32_t)27);
		G_B55_1 = L_135;
		G_B55_2 = L_135;
		if (L_136)
		{
			G_B56_0 = ((int32_t)27);
			G_B56_1 = L_135;
			G_B56_2 = L_135;
			goto IL_0394;
		}
	}
	{
		IntPtr_t L_137;
		L_137.set_m_value_0((void*)(void*)Lexer_State28_m2838297024_MethodInfo_var);
		StateHandler_t105866779 * L_138 = (StateHandler_t105866779 *)il2cpp_codegen_object_new(StateHandler_t105866779_il2cpp_TypeInfo_var);
		StateHandler__ctor_m2721920384(L_138, NULL, L_137, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache1B_41(L_138);
		G_B56_0 = G_B55_0;
		G_B56_1 = G_B55_1;
		G_B56_2 = G_B55_2;
	}

IL_0394:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandler_t105866779 * L_139 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1B_41();
		NullCheck(G_B56_1);
		ArrayElementTypeCheck (G_B56_1, L_139);
		(G_B56_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B56_0), (StateHandler_t105866779 *)L_139);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_fsm_handler_table_1(G_B56_2);
		Int32U5BU5D_t385246372* L_140 = ((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)((int32_t)28)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_140, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t3336159630____U24fieldU2DC_12_FieldInfo_var), /*hidden argument*/NULL);
		((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->set_fsm_return_table_0(L_140);
		return;
	}
}
// System.Char LitJson.Lexer::ProcessEscChar(System.Int32)
extern "C"  Il2CppChar Lexer_ProcessEscChar_m4259284362 (Il2CppObject * __this /* static, unused */, int32_t ___esc_char0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer_ProcessEscChar_m4259284362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___esc_char0;
		switch (((int32_t)((int32_t)L_0-(int32_t)((int32_t)114))))
		{
			case 0:
			{
				goto IL_005f;
			}
			case 1:
			{
				goto IL_0015;
			}
			case 2:
			{
				goto IL_005c;
			}
		}
	}

IL_0015:
	{
		int32_t L_1 = ___esc_char0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)34))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_2 = ___esc_char0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)39))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_3 = ___esc_char0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)47))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = ___esc_char0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)92))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_5 = ___esc_char0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)98))))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_6 = ___esc_char0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)102))))
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_7 = ___esc_char0;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)110))))
		{
			goto IL_0059;
		}
	}
	{
		goto IL_0067;
	}

IL_0052:
	{
		int32_t L_8 = ___esc_char0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		Il2CppChar L_9 = Convert_ToChar_m4189066566(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0059:
	{
		return ((int32_t)10);
	}

IL_005c:
	{
		return ((int32_t)9);
	}

IL_005f:
	{
		return ((int32_t)13);
	}

IL_0062:
	{
		return 8;
	}

IL_0064:
	{
		return ((int32_t)12);
	}

IL_0067:
	{
		return ((int32_t)63);
	}
}
// System.Boolean LitJson.Lexer::State1(LitJson.FsmContext)
extern "C"  bool Lexer_State1_m551089695 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_01eb;
	}

IL_0005:
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) == ((int32_t)((int32_t)32))))
		{
			goto IL_003b;
		}
	}
	{
		FsmContext_t2331368794 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t1514038666 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) < ((int32_t)((int32_t)9))))
		{
			goto IL_0040;
		}
	}
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t1514038666 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_input_char_7();
		if ((((int32_t)L_8) > ((int32_t)((int32_t)13))))
		{
			goto IL_0040;
		}
	}

IL_003b:
	{
		goto IL_01eb;
	}

IL_0040:
	{
		FsmContext_t2331368794 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t1514038666 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		if ((((int32_t)L_11) < ((int32_t)((int32_t)49))))
		{
			goto IL_008a;
		}
	}
	{
		FsmContext_t2331368794 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t1514038666 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) > ((int32_t)((int32_t)57))))
		{
			goto IL_008a;
		}
	}
	{
		FsmContext_t2331368794 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t1514038666 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		StringBuilder_t1712802186 * L_17 = L_16->get_string_buffer_10();
		FsmContext_t2331368794 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t1514038666 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		NullCheck(L_17);
		StringBuilder_Append_m2383614642(L_17, (((int32_t)((uint16_t)L_20))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_NextState_1(3);
		return (bool)1;
	}

IL_008a:
	{
		FsmContext_t2331368794 * L_22 = ___ctx0;
		NullCheck(L_22);
		Lexer_t1514038666 * L_23 = L_22->get_L_2();
		NullCheck(L_23);
		int32_t L_24 = L_23->get_input_char_7();
		V_0 = L_24;
		int32_t L_25 = V_0;
		switch (((int32_t)((int32_t)L_25-(int32_t)((int32_t)44))))
		{
			case 0:
			{
				goto IL_0123;
			}
			case 1:
			{
				goto IL_0133;
			}
			case 2:
			{
				goto IL_00b3;
			}
			case 3:
			{
				goto IL_01cd;
			}
			case 4:
			{
				goto IL_0159;
			}
		}
	}

IL_00b3:
	{
		int32_t L_26 = V_0;
		switch (((int32_t)((int32_t)L_26-(int32_t)((int32_t)91))))
		{
			case 0:
			{
				goto IL_0123;
			}
			case 1:
			{
				goto IL_00c8;
			}
			case 2:
			{
				goto IL_0123;
			}
		}
	}

IL_00c8:
	{
		int32_t L_27 = V_0;
		switch (((int32_t)((int32_t)L_27-(int32_t)((int32_t)123))))
		{
			case 0:
			{
				goto IL_0123;
			}
			case 1:
			{
				goto IL_00dd;
			}
			case 2:
			{
				goto IL_0123;
			}
		}
	}

IL_00dd:
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)34))))
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) == ((int32_t)((int32_t)39))))
		{
			goto IL_019d;
		}
	}
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) == ((int32_t)((int32_t)58))))
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_31 = V_0;
		if ((((int32_t)L_31) == ((int32_t)((int32_t)102))))
		{
			goto IL_017f;
		}
	}
	{
		int32_t L_32 = V_0;
		if ((((int32_t)L_32) == ((int32_t)((int32_t)110))))
		{
			goto IL_0189;
		}
	}
	{
		int32_t L_33 = V_0;
		if ((((int32_t)L_33) == ((int32_t)((int32_t)116))))
		{
			goto IL_0193;
		}
	}
	{
		goto IL_01e9;
	}

IL_0112:
	{
		FsmContext_t2331368794 * L_34 = ___ctx0;
		NullCheck(L_34);
		L_34->set_NextState_1(((int32_t)19));
		FsmContext_t2331368794 * L_35 = ___ctx0;
		NullCheck(L_35);
		L_35->set_Return_0((bool)1);
		return (bool)1;
	}

IL_0123:
	{
		FsmContext_t2331368794 * L_36 = ___ctx0;
		NullCheck(L_36);
		L_36->set_NextState_1(1);
		FsmContext_t2331368794 * L_37 = ___ctx0;
		NullCheck(L_37);
		L_37->set_Return_0((bool)1);
		return (bool)1;
	}

IL_0133:
	{
		FsmContext_t2331368794 * L_38 = ___ctx0;
		NullCheck(L_38);
		Lexer_t1514038666 * L_39 = L_38->get_L_2();
		NullCheck(L_39);
		StringBuilder_t1712802186 * L_40 = L_39->get_string_buffer_10();
		FsmContext_t2331368794 * L_41 = ___ctx0;
		NullCheck(L_41);
		Lexer_t1514038666 * L_42 = L_41->get_L_2();
		NullCheck(L_42);
		int32_t L_43 = L_42->get_input_char_7();
		NullCheck(L_40);
		StringBuilder_Append_m2383614642(L_40, (((int32_t)((uint16_t)L_43))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_44 = ___ctx0;
		NullCheck(L_44);
		L_44->set_NextState_1(2);
		return (bool)1;
	}

IL_0159:
	{
		FsmContext_t2331368794 * L_45 = ___ctx0;
		NullCheck(L_45);
		Lexer_t1514038666 * L_46 = L_45->get_L_2();
		NullCheck(L_46);
		StringBuilder_t1712802186 * L_47 = L_46->get_string_buffer_10();
		FsmContext_t2331368794 * L_48 = ___ctx0;
		NullCheck(L_48);
		Lexer_t1514038666 * L_49 = L_48->get_L_2();
		NullCheck(L_49);
		int32_t L_50 = L_49->get_input_char_7();
		NullCheck(L_47);
		StringBuilder_Append_m2383614642(L_47, (((int32_t)((uint16_t)L_50))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_51 = ___ctx0;
		NullCheck(L_51);
		L_51->set_NextState_1(4);
		return (bool)1;
	}

IL_017f:
	{
		FsmContext_t2331368794 * L_52 = ___ctx0;
		NullCheck(L_52);
		L_52->set_NextState_1(((int32_t)12));
		return (bool)1;
	}

IL_0189:
	{
		FsmContext_t2331368794 * L_53 = ___ctx0;
		NullCheck(L_53);
		L_53->set_NextState_1(((int32_t)16));
		return (bool)1;
	}

IL_0193:
	{
		FsmContext_t2331368794 * L_54 = ___ctx0;
		NullCheck(L_54);
		L_54->set_NextState_1(((int32_t)9));
		return (bool)1;
	}

IL_019d:
	{
		FsmContext_t2331368794 * L_55 = ___ctx0;
		NullCheck(L_55);
		Lexer_t1514038666 * L_56 = L_55->get_L_2();
		NullCheck(L_56);
		bool L_57 = L_56->get_allow_single_quoted_strings_3();
		if (L_57)
		{
			goto IL_01af;
		}
	}
	{
		return (bool)0;
	}

IL_01af:
	{
		FsmContext_t2331368794 * L_58 = ___ctx0;
		NullCheck(L_58);
		Lexer_t1514038666 * L_59 = L_58->get_L_2();
		NullCheck(L_59);
		L_59->set_input_char_7(((int32_t)34));
		FsmContext_t2331368794 * L_60 = ___ctx0;
		NullCheck(L_60);
		L_60->set_NextState_1(((int32_t)23));
		FsmContext_t2331368794 * L_61 = ___ctx0;
		NullCheck(L_61);
		L_61->set_Return_0((bool)1);
		return (bool)1;
	}

IL_01cd:
	{
		FsmContext_t2331368794 * L_62 = ___ctx0;
		NullCheck(L_62);
		Lexer_t1514038666 * L_63 = L_62->get_L_2();
		NullCheck(L_63);
		bool L_64 = L_63->get_allow_comments_2();
		if (L_64)
		{
			goto IL_01df;
		}
	}
	{
		return (bool)0;
	}

IL_01df:
	{
		FsmContext_t2331368794 * L_65 = ___ctx0;
		NullCheck(L_65);
		L_65->set_NextState_1(((int32_t)25));
		return (bool)1;
	}

IL_01e9:
	{
		return (bool)0;
	}

IL_01eb:
	{
		FsmContext_t2331368794 * L_66 = ___ctx0;
		NullCheck(L_66);
		Lexer_t1514038666 * L_67 = L_66->get_L_2();
		NullCheck(L_67);
		bool L_68 = Lexer_GetChar_m657915143(L_67, /*hidden argument*/NULL);
		if (L_68)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State2(LitJson.FsmContext)
extern "C"  bool Lexer_State2_m803286364 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)49))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t2331368794 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t1514038666 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) > ((int32_t)((int32_t)57))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t2331368794 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t1514038666 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		StringBuilder_t1712802186 * L_10 = L_9->get_string_buffer_10();
		FsmContext_t2331368794 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t1514038666 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		NullCheck(L_10);
		StringBuilder_Append_m2383614642(L_10, (((int32_t)((uint16_t)L_13))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_14 = ___ctx0;
		NullCheck(L_14);
		L_14->set_NextState_1(3);
		return (bool)1;
	}

IL_0056:
	{
		FsmContext_t2331368794 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t1514038666 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		V_0 = L_17;
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)48))))
		{
			goto IL_006f;
		}
	}
	{
		goto IL_0095;
	}

IL_006f:
	{
		FsmContext_t2331368794 * L_19 = ___ctx0;
		NullCheck(L_19);
		Lexer_t1514038666 * L_20 = L_19->get_L_2();
		NullCheck(L_20);
		StringBuilder_t1712802186 * L_21 = L_20->get_string_buffer_10();
		FsmContext_t2331368794 * L_22 = ___ctx0;
		NullCheck(L_22);
		Lexer_t1514038666 * L_23 = L_22->get_L_2();
		NullCheck(L_23);
		int32_t L_24 = L_23->get_input_char_7();
		NullCheck(L_21);
		StringBuilder_Append_m2383614642(L_21, (((int32_t)((uint16_t)L_24))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_25 = ___ctx0;
		NullCheck(L_25);
		L_25->set_NextState_1(4);
		return (bool)1;
	}

IL_0095:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State3(LitJson.FsmContext)
extern "C"  bool Lexer_State3_m1508224687 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_0140;
	}

IL_0005:
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) < ((int32_t)((int32_t)48))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t2331368794 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t1514038666 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) > ((int32_t)((int32_t)57))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t1514038666 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		StringBuilder_t1712802186 * L_8 = L_7->get_string_buffer_10();
		FsmContext_t2331368794 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t1514038666 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		NullCheck(L_8);
		StringBuilder_Append_m2383614642(L_8, (((int32_t)((uint16_t)L_11))), /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_004b:
	{
		FsmContext_t2331368794 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t1514038666 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) == ((int32_t)((int32_t)32))))
		{
			goto IL_0081;
		}
	}
	{
		FsmContext_t2331368794 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t1514038666 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		if ((((int32_t)L_17) < ((int32_t)((int32_t)9))))
		{
			goto IL_0091;
		}
	}
	{
		FsmContext_t2331368794 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t1514038666 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		if ((((int32_t)L_20) > ((int32_t)((int32_t)13))))
		{
			goto IL_0091;
		}
	}

IL_0081:
	{
		FsmContext_t2331368794 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_22 = ___ctx0;
		NullCheck(L_22);
		L_22->set_NextState_1(1);
		return (bool)1;
	}

IL_0091:
	{
		FsmContext_t2331368794 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t1514038666 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		V_0 = L_25;
		int32_t L_26 = V_0;
		switch (((int32_t)((int32_t)L_26-(int32_t)((int32_t)44))))
		{
			case 0:
			{
				goto IL_00d7;
			}
			case 1:
			{
				goto IL_00b2;
			}
			case 2:
			{
				goto IL_00f2;
			}
		}
	}

IL_00b2:
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)69))))
		{
			goto IL_0118;
		}
	}
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)93))))
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) == ((int32_t)((int32_t)101))))
		{
			goto IL_0118;
		}
	}
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) == ((int32_t)((int32_t)125))))
		{
			goto IL_00d7;
		}
	}
	{
		goto IL_013e;
	}

IL_00d7:
	{
		FsmContext_t2331368794 * L_31 = ___ctx0;
		NullCheck(L_31);
		Lexer_t1514038666 * L_32 = L_31->get_L_2();
		NullCheck(L_32);
		Lexer_UngetChar_m1644879734(L_32, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_33 = ___ctx0;
		NullCheck(L_33);
		L_33->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_34 = ___ctx0;
		NullCheck(L_34);
		L_34->set_NextState_1(1);
		return (bool)1;
	}

IL_00f2:
	{
		FsmContext_t2331368794 * L_35 = ___ctx0;
		NullCheck(L_35);
		Lexer_t1514038666 * L_36 = L_35->get_L_2();
		NullCheck(L_36);
		StringBuilder_t1712802186 * L_37 = L_36->get_string_buffer_10();
		FsmContext_t2331368794 * L_38 = ___ctx0;
		NullCheck(L_38);
		Lexer_t1514038666 * L_39 = L_38->get_L_2();
		NullCheck(L_39);
		int32_t L_40 = L_39->get_input_char_7();
		NullCheck(L_37);
		StringBuilder_Append_m2383614642(L_37, (((int32_t)((uint16_t)L_40))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_41 = ___ctx0;
		NullCheck(L_41);
		L_41->set_NextState_1(5);
		return (bool)1;
	}

IL_0118:
	{
		FsmContext_t2331368794 * L_42 = ___ctx0;
		NullCheck(L_42);
		Lexer_t1514038666 * L_43 = L_42->get_L_2();
		NullCheck(L_43);
		StringBuilder_t1712802186 * L_44 = L_43->get_string_buffer_10();
		FsmContext_t2331368794 * L_45 = ___ctx0;
		NullCheck(L_45);
		Lexer_t1514038666 * L_46 = L_45->get_L_2();
		NullCheck(L_46);
		int32_t L_47 = L_46->get_input_char_7();
		NullCheck(L_44);
		StringBuilder_Append_m2383614642(L_44, (((int32_t)((uint16_t)L_47))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_48 = ___ctx0;
		NullCheck(L_48);
		L_48->set_NextState_1(7);
		return (bool)1;
	}

IL_013e:
	{
		return (bool)0;
	}

IL_0140:
	{
		FsmContext_t2331368794 * L_49 = ___ctx0;
		NullCheck(L_49);
		Lexer_t1514038666 * L_50 = L_49->get_L_2();
		NullCheck(L_50);
		bool L_51 = Lexer_GetChar_m657915143(L_50, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State4(LitJson.FsmContext)
extern "C"  bool Lexer_State4_m1356451111 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) == ((int32_t)((int32_t)32))))
		{
			goto IL_0042;
		}
	}
	{
		FsmContext_t2331368794 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t1514038666 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) < ((int32_t)((int32_t)9))))
		{
			goto IL_0052;
		}
	}
	{
		FsmContext_t2331368794 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t1514038666 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_input_char_7();
		if ((((int32_t)L_10) > ((int32_t)((int32_t)13))))
		{
			goto IL_0052;
		}
	}

IL_0042:
	{
		FsmContext_t2331368794 * L_11 = ___ctx0;
		NullCheck(L_11);
		L_11->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_12 = ___ctx0;
		NullCheck(L_12);
		L_12->set_NextState_1(1);
		return (bool)1;
	}

IL_0052:
	{
		FsmContext_t2331368794 * L_13 = ___ctx0;
		NullCheck(L_13);
		Lexer_t1514038666 * L_14 = L_13->get_L_2();
		NullCheck(L_14);
		int32_t L_15 = L_14->get_input_char_7();
		V_0 = L_15;
		int32_t L_16 = V_0;
		switch (((int32_t)((int32_t)L_16-(int32_t)((int32_t)44))))
		{
			case 0:
			{
				goto IL_0098;
			}
			case 1:
			{
				goto IL_0073;
			}
			case 2:
			{
				goto IL_00b3;
			}
		}
	}

IL_0073:
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)69))))
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)93))))
		{
			goto IL_0098;
		}
	}
	{
		int32_t L_19 = V_0;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)101))))
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)125))))
		{
			goto IL_0098;
		}
	}
	{
		goto IL_00ff;
	}

IL_0098:
	{
		FsmContext_t2331368794 * L_21 = ___ctx0;
		NullCheck(L_21);
		Lexer_t1514038666 * L_22 = L_21->get_L_2();
		NullCheck(L_22);
		Lexer_UngetChar_m1644879734(L_22, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_23 = ___ctx0;
		NullCheck(L_23);
		L_23->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_24 = ___ctx0;
		NullCheck(L_24);
		L_24->set_NextState_1(1);
		return (bool)1;
	}

IL_00b3:
	{
		FsmContext_t2331368794 * L_25 = ___ctx0;
		NullCheck(L_25);
		Lexer_t1514038666 * L_26 = L_25->get_L_2();
		NullCheck(L_26);
		StringBuilder_t1712802186 * L_27 = L_26->get_string_buffer_10();
		FsmContext_t2331368794 * L_28 = ___ctx0;
		NullCheck(L_28);
		Lexer_t1514038666 * L_29 = L_28->get_L_2();
		NullCheck(L_29);
		int32_t L_30 = L_29->get_input_char_7();
		NullCheck(L_27);
		StringBuilder_Append_m2383614642(L_27, (((int32_t)((uint16_t)L_30))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_31 = ___ctx0;
		NullCheck(L_31);
		L_31->set_NextState_1(5);
		return (bool)1;
	}

IL_00d9:
	{
		FsmContext_t2331368794 * L_32 = ___ctx0;
		NullCheck(L_32);
		Lexer_t1514038666 * L_33 = L_32->get_L_2();
		NullCheck(L_33);
		StringBuilder_t1712802186 * L_34 = L_33->get_string_buffer_10();
		FsmContext_t2331368794 * L_35 = ___ctx0;
		NullCheck(L_35);
		Lexer_t1514038666 * L_36 = L_35->get_L_2();
		NullCheck(L_36);
		int32_t L_37 = L_36->get_input_char_7();
		NullCheck(L_34);
		StringBuilder_Append_m2383614642(L_34, (((int32_t)((uint16_t)L_37))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_38 = ___ctx0;
		NullCheck(L_38);
		L_38->set_NextState_1(7);
		return (bool)1;
	}

IL_00ff:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State5(LitJson.FsmContext)
extern "C"  bool Lexer_State5_m4046667347 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)48))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t2331368794 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t1514038666 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) > ((int32_t)((int32_t)57))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t2331368794 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t1514038666 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		StringBuilder_t1712802186 * L_10 = L_9->get_string_buffer_10();
		FsmContext_t2331368794 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t1514038666 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		NullCheck(L_10);
		StringBuilder_Append_m2383614642(L_10, (((int32_t)((uint16_t)L_13))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_14 = ___ctx0;
		NullCheck(L_14);
		L_14->set_NextState_1(6);
		return (bool)1;
	}

IL_0056:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State6(LitJson.FsmContext)
extern "C"  bool Lexer_State6_m1276813619 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_010d;
	}

IL_0005:
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) < ((int32_t)((int32_t)48))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t2331368794 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t1514038666 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) > ((int32_t)((int32_t)57))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t1514038666 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		StringBuilder_t1712802186 * L_8 = L_7->get_string_buffer_10();
		FsmContext_t2331368794 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t1514038666 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		NullCheck(L_8);
		StringBuilder_Append_m2383614642(L_8, (((int32_t)((uint16_t)L_11))), /*hidden argument*/NULL);
		goto IL_010d;
	}

IL_004b:
	{
		FsmContext_t2331368794 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t1514038666 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) == ((int32_t)((int32_t)32))))
		{
			goto IL_0081;
		}
	}
	{
		FsmContext_t2331368794 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t1514038666 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		if ((((int32_t)L_17) < ((int32_t)((int32_t)9))))
		{
			goto IL_0091;
		}
	}
	{
		FsmContext_t2331368794 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t1514038666 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		if ((((int32_t)L_20) > ((int32_t)((int32_t)13))))
		{
			goto IL_0091;
		}
	}

IL_0081:
	{
		FsmContext_t2331368794 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_22 = ___ctx0;
		NullCheck(L_22);
		L_22->set_NextState_1(1);
		return (bool)1;
	}

IL_0091:
	{
		FsmContext_t2331368794 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t1514038666 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		V_0 = L_25;
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)44))))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)69))))
		{
			goto IL_00e5;
		}
	}
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)93))))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) == ((int32_t)((int32_t)101))))
		{
			goto IL_00e5;
		}
	}
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) == ((int32_t)((int32_t)125))))
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_010b;
	}

IL_00ca:
	{
		FsmContext_t2331368794 * L_31 = ___ctx0;
		NullCheck(L_31);
		Lexer_t1514038666 * L_32 = L_31->get_L_2();
		NullCheck(L_32);
		Lexer_UngetChar_m1644879734(L_32, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_33 = ___ctx0;
		NullCheck(L_33);
		L_33->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_34 = ___ctx0;
		NullCheck(L_34);
		L_34->set_NextState_1(1);
		return (bool)1;
	}

IL_00e5:
	{
		FsmContext_t2331368794 * L_35 = ___ctx0;
		NullCheck(L_35);
		Lexer_t1514038666 * L_36 = L_35->get_L_2();
		NullCheck(L_36);
		StringBuilder_t1712802186 * L_37 = L_36->get_string_buffer_10();
		FsmContext_t2331368794 * L_38 = ___ctx0;
		NullCheck(L_38);
		Lexer_t1514038666 * L_39 = L_38->get_L_2();
		NullCheck(L_39);
		int32_t L_40 = L_39->get_input_char_7();
		NullCheck(L_37);
		StringBuilder_Append_m2383614642(L_37, (((int32_t)((uint16_t)L_40))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_41 = ___ctx0;
		NullCheck(L_41);
		L_41->set_NextState_1(7);
		return (bool)1;
	}

IL_010b:
	{
		return (bool)0;
	}

IL_010d:
	{
		FsmContext_t2331368794 * L_42 = ___ctx0;
		NullCheck(L_42);
		Lexer_t1514038666 * L_43 = L_42->get_L_2();
		NullCheck(L_43);
		bool L_44 = Lexer_GetChar_m657915143(L_43, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State7(LitJson.FsmContext)
extern "C"  bool Lexer_State7_m1103110371 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)48))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t2331368794 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t1514038666 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) > ((int32_t)((int32_t)57))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t2331368794 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t1514038666 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		StringBuilder_t1712802186 * L_10 = L_9->get_string_buffer_10();
		FsmContext_t2331368794 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t1514038666 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		NullCheck(L_10);
		StringBuilder_Append_m2383614642(L_10, (((int32_t)((uint16_t)L_13))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_14 = ___ctx0;
		NullCheck(L_14);
		L_14->set_NextState_1(8);
		return (bool)1;
	}

IL_0056:
	{
		FsmContext_t2331368794 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t1514038666 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		V_0 = L_17;
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)43))))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_19 = V_0;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)45))))
		{
			goto IL_0077;
		}
	}
	{
		goto IL_009d;
	}

IL_0077:
	{
		FsmContext_t2331368794 * L_20 = ___ctx0;
		NullCheck(L_20);
		Lexer_t1514038666 * L_21 = L_20->get_L_2();
		NullCheck(L_21);
		StringBuilder_t1712802186 * L_22 = L_21->get_string_buffer_10();
		FsmContext_t2331368794 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t1514038666 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		NullCheck(L_22);
		StringBuilder_Append_m2383614642(L_22, (((int32_t)((uint16_t)L_25))), /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_26 = ___ctx0;
		NullCheck(L_26);
		L_26->set_NextState_1(8);
		return (bool)1;
	}

IL_009d:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State8(LitJson.FsmContext)
extern "C"  bool Lexer_State8_m4126769589 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_00d7;
	}

IL_0005:
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) < ((int32_t)((int32_t)48))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t2331368794 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t1514038666 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) > ((int32_t)((int32_t)57))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t1514038666 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		StringBuilder_t1712802186 * L_8 = L_7->get_string_buffer_10();
		FsmContext_t2331368794 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t1514038666 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		NullCheck(L_8);
		StringBuilder_Append_m2383614642(L_8, (((int32_t)((uint16_t)L_11))), /*hidden argument*/NULL);
		goto IL_00d7;
	}

IL_004b:
	{
		FsmContext_t2331368794 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t1514038666 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) == ((int32_t)((int32_t)32))))
		{
			goto IL_0081;
		}
	}
	{
		FsmContext_t2331368794 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t1514038666 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		if ((((int32_t)L_17) < ((int32_t)((int32_t)9))))
		{
			goto IL_0091;
		}
	}
	{
		FsmContext_t2331368794 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t1514038666 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		if ((((int32_t)L_20) > ((int32_t)((int32_t)13))))
		{
			goto IL_0091;
		}
	}

IL_0081:
	{
		FsmContext_t2331368794 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_22 = ___ctx0;
		NullCheck(L_22);
		L_22->set_NextState_1(1);
		return (bool)1;
	}

IL_0091:
	{
		FsmContext_t2331368794 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t1514038666 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		V_0 = L_25;
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)44))))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)93))))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)125))))
		{
			goto IL_00ba;
		}
	}
	{
		goto IL_00d5;
	}

IL_00ba:
	{
		FsmContext_t2331368794 * L_29 = ___ctx0;
		NullCheck(L_29);
		Lexer_t1514038666 * L_30 = L_29->get_L_2();
		NullCheck(L_30);
		Lexer_UngetChar_m1644879734(L_30, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_31 = ___ctx0;
		NullCheck(L_31);
		L_31->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_32 = ___ctx0;
		NullCheck(L_32);
		L_32->set_NextState_1(1);
		return (bool)1;
	}

IL_00d5:
	{
		return (bool)0;
	}

IL_00d7:
	{
		FsmContext_t2331368794 * L_33 = ___ctx0;
		NullCheck(L_33);
		Lexer_t1514038666 * L_34 = L_33->get_L_2();
		NullCheck(L_34);
		bool L_35 = Lexer_GetChar_m657915143(L_34, /*hidden argument*/NULL);
		if (L_35)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State9(LitJson.FsmContext)
extern "C"  bool Lexer_State9_m3407663359 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)114))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)10));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State10(LitJson.FsmContext)
extern "C"  bool Lexer_State10_m3929854498 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)117))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)11));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State11(LitJson.FsmContext)
extern "C"  bool Lexer_State11_m3893158315 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)101))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0035;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_0035:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State12(LitJson.FsmContext)
extern "C"  bool Lexer_State12_m4025570652 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)97))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)13));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State13(LitJson.FsmContext)
extern "C"  bool Lexer_State13_m3988562965 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)108))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)14));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State14(LitJson.FsmContext)
extern "C"  bool Lexer_State14_m3354309022 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)115))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)15));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State15(LitJson.FsmContext)
extern "C"  bool Lexer_State15_m3741500279 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)101))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0035;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_0035:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State16(LitJson.FsmContext)
extern "C"  bool Lexer_State16_m3863621896 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)117))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)17));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State17(LitJson.FsmContext)
extern "C"  bool Lexer_State17_m3264355937 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)108))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)18));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State18(LitJson.FsmContext)
extern "C"  bool Lexer_State18_m3616214826 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)108))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0035;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_0035:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State19(LitJson.FsmContext)
extern "C"  bool Lexer_State19_m3579281075 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_0076;
	}

IL_0005:
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)34))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)92))))
		{
			goto IL_0042;
		}
	}
	{
		goto IL_0054;
	}

IL_0026:
	{
		FsmContext_t2331368794 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t1514038666 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		Lexer_UngetChar_m1644879734(L_6, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_NextState_1(((int32_t)20));
		return (bool)1;
	}

IL_0042:
	{
		FsmContext_t2331368794 * L_9 = ___ctx0;
		NullCheck(L_9);
		L_9->set_StateStack_3(((int32_t)19));
		FsmContext_t2331368794 * L_10 = ___ctx0;
		NullCheck(L_10);
		L_10->set_NextState_1(((int32_t)21));
		return (bool)1;
	}

IL_0054:
	{
		FsmContext_t2331368794 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t1514038666 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		StringBuilder_t1712802186 * L_13 = L_12->get_string_buffer_10();
		FsmContext_t2331368794 * L_14 = ___ctx0;
		NullCheck(L_14);
		Lexer_t1514038666 * L_15 = L_14->get_L_2();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_input_char_7();
		NullCheck(L_13);
		StringBuilder_Append_m2383614642(L_13, (((int32_t)((uint16_t)L_16))), /*hidden argument*/NULL);
		goto IL_0076;
	}

IL_0076:
	{
		FsmContext_t2331368794 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t1514038666 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		bool L_19 = Lexer_GetChar_m657915143(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State20(LitJson.FsmContext)
extern "C"  bool Lexer_State20_m3151936696 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)34))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0035;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_0035:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State21(LitJson.FsmContext)
extern "C"  bool Lexer_State21_m3115240513 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer_State21_m3115240513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		switch (((int32_t)((int32_t)L_5-(int32_t)((int32_t)114))))
		{
			case 0:
			{
				goto IL_0078;
			}
			case 1:
			{
				goto IL_0031;
			}
			case 2:
			{
				goto IL_0078;
			}
			case 3:
			{
				goto IL_006e;
			}
		}
	}

IL_0031:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)34))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)39))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)47))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)92))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)98))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)102))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)110))))
		{
			goto IL_0078;
		}
	}
	{
		goto IL_00a7;
	}

IL_006e:
	{
		FsmContext_t2331368794 * L_13 = ___ctx0;
		NullCheck(L_13);
		L_13->set_NextState_1(((int32_t)22));
		return (bool)1;
	}

IL_0078:
	{
		FsmContext_t2331368794 * L_14 = ___ctx0;
		NullCheck(L_14);
		Lexer_t1514038666 * L_15 = L_14->get_L_2();
		NullCheck(L_15);
		StringBuilder_t1712802186 * L_16 = L_15->get_string_buffer_10();
		FsmContext_t2331368794 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t1514038666 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_input_char_7();
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		Il2CppChar L_20 = Lexer_ProcessEscChar_m4259284362(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		StringBuilder_Append_m2383614642(L_16, L_20, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_21 = ___ctx0;
		FsmContext_t2331368794 * L_22 = ___ctx0;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_StateStack_3();
		NullCheck(L_21);
		L_21->set_NextState_1(L_23);
		return (bool)1;
	}

IL_00a7:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State22(LitJson.FsmContext)
extern "C"  bool Lexer_State22_m3247652850 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer_State22_m3247652850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = ((int32_t)4096);
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		L_1->set_unichar_13(0);
		goto IL_00ef;
	}

IL_0019:
	{
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)48))))
		{
			goto IL_003d;
		}
	}
	{
		FsmContext_t2331368794 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t1514038666 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)57))))
		{
			goto IL_0085;
		}
	}

IL_003d:
	{
		FsmContext_t2331368794 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t1514038666 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_input_char_7();
		if ((((int32_t)L_10) < ((int32_t)((int32_t)65))))
		{
			goto IL_0061;
		}
	}
	{
		FsmContext_t2331368794 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t1514038666 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		if ((((int32_t)L_13) <= ((int32_t)((int32_t)70))))
		{
			goto IL_0085;
		}
	}

IL_0061:
	{
		FsmContext_t2331368794 * L_14 = ___ctx0;
		NullCheck(L_14);
		Lexer_t1514038666 * L_15 = L_14->get_L_2();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_input_char_7();
		if ((((int32_t)L_16) < ((int32_t)((int32_t)97))))
		{
			goto IL_00ed;
		}
	}
	{
		FsmContext_t2331368794 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t1514038666 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_input_char_7();
		if ((((int32_t)L_19) > ((int32_t)((int32_t)102))))
		{
			goto IL_00ed;
		}
	}

IL_0085:
	{
		FsmContext_t2331368794 * L_20 = ___ctx0;
		NullCheck(L_20);
		Lexer_t1514038666 * L_21 = L_20->get_L_2();
		Lexer_t1514038666 * L_22 = L_21;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_unichar_13();
		FsmContext_t2331368794 * L_24 = ___ctx0;
		NullCheck(L_24);
		Lexer_t1514038666 * L_25 = L_24->get_L_2();
		NullCheck(L_25);
		int32_t L_26 = L_25->get_input_char_7();
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		int32_t L_27 = Lexer_HexValue_m4177870290(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		NullCheck(L_22);
		L_22->set_unichar_13(((int32_t)((int32_t)L_23+(int32_t)((int32_t)((int32_t)L_27*(int32_t)L_28)))));
		int32_t L_29 = V_0;
		V_0 = ((int32_t)((int32_t)L_29+(int32_t)1));
		int32_t L_30 = V_1;
		V_1 = ((int32_t)((int32_t)L_30/(int32_t)((int32_t)16)));
		int32_t L_31 = V_0;
		if ((!(((uint32_t)L_31) == ((uint32_t)4))))
		{
			goto IL_00e8;
		}
	}
	{
		FsmContext_t2331368794 * L_32 = ___ctx0;
		NullCheck(L_32);
		Lexer_t1514038666 * L_33 = L_32->get_L_2();
		NullCheck(L_33);
		StringBuilder_t1712802186 * L_34 = L_33->get_string_buffer_10();
		FsmContext_t2331368794 * L_35 = ___ctx0;
		NullCheck(L_35);
		Lexer_t1514038666 * L_36 = L_35->get_L_2();
		NullCheck(L_36);
		int32_t L_37 = L_36->get_unichar_13();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		Il2CppChar L_38 = Convert_ToChar_m4189066566(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		NullCheck(L_34);
		StringBuilder_Append_m2383614642(L_34, L_38, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_39 = ___ctx0;
		FsmContext_t2331368794 * L_40 = ___ctx0;
		NullCheck(L_40);
		int32_t L_41 = L_40->get_StateStack_3();
		NullCheck(L_39);
		L_39->set_NextState_1(L_41);
		return (bool)1;
	}

IL_00e8:
	{
		goto IL_00ef;
	}

IL_00ed:
	{
		return (bool)0;
	}

IL_00ef:
	{
		FsmContext_t2331368794 * L_42 = ___ctx0;
		NullCheck(L_42);
		Lexer_t1514038666 * L_43 = L_42->get_L_2();
		NullCheck(L_43);
		bool L_44 = Lexer_GetChar_m657915143(L_43, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_0019;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State23(LitJson.FsmContext)
extern "C"  bool Lexer_State23_m3210645163 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_0076;
	}

IL_0005:
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)39))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)92))))
		{
			goto IL_0042;
		}
	}
	{
		goto IL_0054;
	}

IL_0026:
	{
		FsmContext_t2331368794 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t1514038666 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		Lexer_UngetChar_m1644879734(L_6, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_NextState_1(((int32_t)24));
		return (bool)1;
	}

IL_0042:
	{
		FsmContext_t2331368794 * L_9 = ___ctx0;
		NullCheck(L_9);
		L_9->set_StateStack_3(((int32_t)23));
		FsmContext_t2331368794 * L_10 = ___ctx0;
		NullCheck(L_10);
		L_10->set_NextState_1(((int32_t)21));
		return (bool)1;
	}

IL_0054:
	{
		FsmContext_t2331368794 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t1514038666 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		StringBuilder_t1712802186 * L_13 = L_12->get_string_buffer_10();
		FsmContext_t2331368794 * L_14 = ___ctx0;
		NullCheck(L_14);
		Lexer_t1514038666 * L_15 = L_14->get_L_2();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_input_char_7();
		NullCheck(L_13);
		StringBuilder_Append_m2383614642(L_13, (((int32_t)((uint16_t)L_16))), /*hidden argument*/NULL);
		goto IL_0076;
	}

IL_0076:
	{
		FsmContext_t2331368794 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t1514038666 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		bool L_19 = Lexer_GetChar_m657915143(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State24(LitJson.FsmContext)
extern "C"  bool Lexer_State24_m2576391220 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)39))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0042;
	}

IL_0025:
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t1514038666 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		L_7->set_input_char_7(((int32_t)34));
		FsmContext_t2331368794 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_Return_0((bool)1);
		FsmContext_t2331368794 * L_9 = ___ctx0;
		NullCheck(L_9);
		L_9->set_NextState_1(1);
		return (bool)1;
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State25(LitJson.FsmContext)
extern "C"  bool Lexer_State25_m2963582477 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m657915143(L_1, /*hidden argument*/NULL);
		FsmContext_t2331368794 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t1514038666 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)42))))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)47))))
		{
			goto IL_0037;
		}
	}
	{
		goto IL_0041;
	}

IL_002d:
	{
		FsmContext_t2331368794 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(((int32_t)27));
		return (bool)1;
	}

IL_0037:
	{
		FsmContext_t2331368794 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_NextState_1(((int32_t)26));
		return (bool)1;
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State26(LitJson.FsmContext)
extern "C"  bool Lexer_State26_m3085704094 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	{
		goto IL_0020;
	}

IL_0005:
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0020;
		}
	}
	{
		FsmContext_t2331368794 * L_3 = ___ctx0;
		NullCheck(L_3);
		L_3->set_NextState_1(1);
		return (bool)1;
	}

IL_0020:
	{
		FsmContext_t2331368794 * L_4 = ___ctx0;
		NullCheck(L_4);
		Lexer_t1514038666 * L_5 = L_4->get_L_2();
		NullCheck(L_5);
		bool L_6 = Lexer_GetChar_m657915143(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State27(LitJson.FsmContext)
extern "C"  bool Lexer_State27_m2486438135 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	{
		goto IL_0021;
	}

IL_0005:
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)42)))))
		{
			goto IL_0021;
		}
	}
	{
		FsmContext_t2331368794 * L_3 = ___ctx0;
		NullCheck(L_3);
		L_3->set_NextState_1(((int32_t)28));
		return (bool)1;
	}

IL_0021:
	{
		FsmContext_t2331368794 * L_4 = ___ctx0;
		NullCheck(L_4);
		Lexer_t1514038666 * L_5 = L_4->get_L_2();
		NullCheck(L_5);
		bool L_6 = Lexer_GetChar_m657915143(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State28(LitJson.FsmContext)
extern "C"  bool Lexer_State28_m2838297024 (Il2CppObject * __this /* static, unused */, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	{
		goto IL_0041;
	}

IL_0005:
	{
		FsmContext_t2331368794 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t1514038666 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)42)))))
		{
			goto IL_001c;
		}
	}
	{
		goto IL_0041;
	}

IL_001c:
	{
		FsmContext_t2331368794 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t1514038666 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0037;
		}
	}
	{
		FsmContext_t2331368794 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(1);
		return (bool)1;
	}

IL_0037:
	{
		FsmContext_t2331368794 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(((int32_t)27));
		return (bool)1;
	}

IL_0041:
	{
		FsmContext_t2331368794 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t1514038666 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		bool L_10 = Lexer_GetChar_m657915143(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::GetChar()
extern "C"  bool Lexer_GetChar_m657915143 (Lexer_t1514038666 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Lexer_NextChar_m2082205690(__this, /*hidden argument*/NULL);
		int32_t L_1 = L_0;
		V_0 = L_1;
		__this->set_input_char_7(L_1);
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		return (bool)1;
	}

IL_0017:
	{
		__this->set_end_of_input_4((bool)1);
		return (bool)0;
	}
}
// System.Int32 LitJson.Lexer::NextChar()
extern "C"  int32_t Lexer_NextChar_m2082205690 (Lexer_t1514038666 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_input_buffer_6();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = __this->get_input_buffer_6();
		V_0 = L_1;
		__this->set_input_buffer_6(0);
		int32_t L_2 = V_0;
		return L_2;
	}

IL_001b:
	{
		TextReader_t283511965 * L_3 = __this->get_reader_8();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_3);
		return L_4;
	}
}
// System.Boolean LitJson.Lexer::NextToken()
extern "C"  bool Lexer_NextToken_m2425003787 (Lexer_t1514038666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lexer_NextToken_m2425003787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StateHandler_t105866779 * V_0 = NULL;
	{
		FsmContext_t2331368794 * L_0 = __this->get_fsm_context_5();
		NullCheck(L_0);
		L_0->set_Return_0((bool)0);
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		StateHandlerU5BU5D_t1323790106* L_1 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_fsm_handler_table_1();
		int32_t L_2 = __this->get_state_9();
		NullCheck(L_1);
		int32_t L_3 = ((int32_t)((int32_t)L_2-(int32_t)1));
		StateHandler_t105866779 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		StateHandler_t105866779 * L_5 = V_0;
		FsmContext_t2331368794 * L_6 = __this->get_fsm_context_5();
		NullCheck(L_5);
		bool L_7 = StateHandler_Invoke_m2992258958(L_5, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_8 = __this->get_input_char_7();
		JsonException_t3682484112 * L_9 = (JsonException_t3682484112 *)il2cpp_codegen_object_new(JsonException_t3682484112_il2cpp_TypeInfo_var);
		JsonException__ctor_m2210441219(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0038:
	{
		bool L_10 = __this->get_end_of_input_4();
		if (!L_10)
		{
			goto IL_0045;
		}
	}
	{
		return (bool)0;
	}

IL_0045:
	{
		FsmContext_t2331368794 * L_11 = __this->get_fsm_context_5();
		NullCheck(L_11);
		bool L_12 = L_11->get_Return_0();
		if (!L_12)
		{
			goto IL_00c1;
		}
	}
	{
		StringBuilder_t1712802186 * L_13 = __this->get_string_buffer_10();
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		__this->set_string_value_11(L_14);
		StringBuilder_t1712802186 * L_15 = __this->get_string_buffer_10();
		StringBuilder_t1712802186 * L_16 = __this->get_string_buffer_10();
		NullCheck(L_16);
		int32_t L_17 = StringBuilder_get_Length_m3238060835(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		StringBuilder_Remove_m940064945(L_15, 0, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t1514038666_il2cpp_TypeInfo_var);
		Int32U5BU5D_t385246372* L_18 = ((Lexer_t1514038666_StaticFields*)Lexer_t1514038666_il2cpp_TypeInfo_var->static_fields)->get_fsm_return_table_0();
		int32_t L_19 = __this->get_state_9();
		NullCheck(L_18);
		int32_t L_20 = ((int32_t)((int32_t)L_19-(int32_t)1));
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		__this->set_token_12(L_21);
		int32_t L_22 = __this->get_token_12();
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)65542)))))
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_23 = __this->get_input_char_7();
		__this->set_token_12(L_23);
	}

IL_00ae:
	{
		FsmContext_t2331368794 * L_24 = __this->get_fsm_context_5();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_NextState_1();
		__this->set_state_9(L_25);
		return (bool)1;
	}

IL_00c1:
	{
		FsmContext_t2331368794 * L_26 = __this->get_fsm_context_5();
		NullCheck(L_26);
		int32_t L_27 = L_26->get_NextState_1();
		__this->set_state_9(L_27);
		goto IL_000c;
	}
}
// System.Void LitJson.Lexer::UngetChar()
extern "C"  void Lexer_UngetChar_m1644879734 (Lexer_t1514038666 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_input_char_7();
		__this->set_input_buffer_6(L_0);
		return;
	}
}
// System.Void LitJson.Lexer/StateHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void StateHandler__ctor_m2721920384 (StateHandler_t105866779 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean LitJson.Lexer/StateHandler::Invoke(LitJson.FsmContext)
extern "C"  bool StateHandler_Invoke_m2992258958 (StateHandler_t105866779 * __this, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateHandler_Invoke_m2992258958((StateHandler_t105866779 *)__this->get_prev_9(),___ctx0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ctx0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, FsmContext_t2331368794 * ___ctx0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ctx0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___ctx0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.Lexer/StateHandler::BeginInvoke(LitJson.FsmContext,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StateHandler_BeginInvoke_m504595634 (StateHandler_t105866779 * __this, FsmContext_t2331368794 * ___ctx0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___ctx0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean LitJson.Lexer/StateHandler::EndInvoke(System.IAsyncResult)
extern "C"  bool StateHandler_EndInvoke_m1267321198 (StateHandler_t105866779 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Conversion methods for marshalling of: LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t3566284522_marshal_pinvoke(const ObjectMetadata_t3566284522& unmarshaled, ObjectMetadata_t3566284522_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ObjectMetadata_t3566284522_marshal_pinvoke_back(const ObjectMetadata_t3566284522_marshaled_pinvoke& marshaled, ObjectMetadata_t3566284522& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t3566284522_marshal_pinvoke_cleanup(ObjectMetadata_t3566284522_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t3566284522_marshal_com(const ObjectMetadata_t3566284522& unmarshaled, ObjectMetadata_t3566284522_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ObjectMetadata_t3566284522_marshal_com_back(const ObjectMetadata_t3566284522_marshaled_com& marshaled, ObjectMetadata_t3566284522& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t3566284522_marshal_com_cleanup(ObjectMetadata_t3566284522_marshaled_com& marshaled)
{
}
// System.Type LitJson.ObjectMetadata::get_ElementType()
extern "C"  Type_t * ObjectMetadata_get_ElementType_m991775292 (ObjectMetadata_t3566284522 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectMetadata_get_ElementType_m991775292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = __this->get_element_type_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(JsonData_t1524858407_0_0_0_var), /*hidden argument*/NULL);
		return L_1;
	}

IL_0016:
	{
		Type_t * L_2 = __this->get_element_type_0();
		return L_2;
	}
}
extern "C"  Type_t * ObjectMetadata_get_ElementType_m991775292_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ObjectMetadata_t3566284522 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t3566284522 *>(__this + 1);
	return ObjectMetadata_get_ElementType_m991775292(_thisAdjusted, method);
}
// System.Void LitJson.ObjectMetadata::set_ElementType(System.Type)
extern "C"  void ObjectMetadata_set_ElementType_m1776330551 (ObjectMetadata_t3566284522 * __this, Type_t * ___value0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set_element_type_0(L_0);
		return;
	}
}
extern "C"  void ObjectMetadata_set_ElementType_m1776330551_AdjustorThunk (Il2CppObject * __this, Type_t * ___value0, const MethodInfo* method)
{
	ObjectMetadata_t3566284522 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t3566284522 *>(__this + 1);
	ObjectMetadata_set_ElementType_m1776330551(_thisAdjusted, ___value0, method);
}
// System.Boolean LitJson.ObjectMetadata::get_IsDictionary()
extern "C"  bool ObjectMetadata_get_IsDictionary_m1062828228 (ObjectMetadata_t3566284522 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_is_dictionary_1();
		return L_0;
	}
}
extern "C"  bool ObjectMetadata_get_IsDictionary_m1062828228_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ObjectMetadata_t3566284522 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t3566284522 *>(__this + 1);
	return ObjectMetadata_get_IsDictionary_m1062828228(_thisAdjusted, method);
}
// System.Void LitJson.ObjectMetadata::set_IsDictionary(System.Boolean)
extern "C"  void ObjectMetadata_set_IsDictionary_m2176094566 (ObjectMetadata_t3566284522 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_is_dictionary_1(L_0);
		return;
	}
}
extern "C"  void ObjectMetadata_set_IsDictionary_m2176094566_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	ObjectMetadata_t3566284522 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t3566284522 *>(__this + 1);
	ObjectMetadata_set_IsDictionary_m2176094566(_thisAdjusted, ___value0, method);
}
// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata> LitJson.ObjectMetadata::get_Properties()
extern "C"  Il2CppObject* ObjectMetadata_get_Properties_m3723938162 (ObjectMetadata_t3566284522 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = __this->get_properties_2();
		return L_0;
	}
}
extern "C"  Il2CppObject* ObjectMetadata_get_Properties_m3723938162_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ObjectMetadata_t3566284522 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t3566284522 *>(__this + 1);
	return ObjectMetadata_get_Properties_m3723938162(_thisAdjusted, method);
}
// System.Void LitJson.ObjectMetadata::set_Properties(System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>)
extern "C"  void ObjectMetadata_set_Properties_m3670768576 (ObjectMetadata_t3566284522 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___value0;
		__this->set_properties_2(L_0);
		return;
	}
}
extern "C"  void ObjectMetadata_set_Properties_m3670768576_AdjustorThunk (Il2CppObject * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	ObjectMetadata_t3566284522 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t3566284522 *>(__this + 1);
	ObjectMetadata_set_Properties_m3670768576(_thisAdjusted, ___value0, method);
}
// System.Object LitJson.OrderedDictionaryEnumerator::get_Current()
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Current_m1860742452 (OrderedDictionaryEnumerator_t386339177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Current_m1860742452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DictionaryEntry_t3123975638  L_0 = OrderedDictionaryEnumerator_get_Entry_m2900755332(__this, /*hidden argument*/NULL);
		DictionaryEntry_t3123975638  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3123975638_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.DictionaryEntry LitJson.OrderedDictionaryEnumerator::get_Entry()
extern "C"  DictionaryEntry_t3123975638  OrderedDictionaryEnumerator_get_Entry_m2900755332 (OrderedDictionaryEnumerator_t386339177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Entry_m2900755332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3707786873  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		KeyValuePair_2_t3707786873  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t3707786873  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Current() */, IEnumerator_1_t4140357341_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		String_t* L_2 = KeyValuePair_2_get_Key_m913646779((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m913646779_MethodInfo_var);
		JsonData_t1524858407 * L_3 = KeyValuePair_2_get_Value_m4274503699((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m4274503699_MethodInfo_var);
		DictionaryEntry_t3123975638  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m2585376310(&L_4, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object LitJson.OrderedDictionaryEnumerator::get_Key()
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Key_m3653726345 (OrderedDictionaryEnumerator_t386339177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Key_m3653726345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3707786873  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		KeyValuePair_2_t3707786873  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t3707786873  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Current() */, IEnumerator_1_t4140357341_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		String_t* L_2 = KeyValuePair_2_get_Key_m913646779((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m913646779_MethodInfo_var);
		return L_2;
	}
}
// System.Object LitJson.OrderedDictionaryEnumerator::get_Value()
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Value_m2497007576 (OrderedDictionaryEnumerator_t386339177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Value_m2497007576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3707786873  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		KeyValuePair_2_t3707786873  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t3707786873  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Current() */, IEnumerator_1_t4140357341_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		JsonData_t1524858407 * L_2 = KeyValuePair_2_get_Value_m4274503699((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m4274503699_MethodInfo_var);
		return L_2;
	}
}
// System.Void LitJson.OrderedDictionaryEnumerator::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>)
extern "C"  void OrderedDictionaryEnumerator__ctor_m3267133839 (OrderedDictionaryEnumerator_t386339177 * __this, Il2CppObject* ___enumerator0, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___enumerator0;
		__this->set_list_enumerator_0(L_0);
		return;
	}
}
// System.Boolean LitJson.OrderedDictionaryEnumerator::MoveNext()
extern "C"  bool OrderedDictionaryEnumerator_MoveNext_m1132456593 (OrderedDictionaryEnumerator_t386339177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_MoveNext_m1132456593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void LitJson.OrderedDictionaryEnumerator::Reset()
extern "C"  void OrderedDictionaryEnumerator_Reset_m1965424276 (OrderedDictionaryEnumerator_t386339177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_Reset_m1965424276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// Conversion methods for marshalling of: LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t3727440473_marshal_pinvoke(const PropertyMetadata_t3727440473& unmarshaled, PropertyMetadata_t3727440473_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
extern "C" void PropertyMetadata_t3727440473_marshal_pinvoke_back(const PropertyMetadata_t3727440473_marshaled_pinvoke& marshaled, PropertyMetadata_t3727440473& unmarshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t3727440473_marshal_pinvoke_cleanup(PropertyMetadata_t3727440473_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t3727440473_marshal_com(const PropertyMetadata_t3727440473& unmarshaled, PropertyMetadata_t3727440473_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
extern "C" void PropertyMetadata_t3727440473_marshal_com_back(const PropertyMetadata_t3727440473_marshaled_com& marshaled, PropertyMetadata_t3727440473& unmarshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t3727440473_marshal_com_cleanup(PropertyMetadata_t3727440473_marshaled_com& marshaled)
{
}
// System.Void LitJson.WrapperFactory::.ctor(System.Object,System.IntPtr)
extern "C"  void WrapperFactory__ctor_m283261507 (WrapperFactory_t2158548929 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// LitJson.IJsonWrapper LitJson.WrapperFactory::Invoke()
extern "C"  Il2CppObject * WrapperFactory_Invoke_m1494838554 (WrapperFactory_t2158548929 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WrapperFactory_Invoke_m1494838554((WrapperFactory_t2158548929 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.WrapperFactory::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WrapperFactory_BeginInvoke_m1633161851 (WrapperFactory_t2158548929 * __this, AsyncCallback_t3962456242 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// LitJson.IJsonWrapper LitJson.WrapperFactory::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * WrapperFactory_EndInvoke_m966658048 (WrapperFactory_t2158548929 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void LitJson.WriterContext::.ctor()
extern "C"  void WriterContext__ctor_m2317705773 (WriterContext_t1011093999 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
