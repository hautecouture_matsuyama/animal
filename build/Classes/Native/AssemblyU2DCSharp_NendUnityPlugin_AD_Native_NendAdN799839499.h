﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// NendUnityPlugin.AD.Native.NendAdNativeView
struct NendAdNativeView_t2014609480;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.NendAdNative/<RegisterAdView>c__AnonStorey0
struct  U3CRegisterAdViewU3Ec__AnonStorey0_t799839499  : public Il2CppObject
{
public:
	// NendUnityPlugin.AD.Native.NendAdNativeView NendUnityPlugin.AD.Native.NendAdNative/<RegisterAdView>c__AnonStorey0::view
	NendAdNativeView_t2014609480 * ___view_0;

public:
	inline static int32_t get_offset_of_view_0() { return static_cast<int32_t>(offsetof(U3CRegisterAdViewU3Ec__AnonStorey0_t799839499, ___view_0)); }
	inline NendAdNativeView_t2014609480 * get_view_0() const { return ___view_0; }
	inline NendAdNativeView_t2014609480 ** get_address_of_view_0() { return &___view_0; }
	inline void set_view_0(NendAdNativeView_t2014609480 * value)
	{
		___view_0 = value;
		Il2CppCodeGenWriteBarrier(&___view_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
