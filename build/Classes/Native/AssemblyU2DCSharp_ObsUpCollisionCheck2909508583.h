﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObsUpCollisionCheck
struct  ObsUpCollisionCheck_t2909508583  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean ObsUpCollisionCheck::isDead
	bool ___isDead_2;

public:
	inline static int32_t get_offset_of_isDead_2() { return static_cast<int32_t>(offsetof(ObsUpCollisionCheck_t2909508583, ___isDead_2)); }
	inline bool get_isDead_2() const { return ___isDead_2; }
	inline bool* get_address_of_isDead_2() { return &___isDead_2; }
	inline void set_isDead_2(bool value)
	{
		___isDead_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
