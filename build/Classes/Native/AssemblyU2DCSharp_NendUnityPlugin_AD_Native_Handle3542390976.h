﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.Camera
struct Camera_t4157153871;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI/<IsViewable>c__AnonStorey0
struct  U3CIsViewableU3Ec__AnonStorey0_t3542390976  : public Il2CppObject
{
public:
	// UnityEngine.Camera NendUnityPlugin.AD.Native.Handlers.ImpressionHandlerUI/<IsViewable>c__AnonStorey0::camera
	Camera_t4157153871 * ___camera_0;

public:
	inline static int32_t get_offset_of_camera_0() { return static_cast<int32_t>(offsetof(U3CIsViewableU3Ec__AnonStorey0_t3542390976, ___camera_0)); }
	inline Camera_t4157153871 * get_camera_0() const { return ___camera_0; }
	inline Camera_t4157153871 ** get_address_of_camera_0() { return &___camera_0; }
	inline void set_camera_0(Camera_t4157153871 * value)
	{
		___camera_0 = value;
		Il2CppCodeGenWriteBarrier(&___camera_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
