﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Video_NendAdV1781712043.h"

// NendUnityPlugin.AD.Video.NendAdRewardedVideo/NendAdVideoRewarded
struct NendAdVideoRewarded_t687064503;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Video.NendAdRewardedVideo
struct  NendAdRewardedVideo_t3161671531  : public NendAdVideo_t1781712043
{
public:
	// NendUnityPlugin.AD.Video.NendAdRewardedVideo/NendAdVideoRewarded NendUnityPlugin.AD.Video.NendAdRewardedVideo::Rewarded
	NendAdVideoRewarded_t687064503 * ___Rewarded_10;

public:
	inline static int32_t get_offset_of_Rewarded_10() { return static_cast<int32_t>(offsetof(NendAdRewardedVideo_t3161671531, ___Rewarded_10)); }
	inline NendAdVideoRewarded_t687064503 * get_Rewarded_10() const { return ___Rewarded_10; }
	inline NendAdVideoRewarded_t687064503 ** get_address_of_Rewarded_10() { return &___Rewarded_10; }
	inline void set_Rewarded_10(NendAdVideoRewarded_t687064503 * value)
	{
		___Rewarded_10 = value;
		Il2CppCodeGenWriteBarrier(&___Rewarded_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
