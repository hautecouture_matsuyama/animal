﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache
struct TextureCache_t3323732960;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>
struct Dictionary_2_t3625702484;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache
struct  TextureCache_t3323732960  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D> NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache::m_Cache
	Dictionary_2_t3625702484 * ___m_Cache_1;

public:
	inline static int32_t get_offset_of_m_Cache_1() { return static_cast<int32_t>(offsetof(TextureCache_t3323732960, ___m_Cache_1)); }
	inline Dictionary_2_t3625702484 * get_m_Cache_1() const { return ___m_Cache_1; }
	inline Dictionary_2_t3625702484 ** get_address_of_m_Cache_1() { return &___m_Cache_1; }
	inline void set_m_Cache_1(Dictionary_2_t3625702484 * value)
	{
		___m_Cache_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Cache_1, value);
	}
};

struct TextureCache_t3323732960_StaticFields
{
public:
	// NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache NendUnityPlugin.AD.Native.Utils.TextureLoader/TextureCache::s_Instance
	TextureCache_t3323732960 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TextureCache_t3323732960_StaticFields, ___s_Instance_0)); }
	inline TextureCache_t3323732960 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TextureCache_t3323732960 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TextureCache_t3323732960 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_Instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
