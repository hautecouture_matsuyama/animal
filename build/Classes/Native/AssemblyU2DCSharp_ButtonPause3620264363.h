﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonPause
struct  ButtonPause_t3620264363  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ButtonPause::pause_screen
	GameObject_t1113636619 * ___pause_screen_2;

public:
	inline static int32_t get_offset_of_pause_screen_2() { return static_cast<int32_t>(offsetof(ButtonPause_t3620264363, ___pause_screen_2)); }
	inline GameObject_t1113636619 * get_pause_screen_2() const { return ___pause_screen_2; }
	inline GameObject_t1113636619 ** get_address_of_pause_screen_2() { return &___pause_screen_2; }
	inline void set_pause_screen_2(GameObject_t1113636619 * value)
	{
		___pause_screen_2 = value;
		Il2CppCodeGenWriteBarrier(&___pause_screen_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
