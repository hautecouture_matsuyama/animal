﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "mscorlib_System_Action_1_gen269755560.h"
#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Boolean97287965.h"
#include "mscorlib_System_AsyncCallback3962456242.h"
#include "mscorlib_System_Action_1_gen3252573759.h"
#include "System_Core_System_Action_2_gen2523487705.h"
#include "System_Core_System_Action_2_gen2470008838.h"
#include "System_Core_System_Action_3_gen1173861992.h"
#include "mscorlib_System_Int322950945753.h"
#include "System_Core_System_Action_3_gen3632554945.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4107749331.h"
#include "mscorlib_System_UInt322560061978.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4270623997.h"
#include "mscorlib_System_NotSupportedException1314879016.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1315508877.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgu287865710.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1478383543.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3750793324.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg2723150157.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen3913667990.h"
#include "mscorlib_System_String1847450689.h"
#include "mscorlib_System_ArgumentOutOfRangeException777629997.h"
#include "mscorlib_System_Exception1436737249.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1801353056.h"
#include "LitJson_LitJson_ArrayMetadata894288939.h"
#include "mscorlib_System_InvalidOperationException56020091.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen178381343.h"
#include "LitJson_LitJson_ObjectMetadata3566284522.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen339537294.h"
#include "LitJson_LitJson_PropertyMetadata3727440473.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4239932009.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex3332867892.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1911769025.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1004704908.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen380507487.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_Common_Gravity3768410666.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1004352082.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2041360493.h"
#include "mscorlib_System_Byte1134296376.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246557291.h"
#include "mscorlib_System_Char3634460470.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4031039755.h"
#include "mscorlib_System_Collections_DictionaryEntry3123975638.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4116331090.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3209266973.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen978588483.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g71524366.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1251464211.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_344400094.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3923459794.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23016395677.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4084615745.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23177551628.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen454463237.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23842366416.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen951840635.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g44776518.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3308121025.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22401056908.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3437281436.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22530217319.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1451382081.h"
#include "mscorlib_System_Collections_Generic_Link544317964.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen587985571.h"
#include "mscorlib_System_Collections_Hashtable_Slot3975888750.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1291559127.h"
#include "mscorlib_System_Collections_SortedList_Slot384495010.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen350626606.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3855323497.h"
#include "mscorlib_System_Decimal2948259380.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1501729480.h"
#include "mscorlib_System_Double594665363.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3459884504.h"
#include "mscorlib_System_Int162552820387.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3858009870.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen348664125.h"
#include "mscorlib_System_Int643736567304.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1747214298.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3987170281.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1194929827.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3630214274.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1267231508.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelDa360167391.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1765566171.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFi858502054.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3232839231.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo2325775114.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2368758583.h"
#include "mscorlib_System_Reflection_ParameterModifier1461694466.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen958356908.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCac51292791.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3780029419.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI2872965302.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen153918522.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3541821701.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2576641779.h"
#include "mscorlib_System_SByte1669577662.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1040666831.h"
#include "System_System_Security_Cryptography_X509Certificate133602714.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2304330891.h"
#include "mscorlib_System_Single1397266774.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen83702344.h"
#include "System_System_Text_RegularExpressions_Mark3471605523.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1788223366.h"
#include "mscorlib_System_TimeSpan881159249.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3084789075.h"
#include "mscorlib_System_UInt162177724958.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3467126095.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen746136913.h"
#include "mscorlib_System_UInt644134040092.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1629489814.h"
#include "System_System_Uri_UriScheme722425697.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4063781272.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3156717155.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3507565409.h"
#include "UnityEngine_UnityEngine_Color322600501292.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen370852074.h"
#include "UnityEngine_UnityEngine_ContactPoint3758755253.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2337465.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3390240644.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4267370966.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3360306849.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen818507063.h"
#include "UnityEngine_UnityEngine_Keyframe4206410242.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1963066083.h"
#include "UnityEngine_UnityEngine_RaycastHit1056001966.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3186646106.h"
#include "UnityEngine_UnityEngine_RaycastHit2D2279581989.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3267543976.h"
#include "UnityEngine_UnityEngine_Rect2360479859.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4136673857.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3229609740.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1582286363.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_675222246.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3032373948.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2125309831.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2828920985.h"
#include "UnityEngine_UnityEngine_Touch1921856868.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2694367513.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1787303396.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen982565223.h"
#include "UnityEngine_UnityEngine_UICharInfo75501106.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen807363631.h"
#include "UnityEngine_UnityEngine_UILineInfo4195266810.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen669594426.h"
#include "UnityEngine_UnityEngine_UIVertex4057497605.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3063293640.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen334410285.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4226093054.h"
#include "UnityEngine_UnityEngine_Vector43319028937.h"
#include "mscorlib_System_Collections_Generic_CollectionDebu1274252988.h"
#include "mscorlib_System_Collections_Generic_CollectionDebug720968023.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa382820652.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen4029972201.h"
#include "mscorlib_System_ArgumentException132251570.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1591133118.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen943317371.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1081890840.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen434075093.h"
#include "mscorlib_System_DateTimeOffset3229287507.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1046136220.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen398320473.h"
#include "mscorlib_System_Guid3193532887.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa803549086.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen155733339.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa932709497.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen284893750.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2435436339.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1787620592.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa575753490.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen4222905039.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3028729878.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2380914131.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1009320488.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen361504741.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa453104625.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen4100256174.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1212910182.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen565094435.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa213083192.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3860234741.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2223071735.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1575255988.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2047870143.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1400054396.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1910100938.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1262285191.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defaul8832856.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3655984405.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1574916797.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen927101050.h"

// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2523487705;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2470008838;
// System.Action`3<System.Object,System.Int32,System.Object>
struct Action_3_t1173861992;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t3632554945;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t4107749331;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t1315508877;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t3750793324;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t4270623997;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t777629997;
// System.String
struct String_t;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t3512676632;
// System.Exception
struct Exception_t1436737249;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t1478383543;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3710464795;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t720436178;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t3913667990;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1465843424;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3155720625;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct DefaultComparer_t382820652;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t1591133118;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t1081890840;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t1046136220;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t803549086;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
struct DefaultComparer_t932709497;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>
struct DefaultComparer_t2435436339;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct DefaultComparer_t575753490;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t3028729878;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.AnimatorClipInfo>
struct DefaultComparer_t1009320488;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>
struct DefaultComparer_t453104625;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t1212910182;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Rect>
struct DefaultComparer_t213083192;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t2223071735;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t2047870143;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t1910100938;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>
struct DefaultComparer_t8832856;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>
struct DefaultComparer_t1574916797;
extern Il2CppClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1817882028_MetadataUsageId;
extern const uint32_t Action_2_BeginInvoke_m1990245223_MetadataUsageId;
extern Il2CppClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m1898539978_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m3837913694_MetadataUsageId;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2500457056_MetadataUsageId;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1192421843_MetadataUsageId;
extern Il2CppClass* ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral797640427;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2988101436_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3853944826;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m1047641207_MetadataUsageId;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m1974867852_MetadataUsageId;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m3555240367_MetadataUsageId;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m4135188594_MetadataUsageId;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m865416608_MetadataUsageId;
extern Il2CppClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2287514662;
extern Il2CppCodeGenString* _stringLiteral3056411131;
extern const uint32_t InternalEnumerator_1_get_Current_m2746872357_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2608927213_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2597556806_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m4245242303_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1708547365_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3879265245_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2100201398_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3073360606_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1945804797_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1920303382_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1845246162_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m4241643334_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3775994610_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m4006695586_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3781046_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m476140818_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1679394243_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3081223448_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m923139215_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2223614542_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3008260692_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2832154098_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3225386639_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2128158355_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2894466703_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2723520268_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3500427238_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m685192625_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3900374024_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3839250771_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3354878040_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3653231044_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2698009637_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2680116177_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m165106323_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m356936020_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m144365666_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3191242573_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3911557813_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1408339225_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2389908135_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2612852447_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1909182215_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1698047500_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m784835552_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1897120917_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1588647567_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1007906068_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m348524980_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1232221964_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2315302778_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1523142935_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m254780543_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2907722321_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m114240259_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1655128652_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3941815949_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m4124877207_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3628030453_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3609142494_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1836170281_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2350635577_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3792939945_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1070921822_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1534474313_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3331252162_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1477715453_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2356858238_MetadataUsageId;
extern Il2CppClass* IComparable_t36111218_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4135314742;
extern const uint32_t DefaultComparer_Compare_m348911778_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m732589824_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m655397166_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m3591589106_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m1297750557_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m4042058291_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m982533255_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m947823904_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m3967426329_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m1658087432_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m3278268937_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m1920986590_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m543097977_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m3931992727_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m1916473435_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m1932373082_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m3648806637_MetadataUsageId;
extern const uint32_t DefaultComparer_Compare_m1369122336_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t2843939325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3710464795  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeNamedArgument_t287865710  m_Items[1];

public:
	inline CustomAttributeNamedArgument_t287865710  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t287865710 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeNamedArgument_t287865710  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeNamedArgument_t287865710  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t287865710 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeNamedArgument_t287865710  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1465843424  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeTypedArgument_t2723150157  m_Items[1];

public:
	inline CustomAttributeTypedArgument_t2723150157  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t2723150157 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeTypedArgument_t2723150157  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeTypedArgument_t2723150157  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t2723150157 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeTypedArgument_t2723150157  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3635202442_gshared (Action_1_t269755560 * __this, bool ___obj0, const MethodInfo* method);
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m2461023210_gshared (Action_1_t3252573759 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1763453775_gshared (Action_2_t2523487705 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m406858745_gshared (Action_2_t2470008838 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
// System.Void System.Action`3<System.Object,System.Int32,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3429238884_gshared (Action_3_t1173861992 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m1376317295_gshared (Action_3_t3632554945 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2980597965_gshared (InternalEnumerator_1_t1801353056 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1394395022_gshared (InternalEnumerator_1_t1801353056 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::get_Current()
extern "C"  ArrayMetadata_t894288939  InternalEnumerator_1_get_Current_m2746872357_gshared (InternalEnumerator_1_t1801353056 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3121310385_gshared (InternalEnumerator_1_t1801353056 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3200291779_gshared (InternalEnumerator_1_t1801353056 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1439724028_gshared (InternalEnumerator_1_t1801353056 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3193665258_gshared (InternalEnumerator_1_t178381343 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2006725664_gshared (InternalEnumerator_1_t178381343 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::get_Current()
extern "C"  ObjectMetadata_t3566284522  InternalEnumerator_1_get_Current_m2608927213_gshared (InternalEnumerator_1_t178381343 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m177159026_gshared (InternalEnumerator_1_t178381343 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2717850677_gshared (InternalEnumerator_1_t178381343 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2752374182_gshared (InternalEnumerator_1_t178381343 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3337644433_gshared (InternalEnumerator_1_t339537294 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1694348991_gshared (InternalEnumerator_1_t339537294 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::get_Current()
extern "C"  PropertyMetadata_t3727440473  InternalEnumerator_1_get_Current_m2597556806_gshared (InternalEnumerator_1_t339537294 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3865845908_gshared (InternalEnumerator_1_t339537294 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1606295072_gshared (InternalEnumerator_1_t339537294 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m299856312_gshared (InternalEnumerator_1_t339537294 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1359891754_gshared (InternalEnumerator_1_t4239932009 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m81420524_gshared (InternalEnumerator_1_t4239932009 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern "C"  TableRange_t3332867892  InternalEnumerator_1_get_Current_m4245242303_gshared (InternalEnumerator_1_t4239932009 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2980550840_gshared (InternalEnumerator_1_t4239932009 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m33109155_gshared (InternalEnumerator_1_t4239932009 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4138845038_gshared (InternalEnumerator_1_t4239932009 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3734861738_gshared (InternalEnumerator_1_t1911769025 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2707779927_gshared (InternalEnumerator_1_t1911769025 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1708547365_gshared (InternalEnumerator_1_t1911769025 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m55999184_gshared (InternalEnumerator_1_t1911769025 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2447779733_gshared (InternalEnumerator_1_t1911769025 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2850975202_gshared (InternalEnumerator_1_t1911769025 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m320219217_gshared (InternalEnumerator_1_t380507487 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3916136950_gshared (InternalEnumerator_1_t380507487 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3879265245_gshared (InternalEnumerator_1_t380507487 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m159594892_gshared (InternalEnumerator_1_t380507487 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1212749330_gshared (InternalEnumerator_1_t380507487 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3280316341_gshared (InternalEnumerator_1_t380507487 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3349908318_gshared (InternalEnumerator_1_t1004352082 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m903423974_gshared (InternalEnumerator_1_t1004352082 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern "C"  bool InternalEnumerator_1_get_Current_m2100201398_gshared (InternalEnumerator_1_t1004352082 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1503522504_gshared (InternalEnumerator_1_t1004352082 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2018798800_gshared (InternalEnumerator_1_t1004352082 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m154749640_gshared (InternalEnumerator_1_t1004352082 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4191108945_gshared (InternalEnumerator_1_t2041360493 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3327951435_gshared (InternalEnumerator_1_t2041360493 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m3073360606_gshared (InternalEnumerator_1_t2041360493 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1277470738_gshared (InternalEnumerator_1_t2041360493 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3493290831_gshared (InternalEnumerator_1_t2041360493 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m123458112_gshared (InternalEnumerator_1_t2041360493 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2123683127_gshared (InternalEnumerator_1_t246557291 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2793870849_gshared (InternalEnumerator_1_t246557291 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m1945804797_gshared (InternalEnumerator_1_t246557291 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1641466962_gshared (InternalEnumerator_1_t246557291 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m881342307_gshared (InternalEnumerator_1_t246557291 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1909384544_gshared (InternalEnumerator_1_t246557291 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2336656763_gshared (InternalEnumerator_1_t4031039755 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2336872218_gshared (InternalEnumerator_1_t4031039755 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern "C"  DictionaryEntry_t3123975638  InternalEnumerator_1_get_Current_m1920303382_gshared (InternalEnumerator_1_t4031039755 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1295084274_gshared (InternalEnumerator_1_t4031039755 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2648133761_gshared (InternalEnumerator_1_t4031039755 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2577879725_gshared (InternalEnumerator_1_t4031039755 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m619554185_gshared (InternalEnumerator_1_t4116331090 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3677481164_gshared (InternalEnumerator_1_t4116331090 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern "C"  Link_t3209266973  InternalEnumerator_1_get_Current_m1845246162_gshared (InternalEnumerator_1_t4116331090 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2867624895_gshared (InternalEnumerator_1_t4116331090 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m326441406_gshared (InternalEnumerator_1_t4116331090 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1457790320_gshared (InternalEnumerator_1_t4116331090 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1424655733_gshared (InternalEnumerator_1_t978588483 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1318888374_gshared (InternalEnumerator_1_t978588483 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t71524366  InternalEnumerator_1_get_Current_m4241643334_gshared (InternalEnumerator_1_t978588483 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3575233890_gshared (InternalEnumerator_1_t978588483 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m435531507_gshared (InternalEnumerator_1_t978588483 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1543390728_gshared (InternalEnumerator_1_t978588483 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1816464321_gshared (InternalEnumerator_1_t1251464211 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3362997282_gshared (InternalEnumerator_1_t1251464211 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::get_Current()
extern "C"  KeyValuePair_2_t344400094  InternalEnumerator_1_get_Current_m3775994610_gshared (InternalEnumerator_1_t1251464211 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m616568955_gshared (InternalEnumerator_1_t1251464211 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1298284700_gshared (InternalEnumerator_1_t1251464211 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2711395376_gshared (InternalEnumerator_1_t1251464211 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4107498329_gshared (InternalEnumerator_1_t3923459794 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2625604376_gshared (InternalEnumerator_1_t3923459794 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::get_Current()
extern "C"  KeyValuePair_2_t3016395677  InternalEnumerator_1_get_Current_m4006695586_gshared (InternalEnumerator_1_t3923459794 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4197773997_gshared (InternalEnumerator_1_t3923459794 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3269441358_gshared (InternalEnumerator_1_t3923459794 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2156158931_gshared (InternalEnumerator_1_t3923459794 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3117895283_gshared (InternalEnumerator_1_t4084615745 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871276011_gshared (InternalEnumerator_1_t4084615745 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::get_Current()
extern "C"  KeyValuePair_2_t3177551628  InternalEnumerator_1_get_Current_m3781046_gshared (InternalEnumerator_1_t4084615745 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1716856402_gshared (InternalEnumerator_1_t4084615745 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3566201632_gshared (InternalEnumerator_1_t4084615745 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2542460092_gshared (InternalEnumerator_1_t4084615745 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m962177456_gshared (InternalEnumerator_1_t454463237 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1716381123_gshared (InternalEnumerator_1_t454463237 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern "C"  KeyValuePair_2_t3842366416  InternalEnumerator_1_get_Current_m476140818_gshared (InternalEnumerator_1_t454463237 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2889979481_gshared (InternalEnumerator_1_t454463237 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1290015243_gshared (InternalEnumerator_1_t454463237 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3299696349_gshared (InternalEnumerator_1_t454463237 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3402055178_gshared (InternalEnumerator_1_t951840635 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4046360160_gshared (InternalEnumerator_1_t951840635 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::get_Current()
extern "C"  KeyValuePair_2_t44776518  InternalEnumerator_1_get_Current_m1679394243_gshared (InternalEnumerator_1_t951840635 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2328184924_gshared (InternalEnumerator_1_t951840635 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m537193559_gshared (InternalEnumerator_1_t951840635 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2000685912_gshared (InternalEnumerator_1_t951840635 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1341209356_gshared (InternalEnumerator_1_t3308121025 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4239728915_gshared (InternalEnumerator_1_t3308121025 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t2401056908  InternalEnumerator_1_get_Current_m3081223448_gshared (InternalEnumerator_1_t3308121025 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2284280372_gshared (InternalEnumerator_1_t3308121025 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4098771594_gshared (InternalEnumerator_1_t3308121025 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2951889983_gshared (InternalEnumerator_1_t3308121025 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m807987550_gshared (InternalEnumerator_1_t3437281436 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2386791007_gshared (InternalEnumerator_1_t3437281436 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2530217319  InternalEnumerator_1_get_Current_m923139215_gshared (InternalEnumerator_1_t3437281436 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141782011_gshared (InternalEnumerator_1_t3437281436 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2342933386_gshared (InternalEnumerator_1_t3437281436 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1773160976_gshared (InternalEnumerator_1_t3437281436 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3390957028_gshared (InternalEnumerator_1_t1451382081 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1223176161_gshared (InternalEnumerator_1_t1451382081 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern "C"  Link_t544317964  InternalEnumerator_1_get_Current_m2223614542_gshared (InternalEnumerator_1_t1451382081 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1676501075_gshared (InternalEnumerator_1_t1451382081 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1913545470_gshared (InternalEnumerator_1_t1451382081 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2785895009_gshared (InternalEnumerator_1_t1451382081 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1196506529_gshared (InternalEnumerator_1_t587985571 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1256724261_gshared (InternalEnumerator_1_t587985571 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern "C"  Slot_t3975888750  InternalEnumerator_1_get_Current_m3008260692_gshared (InternalEnumerator_1_t587985571 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1514266661_gshared (InternalEnumerator_1_t587985571 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1995846647_gshared (InternalEnumerator_1_t587985571 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3200332883_gshared (InternalEnumerator_1_t587985571 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m819716934_gshared (InternalEnumerator_1_t1291559127 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4202665280_gshared (InternalEnumerator_1_t1291559127 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern "C"  Slot_t384495010  InternalEnumerator_1_get_Current_m2832154098_gshared (InternalEnumerator_1_t1291559127 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2629988057_gshared (InternalEnumerator_1_t1291559127 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1519877610_gshared (InternalEnumerator_1_t1291559127 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3354536447_gshared (InternalEnumerator_1_t1291559127 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3456047704_gshared (InternalEnumerator_1_t350626606 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m546509994_gshared (InternalEnumerator_1_t350626606 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern "C"  DateTime_t3738529785  InternalEnumerator_1_get_Current_m3225386639_gshared (InternalEnumerator_1_t350626606 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2742943179_gshared (InternalEnumerator_1_t350626606 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m649519051_gshared (InternalEnumerator_1_t350626606 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1161444633_gshared (InternalEnumerator_1_t350626606 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m143506773_gshared (InternalEnumerator_1_t3855323497 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m169899350_gshared (InternalEnumerator_1_t3855323497 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern "C"  Decimal_t2948259380  InternalEnumerator_1_get_Current_m2128158355_gshared (InternalEnumerator_1_t3855323497 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m688818811_gshared (InternalEnumerator_1_t3855323497 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m937653815_gshared (InternalEnumerator_1_t3855323497 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4210671224_gshared (InternalEnumerator_1_t3855323497 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1559487635_gshared (InternalEnumerator_1_t1501729480 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14983211_gshared (InternalEnumerator_1_t1501729480 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern "C"  double InternalEnumerator_1_get_Current_m2894466703_gshared (InternalEnumerator_1_t1501729480 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3853320011_gshared (InternalEnumerator_1_t1501729480 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2043273260_gshared (InternalEnumerator_1_t1501729480 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m377783729_gshared (InternalEnumerator_1_t1501729480 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2910272776_gshared (InternalEnumerator_1_t3459884504 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m521819017_gshared (InternalEnumerator_1_t3459884504 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern "C"  int16_t InternalEnumerator_1_get_Current_m2723520268_gshared (InternalEnumerator_1_t3459884504 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888718134_gshared (InternalEnumerator_1_t3459884504 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3716424577_gshared (InternalEnumerator_1_t3459884504 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3525157932_gshared (InternalEnumerator_1_t3459884504 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m217498388_gshared (InternalEnumerator_1_t3858009870 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m779787360_gshared (InternalEnumerator_1_t3858009870 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3500427238_gshared (InternalEnumerator_1_t3858009870 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2435291801_gshared (InternalEnumerator_1_t3858009870 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3519406884_gshared (InternalEnumerator_1_t3858009870 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3802174768_gshared (InternalEnumerator_1_t3858009870 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4055378331_gshared (InternalEnumerator_1_t348664125 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3873091784_gshared (InternalEnumerator_1_t348664125 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C"  int64_t InternalEnumerator_1_get_Current_m685192625_gshared (InternalEnumerator_1_t348664125 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m180319738_gshared (InternalEnumerator_1_t348664125 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m830510730_gshared (InternalEnumerator_1_t348664125 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4266213580_gshared (InternalEnumerator_1_t348664125 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1579105305_gshared (InternalEnumerator_1_t1747214298 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3011999097_gshared (InternalEnumerator_1_t1747214298 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m3900374024_gshared (InternalEnumerator_1_t1747214298 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m548105685_gshared (InternalEnumerator_1_t1747214298 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2818366163_gshared (InternalEnumerator_1_t1747214298 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3520556285_gshared (InternalEnumerator_1_t1747214298 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1675719794_gshared (InternalEnumerator_1_t3987170281 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2914412419_gshared (InternalEnumerator_1_t3987170281 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m3839250771_gshared (InternalEnumerator_1_t3987170281 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2982675020_gshared (InternalEnumerator_1_t3987170281 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2006926799_gshared (InternalEnumerator_1_t3987170281 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4035695998_gshared (InternalEnumerator_1_t3987170281 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m643493702_gshared (InternalEnumerator_1_t1194929827 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m895873066_gshared (InternalEnumerator_1_t1194929827 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t287865710  InternalEnumerator_1_get_Current_m3354878040_gshared (InternalEnumerator_1_t1194929827 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4189894603_gshared (InternalEnumerator_1_t1194929827 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3577625655_gshared (InternalEnumerator_1_t1194929827 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4138204635_gshared (InternalEnumerator_1_t1194929827 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m748741755_gshared (InternalEnumerator_1_t3630214274 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m596870847_gshared (InternalEnumerator_1_t3630214274 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t2723150157  InternalEnumerator_1_get_Current_m3653231044_gshared (InternalEnumerator_1_t3630214274 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4213507601_gshared (InternalEnumerator_1_t3630214274 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2438347491_gshared (InternalEnumerator_1_t3630214274 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3577491700_gshared (InternalEnumerator_1_t3630214274 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1486034688_gshared (InternalEnumerator_1_t1267231508 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3619766341_gshared (InternalEnumerator_1_t1267231508 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
extern "C"  LabelData_t360167391  InternalEnumerator_1_get_Current_m2698009637_gshared (InternalEnumerator_1_t1267231508 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m435848551_gshared (InternalEnumerator_1_t1267231508 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m106460639_gshared (InternalEnumerator_1_t1267231508 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1728532725_gshared (InternalEnumerator_1_t1267231508 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1594304290_gshared (InternalEnumerator_1_t1765566171 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m720350288_gshared (InternalEnumerator_1_t1765566171 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern "C"  LabelFixup_t858502054  InternalEnumerator_1_get_Current_m2680116177_gshared (InternalEnumerator_1_t1765566171 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1976902927_gshared (InternalEnumerator_1_t1765566171 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1063909490_gshared (InternalEnumerator_1_t1765566171 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2234422530_gshared (InternalEnumerator_1_t1765566171 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m238559784_gshared (InternalEnumerator_1_t3232839231 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2428767548_gshared (InternalEnumerator_1_t3232839231 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern "C"  ILTokenInfo_t2325775114  InternalEnumerator_1_get_Current_m165106323_gshared (InternalEnumerator_1_t3232839231 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3624751851_gshared (InternalEnumerator_1_t3232839231 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m130608859_gshared (InternalEnumerator_1_t3232839231 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m819973544_gshared (InternalEnumerator_1_t3232839231 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m39232262_gshared (InternalEnumerator_1_t2368758583 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3165277182_gshared (InternalEnumerator_1_t2368758583 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern "C"  ParameterModifier_t1461694466  InternalEnumerator_1_get_Current_m356936020_gshared (InternalEnumerator_1_t2368758583 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m694606607_gshared (InternalEnumerator_1_t2368758583 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2840529825_gshared (InternalEnumerator_1_t2368758583 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3664960764_gshared (InternalEnumerator_1_t2368758583 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1999141680_gshared (InternalEnumerator_1_t958356908 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1259718730_gshared (InternalEnumerator_1_t958356908 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::get_Current()
extern "C"  ResourceCacheItem_t51292791  InternalEnumerator_1_get_Current_m144365666_gshared (InternalEnumerator_1_t958356908 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141016822_gshared (InternalEnumerator_1_t958356908 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m887344916_gshared (InternalEnumerator_1_t958356908 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2960571514_gshared (InternalEnumerator_1_t958356908 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2908852803_gshared (InternalEnumerator_1_t3780029419 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4008893642_gshared (InternalEnumerator_1_t3780029419 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::get_Current()
extern "C"  ResourceInfo_t2872965302  InternalEnumerator_1_get_Current_m3191242573_gshared (InternalEnumerator_1_t3780029419 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4235876088_gshared (InternalEnumerator_1_t3780029419 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m725544411_gshared (InternalEnumerator_1_t3780029419 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3174983217_gshared (InternalEnumerator_1_t3780029419 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4200721464_gshared (InternalEnumerator_1_t153918522 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2234754688_gshared (InternalEnumerator_1_t153918522 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m3911557813_gshared (InternalEnumerator_1_t153918522 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3892960115_gshared (InternalEnumerator_1_t153918522 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3983612351_gshared (InternalEnumerator_1_t153918522 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1020308708_gshared (InternalEnumerator_1_t153918522 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3460713284_gshared (InternalEnumerator_1_t2576641779 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3545912565_gshared (InternalEnumerator_1_t2576641779 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C"  int8_t InternalEnumerator_1_get_Current_m1408339225_gshared (InternalEnumerator_1_t2576641779 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m767948013_gshared (InternalEnumerator_1_t2576641779 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1406845627_gshared (InternalEnumerator_1_t2576641779 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1015797184_gshared (InternalEnumerator_1_t2576641779 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3443175323_gshared (InternalEnumerator_1_t1040666831 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2314612291_gshared (InternalEnumerator_1_t1040666831 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern "C"  X509ChainStatus_t133602714  InternalEnumerator_1_get_Current_m2389908135_gshared (InternalEnumerator_1_t1040666831 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1096730143_gshared (InternalEnumerator_1_t1040666831 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m469889800_gshared (InternalEnumerator_1_t1040666831 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1732823414_gshared (InternalEnumerator_1_t1040666831 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m31115849_gshared (InternalEnumerator_1_t2304330891 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181721336_gshared (InternalEnumerator_1_t2304330891 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern "C"  float InternalEnumerator_1_get_Current_m2612852447_gshared (InternalEnumerator_1_t2304330891 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1823542095_gshared (InternalEnumerator_1_t2304330891 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m630370856_gshared (InternalEnumerator_1_t2304330891 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3134701632_gshared (InternalEnumerator_1_t2304330891 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1122952091_gshared (InternalEnumerator_1_t83702344 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m396346696_gshared (InternalEnumerator_1_t83702344 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern "C"  Mark_t3471605523  InternalEnumerator_1_get_Current_m1909182215_gshared (InternalEnumerator_1_t83702344 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088805473_gshared (InternalEnumerator_1_t83702344 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1837758743_gshared (InternalEnumerator_1_t83702344 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4133541970_gshared (InternalEnumerator_1_t83702344 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3261326277_gshared (InternalEnumerator_1_t1788223366 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3763767775_gshared (InternalEnumerator_1_t1788223366 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
extern "C"  TimeSpan_t881159249  InternalEnumerator_1_get_Current_m1698047500_gshared (InternalEnumerator_1_t1788223366 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1299775605_gshared (InternalEnumerator_1_t1788223366 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4260521517_gshared (InternalEnumerator_1_t1788223366 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3958061110_gshared (InternalEnumerator_1_t1788223366 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2202456613_gshared (InternalEnumerator_1_t3084789075 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3848218235_gshared (InternalEnumerator_1_t3084789075 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern "C"  uint16_t InternalEnumerator_1_get_Current_m784835552_gshared (InternalEnumerator_1_t3084789075 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m191386315_gshared (InternalEnumerator_1_t3084789075 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m359678482_gshared (InternalEnumerator_1_t3084789075 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m973048327_gshared (InternalEnumerator_1_t3084789075 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3383770493_gshared (InternalEnumerator_1_t3467126095 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m342565588_gshared (InternalEnumerator_1_t3467126095 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern "C"  uint32_t InternalEnumerator_1_get_Current_m1897120917_gshared (InternalEnumerator_1_t3467126095 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612699335_gshared (InternalEnumerator_1_t3467126095 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1121538879_gshared (InternalEnumerator_1_t3467126095 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3855324972_gshared (InternalEnumerator_1_t3467126095 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3215746182_gshared (InternalEnumerator_1_t746136913 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m872612294_gshared (InternalEnumerator_1_t746136913 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern "C"  uint64_t InternalEnumerator_1_get_Current_m1588647567_gshared (InternalEnumerator_1_t746136913 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2395961985_gshared (InternalEnumerator_1_t746136913 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3637184090_gshared (InternalEnumerator_1_t746136913 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1975820803_gshared (InternalEnumerator_1_t746136913 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m362401472_gshared (InternalEnumerator_1_t1629489814 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4196663616_gshared (InternalEnumerator_1_t1629489814 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern "C"  UriScheme_t722425697  InternalEnumerator_1_get_Current_m1007906068_gshared (InternalEnumerator_1_t1629489814 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2671801110_gshared (InternalEnumerator_1_t1629489814 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m648941584_gshared (InternalEnumerator_1_t1629489814 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m190587569_gshared (InternalEnumerator_1_t1629489814 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4044013350_gshared (InternalEnumerator_1_t4063781272 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4107572074_gshared (InternalEnumerator_1_t4063781272 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::get_Current()
extern "C"  AnimatorClipInfo_t3156717155  InternalEnumerator_1_get_Current_m348524980_gshared (InternalEnumerator_1_t4063781272 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1911422882_gshared (InternalEnumerator_1_t4063781272 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m969498836_gshared (InternalEnumerator_1_t4063781272 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4074044800_gshared (InternalEnumerator_1_t4063781272 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m539509188_gshared (InternalEnumerator_1_t3507565409 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m996811230_gshared (InternalEnumerator_1_t3507565409 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
extern "C"  Color32_t2600501292  InternalEnumerator_1_get_Current_m1232221964_gshared (InternalEnumerator_1_t3507565409 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4196752819_gshared (InternalEnumerator_1_t3507565409 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3259955982_gshared (InternalEnumerator_1_t3507565409 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1590908934_gshared (InternalEnumerator_1_t3507565409 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1352157576_gshared (InternalEnumerator_1_t370852074 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1009155168_gshared (InternalEnumerator_1_t370852074 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
extern "C"  ContactPoint_t3758755253  InternalEnumerator_1_get_Current_m2315302778_gshared (InternalEnumerator_1_t370852074 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m629296715_gshared (InternalEnumerator_1_t370852074 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m756188704_gshared (InternalEnumerator_1_t370852074 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1187868016_gshared (InternalEnumerator_1_t370852074 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1270930859_gshared (InternalEnumerator_1_t2337465 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m500203932_gshared (InternalEnumerator_1_t2337465 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::get_Current()
extern "C"  ContactPoint2D_t3390240644  InternalEnumerator_1_get_Current_m1523142935_gshared (InternalEnumerator_1_t2337465 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2358118467_gshared (InternalEnumerator_1_t2337465 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1306615151_gshared (InternalEnumerator_1_t2337465 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1174937354_gshared (InternalEnumerator_1_t2337465 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2311732727_gshared (InternalEnumerator_1_t4267370966 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1686642781_gshared (InternalEnumerator_1_t4267370966 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C"  RaycastResult_t3360306849  InternalEnumerator_1_get_Current_m254780543_gshared (InternalEnumerator_1_t4267370966 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m923624021_gshared (InternalEnumerator_1_t4267370966 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3517794894_gshared (InternalEnumerator_1_t4267370966 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1344185775_gshared (InternalEnumerator_1_t4267370966 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1744883412_gshared (InternalEnumerator_1_t818507063 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3217592429_gshared (InternalEnumerator_1_t818507063 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern "C"  Keyframe_t4206410242  InternalEnumerator_1_get_Current_m2907722321_gshared (InternalEnumerator_1_t818507063 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m605068928_gshared (InternalEnumerator_1_t818507063 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3211169941_gshared (InternalEnumerator_1_t818507063 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1109261117_gshared (InternalEnumerator_1_t818507063 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m615777089_gshared (InternalEnumerator_1_t1963066083 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2621383412_gshared (InternalEnumerator_1_t1963066083 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
extern "C"  RaycastHit_t1056001966  InternalEnumerator_1_get_Current_m114240259_gshared (InternalEnumerator_1_t1963066083 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1635397542_gshared (InternalEnumerator_1_t1963066083 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m958164402_gshared (InternalEnumerator_1_t1963066083 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3164144724_gshared (InternalEnumerator_1_t1963066083 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2635640285_gshared (InternalEnumerator_1_t3186646106 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1775752715_gshared (InternalEnumerator_1_t3186646106 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern "C"  RaycastHit2D_t2279581989  InternalEnumerator_1_get_Current_m1655128652_gshared (InternalEnumerator_1_t3186646106 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2424959150_gshared (InternalEnumerator_1_t3186646106 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m555942266_gshared (InternalEnumerator_1_t3186646106 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2734554195_gshared (InternalEnumerator_1_t3186646106 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3848674141_gshared (InternalEnumerator_1_t3267543976 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2556670574_gshared (InternalEnumerator_1_t3267543976 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.Rect>::get_Current()
extern "C"  Rect_t2360479859  InternalEnumerator_1_get_Current_m3941815949_gshared (InternalEnumerator_1_t3267543976 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rect>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2690353642_gshared (InternalEnumerator_1_t3267543976 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1453932905_gshared (InternalEnumerator_1_t3267543976 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rect>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3804980664_gshared (InternalEnumerator_1_t3267543976 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3913006324_gshared (InternalEnumerator_1_t4136673857 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1451164462_gshared (InternalEnumerator_1_t4136673857 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern "C"  HitInfo_t3229609740  InternalEnumerator_1_get_Current_m4124877207_gshared (InternalEnumerator_1_t4136673857 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2032951142_gshared (InternalEnumerator_1_t4136673857 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3977286481_gshared (InternalEnumerator_1_t4136673857 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2088624192_gshared (InternalEnumerator_1_t4136673857 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3535695642_gshared (InternalEnumerator_1_t1582286363 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1918396835_gshared (InternalEnumerator_1_t1582286363 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern "C"  GcAchievementData_t675222246  InternalEnumerator_1_get_Current_m3628030453_gshared (InternalEnumerator_1_t1582286363 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m943285433_gshared (InternalEnumerator_1_t1582286363 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4174463085_gshared (InternalEnumerator_1_t1582286363 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4151310216_gshared (InternalEnumerator_1_t1582286363 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m258868363_gshared (InternalEnumerator_1_t3032373948 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4039902941_gshared (InternalEnumerator_1_t3032373948 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern "C"  GcScoreData_t2125309831  InternalEnumerator_1_get_Current_m3609142494_gshared (InternalEnumerator_1_t3032373948 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2582019288_gshared (InternalEnumerator_1_t3032373948 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3187018662_gshared (InternalEnumerator_1_t3032373948 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1036267697_gshared (InternalEnumerator_1_t3032373948 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m63033851_gshared (InternalEnumerator_1_t2828920985 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4226416995_gshared (InternalEnumerator_1_t2828920985 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.Touch>::get_Current()
extern "C"  Touch_t1921856868  InternalEnumerator_1_get_Current_m1836170281_gshared (InternalEnumerator_1_t2828920985 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2499755184_gshared (InternalEnumerator_1_t2828920985 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1432840524_gshared (InternalEnumerator_1_t2828920985 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Touch>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m300199243_gshared (InternalEnumerator_1_t2828920985 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1739091604_gshared (InternalEnumerator_1_t2694367513 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1341907970_gshared (InternalEnumerator_1_t2694367513 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2350635577_gshared (InternalEnumerator_1_t2694367513 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164048921_gshared (InternalEnumerator_1_t2694367513 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m28687982_gshared (InternalEnumerator_1_t2694367513 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1555187632_gshared (InternalEnumerator_1_t2694367513 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4175030001_gshared (InternalEnumerator_1_t982565223 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2305395628_gshared (InternalEnumerator_1_t982565223 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
extern "C"  UICharInfo_t75501106  InternalEnumerator_1_get_Current_m3792939945_gshared (InternalEnumerator_1_t982565223 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2649471526_gshared (InternalEnumerator_1_t982565223 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3790132913_gshared (InternalEnumerator_1_t982565223 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3713722659_gshared (InternalEnumerator_1_t982565223 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3322594868_gshared (InternalEnumerator_1_t807363631 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1601477281_gshared (InternalEnumerator_1_t807363631 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::get_Current()
extern "C"  UILineInfo_t4195266810  InternalEnumerator_1_get_Current_m1070921822_gshared (InternalEnumerator_1_t807363631 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3553395619_gshared (InternalEnumerator_1_t807363631 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2788308318_gshared (InternalEnumerator_1_t807363631 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1396448578_gshared (InternalEnumerator_1_t807363631 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2490839835_gshared (InternalEnumerator_1_t669594426 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4072625129_gshared (InternalEnumerator_1_t669594426 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::get_Current()
extern "C"  UIVertex_t4057497605  InternalEnumerator_1_get_Current_m1534474313_gshared (InternalEnumerator_1_t669594426 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2209458050_gshared (InternalEnumerator_1_t669594426 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m365545176_gshared (InternalEnumerator_1_t669594426 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3686444574_gshared (InternalEnumerator_1_t669594426 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m945079686_gshared (InternalEnumerator_1_t3063293640 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m158730371_gshared (InternalEnumerator_1_t3063293640 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern "C"  Vector2_t2156229523  InternalEnumerator_1_get_Current_m3331252162_gshared (InternalEnumerator_1_t3063293640 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1847780851_gshared (InternalEnumerator_1_t3063293640 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m663714168_gshared (InternalEnumerator_1_t3063293640 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1262669372_gshared (InternalEnumerator_1_t3063293640 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4132027968_gshared (InternalEnumerator_1_t334410285 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1916984356_gshared (InternalEnumerator_1_t334410285 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t3722313464  InternalEnumerator_1_get_Current_m1477715453_gshared (InternalEnumerator_1_t334410285 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3537550566_gshared (InternalEnumerator_1_t334410285 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2188147046_gshared (InternalEnumerator_1_t334410285 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector3>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1262906009_gshared (InternalEnumerator_1_t334410285 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2307827786_gshared (InternalEnumerator_1_t4226093054 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1944844050_gshared (InternalEnumerator_1_t4226093054 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<UnityEngine.Vector4>::get_Current()
extern "C"  Vector4_t3319028937  InternalEnumerator_1_get_Current_m2356858238_gshared (InternalEnumerator_1_t4226093054 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3982010935_gshared (InternalEnumerator_1_t4226093054 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m987068791_gshared (InternalEnumerator_1_t4226093054 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector4>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1258813334_gshared (InternalEnumerator_1_t4226093054 * __this, const MethodInfo* method);

// System.Void System.Action`1<System.Boolean>::Invoke(T)
#define Action_1_Invoke_m3635202442(__this, ___obj0, method) ((  void (*) (Action_1_t269755560 *, bool, const MethodInfo*))Action_1_Invoke_m3635202442_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<System.Object>::Invoke(T)
#define Action_1_Invoke_m2461023210(__this, ___obj0, method) ((  void (*) (Action_1_t3252573759 *, Il2CppObject *, const MethodInfo*))Action_1_Invoke_m2461023210_gshared)(__this, ___obj0, method)
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
#define Action_2_Invoke_m1763453775(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2523487705 *, bool, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m1763453775_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
#define Action_2_Invoke_m406858745(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2470008838 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m406858745_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`3<System.Object,System.Int32,System.Object>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m3429238884(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t1173861992 *, Il2CppObject *, int32_t, Il2CppObject *, const MethodInfo*))Action_3_Invoke_m3429238884_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m1376317295(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t3632554945 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Action_3_Invoke_m1376317295_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m3628145864 (ArgumentOutOfRangeException_t777629997 * __this, String_t* ___paramName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::CopyTo(System.Array,System.Int32)
extern "C"  void Array_CopyTo_m225704097 (Il2CppArray * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor(System.String)
extern "C"  void NotSupportedException__ctor_m2494070935 (NotSupportedException_t1314879016 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2980597965(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1801353056 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2980597965_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1394395022(__this, method) ((  void (*) (InternalEnumerator_1_t1801353056 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1394395022_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::get_Current()
#define InternalEnumerator_1_get_Current_m2746872357(__this, method) ((  ArrayMetadata_t894288939  (*) (InternalEnumerator_1_t1801353056 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2746872357_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3121310385(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1801353056 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3121310385_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::Dispose()
#define InternalEnumerator_1_Dispose_m3200291779(__this, method) ((  void (*) (InternalEnumerator_1_t1801353056 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3200291779_gshared)(__this, method)
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m21610649 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1439724028(__this, method) ((  bool (*) (InternalEnumerator_1_t1801353056 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1439724028_gshared)(__this, method)
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m237278729 (InvalidOperationException_t56020091 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3193665258(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t178381343 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3193665258_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2006725664(__this, method) ((  void (*) (InternalEnumerator_1_t178381343 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2006725664_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::get_Current()
#define InternalEnumerator_1_get_Current_m2608927213(__this, method) ((  ObjectMetadata_t3566284522  (*) (InternalEnumerator_1_t178381343 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2608927213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m177159026(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t178381343 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m177159026_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::Dispose()
#define InternalEnumerator_1_Dispose_m2717850677(__this, method) ((  void (*) (InternalEnumerator_1_t178381343 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2717850677_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2752374182(__this, method) ((  bool (*) (InternalEnumerator_1_t178381343 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2752374182_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3337644433(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t339537294 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3337644433_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1694348991(__this, method) ((  void (*) (InternalEnumerator_1_t339537294 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1694348991_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::get_Current()
#define InternalEnumerator_1_get_Current_m2597556806(__this, method) ((  PropertyMetadata_t3727440473  (*) (InternalEnumerator_1_t339537294 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2597556806_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3865845908(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t339537294 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3865845908_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::Dispose()
#define InternalEnumerator_1_Dispose_m1606295072(__this, method) ((  void (*) (InternalEnumerator_1_t339537294 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1606295072_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::MoveNext()
#define InternalEnumerator_1_MoveNext_m299856312(__this, method) ((  bool (*) (InternalEnumerator_1_t339537294 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m299856312_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1359891754(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4239932009 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1359891754_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m81420524(__this, method) ((  void (*) (InternalEnumerator_1_t4239932009 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m81420524_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
#define InternalEnumerator_1_get_Current_m4245242303(__this, method) ((  TableRange_t3332867892  (*) (InternalEnumerator_1_t4239932009 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4245242303_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2980550840(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4239932009 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2980550840_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
#define InternalEnumerator_1_Dispose_m33109155(__this, method) ((  void (*) (InternalEnumerator_1_t4239932009 *, const MethodInfo*))InternalEnumerator_1_Dispose_m33109155_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4138845038(__this, method) ((  bool (*) (InternalEnumerator_1_t4239932009 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4138845038_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3734861738(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1911769025 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3734861738_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2707779927(__this, method) ((  void (*) (InternalEnumerator_1_t1911769025 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2707779927_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
#define InternalEnumerator_1_get_Current_m1708547365(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1911769025 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1708547365_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m55999184(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1911769025 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m55999184_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
#define InternalEnumerator_1_Dispose_m2447779733(__this, method) ((  void (*) (InternalEnumerator_1_t1911769025 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2447779733_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2850975202(__this, method) ((  bool (*) (InternalEnumerator_1_t1911769025 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2850975202_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m320219217(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t380507487 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m320219217_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3916136950(__this, method) ((  void (*) (InternalEnumerator_1_t380507487 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3916136950_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::get_Current()
#define InternalEnumerator_1_get_Current_m3879265245(__this, method) ((  int32_t (*) (InternalEnumerator_1_t380507487 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3879265245_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m159594892(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t380507487 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m159594892_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::Dispose()
#define InternalEnumerator_1_Dispose_m1212749330(__this, method) ((  void (*) (InternalEnumerator_1_t380507487 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1212749330_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3280316341(__this, method) ((  bool (*) (InternalEnumerator_1_t380507487 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3280316341_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3349908318(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1004352082 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3349908318_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m903423974(__this, method) ((  void (*) (InternalEnumerator_1_t1004352082 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m903423974_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
#define InternalEnumerator_1_get_Current_m2100201398(__this, method) ((  bool (*) (InternalEnumerator_1_t1004352082 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2100201398_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1503522504(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1004352082 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1503522504_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
#define InternalEnumerator_1_Dispose_m2018798800(__this, method) ((  void (*) (InternalEnumerator_1_t1004352082 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2018798800_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
#define InternalEnumerator_1_MoveNext_m154749640(__this, method) ((  bool (*) (InternalEnumerator_1_t1004352082 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m154749640_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4191108945(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2041360493 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4191108945_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3327951435(__this, method) ((  void (*) (InternalEnumerator_1_t2041360493 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3327951435_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
#define InternalEnumerator_1_get_Current_m3073360606(__this, method) ((  uint8_t (*) (InternalEnumerator_1_t2041360493 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3073360606_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1277470738(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2041360493 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1277470738_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
#define InternalEnumerator_1_Dispose_m3493290831(__this, method) ((  void (*) (InternalEnumerator_1_t2041360493 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3493290831_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
#define InternalEnumerator_1_MoveNext_m123458112(__this, method) ((  bool (*) (InternalEnumerator_1_t2041360493 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m123458112_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2123683127(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t246557291 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2123683127_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2793870849(__this, method) ((  void (*) (InternalEnumerator_1_t246557291 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2793870849_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
#define InternalEnumerator_1_get_Current_m1945804797(__this, method) ((  Il2CppChar (*) (InternalEnumerator_1_t246557291 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1945804797_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1641466962(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t246557291 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1641466962_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
#define InternalEnumerator_1_Dispose_m881342307(__this, method) ((  void (*) (InternalEnumerator_1_t246557291 *, const MethodInfo*))InternalEnumerator_1_Dispose_m881342307_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1909384544(__this, method) ((  bool (*) (InternalEnumerator_1_t246557291 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1909384544_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2336656763(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4031039755 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2336656763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2336872218(__this, method) ((  void (*) (InternalEnumerator_1_t4031039755 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2336872218_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
#define InternalEnumerator_1_get_Current_m1920303382(__this, method) ((  DictionaryEntry_t3123975638  (*) (InternalEnumerator_1_t4031039755 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1920303382_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1295084274(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4031039755 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1295084274_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
#define InternalEnumerator_1_Dispose_m2648133761(__this, method) ((  void (*) (InternalEnumerator_1_t4031039755 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2648133761_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2577879725(__this, method) ((  bool (*) (InternalEnumerator_1_t4031039755 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2577879725_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m619554185(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4116331090 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m619554185_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3677481164(__this, method) ((  void (*) (InternalEnumerator_1_t4116331090 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3677481164_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
#define InternalEnumerator_1_get_Current_m1845246162(__this, method) ((  Link_t3209266973  (*) (InternalEnumerator_1_t4116331090 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1845246162_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2867624895(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4116331090 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2867624895_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
#define InternalEnumerator_1_Dispose_m326441406(__this, method) ((  void (*) (InternalEnumerator_1_t4116331090 *, const MethodInfo*))InternalEnumerator_1_Dispose_m326441406_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1457790320(__this, method) ((  bool (*) (InternalEnumerator_1_t4116331090 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1457790320_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1424655733(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t978588483 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1424655733_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1318888374(__this, method) ((  void (*) (InternalEnumerator_1_t978588483 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1318888374_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
#define InternalEnumerator_1_get_Current_m4241643334(__this, method) ((  KeyValuePair_2_t71524366  (*) (InternalEnumerator_1_t978588483 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4241643334_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3575233890(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t978588483 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3575233890_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
#define InternalEnumerator_1_Dispose_m435531507(__this, method) ((  void (*) (InternalEnumerator_1_t978588483 *, const MethodInfo*))InternalEnumerator_1_Dispose_m435531507_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1543390728(__this, method) ((  bool (*) (InternalEnumerator_1_t978588483 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1543390728_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1816464321(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1251464211 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1816464321_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3362997282(__this, method) ((  void (*) (InternalEnumerator_1_t1251464211 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3362997282_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::get_Current()
#define InternalEnumerator_1_get_Current_m3775994610(__this, method) ((  KeyValuePair_2_t344400094  (*) (InternalEnumerator_1_t1251464211 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3775994610_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m616568955(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1251464211 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m616568955_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::Dispose()
#define InternalEnumerator_1_Dispose_m1298284700(__this, method) ((  void (*) (InternalEnumerator_1_t1251464211 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1298284700_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2711395376(__this, method) ((  bool (*) (InternalEnumerator_1_t1251464211 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2711395376_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4107498329(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3923459794 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4107498329_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2625604376(__this, method) ((  void (*) (InternalEnumerator_1_t3923459794 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2625604376_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::get_Current()
#define InternalEnumerator_1_get_Current_m4006695586(__this, method) ((  KeyValuePair_2_t3016395677  (*) (InternalEnumerator_1_t3923459794 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4006695586_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4197773997(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3923459794 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4197773997_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::Dispose()
#define InternalEnumerator_1_Dispose_m3269441358(__this, method) ((  void (*) (InternalEnumerator_1_t3923459794 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3269441358_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2156158931(__this, method) ((  bool (*) (InternalEnumerator_1_t3923459794 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2156158931_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3117895283(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4084615745 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3117895283_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871276011(__this, method) ((  void (*) (InternalEnumerator_1_t4084615745 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871276011_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::get_Current()
#define InternalEnumerator_1_get_Current_m3781046(__this, method) ((  KeyValuePair_2_t3177551628  (*) (InternalEnumerator_1_t4084615745 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3781046_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1716856402(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4084615745 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1716856402_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::Dispose()
#define InternalEnumerator_1_Dispose_m3566201632(__this, method) ((  void (*) (InternalEnumerator_1_t4084615745 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3566201632_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2542460092(__this, method) ((  bool (*) (InternalEnumerator_1_t4084615745 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2542460092_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m962177456(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t454463237 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m962177456_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1716381123(__this, method) ((  void (*) (InternalEnumerator_1_t454463237 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1716381123_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
#define InternalEnumerator_1_get_Current_m476140818(__this, method) ((  KeyValuePair_2_t3842366416  (*) (InternalEnumerator_1_t454463237 *, const MethodInfo*))InternalEnumerator_1_get_Current_m476140818_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2889979481(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t454463237 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2889979481_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
#define InternalEnumerator_1_Dispose_m1290015243(__this, method) ((  void (*) (InternalEnumerator_1_t454463237 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1290015243_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3299696349(__this, method) ((  bool (*) (InternalEnumerator_1_t454463237 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3299696349_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3402055178(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t951840635 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3402055178_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4046360160(__this, method) ((  void (*) (InternalEnumerator_1_t951840635 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4046360160_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::get_Current()
#define InternalEnumerator_1_get_Current_m1679394243(__this, method) ((  KeyValuePair_2_t44776518  (*) (InternalEnumerator_1_t951840635 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1679394243_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2328184924(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t951840635 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2328184924_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::Dispose()
#define InternalEnumerator_1_Dispose_m537193559(__this, method) ((  void (*) (InternalEnumerator_1_t951840635 *, const MethodInfo*))InternalEnumerator_1_Dispose_m537193559_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2000685912(__this, method) ((  bool (*) (InternalEnumerator_1_t951840635 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2000685912_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1341209356(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3308121025 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1341209356_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4239728915(__this, method) ((  void (*) (InternalEnumerator_1_t3308121025 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4239728915_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
#define InternalEnumerator_1_get_Current_m3081223448(__this, method) ((  KeyValuePair_2_t2401056908  (*) (InternalEnumerator_1_t3308121025 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3081223448_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2284280372(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3308121025 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2284280372_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
#define InternalEnumerator_1_Dispose_m4098771594(__this, method) ((  void (*) (InternalEnumerator_1_t3308121025 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4098771594_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2951889983(__this, method) ((  bool (*) (InternalEnumerator_1_t3308121025 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2951889983_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m807987550(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3437281436 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m807987550_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2386791007(__this, method) ((  void (*) (InternalEnumerator_1_t3437281436 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2386791007_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
#define InternalEnumerator_1_get_Current_m923139215(__this, method) ((  KeyValuePair_2_t2530217319  (*) (InternalEnumerator_1_t3437281436 *, const MethodInfo*))InternalEnumerator_1_get_Current_m923139215_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141782011(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3437281436 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141782011_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
#define InternalEnumerator_1_Dispose_m2342933386(__this, method) ((  void (*) (InternalEnumerator_1_t3437281436 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2342933386_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1773160976(__this, method) ((  bool (*) (InternalEnumerator_1_t3437281436 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1773160976_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3390957028(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1451382081 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3390957028_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1223176161(__this, method) ((  void (*) (InternalEnumerator_1_t1451382081 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1223176161_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
#define InternalEnumerator_1_get_Current_m2223614542(__this, method) ((  Link_t544317964  (*) (InternalEnumerator_1_t1451382081 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2223614542_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1676501075(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1451382081 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1676501075_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
#define InternalEnumerator_1_Dispose_m1913545470(__this, method) ((  void (*) (InternalEnumerator_1_t1451382081 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1913545470_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2785895009(__this, method) ((  bool (*) (InternalEnumerator_1_t1451382081 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2785895009_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1196506529(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t587985571 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1196506529_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1256724261(__this, method) ((  void (*) (InternalEnumerator_1_t587985571 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1256724261_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
#define InternalEnumerator_1_get_Current_m3008260692(__this, method) ((  Slot_t3975888750  (*) (InternalEnumerator_1_t587985571 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3008260692_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1514266661(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t587985571 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1514266661_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
#define InternalEnumerator_1_Dispose_m1995846647(__this, method) ((  void (*) (InternalEnumerator_1_t587985571 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1995846647_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3200332883(__this, method) ((  bool (*) (InternalEnumerator_1_t587985571 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3200332883_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m819716934(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1291559127 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m819716934_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4202665280(__this, method) ((  void (*) (InternalEnumerator_1_t1291559127 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4202665280_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
#define InternalEnumerator_1_get_Current_m2832154098(__this, method) ((  Slot_t384495010  (*) (InternalEnumerator_1_t1291559127 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2832154098_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2629988057(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1291559127 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2629988057_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
#define InternalEnumerator_1_Dispose_m1519877610(__this, method) ((  void (*) (InternalEnumerator_1_t1291559127 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1519877610_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3354536447(__this, method) ((  bool (*) (InternalEnumerator_1_t1291559127 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3354536447_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3456047704(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t350626606 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3456047704_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m546509994(__this, method) ((  void (*) (InternalEnumerator_1_t350626606 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m546509994_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
#define InternalEnumerator_1_get_Current_m3225386639(__this, method) ((  DateTime_t3738529785  (*) (InternalEnumerator_1_t350626606 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3225386639_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2742943179(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t350626606 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2742943179_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
#define InternalEnumerator_1_Dispose_m649519051(__this, method) ((  void (*) (InternalEnumerator_1_t350626606 *, const MethodInfo*))InternalEnumerator_1_Dispose_m649519051_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1161444633(__this, method) ((  bool (*) (InternalEnumerator_1_t350626606 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1161444633_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m143506773(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3855323497 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m143506773_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m169899350(__this, method) ((  void (*) (InternalEnumerator_1_t3855323497 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m169899350_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
#define InternalEnumerator_1_get_Current_m2128158355(__this, method) ((  Decimal_t2948259380  (*) (InternalEnumerator_1_t3855323497 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2128158355_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m688818811(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3855323497 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m688818811_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
#define InternalEnumerator_1_Dispose_m937653815(__this, method) ((  void (*) (InternalEnumerator_1_t3855323497 *, const MethodInfo*))InternalEnumerator_1_Dispose_m937653815_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4210671224(__this, method) ((  bool (*) (InternalEnumerator_1_t3855323497 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4210671224_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1559487635(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1501729480 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1559487635_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14983211(__this, method) ((  void (*) (InternalEnumerator_1_t1501729480 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14983211_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
#define InternalEnumerator_1_get_Current_m2894466703(__this, method) ((  double (*) (InternalEnumerator_1_t1501729480 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2894466703_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3853320011(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1501729480 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3853320011_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
#define InternalEnumerator_1_Dispose_m2043273260(__this, method) ((  void (*) (InternalEnumerator_1_t1501729480 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2043273260_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
#define InternalEnumerator_1_MoveNext_m377783729(__this, method) ((  bool (*) (InternalEnumerator_1_t1501729480 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m377783729_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2910272776(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3459884504 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2910272776_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m521819017(__this, method) ((  void (*) (InternalEnumerator_1_t3459884504 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m521819017_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
#define InternalEnumerator_1_get_Current_m2723520268(__this, method) ((  int16_t (*) (InternalEnumerator_1_t3459884504 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2723520268_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888718134(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3459884504 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888718134_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
#define InternalEnumerator_1_Dispose_m3716424577(__this, method) ((  void (*) (InternalEnumerator_1_t3459884504 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3716424577_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3525157932(__this, method) ((  bool (*) (InternalEnumerator_1_t3459884504 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3525157932_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m217498388(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3858009870 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m217498388_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m779787360(__this, method) ((  void (*) (InternalEnumerator_1_t3858009870 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m779787360_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
#define InternalEnumerator_1_get_Current_m3500427238(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3858009870 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3500427238_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2435291801(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3858009870 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2435291801_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
#define InternalEnumerator_1_Dispose_m3519406884(__this, method) ((  void (*) (InternalEnumerator_1_t3858009870 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3519406884_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3802174768(__this, method) ((  bool (*) (InternalEnumerator_1_t3858009870 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3802174768_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4055378331(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t348664125 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4055378331_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3873091784(__this, method) ((  void (*) (InternalEnumerator_1_t348664125 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3873091784_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
#define InternalEnumerator_1_get_Current_m685192625(__this, method) ((  int64_t (*) (InternalEnumerator_1_t348664125 *, const MethodInfo*))InternalEnumerator_1_get_Current_m685192625_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m180319738(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t348664125 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m180319738_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
#define InternalEnumerator_1_Dispose_m830510730(__this, method) ((  void (*) (InternalEnumerator_1_t348664125 *, const MethodInfo*))InternalEnumerator_1_Dispose_m830510730_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4266213580(__this, method) ((  bool (*) (InternalEnumerator_1_t348664125 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4266213580_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1579105305(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1747214298 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1579105305_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3011999097(__this, method) ((  void (*) (InternalEnumerator_1_t1747214298 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3011999097_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
#define InternalEnumerator_1_get_Current_m3900374024(__this, method) ((  IntPtr_t (*) (InternalEnumerator_1_t1747214298 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3900374024_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m548105685(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1747214298 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m548105685_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
#define InternalEnumerator_1_Dispose_m2818366163(__this, method) ((  void (*) (InternalEnumerator_1_t1747214298 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2818366163_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3520556285(__this, method) ((  bool (*) (InternalEnumerator_1_t1747214298 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3520556285_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1675719794(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3987170281 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1675719794_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2914412419(__this, method) ((  void (*) (InternalEnumerator_1_t3987170281 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2914412419_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
#define InternalEnumerator_1_get_Current_m3839250771(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3987170281 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3839250771_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2982675020(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3987170281 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2982675020_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
#define InternalEnumerator_1_Dispose_m2006926799(__this, method) ((  void (*) (InternalEnumerator_1_t3987170281 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2006926799_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4035695998(__this, method) ((  bool (*) (InternalEnumerator_1_t3987170281 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4035695998_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m643493702(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1194929827 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m643493702_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m895873066(__this, method) ((  void (*) (InternalEnumerator_1_t1194929827 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m895873066_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
#define InternalEnumerator_1_get_Current_m3354878040(__this, method) ((  CustomAttributeNamedArgument_t287865710  (*) (InternalEnumerator_1_t1194929827 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3354878040_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4189894603(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1194929827 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4189894603_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
#define InternalEnumerator_1_Dispose_m3577625655(__this, method) ((  void (*) (InternalEnumerator_1_t1194929827 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3577625655_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4138204635(__this, method) ((  bool (*) (InternalEnumerator_1_t1194929827 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4138204635_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m748741755(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3630214274 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m748741755_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m596870847(__this, method) ((  void (*) (InternalEnumerator_1_t3630214274 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m596870847_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
#define InternalEnumerator_1_get_Current_m3653231044(__this, method) ((  CustomAttributeTypedArgument_t2723150157  (*) (InternalEnumerator_1_t3630214274 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3653231044_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4213507601(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3630214274 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4213507601_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
#define InternalEnumerator_1_Dispose_m2438347491(__this, method) ((  void (*) (InternalEnumerator_1_t3630214274 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2438347491_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3577491700(__this, method) ((  bool (*) (InternalEnumerator_1_t3630214274 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3577491700_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1486034688(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1267231508 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1486034688_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3619766341(__this, method) ((  void (*) (InternalEnumerator_1_t1267231508 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3619766341_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
#define InternalEnumerator_1_get_Current_m2698009637(__this, method) ((  LabelData_t360167391  (*) (InternalEnumerator_1_t1267231508 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2698009637_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m435848551(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1267231508 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m435848551_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
#define InternalEnumerator_1_Dispose_m106460639(__this, method) ((  void (*) (InternalEnumerator_1_t1267231508 *, const MethodInfo*))InternalEnumerator_1_Dispose_m106460639_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1728532725(__this, method) ((  bool (*) (InternalEnumerator_1_t1267231508 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1728532725_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1594304290(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1765566171 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1594304290_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m720350288(__this, method) ((  void (*) (InternalEnumerator_1_t1765566171 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m720350288_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
#define InternalEnumerator_1_get_Current_m2680116177(__this, method) ((  LabelFixup_t858502054  (*) (InternalEnumerator_1_t1765566171 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2680116177_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1976902927(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1765566171 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1976902927_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
#define InternalEnumerator_1_Dispose_m1063909490(__this, method) ((  void (*) (InternalEnumerator_1_t1765566171 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1063909490_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2234422530(__this, method) ((  bool (*) (InternalEnumerator_1_t1765566171 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2234422530_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m238559784(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3232839231 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m238559784_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2428767548(__this, method) ((  void (*) (InternalEnumerator_1_t3232839231 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2428767548_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m165106323(__this, method) ((  ILTokenInfo_t2325775114  (*) (InternalEnumerator_1_t3232839231 *, const MethodInfo*))InternalEnumerator_1_get_Current_m165106323_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3624751851(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3232839231 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3624751851_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m130608859(__this, method) ((  void (*) (InternalEnumerator_1_t3232839231 *, const MethodInfo*))InternalEnumerator_1_Dispose_m130608859_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m819973544(__this, method) ((  bool (*) (InternalEnumerator_1_t3232839231 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m819973544_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m39232262(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2368758583 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m39232262_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3165277182(__this, method) ((  void (*) (InternalEnumerator_1_t2368758583 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3165277182_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
#define InternalEnumerator_1_get_Current_m356936020(__this, method) ((  ParameterModifier_t1461694466  (*) (InternalEnumerator_1_t2368758583 *, const MethodInfo*))InternalEnumerator_1_get_Current_m356936020_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m694606607(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2368758583 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m694606607_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
#define InternalEnumerator_1_Dispose_m2840529825(__this, method) ((  void (*) (InternalEnumerator_1_t2368758583 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2840529825_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3664960764(__this, method) ((  bool (*) (InternalEnumerator_1_t2368758583 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3664960764_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1999141680(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t958356908 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1999141680_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1259718730(__this, method) ((  void (*) (InternalEnumerator_1_t958356908 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1259718730_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::get_Current()
#define InternalEnumerator_1_get_Current_m144365666(__this, method) ((  ResourceCacheItem_t51292791  (*) (InternalEnumerator_1_t958356908 *, const MethodInfo*))InternalEnumerator_1_get_Current_m144365666_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141016822(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t958356908 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141016822_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::Dispose()
#define InternalEnumerator_1_Dispose_m887344916(__this, method) ((  void (*) (InternalEnumerator_1_t958356908 *, const MethodInfo*))InternalEnumerator_1_Dispose_m887344916_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2960571514(__this, method) ((  bool (*) (InternalEnumerator_1_t958356908 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2960571514_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2908852803(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3780029419 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2908852803_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4008893642(__this, method) ((  void (*) (InternalEnumerator_1_t3780029419 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4008893642_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m3191242573(__this, method) ((  ResourceInfo_t2872965302  (*) (InternalEnumerator_1_t3780029419 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3191242573_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4235876088(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3780029419 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4235876088_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m725544411(__this, method) ((  void (*) (InternalEnumerator_1_t3780029419 *, const MethodInfo*))InternalEnumerator_1_Dispose_m725544411_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3174983217(__this, method) ((  bool (*) (InternalEnumerator_1_t3780029419 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3174983217_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4200721464(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t153918522 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4200721464_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2234754688(__this, method) ((  void (*) (InternalEnumerator_1_t153918522 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2234754688_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
#define InternalEnumerator_1_get_Current_m3911557813(__this, method) ((  uint8_t (*) (InternalEnumerator_1_t153918522 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3911557813_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3892960115(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t153918522 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3892960115_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
#define InternalEnumerator_1_Dispose_m3983612351(__this, method) ((  void (*) (InternalEnumerator_1_t153918522 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3983612351_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1020308708(__this, method) ((  bool (*) (InternalEnumerator_1_t153918522 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1020308708_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3460713284(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2576641779 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3460713284_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3545912565(__this, method) ((  void (*) (InternalEnumerator_1_t2576641779 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3545912565_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
#define InternalEnumerator_1_get_Current_m1408339225(__this, method) ((  int8_t (*) (InternalEnumerator_1_t2576641779 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1408339225_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m767948013(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2576641779 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m767948013_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
#define InternalEnumerator_1_Dispose_m1406845627(__this, method) ((  void (*) (InternalEnumerator_1_t2576641779 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1406845627_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1015797184(__this, method) ((  bool (*) (InternalEnumerator_1_t2576641779 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1015797184_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3443175323(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1040666831 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3443175323_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2314612291(__this, method) ((  void (*) (InternalEnumerator_1_t1040666831 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2314612291_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
#define InternalEnumerator_1_get_Current_m2389908135(__this, method) ((  X509ChainStatus_t133602714  (*) (InternalEnumerator_1_t1040666831 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2389908135_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1096730143(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1040666831 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1096730143_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
#define InternalEnumerator_1_Dispose_m469889800(__this, method) ((  void (*) (InternalEnumerator_1_t1040666831 *, const MethodInfo*))InternalEnumerator_1_Dispose_m469889800_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1732823414(__this, method) ((  bool (*) (InternalEnumerator_1_t1040666831 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1732823414_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m31115849(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2304330891 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m31115849_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181721336(__this, method) ((  void (*) (InternalEnumerator_1_t2304330891 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181721336_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
#define InternalEnumerator_1_get_Current_m2612852447(__this, method) ((  float (*) (InternalEnumerator_1_t2304330891 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2612852447_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1823542095(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2304330891 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1823542095_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
#define InternalEnumerator_1_Dispose_m630370856(__this, method) ((  void (*) (InternalEnumerator_1_t2304330891 *, const MethodInfo*))InternalEnumerator_1_Dispose_m630370856_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3134701632(__this, method) ((  bool (*) (InternalEnumerator_1_t2304330891 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3134701632_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1122952091(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t83702344 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1122952091_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m396346696(__this, method) ((  void (*) (InternalEnumerator_1_t83702344 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m396346696_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
#define InternalEnumerator_1_get_Current_m1909182215(__this, method) ((  Mark_t3471605523  (*) (InternalEnumerator_1_t83702344 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1909182215_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088805473(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t83702344 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088805473_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
#define InternalEnumerator_1_Dispose_m1837758743(__this, method) ((  void (*) (InternalEnumerator_1_t83702344 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1837758743_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4133541970(__this, method) ((  bool (*) (InternalEnumerator_1_t83702344 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4133541970_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3261326277(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1788223366 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3261326277_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3763767775(__this, method) ((  void (*) (InternalEnumerator_1_t1788223366 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3763767775_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
#define InternalEnumerator_1_get_Current_m1698047500(__this, method) ((  TimeSpan_t881159249  (*) (InternalEnumerator_1_t1788223366 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1698047500_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1299775605(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1788223366 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1299775605_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
#define InternalEnumerator_1_Dispose_m4260521517(__this, method) ((  void (*) (InternalEnumerator_1_t1788223366 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4260521517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3958061110(__this, method) ((  bool (*) (InternalEnumerator_1_t1788223366 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3958061110_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2202456613(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3084789075 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2202456613_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3848218235(__this, method) ((  void (*) (InternalEnumerator_1_t3084789075 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3848218235_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
#define InternalEnumerator_1_get_Current_m784835552(__this, method) ((  uint16_t (*) (InternalEnumerator_1_t3084789075 *, const MethodInfo*))InternalEnumerator_1_get_Current_m784835552_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m191386315(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3084789075 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m191386315_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
#define InternalEnumerator_1_Dispose_m359678482(__this, method) ((  void (*) (InternalEnumerator_1_t3084789075 *, const MethodInfo*))InternalEnumerator_1_Dispose_m359678482_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
#define InternalEnumerator_1_MoveNext_m973048327(__this, method) ((  bool (*) (InternalEnumerator_1_t3084789075 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m973048327_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3383770493(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3467126095 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3383770493_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m342565588(__this, method) ((  void (*) (InternalEnumerator_1_t3467126095 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m342565588_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
#define InternalEnumerator_1_get_Current_m1897120917(__this, method) ((  uint32_t (*) (InternalEnumerator_1_t3467126095 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1897120917_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612699335(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3467126095 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612699335_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
#define InternalEnumerator_1_Dispose_m1121538879(__this, method) ((  void (*) (InternalEnumerator_1_t3467126095 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1121538879_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3855324972(__this, method) ((  bool (*) (InternalEnumerator_1_t3467126095 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3855324972_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3215746182(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t746136913 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3215746182_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m872612294(__this, method) ((  void (*) (InternalEnumerator_1_t746136913 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m872612294_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
#define InternalEnumerator_1_get_Current_m1588647567(__this, method) ((  uint64_t (*) (InternalEnumerator_1_t746136913 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1588647567_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2395961985(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t746136913 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2395961985_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
#define InternalEnumerator_1_Dispose_m3637184090(__this, method) ((  void (*) (InternalEnumerator_1_t746136913 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3637184090_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1975820803(__this, method) ((  bool (*) (InternalEnumerator_1_t746136913 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1975820803_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m362401472(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1629489814 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m362401472_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4196663616(__this, method) ((  void (*) (InternalEnumerator_1_t1629489814 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4196663616_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
#define InternalEnumerator_1_get_Current_m1007906068(__this, method) ((  UriScheme_t722425697  (*) (InternalEnumerator_1_t1629489814 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1007906068_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2671801110(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1629489814 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2671801110_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
#define InternalEnumerator_1_Dispose_m648941584(__this, method) ((  void (*) (InternalEnumerator_1_t1629489814 *, const MethodInfo*))InternalEnumerator_1_Dispose_m648941584_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
#define InternalEnumerator_1_MoveNext_m190587569(__this, method) ((  bool (*) (InternalEnumerator_1_t1629489814 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m190587569_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4044013350(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4063781272 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4044013350_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4107572074(__this, method) ((  void (*) (InternalEnumerator_1_t4063781272 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4107572074_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m348524980(__this, method) ((  AnimatorClipInfo_t3156717155  (*) (InternalEnumerator_1_t4063781272 *, const MethodInfo*))InternalEnumerator_1_get_Current_m348524980_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1911422882(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4063781272 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1911422882_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m969498836(__this, method) ((  void (*) (InternalEnumerator_1_t4063781272 *, const MethodInfo*))InternalEnumerator_1_Dispose_m969498836_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4074044800(__this, method) ((  bool (*) (InternalEnumerator_1_t4063781272 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4074044800_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m539509188(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3507565409 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m539509188_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m996811230(__this, method) ((  void (*) (InternalEnumerator_1_t3507565409 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m996811230_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
#define InternalEnumerator_1_get_Current_m1232221964(__this, method) ((  Color32_t2600501292  (*) (InternalEnumerator_1_t3507565409 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1232221964_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4196752819(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3507565409 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4196752819_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
#define InternalEnumerator_1_Dispose_m3259955982(__this, method) ((  void (*) (InternalEnumerator_1_t3507565409 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3259955982_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1590908934(__this, method) ((  bool (*) (InternalEnumerator_1_t3507565409 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1590908934_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1352157576(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t370852074 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1352157576_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1009155168(__this, method) ((  void (*) (InternalEnumerator_1_t370852074 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1009155168_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
#define InternalEnumerator_1_get_Current_m2315302778(__this, method) ((  ContactPoint_t3758755253  (*) (InternalEnumerator_1_t370852074 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2315302778_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m629296715(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t370852074 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m629296715_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
#define InternalEnumerator_1_Dispose_m756188704(__this, method) ((  void (*) (InternalEnumerator_1_t370852074 *, const MethodInfo*))InternalEnumerator_1_Dispose_m756188704_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1187868016(__this, method) ((  bool (*) (InternalEnumerator_1_t370852074 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1187868016_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1270930859(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2337465 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1270930859_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m500203932(__this, method) ((  void (*) (InternalEnumerator_1_t2337465 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m500203932_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::get_Current()
#define InternalEnumerator_1_get_Current_m1523142935(__this, method) ((  ContactPoint2D_t3390240644  (*) (InternalEnumerator_1_t2337465 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1523142935_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2358118467(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2337465 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2358118467_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::Dispose()
#define InternalEnumerator_1_Dispose_m1306615151(__this, method) ((  void (*) (InternalEnumerator_1_t2337465 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1306615151_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1174937354(__this, method) ((  bool (*) (InternalEnumerator_1_t2337465 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1174937354_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2311732727(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4267370966 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2311732727_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1686642781(__this, method) ((  void (*) (InternalEnumerator_1_t4267370966 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1686642781_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
#define InternalEnumerator_1_get_Current_m254780543(__this, method) ((  RaycastResult_t3360306849  (*) (InternalEnumerator_1_t4267370966 *, const MethodInfo*))InternalEnumerator_1_get_Current_m254780543_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m923624021(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4267370966 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m923624021_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
#define InternalEnumerator_1_Dispose_m3517794894(__this, method) ((  void (*) (InternalEnumerator_1_t4267370966 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3517794894_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1344185775(__this, method) ((  bool (*) (InternalEnumerator_1_t4267370966 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1344185775_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1744883412(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t818507063 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1744883412_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3217592429(__this, method) ((  void (*) (InternalEnumerator_1_t818507063 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3217592429_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
#define InternalEnumerator_1_get_Current_m2907722321(__this, method) ((  Keyframe_t4206410242  (*) (InternalEnumerator_1_t818507063 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2907722321_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m605068928(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t818507063 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m605068928_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
#define InternalEnumerator_1_Dispose_m3211169941(__this, method) ((  void (*) (InternalEnumerator_1_t818507063 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3211169941_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1109261117(__this, method) ((  bool (*) (InternalEnumerator_1_t818507063 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1109261117_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m615777089(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1963066083 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m615777089_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2621383412(__this, method) ((  void (*) (InternalEnumerator_1_t1963066083 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2621383412_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
#define InternalEnumerator_1_get_Current_m114240259(__this, method) ((  RaycastHit_t1056001966  (*) (InternalEnumerator_1_t1963066083 *, const MethodInfo*))InternalEnumerator_1_get_Current_m114240259_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1635397542(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1963066083 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1635397542_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
#define InternalEnumerator_1_Dispose_m958164402(__this, method) ((  void (*) (InternalEnumerator_1_t1963066083 *, const MethodInfo*))InternalEnumerator_1_Dispose_m958164402_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3164144724(__this, method) ((  bool (*) (InternalEnumerator_1_t1963066083 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3164144724_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2635640285(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3186646106 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2635640285_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1775752715(__this, method) ((  void (*) (InternalEnumerator_1_t3186646106 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1775752715_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
#define InternalEnumerator_1_get_Current_m1655128652(__this, method) ((  RaycastHit2D_t2279581989  (*) (InternalEnumerator_1_t3186646106 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1655128652_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2424959150(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3186646106 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2424959150_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
#define InternalEnumerator_1_Dispose_m555942266(__this, method) ((  void (*) (InternalEnumerator_1_t3186646106 *, const MethodInfo*))InternalEnumerator_1_Dispose_m555942266_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2734554195(__this, method) ((  bool (*) (InternalEnumerator_1_t3186646106 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2734554195_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3848674141(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3267543976 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3848674141_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2556670574(__this, method) ((  void (*) (InternalEnumerator_1_t3267543976 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2556670574_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Rect>::get_Current()
#define InternalEnumerator_1_get_Current_m3941815949(__this, method) ((  Rect_t2360479859  (*) (InternalEnumerator_1_t3267543976 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3941815949_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rect>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2690353642(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3267543976 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2690353642_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::Dispose()
#define InternalEnumerator_1_Dispose_m1453932905(__this, method) ((  void (*) (InternalEnumerator_1_t3267543976 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1453932905_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rect>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3804980664(__this, method) ((  bool (*) (InternalEnumerator_1_t3267543976 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3804980664_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3913006324(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4136673857 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3913006324_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1451164462(__this, method) ((  void (*) (InternalEnumerator_1_t4136673857 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1451164462_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m4124877207(__this, method) ((  HitInfo_t3229609740  (*) (InternalEnumerator_1_t4136673857 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4124877207_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2032951142(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4136673857 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2032951142_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m3977286481(__this, method) ((  void (*) (InternalEnumerator_1_t4136673857 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3977286481_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2088624192(__this, method) ((  bool (*) (InternalEnumerator_1_t4136673857 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2088624192_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3535695642(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1582286363 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3535695642_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1918396835(__this, method) ((  void (*) (InternalEnumerator_1_t1582286363 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1918396835_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
#define InternalEnumerator_1_get_Current_m3628030453(__this, method) ((  GcAchievementData_t675222246  (*) (InternalEnumerator_1_t1582286363 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3628030453_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m943285433(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1582286363 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m943285433_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
#define InternalEnumerator_1_Dispose_m4174463085(__this, method) ((  void (*) (InternalEnumerator_1_t1582286363 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4174463085_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4151310216(__this, method) ((  bool (*) (InternalEnumerator_1_t1582286363 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4151310216_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m258868363(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3032373948 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m258868363_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4039902941(__this, method) ((  void (*) (InternalEnumerator_1_t3032373948 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4039902941_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
#define InternalEnumerator_1_get_Current_m3609142494(__this, method) ((  GcScoreData_t2125309831  (*) (InternalEnumerator_1_t3032373948 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3609142494_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2582019288(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3032373948 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2582019288_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
#define InternalEnumerator_1_Dispose_m3187018662(__this, method) ((  void (*) (InternalEnumerator_1_t3032373948 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3187018662_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1036267697(__this, method) ((  bool (*) (InternalEnumerator_1_t3032373948 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1036267697_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m63033851(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2828920985 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m63033851_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4226416995(__this, method) ((  void (*) (InternalEnumerator_1_t2828920985 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4226416995_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Touch>::get_Current()
#define InternalEnumerator_1_get_Current_m1836170281(__this, method) ((  Touch_t1921856868  (*) (InternalEnumerator_1_t2828920985 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1836170281_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2499755184(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2828920985 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2499755184_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::Dispose()
#define InternalEnumerator_1_Dispose_m1432840524(__this, method) ((  void (*) (InternalEnumerator_1_t2828920985 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1432840524_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Touch>::MoveNext()
#define InternalEnumerator_1_MoveNext_m300199243(__this, method) ((  bool (*) (InternalEnumerator_1_t2828920985 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m300199243_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1739091604(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2694367513 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1739091604_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1341907970(__this, method) ((  void (*) (InternalEnumerator_1_t2694367513 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1341907970_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
#define InternalEnumerator_1_get_Current_m2350635577(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2694367513 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2350635577_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164048921(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2694367513 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164048921_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
#define InternalEnumerator_1_Dispose_m28687982(__this, method) ((  void (*) (InternalEnumerator_1_t2694367513 *, const MethodInfo*))InternalEnumerator_1_Dispose_m28687982_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1555187632(__this, method) ((  bool (*) (InternalEnumerator_1_t2694367513 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1555187632_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4175030001(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t982565223 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4175030001_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2305395628(__this, method) ((  void (*) (InternalEnumerator_1_t982565223 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2305395628_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m3792939945(__this, method) ((  UICharInfo_t75501106  (*) (InternalEnumerator_1_t982565223 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3792939945_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2649471526(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t982565223 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2649471526_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m3790132913(__this, method) ((  void (*) (InternalEnumerator_1_t982565223 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3790132913_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3713722659(__this, method) ((  bool (*) (InternalEnumerator_1_t982565223 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3713722659_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3322594868(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t807363631 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3322594868_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1601477281(__this, method) ((  void (*) (InternalEnumerator_1_t807363631 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1601477281_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m1070921822(__this, method) ((  UILineInfo_t4195266810  (*) (InternalEnumerator_1_t807363631 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1070921822_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3553395619(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t807363631 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3553395619_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m2788308318(__this, method) ((  void (*) (InternalEnumerator_1_t807363631 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2788308318_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1396448578(__this, method) ((  bool (*) (InternalEnumerator_1_t807363631 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1396448578_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2490839835(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t669594426 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2490839835_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4072625129(__this, method) ((  void (*) (InternalEnumerator_1_t669594426 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4072625129_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::get_Current()
#define InternalEnumerator_1_get_Current_m1534474313(__this, method) ((  UIVertex_t4057497605  (*) (InternalEnumerator_1_t669594426 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1534474313_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2209458050(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t669594426 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2209458050_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::Dispose()
#define InternalEnumerator_1_Dispose_m365545176(__this, method) ((  void (*) (InternalEnumerator_1_t669594426 *, const MethodInfo*))InternalEnumerator_1_Dispose_m365545176_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3686444574(__this, method) ((  bool (*) (InternalEnumerator_1_t669594426 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3686444574_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m945079686(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3063293640 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m945079686_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m158730371(__this, method) ((  void (*) (InternalEnumerator_1_t3063293640 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m158730371_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
#define InternalEnumerator_1_get_Current_m3331252162(__this, method) ((  Vector2_t2156229523  (*) (InternalEnumerator_1_t3063293640 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3331252162_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1847780851(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3063293640 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1847780851_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
#define InternalEnumerator_1_Dispose_m663714168(__this, method) ((  void (*) (InternalEnumerator_1_t3063293640 *, const MethodInfo*))InternalEnumerator_1_Dispose_m663714168_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1262669372(__this, method) ((  bool (*) (InternalEnumerator_1_t3063293640 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1262669372_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4132027968(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t334410285 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4132027968_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1916984356(__this, method) ((  void (*) (InternalEnumerator_1_t334410285 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1916984356_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector3>::get_Current()
#define InternalEnumerator_1_get_Current_m1477715453(__this, method) ((  Vector3_t3722313464  (*) (InternalEnumerator_1_t334410285 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1477715453_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3537550566(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t334410285 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3537550566_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::Dispose()
#define InternalEnumerator_1_Dispose_m2188147046(__this, method) ((  void (*) (InternalEnumerator_1_t334410285 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2188147046_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector3>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1262906009(__this, method) ((  bool (*) (InternalEnumerator_1_t334410285 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1262906009_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2307827786(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4226093054 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2307827786_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1944844050(__this, method) ((  void (*) (InternalEnumerator_1_t4226093054 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1944844050_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector4>::get_Current()
#define InternalEnumerator_1_get_Current_m2356858238(__this, method) ((  Vector4_t3319028937  (*) (InternalEnumerator_1_t4226093054 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2356858238_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3982010935(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4226093054 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3982010935_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::Dispose()
#define InternalEnumerator_1_Dispose_m987068791(__this, method) ((  void (*) (InternalEnumerator_1_t4226093054 *, const MethodInfo*))InternalEnumerator_1_Dispose_m987068791_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector4>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1258813334(__this, method) ((  bool (*) (InternalEnumerator_1_t4226093054 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1258813334_gshared)(__this, method)
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2677842846_gshared (Action_1_t269755560 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3635202442_gshared (Action_1_t269755560 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3635202442((Action_1_t269755560 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1817882028_gshared (Action_1_t269755560 * __this, bool ___obj0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1817882028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4173505031_gshared (Action_1_t269755560 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m118522912_gshared (Action_1_t3252573759 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m2461023210_gshared (Action_1_t3252573759 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2461023210((Action_1_t3252573759 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2344209729_gshared (Action_1_t3252573759 * __this, Il2CppObject * ___obj0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2989437122_gshared (Action_1_t3252573759 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1520833393_gshared (Action_2_t2523487705 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1763453775_gshared (Action_2_t2523487705 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1763453775((Action_2_t2523487705 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1990245223_gshared (Action_2_t2523487705 * __this, bool ___arg10, Il2CppObject * ___arg21, AsyncCallback_t3962456242 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m1990245223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m4064486054_gshared (Action_2_t2523487705 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1537761784_gshared (Action_2_t2470008838 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m406858745_gshared (Action_2_t2470008838 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m406858745((Action_2_t2470008838 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3374412194_gshared (Action_2_t2470008838 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t3962456242 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m304506319_gshared (Action_2_t2470008838 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m3182009741_gshared (Action_3_t1173861992 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,System.Int32,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3429238884_gshared (Action_3_t1173861992 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m3429238884((Action_3_t1173861992 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,System.Int32,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m1898539978_gshared (Action_3_t1173861992 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, AsyncCallback_t3962456242 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m1898539978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m3426989307_gshared (Action_3_t1173861992 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m967275408_gshared (Action_3_t3632554945 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m1376317295_gshared (Action_3_t3632554945 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m1376317295((Action_3_t3632554945 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m222978689_gshared (Action_3_t3632554945 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t3962456242 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m1410110426_gshared (Action_3_t3632554945 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m3091529227_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4107749331 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m9057020_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4107749331 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3208659014_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4107749331 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4047948264_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4107749331 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0055;
			}
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t4270623997 * L_2 = (ArrayReadOnlyList_1_t4270623997 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t2843939325* L_3 = (ObjectU5BU5D_t2843939325*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t4270623997 * L_9 = (ArrayReadOnlyList_1_t4270623997 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		ObjectU5BU5D_t2843939325* L_10 = (ObjectU5BU5D_t2843939325*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m503464442_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4107749331 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m3837913694_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4107749331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m3837913694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m3941491744_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1315508877 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t287865710  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m52354244_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1315508877 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t287865710  L_0 = (CustomAttributeNamedArgument_t287865710 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1580332103_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1315508877 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t287865710  L_0 = (CustomAttributeNamedArgument_t287865710 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t287865710  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1358891892_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1315508877 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0055;
			}
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t1478383543 * L_2 = (ArrayReadOnlyList_1_t1478383543 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3710464795*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t287865710  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t1478383543 * L_9 = (ArrayReadOnlyList_1_t1478383543 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_10 = (CustomAttributeNamedArgumentU5BU5D_t3710464795*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1800277885_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1315508877 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2500457056_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1315508877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2500457056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1150758267_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3750793324 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t2723150157  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1566629109_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3750793324 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t2723150157  L_0 = (CustomAttributeTypedArgument_t2723150157 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m283764921_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3750793324 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t2723150157  L_0 = (CustomAttributeTypedArgument_t2723150157 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t2723150157  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1185613002_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3750793324 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0055;
			}
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t3913667990 * L_2 = (ArrayReadOnlyList_1_t3913667990 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1465843424*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t2723150157  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t3913667990 * L_9 = (ArrayReadOnlyList_1_t3913667990 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_10 = (CustomAttributeTypedArgumentU5BU5D_t1465843424*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m3298287955_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3750793324 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1192421843_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3750793324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1192421843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3411930943_gshared (ArrayReadOnlyList_1_t4270623997 * __this, ObjectU5BU5D_t2843939325* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t2843939325* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1042758841_gshared (ArrayReadOnlyList_1_t4270623997 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t4270623997 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t4270623997 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t4270623997 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m2988101436_gshared (ArrayReadOnlyList_1_t4270623997 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2988101436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t2843939325* L_1 = (ObjectU5BU5D_t2843939325*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t2843939325* L_3 = (ObjectU5BU5D_t2843939325*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m2916695038_gshared (ArrayReadOnlyList_1_t4270623997 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m3450004702_gshared (ArrayReadOnlyList_1_t4270623997 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m1365711605_gshared (ArrayReadOnlyList_1_t4270623997 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m899240452_gshared (ArrayReadOnlyList_1_t4270623997 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2564101847_gshared (ArrayReadOnlyList_1_t4270623997 * __this, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m381552673_gshared (ArrayReadOnlyList_1_t4270623997 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t2843939325*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t2843939325*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m544662236_gshared (ArrayReadOnlyList_1_t4270623997 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_array_0();
		ObjectU5BU5D_t2843939325* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m225704097((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1835926958_gshared (ArrayReadOnlyList_1_t4270623997 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t4107749331 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t4107749331 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4107749331 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t4107749331 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4107749331 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t4107749331 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t4107749331 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m562338247_gshared (ArrayReadOnlyList_1_t4270623997 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t2843939325*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t2843939325*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m1827843425_gshared (ArrayReadOnlyList_1_t4270623997 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m1724926862_gshared (ArrayReadOnlyList_1_t4270623997 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2104218585_gshared (ArrayReadOnlyList_1_t4270623997 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C"  Exception_t1436737249 * ArrayReadOnlyList_1_ReadOnlyError_m1047641207_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m1047641207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_0, (String_t*)_stringLiteral3853944826, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m2942507207_gshared (ArrayReadOnlyList_1_t1478383543 * __this, CustomAttributeNamedArgumentU5BU5D_t3710464795* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3164285357_gshared (ArrayReadOnlyList_1_t1478383543 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t1478383543 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t1478383543 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t1478383543 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t287865710  ArrayReadOnlyList_1_get_Item_m1974867852_gshared (ArrayReadOnlyList_1_t1478383543 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m1974867852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3710464795*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3710464795*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t287865710  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m1428008044_gshared (ArrayReadOnlyList_1_t1478383543 * __this, int32_t ___index0, CustomAttributeNamedArgument_t287865710  ___value1, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2463504623_gshared (ArrayReadOnlyList_1_t1478383543 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3710464795*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2046554184_gshared (ArrayReadOnlyList_1_t1478383543 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3112646016_gshared (ArrayReadOnlyList_1_t1478383543 * __this, CustomAttributeNamedArgument_t287865710  ___item0, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m638462730_gshared (ArrayReadOnlyList_1_t1478383543 * __this, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m232667507_gshared (ArrayReadOnlyList_1_t1478383543 * __this, CustomAttributeNamedArgument_t287865710  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3710464795*)__this->get_array_0();
		CustomAttributeNamedArgument_t287865710  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3710464795*, CustomAttributeNamedArgument_t287865710 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3710464795*)L_0, (CustomAttributeNamedArgument_t287865710 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1127871639_gshared (ArrayReadOnlyList_1_t1478383543 * __this, CustomAttributeNamedArgumentU5BU5D_t3710464795* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3710464795*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m225704097((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m3931906247_gshared (ArrayReadOnlyList_1_t1478383543 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t1315508877 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t1315508877 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1315508877 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1315508877 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1315508877 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t1315508877 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t1315508877 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1911574180_gshared (ArrayReadOnlyList_1_t1478383543 * __this, CustomAttributeNamedArgument_t287865710  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3710464795*)__this->get_array_0();
		CustomAttributeNamedArgument_t287865710  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3710464795*, CustomAttributeNamedArgument_t287865710 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3710464795*)L_0, (CustomAttributeNamedArgument_t287865710 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m587555490_gshared (ArrayReadOnlyList_1_t1478383543 * __this, int32_t ___index0, CustomAttributeNamedArgument_t287865710  ___item1, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m439579722_gshared (ArrayReadOnlyList_1_t1478383543 * __this, CustomAttributeNamedArgument_t287865710  ___item0, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m3226254084_gshared (ArrayReadOnlyList_1_t1478383543 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern "C"  Exception_t1436737249 * ArrayReadOnlyList_1_ReadOnlyError_m3555240367_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m3555240367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_0, (String_t*)_stringLiteral3853944826, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m556992429_gshared (ArrayReadOnlyList_1_t3913667990 * __this, CustomAttributeTypedArgumentU5BU5D_t1465843424* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1143471103_gshared (ArrayReadOnlyList_1_t3913667990 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t3913667990 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t3913667990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t3913667990 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeTypedArgument_t2723150157  ArrayReadOnlyList_1_get_Item_m4135188594_gshared (ArrayReadOnlyList_1_t3913667990 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m4135188594_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_1 = (CustomAttributeTypedArgumentU5BU5D_t1465843424*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1465843424*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t2723150157  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3769996290_gshared (ArrayReadOnlyList_1_t3913667990 * __this, int32_t ___index0, CustomAttributeTypedArgument_t2723150157  ___value1, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2924672952_gshared (ArrayReadOnlyList_1_t3913667990 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1465843424*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m467578319_gshared (ArrayReadOnlyList_1_t3913667990 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m302584359_gshared (ArrayReadOnlyList_1_t3913667990 * __this, CustomAttributeTypedArgument_t2723150157  ___item0, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m337906083_gshared (ArrayReadOnlyList_1_t3913667990 * __this, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m2459654648_gshared (ArrayReadOnlyList_1_t3913667990 * __this, CustomAttributeTypedArgument_t2723150157  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1465843424*)__this->get_array_0();
		CustomAttributeTypedArgument_t2723150157  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1465843424*, CustomAttributeTypedArgument_t2723150157 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1465843424*)L_0, (CustomAttributeTypedArgument_t2723150157 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1534406454_gshared (ArrayReadOnlyList_1_t3913667990 * __this, CustomAttributeTypedArgumentU5BU5D_t1465843424* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1465843424*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m225704097((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m3297894971_gshared (ArrayReadOnlyList_1_t3913667990 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t3750793324 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t3750793324 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3750793324 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3750793324 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3750793324 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t3750793324 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t3750793324 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m3750264679_gshared (ArrayReadOnlyList_1_t3913667990 * __this, CustomAttributeTypedArgument_t2723150157  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1465843424*)__this->get_array_0();
		CustomAttributeTypedArgument_t2723150157  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1465843424*, CustomAttributeTypedArgument_t2723150157 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1465843424*)L_0, (CustomAttributeTypedArgument_t2723150157 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2929789526_gshared (ArrayReadOnlyList_1_t3913667990 * __this, int32_t ___index0, CustomAttributeTypedArgument_t2723150157  ___item1, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m1443718646_gshared (ArrayReadOnlyList_1_t3913667990 * __this, CustomAttributeTypedArgument_t2723150157  ___item0, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m791018368_gshared (ArrayReadOnlyList_1_t3913667990 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ((  Exception_t1436737249 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern "C"  Exception_t1436737249 * ArrayReadOnlyList_1_ReadOnlyError_m865416608_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m865416608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_0, (String_t*)_stringLiteral3853944826, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2980597965_gshared (InternalEnumerator_1_t1801353056 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2980597965_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1801353056 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1801353056 *>(__this + 1);
	InternalEnumerator_1__ctor_m2980597965(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1394395022_gshared (InternalEnumerator_1_t1801353056 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1394395022_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1801353056 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1801353056 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1394395022(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3121310385_gshared (InternalEnumerator_1_t1801353056 * __this, const MethodInfo* method)
{
	{
		ArrayMetadata_t894288939  L_0 = InternalEnumerator_1_get_Current_m2746872357((InternalEnumerator_1_t1801353056 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ArrayMetadata_t894288939  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3121310385_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1801353056 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1801353056 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3121310385(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3200291779_gshared (InternalEnumerator_1_t1801353056 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3200291779_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1801353056 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1801353056 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3200291779(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1439724028_gshared (InternalEnumerator_1_t1801353056 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1439724028_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1801353056 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1801353056 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1439724028(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::get_Current()
extern "C"  ArrayMetadata_t894288939  InternalEnumerator_1_get_Current_m2746872357_gshared (InternalEnumerator_1_t1801353056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2746872357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ArrayMetadata_t894288939  L_8 = ((  ArrayMetadata_t894288939  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ArrayMetadata_t894288939  InternalEnumerator_1_get_Current_m2746872357_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1801353056 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1801353056 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2746872357(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3193665258_gshared (InternalEnumerator_1_t178381343 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3193665258_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t178381343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t178381343 *>(__this + 1);
	InternalEnumerator_1__ctor_m3193665258(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2006725664_gshared (InternalEnumerator_1_t178381343 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2006725664_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t178381343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t178381343 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2006725664(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m177159026_gshared (InternalEnumerator_1_t178381343 * __this, const MethodInfo* method)
{
	{
		ObjectMetadata_t3566284522  L_0 = InternalEnumerator_1_get_Current_m2608927213((InternalEnumerator_1_t178381343 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectMetadata_t3566284522  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m177159026_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t178381343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t178381343 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m177159026(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2717850677_gshared (InternalEnumerator_1_t178381343 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2717850677_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t178381343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t178381343 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2717850677(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2752374182_gshared (InternalEnumerator_1_t178381343 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2752374182_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t178381343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t178381343 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2752374182(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::get_Current()
extern "C"  ObjectMetadata_t3566284522  InternalEnumerator_1_get_Current_m2608927213_gshared (InternalEnumerator_1_t178381343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2608927213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ObjectMetadata_t3566284522  L_8 = ((  ObjectMetadata_t3566284522  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ObjectMetadata_t3566284522  InternalEnumerator_1_get_Current_m2608927213_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t178381343 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t178381343 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2608927213(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3337644433_gshared (InternalEnumerator_1_t339537294 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3337644433_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t339537294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t339537294 *>(__this + 1);
	InternalEnumerator_1__ctor_m3337644433(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1694348991_gshared (InternalEnumerator_1_t339537294 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1694348991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t339537294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t339537294 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1694348991(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3865845908_gshared (InternalEnumerator_1_t339537294 * __this, const MethodInfo* method)
{
	{
		PropertyMetadata_t3727440473  L_0 = InternalEnumerator_1_get_Current_m2597556806((InternalEnumerator_1_t339537294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		PropertyMetadata_t3727440473  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3865845908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t339537294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t339537294 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3865845908(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1606295072_gshared (InternalEnumerator_1_t339537294 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1606295072_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t339537294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t339537294 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1606295072(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m299856312_gshared (InternalEnumerator_1_t339537294 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m299856312_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t339537294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t339537294 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m299856312(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::get_Current()
extern "C"  PropertyMetadata_t3727440473  InternalEnumerator_1_get_Current_m2597556806_gshared (InternalEnumerator_1_t339537294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2597556806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		PropertyMetadata_t3727440473  L_8 = ((  PropertyMetadata_t3727440473  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  PropertyMetadata_t3727440473  InternalEnumerator_1_get_Current_m2597556806_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t339537294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t339537294 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2597556806(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1359891754_gshared (InternalEnumerator_1_t4239932009 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1359891754_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4239932009 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239932009 *>(__this + 1);
	InternalEnumerator_1__ctor_m1359891754(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m81420524_gshared (InternalEnumerator_1_t4239932009 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m81420524_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239932009 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239932009 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m81420524(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2980550840_gshared (InternalEnumerator_1_t4239932009 * __this, const MethodInfo* method)
{
	{
		TableRange_t3332867892  L_0 = InternalEnumerator_1_get_Current_m4245242303((InternalEnumerator_1_t4239932009 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TableRange_t3332867892  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2980550840_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239932009 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239932009 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2980550840(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m33109155_gshared (InternalEnumerator_1_t4239932009 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m33109155_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239932009 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239932009 *>(__this + 1);
	InternalEnumerator_1_Dispose_m33109155(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4138845038_gshared (InternalEnumerator_1_t4239932009 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4138845038_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239932009 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239932009 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4138845038(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern "C"  TableRange_t3332867892  InternalEnumerator_1_get_Current_m4245242303_gshared (InternalEnumerator_1_t4239932009 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4245242303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t3332867892  L_8 = ((  TableRange_t3332867892  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TableRange_t3332867892  InternalEnumerator_1_get_Current_m4245242303_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239932009 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239932009 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4245242303(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3734861738_gshared (InternalEnumerator_1_t1911769025 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3734861738_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1911769025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1911769025 *>(__this + 1);
	InternalEnumerator_1__ctor_m3734861738(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2707779927_gshared (InternalEnumerator_1_t1911769025 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2707779927_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1911769025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1911769025 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2707779927(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m55999184_gshared (InternalEnumerator_1_t1911769025 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1708547365((InternalEnumerator_1_t1911769025 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m55999184_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1911769025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1911769025 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m55999184(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2447779733_gshared (InternalEnumerator_1_t1911769025 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2447779733_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1911769025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1911769025 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2447779733(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2850975202_gshared (InternalEnumerator_1_t1911769025 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2850975202_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1911769025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1911769025 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2850975202(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1708547365_gshared (InternalEnumerator_1_t1911769025 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1708547365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1708547365_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1911769025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1911769025 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1708547365(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m320219217_gshared (InternalEnumerator_1_t380507487 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m320219217_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t380507487 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t380507487 *>(__this + 1);
	InternalEnumerator_1__ctor_m320219217(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3916136950_gshared (InternalEnumerator_1_t380507487 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3916136950_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t380507487 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t380507487 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3916136950(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m159594892_gshared (InternalEnumerator_1_t380507487 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3879265245((InternalEnumerator_1_t380507487 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m159594892_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t380507487 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t380507487 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m159594892(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1212749330_gshared (InternalEnumerator_1_t380507487 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1212749330_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t380507487 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t380507487 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1212749330(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3280316341_gshared (InternalEnumerator_1_t380507487 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3280316341_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t380507487 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t380507487 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3280316341(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<NendUnityPlugin.Common.Gravity>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3879265245_gshared (InternalEnumerator_1_t380507487 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3879265245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3879265245_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t380507487 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t380507487 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3879265245(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3349908318_gshared (InternalEnumerator_1_t1004352082 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3349908318_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1004352082 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1004352082 *>(__this + 1);
	InternalEnumerator_1__ctor_m3349908318(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m903423974_gshared (InternalEnumerator_1_t1004352082 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m903423974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1004352082 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1004352082 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m903423974(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1503522504_gshared (InternalEnumerator_1_t1004352082 * __this, const MethodInfo* method)
{
	{
		bool L_0 = InternalEnumerator_1_get_Current_m2100201398((InternalEnumerator_1_t1004352082 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1503522504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1004352082 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1004352082 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1503522504(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2018798800_gshared (InternalEnumerator_1_t1004352082 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2018798800_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1004352082 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1004352082 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2018798800(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m154749640_gshared (InternalEnumerator_1_t1004352082 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m154749640_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1004352082 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1004352082 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m154749640(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern "C"  bool InternalEnumerator_1_get_Current_m2100201398_gshared (InternalEnumerator_1_t1004352082 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2100201398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bool L_8 = ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  bool InternalEnumerator_1_get_Current_m2100201398_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1004352082 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1004352082 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2100201398(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4191108945_gshared (InternalEnumerator_1_t2041360493 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4191108945_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2041360493 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2041360493 *>(__this + 1);
	InternalEnumerator_1__ctor_m4191108945(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3327951435_gshared (InternalEnumerator_1_t2041360493 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3327951435_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2041360493 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2041360493 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3327951435(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1277470738_gshared (InternalEnumerator_1_t2041360493 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m3073360606((InternalEnumerator_1_t2041360493 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1277470738_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2041360493 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2041360493 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1277470738(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3493290831_gshared (InternalEnumerator_1_t2041360493 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3493290831_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2041360493 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2041360493 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3493290831(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m123458112_gshared (InternalEnumerator_1_t2041360493 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m123458112_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2041360493 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2041360493 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m123458112(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m3073360606_gshared (InternalEnumerator_1_t2041360493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3073360606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m3073360606_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2041360493 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2041360493 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3073360606(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2123683127_gshared (InternalEnumerator_1_t246557291 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2123683127_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t246557291 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246557291 *>(__this + 1);
	InternalEnumerator_1__ctor_m2123683127(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2793870849_gshared (InternalEnumerator_1_t246557291 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2793870849_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246557291 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246557291 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2793870849(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1641466962_gshared (InternalEnumerator_1_t246557291 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = InternalEnumerator_1_get_Current_m1945804797((InternalEnumerator_1_t246557291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppChar L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1641466962_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246557291 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246557291 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1641466962(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m881342307_gshared (InternalEnumerator_1_t246557291 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m881342307_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246557291 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246557291 *>(__this + 1);
	InternalEnumerator_1_Dispose_m881342307(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1909384544_gshared (InternalEnumerator_1_t246557291 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1909384544_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246557291 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246557291 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1909384544(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m1945804797_gshared (InternalEnumerator_1_t246557291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1945804797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppChar L_8 = ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m1945804797_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246557291 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246557291 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1945804797(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2336656763_gshared (InternalEnumerator_1_t4031039755 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2336656763_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4031039755 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4031039755 *>(__this + 1);
	InternalEnumerator_1__ctor_m2336656763(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2336872218_gshared (InternalEnumerator_1_t4031039755 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2336872218_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4031039755 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4031039755 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2336872218(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1295084274_gshared (InternalEnumerator_1_t4031039755 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t3123975638  L_0 = InternalEnumerator_1_get_Current_m1920303382((InternalEnumerator_1_t4031039755 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DictionaryEntry_t3123975638  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1295084274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4031039755 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4031039755 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1295084274(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2648133761_gshared (InternalEnumerator_1_t4031039755 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2648133761_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4031039755 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4031039755 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2648133761(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2577879725_gshared (InternalEnumerator_1_t4031039755 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2577879725_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4031039755 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4031039755 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2577879725(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern "C"  DictionaryEntry_t3123975638  InternalEnumerator_1_get_Current_m1920303382_gshared (InternalEnumerator_1_t4031039755 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1920303382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DictionaryEntry_t3123975638  L_8 = ((  DictionaryEntry_t3123975638  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3123975638  InternalEnumerator_1_get_Current_m1920303382_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4031039755 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4031039755 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1920303382(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m619554185_gshared (InternalEnumerator_1_t4116331090 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m619554185_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4116331090 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4116331090 *>(__this + 1);
	InternalEnumerator_1__ctor_m619554185(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3677481164_gshared (InternalEnumerator_1_t4116331090 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3677481164_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4116331090 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4116331090 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3677481164(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2867624895_gshared (InternalEnumerator_1_t4116331090 * __this, const MethodInfo* method)
{
	{
		Link_t3209266973  L_0 = InternalEnumerator_1_get_Current_m1845246162((InternalEnumerator_1_t4116331090 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t3209266973  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2867624895_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4116331090 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4116331090 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2867624895(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m326441406_gshared (InternalEnumerator_1_t4116331090 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m326441406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4116331090 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4116331090 *>(__this + 1);
	InternalEnumerator_1_Dispose_m326441406(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1457790320_gshared (InternalEnumerator_1_t4116331090 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1457790320_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4116331090 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4116331090 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1457790320(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern "C"  Link_t3209266973  InternalEnumerator_1_get_Current_m1845246162_gshared (InternalEnumerator_1_t4116331090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1845246162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t3209266973  L_8 = ((  Link_t3209266973  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t3209266973  InternalEnumerator_1_get_Current_m1845246162_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4116331090 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4116331090 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1845246162(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1424655733_gshared (InternalEnumerator_1_t978588483 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1424655733_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t978588483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t978588483 *>(__this + 1);
	InternalEnumerator_1__ctor_m1424655733(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1318888374_gshared (InternalEnumerator_1_t978588483 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1318888374_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t978588483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t978588483 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1318888374(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3575233890_gshared (InternalEnumerator_1_t978588483 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t71524366  L_0 = InternalEnumerator_1_get_Current_m4241643334((InternalEnumerator_1_t978588483 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t71524366  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3575233890_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t978588483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t978588483 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3575233890(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m435531507_gshared (InternalEnumerator_1_t978588483 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m435531507_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t978588483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t978588483 *>(__this + 1);
	InternalEnumerator_1_Dispose_m435531507(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1543390728_gshared (InternalEnumerator_1_t978588483 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1543390728_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t978588483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t978588483 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1543390728(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t71524366  InternalEnumerator_1_get_Current_m4241643334_gshared (InternalEnumerator_1_t978588483 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4241643334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t71524366  L_8 = ((  KeyValuePair_2_t71524366  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t71524366  InternalEnumerator_1_get_Current_m4241643334_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t978588483 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t978588483 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4241643334(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1816464321_gshared (InternalEnumerator_1_t1251464211 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1816464321_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1251464211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1251464211 *>(__this + 1);
	InternalEnumerator_1__ctor_m1816464321(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3362997282_gshared (InternalEnumerator_1_t1251464211 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3362997282_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1251464211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1251464211 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3362997282(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m616568955_gshared (InternalEnumerator_1_t1251464211 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t344400094  L_0 = InternalEnumerator_1_get_Current_m3775994610((InternalEnumerator_1_t1251464211 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t344400094  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m616568955_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1251464211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1251464211 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m616568955(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1298284700_gshared (InternalEnumerator_1_t1251464211 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1298284700_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1251464211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1251464211 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1298284700(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2711395376_gshared (InternalEnumerator_1_t1251464211 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2711395376_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1251464211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1251464211 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2711395376(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::get_Current()
extern "C"  KeyValuePair_2_t344400094  InternalEnumerator_1_get_Current_m3775994610_gshared (InternalEnumerator_1_t1251464211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3775994610_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t344400094  L_8 = ((  KeyValuePair_2_t344400094  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t344400094  InternalEnumerator_1_get_Current_m3775994610_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1251464211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1251464211 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3775994610(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4107498329_gshared (InternalEnumerator_1_t3923459794 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4107498329_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3923459794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3923459794 *>(__this + 1);
	InternalEnumerator_1__ctor_m4107498329(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2625604376_gshared (InternalEnumerator_1_t3923459794 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2625604376_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3923459794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3923459794 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2625604376(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4197773997_gshared (InternalEnumerator_1_t3923459794 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3016395677  L_0 = InternalEnumerator_1_get_Current_m4006695586((InternalEnumerator_1_t3923459794 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3016395677  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4197773997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3923459794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3923459794 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4197773997(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3269441358_gshared (InternalEnumerator_1_t3923459794 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3269441358_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3923459794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3923459794 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3269441358(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2156158931_gshared (InternalEnumerator_1_t3923459794 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2156158931_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3923459794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3923459794 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2156158931(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::get_Current()
extern "C"  KeyValuePair_2_t3016395677  InternalEnumerator_1_get_Current_m4006695586_gshared (InternalEnumerator_1_t3923459794 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4006695586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3016395677  L_8 = ((  KeyValuePair_2_t3016395677  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3016395677  InternalEnumerator_1_get_Current_m4006695586_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3923459794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3923459794 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4006695586(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3117895283_gshared (InternalEnumerator_1_t4084615745 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3117895283_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4084615745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4084615745 *>(__this + 1);
	InternalEnumerator_1__ctor_m3117895283(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871276011_gshared (InternalEnumerator_1_t4084615745 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871276011_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4084615745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4084615745 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871276011(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1716856402_gshared (InternalEnumerator_1_t4084615745 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3177551628  L_0 = InternalEnumerator_1_get_Current_m3781046((InternalEnumerator_1_t4084615745 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3177551628  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1716856402_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4084615745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4084615745 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1716856402(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3566201632_gshared (InternalEnumerator_1_t4084615745 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3566201632_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4084615745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4084615745 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3566201632(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2542460092_gshared (InternalEnumerator_1_t4084615745 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2542460092_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4084615745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4084615745 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2542460092(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::get_Current()
extern "C"  KeyValuePair_2_t3177551628  InternalEnumerator_1_get_Current_m3781046_gshared (InternalEnumerator_1_t4084615745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3781046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3177551628  L_8 = ((  KeyValuePair_2_t3177551628  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3177551628  InternalEnumerator_1_get_Current_m3781046_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4084615745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4084615745 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3781046(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m962177456_gshared (InternalEnumerator_1_t454463237 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m962177456_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t454463237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t454463237 *>(__this + 1);
	InternalEnumerator_1__ctor_m962177456(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1716381123_gshared (InternalEnumerator_1_t454463237 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1716381123_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t454463237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t454463237 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1716381123(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2889979481_gshared (InternalEnumerator_1_t454463237 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3842366416  L_0 = InternalEnumerator_1_get_Current_m476140818((InternalEnumerator_1_t454463237 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3842366416  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2889979481_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t454463237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t454463237 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2889979481(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1290015243_gshared (InternalEnumerator_1_t454463237 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1290015243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t454463237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t454463237 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1290015243(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3299696349_gshared (InternalEnumerator_1_t454463237 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3299696349_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t454463237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t454463237 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3299696349(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern "C"  KeyValuePair_2_t3842366416  InternalEnumerator_1_get_Current_m476140818_gshared (InternalEnumerator_1_t454463237 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m476140818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3842366416  L_8 = ((  KeyValuePair_2_t3842366416  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3842366416  InternalEnumerator_1_get_Current_m476140818_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t454463237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t454463237 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m476140818(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3402055178_gshared (InternalEnumerator_1_t951840635 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3402055178_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t951840635 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t951840635 *>(__this + 1);
	InternalEnumerator_1__ctor_m3402055178(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4046360160_gshared (InternalEnumerator_1_t951840635 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4046360160_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t951840635 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t951840635 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4046360160(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2328184924_gshared (InternalEnumerator_1_t951840635 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t44776518  L_0 = InternalEnumerator_1_get_Current_m1679394243((InternalEnumerator_1_t951840635 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t44776518  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2328184924_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t951840635 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t951840635 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2328184924(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m537193559_gshared (InternalEnumerator_1_t951840635 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m537193559_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t951840635 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t951840635 *>(__this + 1);
	InternalEnumerator_1_Dispose_m537193559(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2000685912_gshared (InternalEnumerator_1_t951840635 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2000685912_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t951840635 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t951840635 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2000685912(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Double>>::get_Current()
extern "C"  KeyValuePair_2_t44776518  InternalEnumerator_1_get_Current_m1679394243_gshared (InternalEnumerator_1_t951840635 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1679394243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t44776518  L_8 = ((  KeyValuePair_2_t44776518  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t44776518  InternalEnumerator_1_get_Current_m1679394243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t951840635 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t951840635 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1679394243(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1341209356_gshared (InternalEnumerator_1_t3308121025 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1341209356_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3308121025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3308121025 *>(__this + 1);
	InternalEnumerator_1__ctor_m1341209356(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4239728915_gshared (InternalEnumerator_1_t3308121025 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4239728915_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3308121025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3308121025 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4239728915(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2284280372_gshared (InternalEnumerator_1_t3308121025 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2401056908  L_0 = InternalEnumerator_1_get_Current_m3081223448((InternalEnumerator_1_t3308121025 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2401056908  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2284280372_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3308121025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3308121025 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2284280372(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4098771594_gshared (InternalEnumerator_1_t3308121025 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4098771594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3308121025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3308121025 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4098771594(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2951889983_gshared (InternalEnumerator_1_t3308121025 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2951889983_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3308121025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3308121025 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2951889983(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t2401056908  InternalEnumerator_1_get_Current_m3081223448_gshared (InternalEnumerator_1_t3308121025 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3081223448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2401056908  L_8 = ((  KeyValuePair_2_t2401056908  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2401056908  InternalEnumerator_1_get_Current_m3081223448_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3308121025 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3308121025 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3081223448(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m807987550_gshared (InternalEnumerator_1_t3437281436 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m807987550_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3437281436 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3437281436 *>(__this + 1);
	InternalEnumerator_1__ctor_m807987550(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2386791007_gshared (InternalEnumerator_1_t3437281436 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2386791007_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3437281436 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3437281436 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2386791007(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141782011_gshared (InternalEnumerator_1_t3437281436 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2530217319  L_0 = InternalEnumerator_1_get_Current_m923139215((InternalEnumerator_1_t3437281436 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2530217319  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141782011_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3437281436 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3437281436 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141782011(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2342933386_gshared (InternalEnumerator_1_t3437281436 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2342933386_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3437281436 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3437281436 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2342933386(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1773160976_gshared (InternalEnumerator_1_t3437281436 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1773160976_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3437281436 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3437281436 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1773160976(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2530217319  InternalEnumerator_1_get_Current_m923139215_gshared (InternalEnumerator_1_t3437281436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m923139215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2530217319  L_8 = ((  KeyValuePair_2_t2530217319  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2530217319  InternalEnumerator_1_get_Current_m923139215_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3437281436 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3437281436 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m923139215(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3390957028_gshared (InternalEnumerator_1_t1451382081 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3390957028_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1451382081 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1451382081 *>(__this + 1);
	InternalEnumerator_1__ctor_m3390957028(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1223176161_gshared (InternalEnumerator_1_t1451382081 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1223176161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1451382081 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1451382081 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1223176161(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1676501075_gshared (InternalEnumerator_1_t1451382081 * __this, const MethodInfo* method)
{
	{
		Link_t544317964  L_0 = InternalEnumerator_1_get_Current_m2223614542((InternalEnumerator_1_t1451382081 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t544317964  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1676501075_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1451382081 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1451382081 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1676501075(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1913545470_gshared (InternalEnumerator_1_t1451382081 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1913545470_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1451382081 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1451382081 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1913545470(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2785895009_gshared (InternalEnumerator_1_t1451382081 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2785895009_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1451382081 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1451382081 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2785895009(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern "C"  Link_t544317964  InternalEnumerator_1_get_Current_m2223614542_gshared (InternalEnumerator_1_t1451382081 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2223614542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t544317964  L_8 = ((  Link_t544317964  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t544317964  InternalEnumerator_1_get_Current_m2223614542_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1451382081 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1451382081 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2223614542(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1196506529_gshared (InternalEnumerator_1_t587985571 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1196506529_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t587985571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t587985571 *>(__this + 1);
	InternalEnumerator_1__ctor_m1196506529(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1256724261_gshared (InternalEnumerator_1_t587985571 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1256724261_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t587985571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t587985571 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1256724261(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1514266661_gshared (InternalEnumerator_1_t587985571 * __this, const MethodInfo* method)
{
	{
		Slot_t3975888750  L_0 = InternalEnumerator_1_get_Current_m3008260692((InternalEnumerator_1_t587985571 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t3975888750  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1514266661_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t587985571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t587985571 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1514266661(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1995846647_gshared (InternalEnumerator_1_t587985571 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1995846647_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t587985571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t587985571 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1995846647(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3200332883_gshared (InternalEnumerator_1_t587985571 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3200332883_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t587985571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t587985571 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3200332883(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern "C"  Slot_t3975888750  InternalEnumerator_1_get_Current_m3008260692_gshared (InternalEnumerator_1_t587985571 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3008260692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t3975888750  L_8 = ((  Slot_t3975888750  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Slot_t3975888750  InternalEnumerator_1_get_Current_m3008260692_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t587985571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t587985571 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3008260692(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m819716934_gshared (InternalEnumerator_1_t1291559127 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m819716934_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1291559127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1291559127 *>(__this + 1);
	InternalEnumerator_1__ctor_m819716934(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4202665280_gshared (InternalEnumerator_1_t1291559127 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4202665280_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1291559127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1291559127 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4202665280(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2629988057_gshared (InternalEnumerator_1_t1291559127 * __this, const MethodInfo* method)
{
	{
		Slot_t384495010  L_0 = InternalEnumerator_1_get_Current_m2832154098((InternalEnumerator_1_t1291559127 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t384495010  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2629988057_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1291559127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1291559127 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2629988057(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1519877610_gshared (InternalEnumerator_1_t1291559127 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1519877610_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1291559127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1291559127 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1519877610(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3354536447_gshared (InternalEnumerator_1_t1291559127 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3354536447_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1291559127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1291559127 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3354536447(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern "C"  Slot_t384495010  InternalEnumerator_1_get_Current_m2832154098_gshared (InternalEnumerator_1_t1291559127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2832154098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t384495010  L_8 = ((  Slot_t384495010  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Slot_t384495010  InternalEnumerator_1_get_Current_m2832154098_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1291559127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1291559127 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2832154098(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3456047704_gshared (InternalEnumerator_1_t350626606 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3456047704_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t350626606 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t350626606 *>(__this + 1);
	InternalEnumerator_1__ctor_m3456047704(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m546509994_gshared (InternalEnumerator_1_t350626606 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m546509994_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t350626606 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t350626606 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m546509994(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2742943179_gshared (InternalEnumerator_1_t350626606 * __this, const MethodInfo* method)
{
	{
		DateTime_t3738529785  L_0 = InternalEnumerator_1_get_Current_m3225386639((InternalEnumerator_1_t350626606 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DateTime_t3738529785  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2742943179_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t350626606 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t350626606 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2742943179(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m649519051_gshared (InternalEnumerator_1_t350626606 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m649519051_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t350626606 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t350626606 *>(__this + 1);
	InternalEnumerator_1_Dispose_m649519051(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1161444633_gshared (InternalEnumerator_1_t350626606 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1161444633_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t350626606 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t350626606 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1161444633(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern "C"  DateTime_t3738529785  InternalEnumerator_1_get_Current_m3225386639_gshared (InternalEnumerator_1_t350626606 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3225386639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DateTime_t3738529785  L_8 = ((  DateTime_t3738529785  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DateTime_t3738529785  InternalEnumerator_1_get_Current_m3225386639_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t350626606 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t350626606 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3225386639(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m143506773_gshared (InternalEnumerator_1_t3855323497 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m143506773_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3855323497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3855323497 *>(__this + 1);
	InternalEnumerator_1__ctor_m143506773(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m169899350_gshared (InternalEnumerator_1_t3855323497 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m169899350_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3855323497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3855323497 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m169899350(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m688818811_gshared (InternalEnumerator_1_t3855323497 * __this, const MethodInfo* method)
{
	{
		Decimal_t2948259380  L_0 = InternalEnumerator_1_get_Current_m2128158355((InternalEnumerator_1_t3855323497 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Decimal_t2948259380  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m688818811_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3855323497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3855323497 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m688818811(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m937653815_gshared (InternalEnumerator_1_t3855323497 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m937653815_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3855323497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3855323497 *>(__this + 1);
	InternalEnumerator_1_Dispose_m937653815(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4210671224_gshared (InternalEnumerator_1_t3855323497 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4210671224_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3855323497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3855323497 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4210671224(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern "C"  Decimal_t2948259380  InternalEnumerator_1_get_Current_m2128158355_gshared (InternalEnumerator_1_t3855323497 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2128158355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Decimal_t2948259380  L_8 = ((  Decimal_t2948259380  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Decimal_t2948259380  InternalEnumerator_1_get_Current_m2128158355_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3855323497 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3855323497 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2128158355(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1559487635_gshared (InternalEnumerator_1_t1501729480 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1559487635_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1501729480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1501729480 *>(__this + 1);
	InternalEnumerator_1__ctor_m1559487635(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14983211_gshared (InternalEnumerator_1_t1501729480 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14983211_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1501729480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1501729480 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14983211(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3853320011_gshared (InternalEnumerator_1_t1501729480 * __this, const MethodInfo* method)
{
	{
		double L_0 = InternalEnumerator_1_get_Current_m2894466703((InternalEnumerator_1_t1501729480 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3853320011_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1501729480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1501729480 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3853320011(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2043273260_gshared (InternalEnumerator_1_t1501729480 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2043273260_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1501729480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1501729480 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2043273260(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m377783729_gshared (InternalEnumerator_1_t1501729480 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m377783729_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1501729480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1501729480 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m377783729(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern "C"  double InternalEnumerator_1_get_Current_m2894466703_gshared (InternalEnumerator_1_t1501729480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2894466703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		double L_8 = ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  double InternalEnumerator_1_get_Current_m2894466703_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1501729480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1501729480 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2894466703(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2910272776_gshared (InternalEnumerator_1_t3459884504 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2910272776_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3459884504 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3459884504 *>(__this + 1);
	InternalEnumerator_1__ctor_m2910272776(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m521819017_gshared (InternalEnumerator_1_t3459884504 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m521819017_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3459884504 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3459884504 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m521819017(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888718134_gshared (InternalEnumerator_1_t3459884504 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = InternalEnumerator_1_get_Current_m2723520268((InternalEnumerator_1_t3459884504 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888718134_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3459884504 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3459884504 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888718134(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3716424577_gshared (InternalEnumerator_1_t3459884504 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3716424577_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3459884504 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3459884504 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3716424577(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3525157932_gshared (InternalEnumerator_1_t3459884504 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3525157932_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3459884504 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3459884504 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3525157932(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern "C"  int16_t InternalEnumerator_1_get_Current_m2723520268_gshared (InternalEnumerator_1_t3459884504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2723520268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int16_t L_8 = ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int16_t InternalEnumerator_1_get_Current_m2723520268_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3459884504 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3459884504 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2723520268(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m217498388_gshared (InternalEnumerator_1_t3858009870 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m217498388_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3858009870 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3858009870 *>(__this + 1);
	InternalEnumerator_1__ctor_m217498388(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m779787360_gshared (InternalEnumerator_1_t3858009870 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m779787360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3858009870 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3858009870 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m779787360(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2435291801_gshared (InternalEnumerator_1_t3858009870 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3500427238((InternalEnumerator_1_t3858009870 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2435291801_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3858009870 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3858009870 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2435291801(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3519406884_gshared (InternalEnumerator_1_t3858009870 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3519406884_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3858009870 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3858009870 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3519406884(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3802174768_gshared (InternalEnumerator_1_t3858009870 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3802174768_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3858009870 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3858009870 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3802174768(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3500427238_gshared (InternalEnumerator_1_t3858009870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3500427238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3500427238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3858009870 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3858009870 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3500427238(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4055378331_gshared (InternalEnumerator_1_t348664125 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4055378331_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t348664125 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t348664125 *>(__this + 1);
	InternalEnumerator_1__ctor_m4055378331(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3873091784_gshared (InternalEnumerator_1_t348664125 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3873091784_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t348664125 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t348664125 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3873091784(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m180319738_gshared (InternalEnumerator_1_t348664125 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = InternalEnumerator_1_get_Current_m685192625((InternalEnumerator_1_t348664125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m180319738_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t348664125 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t348664125 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m180319738(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m830510730_gshared (InternalEnumerator_1_t348664125 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m830510730_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t348664125 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t348664125 *>(__this + 1);
	InternalEnumerator_1_Dispose_m830510730(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4266213580_gshared (InternalEnumerator_1_t348664125 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4266213580_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t348664125 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t348664125 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4266213580(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C"  int64_t InternalEnumerator_1_get_Current_m685192625_gshared (InternalEnumerator_1_t348664125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m685192625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int64_t L_8 = ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int64_t InternalEnumerator_1_get_Current_m685192625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t348664125 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t348664125 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m685192625(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1579105305_gshared (InternalEnumerator_1_t1747214298 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1579105305_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1747214298 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747214298 *>(__this + 1);
	InternalEnumerator_1__ctor_m1579105305(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3011999097_gshared (InternalEnumerator_1_t1747214298 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3011999097_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1747214298 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747214298 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3011999097(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m548105685_gshared (InternalEnumerator_1_t1747214298 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = InternalEnumerator_1_get_Current_m3900374024((InternalEnumerator_1_t1747214298 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m548105685_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1747214298 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747214298 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m548105685(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2818366163_gshared (InternalEnumerator_1_t1747214298 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2818366163_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1747214298 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747214298 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2818366163(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3520556285_gshared (InternalEnumerator_1_t1747214298 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3520556285_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1747214298 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747214298 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3520556285(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m3900374024_gshared (InternalEnumerator_1_t1747214298 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3900374024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IntPtr_t L_8 = ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m3900374024_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1747214298 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747214298 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3900374024(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1675719794_gshared (InternalEnumerator_1_t3987170281 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1675719794_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3987170281 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3987170281 *>(__this + 1);
	InternalEnumerator_1__ctor_m1675719794(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2914412419_gshared (InternalEnumerator_1_t3987170281 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2914412419_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3987170281 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3987170281 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2914412419(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2982675020_gshared (InternalEnumerator_1_t3987170281 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = InternalEnumerator_1_get_Current_m3839250771((InternalEnumerator_1_t3987170281 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2982675020_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3987170281 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3987170281 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2982675020(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2006926799_gshared (InternalEnumerator_1_t3987170281 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2006926799_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3987170281 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3987170281 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2006926799(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4035695998_gshared (InternalEnumerator_1_t3987170281 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4035695998_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3987170281 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3987170281 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4035695998(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m3839250771_gshared (InternalEnumerator_1_t3987170281 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3839250771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m3839250771_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3987170281 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3987170281 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3839250771(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m643493702_gshared (InternalEnumerator_1_t1194929827 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m643493702_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1194929827 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1194929827 *>(__this + 1);
	InternalEnumerator_1__ctor_m643493702(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m895873066_gshared (InternalEnumerator_1_t1194929827 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m895873066_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1194929827 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1194929827 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m895873066(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4189894603_gshared (InternalEnumerator_1_t1194929827 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t287865710  L_0 = InternalEnumerator_1_get_Current_m3354878040((InternalEnumerator_1_t1194929827 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeNamedArgument_t287865710  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4189894603_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1194929827 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1194929827 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4189894603(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3577625655_gshared (InternalEnumerator_1_t1194929827 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3577625655_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1194929827 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1194929827 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3577625655(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4138204635_gshared (InternalEnumerator_1_t1194929827 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4138204635_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1194929827 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1194929827 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4138204635(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t287865710  InternalEnumerator_1_get_Current_m3354878040_gshared (InternalEnumerator_1_t1194929827 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3354878040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeNamedArgument_t287865710  L_8 = ((  CustomAttributeNamedArgument_t287865710  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  CustomAttributeNamedArgument_t287865710  InternalEnumerator_1_get_Current_m3354878040_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1194929827 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1194929827 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3354878040(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m748741755_gshared (InternalEnumerator_1_t3630214274 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m748741755_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3630214274 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3630214274 *>(__this + 1);
	InternalEnumerator_1__ctor_m748741755(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m596870847_gshared (InternalEnumerator_1_t3630214274 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m596870847_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3630214274 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3630214274 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m596870847(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4213507601_gshared (InternalEnumerator_1_t3630214274 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t2723150157  L_0 = InternalEnumerator_1_get_Current_m3653231044((InternalEnumerator_1_t3630214274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeTypedArgument_t2723150157  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4213507601_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3630214274 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3630214274 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4213507601(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2438347491_gshared (InternalEnumerator_1_t3630214274 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2438347491_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3630214274 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3630214274 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2438347491(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3577491700_gshared (InternalEnumerator_1_t3630214274 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3577491700_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3630214274 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3630214274 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3577491700(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t2723150157  InternalEnumerator_1_get_Current_m3653231044_gshared (InternalEnumerator_1_t3630214274 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3653231044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeTypedArgument_t2723150157  L_8 = ((  CustomAttributeTypedArgument_t2723150157  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  CustomAttributeTypedArgument_t2723150157  InternalEnumerator_1_get_Current_m3653231044_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3630214274 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3630214274 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3653231044(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1486034688_gshared (InternalEnumerator_1_t1267231508 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1486034688_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1267231508 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1267231508 *>(__this + 1);
	InternalEnumerator_1__ctor_m1486034688(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3619766341_gshared (InternalEnumerator_1_t1267231508 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3619766341_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1267231508 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1267231508 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3619766341(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m435848551_gshared (InternalEnumerator_1_t1267231508 * __this, const MethodInfo* method)
{
	{
		LabelData_t360167391  L_0 = InternalEnumerator_1_get_Current_m2698009637((InternalEnumerator_1_t1267231508 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelData_t360167391  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m435848551_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1267231508 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1267231508 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m435848551(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m106460639_gshared (InternalEnumerator_1_t1267231508 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m106460639_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1267231508 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1267231508 *>(__this + 1);
	InternalEnumerator_1_Dispose_m106460639(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1728532725_gshared (InternalEnumerator_1_t1267231508 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1728532725_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1267231508 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1267231508 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1728532725(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
extern "C"  LabelData_t360167391  InternalEnumerator_1_get_Current_m2698009637_gshared (InternalEnumerator_1_t1267231508 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2698009637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelData_t360167391  L_8 = ((  LabelData_t360167391  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  LabelData_t360167391  InternalEnumerator_1_get_Current_m2698009637_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1267231508 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1267231508 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2698009637(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1594304290_gshared (InternalEnumerator_1_t1765566171 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1594304290_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1765566171 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765566171 *>(__this + 1);
	InternalEnumerator_1__ctor_m1594304290(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m720350288_gshared (InternalEnumerator_1_t1765566171 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m720350288_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1765566171 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765566171 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m720350288(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1976902927_gshared (InternalEnumerator_1_t1765566171 * __this, const MethodInfo* method)
{
	{
		LabelFixup_t858502054  L_0 = InternalEnumerator_1_get_Current_m2680116177((InternalEnumerator_1_t1765566171 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelFixup_t858502054  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1976902927_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1765566171 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765566171 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1976902927(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1063909490_gshared (InternalEnumerator_1_t1765566171 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1063909490_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1765566171 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765566171 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1063909490(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2234422530_gshared (InternalEnumerator_1_t1765566171 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2234422530_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1765566171 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765566171 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2234422530(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern "C"  LabelFixup_t858502054  InternalEnumerator_1_get_Current_m2680116177_gshared (InternalEnumerator_1_t1765566171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2680116177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelFixup_t858502054  L_8 = ((  LabelFixup_t858502054  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  LabelFixup_t858502054  InternalEnumerator_1_get_Current_m2680116177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1765566171 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765566171 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2680116177(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m238559784_gshared (InternalEnumerator_1_t3232839231 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m238559784_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3232839231 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3232839231 *>(__this + 1);
	InternalEnumerator_1__ctor_m238559784(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2428767548_gshared (InternalEnumerator_1_t3232839231 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2428767548_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3232839231 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3232839231 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2428767548(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3624751851_gshared (InternalEnumerator_1_t3232839231 * __this, const MethodInfo* method)
{
	{
		ILTokenInfo_t2325775114  L_0 = InternalEnumerator_1_get_Current_m165106323((InternalEnumerator_1_t3232839231 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ILTokenInfo_t2325775114  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3624751851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3232839231 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3232839231 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3624751851(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m130608859_gshared (InternalEnumerator_1_t3232839231 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m130608859_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3232839231 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3232839231 *>(__this + 1);
	InternalEnumerator_1_Dispose_m130608859(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m819973544_gshared (InternalEnumerator_1_t3232839231 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m819973544_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3232839231 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3232839231 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m819973544(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern "C"  ILTokenInfo_t2325775114  InternalEnumerator_1_get_Current_m165106323_gshared (InternalEnumerator_1_t3232839231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m165106323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ILTokenInfo_t2325775114  L_8 = ((  ILTokenInfo_t2325775114  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ILTokenInfo_t2325775114  InternalEnumerator_1_get_Current_m165106323_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3232839231 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3232839231 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m165106323(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m39232262_gshared (InternalEnumerator_1_t2368758583 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m39232262_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2368758583 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2368758583 *>(__this + 1);
	InternalEnumerator_1__ctor_m39232262(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3165277182_gshared (InternalEnumerator_1_t2368758583 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3165277182_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2368758583 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2368758583 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3165277182(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m694606607_gshared (InternalEnumerator_1_t2368758583 * __this, const MethodInfo* method)
{
	{
		ParameterModifier_t1461694466  L_0 = InternalEnumerator_1_get_Current_m356936020((InternalEnumerator_1_t2368758583 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ParameterModifier_t1461694466  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m694606607_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2368758583 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2368758583 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m694606607(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2840529825_gshared (InternalEnumerator_1_t2368758583 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2840529825_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2368758583 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2368758583 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2840529825(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3664960764_gshared (InternalEnumerator_1_t2368758583 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3664960764_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2368758583 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2368758583 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3664960764(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern "C"  ParameterModifier_t1461694466  InternalEnumerator_1_get_Current_m356936020_gshared (InternalEnumerator_1_t2368758583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m356936020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ParameterModifier_t1461694466  L_8 = ((  ParameterModifier_t1461694466  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ParameterModifier_t1461694466  InternalEnumerator_1_get_Current_m356936020_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2368758583 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2368758583 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m356936020(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1999141680_gshared (InternalEnumerator_1_t958356908 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1999141680_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t958356908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t958356908 *>(__this + 1);
	InternalEnumerator_1__ctor_m1999141680(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1259718730_gshared (InternalEnumerator_1_t958356908 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1259718730_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t958356908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t958356908 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1259718730(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141016822_gshared (InternalEnumerator_1_t958356908 * __this, const MethodInfo* method)
{
	{
		ResourceCacheItem_t51292791  L_0 = InternalEnumerator_1_get_Current_m144365666((InternalEnumerator_1_t958356908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceCacheItem_t51292791  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141016822_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t958356908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t958356908 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2141016822(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m887344916_gshared (InternalEnumerator_1_t958356908 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m887344916_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t958356908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t958356908 *>(__this + 1);
	InternalEnumerator_1_Dispose_m887344916(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2960571514_gshared (InternalEnumerator_1_t958356908 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2960571514_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t958356908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t958356908 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2960571514(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::get_Current()
extern "C"  ResourceCacheItem_t51292791  InternalEnumerator_1_get_Current_m144365666_gshared (InternalEnumerator_1_t958356908 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m144365666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceCacheItem_t51292791  L_8 = ((  ResourceCacheItem_t51292791  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ResourceCacheItem_t51292791  InternalEnumerator_1_get_Current_m144365666_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t958356908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t958356908 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m144365666(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2908852803_gshared (InternalEnumerator_1_t3780029419 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2908852803_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3780029419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3780029419 *>(__this + 1);
	InternalEnumerator_1__ctor_m2908852803(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4008893642_gshared (InternalEnumerator_1_t3780029419 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4008893642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3780029419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3780029419 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4008893642(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4235876088_gshared (InternalEnumerator_1_t3780029419 * __this, const MethodInfo* method)
{
	{
		ResourceInfo_t2872965302  L_0 = InternalEnumerator_1_get_Current_m3191242573((InternalEnumerator_1_t3780029419 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceInfo_t2872965302  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4235876088_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3780029419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3780029419 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4235876088(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m725544411_gshared (InternalEnumerator_1_t3780029419 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m725544411_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3780029419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3780029419 *>(__this + 1);
	InternalEnumerator_1_Dispose_m725544411(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3174983217_gshared (InternalEnumerator_1_t3780029419 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3174983217_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3780029419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3780029419 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3174983217(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::get_Current()
extern "C"  ResourceInfo_t2872965302  InternalEnumerator_1_get_Current_m3191242573_gshared (InternalEnumerator_1_t3780029419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3191242573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceInfo_t2872965302  L_8 = ((  ResourceInfo_t2872965302  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ResourceInfo_t2872965302  InternalEnumerator_1_get_Current_m3191242573_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3780029419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3780029419 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3191242573(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4200721464_gshared (InternalEnumerator_1_t153918522 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4200721464_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t153918522 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t153918522 *>(__this + 1);
	InternalEnumerator_1__ctor_m4200721464(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2234754688_gshared (InternalEnumerator_1_t153918522 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2234754688_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t153918522 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t153918522 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2234754688(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3892960115_gshared (InternalEnumerator_1_t153918522 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m3911557813((InternalEnumerator_1_t153918522 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3892960115_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t153918522 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t153918522 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3892960115(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3983612351_gshared (InternalEnumerator_1_t153918522 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3983612351_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t153918522 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t153918522 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3983612351(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1020308708_gshared (InternalEnumerator_1_t153918522 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1020308708_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t153918522 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t153918522 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1020308708(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m3911557813_gshared (InternalEnumerator_1_t153918522 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3911557813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m3911557813_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t153918522 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t153918522 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3911557813(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3460713284_gshared (InternalEnumerator_1_t2576641779 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3460713284_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2576641779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2576641779 *>(__this + 1);
	InternalEnumerator_1__ctor_m3460713284(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3545912565_gshared (InternalEnumerator_1_t2576641779 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3545912565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2576641779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2576641779 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3545912565(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m767948013_gshared (InternalEnumerator_1_t2576641779 * __this, const MethodInfo* method)
{
	{
		int8_t L_0 = InternalEnumerator_1_get_Current_m1408339225((InternalEnumerator_1_t2576641779 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m767948013_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2576641779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2576641779 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m767948013(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1406845627_gshared (InternalEnumerator_1_t2576641779 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1406845627_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2576641779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2576641779 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1406845627(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1015797184_gshared (InternalEnumerator_1_t2576641779 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1015797184_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2576641779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2576641779 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1015797184(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C"  int8_t InternalEnumerator_1_get_Current_m1408339225_gshared (InternalEnumerator_1_t2576641779 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1408339225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int8_t L_8 = ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int8_t InternalEnumerator_1_get_Current_m1408339225_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2576641779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2576641779 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1408339225(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3443175323_gshared (InternalEnumerator_1_t1040666831 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3443175323_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1040666831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1040666831 *>(__this + 1);
	InternalEnumerator_1__ctor_m3443175323(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2314612291_gshared (InternalEnumerator_1_t1040666831 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2314612291_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1040666831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1040666831 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2314612291(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1096730143_gshared (InternalEnumerator_1_t1040666831 * __this, const MethodInfo* method)
{
	{
		X509ChainStatus_t133602714  L_0 = InternalEnumerator_1_get_Current_m2389908135((InternalEnumerator_1_t1040666831 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		X509ChainStatus_t133602714  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1096730143_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1040666831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1040666831 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1096730143(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m469889800_gshared (InternalEnumerator_1_t1040666831 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m469889800_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1040666831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1040666831 *>(__this + 1);
	InternalEnumerator_1_Dispose_m469889800(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1732823414_gshared (InternalEnumerator_1_t1040666831 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1732823414_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1040666831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1040666831 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1732823414(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern "C"  X509ChainStatus_t133602714  InternalEnumerator_1_get_Current_m2389908135_gshared (InternalEnumerator_1_t1040666831 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2389908135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		X509ChainStatus_t133602714  L_8 = ((  X509ChainStatus_t133602714  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  X509ChainStatus_t133602714  InternalEnumerator_1_get_Current_m2389908135_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1040666831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1040666831 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2389908135(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m31115849_gshared (InternalEnumerator_1_t2304330891 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m31115849_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2304330891 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2304330891 *>(__this + 1);
	InternalEnumerator_1__ctor_m31115849(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181721336_gshared (InternalEnumerator_1_t2304330891 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181721336_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2304330891 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2304330891 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181721336(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1823542095_gshared (InternalEnumerator_1_t2304330891 * __this, const MethodInfo* method)
{
	{
		float L_0 = InternalEnumerator_1_get_Current_m2612852447((InternalEnumerator_1_t2304330891 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1823542095_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2304330891 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2304330891 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1823542095(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m630370856_gshared (InternalEnumerator_1_t2304330891 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m630370856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2304330891 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2304330891 *>(__this + 1);
	InternalEnumerator_1_Dispose_m630370856(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3134701632_gshared (InternalEnumerator_1_t2304330891 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3134701632_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2304330891 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2304330891 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3134701632(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern "C"  float InternalEnumerator_1_get_Current_m2612852447_gshared (InternalEnumerator_1_t2304330891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2612852447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		float L_8 = ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  float InternalEnumerator_1_get_Current_m2612852447_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2304330891 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2304330891 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2612852447(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1122952091_gshared (InternalEnumerator_1_t83702344 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1122952091_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t83702344 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t83702344 *>(__this + 1);
	InternalEnumerator_1__ctor_m1122952091(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m396346696_gshared (InternalEnumerator_1_t83702344 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m396346696_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t83702344 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t83702344 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m396346696(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088805473_gshared (InternalEnumerator_1_t83702344 * __this, const MethodInfo* method)
{
	{
		Mark_t3471605523  L_0 = InternalEnumerator_1_get_Current_m1909182215((InternalEnumerator_1_t83702344 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Mark_t3471605523  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088805473_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t83702344 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t83702344 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088805473(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1837758743_gshared (InternalEnumerator_1_t83702344 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1837758743_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t83702344 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t83702344 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1837758743(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4133541970_gshared (InternalEnumerator_1_t83702344 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4133541970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t83702344 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t83702344 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4133541970(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern "C"  Mark_t3471605523  InternalEnumerator_1_get_Current_m1909182215_gshared (InternalEnumerator_1_t83702344 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1909182215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Mark_t3471605523  L_8 = ((  Mark_t3471605523  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Mark_t3471605523  InternalEnumerator_1_get_Current_m1909182215_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t83702344 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t83702344 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1909182215(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3261326277_gshared (InternalEnumerator_1_t1788223366 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3261326277_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1788223366 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1788223366 *>(__this + 1);
	InternalEnumerator_1__ctor_m3261326277(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3763767775_gshared (InternalEnumerator_1_t1788223366 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3763767775_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1788223366 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1788223366 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3763767775(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1299775605_gshared (InternalEnumerator_1_t1788223366 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t881159249  L_0 = InternalEnumerator_1_get_Current_m1698047500((InternalEnumerator_1_t1788223366 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeSpan_t881159249  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1299775605_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1788223366 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1788223366 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1299775605(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4260521517_gshared (InternalEnumerator_1_t1788223366 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4260521517_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1788223366 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1788223366 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4260521517(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3958061110_gshared (InternalEnumerator_1_t1788223366 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3958061110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1788223366 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1788223366 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3958061110(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
extern "C"  TimeSpan_t881159249  InternalEnumerator_1_get_Current_m1698047500_gshared (InternalEnumerator_1_t1788223366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1698047500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TimeSpan_t881159249  L_8 = ((  TimeSpan_t881159249  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TimeSpan_t881159249  InternalEnumerator_1_get_Current_m1698047500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1788223366 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1788223366 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1698047500(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2202456613_gshared (InternalEnumerator_1_t3084789075 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2202456613_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3084789075 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3084789075 *>(__this + 1);
	InternalEnumerator_1__ctor_m2202456613(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3848218235_gshared (InternalEnumerator_1_t3084789075 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3848218235_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3084789075 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3084789075 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3848218235(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m191386315_gshared (InternalEnumerator_1_t3084789075 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = InternalEnumerator_1_get_Current_m784835552((InternalEnumerator_1_t3084789075 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m191386315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3084789075 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3084789075 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m191386315(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m359678482_gshared (InternalEnumerator_1_t3084789075 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m359678482_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3084789075 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3084789075 *>(__this + 1);
	InternalEnumerator_1_Dispose_m359678482(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m973048327_gshared (InternalEnumerator_1_t3084789075 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m973048327_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3084789075 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3084789075 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m973048327(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern "C"  uint16_t InternalEnumerator_1_get_Current_m784835552_gshared (InternalEnumerator_1_t3084789075 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m784835552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint16_t InternalEnumerator_1_get_Current_m784835552_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3084789075 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3084789075 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m784835552(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3383770493_gshared (InternalEnumerator_1_t3467126095 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3383770493_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3467126095 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3467126095 *>(__this + 1);
	InternalEnumerator_1__ctor_m3383770493(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m342565588_gshared (InternalEnumerator_1_t3467126095 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m342565588_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3467126095 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3467126095 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m342565588(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612699335_gshared (InternalEnumerator_1_t3467126095 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = InternalEnumerator_1_get_Current_m1897120917((InternalEnumerator_1_t3467126095 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612699335_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3467126095 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3467126095 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612699335(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1121538879_gshared (InternalEnumerator_1_t3467126095 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1121538879_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3467126095 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3467126095 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1121538879(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3855324972_gshared (InternalEnumerator_1_t3467126095 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3855324972_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3467126095 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3467126095 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3855324972(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern "C"  uint32_t InternalEnumerator_1_get_Current_m1897120917_gshared (InternalEnumerator_1_t3467126095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1897120917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint32_t L_8 = ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint32_t InternalEnumerator_1_get_Current_m1897120917_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3467126095 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3467126095 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1897120917(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3215746182_gshared (InternalEnumerator_1_t746136913 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3215746182_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t746136913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t746136913 *>(__this + 1);
	InternalEnumerator_1__ctor_m3215746182(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m872612294_gshared (InternalEnumerator_1_t746136913 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m872612294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t746136913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t746136913 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m872612294(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2395961985_gshared (InternalEnumerator_1_t746136913 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = InternalEnumerator_1_get_Current_m1588647567((InternalEnumerator_1_t746136913 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2395961985_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t746136913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t746136913 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2395961985(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3637184090_gshared (InternalEnumerator_1_t746136913 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3637184090_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t746136913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t746136913 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3637184090(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1975820803_gshared (InternalEnumerator_1_t746136913 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1975820803_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t746136913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t746136913 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1975820803(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern "C"  uint64_t InternalEnumerator_1_get_Current_m1588647567_gshared (InternalEnumerator_1_t746136913 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1588647567_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint64_t L_8 = ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint64_t InternalEnumerator_1_get_Current_m1588647567_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t746136913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t746136913 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1588647567(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m362401472_gshared (InternalEnumerator_1_t1629489814 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m362401472_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1629489814 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1629489814 *>(__this + 1);
	InternalEnumerator_1__ctor_m362401472(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4196663616_gshared (InternalEnumerator_1_t1629489814 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4196663616_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1629489814 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1629489814 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4196663616(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2671801110_gshared (InternalEnumerator_1_t1629489814 * __this, const MethodInfo* method)
{
	{
		UriScheme_t722425697  L_0 = InternalEnumerator_1_get_Current_m1007906068((InternalEnumerator_1_t1629489814 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UriScheme_t722425697  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2671801110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1629489814 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1629489814 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2671801110(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m648941584_gshared (InternalEnumerator_1_t1629489814 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m648941584_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1629489814 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1629489814 *>(__this + 1);
	InternalEnumerator_1_Dispose_m648941584(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m190587569_gshared (InternalEnumerator_1_t1629489814 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m190587569_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1629489814 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1629489814 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m190587569(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern "C"  UriScheme_t722425697  InternalEnumerator_1_get_Current_m1007906068_gshared (InternalEnumerator_1_t1629489814 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1007906068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UriScheme_t722425697  L_8 = ((  UriScheme_t722425697  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UriScheme_t722425697  InternalEnumerator_1_get_Current_m1007906068_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1629489814 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1629489814 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1007906068(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4044013350_gshared (InternalEnumerator_1_t4063781272 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4044013350_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4063781272 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4063781272 *>(__this + 1);
	InternalEnumerator_1__ctor_m4044013350(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4107572074_gshared (InternalEnumerator_1_t4063781272 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4107572074_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4063781272 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4063781272 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4107572074(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1911422882_gshared (InternalEnumerator_1_t4063781272 * __this, const MethodInfo* method)
{
	{
		AnimatorClipInfo_t3156717155  L_0 = InternalEnumerator_1_get_Current_m348524980((InternalEnumerator_1_t4063781272 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		AnimatorClipInfo_t3156717155  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1911422882_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4063781272 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4063781272 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1911422882(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m969498836_gshared (InternalEnumerator_1_t4063781272 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m969498836_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4063781272 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4063781272 *>(__this + 1);
	InternalEnumerator_1_Dispose_m969498836(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4074044800_gshared (InternalEnumerator_1_t4063781272 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4074044800_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4063781272 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4063781272 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4074044800(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::get_Current()
extern "C"  AnimatorClipInfo_t3156717155  InternalEnumerator_1_get_Current_m348524980_gshared (InternalEnumerator_1_t4063781272 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m348524980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		AnimatorClipInfo_t3156717155  L_8 = ((  AnimatorClipInfo_t3156717155  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  AnimatorClipInfo_t3156717155  InternalEnumerator_1_get_Current_m348524980_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4063781272 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4063781272 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m348524980(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m539509188_gshared (InternalEnumerator_1_t3507565409 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m539509188_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3507565409 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3507565409 *>(__this + 1);
	InternalEnumerator_1__ctor_m539509188(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m996811230_gshared (InternalEnumerator_1_t3507565409 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m996811230_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3507565409 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3507565409 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m996811230(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4196752819_gshared (InternalEnumerator_1_t3507565409 * __this, const MethodInfo* method)
{
	{
		Color32_t2600501292  L_0 = InternalEnumerator_1_get_Current_m1232221964((InternalEnumerator_1_t3507565409 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color32_t2600501292  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4196752819_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3507565409 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3507565409 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4196752819(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3259955982_gshared (InternalEnumerator_1_t3507565409 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3259955982_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3507565409 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3507565409 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3259955982(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1590908934_gshared (InternalEnumerator_1_t3507565409 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1590908934_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3507565409 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3507565409 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1590908934(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
extern "C"  Color32_t2600501292  InternalEnumerator_1_get_Current_m1232221964_gshared (InternalEnumerator_1_t3507565409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1232221964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color32_t2600501292  L_8 = ((  Color32_t2600501292  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Color32_t2600501292  InternalEnumerator_1_get_Current_m1232221964_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3507565409 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3507565409 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1232221964(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1352157576_gshared (InternalEnumerator_1_t370852074 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1352157576_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t370852074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t370852074 *>(__this + 1);
	InternalEnumerator_1__ctor_m1352157576(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1009155168_gshared (InternalEnumerator_1_t370852074 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1009155168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t370852074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t370852074 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1009155168(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m629296715_gshared (InternalEnumerator_1_t370852074 * __this, const MethodInfo* method)
{
	{
		ContactPoint_t3758755253  L_0 = InternalEnumerator_1_get_Current_m2315302778((InternalEnumerator_1_t370852074 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint_t3758755253  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m629296715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t370852074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t370852074 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m629296715(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m756188704_gshared (InternalEnumerator_1_t370852074 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m756188704_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t370852074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t370852074 *>(__this + 1);
	InternalEnumerator_1_Dispose_m756188704(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1187868016_gshared (InternalEnumerator_1_t370852074 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1187868016_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t370852074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t370852074 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1187868016(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
extern "C"  ContactPoint_t3758755253  InternalEnumerator_1_get_Current_m2315302778_gshared (InternalEnumerator_1_t370852074 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2315302778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint_t3758755253  L_8 = ((  ContactPoint_t3758755253  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ContactPoint_t3758755253  InternalEnumerator_1_get_Current_m2315302778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t370852074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t370852074 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2315302778(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1270930859_gshared (InternalEnumerator_1_t2337465 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1270930859_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2337465 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2337465 *>(__this + 1);
	InternalEnumerator_1__ctor_m1270930859(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m500203932_gshared (InternalEnumerator_1_t2337465 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m500203932_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2337465 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2337465 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m500203932(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2358118467_gshared (InternalEnumerator_1_t2337465 * __this, const MethodInfo* method)
{
	{
		ContactPoint2D_t3390240644  L_0 = InternalEnumerator_1_get_Current_m1523142935((InternalEnumerator_1_t2337465 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint2D_t3390240644  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2358118467_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2337465 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2337465 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2358118467(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1306615151_gshared (InternalEnumerator_1_t2337465 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1306615151_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2337465 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2337465 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1306615151(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1174937354_gshared (InternalEnumerator_1_t2337465 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1174937354_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2337465 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2337465 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1174937354(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::get_Current()
extern "C"  ContactPoint2D_t3390240644  InternalEnumerator_1_get_Current_m1523142935_gshared (InternalEnumerator_1_t2337465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1523142935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint2D_t3390240644  L_8 = ((  ContactPoint2D_t3390240644  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ContactPoint2D_t3390240644  InternalEnumerator_1_get_Current_m1523142935_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2337465 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2337465 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1523142935(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2311732727_gshared (InternalEnumerator_1_t4267370966 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2311732727_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4267370966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267370966 *>(__this + 1);
	InternalEnumerator_1__ctor_m2311732727(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1686642781_gshared (InternalEnumerator_1_t4267370966 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1686642781_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4267370966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267370966 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1686642781(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m923624021_gshared (InternalEnumerator_1_t4267370966 * __this, const MethodInfo* method)
{
	{
		RaycastResult_t3360306849  L_0 = InternalEnumerator_1_get_Current_m254780543((InternalEnumerator_1_t4267370966 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastResult_t3360306849  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m923624021_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4267370966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267370966 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m923624021(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3517794894_gshared (InternalEnumerator_1_t4267370966 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3517794894_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4267370966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267370966 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3517794894(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1344185775_gshared (InternalEnumerator_1_t4267370966 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1344185775_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4267370966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267370966 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1344185775(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C"  RaycastResult_t3360306849  InternalEnumerator_1_get_Current_m254780543_gshared (InternalEnumerator_1_t4267370966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m254780543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastResult_t3360306849  L_8 = ((  RaycastResult_t3360306849  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastResult_t3360306849  InternalEnumerator_1_get_Current_m254780543_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4267370966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267370966 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m254780543(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1744883412_gshared (InternalEnumerator_1_t818507063 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1744883412_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t818507063 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t818507063 *>(__this + 1);
	InternalEnumerator_1__ctor_m1744883412(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3217592429_gshared (InternalEnumerator_1_t818507063 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3217592429_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t818507063 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t818507063 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3217592429(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m605068928_gshared (InternalEnumerator_1_t818507063 * __this, const MethodInfo* method)
{
	{
		Keyframe_t4206410242  L_0 = InternalEnumerator_1_get_Current_m2907722321((InternalEnumerator_1_t818507063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Keyframe_t4206410242  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m605068928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t818507063 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t818507063 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m605068928(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3211169941_gshared (InternalEnumerator_1_t818507063 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3211169941_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t818507063 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t818507063 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3211169941(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1109261117_gshared (InternalEnumerator_1_t818507063 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1109261117_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t818507063 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t818507063 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1109261117(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern "C"  Keyframe_t4206410242  InternalEnumerator_1_get_Current_m2907722321_gshared (InternalEnumerator_1_t818507063 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2907722321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Keyframe_t4206410242  L_8 = ((  Keyframe_t4206410242  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Keyframe_t4206410242  InternalEnumerator_1_get_Current_m2907722321_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t818507063 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t818507063 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2907722321(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m615777089_gshared (InternalEnumerator_1_t1963066083 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m615777089_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1963066083 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1963066083 *>(__this + 1);
	InternalEnumerator_1__ctor_m615777089(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2621383412_gshared (InternalEnumerator_1_t1963066083 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2621383412_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1963066083 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1963066083 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2621383412(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1635397542_gshared (InternalEnumerator_1_t1963066083 * __this, const MethodInfo* method)
{
	{
		RaycastHit_t1056001966  L_0 = InternalEnumerator_1_get_Current_m114240259((InternalEnumerator_1_t1963066083 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastHit_t1056001966  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1635397542_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1963066083 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1963066083 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1635397542(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m958164402_gshared (InternalEnumerator_1_t1963066083 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m958164402_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1963066083 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1963066083 *>(__this + 1);
	InternalEnumerator_1_Dispose_m958164402(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3164144724_gshared (InternalEnumerator_1_t1963066083 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3164144724_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1963066083 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1963066083 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3164144724(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
extern "C"  RaycastHit_t1056001966  InternalEnumerator_1_get_Current_m114240259_gshared (InternalEnumerator_1_t1963066083 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m114240259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastHit_t1056001966  L_8 = ((  RaycastHit_t1056001966  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastHit_t1056001966  InternalEnumerator_1_get_Current_m114240259_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1963066083 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1963066083 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m114240259(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2635640285_gshared (InternalEnumerator_1_t3186646106 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2635640285_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3186646106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3186646106 *>(__this + 1);
	InternalEnumerator_1__ctor_m2635640285(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1775752715_gshared (InternalEnumerator_1_t3186646106 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1775752715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3186646106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3186646106 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1775752715(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2424959150_gshared (InternalEnumerator_1_t3186646106 * __this, const MethodInfo* method)
{
	{
		RaycastHit2D_t2279581989  L_0 = InternalEnumerator_1_get_Current_m1655128652((InternalEnumerator_1_t3186646106 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastHit2D_t2279581989  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2424959150_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3186646106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3186646106 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2424959150(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m555942266_gshared (InternalEnumerator_1_t3186646106 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m555942266_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3186646106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3186646106 *>(__this + 1);
	InternalEnumerator_1_Dispose_m555942266(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2734554195_gshared (InternalEnumerator_1_t3186646106 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2734554195_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3186646106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3186646106 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2734554195(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern "C"  RaycastHit2D_t2279581989  InternalEnumerator_1_get_Current_m1655128652_gshared (InternalEnumerator_1_t3186646106 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1655128652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastHit2D_t2279581989  L_8 = ((  RaycastHit2D_t2279581989  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastHit2D_t2279581989  InternalEnumerator_1_get_Current_m1655128652_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3186646106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3186646106 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1655128652(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3848674141_gshared (InternalEnumerator_1_t3267543976 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3848674141_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3267543976 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3267543976 *>(__this + 1);
	InternalEnumerator_1__ctor_m3848674141(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2556670574_gshared (InternalEnumerator_1_t3267543976 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2556670574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3267543976 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3267543976 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2556670574(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rect>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2690353642_gshared (InternalEnumerator_1_t3267543976 * __this, const MethodInfo* method)
{
	{
		Rect_t2360479859  L_0 = InternalEnumerator_1_get_Current_m3941815949((InternalEnumerator_1_t3267543976 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Rect_t2360479859  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2690353642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3267543976 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3267543976 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2690353642(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1453932905_gshared (InternalEnumerator_1_t3267543976 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1453932905_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3267543976 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3267543976 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1453932905(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rect>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3804980664_gshared (InternalEnumerator_1_t3267543976 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3804980664_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3267543976 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3267543976 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3804980664(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Rect>::get_Current()
extern "C"  Rect_t2360479859  InternalEnumerator_1_get_Current_m3941815949_gshared (InternalEnumerator_1_t3267543976 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3941815949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Rect_t2360479859  L_8 = ((  Rect_t2360479859  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Rect_t2360479859  InternalEnumerator_1_get_Current_m3941815949_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3267543976 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3267543976 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3941815949(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3913006324_gshared (InternalEnumerator_1_t4136673857 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3913006324_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4136673857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4136673857 *>(__this + 1);
	InternalEnumerator_1__ctor_m3913006324(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1451164462_gshared (InternalEnumerator_1_t4136673857 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1451164462_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4136673857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4136673857 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1451164462(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2032951142_gshared (InternalEnumerator_1_t4136673857 * __this, const MethodInfo* method)
{
	{
		HitInfo_t3229609740  L_0 = InternalEnumerator_1_get_Current_m4124877207((InternalEnumerator_1_t4136673857 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		HitInfo_t3229609740  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2032951142_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4136673857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4136673857 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2032951142(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3977286481_gshared (InternalEnumerator_1_t4136673857 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3977286481_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4136673857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4136673857 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3977286481(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2088624192_gshared (InternalEnumerator_1_t4136673857 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2088624192_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4136673857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4136673857 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2088624192(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern "C"  HitInfo_t3229609740  InternalEnumerator_1_get_Current_m4124877207_gshared (InternalEnumerator_1_t4136673857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4124877207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		HitInfo_t3229609740  L_8 = ((  HitInfo_t3229609740  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  HitInfo_t3229609740  InternalEnumerator_1_get_Current_m4124877207_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4136673857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4136673857 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4124877207(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3535695642_gshared (InternalEnumerator_1_t1582286363 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3535695642_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1582286363 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1582286363 *>(__this + 1);
	InternalEnumerator_1__ctor_m3535695642(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1918396835_gshared (InternalEnumerator_1_t1582286363 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1918396835_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1582286363 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1582286363 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1918396835(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m943285433_gshared (InternalEnumerator_1_t1582286363 * __this, const MethodInfo* method)
{
	{
		GcAchievementData_t675222246  L_0 = InternalEnumerator_1_get_Current_m3628030453((InternalEnumerator_1_t1582286363 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcAchievementData_t675222246  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m943285433_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1582286363 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1582286363 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m943285433(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4174463085_gshared (InternalEnumerator_1_t1582286363 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4174463085_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1582286363 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1582286363 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4174463085(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4151310216_gshared (InternalEnumerator_1_t1582286363 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4151310216_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1582286363 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1582286363 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4151310216(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern "C"  GcAchievementData_t675222246  InternalEnumerator_1_get_Current_m3628030453_gshared (InternalEnumerator_1_t1582286363 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3628030453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcAchievementData_t675222246  L_8 = ((  GcAchievementData_t675222246  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  GcAchievementData_t675222246  InternalEnumerator_1_get_Current_m3628030453_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1582286363 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1582286363 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3628030453(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m258868363_gshared (InternalEnumerator_1_t3032373948 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m258868363_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3032373948 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3032373948 *>(__this + 1);
	InternalEnumerator_1__ctor_m258868363(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4039902941_gshared (InternalEnumerator_1_t3032373948 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4039902941_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3032373948 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3032373948 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4039902941(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2582019288_gshared (InternalEnumerator_1_t3032373948 * __this, const MethodInfo* method)
{
	{
		GcScoreData_t2125309831  L_0 = InternalEnumerator_1_get_Current_m3609142494((InternalEnumerator_1_t3032373948 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcScoreData_t2125309831  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2582019288_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3032373948 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3032373948 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2582019288(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3187018662_gshared (InternalEnumerator_1_t3032373948 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3187018662_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3032373948 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3032373948 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3187018662(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1036267697_gshared (InternalEnumerator_1_t3032373948 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1036267697_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3032373948 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3032373948 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1036267697(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern "C"  GcScoreData_t2125309831  InternalEnumerator_1_get_Current_m3609142494_gshared (InternalEnumerator_1_t3032373948 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3609142494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcScoreData_t2125309831  L_8 = ((  GcScoreData_t2125309831  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  GcScoreData_t2125309831  InternalEnumerator_1_get_Current_m3609142494_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3032373948 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3032373948 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3609142494(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m63033851_gshared (InternalEnumerator_1_t2828920985 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m63033851_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2828920985 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2828920985 *>(__this + 1);
	InternalEnumerator_1__ctor_m63033851(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4226416995_gshared (InternalEnumerator_1_t2828920985 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4226416995_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2828920985 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2828920985 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4226416995(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2499755184_gshared (InternalEnumerator_1_t2828920985 * __this, const MethodInfo* method)
{
	{
		Touch_t1921856868  L_0 = InternalEnumerator_1_get_Current_m1836170281((InternalEnumerator_1_t2828920985 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Touch_t1921856868  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2499755184_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2828920985 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2828920985 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2499755184(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1432840524_gshared (InternalEnumerator_1_t2828920985 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1432840524_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2828920985 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2828920985 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1432840524(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Touch>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m300199243_gshared (InternalEnumerator_1_t2828920985 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m300199243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2828920985 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2828920985 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m300199243(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Touch>::get_Current()
extern "C"  Touch_t1921856868  InternalEnumerator_1_get_Current_m1836170281_gshared (InternalEnumerator_1_t2828920985 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1836170281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Touch_t1921856868  L_8 = ((  Touch_t1921856868  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Touch_t1921856868  InternalEnumerator_1_get_Current_m1836170281_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2828920985 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2828920985 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1836170281(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1739091604_gshared (InternalEnumerator_1_t2694367513 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1739091604_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2694367513 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2694367513 *>(__this + 1);
	InternalEnumerator_1__ctor_m1739091604(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1341907970_gshared (InternalEnumerator_1_t2694367513 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1341907970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2694367513 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2694367513 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1341907970(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164048921_gshared (InternalEnumerator_1_t2694367513 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m2350635577((InternalEnumerator_1_t2694367513 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164048921_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2694367513 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2694367513 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164048921(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m28687982_gshared (InternalEnumerator_1_t2694367513 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m28687982_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2694367513 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2694367513 *>(__this + 1);
	InternalEnumerator_1_Dispose_m28687982(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1555187632_gshared (InternalEnumerator_1_t2694367513 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1555187632_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2694367513 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2694367513 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1555187632(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2350635577_gshared (InternalEnumerator_1_t2694367513 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2350635577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2350635577_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2694367513 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2694367513 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2350635577(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4175030001_gshared (InternalEnumerator_1_t982565223 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4175030001_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t982565223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t982565223 *>(__this + 1);
	InternalEnumerator_1__ctor_m4175030001(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2305395628_gshared (InternalEnumerator_1_t982565223 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2305395628_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t982565223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t982565223 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2305395628(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2649471526_gshared (InternalEnumerator_1_t982565223 * __this, const MethodInfo* method)
{
	{
		UICharInfo_t75501106  L_0 = InternalEnumerator_1_get_Current_m3792939945((InternalEnumerator_1_t982565223 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UICharInfo_t75501106  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2649471526_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t982565223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t982565223 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2649471526(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3790132913_gshared (InternalEnumerator_1_t982565223 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3790132913_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t982565223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t982565223 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3790132913(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3713722659_gshared (InternalEnumerator_1_t982565223 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3713722659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t982565223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t982565223 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3713722659(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
extern "C"  UICharInfo_t75501106  InternalEnumerator_1_get_Current_m3792939945_gshared (InternalEnumerator_1_t982565223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3792939945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UICharInfo_t75501106  L_8 = ((  UICharInfo_t75501106  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UICharInfo_t75501106  InternalEnumerator_1_get_Current_m3792939945_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t982565223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t982565223 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3792939945(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3322594868_gshared (InternalEnumerator_1_t807363631 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3322594868_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t807363631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t807363631 *>(__this + 1);
	InternalEnumerator_1__ctor_m3322594868(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1601477281_gshared (InternalEnumerator_1_t807363631 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1601477281_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t807363631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t807363631 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1601477281(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3553395619_gshared (InternalEnumerator_1_t807363631 * __this, const MethodInfo* method)
{
	{
		UILineInfo_t4195266810  L_0 = InternalEnumerator_1_get_Current_m1070921822((InternalEnumerator_1_t807363631 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UILineInfo_t4195266810  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3553395619_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t807363631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t807363631 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3553395619(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2788308318_gshared (InternalEnumerator_1_t807363631 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2788308318_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t807363631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t807363631 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2788308318(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1396448578_gshared (InternalEnumerator_1_t807363631 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1396448578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t807363631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t807363631 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1396448578(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::get_Current()
extern "C"  UILineInfo_t4195266810  InternalEnumerator_1_get_Current_m1070921822_gshared (InternalEnumerator_1_t807363631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1070921822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UILineInfo_t4195266810  L_8 = ((  UILineInfo_t4195266810  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UILineInfo_t4195266810  InternalEnumerator_1_get_Current_m1070921822_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t807363631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t807363631 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1070921822(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2490839835_gshared (InternalEnumerator_1_t669594426 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2490839835_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t669594426 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t669594426 *>(__this + 1);
	InternalEnumerator_1__ctor_m2490839835(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4072625129_gshared (InternalEnumerator_1_t669594426 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4072625129_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t669594426 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t669594426 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4072625129(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2209458050_gshared (InternalEnumerator_1_t669594426 * __this, const MethodInfo* method)
{
	{
		UIVertex_t4057497605  L_0 = InternalEnumerator_1_get_Current_m1534474313((InternalEnumerator_1_t669594426 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UIVertex_t4057497605  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2209458050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t669594426 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t669594426 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2209458050(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m365545176_gshared (InternalEnumerator_1_t669594426 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m365545176_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t669594426 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t669594426 *>(__this + 1);
	InternalEnumerator_1_Dispose_m365545176(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3686444574_gshared (InternalEnumerator_1_t669594426 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3686444574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t669594426 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t669594426 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3686444574(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::get_Current()
extern "C"  UIVertex_t4057497605  InternalEnumerator_1_get_Current_m1534474313_gshared (InternalEnumerator_1_t669594426 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1534474313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UIVertex_t4057497605  L_8 = ((  UIVertex_t4057497605  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UIVertex_t4057497605  InternalEnumerator_1_get_Current_m1534474313_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t669594426 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t669594426 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1534474313(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m945079686_gshared (InternalEnumerator_1_t3063293640 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m945079686_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3063293640 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3063293640 *>(__this + 1);
	InternalEnumerator_1__ctor_m945079686(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m158730371_gshared (InternalEnumerator_1_t3063293640 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m158730371_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3063293640 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3063293640 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m158730371(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1847780851_gshared (InternalEnumerator_1_t3063293640 * __this, const MethodInfo* method)
{
	{
		Vector2_t2156229523  L_0 = InternalEnumerator_1_get_Current_m3331252162((InternalEnumerator_1_t3063293640 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector2_t2156229523  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1847780851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3063293640 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3063293640 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1847780851(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m663714168_gshared (InternalEnumerator_1_t3063293640 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m663714168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3063293640 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3063293640 *>(__this + 1);
	InternalEnumerator_1_Dispose_m663714168(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1262669372_gshared (InternalEnumerator_1_t3063293640 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1262669372_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3063293640 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3063293640 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1262669372(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern "C"  Vector2_t2156229523  InternalEnumerator_1_get_Current_m3331252162_gshared (InternalEnumerator_1_t3063293640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3331252162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector2_t2156229523  L_8 = ((  Vector2_t2156229523  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Vector2_t2156229523  InternalEnumerator_1_get_Current_m3331252162_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3063293640 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3063293640 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3331252162(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4132027968_gshared (InternalEnumerator_1_t334410285 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4132027968_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t334410285 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t334410285 *>(__this + 1);
	InternalEnumerator_1__ctor_m4132027968(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1916984356_gshared (InternalEnumerator_1_t334410285 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1916984356_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t334410285 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t334410285 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1916984356(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3537550566_gshared (InternalEnumerator_1_t334410285 * __this, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = InternalEnumerator_1_get_Current_m1477715453((InternalEnumerator_1_t334410285 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector3_t3722313464  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3537550566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t334410285 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t334410285 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3537550566(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2188147046_gshared (InternalEnumerator_1_t334410285 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2188147046_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t334410285 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t334410285 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2188147046(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector3>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1262906009_gshared (InternalEnumerator_1_t334410285 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1262906009_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t334410285 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t334410285 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1262906009(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t3722313464  InternalEnumerator_1_get_Current_m1477715453_gshared (InternalEnumerator_1_t334410285 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1477715453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector3_t3722313464  L_8 = ((  Vector3_t3722313464  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Vector3_t3722313464  InternalEnumerator_1_get_Current_m1477715453_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t334410285 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t334410285 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1477715453(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2307827786_gshared (InternalEnumerator_1_t4226093054 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2307827786_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4226093054 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4226093054 *>(__this + 1);
	InternalEnumerator_1__ctor_m2307827786(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1944844050_gshared (InternalEnumerator_1_t4226093054 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1944844050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4226093054 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4226093054 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1944844050(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3982010935_gshared (InternalEnumerator_1_t4226093054 * __this, const MethodInfo* method)
{
	{
		Vector4_t3319028937  L_0 = InternalEnumerator_1_get_Current_m2356858238((InternalEnumerator_1_t4226093054 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector4_t3319028937  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3982010935_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4226093054 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4226093054 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3982010935(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m987068791_gshared (InternalEnumerator_1_t4226093054 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m987068791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4226093054 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4226093054 *>(__this + 1);
	InternalEnumerator_1_Dispose_m987068791(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector4>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1258813334_gshared (InternalEnumerator_1_t4226093054 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m21610649((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1258813334_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4226093054 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4226093054 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1258813334(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector4>::get_Current()
extern "C"  Vector4_t3319028937  InternalEnumerator_1_get_Current_m2356858238_gshared (InternalEnumerator_1_t4226093054 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2356858238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, (String_t*)_stringLiteral2287514662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, (String_t*)_stringLiteral3056411131, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m21610649((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector4_t3319028937  L_8 = ((  Vector4_t3319028937  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Vector4_t3319028937  InternalEnumerator_1_get_Current_m2356858238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4226093054 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4226093054 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2356858238(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m775219714_gshared (DefaultComparer_t382820652 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t4029972201 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t4029972201 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t4029972201 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m348911778_gshared (DefaultComparer_t382820652 * __this, KeyValuePair_2_t2530217319  ___x0, KeyValuePair_2_t2530217319  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m348911778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		KeyValuePair_2_t2530217319  L_3 = ___x0;
		KeyValuePair_2_t2530217319  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		KeyValuePair_2_t2530217319  L_6 = ___x0;
		KeyValuePair_2_t2530217319  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		KeyValuePair_2_t2530217319  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, KeyValuePair_2_t2530217319  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (KeyValuePair_2_t2530217319 )L_9);
		return L_10;
	}

IL_004d:
	{
		KeyValuePair_2_t2530217319  L_11 = ___x0;
		KeyValuePair_2_t2530217319  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		KeyValuePair_2_t2530217319  L_14 = ___x0;
		KeyValuePair_2_t2530217319  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		KeyValuePair_2_t2530217319  L_17 = ___y1;
		KeyValuePair_2_t2530217319  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C"  void DefaultComparer__ctor_m757111150_gshared (DefaultComparer_t1591133118 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t943317371 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t943317371 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t943317371 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m732589824_gshared (DefaultComparer_t1591133118 * __this, DateTime_t3738529785  ___x0, DateTime_t3738529785  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m732589824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTime_t3738529785  L_3 = ___x0;
		DateTime_t3738529785  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		DateTime_t3738529785  L_6 = ___x0;
		DateTime_t3738529785  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		DateTime_t3738529785  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, DateTime_t3738529785  >::Invoke(0 /* System.Int32 System.IComparable`1<System.DateTime>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (DateTime_t3738529785 )L_9);
		return L_10;
	}

IL_004d:
	{
		DateTime_t3738529785  L_11 = ___x0;
		DateTime_t3738529785  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		DateTime_t3738529785  L_14 = ___x0;
		DateTime_t3738529785  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		DateTime_t3738529785  L_17 = ___y1;
		DateTime_t3738529785  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C"  void DefaultComparer__ctor_m3288720761_gshared (DefaultComparer_t1081890840 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t434075093 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t434075093 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t434075093 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m655397166_gshared (DefaultComparer_t1081890840 * __this, DateTimeOffset_t3229287507  ___x0, DateTimeOffset_t3229287507  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m655397166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTimeOffset_t3229287507  L_3 = ___x0;
		DateTimeOffset_t3229287507  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		DateTimeOffset_t3229287507  L_6 = ___x0;
		DateTimeOffset_t3229287507  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		DateTimeOffset_t3229287507  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, DateTimeOffset_t3229287507  >::Invoke(0 /* System.Int32 System.IComparable`1<System.DateTimeOffset>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (DateTimeOffset_t3229287507 )L_9);
		return L_10;
	}

IL_004d:
	{
		DateTimeOffset_t3229287507  L_11 = ___x0;
		DateTimeOffset_t3229287507  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		DateTimeOffset_t3229287507  L_14 = ___x0;
		DateTimeOffset_t3229287507  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		DateTimeOffset_t3229287507  L_17 = ___y1;
		DateTimeOffset_t3229287507  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C"  void DefaultComparer__ctor_m1236171334_gshared (DefaultComparer_t1046136220 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t398320473 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t398320473 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t398320473 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3591589106_gshared (DefaultComparer_t1046136220 * __this, Guid_t  ___x0, Guid_t  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3591589106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Guid_t  L_3 = ___x0;
		Guid_t  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Guid_t  L_6 = ___x0;
		Guid_t  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Guid_t  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Guid_t  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Guid>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Guid_t )L_9);
		return L_10;
	}

IL_004d:
	{
		Guid_t  L_11 = ___x0;
		Guid_t  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Guid_t  L_14 = ___x0;
		Guid_t  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Guid_t  L_17 = ___y1;
		Guid_t  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C"  void DefaultComparer__ctor_m2309314806_gshared (DefaultComparer_t803549086 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t155733339 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t155733339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t155733339 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1297750557_gshared (DefaultComparer_t803549086 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1297750557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		int32_t L_3 = ___x0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_6 = ___x0;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		int32_t L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(0 /* System.Int32 System.IComparable`1<System.Int32>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (int32_t)L_9);
		return L_10;
	}

IL_004d:
	{
		int32_t L_11 = ___x0;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = ___x0;
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		int32_t L_17 = ___y1;
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>::.ctor()
extern "C"  void DefaultComparer__ctor_m3948233172_gshared (DefaultComparer_t932709497 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t284893750 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t284893750 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t284893750 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m4042058291_gshared (DefaultComparer_t932709497 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m4042058291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		return 1;
	}

IL_002b:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject*)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Il2CppObject * L_4 = ___x0;
		Il2CppObject * L_5 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_6 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable`1<System.Object>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)L_5);
		return L_6;
	}

IL_004d:
	{
		Il2CppObject * L_7 = ___x0;
		if (!((Il2CppObject *)IsInst(L_7, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Il2CppObject * L_8 = ___x0;
		Il2CppObject * L_9 = ___y1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_8, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_8, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_9);
		return L_10;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_11 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_11, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void DefaultComparer__ctor_m3333451630_gshared (DefaultComparer_t2435436339 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1787620592 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1787620592 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1787620592 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m982533255_gshared (DefaultComparer_t2435436339 * __this, CustomAttributeNamedArgument_t287865710  ___x0, CustomAttributeNamedArgument_t287865710  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m982533255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		CustomAttributeNamedArgument_t287865710  L_3 = ___x0;
		CustomAttributeNamedArgument_t287865710  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		CustomAttributeNamedArgument_t287865710  L_6 = ___x0;
		CustomAttributeNamedArgument_t287865710  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		CustomAttributeNamedArgument_t287865710  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t287865710  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Reflection.CustomAttributeNamedArgument>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (CustomAttributeNamedArgument_t287865710 )L_9);
		return L_10;
	}

IL_004d:
	{
		CustomAttributeNamedArgument_t287865710  L_11 = ___x0;
		CustomAttributeNamedArgument_t287865710  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		CustomAttributeNamedArgument_t287865710  L_14 = ___x0;
		CustomAttributeNamedArgument_t287865710  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		CustomAttributeNamedArgument_t287865710  L_17 = ___y1;
		CustomAttributeNamedArgument_t287865710  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void DefaultComparer__ctor_m2039558311_gshared (DefaultComparer_t575753490 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t4222905039 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t4222905039 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t4222905039 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m947823904_gshared (DefaultComparer_t575753490 * __this, CustomAttributeTypedArgument_t2723150157  ___x0, CustomAttributeTypedArgument_t2723150157  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m947823904_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		CustomAttributeTypedArgument_t2723150157  L_3 = ___x0;
		CustomAttributeTypedArgument_t2723150157  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		CustomAttributeTypedArgument_t2723150157  L_6 = ___x0;
		CustomAttributeTypedArgument_t2723150157  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		CustomAttributeTypedArgument_t2723150157  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t2723150157  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Reflection.CustomAttributeTypedArgument>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (CustomAttributeTypedArgument_t2723150157 )L_9);
		return L_10;
	}

IL_004d:
	{
		CustomAttributeTypedArgument_t2723150157  L_11 = ___x0;
		CustomAttributeTypedArgument_t2723150157  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		CustomAttributeTypedArgument_t2723150157  L_14 = ___x0;
		CustomAttributeTypedArgument_t2723150157  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		CustomAttributeTypedArgument_t2723150157  L_17 = ___y1;
		CustomAttributeTypedArgument_t2723150157  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C"  void DefaultComparer__ctor_m977417144_gshared (DefaultComparer_t3028729878 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2380914131 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2380914131 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2380914131 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3967426329_gshared (DefaultComparer_t3028729878 * __this, TimeSpan_t881159249  ___x0, TimeSpan_t881159249  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3967426329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		TimeSpan_t881159249  L_3 = ___x0;
		TimeSpan_t881159249  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		TimeSpan_t881159249  L_6 = ___x0;
		TimeSpan_t881159249  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		TimeSpan_t881159249  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, TimeSpan_t881159249  >::Invoke(0 /* System.Int32 System.IComparable`1<System.TimeSpan>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (TimeSpan_t881159249 )L_9);
		return L_10;
	}

IL_004d:
	{
		TimeSpan_t881159249  L_11 = ___x0;
		TimeSpan_t881159249  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		TimeSpan_t881159249  L_14 = ___x0;
		TimeSpan_t881159249  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		TimeSpan_t881159249  L_17 = ___y1;
		TimeSpan_t881159249  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.AnimatorClipInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m3264036589_gshared (DefaultComparer_t1009320488 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t361504741 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t361504741 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t361504741 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.AnimatorClipInfo>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1658087432_gshared (DefaultComparer_t1009320488 * __this, AnimatorClipInfo_t3156717155  ___x0, AnimatorClipInfo_t3156717155  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1658087432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		AnimatorClipInfo_t3156717155  L_3 = ___x0;
		AnimatorClipInfo_t3156717155  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		AnimatorClipInfo_t3156717155  L_6 = ___x0;
		AnimatorClipInfo_t3156717155  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		AnimatorClipInfo_t3156717155  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, AnimatorClipInfo_t3156717155  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.AnimatorClipInfo>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (AnimatorClipInfo_t3156717155 )L_9);
		return L_10;
	}

IL_004d:
	{
		AnimatorClipInfo_t3156717155  L_11 = ___x0;
		AnimatorClipInfo_t3156717155  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		AnimatorClipInfo_t3156717155  L_14 = ___x0;
		AnimatorClipInfo_t3156717155  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		AnimatorClipInfo_t3156717155  L_17 = ___y1;
		AnimatorClipInfo_t3156717155  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>::.ctor()
extern "C"  void DefaultComparer__ctor_m704436039_gshared (DefaultComparer_t453104625 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t4100256174 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t4100256174 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t4100256174 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3278268937_gshared (DefaultComparer_t453104625 * __this, Color32_t2600501292  ___x0, Color32_t2600501292  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3278268937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Color32_t2600501292  L_3 = ___x0;
		Color32_t2600501292  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Color32_t2600501292  L_6 = ___x0;
		Color32_t2600501292  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Color32_t2600501292  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Color32_t2600501292  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Color32>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Color32_t2600501292 )L_9);
		return L_10;
	}

IL_004d:
	{
		Color32_t2600501292  L_11 = ___x0;
		Color32_t2600501292  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Color32_t2600501292  L_14 = ___x0;
		Color32_t2600501292  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Color32_t2600501292  L_17 = ___y1;
		Color32_t2600501292  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void DefaultComparer__ctor_m1036717011_gshared (DefaultComparer_t1212910182 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t565094435 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t565094435 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t565094435 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1920986590_gshared (DefaultComparer_t1212910182 * __this, RaycastResult_t3360306849  ___x0, RaycastResult_t3360306849  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1920986590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		RaycastResult_t3360306849  L_3 = ___x0;
		RaycastResult_t3360306849  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		RaycastResult_t3360306849  L_6 = ___x0;
		RaycastResult_t3360306849  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		RaycastResult_t3360306849  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, RaycastResult_t3360306849  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.EventSystems.RaycastResult>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (RaycastResult_t3360306849 )L_9);
		return L_10;
	}

IL_004d:
	{
		RaycastResult_t3360306849  L_11 = ___x0;
		RaycastResult_t3360306849  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		RaycastResult_t3360306849  L_14 = ___x0;
		RaycastResult_t3360306849  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		RaycastResult_t3360306849  L_17 = ___y1;
		RaycastResult_t3360306849  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Rect>::.ctor()
extern "C"  void DefaultComparer__ctor_m334752937_gshared (DefaultComparer_t213083192 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3860234741 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3860234741 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3860234741 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Rect>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m543097977_gshared (DefaultComparer_t213083192 * __this, Rect_t2360479859  ___x0, Rect_t2360479859  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m543097977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Rect_t2360479859  L_3 = ___x0;
		Rect_t2360479859  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Rect_t2360479859  L_6 = ___x0;
		Rect_t2360479859  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Rect_t2360479859  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Rect_t2360479859  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Rect>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Rect_t2360479859 )L_9);
		return L_10;
	}

IL_004d:
	{
		Rect_t2360479859  L_11 = ___x0;
		Rect_t2360479859  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Rect_t2360479859  L_14 = ___x0;
		Rect_t2360479859  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Rect_t2360479859  L_17 = ___y1;
		Rect_t2360479859  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m1074181621_gshared (DefaultComparer_t2223071735 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1575255988 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1575255988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1575255988 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3931992727_gshared (DefaultComparer_t2223071735 * __this, UICharInfo_t75501106  ___x0, UICharInfo_t75501106  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3931992727_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		UICharInfo_t75501106  L_3 = ___x0;
		UICharInfo_t75501106  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		UICharInfo_t75501106  L_6 = ___x0;
		UICharInfo_t75501106  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		UICharInfo_t75501106  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, UICharInfo_t75501106  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.UICharInfo>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (UICharInfo_t75501106 )L_9);
		return L_10;
	}

IL_004d:
	{
		UICharInfo_t75501106  L_11 = ___x0;
		UICharInfo_t75501106  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		UICharInfo_t75501106  L_14 = ___x0;
		UICharInfo_t75501106  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		UICharInfo_t75501106  L_17 = ___y1;
		UICharInfo_t75501106  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m631060898_gshared (DefaultComparer_t2047870143 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1400054396 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1400054396 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1400054396 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1916473435_gshared (DefaultComparer_t2047870143 * __this, UILineInfo_t4195266810  ___x0, UILineInfo_t4195266810  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1916473435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		UILineInfo_t4195266810  L_3 = ___x0;
		UILineInfo_t4195266810  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		UILineInfo_t4195266810  L_6 = ___x0;
		UILineInfo_t4195266810  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		UILineInfo_t4195266810  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, UILineInfo_t4195266810  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.UILineInfo>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (UILineInfo_t4195266810 )L_9);
		return L_10;
	}

IL_004d:
	{
		UILineInfo_t4195266810  L_11 = ___x0;
		UILineInfo_t4195266810  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		UILineInfo_t4195266810  L_14 = ___x0;
		UILineInfo_t4195266810  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		UILineInfo_t4195266810  L_17 = ___y1;
		UILineInfo_t4195266810  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C"  void DefaultComparer__ctor_m2906090291_gshared (DefaultComparer_t1910100938 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1262285191 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1262285191 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1262285191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1932373082_gshared (DefaultComparer_t1910100938 * __this, UIVertex_t4057497605  ___x0, UIVertex_t4057497605  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1932373082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		UIVertex_t4057497605  L_3 = ___x0;
		UIVertex_t4057497605  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		UIVertex_t4057497605  L_6 = ___x0;
		UIVertex_t4057497605  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		UIVertex_t4057497605  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, UIVertex_t4057497605  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.UIVertex>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (UIVertex_t4057497605 )L_9);
		return L_10;
	}

IL_004d:
	{
		UIVertex_t4057497605  L_11 = ___x0;
		UIVertex_t4057497605  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		UIVertex_t4057497605  L_14 = ___x0;
		UIVertex_t4057497605  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		UIVertex_t4057497605  L_17 = ___y1;
		UIVertex_t4057497605  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::.ctor()
extern "C"  void DefaultComparer__ctor_m3508212919_gshared (DefaultComparer_t8832856 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3655984405 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3655984405 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3655984405 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3648806637_gshared (DefaultComparer_t8832856 * __this, Vector2_t2156229523  ___x0, Vector2_t2156229523  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3648806637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Vector2_t2156229523  L_3 = ___x0;
		Vector2_t2156229523  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Vector2_t2156229523  L_6 = ___x0;
		Vector2_t2156229523  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Vector2_t2156229523  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Vector2_t2156229523  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Vector2>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Vector2_t2156229523 )L_9);
		return L_10;
	}

IL_004d:
	{
		Vector2_t2156229523  L_11 = ___x0;
		Vector2_t2156229523  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Vector2_t2156229523  L_14 = ___x0;
		Vector2_t2156229523  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Vector2_t2156229523  L_17 = ___y1;
		Vector2_t2156229523  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::.ctor()
extern "C"  void DefaultComparer__ctor_m3432518839_gshared (DefaultComparer_t1574916797 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t927101050 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t927101050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t927101050 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1369122336_gshared (DefaultComparer_t1574916797 * __this, Vector3_t3722313464  ___x0, Vector3_t3722313464  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1369122336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Vector3_t3722313464  L_3 = ___x0;
		Vector3_t3722313464  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Vector3_t3722313464  L_6 = ___x0;
		Vector3_t3722313464  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Vector3_t3722313464  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Vector3_t3722313464  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Vector3>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Vector3_t3722313464 )L_9);
		return L_10;
	}

IL_004d:
	{
		Vector3_t3722313464  L_11 = ___x0;
		Vector3_t3722313464  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t36111218_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Vector3_t3722313464  L_14 = ___x0;
		Vector3_t3722313464  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Vector3_t3722313464  L_17 = ___y1;
		Vector3_t3722313464  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t36111218_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t36111218_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, (String_t*)_stringLiteral4135314742, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
