﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NendUnityPlugin_AD_Native_NativeA775405424.h"
#include "mscorlib_System_IntPtr840150181.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.Platform.iOS.IOSNativeAd
struct  IOSNativeAd_t2862447157  : public NativeAd_t775405424
{
public:
	// System.IntPtr NendUnityPlugin.Platform.iOS.IOSNativeAd::m_NativeAdPtr
	IntPtr_t ___m_NativeAdPtr_4;

public:
	inline static int32_t get_offset_of_m_NativeAdPtr_4() { return static_cast<int32_t>(offsetof(IOSNativeAd_t2862447157, ___m_NativeAdPtr_4)); }
	inline IntPtr_t get_m_NativeAdPtr_4() const { return ___m_NativeAdPtr_4; }
	inline IntPtr_t* get_address_of_m_NativeAdPtr_4() { return &___m_NativeAdPtr_4; }
	inline void set_m_NativeAdPtr_4(IntPtr_t value)
	{
		___m_NativeAdPtr_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
