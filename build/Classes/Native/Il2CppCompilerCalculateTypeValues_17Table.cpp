﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_ScrollEvent149898510.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis1697763317.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRe3442648935.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect4137855814.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementT4072922106.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarV705693775.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRect343079324.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable3250028441.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transitio1769908631.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Selection2656606514.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility3359423571.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider3903728902.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction337909235.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent3180273144.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis809944411.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState1362986479.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial3850132571.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE2957107092.h"
#include "UnityEngine_UI_UnityEngine_UI_Text1901882714.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle2735377061.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit3587297765.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1873685584.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup123837990.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry2428680409.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping312708592.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexClip626611136.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3312407083.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As3417192999.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2767979955.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMo2604066427.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM3675272090.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit2218508340.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter3850442145.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi3267881214.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup3046220461.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1493259673.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis3613393006.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Const814224393.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2586782146.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalL729725570.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement1785403678.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup2436138090.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3170500204.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder541313304.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility2745813735.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup923838031.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2103211062.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCach701940803.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCach768590915.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac1884415901.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3913627115.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper2453304189.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2675891272.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect2440176439.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline2536100125.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV13991086357.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow773074319.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_2488454196.h"
#include "AssemblyU2DCSharp_U3CModuleU3E692745525.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner2921968851.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner_AnimBannerData3749323191.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner_U3CLoadJsonData1810388169.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner_U3CLoadTextureU1051594053.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner_U3CAnimateBanner688012631.h"
#include "AssemblyU2DCSharp_HCAnimatedBanner_U3CAnimateBanner102123587.h"
#include "AssemblyU2DCSharp_BestScore1653437491.h"
#include "AssemblyU2DCSharp_ButtonHome643243358.h"
#include "AssemblyU2DCSharp_ButtonHomeOver1627401701.h"
#include "AssemblyU2DCSharp_ButtonPause3620264363.h"
#include "AssemblyU2DCSharp_ButtonPlay1912942282.h"
#include "AssemblyU2DCSharp_ButtonReplay987972804.h"
#include "AssemblyU2DCSharp_ButtonReplayOver967915415.h"
#include "AssemblyU2DCSharp_ButtonResume2840336175.h"
#include "AssemblyU2DCSharp_ButtonSound35905277.h"
#include "AssemblyU2DCSharp_Coin2227745140.h"
#include "AssemblyU2DCSharp_DontDestroy1446738193.h"
#include "AssemblyU2DCSharp_GameScreen1800529819.h"
#include "AssemblyU2DCSharp_GameScreen_U3CchangeSpeedU3Ec__Ite85360554.h"
#include "AssemblyU2DCSharp_GameScreen_U3CchangeScoreU3Ec__It982768640.h"
#include "AssemblyU2DCSharp_GameScreen_U3ChideInfoU3Ec__Iter3795124997.h"
#include "AssemblyU2DCSharp_GameScreen_U3CshowIfGameOverU3Ec_966372778.h"
#include "AssemblyU2DCSharp_Ground4133628138.h"
#include "AssemblyU2DCSharp_MenuScreen742580752.h"
#include "AssemblyU2DCSharp_MovingBall2489706458.h"
#include "AssemblyU2DCSharp_MovingBg2796125617.h"
#include "AssemblyU2DCSharp_ObsDownCollisionCheck2541817184.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (ScrollEvent_t149898510), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (Axis_t1697763317)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1701[3] = 
{
	Axis_t1697763317::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (U3CClickRepeatU3Ec__Iterator0_t3442648935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[5] = 
{
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_eventData_0(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24this_1(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24current_2(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24disposing_3(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (ScrollRect_t4137855814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[36] = 
{
	ScrollRect_t4137855814::get_offset_of_m_Content_2(),
	ScrollRect_t4137855814::get_offset_of_m_Horizontal_3(),
	ScrollRect_t4137855814::get_offset_of_m_Vertical_4(),
	ScrollRect_t4137855814::get_offset_of_m_MovementType_5(),
	ScrollRect_t4137855814::get_offset_of_m_Elasticity_6(),
	ScrollRect_t4137855814::get_offset_of_m_Inertia_7(),
	ScrollRect_t4137855814::get_offset_of_m_DecelerationRate_8(),
	ScrollRect_t4137855814::get_offset_of_m_ScrollSensitivity_9(),
	ScrollRect_t4137855814::get_offset_of_m_Viewport_10(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbar_11(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbar_12(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbarVisibility_13(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbarVisibility_14(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbarSpacing_15(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbarSpacing_16(),
	ScrollRect_t4137855814::get_offset_of_m_OnValueChanged_17(),
	ScrollRect_t4137855814::get_offset_of_m_PointerStartLocalCursor_18(),
	ScrollRect_t4137855814::get_offset_of_m_ContentStartPosition_19(),
	ScrollRect_t4137855814::get_offset_of_m_ViewRect_20(),
	ScrollRect_t4137855814::get_offset_of_m_ContentBounds_21(),
	ScrollRect_t4137855814::get_offset_of_m_ViewBounds_22(),
	ScrollRect_t4137855814::get_offset_of_m_Velocity_23(),
	ScrollRect_t4137855814::get_offset_of_m_Dragging_24(),
	ScrollRect_t4137855814::get_offset_of_m_PrevPosition_25(),
	ScrollRect_t4137855814::get_offset_of_m_PrevContentBounds_26(),
	ScrollRect_t4137855814::get_offset_of_m_PrevViewBounds_27(),
	ScrollRect_t4137855814::get_offset_of_m_HasRebuiltLayout_28(),
	ScrollRect_t4137855814::get_offset_of_m_HSliderExpand_29(),
	ScrollRect_t4137855814::get_offset_of_m_VSliderExpand_30(),
	ScrollRect_t4137855814::get_offset_of_m_HSliderHeight_31(),
	ScrollRect_t4137855814::get_offset_of_m_VSliderWidth_32(),
	ScrollRect_t4137855814::get_offset_of_m_Rect_33(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbarRect_34(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbarRect_35(),
	ScrollRect_t4137855814::get_offset_of_m_Tracker_36(),
	ScrollRect_t4137855814::get_offset_of_m_Corners_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (MovementType_t4072922106)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1704[4] = 
{
	MovementType_t4072922106::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (ScrollbarVisibility_t705693775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1705[4] = 
{
	ScrollbarVisibility_t705693775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (ScrollRectEvent_t343079324), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (Selectable_t3250028441), -1, sizeof(Selectable_t3250028441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1707[14] = 
{
	Selectable_t3250028441_StaticFields::get_offset_of_s_List_2(),
	Selectable_t3250028441::get_offset_of_m_Navigation_3(),
	Selectable_t3250028441::get_offset_of_m_Transition_4(),
	Selectable_t3250028441::get_offset_of_m_Colors_5(),
	Selectable_t3250028441::get_offset_of_m_SpriteState_6(),
	Selectable_t3250028441::get_offset_of_m_AnimationTriggers_7(),
	Selectable_t3250028441::get_offset_of_m_Interactable_8(),
	Selectable_t3250028441::get_offset_of_m_TargetGraphic_9(),
	Selectable_t3250028441::get_offset_of_m_GroupsAllowInteraction_10(),
	Selectable_t3250028441::get_offset_of_m_CurrentSelectionState_11(),
	Selectable_t3250028441::get_offset_of_U3CisPointerInsideU3Ek__BackingField_12(),
	Selectable_t3250028441::get_offset_of_U3CisPointerDownU3Ek__BackingField_13(),
	Selectable_t3250028441::get_offset_of_U3ChasSelectionU3Ek__BackingField_14(),
	Selectable_t3250028441::get_offset_of_m_CanvasGroupCache_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (Transition_t1769908631)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1708[5] = 
{
	Transition_t1769908631::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (SelectionState_t2656606514)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1709[5] = 
{
	SelectionState_t2656606514::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (SetPropertyUtility_t3359423571), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (Slider_t3903728902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[15] = 
{
	Slider_t3903728902::get_offset_of_m_FillRect_16(),
	Slider_t3903728902::get_offset_of_m_HandleRect_17(),
	Slider_t3903728902::get_offset_of_m_Direction_18(),
	Slider_t3903728902::get_offset_of_m_MinValue_19(),
	Slider_t3903728902::get_offset_of_m_MaxValue_20(),
	Slider_t3903728902::get_offset_of_m_WholeNumbers_21(),
	Slider_t3903728902::get_offset_of_m_Value_22(),
	Slider_t3903728902::get_offset_of_m_OnValueChanged_23(),
	Slider_t3903728902::get_offset_of_m_FillImage_24(),
	Slider_t3903728902::get_offset_of_m_FillTransform_25(),
	Slider_t3903728902::get_offset_of_m_FillContainerRect_26(),
	Slider_t3903728902::get_offset_of_m_HandleTransform_27(),
	Slider_t3903728902::get_offset_of_m_HandleContainerRect_28(),
	Slider_t3903728902::get_offset_of_m_Offset_29(),
	Slider_t3903728902::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (Direction_t337909235)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1712[5] = 
{
	Direction_t337909235::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (SliderEvent_t3180273144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (Axis_t809944411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1714[3] = 
{
	Axis_t809944411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (SpriteState_t1362986479)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1715[3] = 
{
	SpriteState_t1362986479::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1362986479::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1362986479::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (StencilMaterial_t3850132571), -1, sizeof(StencilMaterial_t3850132571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1716[1] = 
{
	StencilMaterial_t3850132571_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (MatEntry_t2957107092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[10] = 
{
	MatEntry_t2957107092::get_offset_of_baseMat_0(),
	MatEntry_t2957107092::get_offset_of_customMat_1(),
	MatEntry_t2957107092::get_offset_of_count_2(),
	MatEntry_t2957107092::get_offset_of_stencilId_3(),
	MatEntry_t2957107092::get_offset_of_operation_4(),
	MatEntry_t2957107092::get_offset_of_compareFunction_5(),
	MatEntry_t2957107092::get_offset_of_readMask_6(),
	MatEntry_t2957107092::get_offset_of_writeMask_7(),
	MatEntry_t2957107092::get_offset_of_useAlphaClip_8(),
	MatEntry_t2957107092::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (Text_t1901882714), -1, sizeof(Text_t1901882714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1718[7] = 
{
	Text_t1901882714::get_offset_of_m_FontData_28(),
	Text_t1901882714::get_offset_of_m_Text_29(),
	Text_t1901882714::get_offset_of_m_TextCache_30(),
	Text_t1901882714::get_offset_of_m_TextCacheForLayout_31(),
	Text_t1901882714_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t1901882714::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t1901882714::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (Toggle_t2735377061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[5] = 
{
	Toggle_t2735377061::get_offset_of_toggleTransition_16(),
	Toggle_t2735377061::get_offset_of_graphic_17(),
	Toggle_t2735377061::get_offset_of_m_Group_18(),
	Toggle_t2735377061::get_offset_of_onValueChanged_19(),
	Toggle_t2735377061::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (ToggleTransition_t3587297765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1720[3] = 
{
	ToggleTransition_t3587297765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (ToggleEvent_t1873685584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (ToggleGroup_t123837990), -1, sizeof(ToggleGroup_t123837990_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1722[4] = 
{
	ToggleGroup_t123837990::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t123837990::get_offset_of_m_Toggles_3(),
	ToggleGroup_t123837990_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t123837990_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (ClipperRegistry_t2428680409), -1, sizeof(ClipperRegistry_t2428680409_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1723[2] = 
{
	ClipperRegistry_t2428680409_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t2428680409::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (Clipping_t312708592), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (RectangularVertexClipper_t626611136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[2] = 
{
	RectangularVertexClipper_t626611136::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t626611136::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (AspectRatioFitter_t3312407083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[4] = 
{
	AspectRatioFitter_t3312407083::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3312407083::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3312407083::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3312407083::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (AspectMode_t3417192999)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1729[6] = 
{
	AspectMode_t3417192999::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (CanvasScaler_t2767979955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[14] = 
{
	CanvasScaler_t2767979955::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2767979955::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2767979955::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2767979955::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2767979955::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2767979955::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2767979955::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2767979955::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2767979955::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2767979955::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2767979955::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2767979955::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2767979955::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (ScaleMode_t2604066427)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1731[4] = 
{
	ScaleMode_t2604066427::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (ScreenMatchMode_t3675272090)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1732[4] = 
{
	ScreenMatchMode_t3675272090::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (Unit_t2218508340)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1733[6] = 
{
	Unit_t2218508340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (ContentSizeFitter_t3850442145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[4] = 
{
	ContentSizeFitter_t3850442145::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t3850442145::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (FitMode_t3267881214)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1735[4] = 
{
	FitMode_t3267881214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (GridLayoutGroup_t3046220461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[6] = 
{
	GridLayoutGroup_t3046220461::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t3046220461::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t3046220461::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t3046220461::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (Corner_t1493259673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1737[5] = 
{
	Corner_t1493259673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (Axis_t3613393006)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1738[3] = 
{
	Axis_t3613393006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (Constraint_t814224393)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1739[4] = 
{
	Constraint_t814224393::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (HorizontalLayoutGroup_t2586782146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (HorizontalOrVerticalLayoutGroup_t729725570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[5] = 
{
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (LayoutElement_t1785403678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[7] = 
{
	LayoutElement_t1785403678::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t1785403678::get_offset_of_m_MinWidth_3(),
	LayoutElement_t1785403678::get_offset_of_m_MinHeight_4(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (LayoutGroup_t2436138090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[8] = 
{
	LayoutGroup_t2436138090::get_offset_of_m_Padding_2(),
	LayoutGroup_t2436138090::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t2436138090::get_offset_of_m_Rect_4(),
	LayoutGroup_t2436138090::get_offset_of_m_Tracker_5(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t2436138090::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (LayoutRebuilder_t541313304), -1, sizeof(LayoutRebuilder_t541313304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1750[9] = 
{
	LayoutRebuilder_t541313304::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t541313304::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (LayoutUtility_t2745813735), -1, sizeof(LayoutUtility_t2745813735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1751[8] = 
{
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (VerticalLayoutGroup_t923838031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1756[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (ReflectionMethodsCache_t2103211062), -1, sizeof(ReflectionMethodsCache_t2103211062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1757[5] = 
{
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t2103211062_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (Raycast3DCallback_t701940803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (Raycast2DCallback_t768590915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (RaycastAllCallback_t1884415901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1762[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_3(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_4(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1770[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (U3CModuleU3E_t692745532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (HCAnimatedBanner_t2921968851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1773[13] = 
{
	HCAnimatedBanner_t2921968851::get_offset_of_jsonLink_2(),
	HCAnimatedBanner_t2921968851::get_offset_of_bannerDataList_3(),
	0,
	0,
	HCAnimatedBanner_t2921968851::get_offset_of_textureIdName_6(),
	HCAnimatedBanner_t2921968851::get_offset_of_textureUrl_7(),
	HCAnimatedBanner_t2921968851::get_offset_of__transitionLink_8(),
	HCAnimatedBanner_t2921968851::get_offset_of_defaultTLinkAndroid_9(),
	HCAnimatedBanner_t2921968851::get_offset_of_defaultTLinkIOS_10(),
	HCAnimatedBanner_t2921968851::get_offset_of_texturePath_11(),
	HCAnimatedBanner_t2921968851::get_offset_of_count_12(),
	HCAnimatedBanner_t2921968851::get_offset_of_jCoroutine_13(),
	HCAnimatedBanner_t2921968851::get_offset_of_lCoroutine_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (AnimBannerData_t3749323191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[11] = 
{
	AnimBannerData_t3749323191::get_offset_of_textureIdName_0(),
	AnimBannerData_t3749323191::get_offset_of_textureUrl_1(),
	AnimBannerData_t3749323191::get_offset_of_textureWidth_2(),
	AnimBannerData_t3749323191::get_offset_of_textureHeight_3(),
	AnimBannerData_t3749323191::get_offset_of_spriteWidth_4(),
	AnimBannerData_t3749323191::get_offset_of_spriteHeight_5(),
	AnimBannerData_t3749323191::get_offset_of_spriteCount_6(),
	AnimBannerData_t3749323191::get_offset_of_animWaitTime_7(),
	AnimBannerData_t3749323191::get_offset_of_transitionUrlAndroid_8(),
	AnimBannerData_t3749323191::get_offset_of_transitionUrlIOS_9(),
	AnimBannerData_t3749323191::get_offset_of_waitForNextAnimSec_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (U3CLoadJsonDataU3Ec__Iterator0_t1810388169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[5] = 
{
	U3CLoadJsonDataU3Ec__Iterator0_t1810388169::get_offset_of_U3CwwwU3E__1_0(),
	U3CLoadJsonDataU3Ec__Iterator0_t1810388169::get_offset_of_U24this_1(),
	U3CLoadJsonDataU3Ec__Iterator0_t1810388169::get_offset_of_U24current_2(),
	U3CLoadJsonDataU3Ec__Iterator0_t1810388169::get_offset_of_U24disposing_3(),
	U3CLoadJsonDataU3Ec__Iterator0_t1810388169::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (U3CLoadTextureU3Ec__Iterator1_t1051594053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[9] = 
{
	U3CLoadTextureU3Ec__Iterator1_t1051594053::get_offset_of_U3CindexU3E__0_0(),
	U3CLoadTextureU3Ec__Iterator1_t1051594053::get_offset_of_U3CtextureWidthU3E__0_1(),
	U3CLoadTextureU3Ec__Iterator1_t1051594053::get_offset_of_U3CtextureHeightU3E__0_2(),
	U3CLoadTextureU3Ec__Iterator1_t1051594053::get_offset_of_U3CtextureU3E__0_3(),
	U3CLoadTextureU3Ec__Iterator1_t1051594053::get_offset_of_U3CwwwU3E__1_4(),
	U3CLoadTextureU3Ec__Iterator1_t1051594053::get_offset_of_U24this_5(),
	U3CLoadTextureU3Ec__Iterator1_t1051594053::get_offset_of_U24current_6(),
	U3CLoadTextureU3Ec__Iterator1_t1051594053::get_offset_of_U24disposing_7(),
	U3CLoadTextureU3Ec__Iterator1_t1051594053::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (U3CAnimateBannerU3Ec__Iterator2_t688012631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[16] = 
{
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_index_0(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U3CspriteWidthU3E__0_1(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U3CspriteHeightU3E__0_2(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U3ChorCountU3E__0_3(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U3CverCountU3E__0_4(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U3CspriteCountU3E__0_5(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U3CspriteRectsU3E__0_6(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U3CuvRectsU3E__0_7(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U3CrImageU3E__0_8(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U3CrIndexU3E__0_9(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U3CwaitU3E__0_10(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U24this_11(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U24current_12(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U24disposing_13(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U24PC_14(),
	U3CAnimateBannerU3Ec__Iterator2_t688012631::get_offset_of_U24locvar0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (U3CAnimateBannerU3Ec__AnonStorey3_t102123587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[3] = 
{
	U3CAnimateBannerU3Ec__AnonStorey3_t102123587::get_offset_of_textureWidth_0(),
	U3CAnimateBannerU3Ec__AnonStorey3_t102123587::get_offset_of_textureHeight_1(),
	U3CAnimateBannerU3Ec__AnonStorey3_t102123587::get_offset_of_U3CU3Ef__refU242_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (BestScore_t1653437491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (ButtonHome_t643243358), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (ButtonHomeOver_t1627401701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[2] = 
{
	ButtonHomeOver_t1627401701::get_offset_of_colorPushed_2(),
	ButtonHomeOver_t1627401701::get_offset_of_originalColor_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (ButtonPause_t3620264363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[1] = 
{
	ButtonPause_t3620264363::get_offset_of_pause_screen_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (ButtonPlay_t1912942282), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (ButtonReplay_t987972804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (ButtonReplayOver_t967915415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[2] = 
{
	ButtonReplayOver_t967915415::get_offset_of_colorPushed_2(),
	ButtonReplayOver_t967915415::get_offset_of_originalColor_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (ButtonResume_t2840336175), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (ButtonSound_t35905277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1787[3] = 
{
	ButtonSound_t35905277::get_offset_of_colorPushed_2(),
	ButtonSound_t35905277::get_offset_of_originalColor_3(),
	ButtonSound_t35905277::get_offset_of_buttonOff_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (Coin_t2227745140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1788[6] = 
{
	Coin_t2227745140::get_offset_of_inTrain_2(),
	Coin_t2227745140::get_offset_of_collided_3(),
	Coin_t2227745140::get_offset_of_inObsUp_4(),
	Coin_t2227745140::get_offset_of_coinPickClip_5(),
	Coin_t2227745140::get_offset_of_coinPickSource_6(),
	Coin_t2227745140::get_offset_of_sideCheck_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (DontDestroy_t1446738193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (GameScreen_t1800529819), -1, sizeof(GameScreen_t1800529819_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1790[37] = 
{
	GameScreen_t1800529819::get_offset_of_platformType_2(),
	GameScreen_t1800529819::get_offset_of_groundPrefab_3(),
	GameScreen_t1800529819::get_offset_of_obsPrefab_4(),
	GameScreen_t1800529819::get_offset_of_obs2Prefab_5(),
	GameScreen_t1800529819::get_offset_of_obs_6(),
	GameScreen_t1800529819::get_offset_of_player_7(),
	GameScreen_t1800529819::get_offset_of_bottomY_8(),
	GameScreen_t1800529819::get_offset_of_leftX_9(),
	GameScreen_t1800529819::get_offset_of_topY_10(),
	GameScreen_t1800529819::get_offset_of_rightX_11(),
	GameScreen_t1800529819::get_offset_of_obsTileH_12(),
	GameScreen_t1800529819::get_offset_of_obs2TileH_13(),
	GameScreen_t1800529819::get_offset_of_groundTileH_14(),
	GameScreen_t1800529819_StaticFields::get_offset_of_paused_15(),
	GameScreen_t1800529819::get_offset_of_obsHeightScale_16(),
	GameScreen_t1800529819::get_offset_of_gap_17(),
	GameScreen_t1800529819::get_offset_of_height1_18(),
	GameScreen_t1800529819::get_offset_of_height2_19(),
	GameScreen_t1800529819::get_offset_of_obsCollider_20(),
	GameScreen_t1800529819::get_offset_of_obs2Collider_21(),
	GameScreen_t1800529819::get_offset_of_overClip_22(),
	GameScreen_t1800529819::get_offset_of_overSource_23(),
	GameScreen_t1800529819::get_offset_of_deadClip_24(),
	GameScreen_t1800529819_StaticFields::get_offset_of_deadSource_25(),
	GameScreen_t1800529819_StaticFields::get_offset_of_canControl_26(),
	GameScreen_t1800529819::get_offset_of_objectMax_27(),
	GameScreen_t1800529819::get_offset_of_pauseBg_28(),
	GameScreen_t1800529819::get_offset_of_btnResume_29(),
	GameScreen_t1800529819::get_offset_of_btnHome_30(),
	GameScreen_t1800529819::get_offset_of_btnReplay_31(),
	GameScreen_t1800529819::get_offset_of_pauseText_32(),
	GameScreen_t1800529819::get_offset_of_overBg_33(),
	GameScreen_t1800529819::get_offset_of_btnHomeOver_34(),
	GameScreen_t1800529819::get_offset_of_btnReplayOver_35(),
	GameScreen_t1800529819::get_offset_of_btnPause_36(),
	GameScreen_t1800529819::get_offset_of_SherePlugins_37(),
	GameScreen_t1800529819::get_offset_of_gameOverScreen_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (U3CchangeSpeedU3Ec__Iterator0_t85360554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[4] = 
{
	U3CchangeSpeedU3Ec__Iterator0_t85360554::get_offset_of_U24this_0(),
	U3CchangeSpeedU3Ec__Iterator0_t85360554::get_offset_of_U24current_1(),
	U3CchangeSpeedU3Ec__Iterator0_t85360554::get_offset_of_U24disposing_2(),
	U3CchangeSpeedU3Ec__Iterator0_t85360554::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (U3CchangeScoreU3Ec__Iterator1_t982768640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[3] = 
{
	U3CchangeScoreU3Ec__Iterator1_t982768640::get_offset_of_U24current_0(),
	U3CchangeScoreU3Ec__Iterator1_t982768640::get_offset_of_U24disposing_1(),
	U3CchangeScoreU3Ec__Iterator1_t982768640::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (U3ChideInfoU3Ec__Iterator2_t3795124997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[3] = 
{
	U3ChideInfoU3Ec__Iterator2_t3795124997::get_offset_of_U24current_0(),
	U3ChideInfoU3Ec__Iterator2_t3795124997::get_offset_of_U24disposing_1(),
	U3ChideInfoU3Ec__Iterator2_t3795124997::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (U3CshowIfGameOverU3Ec__Iterator3_t966372778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[4] = 
{
	U3CshowIfGameOverU3Ec__Iterator3_t966372778::get_offset_of_U24this_0(),
	U3CshowIfGameOverU3Ec__Iterator3_t966372778::get_offset_of_U24current_1(),
	U3CshowIfGameOverU3Ec__Iterator3_t966372778::get_offset_of_U24disposing_2(),
	U3CshowIfGameOverU3Ec__Iterator3_t966372778::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (Ground_t4133628138), -1, sizeof(Ground_t4133628138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1795[11] = 
{
	Ground_t4133628138::get_offset_of_tilePrefab_2(),
	Ground_t4133628138::get_offset_of_bottomY_3(),
	Ground_t4133628138::get_offset_of_leftX_4(),
	Ground_t4133628138::get_offset_of_topY_5(),
	Ground_t4133628138::get_offset_of_rightX_6(),
	Ground_t4133628138::get_offset_of_groundTileW_7(),
	Ground_t4133628138::get_offset_of_groundTileH_8(),
	Ground_t4133628138::get_offset_of_block1_9(),
	Ground_t4133628138::get_offset_of_block2_10(),
	Ground_t4133628138_StaticFields::get_offset_of_posY_11(),
	Ground_t4133628138_StaticFields::get_offset_of_groundH_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (MenuScreen_t742580752), -1, sizeof(MenuScreen_t742580752_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1796[7] = 
{
	MenuScreen_t742580752::get_offset_of_bottomY_2(),
	MenuScreen_t742580752::get_offset_of_leftX_3(),
	MenuScreen_t742580752::get_offset_of_topY_4(),
	MenuScreen_t742580752::get_offset_of_rightX_5(),
	MenuScreen_t742580752::get_offset_of_groundPrefab_6(),
	MenuScreen_t742580752::get_offset_of_groundTileH_7(),
	MenuScreen_t742580752_StaticFields::get_offset_of_isSoundOn_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (MovingBall_t2489706458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[6] = 
{
	MovingBall_t2489706458::get_offset_of_sign_2(),
	MovingBall_t2489706458::get_offset_of_nowTime_3(),
	0,
	MovingBall_t2489706458::get_offset_of_rotateSpeed_5(),
	MovingBall_t2489706458::get_offset_of_moveSpeed_6(),
	MovingBall_t2489706458::get_offset_of_soccerBall_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (MovingBg_t2796125617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[12] = 
{
	MovingBg_t2796125617::get_offset_of_tilePrefab_2(),
	MovingBg_t2796125617::get_offset_of_bottomY_3(),
	MovingBg_t2796125617::get_offset_of_leftX_4(),
	MovingBg_t2796125617::get_offset_of_topY_5(),
	MovingBg_t2796125617::get_offset_of_rightX_6(),
	MovingBg_t2796125617::get_offset_of_groundTileW_7(),
	MovingBg_t2796125617::get_offset_of_groundTileH_8(),
	MovingBg_t2796125617::get_offset_of_block1_9(),
	MovingBg_t2796125617::get_offset_of_block2_10(),
	MovingBg_t2796125617::get_offset_of_block3_11(),
	MovingBg_t2796125617::get_offset_of_block4_12(),
	MovingBg_t2796125617::get_offset_of_block5_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (ObsDownCollisionCheck_t2541817184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[1] = 
{
	ObsDownCollisionCheck_t2541817184::get_offset_of_isDead_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
