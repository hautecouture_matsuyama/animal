﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAd284737821.h"
#include "AssemblyU2DCSharp_NendUnityPlugin_AD_NendAdBanner_2479415987.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"

// NendUnityPlugin.Common.NendUtils/Account
struct Account_t2230227361;
// NendUnityPlugin.Common.Gravity[]
struct GravityU5BU5D_t868584943;
// NendUnityPlugin.AD.NendAd/Margin
struct Margin_t772722833;
// NendUnityPlugin.Platform.NendAdBannerInterface
struct NendAdBannerInterface_t524056774;
// System.EventHandler
struct EventHandler_t1348719766;
// System.EventHandler`1<NendUnityPlugin.Common.NendAdErrorEventArgs>
struct EventHandler_1_t1314708246;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NendUnityPlugin.AD.NendAdBanner
struct  NendAdBanner_t489953245  : public NendAd_t284737821
{
public:
	// NendUnityPlugin.Common.NendUtils/Account NendUnityPlugin.AD.NendAdBanner::account
	Account_t2230227361 * ___account_2;
	// System.Boolean NendUnityPlugin.AD.NendAdBanner::automaticDisplay
	bool ___automaticDisplay_3;
	// System.Boolean NendUnityPlugin.AD.NendAdBanner::outputLog
	bool ___outputLog_4;
	// System.Boolean NendUnityPlugin.AD.NendAdBanner::adjustSize
	bool ___adjustSize_5;
	// NendUnityPlugin.AD.NendAdBanner/BannerSize NendUnityPlugin.AD.NendAdBanner::size
	int32_t ___size_6;
	// UnityEngine.Color NendUnityPlugin.AD.NendAdBanner::backgroundColor
	Color_t2555686324  ___backgroundColor_7;
	// NendUnityPlugin.Common.Gravity[] NendUnityPlugin.AD.NendAdBanner::gravity
	GravityU5BU5D_t868584943* ___gravity_8;
	// NendUnityPlugin.AD.NendAd/Margin NendUnityPlugin.AD.NendAdBanner::margin
	Margin_t772722833 * ___margin_9;
	// NendUnityPlugin.Platform.NendAdBannerInterface NendUnityPlugin.AD.NendAdBanner::_interface
	Il2CppObject * ____interface_10;
	// System.EventHandler NendUnityPlugin.AD.NendAdBanner::AdLoaded
	EventHandler_t1348719766 * ___AdLoaded_11;
	// System.EventHandler`1<NendUnityPlugin.Common.NendAdErrorEventArgs> NendUnityPlugin.AD.NendAdBanner::AdFailedToReceive
	EventHandler_1_t1314708246 * ___AdFailedToReceive_12;
	// System.EventHandler NendUnityPlugin.AD.NendAdBanner::AdReceived
	EventHandler_t1348719766 * ___AdReceived_13;
	// System.EventHandler NendUnityPlugin.AD.NendAdBanner::AdClicked
	EventHandler_t1348719766 * ___AdClicked_14;
	// System.EventHandler NendUnityPlugin.AD.NendAdBanner::AdBacked
	EventHandler_t1348719766 * ___AdBacked_15;
	// System.EventHandler NendUnityPlugin.AD.NendAdBanner::InformationClicked
	EventHandler_t1348719766 * ___InformationClicked_16;

public:
	inline static int32_t get_offset_of_account_2() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___account_2)); }
	inline Account_t2230227361 * get_account_2() const { return ___account_2; }
	inline Account_t2230227361 ** get_address_of_account_2() { return &___account_2; }
	inline void set_account_2(Account_t2230227361 * value)
	{
		___account_2 = value;
		Il2CppCodeGenWriteBarrier(&___account_2, value);
	}

	inline static int32_t get_offset_of_automaticDisplay_3() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___automaticDisplay_3)); }
	inline bool get_automaticDisplay_3() const { return ___automaticDisplay_3; }
	inline bool* get_address_of_automaticDisplay_3() { return &___automaticDisplay_3; }
	inline void set_automaticDisplay_3(bool value)
	{
		___automaticDisplay_3 = value;
	}

	inline static int32_t get_offset_of_outputLog_4() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___outputLog_4)); }
	inline bool get_outputLog_4() const { return ___outputLog_4; }
	inline bool* get_address_of_outputLog_4() { return &___outputLog_4; }
	inline void set_outputLog_4(bool value)
	{
		___outputLog_4 = value;
	}

	inline static int32_t get_offset_of_adjustSize_5() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___adjustSize_5)); }
	inline bool get_adjustSize_5() const { return ___adjustSize_5; }
	inline bool* get_address_of_adjustSize_5() { return &___adjustSize_5; }
	inline void set_adjustSize_5(bool value)
	{
		___adjustSize_5 = value;
	}

	inline static int32_t get_offset_of_size_6() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___size_6)); }
	inline int32_t get_size_6() const { return ___size_6; }
	inline int32_t* get_address_of_size_6() { return &___size_6; }
	inline void set_size_6(int32_t value)
	{
		___size_6 = value;
	}

	inline static int32_t get_offset_of_backgroundColor_7() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___backgroundColor_7)); }
	inline Color_t2555686324  get_backgroundColor_7() const { return ___backgroundColor_7; }
	inline Color_t2555686324 * get_address_of_backgroundColor_7() { return &___backgroundColor_7; }
	inline void set_backgroundColor_7(Color_t2555686324  value)
	{
		___backgroundColor_7 = value;
	}

	inline static int32_t get_offset_of_gravity_8() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___gravity_8)); }
	inline GravityU5BU5D_t868584943* get_gravity_8() const { return ___gravity_8; }
	inline GravityU5BU5D_t868584943** get_address_of_gravity_8() { return &___gravity_8; }
	inline void set_gravity_8(GravityU5BU5D_t868584943* value)
	{
		___gravity_8 = value;
		Il2CppCodeGenWriteBarrier(&___gravity_8, value);
	}

	inline static int32_t get_offset_of_margin_9() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___margin_9)); }
	inline Margin_t772722833 * get_margin_9() const { return ___margin_9; }
	inline Margin_t772722833 ** get_address_of_margin_9() { return &___margin_9; }
	inline void set_margin_9(Margin_t772722833 * value)
	{
		___margin_9 = value;
		Il2CppCodeGenWriteBarrier(&___margin_9, value);
	}

	inline static int32_t get_offset_of__interface_10() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ____interface_10)); }
	inline Il2CppObject * get__interface_10() const { return ____interface_10; }
	inline Il2CppObject ** get_address_of__interface_10() { return &____interface_10; }
	inline void set__interface_10(Il2CppObject * value)
	{
		____interface_10 = value;
		Il2CppCodeGenWriteBarrier(&____interface_10, value);
	}

	inline static int32_t get_offset_of_AdLoaded_11() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___AdLoaded_11)); }
	inline EventHandler_t1348719766 * get_AdLoaded_11() const { return ___AdLoaded_11; }
	inline EventHandler_t1348719766 ** get_address_of_AdLoaded_11() { return &___AdLoaded_11; }
	inline void set_AdLoaded_11(EventHandler_t1348719766 * value)
	{
		___AdLoaded_11 = value;
		Il2CppCodeGenWriteBarrier(&___AdLoaded_11, value);
	}

	inline static int32_t get_offset_of_AdFailedToReceive_12() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___AdFailedToReceive_12)); }
	inline EventHandler_1_t1314708246 * get_AdFailedToReceive_12() const { return ___AdFailedToReceive_12; }
	inline EventHandler_1_t1314708246 ** get_address_of_AdFailedToReceive_12() { return &___AdFailedToReceive_12; }
	inline void set_AdFailedToReceive_12(EventHandler_1_t1314708246 * value)
	{
		___AdFailedToReceive_12 = value;
		Il2CppCodeGenWriteBarrier(&___AdFailedToReceive_12, value);
	}

	inline static int32_t get_offset_of_AdReceived_13() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___AdReceived_13)); }
	inline EventHandler_t1348719766 * get_AdReceived_13() const { return ___AdReceived_13; }
	inline EventHandler_t1348719766 ** get_address_of_AdReceived_13() { return &___AdReceived_13; }
	inline void set_AdReceived_13(EventHandler_t1348719766 * value)
	{
		___AdReceived_13 = value;
		Il2CppCodeGenWriteBarrier(&___AdReceived_13, value);
	}

	inline static int32_t get_offset_of_AdClicked_14() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___AdClicked_14)); }
	inline EventHandler_t1348719766 * get_AdClicked_14() const { return ___AdClicked_14; }
	inline EventHandler_t1348719766 ** get_address_of_AdClicked_14() { return &___AdClicked_14; }
	inline void set_AdClicked_14(EventHandler_t1348719766 * value)
	{
		___AdClicked_14 = value;
		Il2CppCodeGenWriteBarrier(&___AdClicked_14, value);
	}

	inline static int32_t get_offset_of_AdBacked_15() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___AdBacked_15)); }
	inline EventHandler_t1348719766 * get_AdBacked_15() const { return ___AdBacked_15; }
	inline EventHandler_t1348719766 ** get_address_of_AdBacked_15() { return &___AdBacked_15; }
	inline void set_AdBacked_15(EventHandler_t1348719766 * value)
	{
		___AdBacked_15 = value;
		Il2CppCodeGenWriteBarrier(&___AdBacked_15, value);
	}

	inline static int32_t get_offset_of_InformationClicked_16() { return static_cast<int32_t>(offsetof(NendAdBanner_t489953245, ___InformationClicked_16)); }
	inline EventHandler_t1348719766 * get_InformationClicked_16() const { return ___InformationClicked_16; }
	inline EventHandler_t1348719766 ** get_address_of_InformationClicked_16() { return &___InformationClicked_16; }
	inline void set_InformationClicked_16(EventHandler_t1348719766 * value)
	{
		___InformationClicked_16 = value;
		Il2CppCodeGenWriteBarrier(&___InformationClicked_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
